
<?php $__env->startSection('content'); ?>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Add Task</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <a href="<?php echo e(url('admin-panel/task-manager/view-task')); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-eye"></i> </a>
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>"><?php echo trans('language.menu_task_manager'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('admin-panel/task-manager/add-task')); ?>">Add Task</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                <div class="headingcommon  col-lg-12" style="padding-bottom: 20px !important">Assign Task :-</div>
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-4">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Task</lable>
                          <input type="text" name="name" id="name" class="form-control" placeholder="Task">
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <lable class="from_one1" for="Amount">Priority</lable>
                          <input type="text" name="Caption" id="Caption" class="form-control"  placeholder="Priority">
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <lable class="from_one1" for="Amount">Task Date</lable>
                          <input type="Date" name="taskDate" id="" class="form-control"  placeholder="Task Date">
                        </div>
                      </div>
                    </div>
                    <div class="row">  
                      <div class="col-lg-12 text_area_desc">
                        <lable class="from_one1">Description</lable>
                        <div class="form-group">
                            <textarea rows="2" cols="30" name="father_occupation" class="form-control no-resize"  placeholder="Description"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="row" >
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary" title="Submit">Save
                        </button>
                      </div>
                      <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel
                        </button>
                      </div>
                    </div>
                  </form>
               
                
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>