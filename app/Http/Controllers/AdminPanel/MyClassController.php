<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MyClassController extends Controller
{
    public function studentLeaves() {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.my-class.view-student-leave')->with($data);
    }

    public function addStudentAttendance() {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
            'page_title'    => 'Add Student Attendance', 
            'submit_button' => 'Save',
        );
        return view('admin-panel.my-class.add-student-attendance')->with($data);
    }

    public function editStudentAttendance() {
        $loginInfo          = get_loggedin_user_data();
        $data               = array(
            'login_info'    => $loginInfo,
            'page_title'    => 'Edit Student Attendance',
            'submit_button' => 'Update',
        );
        return view('admin-panel.my-class.add-student-attendance')->with($data);
    }

     public function viewStudentAttendance() {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.my-class.index')->with($data);
    }

     public function studentAttendanceProfile() {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.my-class.profile')->with($data);
    }

      public function addRemark() {
        $loginInfo          = get_loggedin_user_data();
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.my-class.add-remark')->with($data);
    }

     public function viewRemark() {
        $loginInfo          = get_loggedin_user_data();
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.my-class.view-remark')->with($data);
    }

    public function addHomework() {
        $loginInfo          = get_loggedin_user_data();
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.my-class.add-homework')->with($data);
    }

    public function viewHomework() {
        $loginInfo          = get_loggedin_user_data();
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.my-class.view-homework')->with($data);
    }
}
