<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskManagerController extends Controller
{
    public function add() 
    {
    	$loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.task-manager.add')->with($data);
    }
    public function viewTask() 
    {
    	$loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.task-manager.view-task')->with($data);
    }

    public function viewResponses() 
    {
    	$loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.task-manager.view-responses')->with($data);
    }

    public function mapping() 
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.task-manager.mapping')->with($data);
    }
}
