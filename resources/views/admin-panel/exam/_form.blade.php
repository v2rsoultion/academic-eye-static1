@if(isset($exam['exam_id']) && !empty($exam['exam_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
</style>

{!! Form::hidden('exam_id',old('exam_id',isset($exam['exam_id']) ? $exam['exam_id'] : ''),['class' => 'gui-input', 'id' => 'exam_id', 'readonly' => 'true']) !!}

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.exam_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('exam_name', old('exam_name',isset($exam['exam_name']) ? $exam['exam_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.exam_name'), 'id' => 'exam_name']) !!}
        </div>
        @if ($errors->has('exam_name')) <p class="help-block">{{ $errors->first('exam_name') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.medium_type') !!}  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('medium_type', $exam['arr_medium'],isset($exam['medium_type']) ? $exam['medium_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if($errors->has('medium_type')) <p class="help-block">{{ $errors->first('medium_type') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.term_select') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('term_exam_id', $listData['arr_terms'],isset($exam['term_exam_id']) ? $exam['term_exam_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'term_exam_id'])!!}
                <i class="arrow double"></i>
                
            </label>
        </div>
        @if($errors->has('term_exam_id')) <p class="help-block">{{ $errors->first('term_exam_id') }}</p> @endif
    </div>    
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("specialChars", function( value, element ) {
        var regex = new RegExp("^[A-Za-z0-9- ]+$");
        var key = value;

        if (!regex.test(key)) {
           return false;
        }
        return true;
        }, "please use only alphanumeric or alphabetic characters");

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#exam-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                medium_type: {
                    required: true,
                },
                exam_name: {
                    required: true,
                    specialChars:true,
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                }
            }
        });

    });

    

</script>