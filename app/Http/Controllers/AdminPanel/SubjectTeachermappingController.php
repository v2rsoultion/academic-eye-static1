<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\SubjectTeacherMapping\SubjectTeachermapping; // Model 
use App\Model\SubjectClassMapping\SubjectClassmapping; // Model 
use App\Model\Staff\Staff; // Model 
use App\Model\Subject\Subject; // Model 
use App\Model\Classes\Classes; // Model
use Yajra\Datatables\Datatables;

class SubjectTeachermappingController extends Controller
{
    /**
     *  View page for Subject Teacher mapping
     *  @Pratyush on 08 Aug 2018
    **/
    public function index()
    {

        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_map_subject_teacher'),
            'redirect_url'  => url('admin-panel/teacher-subject-mapping/view-subject-teacher-mapping'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.map-subject-to-teacher.index')->with($data);
    }

    /**
     *  Get Subject Teacher mapping's Data for view page(Datatables)
     *  @Pratyush on 08 Aug 2018.
    **/
    public function anyData()
    {
        $loginInfo 			= get_loggedin_user_data();
        $class  			= Classes::where([['school_id', '=', $loginInfo['school_id']]])->orderBy('class_id', 'ASC')->with('getSections')->get();
       
        return Datatables::of($class)
        		->addColumn('no_of_subject', function ($class)
                {
                    return SubjectClassmapping::where('class_id',$class->class_id)->count();
                    
                })
                ->addColumn('no_of_map_subject', function ($class)
                {
                    return SubjectTeachermapping::select('subject_id')->where('class_id',$class->class_id)->groupBy('subject_id')->get()->count();
                    
                })
                ->addColumn('no_of_remain_subject', function ($class)
                {
                	$tot_sub 	= 0;
                	$map_sub 	= 0;
                	$remaining 	= 0;
                	$tot_sub 	= SubjectClassmapping::where('class_id',$class->class_id)->count();
                    $map_sub    = SubjectTeachermapping::select('subject_id')->where('class_id',$class->class_id)->groupBy('subject_id')->get()->count();

                	$remaining 	= $tot_sub - $map_sub;
                    return $remaining;
                    
                })
                ->addColumn('class_sections', function ($class)
                {
                	$sections = '';
                	foreach($class['getSections'] as $key => $value){
                		$sections .= $value->section_name.',';
                	}
                	$sections = rtrim($sections,',');
                    return $class->class_name.' - '.$sections;
                    
                })
        		->addColumn('action', function ($class)
                {
                    $encrypted_class_id = get_encrypted_value($class->class_id, true);
                    return '<div class="dropdown">
                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="zmdi zmdi-label"></i>
                                <span class="caret"></span>
                                </button>
                                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                     <li><a href="map-teacher/' . $encrypted_class_id . '" ">Map Teacher</a></li>
                                     <li><a href="view-report/' . $encrypted_class_id . '" ">View Report</a></li>
                                 </ul>
                             </div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  View page for Subject Teacher mapping - Map Subject
     *  @Pratyush on 08 Aug 2018
    **/
    public function getMapSubject($id)
    {
        $loginInfo  = get_loggedin_user_data();
        $class_name = get_class_name_by_id($id); 
        $data = array(
            'page_title'    => trans('language.map_subject_teacher'),
            'redirect_url'  => url('admin-panel/teacher-subject-mapping/view-subject-teacher-mapping'),
            'login_info'    => $loginInfo,
            'save_url'		=> url('admin-panel/teacher-subject-mapping/save'),
            'class_id'		=> $id,
            'class_name'    => $class_name
        );
        return view('admin-panel.map-subject-to-teacher.map_teacher')->with($data);
    }

    /**
     *  Get Subject Teacher mapping's Data for view page(Datatables)
     *  @Pratyush on 08 Aug 2018.
    **/
    public function anyMapSubjectData(Request $request)
    {
    	
    	$decrypted_class_id	= get_decrypted_value($request->get('class_id'), true);
        $loginInfo 			= get_loggedin_user_data();
        $subject 			= SubjectClassmapping::where('school_id', '=', $loginInfo['school_id'])->where('class_id',$decrypted_class_id)->with('getSubjects')->get();
        $arr_staff                  = get_all_staffs();
        $listData                   = [];
        
        $table = "<table class='table table-bordered table-striped table-hover js-basic-example dataTable' style='width:100%'>
                  <thead>
                    <tr>
                        <th>Subject Code - Subject Name</th>
                        <th>Teacher Name</th>
                        <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>";
                  foreach ($subject as $key => $value){
                    $staff_id_arr = array();
                    $teacher_data = SubjectTeachermapping::select('staff_ids')->where('school_id', '=', $loginInfo['school_id'])->where('class_id',$decrypted_class_id)->where('subject_id',$value->subject_id)->get();

                    foreach ($teacher_data as $teacherkey => $teachervalue){
                            $staff_id_arr[] = $teachervalue->staff_ids;
                    }
                    $table .= "<tr>";
                    $table .= "<td width='20%;'>".$value['getSubjects']->subject_code.' - '.$value['getSubjects']->subject_name."</td>
                               <td width='50%;'>";
                    $table  .= "<label class='field select' style='width: 50%'><select class='form-control show-tick select_form3' name='teacher' multiple id='teacher_ids_".$value->subject_id."'>";
                            foreach($arr_staff as $key2 => $value2){ // For Teacher's details
                                $disable = '';

                                if($key2 == ''){
                                    $disable = 'disabled="disabled"';
                                }
                                $default_select = '';
                                if(in_array($key2, $staff_id_arr)){
                                    $default_select = 'selected';
                                }
                                $table .= "<option value=".$key2." ".$default_select." ".$disable.">".$value2."</option>";
                            } 
                    $table .= "</select></label></td>";
                    $table  .= "<td><button class='btn btn-info' onclick='return assignteacher(".$value->subject_id.");' subject-id = '".$value->subject_id."'>Save</button>";
                    $table .= "</tr>";                          
        
                  }
        $table .= "</tbody></table>";
        // echo $table;die;
        return response()->json(['status'=>1001,'data'=>$table]);
    }

    /**
     *  View page for view report
     *  @Pratyush on 08 Aug 2018
    **/
    public function getViewReport($id)
    {
        $loginInfo = get_loggedin_user_data();
        $class_name = get_class_name_by_id($id); 
        $data = array(
            'page_title'    => trans('language.mst_view_report'),
            'redirect_url'  => url('admin-panel/teacher-subject-mapping/view-subject-teacher-mapping'),
            'login_info'    => $loginInfo,
            'save_url'		=> url('admin-panel/teacher-subject-mapping/save'),
            'class_id'		=> $id,
            'class_name'    => $class_name
        );
        return view('admin-panel.map-subject-to-teacher.view_report')->with($data);
    }

    /**
     *  Add and update subject class mapping's data
     *  @Pratyush on 07 Aug 2018.
    **/
    public function save(Request $request)
    {
    	
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_class_id			= get_decrypted_value($request->get('class_id'), true);
        $subject_id 				= $request->get('subject_id');
        $teacher_ids_arr 			= $request->get('teacher_ids');
        
		if(!empty($request->get('class_id')) && !empty($request->get('subject_id')) && !empty($request->get('teacher_ids'))){

				$success_msg 		= 'Data Insert successfully.';
				$previous_records	= SubjectTeachermapping::where('class_id',$decrypted_class_id)->where('subject_id',$subject_id)->count();
				
				if($previous_records != 0){
					SubjectTeachermapping::where('class_id',$decrypted_class_id)->where('subject_id',$subject_id)->delete();
				}	
				try
	            {
	            	foreach ($teacher_ids_arr as $value){
                        if($value != ''){
    	            		$subject_teacher_mapping	     = new SubjectTeachermapping;		
    	            		$subject_teacher_mapping->admin_id       = $loginInfo['admin_id'];
    		                $subject_teacher_mapping->update_by      = $loginInfo['admin_id'];
    		                $subject_teacher_mapping->school_id      = $loginInfo['school_id'];
    		                $subject_teacher_mapping->class_id 	 	 = $decrypted_class_id;
    		                $subject_teacher_mapping->subject_id 	 = $subject_id;
    		                $subject_teacher_mapping->staff_ids 	 = $value;
    		                $subject_teacher_mapping->save();
                        }
	            	}
	            }
	            catch (\Exception $e)
	            {
	                //failed logic here
	                DB::rollback();
	                $error_message = $e->getMessage();
	                return redirect()->back()->withErrors($error_message);
	            }
	            DB::commit();
				return response()->json(['status'=>1001,'message'=>'Teacher list updated successfully']);
    	}elseif(!empty($request->get('class_id')) && !empty($request->get('subject_id')) && empty($request->get('teacher_ids'))){

                SubjectTeachermapping::where('class_id',$decrypted_class_id)->where('subject_id',$subject_id)->delete();
            return response()->json(['status'=>1002,'message'=>'Teacher list updated successfully']);
        }
        else{
    		return response()->json(['status'=>404,'message'=>'Teacher can not be added/delete']);
    	}
	}

	/**
     *  Get Subject Teacher Mapping view report page's data (Datatables)
     *  @Pratyush on 08 Aug 2018.
    **/
    public function anyViewReportData(Request $request)
    {

        $loginInfo 				= get_loggedin_user_data();
        $decrypted_class_id		= get_decrypted_value($request->get('class_id'), true);

        $teacher 	= SubjectTeachermapping::where('class_id',$decrypted_class_id)->with('getStaff')->groupBy('staff_ids')->get();
        return Datatables::of($teacher)
                ->addColumn('teacher_code', function ($teacher)
                {
        			return $teacher['getStaff']->staff_attendance_unique_id;
                })
                ->addColumn('teacher_name', function ($teacher)
                {
        			return $teacher['getStaff']->staff_name;
                })
                ->addColumn('subjects', function ($teacher)
                {

                	$class_details 	= SubjectTeachermapping::select('class_id')->where('class_id',$teacher->class_id)->with('getClass')->groupBy('class_id')->where('staff_ids',$teacher->staff_ids)->get();
                    $sub_class_det = ''; // Subject Detail class wise.

                	foreach($class_details as $value){ // Getting subject details

                		$sub_details = ''; // Subjects details
                        $subject_details  = SubjectTeachermapping::select('subject_id')->with('getSubject')->where('staff_ids',$teacher->staff_ids)->where('class_id',$value->class_id)->get();

                        foreach ($subject_details as $sub_det){
                            $sub_details .= $sub_det['getSubject']->subject_name .'('.$sub_det['getSubject']->subject_code.'),';
                        }

                    $sub_class_det .= '<strong>'.$value['getClass']->class_name.'</strong> : '.$sub_details .'<br/>';
                	}
                    
        			return $sub_class_det;
                })->rawColumns(['teacher_code' => 'teacher_code','teacher_name' => 'teacher_name','subjects' => 'subjects'])->addIndexColumn()
                ->make(true);
    }
}