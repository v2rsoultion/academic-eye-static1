<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HostelReportsController extends Controller
{
     public function registerStudentReport() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.hostel-reports.register-student-report')->with($data);
    }
    public function allocationReport() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.hostel-reports.allocation-report')->with($data);
    }
    public function leaveStudentReport() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.hostel-reports.leave-student-report')->with($data);
    }
    public function freeSpaceRoomReport() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.hostel-reports.free-space-room-report')->with($data);
    }
    public function feesDueReport() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.hostel-reports.fees-due-report')->with($data);
    }
}
