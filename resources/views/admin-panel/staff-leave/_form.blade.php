<!-- @if(isset($staff['staff_id']) && !empty($staff['staff_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif -->

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
   /* .theme-blush .btn-primary {
    margin-top: 3px !important;
}*/
</style>
<!-- 
{!! Form::hidden('staff_id',old('staff_id',isset($staff['staff_id']) ? $staff['staff_id'] : ''),['class' => 'gui-input', 'id' => 'staff_id', 'readonly' => 'true']) !!}
 -->
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_name', old('staff_name',isset($staff['staff_name']) ? $staff['staff_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_name'), 'id' => 'staff_name']) !!}
        </div>
        @if ($errors->has('staff_name')) <p class="help-block">{{ $errors->first('staff_name') }}</p> @endif
    </div>
     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.leave_form_date') !!} :</lable>
        <div class="form-group">
            {!! Form::text('Form_Date', old('Form_Date',isset($holiday['Form_Date']) ? $holiday['Form_Date']: ''), ['class' => 'form-control','placeholder'=>trans('language.leave_form_date'), 'id' => 'Form_Date']) !!}
        </div>
        @if ($errors->has('Form_Date')) <p class="help-block">{{ $errors->first('Form_Date') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.leave_to_date') !!} :</lable>
        <div class="form-group">
            {!! Form::text('To_Date', old('To_Date',isset($holiday['To_Date']) ? $holiday['To_Date']: ''), ['class' => 'form-control ','placeholder'=>trans('language.leave_to_date'), 'id' => 'To_Date']) !!}
        </div>
        @if ($errors->has('To_Date')) <p class="help-block">{{ $errors->first('To_Date') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">Document File :</lable>
        <label for="file1" class="field file">   
            <input type="file" class="gui-file" name="documentfile" id="documentfile" style="margin-top: 3px !important;">
        </label>
    </div>

</div>

<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.leave_reason') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('leave_reason', old('leave_reason',isset($staff['leave_reason']) ? $staff['leave_reason']: ''), ['class' => 'form-control','placeholder'=>trans('language.leave_reason'), 'id' => 'leave_reason', 'rows'=> 2]) !!}
        </div>
        @if ($errors->has('leave_reason')) <p class="help-block">{{ $errors->first('leave_reason') }}</p> @endif
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
     <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Save">Save
                      </button>
                    </div>
                     <div class="col-lg-1 ">
                      <button type="reset" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Clear">Cancel
                      </button>
                    </div>
   <!--  <div class="col-sm-12">
        
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-primary']) !!}</a> -->
        <!-- <button type="submit" class="btn btn-raised btn-round">Cancel</button> -->
    <!-- </div> -->
</div>


<script type="text/javascript">

    $(document).ready(function() {
        $('.select2').select2();
    });

    jQuery(document).ready(function () {
         jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#staff-leave-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                staff_name: {
                    required: true
                },
                staff_profile_img: {
                    required: true
                },
                student_name: {
                    required: true
                },
                student_email: {
                    required: true,
                    email: true
                },  
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                     element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

     

     
    });

</script>