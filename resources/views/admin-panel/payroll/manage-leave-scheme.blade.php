@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Leave Scheme</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/payroll') !!}">Payroll</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-leave-scheme') !!}">Leave Scheme</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card" style="margin-bottom: 20px;">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body" style="padding: 0px 20px 20px 20px;">
               
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Add Leave Scheme :-</div> -->
                  <form class="" action="" id="manage_vendor" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Leave Name</lable>
                          <input type="text" name="leave_name" id="" class="form-control" placeholder="Leave Name">
                        </div>
                         @if ($errors->has('leave_name')) <p class="help-block">{{ $errors->first('leave_name') }}</p> @endif
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Leave Period</lable>
                          <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Leave Period</option>
                             <option value="1">Monthly</option>
                             <option value="2">Yearly</option>
                            </select>
                        </div>
                         @if ($errors->has('leave_name')) <p class="help-block">{{ $errors->first('leave_name') }}</p> @endif
                      </div>

                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Carry Forward</lable>
                          <div class="radio" style="margin-top:6px !important;">
                           <input id="radio32" name="inventory_invoice" type="radio" value="1">
                            <label for="radio32" class="document_staff" onclick="checkForAllowance('1')"> Yes</label>
                            <input id="radio31" name="inventory_invoice" type="radio" value="0">
                            <label for="radio31" class="document_staff" onclick="checkForAllowance('0')"> No</label>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-3" id="AllowanceMode" style="display: none;">
                        <div class="form-group">
                          <lable class="from_one1">Carry Forward Limit</lable>
                          <input type="text" name="carry_forward_limit" id="" class="form-control" placeholder="Carry Forward Limit">
                        </div>
                          @if ($errors->has('carry_forward_limit')) <p class="help-block">{{ $errors->first('carry_forward_limit') }}</p> @endif  
                      </div>  

                    </div>
                    <div class="row">
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">No of Leaves</lable>
                          <input type="text" name="no_of_leaves" id="" class="form-control" placeholder="No of Leaves">
                        </div>
                      </div>
                      <div class="col-lg-9">
                        <div class="form-group">
                          <lable class="from_one1">Special Instruction</lable>
                          <textarea class="form-control" placeholder="Special Instruction"></textarea>
                        </div>
                      </div>
                    </div>
                      <!-- <hr> -->
                      <div class="row">
                      <div class="col-lg-1 ">
                        <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
                        </button>
                      </div>
                      <div class="col-lg-1 ">
                        <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel
                        </button>
                      </div>
                    
                    </div>
                  </form>
                </div>
              </div>
                  <!-- <hr> -->
                  <div class="card">
                    <div class="body form-gap">
                  <div class="headingcommon  col-lg-12" style="margin-left: -13px">Search By :-</div>
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Leave Name</lable>
                          <input type="text" name="leave_name" id="" class="form-control" placeholder="Leave Name">
                        </div>
                      </div>
                     
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                
                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <!-- <hr> -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Leave Name</th>
                        <th>Leave Period</th>
                        <th>Carry Forward Limit</th>
                        <th>No of Leaves</th>
                        <th>Special Instruction</th>
                        <th>No of Staff Mapped</th>
                        <th>Not Mapped </th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Leave - 1</td>
                        <td>Monthly</td>
                        <td>2</td>
                        <td>10</td>
                        <td> any urgent work</td>
                        <td>5</td>
                        <td>15</td>
                        <td class="text-center">
                          <a href="{{ url('admin-panel/payroll/manage-leave-scheme/view-map-employees')}}" class="btn btn-raised btn-primary">Map Employee</a>
                        </td>
                      </tr>
                     <tr>
                        <td>2</td>
                        <td>Leave - 2</td>
                        <td>Yearly</td>
                        <td>10</td>
                        <td>50</td>
                        <td> Health Issue</td>
                        <td>2</td>
                        <td>15</td>
                        <td class="text-center">
                          <a href="{{ url('admin-panel/payroll/manage-leave-scheme/view-map-employees')}}" class="btn btn-raised btn-primary">Map Employee</a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->

<script type="text/javascript">
  function checkForAllowance(val) {
        var x = document.getElementById("AllowanceMode");
        if(val == 0) {
            if (x.style.display === "none") {
                x.style.display = "none";
            } else {
                x.style.display = "none";
            }
        } else {
            if (x.style.display === "block") {
                x.style.display = "block";
            } else {
                x.style.display = "block";
            }
        }
    }
</script>

@endsection