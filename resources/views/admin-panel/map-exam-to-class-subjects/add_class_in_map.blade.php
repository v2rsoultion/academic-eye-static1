@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
  padding: 14px 10px !important;
  }
  .seprateclass button {
    padding: 0px !important;
    margin-top: 0px !important;
  }
  
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-12">
        <h2>Add Classes</h2>
      </div>
      <div class="col-lg-8 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/examination') !!}">Examination And Report Cards</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/examination') !!}">Map Exam to Class Subjects</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination/map-exam-to-class-subjects/add') !!}">Add Classes</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                <div class="row tabless" >
                  <form class="searchpanel11" action="" method=""  id="searchpanel" style="width: 100%;">
                    <div class="row" style="margin:0px 0px; ">
                      <input type="hidden" id="hide" value="0" />
                       <div class="col-lg-3 padding-0">
                         <div class="form-group">
                            <lable class="from_one1" for="name">Class</lable>
                            <select class="form-control show-tick select_form1 select2" name="classes" id="show_class_subjects" onchange="showSubjects1()">
                               <option value="" >Class</option>
                               <option value="1">1st</option>
                               <option value="2" >2nd</option>
                               <option value="3" >3rd</option>
                               <option value="4" >4th</option>
                               <option value="10th" >10th</option>
                               <option value="11th" >11th</option>
                               <option value="12th" >12th</option>
                            </select>
                         </div>
                      </div>
                      <!-- <div class="col-md-7"></div> -->
                       <div class="col-lg-2" style="margin-top: 23px">
                                 <button type="button" class="btn btn-primary add_class_button" style="padding:6px 15px;"><i class="fas fa-plus-circle"></i> Add</button>
                              </div>
                    </div>
                    <div id="show_subjects1" style="display: none;">
                    <div class="row">
                     <?php 
                        $counter1 = 1;
                         for ($i=1; $i < 5 ; $i++) {  
                           if ($counter1 == 1) {
                             $addClass1 = 'active';
                           }else{
                             $addClass1 = '';
                           }
                         ?>
                          <div class="col-lg-1" style="margin-right: 20px;">
                           <div class="checkbox">
                            <input id="checkbox<?php echo $counter1;  ?>" type="checkbox">
                            <label for="checkbox<?php echo $counter1;  ?>">Subject<?php echo $counter1;  ?></label>
                          </div>
                        </div>
                      
                      <?php $counter1++; } ?>

                       <!-- <div class="clearfix"></div> -->
                        <!-- <div class="col-lg-10 "></div> -->
                       
                          </div>
                        </div>
                           <div class="input_class_wrap">
                              </div>
                          <hr>
                          <div class="row">
                           <div class="col-lg-1">
                            <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
                            </button>
                          </div>
                          <div class="col-lg-1">
                            <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel
                            </button>
                          </div></div>
                        </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
</section>
<!-- Content end here  -->
@endsection

<script type="text/javascript">
  function showSubjects1() {
    document.getElementById('show_subjects1').style.display = "block";
  }
</script>