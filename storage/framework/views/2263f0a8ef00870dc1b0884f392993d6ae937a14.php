<?php if(isset($term['term_exam_id']) && !empty($term['term_exam_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?>

<?php echo Form::hidden('term_exam_id',old('term_exam_id',isset($term['term_exam_id']) ? $term['term_exam_id'] : ''),['class' => 'gui-input', 'id' => 'term_exam_id', 'readonly' => 'true']); ?>

<?php if($errors->any()): ?>
<div class="alert alert-danger" role="alert">
    <?php echo e($errors->first()); ?>

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.terms_name'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('term_name', old('term_name',isset($term['term_exam_name']) ? $term['term_exam_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.terms_name'), 'id' => 'term_name']); ?>

        </div>
        <?php if($errors->has('term_name')): ?> <p class="help-block"><?php echo e($errors->first('term_name')); ?></p> <?php endif; ?>
    </div>
   
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1"><?php echo trans('language.terms_caption'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('term_exam_caption', old('term_exam_caption',isset($term['term_exam_caption']) ? $term['term_exam_caption']: ''), ['class' => 'form-control','placeholder'=>trans('language.terms_caption'), 'id' => 'term_exam_caption']); ?>

        </div>
        <?php if($errors->has('term_exam_caption')): ?> <p class="help-block"><?php echo e($errors->first('term_exam_caption')); ?></p> <?php endif; ?>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
  <div class="col-lg-1">
      <button type="sumbit" class="btn btn-raised btn-primary" title="Save">Save</button>
  </div>
  <div class="col-lg-1">
      <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel</button>
  </div>
</div>
<hr>
 <div class="row clearfix">                                
    <div class="col-lg-3 col-md-3">
        <div class="input-group ">
            <?php echo Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.terms_name'), 'id' => 'name']); ?>

            <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
        </div>
    </div>
    <div class="col-lg-1 col-md-1">
        <?php echo Form::submit('Search', ['class' => 'btn btn-raised  btn-primary ','name'=>'Search']); ?>

    </div>
    <div class="col-lg-1 col-md-1">
        <?php echo Form::button('Clear', ['class' => 'btn btn-raised btn-primary ','name'=>'Clear', 'id' => "clearBtn"]); ?>

    </div>
</div>
<hr>
  <div class="table-responsive">
    <table class="table m-b-0 c_list" id="term-table" style="width:100%">
    <?php echo e(csrf_field()); ?>

        <thead>
            <tr>
                <th>S No</th>
                <th><?php echo e(trans('language.terms_name')); ?></th>
                <th><?php echo e(trans('language.terms_caption')); ?></th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Exam-1</td>
                <td>First test</td>
                <td>    
                  <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                </td>  
            </tr>
            <tr>
                <td>2</td>
                <td>Exam-2</td>
                <td>Second test</td>
                <td>    
                  <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                </td>  
            </tr>
            <tr>
                <td>3</td>
                <td>Exam-3</td>
                <td>Half Yearly Exam test</td>
                <td>    
                  <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                </td>  
            </tr>
        </tbody>
    </table>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#term-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation  rules 
             ------------------------------------------ */

            rules: {
                medium_type: {
                    required: true,
                },
                term_name: {
                    required: true,
                    lettersonly:true,
                },
            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>