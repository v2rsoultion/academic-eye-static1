
<?php $__env->startSection('content'); ?>
<style type="text/css">
  .card .body .table td,
.cshelf1 {
    width: 100px;
    height: auto !important;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Manage Purchase</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/inventory'); ?>">Inventory</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/inventory/manage-purchase'); ?>">Manage Purchase</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body form-gap1">
               
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Purchase Entry :-</div> -->
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">System Invoice No: </lable>
                          <input type="text" name="system_invoice_no" id=" " value="PI-1029" class="form-control" readonly="true">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Invoice No: </lable>
                          <input type="text" name="invoice_no" id=" " class="form-control" placeholder="Invoice No">
                        </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Date: </lable>
                          <input type="text" name="date" id="date" class="form-control" placeholder="Date">
                        </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Vendor</lable>
                            <select class="form-control show-tick select_form1" name="vendor" id="">
                              <option value="">Select Vendor</option>
                              <?php $count = 1; for ($i=0; $i < 4 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Vendor <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Category</lable>
                            <select class="form-control show-tick select_form1" name="category" id="">
                              <option value="">Select Category</option>
                              <option value="1">Pen</option>
                              <option value="2">Notebooks</option>
                            </select>
                         </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Sub-Category</lable>
                            <select class="form-control show-tick select_form1" name="subcategory" id="">
                              <option value="">Select Sub-Category</option>
                              <option value="1">Blue Pen</option>
                              <option value="2">Black Pen</option>
                              <option value="3">Small Notebooks</option>
                              <option value="4">Long Notebooks</option>
                            </select>
                         </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Payment Mode</lable>
                            <select class="form-control show-tick select_form1" name="payment_mode" id="payment_mode">
                              <option value="">Select Payment</option>
                              <option value="1">Cash</option>
                              <option value="2">Bank Names</option>
                              <option value="3">Credit</option>
                              <option value="4">Cheque</option>
                            </select>
                         </div>
                      </div>
                      
                    </div>
                    <div class="row" style="display: none;" id="ask_cheque_details">
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Bank Name: </lable>
                          <input type="text" name="bank_name" id=" " class="form-control" placeholder="Bank Name">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Cheque No: </lable>
                          <input type="text" name="cheque_no" id=" " class="form-control" placeholder="Cheque No">
                        </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Cheque Date: </lable>
                          <input type="text" name="date" id="takeDate" class="form-control" placeholder="Cheque Date">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Cheque Amount: </lable>
                          <input type="text" name="cheque_amount" id=" " class="form-control" placeholder="Cheque Amount">
                        </div>
                      </div>
                    </div>
                  </form>
                  <!--  DataTable for view Records  -->
                 
                    <div class="table-responsive">
                    <table class="table table-bordered m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th style="width: 200px" class="text-center">Item Name</th>
                        <th  class="text-center">Unit</th>
                        <th style="width: 120px" class="text-center">Rate</th>
                        <th style="width: 120px" class="text-center">Quantity</th>
                        <th  class="text-center">Amount</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
            
                      <tr>
                        <td> 
                          <select class="form-control show-tick select_form1" name="" id="value12" onchange="showRate()">
                            <option value="">Select Item</option>
                            <option value="Pen">Pen</option>
                            <option value="Notebooks">Notebooks</option>
                          </select>
                          <br>Available:<label id="num"> </label>
                        </td>
                        <td id="unit" class="text-center"> </td>
                        <td><input type="text" name="rate" id="rate" class="form-control" placeholder="Rate"></td>
                        <td><input type="text" name="quantity" id="quantity" class="form-control" placeholder="Quantity" onchange="showAmount()"></td>
                        <td class="text-right"><input type="text" class="form-control" name="amount" id="amount" readonly="true"> </td>
                        <td style="text-align: center;"><a href="#" class="btn btn-raised btn-primary add_row_details">Add</a></td>
                      </tr>
                    </tbody>
                  </table>

                  <div class="table-responsive">
                    <table class="table table-bordered m-b-0 c_list" id="show_table11" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                   
                    <tbody>

                    </tbody>
                  </table>
                </div>
           
                </div>

                <div class="col-lg-6 float-right padding-0" style="width:100%; margin: 20px 0px;">
                       <table class="table table-bordered m-b-0 c_list" id="" style="width:100%">
                        <tbody>
                        <tr>
                          <td>Purchase Entry</td>
                          <td></td>
                          <td class="text-right"><label id="lbltotal"> </label></td>
                        </tr>
                        <tr>
                          <td class="col-lg-3 col-md-3">Tax(%)</td>
                          <td><input id="txttax" class="select2_single form-control" type="text"> </td>
                          <td class="text-right"><label id="lbltax"> </label></td>
                        </tr>
                        <tr>
                          <td class="col-lg-3 col-md-3">Discount(%)</td>
                          <td><input id="txtdiscount" type="text" class="select2_single form-control"></td>
                          <td class="text-right"><label id="lbldiscount"> </label></td>
                        </tr>
                        <tr>
                          <td class="col-lg-3 col-md-3">Gross Amount</td>
                          <td></td>
                          <td class="text-right"><label id="lblgross"> </label></td>
                        </tr>
                        <tr>
                          <td class="col-lg-3 col-md-3">Round off</td>
                          <td><br><input class="select2_single form-control" type="text" id="round_off">
                            </td>
                          <td> </td>
                        </tr>
                        <tr>
                          <td class="col-md-3 col-xs-3 col-sm-3">Net Amount</td>
                          <td></td>
                          <td class="text-right"><label id="lblnet"> </label></td>
                        </tr>
                       </tbody>
                     </table>
                      <form id="manage_purchase">
                      <div class="row float-right" style="margin-top: 20px">
                      
                      <div class="col-lg-2">
                        <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
                        </button>
                      </div>
                    </div>
                  </form>
                    </div>
                     
              </div>
              

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
<!-- Content end here  -->
<script>
  function showRate() {
    $("#unit").html('Nos.');
    $("#num").html(' 4');
    var vv = $("#value12").val();
  }

  function showAmount() {
    var rate = $("#rate").val();
    var quantity = $("#quantity").val();
    var amount = rate*quantity;
    $('#amount').val(amount);
  }
  $("#payment_mode").on('change', function() {
     var test = $("#payment_mode").val();
     if(test == 4)
     {
       $('#ask_cheque_details').show();
     }
     else
     {
        $('#ask_cheque_details').hide();
     }

  });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>