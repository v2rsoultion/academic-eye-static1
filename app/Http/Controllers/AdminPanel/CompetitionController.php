<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Competition\Competition; // Model 
use App\Model\Competition\CompetitionMapping; // Model 
use App\Model\Student\StudentAcademic;
use App\Model\Classes\Classes; // Model
use Yajra\Datatables\Datatables;

class CompetitionController extends Controller
{
    /**
     *  View page for Competition
     *  @Pratyush on 21 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $competition = [];
        $arr_medium                 = \Config::get('custom.medium_type');
        $competition['arr_medium']  = add_blank_option($arr_medium, 'Select Medium');
        $data = array(
            'page_title'    => trans('language.manage_competitions'),
            'redirect_url'  => url('admin-panel/competition/view-competitions'),
            'login_info'    => $loginInfo,
            'competition'   => $competition
        );
        return view('admin-panel.competition.index')->with($data);
    }

    /**
     *  Add page for Competition
     *  @Pratyush on 21 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $competition	= [];
        $loginInfo 		= get_loggedin_user_data();
        $arr_class      = [];
        $arr_medium     = \Config::get('custom.medium_type');
        if (!empty($id))
        {
            $decrypted_competition_id 	= get_decrypted_value($id, true);
            $competition      			= Competition::Find($decrypted_competition_id);
            if (!$competition)
            {
                return redirect('admin-panel/competition/add-competition')->withError('Competition not found!');
            }
            $arr_class = get_all_classes($competition->medium_type);
            $page_title             	= trans('language.edit_competitions');
            $encrypted_competition_id   		= get_encrypted_value($competition->competition_id, true);
            $save_url               	= url('admin-panel/competition/save/' . $encrypted_competition_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_competitions');
            $save_url      = url('admin-panel/competition/save');
            $submit_button = 'Save';
        }
        
        $competition['arr_medium']  = add_blank_option($arr_medium, 'Select Medium');
        $competition['arr_class']  = add_blank_option($arr_class, 'Select Class');
        // p($competition);
        $data  = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'competition' 		=> $competition,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/competition/view-competitions'),
        );
        return view('admin-panel.competition.add')->with($data);
    }

    /**
     *  Add and update Competition's data
     *  @Pratyush on 21 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
    	
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_competition_id	= get_decrypted_value($id, true);
        if (!empty($id))
        {
            $competition = Competition::find($decrypted_competition_id);

            if (!$competition)
            {
                return redirect('/admin-panel/competition/add-competition/')->withError('Competition not found!');
            }
            $success_msg = 'Competition updated successfully!';
        }
        else
        {
            $competition   	= New Competition;
            $success_msg 	= 'Competition saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'competition_name'   => 'required|unique:competitions,competition_name,' . $decrypted_competition_id . ',competition_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
         		$class_ids = null;	
         		if(Input::get('competition_level') == 1){
                    if(Input::get('competition_class_ids') !="") {
                        $class_ids = implode(',', Input::get('competition_class_ids'));
                    }
         		}
                $competition->admin_id       		      = $loginInfo['admin_id'];
                $competition->update_by      			  = $loginInfo['admin_id'];
                $competition->medium_type                 = Input::get('medium_type');
                $competition->competition_name 	          = Input::get('competition_name');
                $competition->competition_date 		      = date('Y-m-d',strtotime(Input::get('competition_date')));
                $competition->competition_issue_participation_certificate = Input::get('competition_issue_participation_certificate');
                $competition->competition_level 	      = Input::get('competition_level');
                $competition->competition_class_ids 	  = $class_ids;
                $competition->competition_description     = Input::get('competition_description');
                $competition->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/competition/view-competitions')->withSuccess($success_msg);
    }

    /**
     *  Get Competition's Data for view page(Datatables)
     *  @Pratyush on 23 July 2018.
    **/
    public function anyData(Request $request)
    {

        $loginInfo 			= get_loggedin_user_data();
        $competition		= Competition::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('competition_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null)
            {
                $query->where('medium_type', "=", $request->get('medium_type'));
            }
        })->orderBy('competition_id', 'DESC')->get();
        
        return Datatables::of($competition)
        ->addColumn('medium_type', function ($competition)
        {
            $arr_medium  = \Config::get('custom.medium_type');
            return $arr_medium[$competition->medium_type];
        })
        ->addColumn('competition_date', function ($competition)
        {
            return date('d M Y',strtotime($competition->competition_date));
            
        })
        ->addColumn('no_of_winner', function ($competition)
        {
            return CompetitionMapping::where('competition_id',$competition->competition_id)->count();
            
        })
        ->addColumn('competition_level', function ($competition)
        {
            if($competition->competition_level == 0){
                $temp_com_level = 'School';
            }else{
                $temp_com_level = 'Class';
            }
            return $temp_com_level;
            
        })
        ->addColumn('action', function ($competition)
        {
            $encrypted_competition_id = get_encrypted_value($competition->competition_id, true);
            if($competition->competition_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
            <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="Select Winner" href="competition-select-winner/' . $encrypted_competition_id . '" "">Select Winner</a>
                    </li>
                    <li>
                        <a title="List of Winner" href="competition-list-of-winner/' . $encrypted_competition_id . '" "">List of winner</a></li>
                    </li>
                </ul>
            </div>
            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="competition-status/'.$status.'/' . $encrypted_competition_id . '">'.$statusVal.'</a></div>
            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-competition/' . $encrypted_competition_id . '"><i class="zmdi zmdi-edit"></i></a></div>
            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-competition/' . $encrypted_competition_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Destroy Competition's data
     *  @Pratyush on 23 July 2018.
    **/
    public function destroy($id)
    {
        $competition_id = get_decrypted_value($id, true);
        $competition  	= Competition::find($competition_id);
        if($competition)
        {
            $competition->delete();
            $success_msg = "Competition deleted successfully!";
            return redirect('admin-panel/competition/view-competitions')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Competition not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Competition's status
     *  @Pratyush on 23 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $competition_id = get_decrypted_value($id, true);
        $competition  	= Competition::find($competition_id);
        if($competition)
        {
            $competition->competition_status  = $status;
            $competition->save();
            $success_msg = "Competition status updated!";
            return redirect('admin-panel/competition/view-competitions')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Competition not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page for Select winner List
     *  @Pratyush on 30 July 2018
    **/
    public function selectWinner($id)
    {
        
        $loginInfo                  = get_loggedin_user_data();
        $arr_medium                 = \Config::get('custom.medium_type');
        $section['arr_medium']      = add_blank_option($arr_medium, 'Select Medium');
        $arr_class                  = [];
        $arr_section                = [];
        $listData                   = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        //$competition_id             = get_decrypted_value($id, true);
        $data = array(
            'page_title'        => trans('language.comp_select_winner'),
            'redirect_url'      => url('admin-panel/virtual-class/add-student'),
            'login_info'        => $loginInfo,
            'listData'          => $listData,
            'competition_id'    => $id,
        );
        return view('admin-panel.competition.view_select_winner')->with($data);
    }

    /**
     *  Get Student List Data for Select winner page(Datatables)
     *  @Pratyush on 30 July 2018
    **/
    public function selectWinnerData(Request $request,$id=false)
    {

        $student        = [];
        $arr_students   = get_all_student_list_competition($request);
        //p($arr_students);
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        return Datatables::of($student)
            ->addColumn('class_section', function ($student)
                {
                     return $student->student_class.' - '.$student->student_session;
                    
                })
            ->addColumn('action', function ($student)
            {
                $encrypted_student_id   = get_encrypted_value($student->student_id, true);
                $competition_id         = $student->competition_id;
               
                return '<div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="caret"></span>
                            </button>
                             <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li><a href="#" id="student_pop_id" student-id="'.$encrypted_student_id.'">Set Position</a></li>
                             </ul>
                         </div>';
               
                
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  View page for List of Winner
     *  @Pratyush on 2 July 2018
    **/
    public function listWinner($id)
    {
        $loginInfo                  = get_loggedin_user_data();
        $arr_class                  = get_all_classes();
        $arr_section                = get_class_section();
        $listData                   = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'page_title'        => trans('language.manage_virtual_classes'),
            'redirect_url'      => url('admin-panel/virtual-class/add-student'),
            'login_info'        => $loginInfo,
            'listData'          => $listData,
            'competition_id'    => $id,
        );
        return view('admin-panel.competition.view_list_winner')->with($data);
    }

    /**
     *  Get Student List Data for List of Winner page(Datatables)
     *  @Pratyush on 2 July 2018
    **/
    public function listWinnerData(Request $request,$id=false)
    {
        
        $competition_id         = get_decrypted_value($request->get('competition_id'),true);
        $loginInfo              = get_loggedin_user_data();
        $competition_arr        = [];
        $competition_obj        = [];

        $competition_mapping    = CompetitionMapping::where(function($query) use ($loginInfo,$request,$competition_id){
                if (!empty($loginInfo['school_id']))
                    {
                        $query->where('student_competition_mapping.school_id',"=", $loginInfo['school_id']);
                    }
                    if (!empty($request) && $request->has('competition_id'));
                    {
                        $query->where('student_competition_mapping.competition_id',"=", $competition_id);
                    }
            })->with('getStudent')->join('student_academic_info', function($join) use ($request){
                $join->on('student_academic_info.student_id', '=', 'student_competition_mapping.student_id');
                if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null)
                {
                    $join->where('current_section_id', '=',$request->get('section_id'));
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
                {
                    $join->where('current_class_id', '=',$request->get('class_id'));
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
                {
                    $join->where('current_class_id', '=',$request->get('class_id'));
                }
            })->with('getStudentAcademic.getCurrentSection')->with('getStudentAcademic.getCurrentClass')->get();
        
        foreach ($competition_mapping as $key => $value){
            $competition_arr[$key]['compe_map_id']      = $value->student_competition_map_id;
            $competition_arr[$key]['position_rank']     = $value->position_rank;
            $competition_arr[$key]['student_name']      = $value['getStudent']['student_name'];
            $competition_arr[$key]['section_name']      = $value['getStudentAcademic']['getCurrentSection']['section_name'];
            $competition_arr[$key]['class_name']        = $value['getStudentAcademic']['getCurrentClass']['class_name'];

        }
        
        foreach ($competition_arr as $key2 => $arr_competiton)
        {
            $competition_obj[$key2] = (object) $arr_competiton;
        }
        return Datatables::of($competition_obj)
            ->addColumn('class_section', function ($competition_obj)
                {
                     return $competition_obj->class_name.' - '.$competition_obj->section_name;
                    
                })
            ->addColumn('action', function ($competition_obj)
            {
                $encrypted_com_map_id = get_encrypted_value($competition_obj->compe_map_id, true);
                
                return '<div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="caret"></span>
                            </button>
                             <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li><a href="#" id="rank_pop_id" com-map-id="'.$encrypted_com_map_id.'" rank-id="'.$competition_obj->position_rank.'">Edit Rank</a></li>
                                <li><a href="../competition-delete-student/' . $encrypted_com_map_id . '" onclick="return confirm('."'Are you sure?'".')" >Remove Student</a></li>
                             </ul>
                         </div>';
               
                
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  update Student Position for competition
     *  @Pratyush on 02 July 2018
    **/
    public function setPosition(Request $request)
    {
        
        $loginInfo          = get_loggedin_user_data();
        //Get variables from request
        if(!empty($request) && $request->has('competition_id')){
            $competition_id     = get_decrypted_value($request->get('competition_id'), true);
        }
        if(!empty($request) && $request->has('student_id')){
            $student_id         = get_decrypted_value($request->get('student_id'), true);
        }
        if(!empty($request) && $request->has('competition_map_id')){
            $decrypted_competition_map_id = get_decrypted_value($request->get('competition_map_id'), true);
        }
        $rank               = $request->get('rank');
        $operation_type     = $request->get('operation_type');
        
        if($operation_type == 1){
            $competition_map    = new CompetitionMapping;
            $success_msg        = 'Participant saved successfully!';
        }elseif($operation_type == 2){
            $competition_map     = CompetitionMapping::find($decrypted_competition_map_id);

            if(empty($competition_map)){
                
                return response()->json(['status'=>404,'message'=>'Participant not found']);
            }
        }

        DB::beginTransaction();
            try
            {
                if($operation_type == 1){        
                    $competition_map->admin_id          = $loginInfo['admin_id'];
                    $competition_map->update_by         = $loginInfo['admin_id'];
                    $competition_map->school_id         = $loginInfo['school_id'];
                    $competition_map->competition_id    = $competition_id;
                    $competition_map->student_id        = $student_id;
                    $competition_map->position_rank     = $rank;
                    $competition_map->save();
                }else{
                    $competition_map->position_rank  = $rank;
                    $competition_map->save();
                }
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return response()->json(['status'=>404,'message'=>'Position can not be added']);
            }

            DB::commit();
            return response()->json(['status'=>1001,'message'=>'Position added successfully']);

    }


    /**
     *  Remove student from Competition mapping table
     *  @Pratyush on 03 Aug 2018.
    **/
    public function destoryStudent($id)
    {
        
        $competition_map_id     = get_decrypted_value($id, true);
        $competition_map        = CompetitionMapping::find($competition_map_id);
        if($competition_map)
        {
            $competition_map->delete();
            $success_msg = "Student Removed successfully!";
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Student not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}