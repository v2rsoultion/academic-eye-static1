
<?php $__env->startSection('content'); ?>
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
  .table-responsive {
    overflow-x: visible !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Certificate</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/account'); ?>">Certificate</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="<?php echo URL::to('public/assets/images/certificate/Competition certificates.svg'); ?>" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Participation Certificate</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="<?php echo e(route('pdfviewp',['download'=>'pdf'])); ?>" class="cusa" title="View " style="width: 48%; margin: 15px auto;">
                              <i class="fas fa-eye"></i>View 
                            </a>
                            <div class="clearfix"></div>
                            
                        <div class="row clearfix"></div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="<?php echo URL::to('public/assets/images/certificate/medal.svg'); ?>" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Winner Certificate</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="<?php echo e(route('pdfview1',['download'=>'pdf'])); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                <i class="fas fa-eye"></i>View 
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                            <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/certificate/Character Certificates.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Character Certificate</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="<?php echo e(route('pdfview2',['download'=>'pdf'])); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                                <ul class="header-dropdown opendiv">
                                      <li class="dropdown">
                                        <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right slideUp">
                                          <li>
                                            <a href="<?php echo e(route('pdfview3',['download'=>'pdf'])); ?>" title="">character certificate - 1</a>
                                          </li>
                                         
                                        </ul>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                              </div>
                            </div>

                             <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/certificate/Transfer Certificates.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">TC Certificate</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="<?php echo e(route('pdfview4',['download'=>'pdf'])); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                                <ul class="header-dropdown opendiv">
                                      <li class="dropdown">
                                        <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right slideUp">
                                          <li>
                                            <a href="<?php echo e(route('pdfview5',['download'=>'pdf'])); ?>" title="">TC - template 2</a>
                                          </li>
                                          <li>
                                            <a href="<?php echo e(route('pdfview6',['download'=>'pdf'])); ?>" title="">TC - template 3</a>
                                          </li>
                                        </ul>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                              </div>
                            </div>
                            
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="<?php echo URL::to('public/assets/images/certificate/id.svg'); ?>" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Report Card Certificate</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="<?php echo e(route('pdfview8',['download'=>'pdf'])); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                <i class="fas fa-eye"></i>View 
                              </a>
                              <div class="clearfix"></div>
                              <ul class="header-dropdown opendiv">
                                      <li class="dropdown">
                                        <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right slideUp">
                                          <li>
                                            <a href="<?php echo e(route('pdfview9',['download'=>'pdf'])); ?>" title="">Report card - template 2</a>
                                          </li>
                                          <li>
                                            <a href="<?php echo e(route('pdfview10',['download'=>'pdf'])); ?>" title="">Report card - template 3</a>
                                          </li>
                                        </ul>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                            </div>
                          </div>
                          <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="<?php echo URL::to('public/assets/images/certificate/fee.svg'); ?>" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Fee Receipt</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="<?php echo e(route('pdfview7',['download'=>'pdf'])); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                <i class="fas fa-eye"></i>View 
                              </a>
                              <div class="clearfix"></div>
                              <ul class="header-dropdown opendiv">
                                      <li class="dropdown">
                                        <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right slideUp">
                                          <li>
                                            <a href="<?php echo e(route('pdfview11',['download'=>'pdf'])); ?>" title="">Fee Receipt template-2</a>
                                          </li>
                                          
                                        </ul>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                            </div>
                          </div>
                        </div>
                            
                                
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>