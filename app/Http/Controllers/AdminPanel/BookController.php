<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Book\Book; // Model
use App\Model\IssueBook\IssueBook; // Model 
use App\Model\Book\BookCopiesInfo; // Model
use Yajra\Datatables\Datatables;

class BookController extends Controller
{
    /**
     *  View page for Book
     *  @Pratyush on 14 Aug 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_bbook'),
            'redirect_url'  => url('admin-panel/book/view-books'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.book.index')->with($data);
    }

    /**
     *  Add page for Book
     *  @Pratyush on 14 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data                       = [];
        $book                       = [];
        $listData                   = [];
        $cupboard_arr               = get_all_cupbpard_data();
        $book_category              = get_all_book_categoory_data();
        $book_vendor                = get_all_book_vendor_data();
        $book_type                  = get_book_type();
        $listData['arr_cubboard']   = add_blank_option($cupboard_arr, 'Select CupBoard');
        $listData['arr_category']   = add_blank_option($book_category, 'Select Category');
        $listData['arr_vendor']     = add_blank_option($book_vendor, 'Select Vendor');
        $cupboard_shelf             = get_cupboard_shelf();
        $listData['book_type']      = add_blank_option($book_type, 'Select Type');
        $loginInfo                  = get_loggedin_user_data();
        if (!empty($id))
        {

            $decrypted_book_id      = get_decrypted_value($id, true);
            $book                   = Book::Find($decrypted_book_id);
            if (!$book)
            {
                return redirect('admin-panel/book/add-book')->withError('book not found!');
            }
            $cupboard_id            = $book->book_cupboard_id; 
            $cupboard_shelf         = get_cupboard_shelf($cupboard_id);

            $page_title             = trans('language.edit_bbook');
            $encrypted_book_id      = get_encrypted_value($book->book_id, true);
            $save_url               = url('admin-panel/book/save/' . $encrypted_book_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_bbook');
            $save_url      = url('admin-panel/book/save');
            $submit_button = 'Save';
        }

        $listData['cupboard_shelf']    = add_blank_option($cupboard_shelf, 'Select CupBoard Shelf');
        
        $data                          = array(
            'page_title'        => $page_title,
            'save_url'          => $save_url,
            'submit_button'     => $submit_button,
            'book'              => $book,
            'listData'          => $listData,
            'login_info'        => $loginInfo,
            'redirect_url'      => url('admin-panel/book/view-books'),
        );
        return view('admin-panel.book.add')->with($data);
    }

    /**
     *  Get CupBoard Shelf data according CupBoard
     *  @Shree on 16 Aug 2018
    **/
    public function getCupboardShelfData()
    {
        $cupboard_id    = Input::get('cupboard_id');
        $cupboard_shelf = get_cupboard_shelf($cupboard_id);
        $data           = view('admin-panel.book.ajax-select',compact('cupboard_shelf'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Add and update Book's data
     *  @Pratyush on 16 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
        //dd($request->has('')? 1 : 0);    
        $loginInfo                  = get_loggedin_user_data();
        $decrypted_book_id          = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $book = Book::find($decrypted_book_id);

            if (!$book)
            {
                return redirect('/admin-panel/book/add-book/')->withError('Book not found!');
            }
            $success_msg = 'Book updated successfully!';
        }
        else
        {
            $book           = New Book;
            $success_msg    = 'Book saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'book_name'   => 'required|unique:books,book_name,' . $decrypted_book_id . ',book_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $book->admin_id                 = $loginInfo['admin_id'];
                $book->update_by                = $loginInfo['admin_id'];
                $book->school_id                = $loginInfo['school_id'];
                $book->book_name                = Input::get('book_name');
                $book->book_category_id         = Input::get('book_category_id');
                $book->book_type                = Input::get('book_type');
                $book->book_subtitle            = Input::get('book_subtitle');
                $book->book_isbn_no             = Input::get('book_isbn_no');
                $book->author_name              = Input::get('author_name');
                $book->publisher_name           = Input::get('publisher_name');
                $book->edition                  = Input::get('edition');
                $book->book_vendor_id           = Input::get('book_vendor_id');
                $book->book_copies_no           = 0;
                $book->book_cupboard_id         = Input::get('book_cupboard_id');
                $book->book_cupboardshelf_id    = Input::get('book_cupboardshelf_id');
                $book->book_price               = Input::get('book_price');
                $book->book_remark              = Input::get('book_remark');
                $book->save();
                $temp_id = $book->book_id;

            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                $temp_id = '';
                //p($error_message);
                return redirect()->back()->withErrors($error_message);
            }
            
            if($temp_id != ''){
                
                foreach ($request->get('addition_copy') as $value){
                    if(isset($value['staff'])) {
                        $for_staff = 1;
                    }else{
                        $for_staff = 0;
                    }
                    $book_copies = new BookCopiesInfo;    
                    $book_copies->admin_id               = $loginInfo['admin_id'];
                    $book_copies->update_by              = $loginInfo['admin_id'];
                    $book_copies->school_id              = $loginInfo['school_id'];
                    $book_copies->book_id                = $temp_id;
                    $book_copies->book_unique_id         = $value['unique_no'];
                    $book_copies->exclusive_for_staff    = $for_staff;
                    $book_copies->save();
                }
            }

            DB::commit();
        }
        return redirect('admin-panel/book/view-books')->withSuccess($success_msg);
    }

    /**
     *  Get Book's Data for view page(Datatables)
     *  @Pratyush on 20 July 2018.
    **/
    public function anyData(Request $request)
    {
        
        $loginInfo          = get_loggedin_user_data();
        // $book               = Book::where([['school_id', '=', $loginInfo['school_id']]])->with('getCupBoard')->with('getCupBoardShelf')->orderBy('book_id', 'ASC')->get();
        $book               = Book::where(function($query) use ($loginInfo,$request){
                if (!empty($loginInfo['school_id']))
                    {
                        $query->where('school_id',"=", $loginInfo['school_id']);
                    }
                    if (!empty($request) && $request->has('book_name'));
                    {
                        $query->where('book_name', "like", "%{$request->get('book_name')}%");
                    }
                    if (!empty($request) && $request->has('publisher'));
                    {
                        $query->where('publisher_name', "like", "%{$request->get('publisher')}%");
                    }
                    if (!empty($request) && $request->has('subtitle'));
                    {
                        $query->where('book_subtitle', "like", "%{$request->get('subtitle')}%");
                    }
            })->with('getCupBoard')->with('getCupBoardShelf')->orderBy('book_id', 'ASC')->get();
        return Datatables::of($book)
                ->addColumn('issued_copies', function ($book)
                {
                    return IssueBook::where('book_id',$book->book_id)->where('issued_book_status',1)->count();

                })
                ->addColumn('cupboard_shelf', function ($book)
                {
                    return $book['getCupBoard']->book_cupboard_name.' + '. $book['getCupBoardShelf']->book_cupboardshelf_name;
                })
                ->addColumn('action', function ($book)
                {
                    $encrypted_book_id = get_encrypted_value($book->book_id, true);
                    if($book->book_status == 0) {
                        $status = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                            <div class="dropdown">
                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="zmdi zmdi-label"></i>
                                <span class="caret"></span>
                                </button>
                                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                    <li><a href="view-book-details/' . $encrypted_book_id . '" ">View</a></li>
                                 </ul>
                             </div>
                            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="book-status/'.$status.'/' . $encrypted_book_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-book/' . $encrypted_book_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-book/' . $encrypted_book_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    public function displayBook($id){
        $loginInfo          = get_loggedin_user_data();
        $decrypted_book_id  = get_decrypted_value($id, true);
        $book               = Book::where('school_id', '=', $loginInfo['school_id'])->where('book_id',$decrypted_book_id)->with('getCupBoard')->with('getCupBoardShelf')->orderBy('book_id', 'ASC')->first();

        $data = array(
            'page_title'    => trans('language.view_bbook'),
            'redirect_url'  => url('admin-panel/book/view-books'),
            'login_info'    => $loginInfo,
            'book'          => $book,
            'id'            => $id    
        );
        return view('admin-panel.book.view_book_details')->with($data);

    }

    /**
     *  Get Book's Data for view page(Datatables)
     *  @Pratyush on 20 July 2018.
    **/
    public function singleBookData(Request $request)
    {
        $decrypted_book_id  = get_decrypted_value($request->get('b_id'), true);
        
        $loginInfo          = get_loggedin_user_data();
        $book               = BookCopiesInfo::where('school_id', '=', $loginInfo['school_id'])->where('book_id',$decrypted_book_id)->orderBy('book_info_id', 'ASC')->get();
        
        return Datatables::of($book)
                ->addColumn('issued_copies', function ($book)
                {
                    if($book->book_copy_status == 1) {
                        $status = 'Issued';
                    } else {
                        $status = 'Available';
                    }
                    return $status;
                })
                ->addColumn('action', function ($book)
                {
                    $encrypted_book_id = get_encrypted_value($book->book_info_id, true);
                    
                    return '<div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="../delete-single-book/' . $encrypted_book_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Destroy book's data
     *  @Pratyush on 17 Aug 2018.
    **/
    public function destroy($id)
    {
        $book_id       = get_decrypted_value($id, true);
        $book          = Book::find($book_id);
        if ($book)
        {
            $book->delete();
            $success_msg = "Book deleted successfully!";
            return redirect('admin-panel/book/view-books')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Book not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Destroy book Copies's data
     *  @Pratyush on 17 Aug 2018.
    **/
    public function destroySingleBook($id)
    {
        $book_copy_id    = get_decrypted_value($id, true);
        $book_copy       = BookCopiesInfo::find($book_copy_id);
        
        if ($book_copy)
        {
            $book_copy->delete();
            $success_msg = "Book's copy deleted successfully!";
            $encrypted_book_id = get_encrypted_value($book_copy->book_id, true);
            return redirect('admin-panel/book/view-book-details/'.$encrypted_book_id)->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Book's copy not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

}
