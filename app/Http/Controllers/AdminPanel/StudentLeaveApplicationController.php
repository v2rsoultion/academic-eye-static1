<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\StudentLeaveApplication\StudentLeaveApplication; // Model
use Yajra\Datatables\Datatables;

class StudentLeaveApplicationController extends Controller
{
    /**
     *  View page for Title
     *  @Pratyush on 20 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_student_leave_application'),
            'redirect_url'  => url('admin-panel/student-leave-application/view-leave-application'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.student-leave-application.index')->with($data);
    }

    /**
     *  Add page for Student Leave Application
     *  @Pratyush on 10 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    			= [];
        $leave_application 	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_student_leave_id = get_decrypted_value($id, true);
            $leave_application			= StudentLeaveApplication::Find($decrypted_student_leave_id);
            if (!$leave_application)
            {
                return redirect('admin-panel/student-leave-application/add-leave-application')->withError('Leave Application not found!');
            }
            $page_title             	= trans('language.edit_student_leave_application');
            $encrypted_student_leave_id   		= get_encrypted_value($leave_application->student_leave_id, true);
            $save_url               	= url('admin-panel/student-leave-application/save/' . $encrypted_student_leave_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_student_leave_application');
            $save_url      = url('admin-panel/student-leave-application/save');
            $submit_button = 'Save';
        }
        $data  = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'leave_application' => $leave_application,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/student-leave-application/view-leave-application'),
        );
        return view('admin-panel.student-leave-application.add')->with($data);
    }

    /**
     *  Add and update Leave Application's data
     *  @Pratyush on 10 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_student_leave_id	= get_decrypted_value($id, true);
        if (!empty($id))
        {
            $leave_application = StudentLeaveApplication::find($decrypted_student_leave_id);

            if (!$leave_application)
            {
                return redirect('/admin-panel/student-leave-application/add-leave-application/')->withError('Leave Application not found!');
            }
            $success_msg = 'Leave Application updated successfully!';
        }
        else
        {
            $leave_application  = New StudentLeaveApplication;
            $success_msg 		= 'Leave Application saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                // 'title_name'   => 'required|unique:titles,title_name,' . $decrypted_title_id . ',title_id',
                'student_leave_reason'   	=> 'required',
                'student_leave_from_date'   => 'required',
                'student_leave_to_date'   	=> 'required',
                'documents'   				=> 'mimes:pdf,jpg,jpeg,png',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $leave_application->admin_id      			   = $loginInfo['admin_id'];
                $leave_application->update_by      			   = $loginInfo['admin_id'];
                $leave_application->school_id      			   = $loginInfo['school_id'];
                $leave_application->student_id     			   = $loginInfo['student_id'];
                $leave_application->student_leave_reason 	   = Input::get('student_leave_reason');
                $leave_application->student_leave_from_date    = Input::get('student_leave_from_date');
                $leave_application->student_leave_to_date 	   = Input::get('student_leave_to_date');
                if ($request->hasFile('documents'))
		        {
		            $file                          = $request->File('documents');
		            $config_document_upload_path   = \Config::get('custom.student_leave_document_file');
		            $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
		            $ext                           = substr($file->getClientOriginalName(),-4);
		            $name                          = substr($file->getClientOriginalName(),0,-4);
		            $filename                      = $name.mt_rand(0,100000).time().$ext;
		            $file->move($destinationPath, $filename);
		            $leave_application->student_leave_attachment = $filename;
		        }
                $leave_application->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                p($error_message);
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/student-leave-application/view-leave-application')->withSuccess($success_msg);
    }

    /**
     *  Get Title's Data for view page(Datatables)
     *  @Pratyush on 20 July 2018.
    **/
    public function anyData()
    {
        $loginInfo 				= get_loggedin_user_data();
        if($loginInfo['admin_type'] == 4){
            $leave_application      = StudentLeaveApplication::where('school_id', '=', $loginInfo['school_id'])->where('student_id',$loginInfo['student_id'])->orderBy('student_leave_id', 'ASC')->get();    
        }else{
            $leave_application      = StudentLeaveApplication::where('school_id', '=', $loginInfo['school_id'])->orderBy('student_leave_id', 'ASC')->get();    
        }
        return Datatables::of($leave_application)
        		->addColumn('date_from', function ($leave_application)
                {	
                    return date('d M Y',strtotime($leave_application->student_leave_from_date));
                    
                })
                ->addColumn('date_to', function ($leave_application)
                {	
                    return date('d M Y',strtotime($leave_application->student_leave_to_date));
                    
                })
                ->addColumn('student_leave_status', function ($leave_application)
                {	
                	$status = '';
                	if($leave_application->student_leave_status == 0){
                		$status = 'Pending';
                	}elseif($leave_application->student_leave_status == 1){
	               		$status = 'Approved';
                	}elseif($leave_application->student_leave_status == 2){
                		$status = 'Disapproved';
                	}
                    return $status;
                    
                })
                ->addColumn('student_leave_attachment', function ($leave_application)
                {	
                	$attachment_link = '';
                	if($leave_application->student_leave_attachment == ''){
                		$attachment_link = 'No link available';
                	}else{
                		$config_document_upload_path   = \Config::get('custom.student_leave_document_file');
                		$doc_path = $config_document_upload_path['display_path'].$leave_application->student_leave_attachment;
                		$attachment_link = '<span class="profile_detail_span_6"><a href="'.url($doc_path).'" target="_blank">View Attachment</a></span>';
                	}
                    return $attachment_link;
                    
                })
        		->addColumn('action', function ($leave_application)
                {
                    $encrypted_student_leave_id = get_encrypted_value($leave_application->student_leave_id, true);

                    return ' <div class="dropdown" >
                        <button class="btn btn-primary dropdown-toggle custom_btn" type="button" data-toggle="dropdown">Action
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="add-leave-application/'. $encrypted_student_leave_id .'" ">Edit</a></li>
                            <li><a href="delete/' . $encrypted_student_leave_id . '" onclick="return confirm('."'Are you sure?'".')" >Delete</a></li>
                            <li><a href="student-leave-application-status/1/' . $encrypted_student_leave_id . '">Approve</a></li>
                            <li><a href="student-leave-application-status/2/' . $encrypted_student_leave_id . '">Disapprove</a></li>
                        </ul>
                    </div>';
                })
                ->rawColumns(['action' => 'action','student_leave_attachment' => 'student_leave_attachment'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Delete Leave Application's data
     *  @Pratyush on 10 Aug 2018.
    **/
    public function destroy($id)
    {
        $student_leave_id 		= get_decrypted_value($id, true);
        $leave_application		= StudentLeaveApplication::find($student_leave_id);
        if ($leave_application)
        {
            $leave_application->delete();
            $success_msg = "Leave Application deleted successfully!";
            return redirect('admin-panel/student-leave-application/view-leave-application')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Leave Application not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Title's status
     *  @Pratyush on 20 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $student_leave_id 		= get_decrypted_value($id, true);
        $leave_application		= StudentLeaveApplication::find($student_leave_id);
        if ($leave_application)
        {
            $leave_application->student_leave_status  = $status;
            $leave_application->save();
            $success_msg = "Leave Application status updated!";
            return redirect('admin-panel/student-leave-application/view-leave-application')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Leave Application not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
