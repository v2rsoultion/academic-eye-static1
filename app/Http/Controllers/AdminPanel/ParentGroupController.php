<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParentGroupController extends Controller
{
    public function parentGroup()
    {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.parent-group.add-parent-group')->with($data);
    }
    public function manageParentStudent() 
     {
     	 $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.parent-group.manage-parent-student')->with($data);
     }

     public function addParent() 
     {
     	 $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.parent-group.add-parent')->with($data);
     }

      public function parentList() 
     {
     	 $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.parent-group.parent-send-message')->with($data);
     }
}
