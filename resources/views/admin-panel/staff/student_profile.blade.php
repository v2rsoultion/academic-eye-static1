@extends('admin-panel.layout.header')
@section('content')



<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_students') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.student') !!}</a></li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.student_profile') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $student['student_name'] !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="card profile-header">
                    <div class="body text-center">
                        <div class="row clearfix">
                            
                            <div class="col-lg-3 col-md-12">
                                <div class="profile-image"> 
                                    @if(!empty($student['profile']))
                                        <img src="{!! URL::to($student['profile']) !!}" alt="{!! $student['student_name'] !!}">
                                    @else 
                                        <img src="{!! URL::to('public/admin/assets/images/profile_av.jpg') !!}" alt="{!! $student['student_name'] !!}">
                                    @endif
                                </div> 
                            </div>
                            <div class="col-lg-3 col-md-12">
                                <div>
                                    <hr>
                                    <div class="profile_detail_1">
                                       <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                          <b class="profile_detail_bold_1">{!! trans('language.student_roll_no') !!} :</b> <span class="profile_detail_span_1">{!! $student['student_roll_no'] !!}</span>
                                       </div>
                                    </div>
                                    <div class="profile_detail_1">
                                       <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                          <b class="profile_detail_bold_1">{!! trans('language.student_enroll_number') !!} :</b> <span class="profile_detail_span_1">{!! $student['student_enroll_number'] !!}</span>
                                       </div>
                                    </div>
                                    <div class="profile_detail_1">
                                       <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                          <b class="profile_detail_bold_1">{!! trans('language.student_name') !!} :</b>
                                          <span  class="profile_detail_span_1">{!! $student['student_name'] !!}</span>
                                       </div>
                                    </div>
                                    <div class="profile_detail_1">
                                       <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                          <b class="profile_detail_bold_1">{!! trans('language.student_class_section') !!} :</b>
                                          <span class="profile_detail_span_1">{!! $student['current_class'] !!} - {!! $student['current_section'] !!}</span>
                                       </div>
                                    </div>
                                    <div class="profile_detail_1">
                                       <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                          <b class="profile_detail_bold_1">{!! trans('language.student_status') !!} :</b>
                                          <span class="profile_detail_span_1">{!! $student['status'] !!}</span>
                                       </div>
                                    </div>   
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                 
            </div>                                        
        </div>
    </div>

    
    
    </style>
    <div class="clearfix">
   <div class="col-lg-12 col-md-12 col-sm-12">
       <div class="card">
           <ul class="nav nav-tabs menu_tabs_info">
               <li class="nav-item"><a class="nav-link active2 active" data-toggle="tab" href="#personal">Personal Info</a></li>
               <li class="nav-item"><a class="nav-link active2" data-toggle="tab" href="#academics">Academics Info</a></li>
               <li class="nav-item"><a class="nav-link active2" data-toggle="tab" href="#documents">Documents</a></li>
               <li class="nav-item"><a class="nav-link active2" data-toggle="tab" href="#attendance">Attendance</a></li>
               <li class="nav-item"><a class="nav-link active2" data-toggle="tab" href="#leave">Leave Application</a></li>
               <li class="nav-item"><a class="nav-link active2" data-toggle="tab" href="#fees">Fees</a></li>
               <li class="nav-item"><a class="nav-link active2" data-toggle="tab" href="#marks">Marks</a></li>
               <li class="nav-item"><a class="nav-link active2" data-toggle="tab" href="#remarks">Remarks</a></li> 
               <li class="nav-item"><a class="nav-link active2" data-toggle="tab" href="#issued">Issued Certificates</a></li>
           </ul>
        </div>

<div class="tab-content">
   <div class="tab-pane body active" id="personal">
       <div class="">
           <div class="row clearfix">
               <div class="col-lg-12 col-md-12 col-sm-12">
                   <div class="tab-content">                   
                       <div class="tab-pane active" id="classlist">
                           <div class="row clearfix">
                             <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="card border_info_tabs_1">
                                     <div class="body"> 
                                         <!-- Nav tabs -->
                                         <ul class="nav nav-tabs" role="tablist">
                                             <li class="nav-item" style="margin-left: -14px;"><a class="nav-link active" data-toggle="tab" href="#home_with_icon_title"> <i class="zmdi zmdi-home"></i> Basic Info </a></li>
                                             <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile_with_icon_title"><i class="zmdi zmdi-account"></i> Other Info </a></li>
                                             <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#parent_with_icon_title"><i class="zmdi zmdi-account"></i> Parent Info </a></li>
                                             <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#address_with_icon_title"> <i class="zmdi zmdi-home"></i> Address Info </a></li>
                                             <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#health_with_icon_title"><i class="zmdi zmdi-account"></i> Health Info </a></li>
                                         </ul>
                                         
                                         <!-- Tab panes -->
                                         <div class="tab-content">
                                             <div role="tabpanel" class="tab-pane in active" id="home_with_icon_title">
                                                <div class="profile_detail_1">
                                                <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_1">{!! trans('language.student_roll_no') !!} :</b> <span class="profile_detail_span_1">{!! $student['student_roll_no'] !!}</span>
                                                </div>
                                                </div>
                                                <div class="profile_detail_1">
                                                <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_1">{!! trans('language.student_enroll_number') !!} :</b> <span class="profile_detail_span_1">{!! $student['student_enroll_number'] !!}</span>
                                                </div>
                                                </div>
                                                <div class="profile_detail_1">
                                                <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_1">{!! trans('language.student_name') !!} :</b>
                                                    <span  class="profile_detail_span_1">{!! $student['student_name'] !!}</span>
                                                </div>
                                                </div>
                                                <div class="profile_detail_1">
                                                <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_1">{!! trans('language.student_class_section') !!} :</b>
                                                    <span class="profile_detail_span_1">{!! $student['current_class'] !!} - {!! $student['current_section'] !!}</span>
                                                </div>
                                                </div>
                                                <div class="profile_detail_1">
                                                <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_1">{!! trans('language.student_status') !!} :</b>
                                                    <span class="profile_detail_span_1">{!! $student['status'] !!}</span>
                                                </div>
                                                </div> 
                                                
                                             </div>
                                             <div role="tabpanel" class="tab-pane" id="profile_with_icon_title">
                                                <div class="personal_info_1">
                                                   <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                      <b  class="personal_info_bold_1">Category :</b> <span class="personal_info_span_8">ST/SC</span>
                                                   </div>
                                                </div>
                                                <div class="personal_info_1">
                                                   <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                      <b  class="personal_info_bold_1">Caste :</b> <span class="personal_info_span_9">Kumar</span>
                                                   </div>
                                                </div>
                                                <div class="personal_info_1">
                                                   <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                      <b  class="personal_info_bold_1">Religion :</b> <span class="personal_info_span_10">Hindu</span>
                                                   </div>
                                                </div>
                                                <div class="personal_info_1">
                                                   <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                      <b  class="personal_info_bold_1">Nationality :</b> <span class="personal_info_span_11">Indian</span>
                                                   </div>
                                                </div>
                                                <div class="personal_info_1">
                                                   <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                      <b  class="personal_info_bold_1">Sibling Name :</b> <span class="personal_info_span_12">11010</span>
                                                   </div>
                                                </div>
                                                <div class="personal_info_1">
                                                   <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                      <b  class="personal_info_bold_1">Sibling Class :</b> <span class="personal_info_span_13">XI</span>
                                                   </div>
                                                </div>
                                                <div class="personal_info_1">
                                                   <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                      <b  class="personal_info_bold_1">Adhaar Card Number :</b> <span class="personal_info_span_14">11010-552452-25</span>
                                                   </div>
                                                </div>
                                             </div>
                                             <div role="tabpanel" class="tab-pane" id="parent_with_icon_title">
                                                <div class="parent_info_1">
                                                   <div class="parent_father_info_1">
                                                      <h6>Father Info :</h6>
                                                      <div class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Father Name :</b> <span class="parent_info_div_span_1">Rajesh Kumar</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Father Mobile Number :</b> <span class="parent_info_div_span_2">91+ 124512154</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Father Email Address :</b> <span class="parent_info_div_span_3">rajeshkumar@gmail.com</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Father Annual Salary :</b> <span class="parent_info_div_span_4">2.5 Lac</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Father's Occupation :</b> <span class="parent_info_div_span_5">Army</span>
                                                         </div>
                                                      </div>
                                                      <hr> 
                                                   </div>
                                                   <div class="parent_mother_info_1">
                                                      <h6>Mother Info :</h6>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Mother Name :</b> <span class="parent_info_div_span_6">Kiran Kumari</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Mother Mobile Number :</b> <span class="parent_info_div_span_7">91+ 124512154</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Mother Email Address :</b> <span class="parent_info_div_span_8">kirankumari@gmail.com</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Mother Annual Salary :</b> <span class="parent_info_div_span_9">2.5 Lac</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Mother's Occupation :</b> <span class="parent_info_div_span_10">Doctor</span>
                                                         </div>
                                                      </div>
                                                      <hr> 
                                                   </div>
                                                   <div class="parent_guardian_info_1">
                                                      <h6>Guardian Info :</h6>
                                                      <div class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Guardian Name :</b> <span  class="parent_info_div_span_11">Radha Kumari</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Guardian Mobile Number :</b> <span class="parent_info_div_span_12">91+ 124512154</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Guardian Email Address :</b> <span class="parent_info_div_span_13">radhakumari@gmail.com</span>
                                                         </div>
                                                      </div> 
                                                   </div>
                                                </div>
                                             </div>

                                             <div role="tabpanel" class="tab-pane" id="address_with_icon_title">
                                                <div class="parent_address_info_1">
                                                   <div class="parent_address_temparay_info_1">
                                                      <h6>Temporary Address :</h6>
                                                      <div class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Address :</b> <span class="parent_info_div_span_14">Jodhpur,Rajasthan</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">City :</b> <span class="parent_info_div_span_15">Jaipur</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">State :</b> <span class="parent_info_div_span_16">Rajasthan</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Pincode :</b> <span class="parent_info_div_span_17">220123</span>
                                                         </div>
                                                      </div>
                                                      <hr>
                                                   </div>

                                                   <div style="float:left;border:0px solid #333;width:100%;padding:10px 0px;">
                                                      <h6>Permanent Address :</h6>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Address :</b> <span class="parent_info_div_span_18">Jodhpur,Rajasthan</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">City :</b> <span class="parent_info_div_span_19">Jaipur</span>
                                                         </div>
                                                      </div>
                                                      <div  class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">State :</b> <span class="parent_info_div_span_20">Rajasthan</span>
                                                         </div>
                                                      </div>
                                                      <div class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Pincode :</b> <span class="parent_info_div_span_21">220123</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>

                                             <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                                <div class="parent_info_div_1">
                                                   <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                      <b class="parent_info_div_bold_1">Height :</b> <span class="parent_info_div_span_22">6 ft</span>
                                                   </div>
                                                </div>
                                                <div  class="parent_info_div_1">
                                                   <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                      <b class="parent_info_div_bold_1">Weight :</b> <span class="parent_info_div_span_23">50 Kg</span>
                                                   </div>
                                                </div>
                                                <div  class="parent_info_div_1">
                                                   <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                      <b class="parent_info_div_bold_1">Blood Group  :</b> <span class="parent_info_div_span_24">A+</span>
                                                   </div>
                                                </div>
                                                <div  class="parent_info_div_1">
                                                   <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                      <b class="parent_info_div_bold_1">Vision Right  :</b> <span class="parent_info_div_span_25">----</span>
                                                   </div>
                                                </div>
                                                <div  class="parent_info_div_1">
                                                   <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                      <b class="parent_info_div_bold_1">Vision Left  :</b> <span class="parent_info_div_span_26">----</span>
                                                   </div>
                                                </div>
                                                <div  class="parent_info_div_1">
                                                   <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1" style="font-size: 18px;">beenhere</i> <span class="icon-name"></span>
                                                      <b class="parent_info_div_bold_1">Medical Issue  :</b> 
                                                      <span class="parent_info_div_span_27">
                                                         Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                      </span>
                                                   </div>
                                                </div>
                                             </div>

                                         </div>
                                     </div>
                                 </div>
                             </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>                                                        
   </div>

<div class="tab-pane body" id="academics">
    <div class="">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="">
                            <div class="card border_info_tabs_1">
                                <div class="row clearfix">
                                   <div class="col-lg-12 col-md-12 col-sm-12">
                                       <div class="card">
                                           <!-- <div class="header">
                                               <h2><strong>Tabs</strong> With Icon Title</h2>
                                               <ul class="header-dropdown">
                                                   <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                                       <ul class="dropdown-menu dropdown-menu-right">
                                                           <li><a href="javascript:void(0);">Action</a></li>
                                                           <li><a href="javascript:void(0);">Another action</a></li>
                                                           <li><a href="javascript:void(0);">Something else</a></li>
                                                       </ul>
                                                   </li>
                                                   <li class="remove">
                                                       <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                                   </li>
                                               </ul>
                                           </div> -->
                                           <div class="body"> 
                                               <!-- Nav tabs -->
                                               <ul class="nav nav-tabs" role="tablist">
                                                   <li class="nav-item" style="margin-left:-10px;"><a class="nav-link active" data-toggle="tab" href="#academic_current_info"> <i class="zmdi zmdi-home"></i> Academic Current Info </a></li>
                                                   <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#academic_history_info"> <i class="zmdi zmdi-home"></i> Academic History Info </a></li>
                                                   <!-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#messages_with_icon_title"><i class="zmdi zmdi-email"></i> MESSAGES </a></li>
                                                   <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#settings_with_icon_title"><i class="zmdi zmdi-settings"></i> SETTINGS </a></li> -->
                                               </ul>
                                               
                                               <!-- Tab panes -->
                                               <div class="tab-content">
                                                   <div role="tabpanel" class="tab-pane in active" id="academic_current_info">
                                                      <div class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Admit Session :</b> <span class="parent_info_div_span_28">2017-2018</span>
                                                         </div>
                                                      </div>
                                                      <div class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Admit Class :</b> <span class="parent_info_div_span_29">XII</span>
                                                         </div>
                                                      </div>
                                                      <div class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Admit Section :</b> <span class="parent_info_div_span_30">A</span>
                                                         </div>
                                                      </div>
                                                      <div class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">House/Group  :</b> <span class="parent_info_div_span_31">Group 1</span>
                                                         </div>
                                                      </div>
                                                      <div class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Current Session :</b> <span  class="parent_info_div_span_32">2016-2017</span>
                                                         </div>
                                                      </div>
                                                      <div class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Current Class  :</b> <span class="parent_info_div_span_33">XI</span>
                                                         </div>
                                                      </div>
                                                      <div class="parent_info_div_1">
                                                         <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1">beenhere</i> <span class="icon-name"></span>
                                                            <b class="parent_info_div_bold_1">Current Section :</b> <span class="parent_info_div_span_34">A</span>
                                                         </div>
                                                      </div>
                                                   </div>

                                                   <div role="tabpanel" class="tab-pane" id="academic_history_info">

                                                   <div class="">
                                                       <div class="border_info_tabs_1">
                                                           <div class="table-responsive">
                                                               <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                                                   <thead>
                                                                       <tr>
                                                                           <th>S No</th>
                                                                           <th>Class Name-section</th>
                                                                           <th>Year</th>
                                                                           <th>Rank</th>
                                                                           <th>Percentage</th>
                                                                           <th>Any CC activity winner</th>
                                                                       </tr>
                                                                   </thead>
                                                                   <tbody>
                                                                       <tr>
                                                                           <td>1</td>
                                                                           <td>VII-A</td>
                                                                           <td>2017/7/17</td>
                                                                           <td>11</td>
                                                                           <td>60%</td>
                                                                           <td>----</td>
                                                                       </tr>
                                                                       <tr>
                                                                           <td>2</td>
                                                                           <td>VII-A</td>
                                                                           <td>2017/7/17</td>
                                                                           <td>11</td>
                                                                           <td>60%</td>
                                                                           <td>----</td>
                                                                       </tr>
                                                                       <tr>
                                                                           <td>3</td>
                                                                           <td>VII-A</td>
                                                                           <td>2017/7/17</td>
                                                                           <td>11</td>
                                                                           <td>60%</td>
                                                                           <td>----</td>
                                                                       </tr>
                                                                       <tr>
                                                                           <td>4</td>
                                                                           <td>VII-A</td>
                                                                           <td>2017/7/17</td>
                                                                           <td>11</td>
                                                                           <td>60%</td>
                                                                           <td>----</td>
                                                                       </tr>                                           
                                                                   </tbody>
                                                               </table>
                                                           </div>
                                                       </div>
                                                   </div>
 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div> 
</div>

   <div class="tab-pane body" id="">
       <div class="">
           <div class="row clearfix">
               <div class="col-lg-12 col-md-12 col-sm-12">
                   <div class="tab-content">                   
                       <div class="tab-pane active" id="classlist">
                           <div class="border_info_tabs_1">
                               <div class="">
                                   <div class="table-responsive">
                                       <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                           <thead>
                                               <tr>
                                                   <th>
                                                       <div class="">
                                                           <input id="checkbox1" type="checkbox" class="checkbox2">
                                                       </div>
                                                   </th>
                                                   <th>S NO</th>
                                                   <th>Height</th>
                                                   <th>Weight</th>
                                                   <th>Blood Group</th>
                                                   <th>Vision Right</th>
                                                   <th>VIsion Left</th>
                                                   <th>Medical Issue</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               <tr>
                                                   <td>
                                                       <div class="">
                                                           <input id="checkbox2" type="checkbox" class="checkbox1">
                                                       </div>
                                                   </td>
                                                   <td>1</td>
                                                   <td>6 ft</td>
                                                   <td>50 Kg</td>
                                                   <td>A+</td>
                                                   <td>?</td>
                                                   <td>?</td>
                                                   <td>None</td>
                                               </tr>                                           
                                           </tbody>
                                       </table>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div> 
   </div>

   <div class="tab-pane body" id="documents">
       <div class="">
           <div class="row clearfix">
               <div class="col-lg-12 col-md-12 col-sm-12">
                   <div class="tab-content">                   
                       <div class="tab-pane active" id="classlist">
                           <div class="">
                               <div class="border_info_tabs_1">
                                   <div class="">
                                       <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                           <thead>
                                               <tr>
                                                   <th>S No</th>
                                                   <th>Category</th>
                                                   <th>Document Details</th>
                                                   <th>Submit Date</th>
                                                   <th>Status</th>
                                                   <th>Download</th>
                                                   <th class="text-right">Action</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               <tr>
                                                   <td>1</td>
                                                   <td>----</td>
                                                   <td>----</td>
                                                   <td>2201/20/1</td>
                                                   <td>Pending</td>
                                                   <td>
                                                      <button  class="btn btn-info search_school_level3">View</button>
                                                   </td>
                                                   <td class="text-right">
                                                       <div class="dropdown">
                                                           <button onclick="myFunction8()" class="dropbtn">Action</button>
                                                           <div id="myDropdown8" class="dropdown-content">
                                                           <a href="#" class="btn btn-success view_profile">Approved</a>
                                                           <a href="#" class="btn btn-danger view_profile">Disapproved</a>
                                                           <a href="add-document-details.html" class="btn btn-info view_profile">Edit</a>
                                                           <a href="#" class="btn btn-info view_profile">Delete</a>
                                                           </div>
                                                       </div>
                                                   </td>
                                               </tr>
                                               <tr>
                                                   <td>2</td>
                                                   <td>----</td>
                                                   <td>----</td>
                                                   <td>2201/20/1</td>
                                                   <td>Pending</td>
                                                   <td>
                                                      <button  class="btn btn-info search_school_level3">View</button>
                                                   </td>
                                                   <td class="text-right">
                                                       <div class="dropdown">
                                                           <button onclick="myFunction8()" class="dropbtn">Action</button>
                                                           <div id="myDropdown8" class="dropdown-content">
                                                           <a href="#" class="btn btn-success view_profile">Approved</a>
                                                           <a href="#" class="btn btn-danger view_profile">Disapproved</a>
                                                           <a href="add-document-details.html" class="btn btn-info view_profile">Edit</a>
                                                           <a href="#" class="btn btn-info view_profile">Delete</a>
                                                           </div>
                                                       </div>
                                                   </td>
                                               </tr>                                     
                                           </tbody>
                                       </table>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div> 
   </div>

   <div class="tab-pane" id="attendance">
       <div class="">
           <div class="row clearfix">
               <div class="col-md-12 col-lg-12 col-sm-12">
                   <div class="">
                       <div class="border_info_tabs_1">
                           <button class="btn btn-primary btn-sm btn-round waves-effect" id="change-view-today">today</button>
                           <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-day" >Day</button>
                           <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-week">Week</button>
                           <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-month">Month</button>
                           <div id="calendar" class="m-t-15"></div>
                       </div>
                   </div>
               </div>
           </div>
           <div class="row border_info_tabs_1">
            <div class="col-lg-2 col-md-4 col-sm-6 col-6 text-center">
                <div class="card">
                    <div class="body">
                        <p>Total Absent</p>
                        <input type="text" class="knob" value="38" data-linecap="round" data-width="80" data-height="80" data-thickness="0.15" data-fgColor="#f67a82"
                        readonly>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-6 text-center">
                <div class="card">
                    <div class="body">
                        <p>Total Present</p>
                        <input type="text" class="knob" value="87" data-linecap="round" data-width="80" data-height="80" data-thickness="0.15" data-fgColor="#40b988"
                        readonly>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-6 text-center">
                <div class="card">
                    <div class="body">
                        <p>Total Leave</p>
                        <input type="text" class="knob" value="64" data-linecap="round" data-width="80" data-height="80" data-thickness="0.15" data-fgColor="#f7bb97"
                        readonly>
                    </div>
                </div>
            </div>
        </div>
       </div> 
   </div>

   <div class="tab-pane body" id="leave">
       <div class="">
           <div class="row clearfix">
               <div class="col-lg-12 col-md-12 col-sm-12">
                   <div class="tab-content">                   
                       <div class="tab-pane active" id="classlist">
                           <div class="">
                               <div class="border_info_tabs_1">
                                   <div class="">
                                       <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                           <thead>
                                               <tr>
                                                   <th>S No</th>
                                                   <th>Reason</th>
                                                   <th>Date range</th>
                                                   <th>Attachment link</th>
                                                   <th>Status</th>
                                                   <th class="text-right">Action</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               <tr>
                                                   <td>1</td>
                                                   <td>-----</td>
                                                   <td>1 july 2018 - 5 July 2018</td>
                                                   <td>http://.....</td>
                                                   <td>-----</td>
                                                   <td class="text-right">
                                                       <div class="dropdown">
                                                           <button onclick="myFunction1()" class="dropbtn">Action</button>
                                                           <div id="myDropdown1" class="dropdown-content">
                                                           <a href="#contact" class="btn btn-success view_profile">Approved</a>
                                                           <a href="#contact" class="btn btn-danger view_profile">Disapproved</a>
                                                           </div>
                                                       </div>
                                                   </td>
                                               </tr>
                                               <tr>
                                                   <td>2</td>
                                                   <td>-----</td>
                                                   <td>1 july 2018 - 5 July 2018</td>
                                                   <td>http://.....</td>
                                                   <td>-----</td>
                                                   <td class="text-right">
                                                       <div class="dropdown">
                                                           <button onclick="myFunction1()" class="dropbtn">Action</button>
                                                           <div id="myDropdown1" class="dropdown-content">
                                                           <a href="#" class="btn btn-success view_profile">Approved</a>
                                                           <a href="#" class="btn btn-danger view_profile">Disapproved</a>
                                                           </div>
                                                       </div>
                                                   </td>
                                               </tr>                                     
                                           </tbody>
                                       </table>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div> 
   </div>

   <div class="tab-pane body" id="fees">
       <div class="">
           <div class="row clearfix">
               <div class="col-lg-12 col-md-12 col-sm-12">
                   <div class="tab-content">                   
                       <div class="tab-pane active" id="classlist">
                           <div class="">
                               <div class="border_info_tabs_1">
                                   <div class="table-responsive">
                                       <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                           <thead>
                                               <tr>
                                                   <th>S No</th>
                                                   <th>Total Fees</th>
                                                   <th>Total Paid</th>
                                                   <th>Total Dues</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               <tr>
                                                   <td>1</td>
                                                   <td>10500</td>
                                                   <td>10000</td>
                                                   <td>500</td>
                                               </tr>                                     
                                           </tbody>
                                       </table>
                                   </div>
                               </div>

                               <div class="fees_details_table_1">
                                       <div class="fees_details_table_record_1">
                                           <h5>
                                               Previous Payment Detail
                                           </h5>
                                       </div>
                                   <div class="table-responsive">
                                       <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                           <thead>
                                               <tr>
                                                   <th>S No</th>
                                                   <th>Receipt No</th>
                                                   <th>Particulars </th>
                                                   <th>Amount</th>
                                                   <th>Date</th>
                                                   <th>Transaction Id</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               <tr>
                                                   <td>1</td>
                                                   <td>22012100</td>
                                                   <td>----</td>
                                                   <td>15000</td>
                                                   <td>2018/7/17</td>
                                                   <td>FSGHAF464655</td>
                                               </tr>
                                               <tr>
                                                   <td>2</td>
                                                   <td>22012100</td>
                                                   <td>----</td>
                                                   <td>15000</td>
                                                   <td>2018/7/17</td>
                                                   <td>FSGHAF464655</td>
                                               </tr>                                     
                                           </tbody>
                                       </table>
                                   </div>
                               </div>

                               <div class="fees_details_table_1">
                                   <div class="">
                                       <h5>
                                               Next Due Amount 
                                       </h5>
                                   </div>
                                   <div class="table-responsive">
                                       <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                           <thead>
                                               <tr>
                                                   <th>S No</th>
                                                   <th>Particulars </th>
                                                   <th>Amount</th>
                                                   <th>Date</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               <tr>
                                                   <td>1</td>
                                                   <td>----</td>
                                                   <td>17000</td>
                                                   <td>2016/7/17</td>
                                               </tr>
                                               <tr>
                                                   <td>2</td>
                                                   <td>----</td>
                                                   <td>17000</td>
                                                   <td>2016/7/17</td>
                                               </tr>                                     
                                           </tbody>
                                       </table>
                                   </div>
                               </div>

                               <div class="fees_details_table_1">
                                   <div class="fees_details_table_record_1">
                                       <h5>
                                           Fees Details 
                                       </h5>
                                   </div>
                                   <div class="table-responsive">
                                       <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                           <thead>
                                               <tr>
                                                   <th>S No</th>
                                                   <th>Receipt No</th>
                                                   <th>Particulars </th>
                                                   <th>Amount</th>
                                                   <th>Date</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               <tr>
                                                   <td>1</td>
                                                   <td>11012201</td>
                                                   <td>----</td>
                                                   <td>21000</td>
                                                   <td>2001/17/7</td>
                                               </tr>
                                               <tr>
                                                   <td>2</td>
                                                   <td>11012201</td>
                                                   <td>----</td>
                                                   <td>21000</td>
                                                   <td>2001/17/7</td>
                                               </tr>                                     
                                           </tbody>
                                       </table>
                                   </div>
                               </div>

                               <div class="fees_details_table_1">
                                   <div class="fees_details_table_record_1">
                                       <h5>
                                           All Applied Fees
                                       </h5>
                                   </div>
                                   <div class="table-responsive">
                                       <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                           <thead>
                                               <tr>
                                                   <th>S No</th>
                                                   <th>Particulars </th>
                                                   <th>Amount</th>
                                                   <th>Type</th>
                                                   <th>Date</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               <tr>
                                                   <td>1</td>
                                                   <td>-----</td>
                                                   <td>1000</td>
                                                   <td>Tuition</td>
                                                   <td>2001/7/7</td>
                                               </tr> 
                                               <tr>
                                                   <td>2</td>
                                                   <td>-----</td>
                                                   <td>1000</td>
                                                   <td>Tuition</td>
                                                   <td>2001/7/7</td>
                                               </tr>                                     
                                           </tbody>
                                       </table>
                                   </div>
                               </div>

                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div> 
   </div>

   <div class="tab-pane body" id="remarks">
       <div class="">
           <div class="row clearfix">
               <div class="col-lg-12 col-md-12 col-sm-12">
                   <div class="tab-content">                   
                       <div class="tab-pane active" id="classlist">
                           <div class="">
                               <div class="border_info_tabs_1">
                                   <div class="">
                                       <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                           <thead>
                                               <tr>
                                                   <th>S No</th>
                                                   <th>Subject Name</th>
                                                   <th>Teacher Name</th>
                                                   <th>Remark Desc</th>
                                                   <th>Remark Date</th>
                                                   <th>Rating</th>
                                                   <th class="text-right">Action</th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                               <tr>
                                                   <td>1</td>
                                                   <td>English</td>
                                                   <td>Radha Kumari</td>
                                                   <td>Text of the printing and typesetting industry.</td>
                                                   <td>2001/20/3</td>
                                                   <td>5</td>
                                                   <td class="text-right">
                                                       <div class="dropdown">
                                                           <button onclick="myFunction5()" class="dropbtn">Action</button>
                                                           <div id="myDropdown5" class="dropdown-content">
                                                           <a href="#contact" class="btn btn-info view_profile">Print</a>
                                                           <a href="#contact" class="btn btn-info view_profile">Cancel</a>
                                                           <a href="#contact" class="btn btn-info view_profile">View</a>
                                                           </div>
                                                       </div>
                                                   </td>
                                               </tr>
                                               <tr>
                                                   <td>2</td>
                                                   <td>English</td>
                                                   <td>Radha Kumari</td>
                                                   <td>Text of the printing and typesetting industry.</td>
                                                   <td>2001/20/3</td>
                                                   <td>5</td>
                                                   <td class="text-right">
                                                       <div class="dropdown">
                                                           <button onclick="myFunction5()" class="dropbtn">Action</button>
                                                           <div id="myDropdown5" class="dropdown-content">
                                                           <a href="#" class="btn btn-info view_profile">Print</a>
                                                           <a href="#" class="btn btn-info view_profile">Cancel</a>
                                                           <a href="#" class="btn btn-info view_profile">View</a>
                                                           </div>
                                                       </div>
                                                   </td>
                                               </tr>                                     
                                           </tbody>
                                       </table>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div> 
   </div>

<div class="tab-pane body" id="issued">
    <div class="">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="">
                            <div class="border_info_tabs_1">
                                <div class="parent_academin_certif">
                                    <h5>
                                        Academic Certificates (TC / CC) 
                                    </h5>
                                </div>
                                <div class="">
                                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Book No</th>
                                                <th>Sr No </th>
                                                <th>Name of Certificate</th>
                                                <th>Issued Date</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>10021</td>
                                                <td>1</td>
                                                <td>WsCube Tech</td>
                                                <td>2001/6/12</td>
                                                <td class="text-right">
                                                    <div class="dropdown">
                                                        <button onclick="myFunction6()" class="dropbtn">Action</button>
                                                        <div id="myDropdown6" class="dropdown-content">
                                                        <a href="#" class="btn btn-info view_profile">Print</a>
                                                        <a href="#" class="btn btn-info view_profile">Cancel</a>
                                                        <a href="#" class="btn btn-info view_profile">View</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>                                     
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="fees_details_table_1">
                                <div class="fees_details_table_record_1">
                                    <h5>
                                        Competition Certificates
                                    </h5>
                                </div>
                                <div class="">
                                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Certificate No</th>
                                                <th>Name of Competition</th>
                                                <th>Date of Competition</th>
                                                <th>Position</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>11001</td>
                                                <td>WsCube Tech</td>
                                                <td>2011/2/18</td>
                                                <td>2</td>
                                                <td class="text-right">
                                                    <div class="dropdown">
                                                        <button onclick="myFunction7()" class="dropbtn">Action</button>
                                                        <div id="myDropdown7" class="dropdown-content">
                                                        <a href="#" class="btn btn-info view_profile">Print</a>
                                                        <a href="#" class="btn btn-info view_profile">Cancel</a>
                                                        <a href="#" class="btn btn-info view_profile">View</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>                                     
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
</div>
</div>                          
</div>
</section>
<script>

    $(document).ready(function() {
$(".nav-item").click(function () {
    $(".nav-item").removeClass("testing");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).addClass("testing");   
});
});

</script>
@endsection




