<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountsController extends Controller
{
 	public function accountsGroup() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.account.manage-accounts-group')->with($data);
    }

    public function accountsHeads() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.account.manage-accounts-heads')->with($data);
    }

    public function openingBalance() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.account.manage-opening-balance')->with($data);
    }

    public function journalEntries() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.account.manage-journal-entries')->with($data);
    }

    public function viewJournalEntries() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.account.view-journal-entries')->with($data);
    }

    public function paymentReceiptVoucher() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.account.payment-receipt-voucher')->with($data);
    }

    public function viewProfitLoss() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.account.profit-loss-view-ledger')->with($data);
    }

    public function viewTrialBalance() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.account.trial-balance-view-ledger')->with($data);
    }

    public function ledgerVouchers() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.account.ledger-vouchers')->with($data);
    }

    public function viewLedgerVouchers() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.account.view-ledger-vouchers')->with($data);
    }

    public function viewBalanceSheet() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.account.view-balance-sheet')->with($data);
    }

     
}
