@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    /*.btn-round.btn-simple {
    padding: 5px 22px !important;
}*/
</style>
<!--  Main content here -->
<section class="content">
   <div class="block-header">
      <div class="row">
         <div class="col-lg-5 col-md-6 col-sm-12">
            <h2>{!! trans('language.one_time') !!}</h2>
         </div>
         <div class="col-lg-7 col-md-6 col-sm-12 line">
            <a href="{!! url('admin-panel/fees-collection/one-time/view-one-time') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-eye"></i> </a>
            <ul class="breadcrumb float-md-right">
               <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">{!! trans('language.menu_fee_collection') !!}</a></li>
               <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">{!! trans('language.one_time') !!}</a></li>
               <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection/one-time/add-one-time') !!}">{!! trans('language.add_one_time') !!}</a></li>
               
            </ul>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row clearfix">
         <div class="col-lg-12" id="bodypadd">
            <div class="tab-content">
               <div class="tab-pane active" id="classlist">
                  <div class="card">
                     <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
                     </div>
                     <div class="body form-gap">
                        <form class="" action="" method=""  id="add_fee_structure" style="width: 100%;">
                           <div class="row">
                              <div class="headingcommon  col-lg-12">Fees Heads :-</div>
                              <div class="col-lg-3">
                                 <div class="form-group">
                                    <lable class="from_one1" for="name">Fees Particular Name</lable>
                                    <input type="text" name="name" id="fees_particular_name" class="form-control" placeholder="Fees Particular Name">
                                 </div>
                              </div>
                              <div class="col-lg-3">
                                 <div class="form-group">
                                    <lable class="from_one1" for="name">Alias </lable>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Alias">
                                 </div>
                              </div>
                              <div class="col-lg-3">
                                 <div class="form-group">
                                    <lable class="from_one1" for="name">Class</lable>
                                    <div class="form-group m-bottom-0">
                                    <select class="form-control show-tick select_form1" name="classes" id="" multiple="multiple">
                                       <!-- <option value="">Class</option> -->
                                       <option value="1">1st</option>
                                       <option value="2">2nd</option>
                                       <option value="3">3rd</option>
                                       <option value="4">4th</option>
                                       <option value="10th"> 10th</option>
                                       <option value="11th">11th</option>
                                       <option value="12th">12th</option>
                                    </select>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-3">
                                 <div class="form-group">
                                    <lable class="from_one1" for="name">Amount </lable>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Amount">
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="col-lg-3">
                                 <div class="form-group">
                                    <lable class="from_one1" for="name">Effective Date </lable>
                                    <input type="text" name="name" id="effectiveDate" class="form-control" placeholder="Effective Date">
                                 </div>
                              </div>
                               <div class="col-lg-3 checkboxcustom">
                                 <!-- <div class="form-group"> -->
                                    <div class="checkbox">
                                       <input id="checkbox" type="checkbox">
                                       <label class="from_one1" for="checkbox">Refundable</label>
                                    </div>
                                 <!-- </div> -->
                               </div>
                               <!--  Submit button -->
                               <div class="clearfix"></div>
                               <div class="col-lg-12">
                               <button type="submit" class="btn btn-raised btn-primary float-right" title="Submit">Submit
                              </button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   </div>
  
</section>
<!-- Content end here  -->
@endsection

