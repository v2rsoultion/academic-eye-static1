@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 14px 10px !important;
  }
  .checkbox{
    float: left;
    margin-right: 20px;
    margin-top: 20px;
  }
  .nav-tabs>.nav-item>.nav-link {
    width: 100px;
    margin-right: 5px;
        border: 1px solid #ccc !important;
  }
  #tabingclass .nav-tabs>.nav-item>.nav-link {
    border-radius: 4px !important;
    padding: 8px 25px;
  }
   #tabingclass .nav-link:hover {
        background: #1f2f60 !important;
    color: #fff !important;
}
#tabingclass .nav-link:active {
        background: #1f2f60 !important;
    color: #fff !important;
}
.idi .nav-tabs>.nav-item>.nav-link.active {
  background: #1f2f60 !important;
    color: #fff !important;
}
.modal-dialog {
  max-width: 550px !important;
}

</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-12">
        <h2>Mapped Classes</h2>
      </div>
      <div class="col-lg-8 col-md-6 col-sm-12">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/examination') !!}">Examination And Report Cards</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/examination') !!}">Map Exam to Class Subjects</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination/map-exam-to-class-subjects/mapped-class') !!}">Mapped Classes</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 30px !important;">
                  <div class="" >
            
                    <div class="body idi" id="tabingclass"> 
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <?php
                         $counter=1; for ($i=1; $i < 5 ; $i++) {  
                          if ($counter == 1) {
                            $addClass = 'active show';
                          }else{
                            $addClass = '';
                          }
                          ?>
                          <li class="nav-item"><a class="nav-link <?php echo $addClass?>" data-toggle="tab" href="#class<?php echo $counter;  ?>"> <b>Class<?php echo $counter;?> </b></a></li>
                       <?php $counter++; } ?>

                    </ul>
                    
                    <!-- Tab panes -->
                    <div class="tab-content ">

                       <?php 
                       $counter1 = 1;
                       $counter12 = 1;
                       $counter123 = 1;
                       $counter1234 = 1;
                  
                        for ($i=1; $i < 5 ; $i++) {  
                          if ($counter1 == 1) {
                            $addClass1 = 'active';
                          }else{
                            $addClass1 = '';
                          }
                        ?>
                        <div role="tabpanel" class="tab-pane tabingclasss <?php echo $addClass1;?>" id="class<?php echo $counter1;  ?>"> 
                          
                               <div class="checkbox">
                                 <input id="checkbox<?php echo $counter1;  ?>" type="checkbox">
                                 <label for="checkbox<?php echo $counter1;  ?>">Subject<?php echo $counter1;  ?></label>
                              </div>
                              <div class="checkbox">
                                 <input id="checkbox1<?php echo $counter12;  ?>" type="checkbox">
                                 <label for="checkbox1<?php echo $counter12;  ?>">Subject<?php echo $counter1;  ?></label>
                              </div>
                              <div class="checkbox">
                                 <input id="checkbox2<?php echo $counter123;  ?>" type="checkbox">
                                 <label for="checkbox2<?php echo $counter123;  ?>">Subject<?php echo $counter1;  ?></label>
                              </div>
                               <div class="checkbox">
                                 <input id="checkbox3<?php echo $counter1234;  ?>" type="checkbox">
                                 <label for="checkbox3<?php echo $counter1234;  ?>">Subject<?php echo $counter1;  ?></label>
                              </div>
                                <div>
                           <button type="button" class="btn btn-raised btn-danger" style="padding: 9px 15px;"><i class="fas fa-minus-circle"></i> Remove</button></div>
                          
                              <div class="clearfix"></div>
                           
                            </div>
                              <?php $counter1++; $counter12++; $counter123++; $counter1234++; } ?>
                        </div>

                     
                    </div>
                  </div>
                  <hr>
                    <div class="col-lg-2 padding-0">
                      <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
                      </button>
                    </div>
        
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>

<!-- Content end here  -->
@endsection