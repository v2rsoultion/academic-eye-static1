<?php

namespace App\Model\State;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = "state";
    protected $primaryKey = "state_id";

    public function countryState()
    {
        return $this->belongsTo('App\Model\Country\Country', 'country_id');
    }

    public function getState()
    {
    	return $this->hasMany('App\Model\City\City','state_id');
    }
}
