<?php $__env->startSection('content'); ?>

<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2><?php echo trans('language.view_school'); ?></h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12"> 
                 <a href="<?php echo URL::to('admin-panel/school/add-school/'.$login_info['encrypted_school_id']); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
               <li class="breadcrumb-item">Configuration</li>
               <li class="breadcrumb-item"><?php echo trans('language.school'); ?></li>
               <li class="breadcrumb-item"><a href=""><?php echo trans('language.view_school'); ?></a></li>
                </ul>                
            </div>
        </div>
    </div>    
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-4 col-md-10">
                <div class="card profile-header">
                    <div class="body text-center">
                        <?php if(!empty($school['logo'])): ?>
                        <div class="profile-image"> <img src="<?php echo URL::to($school['logo']); ?>" alt=""> </div>
                        <?php else: ?>
                        <div class="profile-image"> <img src="<?php echo URL::to('public/admin/assets/images/profile_av.jpg'); ?>" alt=""> </div>
                        <?php endif; ?>
                        <div>
                            <h4 class="m-b-0"><strong><?php echo $school['school_name'];; ?></strong> </h4>
                            <span class="job_post"><?php echo $login_info['admin_role'];; ?></span>
                            <p><?php echo $school['school_address'];; ?></p>
                        </div>
                        
                    </div>                    
                </div>                               
                <div class="card">
                    <div class="body">
                        <div class=" workingtime">
                            <h6>Session</h6>
                            <?php
                            $updated_session    = get_current_session();
                            $updated_session_id = !empty($updated_session) ? $updated_session['session_id'] : null; 
                            
                            ?>
                            <div class="row">
                                <label class="field select col-md-12">
                                    <?php echo Form::select('session_id',get_arr_session(),isset($updated_session_id) ? $updated_session_id : null, ['class' => 'form-control show-tick select_form1','id'=>'set_dashboard_session']); ?>

                                    <i class="arrow double"></i>
                                </label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        
                    </div>
                </div>  
                       
            </div>
            <div class="col-lg-8 col-md-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#basic">Basic Info</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#branch">Branch Info</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#address">Address Info</a></li>                        
                    </ul>
                    <div class="tab-content">

                        <!--  Basic Info Tab -->
                        <div class="tab-pane body active" id="basic">
                            <ul class="list-unstyled">
                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_registration_no'); ?>:</strong> <?php echo $school['school_registration_no']; ?></p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_sno_numbers'); ?>:</strong> <?php echo $school['school_sno_numbers']; ?></p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_board_of_exams'); ?>:</strong> <?php echo $school['school_board_of_exams']; ?></p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_medium'); ?>:</strong> <?php echo $school['school_medium']; ?> </p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_total_students'); ?>:</strong> <?php echo $school['school_total_students']." Students"; ?>:</p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_total_staff'); ?>:</strong> <?php echo $school['school_total_staff']." Staff"; ?>:</p></li>
                            </ul>
                            <hr>
                            <p> <?php echo $school['school_description']; ?> </p>                                                        
                        </div>

                        <!-- Branch Info Tab -->
                        <div class="tab-pane body" id="branch">
                            <ul class="list-unstyled">
                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_board_of_exams'); ?>:</strong> <?php echo $school['boards']; ?></p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_class_from'); ?>:</strong> <?php echo $school['school_class_from']." Class"; ?></p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_class_to'); ?>:</strong> <?php echo $school['school_class_to']." Class"; ?></p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_fee_range_from'); ?>:</strong> <?php echo "Rs ".$school['school_fee_range_from']; ?></p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_fee_range_to'); ?>:</strong> <?php echo "Rs ".$school['school_fee_range_to']; ?></p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_facilities'); ?>:</strong> <?php echo $school['school_facilities']; ?></p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_url'); ?>:</strong> <?php echo $school['school_url']; ?></p></li>

                            </ul>
                        </div> 

                        <!-- Address Info Tab -->
                        <div class="tab-pane body" id="address">
                            <ul class="list-unstyled">
                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_address'); ?>:</strong> <?php echo $school['school_address']; ?></p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.country'); ?>:</strong> <?php echo $school['country_name']; ?></p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.state'); ?>:</strong> <?php echo $school['state_name']; ?></p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.city'); ?>:</strong> <?php echo $school['city_name']; ?></p></li>

                                <li><p><i class="zmdi zmdi-label m-r-5"></i><strong><?php echo trans('language.school_pincode'); ?>:</strong> <?php echo $school['school_pincode']; ?></p></li>

                            </ul>
                        </div>                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="header">
                                <h2><strong>Recent</strong> Activity</h2>
                                <ul class="header-dropdown">
                                    <li class="remove">
                                        <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="body user_activity">
                                <div class="streamline b-accent">
                                    <div class="sl-item">
                                        <img class="user rounded-circle" src="assets/images/xs/avatar4.jpg" alt="">
                                        <div class="sl-content">
                                            <h5 class="m-b-0">Admin Birthday</h5>
                                            <small>Jan 21 <a href="javascript:void(0);" class="text-info">Sophia</a>.</small>
                                        </div>
                                    </div>
                                    <div class="sl-item">
                                        <img class="user rounded-circle" src="assets/images/xs/avatar5.jpg" alt="">
                                        <div class="sl-content">
                                            <h5 class="m-b-0">Add New Contact</h5>
                                            <small>30min ago <a href="javascript:void(0);">Alexander</a>.</small>
                                            <small><strong>P:</strong> +264-625-2323</small>
                                            <small><strong>E:</strong> maryamamiri@gmail.com</small>
                                        </div>
                                    </div>
                                    <div class="sl-item">
                                        <img class="user rounded-circle" src="assets/images/xs/avatar6.jpg" alt="">
                                        <div class="sl-content">
                                            <h5 class="m-b-0">General Surgery</h5>
                                            <small>Today <a href="javascript:void(0);">Grayson</a>.</small>
                                            <small>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</small>
                                        </div>
                                    </div>
                                    <div class="sl-item">
                                        <img class="user rounded-circle" src="assets/images/xs/avatar7.jpg" alt="">
                                        <div class="sl-content">
                                            <h5 class="m-b-0">General Surgery</h5>
                                            <small>45min ago <a href="javascript:void(0);" class="text-info">Fidel Tonn</a>.</small>
                                            <small><strong>P:</strong> +264-625-2323</small>
                                            <small>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</small>
                                        </div>
                                    </div>                        
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>                              
            </div>
        </div>        
    </div>
</section>

<script>
    $(document).ready(function () {
        $("#set_dashboard_session").on('change', function (e)
        {
            var session_id = $(this).val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/session/set-session')); ?>",
                datatType: 'json',
                type: 'POST',
                data: {
                    'session_id': session_id,
                },
                success: function (response) {
                    location.reload();
                    alert(response.message);
                }
            });
        });
    });
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>