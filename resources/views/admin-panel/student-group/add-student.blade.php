﻿@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .card .body .table td, .cshelf1 {
        width: 50px !important;
    }
    .pp{
        width: 800px !important;
    }
    .table-responsive {
        overflow-x: visible !important;
    }
</style>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Add Student</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                 <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                  <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/student') !!}">Student</a></li>
                  <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/student') !!}">Groups</a></li>
                  <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student/group/student-group') !!}">Student Group</a></li>
                  <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student/group/student-group/manage-student') !!}">Manage Student</a></li>
                   <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student/group/student-group/manage-student/add-student') !!}">Add Student</a></li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">

                                <!-- <div class="container-fluid"> -->
                                    <div class="row clearfix">
                                           <div class="col-lg-2 col-md-2 col-sm-12">
                                            <div class="input-group ">
                                                <input type="text" class="form-control" value="" placeholder="Enroll No">
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-2 col-sm-12">
                                            <div class="input-group ">
                                                <input type="text" class="form-control" value="" placeholder="Student Name">
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                      <div class="col-lg-2 col-md-2 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="class_name">
                                                <option value="">Class</option>
                                                <option value="1">Class-1</option>
                                                <option value="2">Class-2</option>
                                                <option value="3">Class-3</option>
                                                <option value="4">Class-4</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="Class_section">
                                                <option value="">Section</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                                <option value="D">D</option>
                                            </select>
                                        </div>
                                       
                                        <div class="col-md-1">
                                            <button class="btn btn-raised btn-primary">Search</button>
                                        </div>
                                        <div class="col-md-1">
                                            <button class="btn btn-raised btn-primary">Clear</button>
                                        </div>
                                       
                                    </div>
                                <!-- </div> -->
                                <div class="alert alert-success" role="alert" id="message" style="display: none; margin-top: 20px">
                                    Student Added Successfully to this Group
                                    <button style="margin-top: 10px" type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Enroll No</th>
                                                <th>Student Name</th>
                                                <th>Class - section</th>                      
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>I1000I1A</td>
                                                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%"> <a href="#" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>XII-A</td>
                                                <td> <button type="button" class="btn btn-raised btn-primary"  onclick="showMessage()">Add Student
                                                </button></td>
                                               
                                            </tr>
                                            <tr>
                                               <td>2</td>
                                                <td>I2000I2A</td>
                                                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%"> <a href="#" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>XII-A</td>
                                                <td> <button type="button" class="btn btn-raised btn-primary"  onclick="showMessage()">Add Student
                                                </button></td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>I3000I3A</td>
                                                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%"> <a href="#" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>XII-A</td>
                                                <td> <button type="button" class="btn btn-raised btn-primary"  onclick="showMessage()">Add Student
                                                </button></td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>I4000I4A</td>
                                                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%"> <a href="#" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>XII-A</td>
                                               <td> <button type="button" class="btn btn-raised btn-primary"  onclick="showMessage()">Add Student
                                                </button></td>
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
  function showMessage() {
    document.getElementById('message').style.display = "block";
  }
</script>
@endsection
