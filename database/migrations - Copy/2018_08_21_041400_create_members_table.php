<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'library_members';
    protected $primaryKey = 'member_id';
    public function up()
    {
        if (!Schema::hasTable('library_members')){ 
            Schema::create('library_members', function (Blueprint $table) {
                $table->increments('library_member_id');
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->integer('member_id')->unsigned()->default(0);
                $table->tinyInteger('library_member_type')->default(1)->comment = '1=student,2=staff';
                $table->tinyInteger('member_member_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('library_members', function($table) {
                    $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
                });
            Schema::table('library_members', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
                });
            Schema::table('library_members', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
        }            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library_members');
    }
}
