<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Admin;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use App\Model\Country\Country; // Model 
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Datatables;

class CountryController extends Controller
{
	/**
     *  View page for country
     *  @Khushbu on 08 Sept 2018 
    **/
    public function index()
    {
        $country           = [];
        $loginInfo         = get_loggedin_user_data();
        $data 			   = array(
            'page_title'   => trans('language.view_country'),
            'redirect_url' => url('admin-panel/country/view-country'),
            'login_info'   => $loginInfo,
            'country'      => $country
        );
        return view('admin-panel.country.index')->with($data);
    }
	/**
     *  Add page for country
     *  @Khushbu on 08 Sept 2018 
    **/
    public function add(Request $request, $id = null) 
    {
    	$data    		= [];
        $country	    = [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_country_id 	 = get_decrypted_value($id, true);
            $country                 = Country::where('country_id', $decrypted_country_id)->get();
            $country                 = isset($country[0]) ? $country[0] : [];
            if (!$country)
            {
                return redirect('admin-panel/country/add-country')->withError('Country not found!');
            }
            $page_title               = trans('language.edit_country');
            $encrypted_country_id     = get_encrypted_value($country['country_id'], true);
            $save_url                 = url('admin-panel/country/save/' . $encrypted_country_id);
            $submit_button            = 'Update';
            $arr_country              = get_all_country($country);
            $country['arr_country']   = $arr_country;
        }
        else
        {
            $page_title                = trans('language.add_country');
            $save_url                  = url('admin-panel/country/save');
            $submit_button             = 'Save';
        }
        $data                   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'country'        	=> $country,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/country/view-country'),
        );
        return view('admin-panel.country.add')->with($data);
    }
    /**
     *  Add and update country data
     *  @Khushbu on 08 Sept 2018 
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo            = get_loggedin_user_data();
        $decrypted_country_id = null;
        $decrypted_country_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $country = Country::find($decrypted_country_id);
            if (!$country)
            {
                return redirect('admin-panel/country/view-country/')->withError('Country not found!');
            }
            $success_msg = 'Country updated successfully!';
        }
        else
        { 
            $country      	 = New Country;
            $success_msg     = 'Country saved successfully!';
        }
        $validatior   = Validator::make($request->all(), [
                'country_name'   => 'required|unique:country,country_name,'. $decrypted_country_id .',country_id',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $country->country_name     = Input::get('country_name');
                $country->save();
            }
            catch (\Exception $e)
            {  
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/country/view-country')->withSuccess($success_msg);
    }
    /**
     *  Get Data for view page(Datatables)
     *  @Khushbu on 08 Sept 2018 
    **/
    public function anyData(Request $request, $country_id = array())
    {
        $country     = [];
        $loginInfo   = get_loggedin_user_data();       
        $country     = Country::where(function($query) use ($country_id,$loginInfo,$request) 
            {
                if (!empty($country_id))
                {
                    $query->where('country_id', $country_id);
                }
                if (!empty($request) && !empty($request->get('country_name')))
                {
                    $query->where('country_name', "like", "%{$request->get('country_name')}%");
                }
            })->get();
            return Datatables::of($country)
                ->addColumn('action', function ($country)
                {
                    $encrypted_country_id = get_encrypted_value($country->country_id, true);
                    if($country->country_status == 0) {
                        $status    = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';

                    } else {
                        $status    = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                                <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="country-status/'.$status.'/' . $encrypted_country_id . '">'.$statusVal.'</a></div>
                                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-country/' . $encrypted_country_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-country/' . $encrypted_country_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }
    /**
     *  Destroy Country data
     *  @Khushbu on 08 Sept 2018 
    **/
    public function destroy($id)
    {
        $country_id = get_decrypted_value($id, true);
        $country    = Country::find($country_id);
        if ($country)
        {
            $country->delete();
            $success_msg = "Country deleted successfully!";
            return redirect('admin-panel/country/view-country')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Country not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    /**
     *  Change Country's status
     *  @Khushbu on 08 Sept 2018 
    **/
    public function changeStatus($status,$id)
    {
        $country_id = get_decrypted_value($id, true);
        $country    = Country::find($country_id);
        if ($country)
        {
            $country->country_status  = $status;
            $country->save();
            $success_msg = "Country status updated successfully!";
            return redirect('admin-panel/country/view-country')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Country not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
