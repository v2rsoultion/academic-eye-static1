@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_admission_form') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <a href="{!! url('admin-panel/admission-form/add-admission-form') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">

                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.menu_admission') !!}</li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.admission_form') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="">{!! trans('language.view_admission_form') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <p class="green">
                                        {{ session()->get('success') }}
                                    </p>
                                @endif
                                @if($errors->any())
                                    <p class="red">{{$errors->first()}}</p>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="form-group m-bottom-0">
                                                <label class=" field select" style="width: 100%">
                                                    {!!Form::select('session_id', $admissionForm['arr_session'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'session_id'])!!}
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="form-group m-bottom-0">
                                                <label class=" field select" style="width: 100%">
                                                    {!!Form::select('medium_type', $admissionForm['arr_medium'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type'])!!}
                                                    <i class="arrow double"></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.form_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="admission-form-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.form_name')}}</th>
                                            <th>{{trans('language.form_type')}}</th>
                                            <th>Academic Session</th>
                                            <th>For class</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
     $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#admission-form-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/admission-form/data')}}',
                data: function (d) {
                    d.session_id = $('select[name=session_id]').val();
                    d.medium_type = $('select[name=medium_type]').val();
                    d.name = $('input[name=name]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'form_name', name: 'form_name'},
                {data: 'form_type', name: 'form_type'},
                {data: 'session_name', name: 'session_name'},
                {data: 'class_name', name: 'class_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 5, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });


</script>
@endsection




