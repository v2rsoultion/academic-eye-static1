@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
  .table-responsive {
    overflow-x: visible !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Inventory</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">Inventory</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Inventory/invoice configuration.svg') !!}" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Invoice Configuration</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <!-- <a href="{{ url('admin-panel/staff-menu/my-class/add-student-attendance') }}" class="float-left" title="Add ">
                              <i class="fas fa-plus"></i> Add 
                            </a> -->
                            <a href="{{ url('admin-panel/inventory/manage-invoice-configuration') }}" class="cusa" title="View " style="width: 48%; margin: 15px auto;">
                              <i class="fas fa-eye"></i>Manage 
                            </a>
                            <div class="clearfix"></div>
                            
                        <div class="row clearfix"></div>
                          </div>
                        </div>
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Inventory/Vendor.svg') !!}" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Vendor</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <!-- <a href="{{ url('admin-panel/staff-menu/my-class/add-student-attendance') }}" class="float-left" title="Add ">
                              <i class="fas fa-plus"></i> Add 
                            </a> -->
                            <a href="{{ url('admin-panel/inventory/manage-vendor') }}" class="cusa" title="View " style="width: 48%; margin: 15px auto;">
                              <i class="fas fa-eye"></i>Manage 
                            </a>
                            <div class="clearfix"></div>
                            
                        <div class="row clearfix"></div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="{!! URL::to('public/assets/images/Inventory/Category.svg') !!}" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Category</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                             <!--  <a href="{{ url('/admin-panel/student-leave-application/add-leave-application') }}" class="float-left" title="Add ">
                                <i class="fas fa-plus"></i> Add 
                              </a> -->
                              <a href="{{ url('admin-panel/inventory/manage-category') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                <i class="fas fa-eye"></i>Manage 
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                            <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="{!! URL::to('public/assets/images/Inventory/Unit.svg') !!}" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Unit</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <!-- <a href="{{ url('admin-panel/staff-menu/my-class/remark/add-remark') }}" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a> -->
                                <a href="{{ url('admin-panel/inventory/manage-unit') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                  <i class="fas fa-eye"></i>Manage 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                              
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="{!! URL::to('public/assets/images/Inventory/Items.svg') !!}" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Items</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                               <!--  <a href="{{ url('admin-panel/staff-menu/my-class/homework/add-homework') }}" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a> -->
                                <a href="{{ url('admin-panel/inventory/manage-items') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                  <i class="fas fa-eye"></i>Manage 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Inventory/Purchase Entry.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Purchase Entry</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <!-- <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="{{ url('admin-panel/inventory/manage-purchase') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>Manage 
                                  </a>
                                  <div class="clearfix"></div>
                                   <ul class="header-dropdown opendiv">
                                      <li class="dropdown">
                                        <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right slideUp">
                                          <li>
                                            <a href="{{ url('admin-panel/inventory/view-purchase-entry') }}" title="">View Purchase Entry</a>
                                          </li>
                                         
                                        </ul>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                              </div>
                               <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Inventory/Sales.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Sales</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <!-- <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="{{ url('admin-panel/inventory/manage-sales') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>Manage 
                                  </a>
                                  <div class="clearfix"></div>
                                   <ul class="header-dropdown opendiv">
                                      <li class="dropdown">
                                        <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right slideUp">
                                          <li>
                                            <a href="{{ url('admin-panel/inventory/view-sales') }}" title="">View Sales</a>
                                          </li>
                                         
                                        </ul>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                              </div>
                              
                                 <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Inventory/Print Invoice.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Print Invoice</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <!-- <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="{{ url('admin-panel/inventory/manage-print-invoice') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>Manage 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                               
                            </div>
                            <div class="row">
                              <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Inventory/Purchase Return.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Purchase Return</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <!-- <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="{{ url('admin-panel/inventory/manage-purchase-return') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>Manage 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Inventory/Sales Return.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Sales Return</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <!-- <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="{{ url('admin-panel/inventory/manage-sales-return') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>Manage 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                                <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Inventory/stock register.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Stock Register</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <!-- <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="{{ url('admin-panel/inventory/manage-stock-register') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>Manage 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                            </div>
                                
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </section>
@endsection
