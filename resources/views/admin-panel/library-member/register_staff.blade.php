@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.member_staff') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.library_member') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.member_staff') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">{!! trans('language.view_students') !!}</a></li>
                                    <li class="nav-item"><a class="nav-link"  href="{{ url('/admin-panel/student/add-student') }}">{!! trans('language.add_student') !!}</a></li>
                                </ul>                        
                            </div> -->
                            <div class="body">
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4">
                                            <div class="input-group ">
                                                {!! Form::text('member_staff_emp_id', old('member_staff_emp_id', ''), ['class' => 'form-control ','placeholder'=>trans('language.member_staff_emp_id'), 'id' => 'member_staff_emp_id']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <div class="input-group ">
                                                {!! Form::text('member_staff_name', old('member_staff_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.member_staff_name'), 'id' => 'member_staff_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                {!! Form::open(['files'=>TRUE,'id' => 'member-student-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label class="option block mn" >
                                                    <input type="checkbox" id="check_all"> 
                                                    <span class="checkbox mn"></span>
                                                    </label>
                                                </th>
                                                <th>{{trans('language.member_staff_emp_id')}}</th>
                                                <th>{{trans('language.member_staff_photo')}}</th>
                                                <th>{{trans('language.member_staff_name')}}</th>
                                                <th>{{trans('language.member_staff_designation')}}</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>

                                <div class="container-fluid">
                                    <div class="row row_seach_field1">
                                            {!! Form::submit('Register All', ['class' => 'btn btn-info float-right search_button3','name'=>'save','onclick' => 'return checksubmitvalues()']) !!}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#student-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: "{{url('admin-panel/member/register-staff-data')}}",
                data: function (d) {
                    d.member_staff_emp_id = $('input[name=member_staff_emp_id]').val();
                    d.member_staff_name   = $('input[name=member_staff_name]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'checkbox', name: 'checkbox'},
                {data: 'emp_id', name: 'emp_id'},
                {data: 'profile', render: getImg},
                {data: 'staff_name', name: 'staff_name'},
                {data: 'designation_name', name: 'designation_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0,
                    "orderable": false
                },
                {
                    "targets": 5, // your case first column
                    "width": "20%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "10%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            $("select[name='section_id'").html('');
            $("select[name='section_id'").selectpicker('refresh');
            $("select[name='class_id'").selectpicker('refresh');
            table.draw();
            e.preventDefault();
        })

        
    });

    function getImg(data, type, full, meta) {
        if (data != '') {
            return '<img src="'+data+'" height="50" />';
        } else {
            return 'No Image';
        }
    }
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    
    function getSection(class_id)
    {
        if(class_id != ''){
            $('.mycustloading').css('display','block');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html('');
                    $("select[name='section_id'").html(data.options);
                    $("select[name='section_id'").removeAttr("disabled");
                    $("select[name='section_id'").selectpicker('refresh');
                    $('.mycustloading').css('display','none');
                }
            });
        }else{
            $("select[name='section_id'").html('');
            $("select[name='section_id'").selectpicker('refresh');
        }
    }

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
    });

    function checksubmitvalues(){
        return $(".check:checked").length != 0 ? true : false;
    }
</script>
@endsection