<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterStudentController extends Controller
{
     public function registerStudent() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.register-student.view-register-student')->with($data);
    }
}
