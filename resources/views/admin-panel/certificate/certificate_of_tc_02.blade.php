<!DOCTYPE html>
<html>
<head>
	<title>TC Certificate</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">

	<style type="text/css">
	body {
		width: 1020px;
		margin: auto;
		margin-top: 20px;
		font-size: 30px;
	}

	.content_size {
		font-size: 16px;
		font-style: italic;
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		border-radius: 10px;
		border: 5px solid #1f2f60;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 16px;
		font-style: italic;
	}
	.dotted {
		border-bottom: 2px dotted #000;
		font-size: 13px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 0px;
	}
	.rl {
		padding: 0px;
	}
	/*table{
		height: 40px;

	}*/
	</style>
</head>
<body>
	
	<div class="header">
	
  		<h4 class="align header_gap" style="margin-top:10px; font-size: 25px;margin-bottom: 1px;">APEX SENIOR SECONDARY SCHOOL</h4>
  		<p class="align" style="font-size: 11px;margin-top: 0px;margin-bottom: 5px;"> No. 8-2-404/1, Road No 6, Near Times Hospital, Street Opp GVK. Banjara Hills, Hyderabad- 500034</p>
  		<h5 class="align" style="font-size:14px;margin:0px;">Affiliated to Central Board of Secondary Education, New Delhi</h5>
  		<p class="align" style="font-size: 14px;font-weight: bold;margin-bottom: 10px;margin-top: 5px;">Affiliation No. &nbsp; 130345</p>
		<!-- <div class="align" style="margin: 15px 0px;">
		<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="60px"></div> -->
		<div class="align" style="width: 400px;margin:auto; font-size:20px;"><b>TRANSFER CERTIFICATE</b></div>

		<table style="font-weight: bold;">
			<tr>
				<td style ="width: 65px !important;" class="align_right content_size"> Book </td> 
				<td style="width: 80px" class="dotted align"> 2 </td>
				<td style ="width: 327px !important;" class="align_right content_size"> SI. No.</td> 
				<td style="width: 80px" class="dotted align"> 70  </td>
				<td style ="width: 297px !important;" class="align_right content_size"> Admission  No.</td> 
				<td style="width: 80px" class="dotted align"> 70  </td>
			</tr>
	</table>
	<table>
		<tr>
			<td style="width: 25px;"> </td>
			<td  style ="width: 573px !important;" class="content_size">1) &nbsp;&nbsp; Name of the Student</td>
			<td style="width: 368px" class="text align_left"> - &nbsp;&nbsp;  Fayeza Qaisar</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">2) &nbsp;&nbsp; Father's/Guardian's Name</td>
			<td class="text align_left"> - &nbsp;&nbsp;  Qaisar Ansari</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">3) &nbsp;&nbsp; Nationality</td>
			<td class="text align_left"> - &nbsp;&nbsp;  Indian</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">4) &nbsp;&nbsp; Whether the candidate belongs to Schedule Caste or Schedule Tribe</td>
			<td class="text align_left"> - &nbsp;&nbsp;  - OC -</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">5) &nbsp;&nbsp; Date of admission in the School with class</td>
			<td class="text align_left"> - &nbsp;&nbsp;  04-06-2015</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">6) &nbsp;&nbsp; Date of Birth (in Chrisitan Era) according to Admission Register</td>
			<td class="text align_left"> - &nbsp;&nbsp;  03-01-2004</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">7) &nbsp;&nbsp; Class in which the student last studied</td>
			<td class="text align_left"> - &nbsp;&nbsp;  Std  VI</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">8) &nbsp;&nbsp; School/Board Annual examination last taken with result</td>
			<td class="text align_left"> - &nbsp;&nbsp;  Passed</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">9) &nbsp;&nbsp; Whether failed, if so once / twice in the same class </td>
			<td class="text align_left"> - &nbsp;&nbsp;  - </td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">10) &nbsp;&nbsp; Subjects</td>
			<td class="text align_left"> - &nbsp;&nbsp;  English, Maths, Science, S. Sc</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">11) &nbsp;&nbsp; Languages</td>
			<td class="text align_left"> - &nbsp;&nbsp;  Hindi</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">12) &nbsp;&nbsp; Whether qualified for promotion to the higher class if so, to which class</td>
			<td class="text align_left"> - &nbsp;&nbsp;  - </td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">13) &nbsp;&nbsp; Any fee concession availed of : if so, the nature of such concession</td>
			<td class="text align_left"> - &nbsp;&nbsp;  - </td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">14) &nbsp;&nbsp; Total No. of working days</td>
			<td class="text align_left"> - &nbsp;&nbsp;  18</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">15) &nbsp;&nbsp; Total No. of working days present</td>
			<td class="text align_left"> - &nbsp;&nbsp;  11</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">16) &nbsp;&nbsp; Games played or extra curricular activities in which the student usually took part</td>
			<td class="text align_left"> - &nbsp;&nbsp;  Basket Ball</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">17) &nbsp;&nbsp; General Conduct</td>
			<td class="text align_left"> - &nbsp;&nbsp;  Good</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">18) &nbsp;&nbsp; Date of application for transfer certificate</td>
			<td class="text align_left"> - &nbsp;&nbsp;  28-04-2016</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">19) &nbsp;&nbsp; Date of issue of transfer certificate</td>
			<td class="text align_left"> - &nbsp;&nbsp;  04-05-2016</td>
		</tr>
		<tr>
			<td> </td>
			<td class="content_size">20) &nbsp;&nbsp; Reason for leaving the school</td>
			<td class="text align_left"> - &nbsp;&nbsp;  Transfer</td>
		</tr>
		<tr>
			<td> </td>
			<td class=" content_size">21) &nbsp;&nbsp; Any other remark</td>
			<td class="text align_left"> - &nbsp;&nbsp;  - </td>
		</tr>
	</table>
	<br>
	<table>
		<tr>
			<td style="width: 115px" class="align_right content_size sign">Signature of Class Teacher</td>
			<td style="width: 712px" class="align content_size sign">Checked by<br> State Full Name & Designation </td>
			<td style="width: 145px" class="align content_size sign">Principal<br>Seal</td>
		</tr>
	</table>
	<!-- <table>
		<tr>
			<td style="width: 145px" class="align_right content_size sign">Date: 24-10-2018</td>
			<td style="width: 712px" class="align content_size sign"> </td>
			<td style="width: 145px" class="align content_size sign">Signature</td>
		</tr>
	</table> -->
	</div>
<!-- </div> -->
</body>
</html>

<script type="text/javascript">
	window.print();
</script>