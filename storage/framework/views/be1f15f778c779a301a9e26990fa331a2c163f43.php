﻿
<?php $__env->startSection('content'); ?>
<style type="text/css">
    .table-responsive {
        overflow-x: visible !important;
    }
</style>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>View Track Vehicle</h2>
            </div>
             <div class="col-lg-7 col-md-6 col-sm-12 line">
                 
                <ul class="breadcrumb float-md-right ">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Transport<!-- <?php echo trans('language.staff'); ?> --></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Vehicle</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/transport/track-vehicle/view-track-vehicle'); ?>">View Track Vehicle</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">
                                <?php echo Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']); ?>

                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                <?php echo Form::text('vehicle_no', old('vehicle_no', ''), ['class' => 'form-control ','placeholder'=>'Vehicle Number', 'id' => 'vehicle_no']); ?>

                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                <?php echo Form::text('driver_name', old('driver_name', ''), ['class' => 'form-control ','placeholder'=>'Driver Name', 'id' => 'driver_name']); ?>

                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                <?php echo Form::text('contact_number', old('contact_number', ''), ['class' => 'form-control ','placeholder'=>'Contact Number', 'id' => 'contact_number']); ?>

                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 ">
                                          <button type="submit" class="btn btn-raised btn-primary"  title="Search">Search
                                          </button>
                                        </div>
                                         <div class="col-lg-1 ">
                                          <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                                          </button>
                                        </div>
                                    </div>
                                <?php echo Form::close(); ?>

                                <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Vehicle No</th>
                                                <th>Driver Name</th>
                                                <th>Contact number</th>
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>AP-02-BK-1084</td>
                                                <td>Ajay pal</td>
                                                <td>91+ 654986465</td>
                                                <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a href="#"class="" data-toggle="modal" data-target="#largeModal">View Track</a></li> 
                                                </ul> </div></td> 
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>AP-02-BK-1084</td>
                                                <td>Ajay pal</td>
                                                <td>91+ 654986465</td>
                                                <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a href="#"class="" data-toggle="modal" data-target="#largeModal">View Track</a></li> 
                                                </ul> </div></td>
                                            </tr> 
                                            <tr>
                                                <td>3</td>
                                                <td>AP-02-BK-1084</td>
                                                <td>Ajay pal</td>
                                                <td>91+ 654986465</td>
                                                <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a href="#"class="" data-toggle="modal" data-target="#largeModal">View Track</a></li> 
                                                </ul> </div></td>
                                            </tr> 
                                            <tr>
                                                <td>4</td>
                                                <td>AP-02-BK-1084</td>
                                                <td>Ajay pal</td>
                                                <td>91+ 654986465</td>
                                                <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a href="#"class="" data-toggle="modal" data-target="#largeModal">View Track</a></li> 
                                                </ul> </div></td>
                                            </tr>                                             
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
 
<!-- model data -->

<div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="header header_model_2">
                <div class="header_model_1">
                    <h6><strong>Track</strong></h6>
                </div>
                <ul class="header-dropdown">
                    <li class="remove remove_cross_x_2">
                        <a role="button" class="btn button_1" data-dismiss="modal"><i class="zmdi zmdi-close zmdi_close1"></i></a>
                    </li>
                </ul>
            </div>
            <div class="tab-pane active" id="classlist">
                <div class="card">
                    <div class="body">
                        <div class="">
                           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d228974.9965040397!2d72.89045762709743!3d26.270284781485415!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39418c4eaa06ccb9%3A0x8114ea5b0ae1abb8!2sJodhpur%2C+Rajasthan!5e0!3m2!1sen!2sin!4v1531227746026" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default btn-round waves-effect">SAVE CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div> -->

        </div>
    </div>
</div>

<style type="text/css">

#DataTables_Table_0_filter{
    float: right !important;
}
#DataTables_Table_0_paginate{
    float: right !important;
}
.table tr .v1{
       white-space: normal;
}

</style>

<!-- status fuction -->

<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {

    document.getElementById("myDropdown").classList.toggle("show");
}
function myFunction2() {

    document.getElementById("myDropdown2").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            block_name: {
                validators: {
                        stringLength: {
                        min: 4,
                    },
                        notEmpty: {
                        message: 'Please fill required block name'
                    }
                }
            },
            hostel_name: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required hostel name'
                    }
                }
            },
            floor_no: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required floor no'
                    }
                }
            },
            room_no: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required room no'
                    }
                }
            },
            room_alias: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required room alias'
                    }
                }
            },
            capacity: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required capacity'
                    }
                }
            },
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>