@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Journal Entries</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/account') !!}">Accounts</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/account/manage-journal-entries') !!}">Journal Entries</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">

            <div class="card" style="margin-bottom: 20px;">
              <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div>
              <div class="body" style="padding: 0px 20px;">
               
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Add Journal Entries :-</div> -->
                  <form class="" action="" id="manage_vendor" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Date</lable>
                          <input type="text" name="date" id="date" class="form-control" placeholder="Date">
                        </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Voucher No</lable>
                          <input type="text" name="voucher_no" id="" class="form-control" placeholder="Voucher No">
                        </div>
                         @if ($errors->has('voucher_no')) <p class="help-block">{{ $errors->first('voucher_no') }}</p> @endif
                      </div>
                      <div class="col-lg-4"></div>
                      <div class="col-lg-2 text-right">
                        <a href="#" class="btn btn-raised btn-primary saveBtn add_journal_entries"><i class="fas fa-plus-circle"></i> Add</a>
                      </div>

                    </div>
                      
                  </form>
                  
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <!-- <hr> -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="dummy" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th class="text-center">Particulars</th>
                        <th class="text-center">Debit</th>
                        <th class="text-center">Credit</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td><div class="row">
                          <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Main Heads</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Head</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Head- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div></div>
                         <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Sub Heads</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Subhead</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Subhead- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div></div>
                         <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Group</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Group</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Group- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div></div>
                         <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Head</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Head</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Head- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div></div>
                       </div></td>
                      <td><div class="form-group"><input type="text" name="" class="form-control  saveBtn debit" rel='' placeholder="Debit" id="debit"></div></td>
                      <td><div class="form-group"><input type="text" name="" class="form-control saveBtn credit" placeholder="Credit" id="credit" rel=''></div></td>
                        <!-- <td style="width: 105px">
                          <button class="btn btn-success btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="fas fa-plus-circle"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td> -->
                      </tr>
                  
                      <!-- <tr>
                        <td colspan="1"></td>
                        <td>
                          <div class="row">
                            <div class="col-lg-12">
                            <div class="form-group">
                              <lable class="from_one1">Narration</lable>
                              <input type="text" name="navation" class="form-control" placeholder="Navation">
                            </div>
                            </div>
                          </div>
                        </td>
                        <td colspan="3"></td>
                      </tr>
                      <tr>
                        <td colspan="1"></td>
                        <td colspan="1" class="text-right">Total</td>
                        <td colspan="1"></td>

                      </tr> -->
                    </tbody>
                  </table>
                  <hr>
                  <table style="width: 100%">
                    <tr>
                      <td style="width: 30px"></td>
                      <td style="width: 720px">
                        <div class="col-lg-12 padding-0">
                          <div class="form-group">
                            <lable class="from_one1">Narration</lable><input type="text" name="navation" class="form-control" placeholder="Navation">
                          </div>
                        </div>
                      </td>

                    </tr>
                    <tr>
                      <td colspan="1"></td>
                      <td class="text-right">Total</td>
                      <td class="text-center">0.00 </td>
                      <td class="text-center">0.00 </td>
                      <td colspan="1"></td>
                    </tr>
                  </table>
                </div>
                <hr>
                <div class="row">
                  <div class="col-lg-1">
                    <button type="submit" class="btn btn-raised btn-primary" title= "Save">Save</button>
                  </div>
                  <div class="col-lg-1">
                    <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->


@endsection