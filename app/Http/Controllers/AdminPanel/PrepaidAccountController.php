<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrepaidAccountController extends Controller
{
    /**
     *  View page for Prepaid Account
     *  @Khushbu on 26 Sept 2018
    **/
    public function index()
    {
        $loginInfo 		    = get_loggedin_user_data();
        
        $data 				= array(
            'page_title'    => trans('language.view_prepaid_account'),
            'redirect_url'  => url('admin-panel/fees-collection/prepaid-account/view-prepaid-account'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.prepaid-account.prepaid-account')->with($data);
    }

    /**
     *  Add page for Prepaid Account
     *  @Khushbu on 26 Sept 2018   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        // $job        = [];
        if (!empty($id))
        {
            $page_title             = trans('language.edit_prepaid_account');
            $save_url               = url('admin-panel/fees-collection/prepaid-account/save/' . $decrypted_job_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_prepaid_account');
            $save_url      = url('admin-panel/fees-collection/prepaid-account/save');
            $submit_button = 'Save';
        }

        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/fees-collection/prepaid-account/view-prepaid-account'),
        );
        return view('admin-panel.prepaid-account.prepaid-account')->with($data);
    }
    /**
     *  View page for Manage Amount
     *  @Khushbu on 02 Oct 2018   
    **/ 
    public function  manageAmount() {
         $loginInfo         = get_loggedin_user_data();
        
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.prepaid-account.manage-amount')->with($data);
    }

     /**
     *  View page for Common Entry
     *  @Khushbu on 03 Oct 2018   
    **/ 
    public function commonEntry() {
        $loginInfo          = get_loggedin_user_data();
        
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.prepaid-account.common-entry')->with($data);
    }
}
