<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OneWayTransportController extends Controller
{
    public function request() {

    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.one-way-transport.request')->with($data);
    }

    public function response() {

    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.one-way-transport.response')->with($data);
    }
}
