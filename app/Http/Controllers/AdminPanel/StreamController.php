<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Stream\Stream; // Model
use Yajra\Datatables\Datatables;

class StreamController extends Controller
{
    /**
     *  View page for Stream
     *  @Pratyush on 11 Aug 2018
    **/
    public function index()
    {
        $stream = [];
        $loginInfo      = get_loggedin_user_data();
        $arr_medium     = \Config::get('custom.medium_type');
        $stream['arr_medium']   = add_blank_option($arr_medium, 'Select Medium');
        $data = array(
            'page_title'    => trans('language.view_stream'),
            'redirect_url'  => url('admin-panel/stream/view-streams'),
            'login_info'    => $loginInfo,
            'stream'        => $stream
        );
        return view('admin-panel.stream.index')->with($data);
    }

    /**
     *  Add page for Stream
     *  @Pratyush on 11 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $stream			= [];
        $loginInfo 		= get_loggedin_user_data();
        $arr_medium     = \Config::get('custom.medium_type');
        if (!empty($id))
        {
            $decrypted_stream_id 	= get_decrypted_value($id, true);
            $stream      			= Stream::Find($decrypted_stream_id);
            
            if (!$stream)
            {
                return redirect('admin-panel/stream/add-stream')->withError('Stream not found!');
            }
            $page_title             	= trans('language.edit_stream');
            $encrypted_stream_id   		= get_encrypted_value($stream->stream_id, true);
            $save_url               	= url('admin-panel/stream/save/' . $encrypted_stream_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_stream');
            $save_url      = url('admin-panel/stream/save');
            $submit_button = 'Save';
        }

        $stream['arr_medium']   = add_blank_option($arr_medium, 'Select Medium');

        $data = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'stream' 			=> $stream,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/stream/view-streams'),
        );
        return view('admin-panel.stream.add')->with($data);
    }

    /**
     *  Add and update Stream's data
     *  @Pratyush on 11 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_stream_id		= get_decrypted_value($id, true);
        if (!empty($id))
        {
            $stream = Stream::find($decrypted_stream_id);
            if (!$stream)
            {
                return redirect('/admin-panel/stream/add-stream/')->withError('Stream not found!');
            }
            $success_msg = 'Stream updated successfully!';
        }
        else
        {
            $stream     	= New Stream;
            $success_msg 	= 'Stream saved successfully!';
            $stream->medium_type = Input::get('medium_type');
        }

        $validatior = Validator::make($request->all(), [
            'stream_name'   => 'required|unique:streams,stream_name,' . $decrypted_stream_id . ',stream_id,medium_type,'.$stream->medium_type,
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $stream->admin_id       = $loginInfo['admin_id'];
                $stream->update_by      = $loginInfo['admin_id'];
                $stream->stream_name 	= Input::get('stream_name');
                $stream->medium_type 	= Input::get('medium_type');
                $stream->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();

                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/stream/view-streams')->withSuccess($success_msg);
    }


    /**
     *  Get Stream's Data for view page(Datatables)
     *  @Pratyush on 11 Aug 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $stream  			= Stream::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('stream_name')))
            {
                $query->where('stream_name', "like", "%{$request->get('stream_name')}%");
            }
            if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null)
            {
                $query->where('medium_type', "=", $request->get('medium_type'));
            }
        })->get();
        // p($stream);
        return Datatables::of($stream)
        ->addColumn('medium_type', function ($stream)
        {
            $arr_medium   = \Config::get('custom.medium_type');
            $medium_type  = $arr_medium[$stream->medium_type];
            return $medium_type;
        })
        ->addColumn('action', function ($stream)
        {
            $encrypted_stream_id = get_encrypted_value($stream->stream_id, true);
            if($stream->stream_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="stream-status/'.$status.'/' . $encrypted_stream_id . '">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-stream/' . $encrypted_stream_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-stream/' . $encrypted_stream_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Stream's data
     *  @Pratyush on 11 Aug 2018.
    **/
    public function destroy($id)
    {
        $stream_id 		= get_decrypted_value($id, true);
        $stream 		= Stream::find($stream_id);
        if ($stream)
        {
            $stream->delete();
            $success_msg = "Stream deleted successfully!";
            return redirect('admin-panel/stream/view-streams')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Stream not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Stream's status
     *  @Pratyush on 11 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $stream_id 		= get_decrypted_value($id, true);
        $stream 		= Stream::find($stream_id);
        if ($stream)
        {
            $stream->stream_status  = $status;
            $stream->save();
            $success_msg = "Stream status updated!";
            return redirect('admin-panel/stream/view-streams')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Stream not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    
    /**
     *  Get stream data according medium
     *  @Shree on 7 Sept 2018
    **/
    public function getStreamData()
    {
        $medium_type = Input::get('medium_type');
        $stream = get_all_streams($medium_type);
        $data = view('admin-panel.stream.ajax-stream-select',compact('stream'))->render();
        return response()->json(['options'=>$data]);
    }
}
