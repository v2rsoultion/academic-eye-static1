@extends('admin-panel.layout.header')
@section('content')
{!! Html::script('public/admin/assets/js/jquery.modal.min.js') !!}
{!! Html::style('public/admin/assets/css/jquery.modal.min.css') !!}
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_bbook') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/issue-book') !!}">{!! trans('language.book_issued') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_bbook') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">{!! trans('language.title') !!}</a></li>
                                    <li class="nav-item"><a class="nav-link"  href="{{ url('admin-panel/title/add-title') }}">{!! trans('language.add_title') !!}</a></li>
                                </ul>                        
                            </div> -->
                            <div class="body">
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.bbook_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        {!! Form::hidden('member_id',$member_id) !!}
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('publisher', old('publisher', ''), ['class' => 'form-control ','placeholder'=>trans('language.bbook_publisher'), 'id' => 'publisher']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('subtitle', old('subtitle', ''), ['class' => 'form-control ','placeholder'=>trans('language.bbook_subtitle'), 'id' => 'subtitle']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}                                

                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="book-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{trans('language.bbook_isbn_no')}}</th>
                                            <th>{{trans('language.bbook_name')}}</th>
                                            <th>{{trans('language.bbook_category')}}</th>
                                            <th>{{trans('language.bbook_author')}}</th>
                                            <th>{{trans('language.book_issued_available_copies')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<!-- Model code start here -->

<p><a href="#issue-book" id="openLink" rel="modal:open"></a></p>
<!-- Modal HTML embedded directly into document -->
<div id="issue-book" class="modal">    
  <div class="container">
  <p><strong>Select Date</strong></p>
    {!! Form::open(['files'=>TRUE,'id' => 'book-issue-date-form' , 'class'=>'form-horizontal','url' => $save_url]) !!}
    <div id='displaymsg'></div>
    <div class="row">
        <div class="col-lg-5 col-md-5">
        <lable class="from_one1">{!! trans('language.book_issued_from') !!} :</lable>
        <div class="form-group">
            {!! Form::text('issue_from','',['class' => 'form-control','placeholder'=>trans('language.book_issued_from'), 'id' => 'issue_from']) !!}
        </div>
        @if ($errors->has('issue_from')) <p class="help-block">{{ $errors->first('issue_from') }}</p> @endif
        </div>

        <div class="col-lg-5 col-md-5">
            <lable class="from_one1">{!! trans('language.book_issued_to') !!} :</lable>
            <div class="form-group">
                {!! Form::text('issue_to','',['class' => 'form-control ','placeholder'=>trans('language.book_issued_to'), 'id' => 'issue_to']) !!}
            </div>
            @if ($errors->has('issue_to')) <p class="help-block">{{ $errors->first('issue_to') }}</p> @endif
        </div>
        {!! Form::hidden('book_id','',['id' => 'book_id']) !!}
        {!! Form::hidden('member_id','',['id' => 'member_id']) !!}
        <div class="col-sm-2 book-issue-button">
            {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        </div>

      </div>
      {!! Form::close() !!}      
    </div>
    <a href="#" id="closemypopup" rel="modal:close"></a>
</div>

<script>
    $(document).ready(function () {
        var table = $('#book-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bFilter: false,
            // ajax: "{{url('admin-panel/book/data')}}",

            ajax: {
                url: "{{url('admin-panel/issue-book-data/books-data')}}",
                data: function (d) {
                    d.book_name = $('input[name=name]').val();
                    d.publisher = $('input[name=publisher]').val();
                    d.subtitle  = $('input[name=subtitle]').val();
                    d.member_id = $('input[name=member_id]').val();
                }
            },
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'book_isbn_no', name: 'book_isbn_no'},
                {data: 'book_name', name: 'book_name'},
                {data: 'category', name: 'category'},
                {data: 'author_name', name: 'author_name'},
                {data: 'available_copies', name: 'available_copies'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 6, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })

        $(document).on("click","#student_pop_id", function (e) {
            book_id     = $(this).attr("book-id");
            member_id   = $(this).attr("member-id");
            $("#book_id").val(book_id);
            $("#member_id").val(member_id);
            $('#openLink').trigger('click');
        })

        // $(document).on("click","a.close-modal", function (e) {
        //     window.location.reload(true);
        // })


        jQuery.validator.addMethod("greaterThan", 
        function(value, element, params) {

            if (!/Invalid|NaN/.test(value)) {
                return value >= $('#issue_from').val();
            }

            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) > Number($(params).val())); 
        },'End date must be greater than start date.');

        $("#book-issue-date-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                issue_from: {
                    required: true,  
                },
                issue_to: {
                    required: true,  
                    greaterThan: $('#issue_from').val(),
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        $('#issue_to').bootstrapMaterialDatePicker({ weekStart : 0,time: false });
        $('#issue_from').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false}).on('change', function(e, date)
        {
        $('#issue_to').bootstrapMaterialDatePicker('setMinDate', date);
        });

    });


</script>
@endsection




