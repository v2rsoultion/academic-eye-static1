<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'facilities';
    protected $primaryKey = 'facility_id';
    public function up()
    {
        if (!Schema::hasTable('facilities')) { 
            Schema::create('facilities', function (Blueprint $table) {
                $table->increments('facility_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('facility_name', 255)->nullable();
                $table->tinyInteger('facility_fees_applied')->default(1)->comment = '0=not apply,1=apply';
                $table->decimal('facility_fees',18,2)->default(0.00);
                $table->tinyInteger('facility_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('facilities', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('facilities', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facilities');
    }
}
