<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'schools';
    protected $primaryKey = 'school_id';
    public function up()
    {
        if (!Schema::hasTable('schools')) {
            Schema::create('schools', function (Blueprint $table) {
                $table->increments('school_id')->unsigned();
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->string('school_name', 255);
                $table->string('school_registration_no', 255)->nullable();
                $table->text('school_sno_numbers')->comment = 'SNO numbers are stored with comma';
                $table->text('school_description')->nullable();
                $table->text('school_address')->nullable();
                $table->string('school_district', 50)->nullable();
                $table->string('school_city', 50)->nullable();
                $table->string('school_state', 50)->nullable();
                $table->string('school_pincode', 10)->nullable();
                $table->string('school_board_of_exams',255)->nullable();
                $table->string('school_medium',50)->nullable();
                $table->integer('school_class_from')->unsigned()->default(0);
                $table->integer('school_class_to')->unsigned()->default(0);
                $table->integer('school_total_students')->unsigned()->default(0);
                $table->integer('school_total_staff')->unsigned()->default(0);
                $table->double('school_fee_range_from',18, 2)->unsigned()->default(0);
                $table->double('school_fee_range_to',18, 2)->unsigned()->default(0);
                $table->text('school_facilities')->comment = 'Facilities are stored with comma';
                $table->text('school_url')->nullable();
                $table->text('school_logo')->nullable();
                $table->string('school_email', 255);
                $table->string('school_mobile_number', 20)->nullable();
                $table->string('school_telephone', 20)->nullable();
                $table->string('school_fax_number', 20)->nullable();
                $table->tinyInteger('school_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('schools', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('schools', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
