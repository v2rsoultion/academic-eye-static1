<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubstituteReportController extends Controller
{
    public function absentTeacherReport() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.substitute-management-report.absent_teacher')->with($data);
    }

    public function scheduleTeacherReport() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.substitute-management-report.view_schedule_teacher')->with($data);
    }

    public function substituteTeacherReport() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.substitute-management-report.view_substitude_teacher')->with($data);
    }

    public function formToDateReport() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.substitute-management-report.fromtodate')->with($data);
    }

    public function payExtraReport() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.substitute-management-report.pay_extra')->with($data);
    }

}
