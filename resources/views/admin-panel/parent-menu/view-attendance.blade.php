@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>View Attendance</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
               
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/parent/view-profile') !!}">My Profile</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/parent/children-details') !!}">Attendance</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/staff-menu/my-class/view-student-attendance') !!}">View Attendance</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           <div class="body form-gap ">
                               <form>
                                    <div class="row clearfix">
                                      
                                      <div class="col-lg-3">
                                        <div class="form-group">
                                          <!-- <lable class="from_one1" for="name">Date</lable> -->
                                          <input type="text" name="name" id="date" class="form-control" placeholder="Date">
                                        </div>
                                      </div>
                                        <div class="col-lg-1 col-md-1">
                                          <button type="submit" class="btn btn-raised btn-primary" title="Search">Search
                                          </button>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <button type="submit" class="btn btn-raised btn-primary" title="Clear">Clear
                                          </button>
                                        </div>
                                    </div>
                                </form>
                                 <div style="width:100%;border: 1px solid #ccc;border-radius: 5px;font-size:13px;padding: 5px 10px;margin-left: 0px;margin-bottom: 10px;">
                                  <div><b>Roll No:</b> 101</div>
                                  <div><b>Enroll No:</b> 1001A1</div>
                                  <div><b>Name:</b> Ankit Dave</div>
                                  <div><b>Class:</b> Class-1 - A</div>
                                  </div>
                  
                                <div class="table-responsive">
                                   
                                <table class="table m-b-0 c_list" id="#" style="width:100%">
                                {{ csrf_field() }}
                                <thead>
                                    <tr>
                                        <th>{{trans('language.s_no')}}</th>
                                        <th>Date</th>
                                        <th>Attendance</th>
                                        <!-- <th>Action </th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                          <td>1</td>
                                          <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                               
                                        </tr>
                                          <tr>
                                          <td>2</td>
                                         <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                            
                                        </tr>
                                        <tr>
                                          <td>3</td>
                                          <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                        </tr>
                                        <tr>
                                          <td>4</td>
                                          <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                        </tr>
                                       <tr>
                                          <td>5</td>
                                         <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                        </tr>
                                          <tr>
                                          <td>6</td>
                                          <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                        </tr>
                                          <tr>
                                          <td>7</td>
                                          <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                        </tr>
                                          <tr>
                                          <td>8</td>
                                          <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                        </tr>
                                </tbody>
                            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#staff-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/staff/data')}}',
                data: function (d) {
                    d.class_id = $('input[name="name"]').val();
                    d.section_id = $('select[name="designation_id"]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'staff_name', name: 'staff_name'},
                {data: 'staff_designation_name', name: 'staff_designation_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

</script>


<div class="modal fade" id="attendanceModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ankit Dave </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <table class="table m-b-0 c_list">
                    <thead>
                      <tr>
                        <th class="text-center">S no</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Attendance</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">20 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                      </tr>
                     <tr>
                        <td class="text-center">2</td>
                        <td class="text-center">21 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: red;"></i> (Absent) </td>
                      </tr>
                      <tr>
                        <td class="text-center">3</td>
                        <td class="text-center">22 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                      </tr>
                      <tr>
                        <td class="text-center">4</td>
                        <td class="text-center">23 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                      </tr>
                      <tr>
                        <td class="text-center">5</td>
                        <td class="text-center">24 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                      </tr>
                      <tr>
                        <td class="text-center">6</td>
                        <td class="text-center">25 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: red;"></i> (Absent) </td>
                      </tr>
                      <tr>
                        <td class="text-center">7</td>
                        <td class="text-center">26 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: red;"></i> (Absent) </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>


@endsection