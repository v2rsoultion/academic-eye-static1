<?php $__env->startSection('content'); ?>

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2><?php echo trans('language.view_title'); ?></h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <a href="<?php echo url('admin-panel/title/add-title'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                   <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><?php echo trans('language.menu_configuration'); ?></li>
                    <li class="breadcrumb-item"><a href="#"><?php echo trans('language.title'); ?></a></li>
                    <li class="breadcrumb-item active"><a href=""><?php echo trans('language.view_title'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                <?php if(session()->has('success')): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?php echo e(session()->get('success')); ?>

                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif; ?>
                                <?php if($errors->any()): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?php echo e($errors->first()); ?>

                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif; ?>
                                <?php echo Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']); ?>

                                    <div class="row clearfix">
                                        <div class="col-md-3">
                                            <div class="input-group ">
                                                <?php echo Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.title_name'), 'id' => 'name']); ?>

                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <?php echo Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']); ?>

                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <?php echo Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]); ?>

                                        </div>
                                    </div>
                                <?php echo Form::close(); ?>

                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="designation-table" style="width:100%">
                                <?php echo e(csrf_field()); ?>

                                    <thead>
                                        <tr>
                                            <th><?php echo e(trans('language.s_no')); ?></th>
                                            <th><?php echo e(trans('language.title_name')); ?></th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#designation-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '<?php echo e(url('admin-panel/title/data')); ?>',
                data: function (d) {
                    d.name = $('input[name=name]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'title_name', name: 'title_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });


</script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>