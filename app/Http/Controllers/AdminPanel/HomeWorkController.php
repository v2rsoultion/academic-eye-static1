<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeWorkController extends Controller
{
    public function addHomework() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.homework.add-homework')->with($data);
    }

    public function viewHomework() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.homework.view-homework')->with($data);
    }
}
