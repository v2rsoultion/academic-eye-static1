<?php if(isset($designation['designation_id']) && !empty($designation['designation_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?>
<style type="text/css">
    .theme-blush .btn-primary {
    margin-top: 2px !important;
}
</style>
<?php echo Form::hidden('designation_id',old('designation_id',isset($designation['designation_id']) ? $designation['designation_id'] : ''),['class' => 'gui-input', 'id' => 'designation_id', 'readonly' => 'true']); ?>

<?php if($errors->any()): ?>
<div class="alert alert-danger" role="alert">
    <?php echo e($errors->first()); ?>

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1"><?php echo trans('language.designation_name'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('designation_name', old('designation_name',isset($designation['designation_name']) ? $designation['designation_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.designation_name'), 'id' => 'designation_name']); ?>

        </div>
        <?php if($errors->has('designation_name')): ?> <p class="help-block"><?php echo e($errors->first('designation_name')); ?></p> <?php endif; ?>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']); ?>

        <a href="<?php echo url('admin-panel/dashboard'); ?>" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

       jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#designation-form").validate({

            /* @validation  states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation  rules ------------------------------------------ */
            rules: {
                designation_name: {
                    required: true,
                    lettersonly:true
                }
            },
            /* @validation  highlighting + error placement  ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>