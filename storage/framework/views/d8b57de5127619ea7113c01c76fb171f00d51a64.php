<?php $__env->startSection('content'); ?>
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }

    input[type='radio']:after {
        width: 18px;
        height: 18px;
        border-radius: 100%;
        top: -2px;
        left: -3px;
        position: relative;
        border: 1px solid #d1d3d1 !important;
        background-color: #fff;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }

    .red input[type='radio']:checked:after {
        width: 18px;
        height: 18px;
        border-radius: 100%;
        top: -2px;
        left: -3px;
        position: relative;
        background-color: #e80b15;
        border: 1px solid #e80b15 !important;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
      }

      .green input[type='radio']:checked:after {
        width: 18px;
        height: 18px;
        border-radius: 15px;
        top: -2px;
        left: -3px;
        position: relative;
        background-color: #235409;
        border: 1px solid #235409 !important;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
      }
      .document_staff {
        padding-left: 12px;
        margin-right: 30px;
      }
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Add Leave Application</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <!-- <a href="<?php echo url('admin-panel/staff/staff-attendance/add-staff-attendance'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a> -->
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/parent/view-profile'); ?>">My Profile</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/parent/children-details'); ?>">Leave Management</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/parent/children-details/add-leave-application'); ?>">Add Leave Application</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div>
                            <div class="body form-gap ">
                              <div class="alert alert-success" role="alert" id="message" style="display: none;">
                                Attendance successfully Updated for today - 18 Oct 2018
                                <button style="margin-top: 10px" type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                                
                                <div class="row clearfix">
    
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1"><?php echo trans('language.stud_leave_app_date_to'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('student_leave_from_date', old('student_leave_from_date',isset($leave_application['student_leave_from_date']) ? $leave_application['student_leave_from_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.stud_leave_app_date_to'), 'id' => 'student_leave_from_date']); ?>

        </div>
        <?php if($errors->has('student_leave_from_date')): ?> <p class="help-block"><?php echo e($errors->first('student_leave_from_date')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-4 col-md-4">
        <lable class="from_one1"><?php echo trans('language.stud_leave_app_date_from'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('student_leave_to_date', old('student_leave_to_date',isset($leave_application['student_leave_to_date']) ? $leave_application['student_leave_to_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.stud_leave_app_date_from'), 'id' => 'student_leave_to_date'] ); ?>

        </div>
        <?php if($errors->has('student_leave_to_date')): ?> <p class="help-block"><?php echo e($errors->first('student_leave_to_date')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">Document File :</lable>
        <div class="form-group">
            <label for="file1" class="field file">
                <input type="file" class="gui-file" name="documents" id="student_document_file" >
                <input type="hidden" class="gui-input" id="student_document_file" placeholder="Upload Photo" readonly>
            </label>
        </div>
        <?php if($errors->has('documents')): ?> <p class="help-block"><?php echo e($errors->first('documents')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-12 col-md-12">
        <lable class="from_one1"><?php echo trans('language.stud_leave_app_reason'); ?> :</lable>
        <div class="form-group">
            <!-- <textarea rows="4" name="description" class="form-control no-resize" placeholder="Description"></textarea> -->
            <?php echo Form::textarea('student_leave_reason',old('student_leave_reason',isset($leave_application['student_leave_reason']) ? $leave_application['student_leave_reason']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.stud_leave_app_reason'),'rows' => 3, 'cols' => 50)); ?>

        </div>
        <?php if($errors->has('student_leave_reason')): ?> <p class="help-block"><?php echo e($errors->first('student_leave_reason')); ?></p> <?php endif; ?>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-primary','name'=>'save']); ?>

        <?php echo Form::button('Cancel', ['class' => 'btn btn-raised btn-primary']); ?>

    </div>
</div>
                            
                        
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>
<script type="text/javascript">
  function showMessage() {
    document.getElementById('message').style.display = "block";
  }
</script>

<script>
    $(document).ready(function () {
        var table = $('#staff-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '<?php echo e(url('admin-panel/staff/data')); ?>',
                data: function (d) {
                    d.class_id = $('input[name="name"]').val();
                    d.section_id = $('select[name="designation_id"]').val();
                }
            },
            //ajax: '<?php echo e(url('admin-panel/class/data')); ?>',
           
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'staff_name', name: 'staff_name'},
                {data: 'staff_designation_name', name: 'staff_designation_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

</script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>