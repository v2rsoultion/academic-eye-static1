<!DOCTYPE html>
<html>
<head>
	<title>TC Certificate</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">

	<style type="text/css">
	body {
		width: 720px;
		margin: auto;
		margin-top: 20px;
		font-size: 30px;
	}

	.content_size {
		font-size: 17px;
		font-style: italic;
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		border-radius: 10px;
		border: 5px solid #1f2f60;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 20px;
		font-style: italic;
	}
	.dotted {
		border-bottom: 2px dotted #000;
		font-size: 20px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 0px;
	}
	.rl {
		padding: 0px;
	}
	table{
		height: 40px;

	}
	</style>
</head>
<body>
	
	<div class="header">
	
  		<h4 class="align header_gap">APEX SENIOR SECONDARY SCHOOL</h4>
  		<h5 class="align" style="font-size:16px;margin-top:10px;">An English medium Co-education Sr. Secondary School</h5>
		<div class="align" style="margin: 15px 0px;">
		<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="60px"></div>
		<div class="align" style="width: 400px;margin:auto; font-size:25px;"><b>TRANSFER CERTIFICATE</b></div>

		<table style="font-weight: bold;">
			<tr>
				<td style ="width: 65px !important;" class="align_right content_size"> Book </td> 
				<td style="width: 80px" class="dotted align"> 2 </td>
				<td style ="width: 177px !important;" class="align_right content_size"> SI. No.</td> 
				<td style="width: 80px" class="dotted align"> 70  </td>
				<td style ="width: 177px !important;" class="align_right content_size"> Admission  No.</td> 
				<td style="width: 80px" class="dotted align"> 70  </td>
			</tr>
	</table>
		
	<table>
		<tr>
			<td  style ="width: 195px !important;" class="align_right content_size">1) &nbsp;&nbsp; Name of the Student</td>
			<td style="width: 166px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  Fayeza Qaisar</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 239px !important;" class="align_right content_size">2) &nbsp;&nbsp; Father's/Guardian's Name</td>
			<td style="width: 123px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  Qaisar Ansari</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 133px !important;" class="align_right content_size">3) &nbsp;&nbsp; Nationality</td>
			<td style="width: 230px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  Indian</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 284px !important;" class="align_right content_size">4) &nbsp;&nbsp; Whether the candidate belongs to Schedule Caste or Schedule Tribe</td>
			<td style="width: 78px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  - OC -</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 344px !important;" class="align_right content_size">5) &nbsp;&nbsp; Date of admission in the School with class</td>
			<td style="width: 18px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  04-06-2015</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 272px !important;" class="align_right content_size">6) &nbsp;&nbsp; Date of Birth (in Chrisitan Era) according to Admission Register</td>
			<td style="width: 89px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  03-01-2004</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 262px !important;" class="align_right content_size">7) &nbsp;&nbsp; Class in which the student last studied</td>
			<td style="width: 98px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  Std  VI</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 288px !important;" class="align_right content_size">8) &nbsp;&nbsp; School/Board Annual examination last taken with result</td>
			<td style="width: 72px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  Passed</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 293px !important;" class="align_right content_size">9) &nbsp;&nbsp; Whether failed, if so once / twice in the same class </td>
			<td style="width: 67px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  - </td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 118px !important;" class="align_right content_size">10) &nbsp;&nbsp; Subjects</td>
			<td style="width: 242px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  English, Maths, Science, S. Sc</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 135px !important;" class="align_right content_size">11) &nbsp;&nbsp; Languages</td>
			<td style="width: 225px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  Hindi</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 297px !important;" class="align_right content_size">12) &nbsp;&nbsp; Whether qualified for promotion to the higher class if so, to which class</td>
			<td style="width: 63px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  - </td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 282px !important;" class="align_right content_size">13) &nbsp;&nbsp; Any fee concession availed of : if so, the nature of such concession</td>
			<td style="width: 78px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  - </td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 233px !important;" class="align_right content_size">14) &nbsp;&nbsp; Total No. of working days</td>
			<td style="width: 127px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  18</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 286px !important;" class="align_right content_size">15) &nbsp;&nbsp; Total No. of working days present</td>
			<td style="width: 73px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  11</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 350px !important;" class="align_right content_size">16) &nbsp;&nbsp; Games played or extra curricular activities in which the student usually took part</td>
			<td style="width: 8px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  Basket Ball</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 173px !important;" class="align_right content_size">17) &nbsp;&nbsp; General Conduct</td>
			<td style="width: 185px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  Good</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 342px !important;" class="align_right content_size">18) &nbsp;&nbsp; Date of application for transfer certificate</td>
			<td style="width: 15px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  28-04-2016</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 291px !important;" class="align_right content_size">19) &nbsp;&nbsp; Date of issue of transfer certificate</td>
			<td style="width: 66px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  04-05-2016</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 257px !important;" class="align_right content_size">20) &nbsp;&nbsp; Reason for leaving the school</td>
			<td style="width: 100px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  Transfer</td>
		</tr>
	</table>
	<table>
		<tr>
			<td  style ="width: 175px !important;" class="align_right content_size">21) &nbsp;&nbsp; Any other remark</td>
			<td style="width: 182px;"> </td>
			<td style="width: 310px" class="text align_left"> - &nbsp;&nbsp;  - </td>
		</tr>
	</table>

	<br><br><br>
	<table>
		<tr>
			<td style="width: 145px" class="align_right content_size sign">Date: 24-10-2018</td>
			<td style="width: 420px" class="align content_size sign"> </td>
			<td style="width: 145px" class="align content_size sign">Signature</td>
		</tr>
	</table>
	</div>
<!-- </div> -->
</body>
</html>

<script type="text/javascript">
	window.print();
</script>