﻿
<?php $__env->startSection('content'); ?>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2> Hostel Block</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/hostel'); ?>"><?php echo trans('language.menu_hostel'); ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/hostel'); ?>">Configuration</a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/hostel/configuration/hostel-block'); ?>">Hostel Block</a></li>
                </ul>                
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div>
                    <div class="body form-gap">
                        <form class="" action="" method=""  id="">
                            <div class="row clearfix">
                                 <div class="col-lg-6 col-md-3 col-sm-12">
                                <lable class="from_one1">Hostel Name :</lable>                 
                                    <select class="form-control show-tick select_form1" name="hostel_name">
                                        <option value="">Hostel Name</option>
                                        <option value="VII">Hostel-1</option>
                                        <option value="X">Hostel-2</option>
                                        
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-3 col-sm-12">
                                    <lable class="from_one1">Block Name :</lable>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Block Name"  name="block_name">
                                    </div>
                                </div>
                               
                                
                            </div>
                             
                           <div id="" class="row seprateclass"  >
                                 <input type="hidden" id="hide" value="0" />
                                 <div class="col-lg-12 commclass">
                                 <h4>No. of Floor :- </h4>
                              </div>
                                <div class="col-lg-2 col-md-2 col-sm-12">
                                    <lable class="from_one1">Floor No :</lable>
                                    <div class="form-group">
                                        <input class="form-control positive-numeric-only" id="id-blah1" min="0" name="floor_no" type="number" value="" placeholder="Floor No">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12">
                                    <lable class="from_one1">No Of Room :</lable>
                                    <div class="form-group">
                                        <input class="form-control positive-numeric-only" id="id-blah1" min="0" name="no_of_room" type="number" value="" placeholder="No Of Room">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-5 col-sm-12 ">
                                    <lable class="from_one1">Description of Floor :</lable>
                                    <div class="form-group">
                                        <textarea rows="2" name="description" class="form-control no-resize" placeholder="Description of Floor"></textarea>
                                    </div>
                                </div>
                                 <div class="col-lg-2" style="padding:20px 0px;">
                                 <button type="button" class="btn btn-primary add_floor_button"><i class="fas fa-plus-circle"></i> Add</button>
                              </div>
                               <div class="input_floor_wrap">
                              </div>
                            </div>
                            <div class="row clearfix">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-raised btn-primary">Save</button>
                                    <button type="submit" class="btn btn-raised btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>
                        <hr>
                        <div class="headingcommon  col-lg-12" style="padding: 10px 0px;">Search By :-</div>
                          <div class="row ">
                                  <div class="col-lg-3 col-md-3 col-sm-12">
                                <lable class="from_one1">Hostel Name :</lable>                 
                                    <select class="form-control show-tick select_form1" name="">
                                        <option value="">Hostel Name</option>
                                        <option value="VII">Hostel-1</option>
                                        <option value="X">Hostel-2</option>
                                    
                                    </select>
                                </div>
                                  <div class="col-lg-3 col-md-3 col-sm-12">
                                       <lable class="from_one1">Block Name :</lable>         
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Block Name"  name="block_name">
                                    </div>
                                </div>
                                
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-raised btn-primary saveBtn">Search</button>
                                </div>
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-raised btn-primary cancelBtn">Clear</button>
                                </div>
                    </div>


               <!--      
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">
                                   -->
                                   <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Hostel Name</th>
                                                <th>Block Name</th>
                                                <th>No. of Floor</th>
                                               <!--  <th>Floor No</th>
                                                <th>Room No</th>
                                                <th>Description each floor</th> -->
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Hostel-1</td>
                                                <td>Block-1</td>
                                                <td> <button type="button" class="btn btn-raised btn-primary" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#floorDetailsModel" >View Details</button></td>
                                                <td>
                                                     <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Hostel-1</td>
                                                <td>Block-1</td>
                                                <td> <button type="button" class="btn btn-raised btn-primary" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#floorDetailsModel" >View Details</button></td>
                                                <td>
                                                     <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Hostel-1</td>
                                                <td>Block-1</td>
                                                <td> <button type="button" class="btn btn-raised btn-primary" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#floorDetailsModel" >View Details</button></td>
                                                <td>
                                                     <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Hostel-1</td>
                                                <td>Block-1</td>
                                                <td> <button type="button" class="btn btn-raised btn-primary" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#floorDetailsModel" >View Details</button></td>
                                                <td>
                                                     <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            <!-- </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
</section>
<?php $__env->stopSection(); ?>

<!-- Action Model  -->
<div class="modal fade" id="floorDetailsModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">No. of Floor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-12">
               <lable class="from_one1">Floor No :</lable>         
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Floor No"  name="floor_no">
            </div>
        </div>
         <div class="col-lg-3 col-md-3 col-sm-12">
               <lable class="from_one1">Room No :</lable>         
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Room No"  name="room_no">
            </div>
        </div>
        <div class="col-lg-2">
            <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 30px !important;">Search</button>
        </div>
        <div class="col-lg-2">
            <button class="btn btn-raised btn-primary" style="margin-top: 30px !important;">Clear</button>
        </div>
        </div>
        <div class="row tabless ">
          <table class="table  m-b-0 c_list pp">
            <thead>
              <tr>
                <th style="width: 60px;">S No</th>
                <th style="width: 140px;">Floor No</th>
                <th>Room No</th>
                <th style="width: 260px;">Description Each Floor</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $count= 1; for ($i=0; $i < 5; $i++) {  ?>
              <tr>
                <td><?php echo $count; ?></td>
                <td>Floor-1</td>
                <td>Room-<?php echo $count; ?></td>
                <td>Lorem Ipsum is simply dummy text of the printing...</td>
                <td><button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button></td>
              </tr>
              <?php $count++; } ?>
            </tbody>
          </table>
        </div>

      </div>
    </div>
  </div>
</div>

<style type="text/css">

.dataTables_filter{
    float: right !important;
}
#DataTables_Table_0_paginate{
    float: right;
}

</style>


<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            block_name: {
                validators: {
                        stringLength: {
                        min: 4,
                    },
                        notEmpty: {
                        message: 'Please fill required block name'
                    }
                }
            },
            hostel_name: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required hostel name'
                    }
                }
            },
            floor_no: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required floor no'
                    }
                }
            },
            no_of_room: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required no of room'
                    }
                }
            },
            description: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required description'
                    }
                }
            },
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>

<script type="text/javascript">

$(document).ready(function() {
  $("input.positive-numeric-only").on("keydown", function(e) {
    var char = e.originalEvent.key.replace(/[^0-9^.^,]/, "");
    if (char.length == 0 && !(e.originalEvent.ctrlKey || e.originalEvent.metaKey)) {
      e.preventDefault();
    }
  });

  $("input.positive-numeric-only").bind("paste", function(e) {
    var numbers = e.originalEvent.clipboardData
      .getData("text")
      .replace(/[^0-9^.^,]/g, "");
    e.preventDefault();
    var the_val = parseFloat(numbers);
    if (the_val > 0) {
      $(this).val(the_val.toFixed(2));
    }
  });

  $("input.positive-numeric-only").focusout(function(e) {
    if (!isNaN(this.value) && this.value.length != 0) {
      this.value = Math.abs(parseFloat(this.value)).toFixed(2);
    } else {
      this.value = 0;
    }
  });
});

</script>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>