@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_leave_application') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <a href="{!! url('admin-panel/staff/staff-leave/add-staff-leave') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/staff') !!}">{!! trans('language.menu_staff') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/staff') !!}">{!! trans('language.leave_application') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/staff/staff-leave/view-staff-leave') !!}">{!! trans('language.view_leave_application') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap ">
                               <form>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.staff_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        
                                        
                      <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-primary"  title="Search">Search
                      </button>
                    </div>
                     <div class="col-lg-1 ">
                      <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                      </button>
                    </div>
                                    </div>
                                </form>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.staff_name')}}</th>
                                                <th>{{trans('language.leave_form_date')}}</th>
                                                <th>{{trans('language.leave_to_date')}}</th>
                                                <th>Total Days</th>
                                                <th>{{trans('language.leave_reason')}}</th>
                                                <th>File url</th>
                                                <th>Status</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Rajash Kumar</td>
                                                <td>01-09-2018</td>
                                                <td>09-09-2018</td>
                                                <td>9 days</td>
                                                <td>Work at home</td>
                                                <td>File url</td>
                                                <td><span class="badge badge-success">Approved</span></td>
                                                <td>
                                                <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="#">Approved </a></li> 
                                                    <li> <a title="#">Disapproved </a></li> 
                                                </ul> </div>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div></td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Rajash Kumar</td>
                                                <td>01-09-2018</td>
                                                <td>09-09-2018</td>
                                                <td>9 days</td>
                                                <td>Work at home</td>
                                                <td>File url</td>
                                                <td><span class="badge badge-success">Approved</span></td>
                                                 <td>
                                                <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="#">Approved </a></li> 
                                                    <li> <a title="#">Disapproved </a></li> 
                                                </ul> </div>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div></td>
                                            </tr>
                                            <tr>
                                                <td>13</td>
                                                <td>Rajash Kumar</td>
                                                <td>01-09-2018</td>
                                                <td>09-09-2018</td>
                                                <td>9 days</td>
                                                <td>Work at home</td>
                                                <td>File url</td>
                                                <td><span class="badge badge-success">Approved</span></td>
                                                <td>
                                                <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="#">Approved </a></li> 
                                                    <li> <a title="#">Disapproved </a></li> 
                                                </ul> </div>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#staff-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/staff/data')}}',
                data: function (d) {
                    d.class_id = $('input[name="name"]').val();
                    d.section_id = $('select[name="designation_id"]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'staff_name', name: 'staff_name'},
                {data: 'staff_designation_name', name: 'staff_designation_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

</script>
@endsection




