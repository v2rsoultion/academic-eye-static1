@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  .nav_item7
  {
    min-height: 230px !important;
  }
  .imgclass:hover .active123 .tab_images123{
    left: 50px !important;
  }
  .imgclass:hover .tab_images123{
    left: 50px !important;
  }
  .nav_item3 {
    min-height: 255px !important;
  }
  .side-gap{
    width: 75% !important;
  }
</style>
<!--  Main content here -->

<section class="content">
      <div class="block-header">
        <div class="row">
          <div class="col-lg-5 col-md-6 col-sm-12">
            <h2>View Staff Attendance</h2>
          </div>
          <div class="col-lg-7 col-md-6 col-sm-12 line">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/staff') !!}">{!! trans('language.staff') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/staff') !!}">{!! trans('language.staff_attendance') !!}</a></li>
                     <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff/staff-attendance/view-staff-attendance') !!}">{!! trans('language.view_staff_attendance') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/staff/staff-attendance/view-profile') !!}">View</a></li>
            </ul>
          </div>
        </div>
      </div>

<div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           <div class="body form-gap ">
                            <div class="headingcommon  col-lg-12">Staff Attendance :-</div>  
                            <!-- <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;
    width: 1035px !important;">
                          <div class="tab-pane active" id="classlist">
                            <div class="card border_info_tabs_1 card_top_border1">
                              <div class="body">
                                <div class="nav_item5">
                                <div class="nav_item1">
                                  <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  Attendance Info </a>
                                </div>
                                <button class="btn btn-primary btn-sm btn-round waves-effect" id="change-view-today">today</button>
                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-day" >Day</button>
                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-week">Week</button>
                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-month">Month</button> -->
                            <div id="calendar" class="m-t-15"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

    </section>
<!-- Content end here  -->
@endsection