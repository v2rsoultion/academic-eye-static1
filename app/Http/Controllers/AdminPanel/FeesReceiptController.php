<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeesReceiptController extends Controller
{
    public function feesReceipt() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.fees-receipt.fee_receipt')->with($data);
    }
}
