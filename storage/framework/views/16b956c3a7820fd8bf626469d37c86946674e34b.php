
<?php $__env->startSection('content'); ?>
<style type="text/css">
  td{
    padding: 14px 10px !important;
  }
  .nav-tabs>.nav-item>.nav-link {
    width: 100px;
    margin-right: 5px;
        border: 1px solid #ccc !important;
  }
  #tabingclass .nav-tabs>.nav-item>.nav-link {
    border-radius: 4px !important;
    padding: 8px 25px;
  }
   #tabingclass .nav-link:hover {
        background: #6572b8 !important;
    color: #fff !important;
}
#tabingclass .nav-link:active {
        background: #6572b8 !important;
    color: #fff !important;
}
.idi .nav-tabs>.nav-item>.nav-link.active {
  background: #6572b8 !important;
    color: #fff !important;
}
.modal-dialog {
  max-width: 550px !important;
}
.checkbox {
  float: left;
  margin-right: 20px;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Remark</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <a href="<?php echo url('admin-panel/staff-menu/my-class/remark/add-remark'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/staff-menu/my-class'); ?>">My Class</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/staff-menu/my-class'); ?>">Remarks</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/staff-menu/my-class/remark/view-remark'); ?>">View Remark</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 20px !important;">
                 
                     <form class="" action="" method=""  id="" style="width: 100%;">
                      <div class="row">
                      
                     
                   <div class="col-lg-3">
                    <div class="form-group">
                      <!-- <lable class="from_one1" for="name">Class</lable> -->
                      <select class="form-control show-tick select_form1" name="classes" id="">
                        <option value="">Subject</option>
                        <option value="A">Subject-1</option>
                        <option value="B">Subject-2</option>
                        <option value="C">Subject-3</option>
                        <option value="D">Subject-4</option>
                      </select>
                    </div>
                  </div>
              
                      <div class="col-lg-3">
                        <div class="form-group">
                         <input type="text" name="date" id="taskDate" placeholder="Date" class="form-control">
                        </div>
                      </div>
                     <div class="col-lg-1">
                      <button type="submit" class="btn btn-raised btn-primary " title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Clear
                      </button>
                    </div>
                  
                    </div>
                   </form>
                    <!--  DataTable for view Records  -->
                     <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                      <thead>
                        <tr>
                          <th>S No</th>
                          <th>Date</th>
                          <th>Subject</th>
                          <th>Teacher</th>
                          <th>Remark Description</th>
                          
                          <!-- <th>Action</th> -->
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 10; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td>09-10-2018</td>
                          <td>Subject-<?php echo $counter; ?></td>
                          <td>Teacher-<?php echo $counter; ?></td>
                          <td style="width: 250px;">Lesson-1 Question Answer in written</td>
                           
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>

<!-- model -->
<div class="modal fade" id="sendMessageModel" tabindex="-1" role="dialog" style="padding: 40px;">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content send_messsage_button1">
            <div class="header header_model_2">
                <div class="header_model_1">
                    <h6 style="padding-left:20px;"><strong>Send Message</strong></h6>
                </div>
                <ul class="header-dropdown" style="padding-right: 20px; ">
                    <li class="remove remove_cross_x_2 float-right" style="position: relative; top: 20px;">
                        <a role="button" class="button_1" data-dismiss="modal"><i class="zmdi zmdi-close zmdi_close1"></i></a>
                    </li>
                </ul>
            
            </div>
          <div class="tab-pane active" id="classlist">
              <div class="card form-gap" style="padding: 20px 40px;">
                  <div class="body">
                        <form class="" action="" method=""  id="">
                            <div class="row clearfix">
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                    <lable class="from_one1">Subject :</lable>
                                    <div class="form-group">
                                       <input type="text" name="subject" class="form-control" placeholder="Subject">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <lable class="from_one1">Message :</lable>
                                    <div class="form-group">
                                       <textarea class="form-control" class="form-control" placeholder="Message"></textarea>
                                    </div>
                                </div>
                            </div>
                           <!--  <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <lable class="from_one1">Send Via :</lable>
                                    <div class="form-group">
                                        <div class="radio" style="margin:10px 4px !important;">
                                            <input type="radio" name="radio_option" id="radio1" value="option1">
                                            <label for="radio1" class="document_staff">Email</label>
                                            <input type="radio" name="radio_option" id="radio2" value="option2">
                                            <label for="radio2" class="document_staff">SMS</label>
                                            <input type="radio" name="radio_option" id="radio4" value="option4" checked="">
                                            <label for="radio4" class="document_staff">Both</label>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row clearfix">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-raised  btn-primary waves-effect float-right" data-dismiss="modal">Cancel</button>    
                                    <button type="submit" class="btn btn-raised  btn-primary float-right" id="send_msg" onclick="showMessage()">Send</button>
                                </div>
                            </div>
                        </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>