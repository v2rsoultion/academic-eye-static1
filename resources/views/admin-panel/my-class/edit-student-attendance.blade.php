@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }
    input[type='radio']:after {
        width: 18px;
        height: 18px;
        border-radius: 100%;
        top: -2px;
        left: -3px;
        position: relative;
        border: 1px solid #d1d3d1 !important;
        background-color: #fff;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }

    .red input[type='radio']:checked:after {
        width: 18px;
        height: 18px;
        border-radius: 100%;
        top: -2px;
        left: -3px;
        position: relative;
        background-color: #e80b15;
        border: 1px solid #e80b15 !important;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
      }

      .green input[type='radio']:checked:after {
        width: 18px;
        height: 18px;
        border-radius: 15px;
        top: -2px;
        left: -3px;
        position: relative;
        background-color: #235409;
        border: 1px solid #235409 !important;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
      }
      .document_staff {
        padding-left: 12px;
        margin-right: 30px;
      }
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Edit Student Attendance</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <!-- <a href="{!! url('admin-panel/staff/staff-attendance/add-staff-attendance') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a> -->
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/student') !!}">{!! trans('language.menu_student') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/student') !!}">Attendance</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/student/student-attendance/edit-student-attendance') !!}">Edit Student Attendance</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">{!! trans('language.student') !!}</a></li>
                                    <li class="nav-item"><a class="nav-link"  href="{{ url('/admin-panel/student/add-student') }}">{!! trans('language.add_student') !!}</a></li>
                                </ul>                        
                            </div> -->
                            <div class="alert alert-success" role="alert" id="message" style="display: none; margin-top: 20px">
                              Attendance successfully added for today - 18 Oct 2018
                              <button style="margin-top: 10px" type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                            <div class="body form-gap ">
                                <form>
                                    <div class="row clearfix">
                                      <div class="col-lg-2">
                                       <div class="form-group">
                                        <lable class="from_one1">Class</lable>
                                          <select class="form-control show-tick select_form1" name="" id="">
                                            <option value="">Select Class</option>
                                            <option value="1">Class-1</option>
                                            <option value="2">Class-2</option>
                                            <option value="3">Class-3</option>
                                            <option value="4">Class-4</option>
                                          </select>
                                       </div>
                                    </div>
                                     <div class="col-lg-2">
                                       <div class="form-group">
                                        <lable class="from_one1">Section</lable>
                                          <select class="form-control show-tick select_form1" name="" id="">
                                            <option value="">Select Section</option>
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                            <option value="D">D</option>
                                          </select>
                                       </div>
                                    </div> 
                                       <div class="col-lg-2">
                                        <div class="form-group">
                                          <lable class="from_one1" for="name">Date</lable>
                                          <input type="text" name="name" id="staff_date" class="form-control" placeholder="Date">
                                        </div>
                                      </div> 
                                
                                        <div class="col-lg-1 col-md-1">
                                             <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Serach">Search
                                         </button>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Cancel">Clear
                                        </button>
                                        </div>
                                    </div>
                               </form>
                                <div class="float-right">
                          <button type="submit" class="btn btn-raised btn-primary" style="margin-top: -68px !important;" title="Save" onclick="showMessage()">Update
                          </button>
                        </div>
                                <div class="table-responsive">
                            <table class="table m-b-0 c_list" id="#" style="width:100%">
                            {{ csrf_field() }}
                                <thead>
                                    <tr>
                                        <th>{{trans('language.s_no')}}</th>
                                        <th>Roll No</th>
                                        <th style="width: 250px">Student Name</th>
                                        <th>Date</th>
                                        <th>Attendance</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                          <td>1</td>
                                          <td>Staff101</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td>
                                              <div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status11" name="attendance_status1" checked="checked" type="radio" value="1" multiple="multiple">
                                                <label for="attendance_status11" class="document_staff">Present</label>
                                              </div>
                                              <div class="red float-left" style="margin-top:6px !important;">
                                                <input id="attendance_status12" name="attendance_status1" type="radio" value="0">
                                                <label for="attendance_status12" class="document_staff">Absent</label> 
                                              </div>
                                            </td>
                                        </tr>
                                          <tr>
                                          <td>2</td>
                                          <td>Staff102</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                             <td>
                                              <div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status21" name="attendance_status2" checked="checked" type="radio" value="1" multiple="multiple">
                                                <label for="attendance_status21" class="document_staff">Present</label>
                                              </div>
                                              <div class="red float-left" style="margin-top:6px !important;">
                                                <input id="attendance_status22" name="attendance_status2" type="radio" value="0">
                                                <label for="attendance_status22" class="document_staff">Absent</label> 
                                              </div>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td>3</td>
                                          <td>Staff103</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td>
                                              <div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status31" name="attendance_status3" checked="checked" type="radio" value="1" multiple="multiple">
                                                <label for="attendance_status31" class="document_staff">Present</label>
                                              </div>
                                              <div class="red float-left" style="margin-top:6px !important;">
                                                <input id="attendance_status32" name="attendance_status3" type="radio" value="0">
                                                <label for="attendance_status32" class="document_staff">Absent</label> 
                                              </div>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td>4</td>
                                          <td>Staff104</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td>
                                              <div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status41" name="attendance_status4" checked="checked" type="radio" value="1" multiple="multiple">
                                                <label for="attendance_status41" class="document_staff">Present</label>
                                              </div>
                                              <div class="red float-left" style="margin-top:6px !important;">
                                                <input id="attendance_status42" name="attendance_status4" type="radio" value="0">
                                                <label for="attendance_status42" class="document_staff">Absent</label> 
                                              </div>
                                            </td>
                                        </tr>
                                       <tr>
                                          <td>5</td>
                                          <td>Staff105</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td>
                                              <div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status51" name="attendance_status5" checked="checked" type="radio" value="1" multiple="multiple">
                                                <label for="attendance_status51" class="document_staff">Present</label>
                                              </div>
                                              <div class="red float-left" style="margin-top:6px !important;">
                                                <input id="attendance_status52" name="attendance_status5" type="radio" value="0">
                                                <label for="attendance_status52" class="document_staff">Absent</label> 
                                              </div>
                                            </td>
                                        </tr>
                                          <tr>
                                          <td>6</td>
                                          <td>Staff106</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td>
                                              <div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status61" name="attendance_status6" checked="checked" type="radio" value="1" multiple="multiple">
                                                <label for="attendance_status61" class="document_staff">Present</label>
                                              </div>
                                              <div class="red float-left" style="margin-top:6px !important;">
                                                <input id="attendance_status62" name="attendance_status6" type="radio" value="0">
                                                <label for="attendance_status62" class="document_staff">Absent</label> 
                                              </div>
                                            </td>
                                        </tr>
                                          <tr>
                                          <td>7</td>
                                          <td>Staff107</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td>
                                              <div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status71" name="attendance_status7" checked="checked" type="radio" value="1" multiple="multiple">
                                                <label for="attendance_status71" class="document_staff">Present</label>
                                              </div>
                                              <div class="red float-left" style="margin-top:6px !important;">
                                                <input id="attendance_status72" name="attendance_status7" type="radio" value="0">
                                                <label for="attendance_status72" class="document_staff">Absent</label> 
                                              </div>
                                            </td>
                                        </tr>
                                          <tr>
                                          <td>8</td>
                                          <td>Staff108</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                          <td>
                                              <div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status81" name="attendance_status8" checked="checked" type="radio" value="1" multiple="multiple">
                                                <label for="attendance_status81" class="document_staff">Present</label>
                                              </div>
                                              <div class="red float-left" style="margin-top:6px !important;">
                                                <input id="attendance_status82" name="attendance_status8" type="radio" value="0">
                                                <label for="attendance_status82" class="document_staff">Absent</label> 
                                              </div>
                                            </td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <div class="float-right">
                          <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Save" onclick="showMessage()">Update
                          </button>
                        </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>
<script type="text/javascript">
  function showMessage() {
    document.getElementById('message').style.display = "block";
  }
</script>
<script>
    $(document).ready(function () {
        var table = $('#staff-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/staff/data')}}',
                data: function (d) {
                    d.class_id = $('input[name="name"]').val();
                    d.section_id = $('select[name="designation_id"]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'staff_name', name: 'staff_name'},
                {data: 'staff_designation_name', name: 'staff_designation_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

</script>
@endsection




