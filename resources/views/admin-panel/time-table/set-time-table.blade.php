@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h2>{!! trans('language.set_time_table') !!}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.academic') !!}</a></li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.time_table') !!}</a></li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.view_time_table') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="#">{!! trans('language.set_time_table') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body">
                                @if(session()->has('success'))
                                    <p class="green">
                                        {{ session()->get('success') }}
                                    </p>
                                @endif
                                @if($errors->any())
                                    <p class="red">{{$errors->first()}}</p>
                                @endif
                            
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list table-bordered" id="time-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.monday')}}</th>
                                                <th>{{trans('language.tuesday')}}</th>
                                                <th>{{trans('language.wednesday')}}</th>
                                                <th>{{trans('language.thursday')}}</th>
                                                <th>{{trans('language.friday')}}</th>
                                                <th>{{trans('language.saturday')}}</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#time-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/time-table/view-time-table/data')}}'
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                
            ],
             columnDefs: [
                {
                    "targets": 6, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        // $('#search-form').on('submit', function(e) {
        //     table.draw();
        //     e.preventDefault();
        // });

        // $('#clearBtn').click(function(){
        //     location.reload();
        // })
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='class_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='class_id'").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }


</script>
@endsection




