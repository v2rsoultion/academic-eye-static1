
<?php $__env->startSection('content'); ?>
<style type="text/css">
  .checkbox label, .radio label{
        line-height: 19px;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Map Employees</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/payroll'); ?>">Payroll</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/payroll/manage-department'); ?>">Department</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/payroll/manage-department/view-map-employees'); ?>">Map Employees</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                 <div class="headingcommon  col-lg-12" style="margin-left: -13px">Map Employees :-</div>
                <!-- <form class="" action="" method="" id="" style="width: 100%;">
                  <div class="row">
                     <div class="col-lg-3">
                      <div class="form-group">
                        <lable class="from_one1" for="name">Department</lable>
                        <select class="form-control show-tick select_form1" name="classes" id="">
                          <option value="">Select Department</option>
                          <option value="1">Vijay Rathore</option>
                          <option value="2">Vijay Rathore</option>
                          <option value="3">Vijay Rathore</option>
                        </select>
                      </div>
                    </div>
                     <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1 ">
                      <button type="reset" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Clear">Clear
                      </button>
                    </div>
                    
                  </div>
                </form> -->
                   <!-- <hr> -->
                    <!--  DataTable for view Records  -->
                    <div class="">
                    <table class="table m-b-0 c_list" id="tablche" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Employee Name</th>
                       </tr>
                      </thead>
                      <tbody>
                        <?php $count = 0; for ($i=0; $i < 5 ; $i++) {  ?>
                        
                        <tr>
                          <td >
                            <div class="checkbox">
                               <input id="checkbox<?php echo  $count; ?>" type="checkbox">
                               <label class="from_one1" for="checkbox<?php echo  $count; ?>" style= "margin-bottom: 4px !important;"></label>
                            </div>
                          </td>
                          <td>
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="5%">
                           Ankit Dave
                        </td>
                          
                        </tr>
                       <?php  $count++; } ?>
                      </tbody>
                    </table>
                  </div>
                    <div class="col-lg-12">
                       <button type="button" class="btn btn-primary float-right" title="Save">Save</button>
                    </div>
                  </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>