@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 14px 10px !important;
  }
  .nav-tabs>.nav-item>.nav-link {
    width: 100px;
    margin-right: 5px;
        border: 1px solid #ccc !important;
  }
  #tabingclass .nav-tabs>.nav-item>.nav-link {
    border-radius: 4px !important;
    padding: 8px 25px;
  }
   #tabingclass .nav-link:hover {
        background: #6572b8 !important;
    color: #fff !important;
}
#tabingclass .nav-link:active {
        background: #6572b8 !important;
    color: #fff !important;
}
.idi .nav-tabs>.nav-item>.nav-link.active {
  background: #6572b8 !important;
    color: #fff !important;
}
.modal-dialog {
  max-width: 550px !important;
}
.checkbox {
  float: left;
  margin-right: 20px;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Homework</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <a href="{!! url('admin-panel/subject/subject-homework/add-homework') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/subject') !!}">My Subject</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/subject') !!}">Homework</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/subject/subject-homework/view-homework') !!}">View Homework</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 20px !important;">
                 
                     <form class="" action="" method=""  id="" style="width: 100%;">
                      <div class="row"> 
                   <div class="col-lg-3">
                    <div class="form-group">
                      <!-- <lable class="from_one1" for="name">Subject</lable> -->
                      <select class="form-control show-tick select_form1" name="classes" id="">
                        <option value="">Subject</option>
                        <option value="A">Subject-1</option>
                        <option value="B">Subject-2</option>
                        <option value="C">Subject-3</option>
                        <option value="D">Subject-4</option>
                      </select>
                    </div>
                  </div>
                  
                      <div class="col-lg-3">
                        <div class="form-group">
                         <input type="text" name="date" id="taskDate" placeholder="Date" class="form-control">
                        </div>
                      </div>
                     <div class="col-lg-1">
                      <button type="submit" class="btn btn-raised btn-primary " title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Clear
                      </button>
                    </div>
                  
                    </div>
                   </form>
                <hr style="width: 100%">
                    <!--  DataTable for view Records  -->
                     <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                      <thead>
                        <tr>
                          <th>S No</th>
                          <th>Subject</th>
                          <th>Date</th>
                          <th>HomeWork Description</th>
                          
                          <!-- <th>Action</th> -->
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 10; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td>Subject-<?php echo $counter; ?></td>
                          <td>09-10-2018</td>
                          <td style="width: 250px;">Lesson-1 Question Answer in written</td>
                           
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
@endsection
