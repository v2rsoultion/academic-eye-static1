
<?php $__env->startSection('content'); ?>
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Journal Entries</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/account'); ?>">Account</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/account'); ?>">Journal Entries</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/account/view-journal-entries'); ?>">View Journal Entries</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                
                  <div class="headingcommon  col-lg-12" style="margin-left: -13px">View Journal Entries :-</div>
                  <!-- <hr> -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="dummy" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Date</th>
                        <th>Particulars</th>
                        <th>Amount</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td> 01-11-2018</td>
                        <td><div>By Head-1</div>
                          <div class="text-center">To </div>
                          </td>
                        <td><div>500</div>
                          <div class="text-center">500</div></td>
                        <td>0.00</td>
                      
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>11-11-2018</td>
                        <td><div>By Head-1</div>
                          <div class="text-center">To Head-2</div>
                          </td>
                        <td><div>1500</div>
                          <div class="text-center">2500</div></td>
                        <td>1000.00</td>
                      
                      </tr>
                       <tr>
                        <td>3</td>
                        <td>12-11-2018</td>
                        <td><div>By Head-1</div>
                          <div class="text-center">To Head-2</div>
                          </td>
                        <td><div>1500</div>
                          <div class="text-center">2500</div></td>
                        <td>1000.00</td>
                      
                      </tr>
                    </tbody>
                  </table>
                 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>