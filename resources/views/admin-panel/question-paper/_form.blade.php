@if(isset($question_paper['question_paper_id']) && !empty($question_paper['question_paper_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif





<style> 

.state-error{
    color: red;
    font-size: 13px;
    margin-bottom: 10px;
}

</style>

{!! Form::hidden('question_paper',old('question_paper',isset($question_paper['question_paper_id']) ? $question_paper['question_paper_id'] : ''),['class' => 'gui-input', 'id' => 'question_paper_id', 'readonly' => 'true']) !!}

<!-- Basic Info Question Paper -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.question_paper_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('name','', ['class' => 'form-control','placeholder'=>trans('language.question_paper_name'), 'id' => 'qp_name']) !!}
        </div>
        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.question_paper_class_id') !!} :</lable>
        <label class=" field select" style="width: 100%">
            {!!Form::select('qp_class_id',$question_paper,'', ['class' => 'form-control show-tick select_form1','id'=>'qp_class_id','onChange' => 'getSection(this.value) || getSubject(this.value)'])!!}
            <i class="arrow double"></i>
        </label>
        @if ($errors->has('qp_class_id')) <p class="help-block">{{ $errors->first('qp_class_id') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.question_paper_section_id') !!}:</lable>
        <label class=" field select" style="width: 100%">
            {!!Form::select('qp_section_id', $question_paper,'', ['class' => 'form-control show-tick select_form1','id'=>'qp_section_id'])!!}
            <i class="arrow double"></i>
        </label>
        @if ($errors->has('qp_section_id')) <p class="help-block">{{ $errors->first('qp_section_id') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.question_paper_subject_id') !!}:</lable>
        <label class=" field select" style="width: 100%" >
            {!!Form::select('qp_subject_id', $question_paper,'', ['class' => 'form-control show-tick select_form1','id'=>'qp_subject_id'])!!}
            <i class="arrow double"></i>
        </label>
        @if ($errors->has('qp_subject_id')) <p class="help-block">{{ $errors->first('qp_subject_id') }}</p> @endif
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.question_paper_exam_type_id') !!} :</lable>
        <label class=" field select" style="width: 100%">
            {!!Form::select('qp_exam_type_id', $question_paper,'', ['class' => 'form-control show-tick select_form1','id'=>'qp_exam_type_id'])!!}
            <i class="arrow double"></i>
        </label>
        @if ($errors->has('qp_exam_type_id')) <p class="help-block">{{ $errors->first('qp_exam_type_id') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.question_paper_file_upload') !!} :</lable>
        <div class="form-group">
            <label for="file1" class="field file">
                <input type="file" class="gui-file" name="documents" id="question_paper_document" >
                <input type="hidden" class="gui-input" id="question_paper_document" placeholder="Upload Photo" readonly>
            </label>
        </div>
        @if ($errors->has('documents')) <p class="help-block">{{ $errors->first('documents') }}</p> @endif
    </div>
   
    
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-primary']) !!}</a>
    </div>
</div>

<style type="text/css">

.field file state-error{
    width: 100% !important;
}
.state-error{
    width: 100% !important;
}
.document_file1 {
    margin-top: 20px;
}
.document_file1 a{
    text-decoration: none !important;
    color: #6572b8 !important;
}


</style>

<script type="text/javascript">

// $("select[name='current_section_id'").removeAttr("disabled");
    jQuery(document).ready(function () {

        var test = $("#question_paper_id").val();
        if(test == ''){
            $("select[name='qp_section_id'").html('');
            $("select[name='qp_section_id'").removeAttr("disabled");
            $("select[name='qp_section_id'").selectpicker('refresh');   

            $("select[name='qp_subject_id'").html('');
            $("select[name='qp_subject_id'").removeAttr("disabled");
            $("select[name='qp_subject_id'").selectpicker('refresh');
        }

        $("#question-paper-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                name: {
                    required: true
                },
                qp_class_id: {
                    required: true
                },
                qp_section_id: {
                    required: true
                },
                qp_subject_id: {
                    required: true
                },
                qp_exam_type_id: {
                    required: true
                },
                documents: {
                    required: function (e) {
                        if($('#question_paper_id').val() != ''){
                            return false;
                        }else{
                            return true;
                        }
                    }
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>

<script type="text/javascript">


function getSection(class_id)
    {
        if(class_id != "") {
        $('.mycustloading').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/question-paper/get-section-data')}}",
            type: 'GET',
            data: {
                'class_id': class_id
            },
            success: function (data) {
                    $("select[name='qp_section_id'").html('');
                    $("select[name='qp_section_id'").html(data.options);
                    $("select[name='qp_section_id'").removeAttr("disabled");
                    $("select[name='qp_section_id'").selectpicker('refresh');
                    $('.mycustloading').hide();
            }
        });
    } else {
            $("select[name='qp_section_id'").html(''); 
            $("select[name='qp_section_id'").selectpicker('refresh');
        }
    }


</script>
<script type="text/javascript">

function getSubject(class_id)
    {
        if(class_id != "") {
        $('.mycustloading').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/question-paper/get-subject-data')}}",
            type: 'GET',
            data: {
                'class_id': class_id
            },
            success: function (data) {
                    $("select[name='qp_subject_id'").html('');
                    $("select[name='qp_subject_id'").html(data.options);
                    $("select[name='qp_subject_id'").removeAttr("disabled");
                    $("select[name='qp_subject_id'").selectpicker('refresh');
                    $('.mycustloading').hide();
            }
        });
    } else {
            $("select[name='qp_subject_id'").html(''); 
            $("select[name='qp_subject_id'").selectpicker('refresh');
        }
    }




</script>