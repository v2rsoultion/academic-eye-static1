<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'shifts';
    protected $primaryKey = 'shift_id';
    public function up()
    {
        if (!Schema::hasTable('shifts')) { 
            Schema::create('shifts', function (Blueprint $table) {
                $table->increments('shift_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('shift_name', 255)->nullable();
                $table->time('shift_start_time')->nullable();
                $table->time('shift_end_time')->nullable();
                $table->tinyInteger('shift_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('shifts', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('shifts', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shifts');
    }
}
