@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_staff_attendance') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <a href="{!! url('admin-panel/staff/staff-attendance/add-staff-attendance') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/staff') !!}">{!! trans('language.menu_staff') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/staff') !!}">{!! trans('language.staff_attendance') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/staff/staff-attendance/view-staff-attendance') !!}">{!! trans('language.view_staff_attendance') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           <div class="body form-gap ">
                               <form>
                                    <div class="row clearfix">
                                      <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('staff_id', old('staff_id', ''), ['class' => 'form-control ','placeholder'=>'Staff ID', 'id' => 'staff_id']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.staff_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="col-lg-1 col-md-1">
                                          <button type="submit" class="btn btn-raised btn-primary" title="Search">Search
                                          </button>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <button type="submit" class="btn btn-raised btn-primary" title="Clear">Clear
                                          </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="table-responsive">
                                   
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                            {{ csrf_field() }}
                                <thead>
                                    <tr>
                                        <th>{{trans('language.s_no')}}</th>
                                        <th>Attendance ID</th>
                                        <th>Staff Name</th>
                                        <th>Attendance</th>
                                        <th>Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                          <td>1</td>
                                          <td>Staff101</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                           <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                                <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="{!! url('admin-panel/staff/staff-attendance/view-profile') !!}">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                          <tr>
                                          <td>2</td>
                                          <td>Staff102</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                            <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="{!! url('admin-panel/staff/staff-attendance/view-profile') !!}">View More</a></li> 
                                                </ul> </div></td>
                                            
                                        </tr>
                                        <tr>
                                          <td>3</td>
                                          <td>Staff103</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                              <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="{!! url('admin-panel/staff/staff-attendance/view-profile') !!}">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                        <tr>
                                          <td>4</td>
                                          <td>Staff104</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                              <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="{!! url('admin-panel/staff/staff-attendance/view-profile') !!}">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                       <tr>
                                          <td>5</td>
                                          <td>Staff105</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                             <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="{!! url('admin-panel/staff/staff-attendance/view-profile') !!}">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                          <tr>
                                          <td>6</td>
                                          <td>Staff106</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                              <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="{!! url('admin-panel/staff/staff-attendance/view-profile') !!}">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                          <tr>
                                          <td>7</td>
                                          <td>Staff107</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                              <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="{!! url('admin-panel/staff/staff-attendance/view-profile') !!}">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                          <tr>
                                          <td>8</td>
                                          <td>Staff108</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                              <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="{!! url('admin-panel/staff/staff-attendance/view-profile') !!}">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                </tbody>
                            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#staff-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/staff/data')}}',
                data: function (d) {
                    d.class_id = $('input[name="name"]').val();
                    d.section_id = $('select[name="designation_id"]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'staff_name', name: 'staff_name'},
                {data: 'staff_designation_name', name: 'staff_designation_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

</script>
@endsection

<div class="modal fade" id="attendanceModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ankit Dave </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <table class="table m-b-0 c_list">
                    <thead>
                      <tr>
                        <th class="text-center">S no</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Attendance</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">20 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: red;"></i> 
                        (Absent)</td>
                      </tr>
                     <tr>
                        <td class="text-center">2</td>
                        <td class="text-center">21 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: red;"></i> 
                        (Absent)</td>
                      </tr>
                      <tr>
                        <td class="text-center">3</td>
                        <td class="text-center">22 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i>  
                        (Present)</td>
                      </tr>
                      <tr>
                        <td class="text-center">4</td>
                        <td class="text-center">23 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i>  
                        (Present)</td>
                      </tr>
                      <tr>
                        <td class="text-center">5</td>
                        <td class="text-center">24 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i>  
                        (Present)</td>
                      </tr>
                      <tr>
                        <td class="text-center">6</td>
                        <td class="text-center">25 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: red;"></i> 
                        (Absent)</td>
                      </tr>
                      <tr>
                        <td class="text-center">7</td>
                        <td class="text-center">26 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: red;"></i> 
                        (Absent)</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>


