<!-- @if(isset($staff_role['staff_role_id']) && !empty($staff_role['staff_role_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif -->

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
    /*.theme-blush .btn-primary {
    margin-top: 3px !important;
}*/
</style>
<!-- 
{!! Form::hidden('staff_role_id',old('staff_role_id',isset($staff_role['staff_role_id']) ? $staff_role['staff_role_id'] : ''),['class' => 'gui-input', 'id' => 'staff_role_id', 'readonly' => 'true']) !!} -->

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.staff_role_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_role_name', old('staff_role_name',isset($staff_role['staff_role_name']) ? $staff_role['staff_role_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_role_name'), 'id' => 'staff_role_name']) !!}
        </div>
        @if ($errors->has('staff_role_name')) <p class="help-block">{{ $errors->first('staff_role_name') }}</p> @endif
    </div>
    
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
   <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-primary"  title="Save">Save
                      </button>
                    </div>
                     <div class="col-lg-1 ">
                      <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel
                      </button>
                    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#staff-role-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                holiday_name: {
                    required: true
                },
                holiday_start_date: {
                    required: true
                },
                holiday_end_date: {
                    required: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        
        $('#holiday_end_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false });
        $('#holiday_start_date').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false}).on('change', function(e, date)
        {
        $('#holiday_end_date').bootstrapMaterialDatePicker('setMinDate', date);
        });
        
    });

    

</script>