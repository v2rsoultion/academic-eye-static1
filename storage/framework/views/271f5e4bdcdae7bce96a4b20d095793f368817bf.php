<!DOCTYPE html>
<html>
<head>
	<title>Fee Receipt-02</title>
<link rel="stylesheet" href="../public/assets/plugins/bootstrap/css/bootstrap.min.css">
<style type="text/css">
	body {
		width: 720px;
		margin: auto;
		font-size: 13.7px;
		font-weight: bold;
	}
	.less {
		font-weight: normal;
	}
	.align {
		text-align: center;
	}
	.header {
		padding-bottom: 20px;
	}
	tr {
		height: 30px !important;
	}
	.bold {
		font-weight: bold;
	}
	.box-size {
		float:left;
		border:1px solid #000;
		width: 15px;
		height: 15px;
		margin-top: 5px;
		vertical-align: middle;
	}
	.box-content {
		float:left;
		margin: 2px 10px 0px 3px;
	}
	.head-gap {
		float:left;
		margin-right: 10px;
	}
	.heading-style {
		text-decoration: underline;
		padding: 20px 0px 0px 0px;
	}
</style>

</head>
<body>
	<div class="header">
		<table style="width: 100%;">
			<tr>
				<td style="width: 1%;">
					<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="80px">
				</td>
				<td style="width: 25%;" class="align">
					<h2 style="margin:0px;">Academic Eye Public School</h2>	
					<p class="less" style="font-size: 12px;margin-top: 0px;margin-bottom: 2px;"> Phone: 0123-345673, Fax: 0123-345691 &nbsp;&nbsp; <br>E-mail : academiceye.info@gmail.com</p>	
				</td>
				<td style="border:1px solid #000;width: 8%;height: 130px;">
				</td>
			</tr>
		</table>
		<hr>
		<table>
			<tr>
				<td colspan="3">
					<div style="width: 70px !important; float: left;">Form No:</div>
				
					<div style="text-align: left;border-bottom:1px dotted #000; width: 100px; float: left;">1991101</div>
					<div class="clearfix"></div>
				</td>
				<td colspan="2" style="text-align: right;">Date: ............................</td>
			</tr>
			<tr>
				<td colspan="3"><h4 class="heading-style bold">Student Information: </h4></td>
			</tr>	
			<tr>
				<td colspan="4">
					<div>Student's Name: ................................................................................................</div>	
				</td>
				<td style="text-align: left;">
					<div style="width:204px;">Student Type: Paid / RTE / Free</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="head-gap"> Gender:</div>
					<div class="box-size"> </div>
					<div class="box-content less">Male</div> 
					<div class="box-size"> </div> <div class="box-content less">Female </div>
					<div class="clearfix"> </div>
				</td>
				<td colspan="2" style="padding-left: -8px;">
					<div class="clearfix"></div>
					<div class="head-gap"> Category:</div>
					<div class="box-size"> </div>
					<div class="box-content less">GEN</div>
					<div class="box-size"> </div>
					<div class="box-content less">OBC</div>
					<div class="box-size"> </div>
					<div class="box-content less">SC/ST</div>
				</td>
				<td>Blood Group: ....................................</td>
			</tr>
			<tr>
				<td colspan="2">
					Date of Birth: .................................
				</td>
				<td colspan="2">Caste: .....................................................</td>
				<td>Religion: ............................................</td>
			</tr>
			<tr>
				<td colspan="4">
					Email: .................................................................................................................
				</td>
				<td colspan="2">
					<div>Nationality: .......................................</div>
				</td>
			</tr>
			<tr>
				<td colspan="3"><h4 class="heading-style bold">Parent Information: </h4></td>
			</tr>	
			<tr>
				<td colspan="5">Father's Name: .................................................................................................................................................................</td>	
			</tr>
			<tr>
				<td colspan="3">
					<div>Occupation: ..................................................................</div>
				</td>
		
				<td colspan="2">
					Annual Salary: .....................................................................
				</td>
			</tr>
			<tr>
				<td colspan="3">Contact No: ..................................................................</td>
		
				<td colspan="2">Email: ...................................................................................</td>
			</tr>
			<tr>
				<td colspan="5">Mother's Name: ................................................................................................................................................................</td>	
			</tr>
			<tr>
				<td colspan="3">
					<div>Occupation: ..................................................................</div>
				</td>
		
				<td colspan="2">
					Annual Salary: .....................................................................
				</td>
			</tr>
			<tr>
				<td colspan="3">Contact No: ..................................................................</td>
		
				<td colspan="2">Email: ...................................................................................</td>
			</tr>
			<tr>
				<td colspan="5"><h4 class="heading-style bold">Guardian Information: </h4></td>
			</tr>	
			<tr>
				<td colspan="5">Guardian's Name: ..........................................................................................................................................................</td>	
			</tr>
			<tr>
				<td colspan="2">Email: .............................................</td>
		
				<td colspan="2">Contact No: ..........................................</td>
				<td>Relation: .........................................</td>
			</tr>
			<tr>
				<td colspan="5"><h4 class="heading-style bold">Previous School Information: </h4></td>
			</tr>	
			<tr>
				<td colspan="5">Previous School Name: .................................................................................................................................................</td>
			</tr>
			<tr>
				<td colspan="3">Class: ............................................................................</td>
				<td>Year: .......................</td>
				<td>TC No.: .............................................</td>
			</tr>
			<tr>
				<td colspan="3"><h4 class="heading-style bold">Address Information: </h4></td>
			</tr>	
			<tr>
				<td colspan="5">Local Address: ................................................................................................................................................................</td>
			</tr>
			<tr>
				<td colspan="5">Permanent Address: .......................................................................................................................................................</td>
			</tr>
		</table>
		<hr>
		<table>
			<tr>
				<td class="bold" style="width:100px;padding-top: -20px;">Declaration: </td>
				<td style="font-size: 12px;" class="less"> I have gone through the rules and regulation unmentioned in the school hand book and i agree school authorities the date of birth mentioned above is correct and i shall not request for it's change.</td>
			</tr>
		</table>
		<hr>
		<table>
			<tr style="font-size: 12px;">
				<td style="width: 110px;">Attachment Req: </td>
				<td style="width:470px;"> 
					<div class="box-size" style="clear: both"> </div>
					<div class="box-content less"> T.C. (Original) </div>
					<div class="box-size"> </div>
					<div class="box-content less"> Mark Sheet (Photocopy) </div>
					<div class="box-size"> </div>
					<div class="box-content less"> Birth Certificate (Photocopy) </div>
					<div class="clearfix"></div>
				</td>
			</tr>
			<tr>
				<div class="clearfix"></div>
				<td style="border-bottom:1px solid #000;padding-top: 30px;"> </td>
				<td> </td>
				<td style="width:122px;border-bottom:1px solid #000;"> </td>
			</tr>
			<tr style="font-size: 12px;" class="align less">
				<td>Sign. of Student</td>
				<td> </td>
				<td>Sign. of Parent/Guardian</td>
			</tr>
		</table>
		<hr style="margin: 0px;">
		<div style="width:190px;color: #fff;background-color: #000;padding: 0px 10px 6px 10px;font-size: 13px;margin-top: -10px;margin-left: 240px;" class="align"> For Office Use Only</div>
		<table style="width:100%;">
			<tr>
				<td colspan="3"style="text-align: right;">Date: ................................</td>
			</tr>
			<tr class="less">
				<td style="width: 320px;font-size: 12.5px;">On Scrutiny, the student has been found eligible for class</td>
				<td style="width: 100px;">..........................................</td>
				<td style="font-size: 12.5px;">is accordingly admit.</td>
			</tr>
			<tr>
				<td class="less">Receipt No.: ............................................................</td>
				<td class="less">Class: ............................</td>
				<td class="less">Form No.: ...........................................</td>
			</tr>
			<tr>
				<td class="bold"> Checked By: __________________________</td>
				<td class="less" style="padding-top: 30px;">Signature of Clerk</td>
				<td style="padding-top: 30px;" class="align">Principle</td>
			</tr>
		</table>
	</div>
	
					
</body>
</html>

<script type="text/javascript">
	// window.print();
</script>