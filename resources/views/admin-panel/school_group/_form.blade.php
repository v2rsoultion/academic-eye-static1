@if(isset($schoolgroup['group_id']) && !empty($schoolgroup['group_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
    /*.theme-blush .btn-primary {
    margin-top: 4px !important;
}*/
</style>

{!! Form::hidden('group_id',old('group_id',isset($schoolgroup['group_id']) ? $schoolgroup['group_id'] : ''),['class' => 'gui-input', 'id' => 'group_id', 'readonly' => 'true']) !!}
<p class="red">
@if ($errors->any())
    {{$errors->first()}}
@endif
</p>
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.school_group_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('group_name', old('group_name',isset($schoolgroup['group_name']) ? $schoolgroup['group_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_group_name'), 'id' => 'group_name']) !!}
        </div>
        @if ($errors->has('group_name')) <p class="help-block">{{ $errors->first('group_name') }}</p> @endif
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[0-9a-zA-Z]+$/.test(value);
        }, "Only alphanumerical characters");

        $("#school-house-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                group_name: {
                    required: true,
                    lettersonly:true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>