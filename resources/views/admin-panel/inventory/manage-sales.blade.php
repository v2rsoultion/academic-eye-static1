@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
.card .body .table td,
.cshelf1 {
    width: 100px;
    height: auto !important;
}
.tabing {
    width: 25%;
    padding: 0px !important;
    border: 1px solid #ccc;
    border-radius: 5px;
    margin-bottom: 20px;
    /*background: #959ecf;*/
   }
   .theme-blush .nav-tabs .nav-link.active{
      color: #fff !important;
    border-radius: 0px !important;
    background: #1f2f60 !important;
    width: 94px;
   }
   .nav-tabs>.nav-item>.nav-link {
    width: 94px;
   }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Manage Sales</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">Inventory</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/inventory/manage-sales') !!}">Manage Sales</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body form-gap1">
               
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Sales :-</div> -->
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">System Invoice No: </lable>
                          <input type="text" name="system_invoice_no" id=" " value="PI-1029" class="form-control" readonly="true">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Invoice No: </lable>
                          <input type="text" name="invoice_no" id=" " class="form-control" placeholder="Invoice No">
                        </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Date: </lable>
                          <input type="text" name="date" id="date" class="form-control" placeholder="Date">
                        </div>
                      </div>
                    
                     <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Category</lable>
                            <select class="form-control show-tick select_form1" name="category" id="">
                              <option value="">Select Category</option>
                              <option value="1">Pen</option>
                              <option value="2">Notebooks</option>
                            </select>
                         </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Sub-Category</lable>
                            <select class="form-control show-tick select_form1" name="subcategory" id="">
                              <option value="">Select Sub-Category</option>
                              <option value="1">Blue Pen</option>
                              <option value="2">Black Pen</option>
                              <option value="3">Small Notebooks</option>
                              <option value="4">Long Notebooks</option>
                            </select>
                         </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Person: </lable>
                          <input type="text" name="person" id="person" class="form-control" data-toggle="modal" data-target="#PersonModel">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Payment Mode</lable>
                            <select class="form-control show-tick select_form1" name="payment_mode" id="payment_mode">
                              <option value="">Select Payment</option>
                              <option value="1">Cash</option>
                              <option value="2">Bank Names</option>
                              <option value="3">Credit</option>
                              <option value="4">Cheque</option>
                            </select>
                         </div>
                      </div>

                     <input type="hidden" name="" class="form-control" id="hidden_item_store" />
                    </div>   
                    <div class="row" style="display: none;" id="ask_cheque_details">
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Bank Name: </lable>
                          <input type="text" name="bank_name" id=" " class="form-control" placeholder="Bank Name">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Cheque No: </lable>
                          <input type="text" name="cheque_no" id=" " class="form-control" placeholder="Cheque No">
                        </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Cheque Date: </lable>
                          <input type="text" name="date" id="takeDate" class="form-control" placeholder="Cheque Date">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Cheque Amount: </lable>
                          <input type="text" name="cheque_amount" id=" " class="form-control" placeholder="Cheque Amount">
                        </div>
                      </div>
                    </div>
                    <div class="row" id="studentdetails" style="display: none;">
                      <!-- <div class="col-lg-2">
                        <div class="form-group">
                          <label>Department:2  </label>
                        </div>
                      </div> -->
                       <div class="col-lg-2">
                        <div class="form-group">
                          <label>Class: 2 C  </label>
                        </div>
                      </div>
                       <div class="col-lg-2">
                        <div class="form-group">
                          <label>Name: Arya </label>
                        </div>
                      </div>
                    </div>

                    <div class="row" id="staffdetails" style="display: none;">
                      <div class="col-lg-2">
                        <div class="form-group">
                          <label>Role: Principal </label>
                        </div>
                      </div>
                       <div class="col-lg-2">
                        <div class="form-group">
                          <label>Name: P P MOHAMMED </label>
                        </div>
                      </div>
                    </div>

                  </form>
                
                  <!--  DataTable for view Records  -->
                 
                    <div class="table-responsive">
                    <table class="table table-bordered m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th style="width: 200px">Item Name</th>
                        <th class="text-center">Unit</th>
                        <th class="text-center" style="width: 120px">Quantity</th>
                        <th class="text-center" style="width: 120px">Rate</th>
                        <th class="text-center">Amount</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
            
                      <tr>
                        <td><select class="form-control show-tick select_form1 item_info" name="category" id="item_info">
                              <option value="">Select Item</option>
                              <option value="Pen">Pen</option>
                              <option value="Notebooks">Notebooks</option>
                            </select>
                             <br>Available:<label id="num"> </label>
                        </td>
                      <td id="unit" class="text-center"> </td>
                        <td><input type="text" name="quantity" id="quantity" class="form-control" placeholder="Quantity" onchange="showAmount()"> </td>
                        <td id="rate" class="text-center"> </td>
                        <td id="amount" class="text-right"> </td>
                        <td style="text-align: center;"><a href="#" class="btn btn-raised btn-primary add_sales_details">Add</a></td>
                      </tr>
                    </tbody>
                  </table>
                   <div class="table-responsive">
                    <table class="table table-bordered m-b-0 c_list" id="show_sales_table" style="width:100%">
                    {{ csrf_field() }}
                   
                    <tbody>

                    </tbody>
                  </table>
                  <!-- <div class="table-responsive" style="display: none;" id="show_table">
                    <table class="table table-bordered m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                   
                    <tbody>
            
                      <tr>
                      <td style="width: 290px;">Pen Black</td>
                      <td class="text-center">10</td>
                      <td class="text-center">10</td>
                      <td class="text-center">100</td>
                      <td class="text-center"><button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button></td>
                      </tr>
                    </tbody>
                  </table>
            
                </div> -->
                </div>

                <div class="col-lg-6 float-right padding-0" style="width:100%; margin: 20px 0px;">
                       <table class="table table-bordered m-b-0 c_list" id="" style="width:100%">
                        <tbody>
                        <tr>
                          <td>Total Amount</td>
                          <td></td>
                          <td class="text-right"><label id="lbltotal"> </label></td>
                        </tr>
                        <tr>
                          <td class="col-lg-3 col-md-3">Tax(%)</td>
                          <td><input id="txttax" class="select2_single form-control" type="text"></td>
                          <td style="text-align: right;"><label id="lbltax"></label></td>
                        </tr>
                        <tr>
                          <td class="col-lg-3 col-md-3">Discount(%)</td>
                          <td><input id="txtdiscount" type="text" class="select2_single form-control"></td>
                          <td class="text-right"><label id="lbldiscount"> </label></td>
                        </tr>
                        <tr>
                          <td class="col-lg-3 col-md-3">Gross Amount</td>
                          <td></td>
                          <td class="rightalign" style="text-align: right;"><label id="lblgross"></label></td>
                        </tr>
                          <tr>
                          <td class="col-lg-3 col-md-3">Advance</td>
                          <td></td>
                          <td class="rightalign"><label id="lblgross"></label></td>
                        </tr>
                        <tr>
                          <td class="col-lg-3 col-md-3">Round off</td>
                          <td style="text-align: center;"><label><input class="select2_single form-control" type="text" readonly="true" id="roundoff"></label>Disable Roundoff<br><input type="checkbox" name=""></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="col-md-3 col-xs-3 col-sm-3">Net Amount</td>
                          <td></td>
                          <td class="rightalign" style="text-align: right;"><label id="lblnet"></label></td>
                        </tr>
                       </tbody></table>
      
                        <form id="manage_purchase">
                      <div class="row float-right" style="margin-top: 20px">
                      
                      <div class="col-lg-2">
                        <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
                        </button>
                      </div>
                    </div>
                  </form>
                    </div>
                     
              </div>
              

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

<!-- Action Model  -->
<div class="modal fade" id="PersonModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Person</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <ul class="nav nav-tabs tabing">
         <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#student">Student</a></li>
         <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#staff">Staff</a></li>

      </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="student">
      <div class="row feecounterclass">
          <!-- <div class="col-lg-4">
            <div class="form-group">
                <lable class="from_one1">Department</lable>
                <select class="form-control show-tick select_form1" name="category" id="">
                  <option value="">Select Department</option>
                  <?php $count = 1; for ($i=0; $i < 4 ; $i++) {  ?>
                   <option value="<?php echo $count; ?>">Department <?php echo $count; ?></option>
                 <?php $count++; } ?>
                </select>
             </div>
          </div> -->
          <div class="col-lg-4">
            <div class="form-group">
                <lable class="from_one1">Class</lable>
                <select class="form-control show-tick select_form1" name="category" id="">
                  <option value="">Select Class</option>
                  <?php $count = 1; for ($i=0; $i < 4 ; $i++) {  ?>
                   <option value="<?php echo $count; ?>">Class <?php echo $count; ?></option>
                 <?php $count++; } ?>
                </select>
             </div>
          </div>
            
          <div class="col-lg-4">
            <div class="form-group">
                <lable class="from_one1">Sales</lable>
                <select class="form-control show-tick select_form1" name="category" id="studentDetails">
                  <option value="">Select Sales</option>
                  <option>Amit</option>
                  <option>Aman</option>
                  <option>Sumit</option>
                </select>
             </div>
          </div>
      </div> 
    </div>
      <div class="tab-pane fade" id="staff">
      <div class="row feecounterclass">
          <div class="col-lg-4">
            <div class="form-group">
                <lable class="from_one1">Sales</lable>
                <select class="form-control show-tick select_form1" name="category" id="">
                  <option value="">Select Sales</option>
                  <option>Principal</option>
                  <option>Vice Principal</option>
                  <option>Clerk</option>
                  <option>PTA</option>
                  <option>Data Ebtry OPERATOR</option>
                  <option>Management</option>
                </select>
             </div>
          </div>
           <div class="col-lg-4">
            <div class="form-group">
                <lable class="from_one1">Name</lable>
                <select class="form-control show-tick select_form1" name="category" id="staffDetails">
                  <option value="">Select Name</option>
                  <option>Amit</option>
                  <option>Aman</option>
                  <option>Sumit</option>
                  <option>Pooja</option>
                </select>
             </div>
          </div>
      </div> 
    </div>
  </div>

      </div>
    </div>
  </div>
</div>


<!-- Action Model  -->
<div class="modal fade" id="ItemsDetailsInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Items</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Sales</th>
                        <th>Available</th>
                        <th>Rate</th>
                        <th class="text-center">Select</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr id="row-1">
                        <td id="item1">Black Pen</td>
                        <td>26-09-2018</td>
                        <td id="available1">71</td>
                        <td id="rate1">15</td>
                        <td class="text-center">
                          <a href="#" class="btn btn-raised btn-primary" onclick="select(1)">Select</a>
                        </td>
                      </tr>
                      <tr id="row-2">
                         <td id="item2">Orange Pen</td>
                        <td>15-10-2018</td>
                        <td id="available2">4</td>
                        <td id="rate2">100</td>
                        <td class="text-center">
                          <a href="#" class="btn btn-raised btn-primary" onclick="select(2)">Select</a>
                        </td>
                      </tr>  
                      <tr id="row-3">
                         <td id="item3">Blue Pen</td>
                        <td>15-10-2018</td>
                        <td id="available3">4</td>
                        <td id="rate3">100</td>
                        <td class="text-center">
                          <a href="#" class="btn btn-raised btn-primary" onclick="select(3)">Select</a>
                        </td>
                      </tr> 
                       <tr id="row-4">
                         <td id="item4">Red Pen</td>
                        <td>15-10-2018</td>
                        <td id="available4">4</td>
                        <td id="rate4">80</td>
                        <td class="text-center">
                          <a href="#" class="btn btn-raised btn-primary" onclick="select(4)">Select</a>
                        </td>
                      </tr> 
                    </tbody>
                  </table>
            
                </div>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="NotebookDetailsInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Items</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Sales</th>
                        <th>Available</th>
                        <th>Rate</th>
                        <th class="text-center">Select</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td id="item5">Drawing Notebooks</td>
                        <td>26-09-2018</td>
                        <td id="available5">71</td>
                        <td id="rate5">15</td>
                        <td class="text-center">
                          <a href="#" class="btn btn-raised btn-primary" onclick="selected(5)">Select</a>
                        </td>
                      </tr>
                      <tr>
                         <td id="item6">Register Notebooks</td>
                        <td>15-10-2018</td>
                        <td id="available6">4</td>
                        <td id="rate6">100</td>
                        <td class="text-center">
                          <a href="#" class="btn btn-raised btn-primary" onclick="selected(6)">Select</a>
                        </td>
                      </tr>  
                      <tr>
                         <td id="item7">Dairy</td>
                        <td>15-10-2018</td>
                        <td id="available7">4</td>
                        <td id="rate7">100</td>
                        <td class="text-center">
                          <a href="#" class="btn btn-raised btn-primary" onclick="selected(7)">Select</a>
                        </td>
                      </tr> 
                       <tr>
                         <td id="item8">Books</td>
                        <td>15-10-2018</td>
                        <td id="available8">4</td>
                        <td id="rate8">80</td>
                        <td class="text-center">
                          <a href="#" class="btn btn-raised btn-primary" onclick="selected(8)">Select</a>
                        </td>
                      </tr> 
                    </tbody>
                  </table>
            
                </div>

      </div>
    </div>
  </div>
</div>
<!-- Content end here  -->
<script type="text/javascript">
  function select(count) {
    $("#ItemsDetailsInfoModal").modal('hide');  
    var item = $("#item"+count).text();
    $("#hidden_item_store").val(item);
    var available = $("#available"+count).text();
    var rate = $("#rate"+count).text();
    $("#num").text(available);
    $("#unit").text('Nos.');
    $("#rate").text(rate);
  }
  function selected(count) {
    $("#NotebookDetailsInfoModal").modal('hide');  
    var item = $("#item"+count).text();
    $("#hidden_item_store").val(item);
    var available = $("#available"+count).text();
    var rate = $("#rate"+count).text();
    $("#num").text(available);
    $("#unit").text('Nos.');
    $("#rate").text(rate);
  }
  function showAmount() {
    var rate = $("#rate").text();
    var quantity = $("#quantity").val();
    var amount = rate*quantity;
    $('#amount').text(amount);
  }

  $("#payment_mode").on('change', function() {
     var test = $("#payment_mode").val();
     if(test == 4)
     {
       $('#ask_cheque_details').show();
     }
     else
     {
        $('#ask_cheque_details').hide();
     }

  });
</script>
@endsection