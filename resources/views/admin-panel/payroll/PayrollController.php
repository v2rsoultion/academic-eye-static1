<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PayrollController extends Controller
{
    public function mangeDepartment() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.manage-department')->with($data);
    }
    
    public function viewMapEmployees() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.map-employees')->with($data);
    }

    public function mangeGrades() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.manage-grade')->with($data);
    }

    public function viewGradeMapEmployees() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.grade-map-employees')->with($data);
    }

    public function manageLeaveScheme() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.manage-leave-scheme')->with($data);
    }

    public function leaveSchemeMapEmployees() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.leave-scheme-map-employees')->with($data);
    }

     public function managePfEsiSetting() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.manage-pf-esi-setting')->with($data);
    }

    public function manageTdsSetting() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.manage-tds-setting')->with($data);
    }

    public function managePtSetting() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.manage-pt-setting')->with($data);
    }

    public function manageSalaryHeads() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.manage-salary-heads')->with($data);
    }

    public function salaryStructure() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.manage-salary-structure')->with($data);
    }

    public function mapStructure() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.map-salary-structure')->with($data);
    }

    public function addArrears() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.add-arrears')->with($data);
    }

    public function manageArrears() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.manage-arrears')->with($data);
    }

    public function addBonus() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.add-bonus')->with($data);
    }

    public function manageBonus() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.manage-bonus')->with($data);
    }

    public function manageLoan() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.manage-loan')->with($data);
    }

    public function manageAdvance() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.manage-advance')->with($data);
    }

    public function manageSalaryGeneration() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.payroll.manage-salary-generation')->with($data);
    }
}
