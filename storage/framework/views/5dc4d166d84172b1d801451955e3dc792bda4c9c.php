<style type="text/css">

 /*.sidebar .menu .list a.active {
  background-color: #f5f7f9 !important;
    border-left: 2px solid #6572b8 !important;
}*/
</style>
<aside id="leftsidebar" class="sidebar">
<div class="tab-content">
<div class="tab-pane stretchRight active" id="dashboard">
  <div class="menu">
    <ul class="list">
      <li>
        <div class="user-info">
          <div class="image" style="padding-top: 8px !important;"><a href=""><img src="<?php echo URL::to('public/assets/images/logo_new1.png'); ?>" alt="User" width="70%" ></a></div>
        </div>
      </li>

      <!-- ======== Start here side menu bar ========== -->
      <div class="adminmenu" style="padding-top: 6px;">
        <ul class="text-center">
          <a href="<?php echo e(url('admin-panel/menu/configuration')); ?>" title="Configuration" class="icyLink">
            <li class="<?php if(Request::segment(3) == "configuration" || Request::segment(2) == "configuration" || Request::segment(2) == "school" || Request::segment(2) == "session" || Request::segment(2) == "holidays" || Request::segment(2) == "shift" || Request::segment(2) == "facilities" || Request::segment(2) == "document-category" || Request::segment(2) == "designation" || Request::segment(2) == "title" || Request::segment(2) == "caste" || Request::segment(2) == "religion" || Request::segment(2) == "nationality" || Request::segment(2) == "schoolgroup" || Request::segment(2) == "room-no" || Request::segment(2) == "stream" || Request::segment(2) == "country" || Request::segment(2) == "state" || Request::segment(2) == "city"): ?>  active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_configuration'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/academic')); ?>" title="Academic">
            <li class="<?php if(Request::segment(3) == "academic" || Request::segment(2) == "academic" || Request::segment(2) == "class" || Request::segment(2) == "section" || Request::segment(2) == "class-teacher-allocation" || Request::segment(2) == "subject" || Request::segment(2) == "competition" || Request::segment(2) == "time-table"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_academic'); ?></span> 
            </li>
          </a>
           <a href="<?php echo e(url('admin-panel/menu/admission')); ?>" title="Admission">
            <li class="<?php if(Request::segment(3) == "admission" || Request::segment(2) == "admission" || Request::segment(2) == "admission-form"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_admission'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/student')); ?>" title="Student">
            <li class="<?php if(Request::segment(3) == "student" || Request::segment(2) == "student" || Request::segment(2) == "student-leave-application"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_student'); ?></span> 
            </li>
          </a>
       
          <a href="<?php echo e(url('admin-panel/menu/recruitment')); ?>" title="Recruitment">
            <li class="<?php if(Request::segment(3) == "recruitment" || Request::segment(2) == "recruitment"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_recruitment'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/examination')); ?>" title="Examination">
            <li class="<?php if(Request::segment(3) == "examination" || Request::segment(2) == "examination"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Examination.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_examination'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/fees-collection')); ?>" title="Fees Collection">
            <li class="<?php if(Request::segment(3) == "fees-collection" || Request::segment(2) == "fees-collection"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/FeesCollection.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_fee_collection'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/staff')); ?>" title="Staff">
            <li class="<?php if(Request::segment(3) == "staff" || Request::segment(2) == "staff"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Staff.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_staff'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/hostel')); ?>" title="Hostel">
            <li class="<?php if(Request::segment(3) == "hostel" || Request::segment(2) == "hostel"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Hostel.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_hostel'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/library')); ?>" title="Library">
            <li class="<?php if(Request::segment(3) == "library" || Request::segment(2) == "library"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Library.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_library'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/transport')); ?>" title="Transport">
            <li class="<?php if(Request::segment(3) == "transport" || Request::segment(2) == "transport"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Transport.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_transport'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/visitor')); ?>" title="Visitor">
            <div class="firstdiv12">
              <li class="<?php if(Request::segment(3) == "visitor" || Request::segment(2) == "visitor"): ?> active <?php endif; ?>">
                <span><?php echo trans('language.menu_visitor'); ?></span> 
              </li>
            </div>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/notice-board')); ?>" title="Noticeboard">
            <li class="<?php if(Request::segment(3) == "notice-board" || Request::segment(2) == "notice-board"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Noticeboard.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_notice_board'); ?></span> 
            </li>
          </a>
           <a href="<?php echo e(url('admin-panel/menu/online-content')); ?>" title="Online Content">
            <li class="<?php if(Request::segment(3) == "online-content" || Request::segment(2) == "online-content"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_online_content'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>" title="Task Manager">
            <li class="<?php if(Request::segment(3) == "task-manager" || Request::segment(2) == "task-manager"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_task_manager'); ?></span> 
            </li>
          </a>
          <!-- <a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>" title="Theme Option's">
            <li class="<?php if(Request::segment(3) == "" || Request::segment(2) == ""): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_theme_options'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>" title="University">
            <li class="<?php if(Request::segment(3) == "" || Request::segment(2) == ""): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_university'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>" title="Componets">
            <li class="<?php if(Request::segment(3) == "" || Request::segment(2) == ""): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_components'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>" title="Extra">
            <li class="<?php if(Request::segment(3) == "" || Request::segment(2) == ""): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Extra.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_extra'); ?></span> 
            </li>
          </a> -->
        </ul>
        <br><br><br>
        <div class="clearfix"></div>
      </div>
    </ul>
  </div>
</div>
</div>
</aside>
