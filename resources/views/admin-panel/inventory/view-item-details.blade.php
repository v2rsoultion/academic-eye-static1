@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Item Details</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">Inventory</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/inventory/view-purchase-entry') !!}">View Purchase Entry</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/inventory/view-purchase-entry/item-details') !!}">View Item Details</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                  
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">View Item Details :-</div> -->
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-3">
                        <div class="form-group">
                          <!-- <lable class="from_one1" for="name">Name</lable> -->
                          <input type="text" name="item_name" id="" class="form-control" placeholder="Item Name">
                        </div>
                      </div>
                     <div class="col-lg-3">
                        <div class="form-group">
                            <select class="form-control show-tick select_form1" name="vendor" id="">
                              <option value="">Select Unit</option>
                              <option value="1">Nos</option>
                              <option value="2">Mtrs</option>
                              <option value="3">Kg</option>
                              <option value="4">Cm</option>

                            </select>
                         </div>
                      </div>
                    
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary" title="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
              <div style="width:100%;border: 1px solid #ccc;border-radius: 5px;font-size:13px;padding: 5px 10px;margin-left: 0px;margin-bottom: 10px;">
                <div><b>Invoice No:</b> PI-1028</div>
                <div><b>Tax:</b> 10%</div>
                <div><b>Discount:</b> 5%</div>
                <div><b>Total Amount:</b> 10000</div>
              </div>

                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <!-- <hr> -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>S No</th>
                         <th>Item Name</th>
                        <th>Unit</th>
                        <th>Rate</th>
                        <th>Quantity</th>
                        <th>Amount</th>
                        
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $count = 1; for ($i=0; $i < 15; $i++) {  ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td>Pen Black</td>
                        <td>Nos.</td>
                        <td>12</td>
                        <td>12</td>
                        <td>144.00</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <?php $count++; } ?>
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->


@endsection