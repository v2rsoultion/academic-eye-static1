@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>My Subjects</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/subject') !!}">My Subjects</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  @if($login_info['admin_type'] == 2 || $login_info['admin_type'] == 3)
                   <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Staff/Performance.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Home Work</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('admin-panel/subject/subject-homework/add-homework') }}" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="{{ url('admin-panel/subject/subject-homework/view-homework') }}" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  @endif
                  @if($login_info['admin_type'] == 2 || $login_info['admin_type'] == 3)
                    <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Staff/Performance.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Remarks</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('admin-panel/subject/subject-remarks/add-remark') }}" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="{{ url('admin-panel/subject/subject-remarks/view-remark') }}" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    @endif
                     @if($login_info['admin_type'] == 2 || $login_info['admin_type'] == 3)
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Staff/Performance.svg') !!}" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Marks</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="" class="float-left" title="Add ">
                              <i class="fas fa-plus"></i> Add 
                            </a>
                            <a href="" class="float-right" title="View ">
                              <i class="fas fa-eye"></i>View 
                            </a>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        @endif
                    @if($login_info['admin_type'] == 2 || $login_info['admin_type'] == 3)
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Staff/Performance.svg') !!}" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Performance</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="" class="float-left" title="Add ">
                              <i class="fas fa-plus"></i> Add 
                            </a>
                            <a href="" class="float-right" title="View ">
                              <i class="fas fa-eye"></i>View 
                            </a>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        @endif
                        
                </div>
                
                                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
@endsection
