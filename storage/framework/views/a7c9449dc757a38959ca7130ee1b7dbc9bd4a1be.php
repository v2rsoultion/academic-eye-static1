
<?php $__env->startSection('content'); ?>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Mapping</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <a href="<?php echo e(url('admin-panel/task-manager/view-task')); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-eye"></i> </a>
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>"><?php echo trans('language.menu_task_manager'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('admin-panel/task-manager/view-task')); ?>">View Task</a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('admin-panel/task-manager/view-task/mapping')); ?>">Mapping</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                <div class="headingcommon  col-lg-12" style="padding-bottom: 20px !important">Map with Student / Staff:-</div>
                <form class="" action="" method="" id="mapp_form" style="width: 100%;">
                  <div class="row">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <div class="radio" style="margin-top:6px !important;">
                          <input id="radio32" name="map_with_student_staff" type="radio" value="1">
                          <label for="radio32" class="document_staff" onclick="checkForSelect('1')">Student</label>
                          <input id="radio31" name="map_with_student_staff" type="radio" value="0">
                          <label for="radio31" class="document_staff" onclick="checkForSelect('0')">Staff</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6" id="mapWithStudent" style="display: none;">
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <select class="form-control show-tick select_form1 select2">
                              <option value="">Select Class</option>
                              <option value="1">Class-1</option>
                              <option value="2">Class-2</option>
                              <option value="3">Class-3</option>
                              <option value="4">Class-4</option>    
                            </select>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <select class="form-control show-tick select_form1 select2">
                              <option value="">Select Section</option>
                              <option value="A">A</option>
                              <option value="B">B</option>
                              <option value="C">C</option>
                              <option value="D">D</option>    
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-1">
                      <button type="submit" class="btn btn-raised btn-primary" title="Submit" id="save">Search
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Clear
                      </button>
                    </div>
                  </div>
                </form>
                <div id="table_data_show">
                  <table class="table m-b-0 c_list" id="tablche" width="100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>User Name</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $count = 0; for ($i=0; $i < 5 ; $i++) {  ?>
                    <tr>
                      <td>
                        <div class="checkbox">
                          <input id="checkbox<?php echo  $count; ?>" type="checkbox">
                          <label class="from_one1" for="checkbox<?php echo  $count; ?>" style= "margin-bottom: 4px !important;"></label>
                        </div>
                      </td>
                      <td>
                        <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="5%"> Ankit Dave
                      </td>
                    </tr>
                    <?php  $count++; } ?>
                    </tbody>
                  </table>

                  <div class="col-lg-12">
                     <button type="button" class="btn btn-primary float-right" title="Save">Save</button>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">

  function checkForSelect(val) {
        var x = document.getElementById("mapWithStudent");
        if(val == 0) {
            if (x.style.display === "none") {
                x.style.display = "none";
            } else {
                x.style.display = "none";
            }
        } else {
            if (x.style.display === "block") {
                x.style.display = "block";
            } else {
                x.style.display = "block";
            }
        }

        var y = document.getElementById("mapWithStaff");
        if(val == 1) {
            if (y.style.display === "none") {
                y.style.display = "none";
            } else {
                y.style.display = "none";
            }
        } else {
            if (y.style.display === "block") {
                y.style.display = "block";
            } else {
                y.style.display = "block";
            }
        }
    }

    // $("#save").on('click', function() {
    //   $("#table_data_show").show();
    // });

</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>