
<?php $__env->startSection('content'); ?>
<style type="text/css">
  .card .body .table td,
.cshelf1 {
    /*width: 100px;*/
    height: auto !important;
}

</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Payment Receipt Voucher  </h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/account'); ?>">Accounts</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/account/payment-receipt-voucher'); ?>">Payment Receipt Voucher</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>  
              <div class="body" style="padding: 0px 20px;">
                  <div class="headingcommon  col-lg-12" style="margin-left: -13px">Select Mode of Entry :-</div>
                  <form class="" action="" method="" id="manage_print_invoice" style="width: 100%;">
                    <div class="row">
                      <div class="col-lg-3 col-md-3">
                        <div class="form-group">
                          <div class="radio" style="margin-top:6px !important;">
                               <input id="radio32" name="inventory_invoice" type="radio" value="1">
                                <label for="radio32" class="document_staff" onclick="checkForAccounting('1')">Accounting</label>
                                <input id="radio31" name="inventory_invoice" type="radio" value="0">
                                <label for="radio31" class="document_staff" onclick="checkForAccounting('0')">Invoice</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                  </form>
                  <div class="" id="accountingMode" style="display: none;">
               
                  <div class="headingcommon  col-lg-12" style="margin-left: -13px">Accounting Mode :-  Payment</div>
                  <form class="" action="" id="manage_vendor" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Date</lable>
                          <input type="text" name="date" id="date" class="form-control" placeholder="Date">
                        </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Voucher No</lable>
                          <input type="text" name="voucher_no" id="" class="form-control" placeholder="Voucher No">
                        </div>
                         <?php if($errors->has('voucher_no')): ?> <p class="help-block"><?php echo e($errors->first('voucher_no')); ?></p> <?php endif; ?>
                      </div>
                      <div class="col-lg-4"></div>
                      <div class="col-lg-2 text-right">
                        <a href="#" class="btn btn-raised btn-primary saveBtn add_journal_entries"><i class="fas fa-plus-circle"></i> Add</a>
                      </div>

                    </div>
                      
                  </form>
                  
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <!-- <hr> -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="dummy" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Particulars</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td><div class="row">
                          <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Main Heads</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Head</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Head- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div></div>
                         <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Sub Heads</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Subhead</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Subhead- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div></div>
                         <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Group</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Group</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Group- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div></div>
                         <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Head</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Head</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Head- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div></div>
                       </div></td>
                      <td><div class="form-group"><input type="text" name="" class="form-control  saveBtn debit" rel='' placeholder="Debit" id="debit"></div></td>
                      <td><div class="form-group"><input type="text" name="" class="form-control saveBtn credit" placeholder="Credit" id="credit" rel=''></div></td>
                        <!-- <td style="width: 105px">
                          <button class="btn btn-success btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="fas fa-plus-circle"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td> -->
                      </tr>
                    </tbody>
                  </table>
                 <table style="width: 100%">
                    <tr>
                      <td style="width: 30px"></td>
                      <td style="width: 720px">
                        <div class="col-lg-12 padding-0">
                          <div class="form-group">
                            <lable class="from_one1">Narration</lable><input type="text" name="navation" class="form-control" placeholder="Navation">
                          </div>
                        </div>
                      </td>

                    </tr>
                    <tr>
                      <td colspan="1"></td>
                      <td class="text-right">Total</td>
                      <td class="text-center">0.00 </td>
                      <td class="text-center">0.00 </td>
                      <td colspan="1"></td>
                    </tr>
                  </table>
            
                </div>
                <hr>
                <div class="row">
                  <div class="col-lg-1">
                    <button type="submit" class="btn btn-raised btn-primary" title= "Save">Save</button>
                  </div>
                  <div class="col-lg-1">
                    <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel</button>
                  </div>
                </div>
              </div>

              <div class="" id="invoiceMode" style="display: none;">
                 <div class="headingcommon  col-lg-12" style="margin-left: -13px">Invoice Mode :-  Purchase Voucher</div>
                <form class="" action="" id="manage_vendor" style="width: 100%;">
                  <div class="row" >
                    <div class="col-lg-4">
                      <div class="form-group">
                        <lable class="from_one1">Date</lable>
                        <input type="text" name="date" id="taskDate" class="form-control" placeholder="Date">
                      </div>
                    </div>
                     <div class="col-lg-4">
                      <div class="form-group">
                        <lable class="from_one1">Voucher No</lable>
                        <input type="text" name="voucher_no" id="" class="form-control" placeholder="Voucher No">
                      </div>
                       <?php if($errors->has('voucher_no')): ?> <p class="help-block"><?php echo e($errors->first('voucher_no')); ?></p> <?php endif; ?>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <lable class="from_one1">Ref. Voucher</lable>
                        <input type="text" name="ref_voucher" id="" class="form-control" placeholder="Ref Voucher">
                      </div>
                       <?php if($errors->has('ref_voucher')): ?> <p class="help-block"><?php echo e($errors->first('ref_voucher')); ?></p> <?php endif; ?>
                    </div>

                  </div>

                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <lable class="from_one1">Party Account Name</lable>
                        <br>
                        <a href="#" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#InfoModel"><i class="fas fa-plus-circle"></i> Add</a>
                      </div>
                       <?php if($errors->has('voucher_no')): ?> <p class="help-block"><?php echo e($errors->first('voucher_no')); ?></p> <?php endif; ?>
                    </div>

                    <div class="col-lg-4">
                      <div class="form-group">
                        <lable class="from_one1">Purchase Ledger</lable>
                        <br>
                        <a href="#" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#InfoModel"><i class="fas fa-plus-circle"></i> Add</a>
                      </div>
                       <?php if($errors->has('voucher_no')): ?> <p class="help-block"><?php echo e($errors->first('voucher_no')); ?></p> <?php endif; ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                          <input type="text" name="" class="form-control" readonly="true" value="Head Name: Amit" />
                        </div>
                      </div>
                      <div class="col-lg-1"></div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <input type="text" name="" class="form-control" readonly="true" value="Head Name: Aman" />
                        </div>
                      </div>
                  </div>  
                </form>
                
                <div class="clearfix"></div>
                <!-- <hr> -->
                <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th>Name of Item</th>
                        <th>Quantity</th>
                        <th>@Rate</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><div class="form-group">
                        <a href="#" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#InfoModel"><i class="fas fa-plus-circle"></i> Add Ledger/ A/c Head</a>
                      </div></td>
                        <td></td>
                        <td></td>
                        <td>10000</td>
                      </tr>
                      <tr>
                        <td colspan="3" class="text-right"> Total</td>
                        <td colspan="1"> 10000</td>
                      </tr>

                       <tr>
                        <td><div class="form-group">
                        <a href="#" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#InfoModel11"><i class="fas fa-plus-circle"></i> Add Taxes</a>
                      </div></td>
                        <td></td>
                        <td></td>
                        <td>10000</td>
                      </tr>
                      <tr>
                        <td colspan="3" class="text-right"> Total</td>
                        <td colspan="1">20000</td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- <hr> -->
                  <table style="width: 100%">
                    <tr>
                      <td style="width: 720px">
                        <div class="col-lg-12 padding-0">
                          <div class="form-group">
                            <lable class="from_one1">Narration</lable>
                            <textarea name="narration" class="form-control" placeholder="Narration"></textarea>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
                <hr>
                <div class="row">
                  <div class="col-lg-1">
                    <button type="submit" class="btn btn-raised btn-primary" title= "Save">Save</button>
                  </div>
                  <div class="col-lg-1">
                    <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel</button>
                  </div>
                </div>
              </div>


              </div>
              

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
<!-- Content end here  -->
<script type="text/javascript">
  function checkForAccounting(val) {
        var x = document.getElementById("accountingMode");
        if(val == 0) {
            if (x.style.display === "none") {
                x.style.display = "none";
            } else {
                x.style.display = "none";
            }
        } else {
            if (x.style.display === "block") {
                x.style.display = "block";
            } else {
                x.style.display = "block";
            }
        }

        var y = document.getElementById("invoiceMode");
        if(val == 1) {
            if (y.style.display === "none") {
                y.style.display = "none";
            } else {
                y.style.display = "none";
            }
        } else {
            if (y.style.display === "block") {
                y.style.display = "block";
            } else {
                y.style.display = "block";
            }
        }
    }
</script>


<!-- Action Model  -->
<div class="modal fade" id="InfoModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Ledger/ A/c Head</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-3">
            <div class="form-group">
              <lable class="from_one1">Main Heads</lable>
              <select class="form-control show-tick select_form1" name="" id="">
                <option value="">Select Head</option>
                <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                 <option value="<?php echo $count; ?>">Head- <?php echo $count; ?></option>
               <?php $count++; } ?>
              </select>
           </div></div>
           <div class="col-lg-3">
          <div class="form-group">
              <lable class="from_one1">Sub Heads</lable>
              <select class="form-control show-tick select_form1" name="" id="">
                <option value="">Select Subhead</option>
                <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                 <option value="<?php echo $count; ?>">Subhead- <?php echo $count; ?></option>
               <?php $count++; } ?>
              </select>
           </div></div>
           <div class="col-lg-3">
          <div class="form-group">
              <lable class="from_one1">Group</lable>
              <select class="form-control show-tick select_form1" name="" id="">
                <option value="">Select Group</option>
                <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                 <option value="<?php echo $count; ?>">Group- <?php echo $count; ?></option>
               <?php $count++; } ?>
              </select>
           </div></div>
           <div class="col-lg-3">
          <div class="form-group">
              <lable class="from_one1">Head</lable>
              <select class="form-control show-tick select_form1" name="" id="">
                <option value="">Select Head</option>
                <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                 <option value="<?php echo $count; ?>">Head- <?php echo $count; ?></option>
               <?php $count++; } ?>
              </select>
           </div></div>
        </div>
        <div class="row">
          <div class="col-lg-3">
            <div class="form-group">
              <lable class="from_one1">Quantity</lable>
              <input type="text" class="form-control" placeholder="Quantity">           
              </div>
          </div>
           <div class="col-lg-3">
            <div class="form-group">
              <lable class="from_one1">Rate</lable>
              <input type="text" class="form-control" placeholder="Rate">           
              </div>
          </div>
         </div>
         <hr>
         <div class="row">
           <div class="col-lg-1">
             <button type="submit" class="btn btn-raised btn-primary">Ok</button>
           </div>
           <div class="col-lg-1">  
             <button type="reset" class="btn btn-raised btn-primary" data-dismiss="modal">Cancel</button>
           </div>
         </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="InfoModel11" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Taxes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-3">
            <div class="form-group">
              <lable class="from_one1">Main Heads</lable>
              <select class="form-control show-tick select_form1" name="" id="">
                <option value="">Select Head</option>
                <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                 <option value="<?php echo $count; ?>">Head- <?php echo $count; ?></option>
               <?php $count++; } ?>
              </select>
           </div></div>
           <div class="col-lg-3">
          <div class="form-group">
              <lable class="from_one1">Sub Heads</lable>
              <select class="form-control show-tick select_form1" name="" id="">
                <option value="">Select Subhead</option>
                <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                 <option value="<?php echo $count; ?>">Subhead- <?php echo $count; ?></option>
               <?php $count++; } ?>
              </select>
           </div></div>
           <div class="col-lg-3">
          <div class="form-group">
              <lable class="from_one1">Group</lable>
              <select class="form-control show-tick select_form1" name="" id="">
                <option value="">Select Group</option>
                <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                 <option value="<?php echo $count; ?>">Group- <?php echo $count; ?></option>
               <?php $count++; } ?>
              </select>
           </div></div>
           <div class="col-lg-3">
          <div class="form-group">
              <lable class="from_one1">Head</lable>
              <select class="form-control show-tick select_form1" name="" id="">
                <option value="">Select Head</option>
                <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                 <option value="<?php echo $count; ?>">Head- <?php echo $count; ?></option>
               <?php $count++; } ?>
              </select>
           </div></div>
        </div>
        <div class="row">
          <div class="col-lg-3">
            <div class="form-group">
              <lable class="from_one1">Quantity</lable>
              <input type="text" class="form-control" placeholder="Quantity">           
              </div>
          </div>
           <div class="col-lg-3">
            <div class="form-group">
              <lable class="from_one1">Rate</lable>
              <input type="text" class="form-control" placeholder="Rate">           
              </div>
          </div>
         </div>
         <hr>
         <div class="row">
           <div class="col-lg-1">
             <button type="submit" class="btn btn-raised btn-primary">Ok</button>
           </div>
           <div class="col-lg-1">  
             <button type="reset" class="btn btn-raised btn-primary" data-dismiss="modal">Cancel</button>
           </div>
         </div>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>








<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>