
<?php $__env->startSection('content'); ?>
<style type="text/css">
  .checkbox label, .radio label{
        line-height: 19px;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Apply Fees on RTE Head</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
           <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>">Apply Fees on RTE Head</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection/rte-collection/apply-fees-on-head">View</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
            
              <div class="body form-gap">
                  <div class="row tabless" >
                    <div class="col-lg-8"></div>
                     <div class="col-lg-2 paddingtopclass"><b>Student Applied : </b> 2598</div>
                    <div class="col-lg-2 paddingtopclass "><b>Remaining: </b> 258956</div>
                    <div class="clearfix"></div>
                     <form class="" action="" method="post" id="searchpanel" style="width: 100%;">
                    <div class="row">
                    <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                    <div class="col-lg-3">
                      <div class="form-group">
                        <lable class="from_one1" for="name">Heads Name</lable>
                        <select class="form-control show-tick select_form1" name="classes" id="">
                          <option value="">Select Name</option>
                          <option value="1">Vijay Rathore</option>
                          <option value="2">Vijay Rathore</option>
                          <option value="3">Vijay Rathore</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <lable class="from_one1" for="name">Class</lable>
                        <select class="form-control show-tick select_form1" name="classes" id="">
                          <option value="">Select Class</option>
                          <option value="1">1st</option>
                          <option value="2">2nd</option>
                          <option value="3">3rd</option>
                          <option value="4">4th</option>
                          <option value="10th"> 10th</option>
                          <option value="11th">11th</option>
                          <option value="12th">12th</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <lable class="from_one1" for="name">Section</lable>
                        <select class="form-control show-tick select_form1" name="classes" id="">
                          <option value="">Select Section</option>
                          <option value="A">A</option>
                          <option value="B">B</option>
                          <option value="C">C</option>
                          <option value="D">D</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Save">Save
                      </button>
                    </div>
                     <div class="col-lg-1 ">
                      <button type="reset" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Clear">Clear
                      </button>
                    </div>
                  </div>
                </form>
                   
                    <!--  DataTable for view Records  -->
                    <table class="table m-b-0 c_list" id="tablche" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Enroll No</th>
                          <th>Student Name</th>
                          <th class="text-center" >Father Name</th>
                       </tr>
                      </thead>
                      <tbody>
                        <?php $count = 0; for ($i=0; $i < 10 ; $i++) {  ?>
                        
                        <tr>
                          <td >
                            <div class="checkbox">
                               <input id="checkbox<?php echo  $count; ?>" type="checkbox">
                               <label class="from_one1" for="checkbox<?php echo  $count; ?>" style= "margin-bottom: 4px !important;"></label>
                            </div>
                          </td>
                          <td>ABC20156</td>
                          <td width="20px" >
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                           Ankit Dave
                        </td>
                          <td  class="text-center">Akash Dave</td>
                        </tr>
                       <?php  $count++; } ?>
                      </tbody>
                    </table>
                    <div class="col-lg-12">
                       <button type="button" class="btn btn-primary float-right" title="Apply Fees Head">Apply Fees Head</button>
                    </div>
                  </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>