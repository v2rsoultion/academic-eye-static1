<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\TimeTable\TimeTable; // Model 
use Yajra\Datatables\Datatables;

class TimeTableController extends Controller
{
    /**
     *  show set time table data
     *  @Khushbu on 06 Sept 2018
    **/
    public function showTimeTable() {
        $time_table           = [];
        $loginInfo            = get_loggedin_user_data();
        $data                 = array(
            'page_title'      => trans('language.set_time_table'),
            'redirect_url'    => url('admin-panel/time-table/set-time-table'),
            'login_info'      => $loginInfo,
            'time_table'      => $time_table
        );
        return view('admin-panel.time-table.set-time-table')->with($data);
    }
    /**
     *  View page for time table
     *  @Khushbu on 06 Sept 2018
    **/
    public function index()
    {
        $time_table               = [];
        $loginInfo                = get_loggedin_user_data();
        $arr_class                = [];
        $arr_medium               = \Config::get('custom.medium_type');
        $arr_week_days            = \Config::get('custom.week_days');
        $time_table['arr_medium'] = add_blank_option($arr_medium, 'Select Medium');
        $time_table['arr_class']  = add_blank_option($arr_class, 'Select Class');
        $data                     = array(
            'page_title'          => trans('language.view_time_table'),
            'redirect_url'        => url('admin-panel/time-table/view-time-table'),
            'login_info'          => $loginInfo,
            'time_table'          => $time_table
        );
        return view('admin-panel.time-table.index')->with($data);
    }
    /**
     *  Add page for time table
     *  @Khushbu on 06 Sept 2018
    **/
    public function add(Request $request, $id = null) 
    {
    	$data    		= [];
        $time_table	    = [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_time_table_id 	= get_decrypted_value($id, true);
            $time_table                 = $this->getTimeTableData($decrypted_time_table_id,"");
            $time_table                 = isset($time_table[0]) ? $time_table[0] : [];
            if (!$time_table)
            {
                return redirect('admin-panel/time-table/add-time-table')->withError('Time table not found!');
            }
            $page_title               = trans('language.edit_time_table');
            $encrypted_time_table_id  = get_encrypted_value($time_table['time_table_id'], true);
            $save_url                 = url('admin-panel/time-table/save/' . $encrypted_time_table_id);
            $submit_button            = 'Update';
            $arr_class                = get_all_classes($time_table['medium_type']);
            $time_table['arr_class']  = add_blank_option($arr_class, 'Select Class');
            $arr_section              = get_class_section($time_table['class_id']);
            $time_table['arr_section']= add_blank_option($arr_section, 'Select Section');
            $week_days                = [];
            $time_table['week_days']  =TimeTable::find($decrypted_time_table_id); 
            $week                     = [];
            $week_key                 = $time_table['week_days'];
            $time_table['week']       = explode(", ", $week_key['time_table_week_days']);
        }
        else
        {
            $arr_class                 = [];
            $time_table['arr_class']   = add_blank_option($arr_class, 'Select Class');
            $arr_section               = [];
            $time_table['arr_section'] = add_blank_option($arr_section, 'Select Section');
            $page_title                = trans('language.add_time_table');
            $save_url                  = url('admin-panel/time-table/save');
            $submit_button             = 'Save';
        }
        $arr_medium                    = \Config::get('custom.medium_type');
        $time_table['arr_medium']      = add_blank_option($arr_medium, 'Select Medium');
        $arr_week_days                 = \Config::get('custom.week_days');
        $time_table['arr_week_days']   = $arr_week_days;
        // $time_table['arr_week_days']   = add_blank_option($arr_week_days, 'Nothing selected');
        $data                          = array(
            'page_title'    	       => $page_title,
            'save_url'      	       => $save_url,
            'submit_button' 	       => $submit_button,
            'time_table'               => $time_table,
            'login_info'    	       => $loginInfo,
            'redirect_url'  	       => url('admin-panel/time-table/view-time-table'),
        );
        return view('admin-panel.time-table.add')->with($data);
    }
    /**
     *  Add and update time-table's data
     *  @Khushbu on 06 Sept 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo               = get_loggedin_user_data();
        $admin_id                = $loginInfo['admin_id'];
        $decrypted_time_table_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $time_table = TimeTable::find($decrypted_time_table_id);
            $admin_id   = $time_table['admin_id'];
            if (!$time_table)
            {
                return redirect('admin-panel/time-table/view-time-table/')->withError('Time table not found!');
            }
            $success_msg = 'Time table updated successfully!';
        }
        else
        { 
            $time_table      =  New TimeTable;
            $success_msg     = 'Time table saved successfully!';
        }
        
        $class_id       = null;
        $section_id     = null;
        if ($request->has('class_id'))
        {
            $class_id   = Input::get('class_id');
            $section_id = Input::get('section_id');
        }
        if ($request->has('section_id'))
        {
            $class_id   = Input::get('class_id');
            $section_id = Input::get('section_id');
        }
        $validatior   = Validator::make($request->all(), [
                'name'        => 'required|unique:time_tables,time_table_name,' . $decrypted_time_table_id . ',time_table_id,class_id,' . $class_id . ',section_id,' . $section_id,
                'class_id'    => 'required|unique:time_tables,class_id,' . $decrypted_time_table_id . ',time_table_id,class_id,' . $class_id . ',section_id,' . $section_id,
                'section_id'  => 'required',
                'medium_type' => 'required',
                'week_days'   => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $time_table->admin_id            = $admin_id;
                $time_table->update_by           = $loginInfo['admin_id'];
                $time_table->time_table_name     = Input::get('name');
                $time_table->medium_type         = Input::get('medium_type');
                $time_table->class_id            = Input::get('class_id');
                $time_table->section_id          = Input::get('section_id');
                $week_days1                      =  Input::get('week_days');
                $week_day                        = implode(", ", $week_days1);
                $time_table->time_table_week_days= $week_day;
                $time_table->save();
            }
            catch (\Exception $e)
            {       
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/time-table/view-time-table')->withSuccess($success_msg);
    }
    /**
     *  Get Data for view page(Datatables)
     *  @Khushbu on 07 Sept 2018
    **/
    public function anyData(Request $request)
    {
        $time_table     = [];
        $arr_time_table = $this->getTimeTableData("",$request);
        foreach ($arr_time_table as $key => $time_table_data)
        {
            $time_table[$key] = (object) $time_table_data;
        }
        return Datatables::of($time_table)       
            ->addColumn('action', function ($time_table)
            {
                $encrypted_time_table_id = get_encrypted_value($time_table->time_table_id, true);
                if($time_table->time_table_status == 0) {
                    $status    = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status    = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                        <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="time-table-status/'.$status.'/' . $encrypted_time_table_id . '">'.$statusVal.'</a></div>
                        <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-time-table/' . $encrypted_time_table_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                        <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-time-table/' . $encrypted_time_table_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }
    /**
     *  Get Time Table data
     *  @Khushbu on 07 Sept 2018
    **/
    public function getTimeTableData($time_table_id = array(),$request)
    {
        $loginInfo                 = get_loggedin_user_data();
        $time_table_return         = [];
        $time_table                = [];
        $arr_class                 = [];
        $arr_medium                = \Config::get('custom.medium_type');
        $arr_week_days             = \Config::get('custom.week_days');
        $time_table['arr_class']   = $arr_class;
        $arr_time_table_data       = TimeTable::where(function($query) use ($time_table_id,$loginInfo,$request) 
            {
                if (!empty($time_table_id))
                {
                    $query->where('time_table_id', $time_table_id);
                }
                if (!empty($request) && !empty($request->get('name')))
                {
                    $query->where('time_table_name', "like", "%{$request->get('name')}%");
                }
                if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null)
                {
                    $query->where('medium_type', "=", $request->get('medium_type'));
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
                {
                    $query->where('class_id', "=", $request->get('class_id'));
                }
                if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null)
                {
                    $query->where('section_id', "=", $request->get('section_id'));
                }
            })->with('timeTableClass', 'timeTableSection')->get();

        if (!empty($arr_time_table_data))
        {
            foreach ($arr_time_table_data as $key => $time_table_data)
            {       
                $week= explode(", ", $time_table_data['time_table_week_days']);
                $week_days= '';
                foreach($week as $key) 
                {
                    $week_days .= $arr_week_days[$key].',';
                }
                $time_table = array(
                    'time_table_id'     => $time_table_data['time_table_id'],
                    'time_table_name'   => $time_table_data['time_table_name'],
                    'medium_type'       => $time_table_data['medium_type'],
                    'medium_type1'      => $arr_medium[$time_table_data['medium_type']],
                    'class_id'          => $time_table_data['class_id'],
                    'section_id'        => $time_table_data['section_id'],
                    'week_days1'        =>  rtrim($week_days, ','),
                    'time_table_status' => $time_table_data['time_table_status'],       
                );
                if (isset($time_table_data['timeTableClass']['class_name']))
                {
                    $time_table['class_name'] = $time_table_data['timeTableClass']['class_name'];
                }
                if (isset($time_table_data['timeTableSection']['section_name']))
                {
                    $time_table['section_name'] = $time_table_data['timeTableSection']['section_name'];
                }
                $time_table_return[] = $time_table;
            }
        }
        return $time_table_return;
    }
    /**
     *  Destroy time-table data
     *  @Khushbu on 07 Sept 2018
    **/
    public function destroy($id)
    {
        $time_table_id = get_decrypted_value($id, true);
        $time_table    = TimeTable::find($time_table_id);
        if ($time_table)
        {
            $time_table->delete();
            $success_msg = "Time table deleted successfully!";
            return redirect('admin-panel/time-table/view-time-table')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Time table not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Time-table's status
     *  @Khushbu on 07 Sept 2018
    **/
    public function changeStatus($status,$id)
    {
        $time_table_id = get_decrypted_value($id, true);
        $time_table    = TimeTable::find($time_table_id);
        if ($time_table)
        {
            $time_table->time_table_status  = $status;
            $time_table->save();
            $success_msg = "Time table status updated successfully!";
            return redirect('admin-panel/time-table/view-time-table')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Time table not found!";
            return redirect('admin-panel/time-table/view-time-table')->withErrors($error_message);
        }
    }
}
