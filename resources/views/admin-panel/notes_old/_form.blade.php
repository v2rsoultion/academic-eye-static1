@if(isset($notes['online_note_id']) && !empty($notes['online_note_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

@if(!isset($student['notes_class_id']) && empty($student['notes_class_id']))
<?php $admit_section_readonly = false; $admit_section_disabled='disabled'; ?>
@else
<?php $admit_section_readonly = false; $admit_section_disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
</style>

{!! Form::hidden('online_note_id',old('online_note_id',isset($notes['online_note_id']) ? $notes['online_note_id'] : ''),['class' => 'gui-input', 'id' => 'notes-form', 'readonly' => 'true']) !!}

<!-- Basic Info notes -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('name', old('name',isset($notes['online_note_name']) ? $notes['online_note_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.name'), 'id' => 'name']) !!}
        </div>
        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
    </div>

    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.class') !!} :</lable>
        <label class=" field select" style="width: 100%">
            {!!Form::select('notes_class_id', $notes['arr_class'],isset($notes['class_id']) ? $notes['class_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'notes_class_id','onChange' => 'getSection(this.value,"1")'])!!}
            <i class="arrow double"></i>
        </label>
        @if ($errors->has('notes_class_id')) <p class="help-block">{{ $errors->first('notes_class_id') }}</p> @endif
    </div>

    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.section') !!}:</lable>
        <label class=" field select" style="width: 100%" >
            {!!Form::select('notes_section_id', $notes['arr_section'],isset($notes['section_id']) ? $notes['section_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'notes_section_id',$admit_section_disabled])!!}
            <i class="arrow double"></i>
        </label>
        @if ($errors->has('notes_section_id')) <p class="help-block">{{ $errors->first('notes_section_id') }}</p> @endif
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.subject') !!}:</lable>
        <label class=" field select" style="width: 100%" >
            {!!Form::select('notes_subject_id', $notes['arr_subject'],isset($notes['subject_id']) ? $notes['subject_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'notes_subject_id'])!!}
            <i class="arrow double"></i>
        </label>
        @if ($errors->has('notes_subject_id')) <p class="help-block">{{ $errors->first('notes_subject_id') }}</p> @endif
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.unit') !!} :</lable>
        <div class="form-group">
            {!! Form::text('unit', old('unit',isset($notes['online_note_unit']) ? $notes['online_note_unit']: ''), ['class' => 'form-control','placeholder'=>trans('language.name'), 'id' => 'unit']) !!}
        </div>
        @if ($errors->has('unit')) <p class="help-block">{{ $errors->first('unit') }}</p> @endif
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.topic') !!} :</lable>
        <div class="form-group">
            {!! Form::text('topic', old('unit',isset($notes['online_note_topic']) ? $notes['online_note_topic']: ''), ['class' => 'form-control','placeholder'=>trans('language.name'), 'id' => 'topic']) !!}
        </div>
        @if ($errors->has('topic')) <p class="help-block">{{ $errors->first('topic') }}</p> @endif
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#notes-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                name: {
                    required: true
                },
                notes_class_id: {
                    required: true
                },
                notes_section_id: {
                    required: true
                },
                notes_subject_id: {
                    required: true
                },
                unit: {
                    required: true
                },
                topic: {
                    required: true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>

<script type="text/javascript">


function getSection(class_id,type)
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/notes/get-section-data')}}",
            type: 'GET',
            data: {
                'class_id': class_id
            },
            success: function (data) {
                if(type == 1) {
                    $("select[name='notes_section_id'").html('');
                    $("select[name='notes_section_id'").html(data.options);
                    $("select[name='notes_section_id'").removeAttr("disabled");
                    $("select[name='notes_section_id'").selectpicker('refresh');

                }
                //  else if(type == 2){
                //     $("select[name='current_section_id'").html('');
                //     $("select[name='current_section_id'").html(data.options);
                //     $("select[name='current_section_id'").removeAttr("disabled");
                //     $("select[name='current_section_id'").selectpicker('refresh');
                // }
            }
        });
    }


</script>