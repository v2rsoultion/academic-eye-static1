<?php $__env->startSection('content'); ?>
<style type="text/css">
    .table-responsive {
        overflow-x: visible !important;
    }
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2><?php echo trans('language.view_student_list'); ?></h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <a href="<?php echo url('admin-panel/student/add-student'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/student'); ?>"><<?php echo trans('language.menu_student'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/student'); ?>"><?php echo trans('language.student'); ?></a></li>
                    <li class="breadcrumb-item active"><a href=""><?php echo trans('language.view_student_list'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                <?php if(session()->has('success')): ?>
                                    <p class="green">
                                        <?php echo e(session()->get('success')); ?>

                                    </p>
                                <?php endif; ?>
                                <?php if($errors->any()): ?>
                                    <p class="red"><?php echo e($errors->first()); ?></p>
                                <?php endif; ?>
                                <?php echo Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']); ?>

                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                <?php echo Form::select('medium_type', $listData['arr_medium'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type', 'onChange'=>'getClass(this.value)']); ?>

                                                <i class="arrow double"></i>
                                            </label>
                                            <?php if($errors->has('medium_type')): ?> <p class="help-block"><?php echo e($errors->first('medium_type')); ?></p> <?php endif; ?>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                <?php echo Form::select('class_id', $listData['arr_class'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)']); ?>

                                                <i class="arrow double"></i>
                                            </label>
                                            <?php if($errors->has('class_id')): ?> <p class="help-block"><?php echo e($errors->first('class_id')); ?></p> <?php endif; ?>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                <?php echo Form::select('section_id', $listData['arr_section'], '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','disabled']); ?>

                                                <i class="arrow double"></i>
                                            </label>
                                            <?php if($errors->has('section_id')): ?> <p class="help-block"><?php echo e($errors->first('section_id')); ?></p> <?php endif; ?>
                                        </div>
                                       <div class="col-lg-1 ">
                                          <button type="submit" class="btn btn-raised btn-primary"  title="Search">Search
                                          </button>
                                        </div>
                                         <div class="col-lg-1 ">
                                          <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                                          </button>
                                        </div>
                                    </div>
                                <?php echo Form::close(); ?>

                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th><?php echo e(trans('language.s_no')); ?></th>
                                                <th><?php echo e(trans('language.class_name')); ?></th>
                                                <th><?php echo e(trans('language.section_name')); ?></th>
                                                <th><?php echo e(trans('language.medium_type')); ?></th>
                                                <th><?php echo e(trans('language.total_students')); ?></th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            <td>1</td>
                                            <td>10th</td>
                                            <td>A</td>
                                            <td>Hindi</td>
                                            <td>30</td>
                                            <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="Approved" href="<?php echo e(url('admin-panel/student/student-profile')); ?>">View Profile</a></li> 
                                                </ul> </div></td>
                                                </tr> 
                                                <tr>
                                            <td>2</td>
                                            <td>10th</td>
                                            <td>A</td>
                                            <td>Hindi</td>
                                            <td>30</td>
                                            <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="Approved" href="<?php echo e(url('admin-panel/student/student-profile')); ?>">View Profile</a></li> 
                                                </ul> </div></td>
                                                </tr> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#student-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '<?php echo e(url('admin-panel/student/list-data')); ?>',
                data: function (d) {
                    d.class_id = $('select[name="class_id"]').val();
                    d.section_id = $('select[name="section_id"]').val();
                }
            },
            //ajax: '<?php echo e(url('admin-panel/class/data')); ?>',
           
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'class_name', name: 'class_name'},
                {data: 'section_name', name: 'section_name'},
                {data: 'medium_type', name: 'medium_type'},
                {data: 'total_students', name: 'total_students'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 4, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
           location.reload();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/class/get-class-data')); ?>",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='class_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='class_id'").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }
    function getSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/student/get-section-data')); ?>",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $("select[name='section_id'").removeAttr("disabled");
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='class_id'").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }

</script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>