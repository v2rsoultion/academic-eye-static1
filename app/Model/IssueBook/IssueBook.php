<?php

namespace App\Model\IssueBook;

use Illuminate\Database\Eloquent\Model;

class IssueBook extends Model
{
    protected $table      = 'issued_books';
    protected $primaryKey = 'issued_book_id';
}
