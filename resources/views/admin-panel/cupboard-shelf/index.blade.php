@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_cupboard_shelf') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.cupboard_shelf') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_cupboard_shelf') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">{!! trans('language.title') !!}</a></li>
                                    <li class="nav-item"><a class="nav-link"  href="{{ url('admin-panel/title/add-title') }}">{!! trans('language.add_title') !!}</a></li>
                                </ul>                        
                            </div> -->
                            <div class="body">
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="cupboard-shelf-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.cupboard_shelf_name')}}</th>
                                            <th>{{trans('language.cupboard_shelf_cup')}}</th>
                                            <th>{{trans('language.cupboard_shelf_capacity')}}</th>
                                            <th>{{trans('language.cupboard_shelf_detail')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#cupboard-shelf-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: "{{url('admin-panel/cupboard-shelf/data')}}",
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'book_cupboardshelf_name', name: 'book_cupboardshelf_name'},
                {data: 'cupboard_name', name: 'cupboard_name'},
                {data: 'book_cupboard_capacity', name: 'book_cupboard_capacity'},
                {data: 'details', name: 'details'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 5, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });


</script>
@endsection




