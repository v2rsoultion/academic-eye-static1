@if(isset($city['city_id']) && !empty($city['city_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('city_id',old('city_id',isset($city['city_id']) ? $city['city_id'] : ''),['class' => 'gui-input', 'id' => 'city_id', 'readonly' => 'true']) !!}

@if ($errors->any())
    <div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.country_name') !!} :</lable>
        <label class=" field select" style="width: 100%">
            {!!Form::select('country_id', $city['arr_country'],isset($city['country_id']) ? $city['country_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'country_id', 'onChange'=>'getCountryState(this.value)'])!!}
            <i class="arrow double"></i>
        </label>
        @if($errors->has('country_id')) <p class="help-block">{{ $errors->first('country_id') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.state_name') !!} :</lable>
        <label class=" field select" style="width: 100%">
            {!!Form::select('state_id', $city['arr_state'],isset($city['state_id']) ? $city['state_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'state_id'])!!}
            <i class="arrow double"></i>
        </label>
        @if($errors->has('state_id')) <p class="help-block">{{ $errors->first('state_id') }}</p> @endif
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.city_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('city_name', old('city_name',isset($city['city_name']) ? $city['city_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.city_name'), 'id' => 'city_name']) !!}
        </div>
        @if ($errors->has('city_name')) <p class="help-block">{{ $errors->first('city_name') }}</p> @endif
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#city-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            // errorLabelContainer: '.errorTxt',

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                country_id: {
                    required: true
                },
                state_id: {
                    required: true
                },
                city_name: {
                    required: true,
                    lettersonly:true
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });  
    });

    function getCountryState(country_id)
    {
        if(country_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/state/get-country-state-data')}}",
                type: 'GET',
                data: {
                    'country_id': country_id
                },
                success: function (data) {
                    $("select[name='state_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='state_id'").html('<option value="">Select State</option>');
            $(".mycustloading").hide();
        }
    }
</script>