<?php $__env->startSection('content'); ?>
<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2><?php echo trans('language.add_staff_attendance'); ?></h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <a href="<?php echo url('admin-panel/staff/staff-attendance/view-staff-attendance'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-eye"></i> </a>
                <ul class="breadcrumb float-md-right ">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/staff'); ?>"><?php echo trans('language.menu_staff'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/staff'); ?>"><?php echo trans('language.staff_attendance'); ?></a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/staff/staff-attendance/add-staff-attendance'); ?>"><?php echo trans('language.add_staff_attendance'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <!-- <div class="header"> -->
                        <!-- <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2> -->
                    <!-- </div> -->
                    <div class="body">
                    <?php echo Form::open(['files'=>TRUE,'id' => 'staff-attendance-form' , 'class'=>'form-horizontal','url' =>$save_url]); ?>

                    <?php echo $__env->make('admin-panel.staff-attendance._form',['submit_button' => $submit_button], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo Form::close(); ?>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>