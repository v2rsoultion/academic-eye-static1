
<?php $__env->startSection('content'); ?>
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
  .table-responsive {
    overflow-x: visible !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Aman</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/parent/view-profile'); ?>">My Profile</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/parent/children-details'); ?>">Children Details</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                 
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="<?php echo URL::to('public/assets/images/Student/Attendence.svg'); ?>" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Attendence</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                           
                            <a href="<?php echo e(url('admin-panel/parent/children-details/view-attendance')); ?>" class="cusa" title="View " style="width: 48%; margin: 15px auto;">
                              <i class="fas fa-eye"></i>View 
                            </a>
                            <div class="clearfix"></div>
                            
                        <div class="row clearfix"></div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="<?php echo URL::to('public/assets/images/Student/LeaveManagement.svg'); ?>" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Leave Management</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="<?php echo e(url('admin-panel/parent/children-details/add-leave-application')); ?>" class="float-left" title="Add ">
                                <i class="fas fa-plus"></i> Add 
                              </a>
                              <a href="<?php echo e(url('admin-panel/parent/children-details/view-leave-application')); ?>" class="float-right" title="View" style="width: 48%; margin: 15px auto;">
                                <i class="fas fa-eye"></i>View 
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                            <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/Student/remarks.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Remarks</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <!-- <a href="<?php echo e(url('admin-panel/staff-menu/my-class/remark/add-remark')); ?>" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a> -->
                                <a href="<?php echo e(url('admin-panel/parent/children-details/view-remarks')); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                              <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/Student/homework.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Home Work</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                               <!--  <a href="<?php echo e(url('admin-panel/staff-menu/my-class/homework/add-homework')); ?>" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a> -->
                                <a href="<?php echo e(url('admin-panel/parent/children-details/view-homework')); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                        </div>
                        <div class="row">
                         
                            <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Academic/TimeTable.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Time Table</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <!-- <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="<?php echo e(url('admin-panel/parent/children-details/view-time-table')); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                            </div>
                             <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Academic/TimeTable.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Vehicle Tracking</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <!-- <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="<?php echo e(url('admin-panel/parent/children-details/view-time-table')); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                               <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Academic/TimeTable.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Online Content</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <!-- <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="<?php echo e(url('admin-panel/parent/children-details/view-time-table')); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              
                                </div>
                                
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>