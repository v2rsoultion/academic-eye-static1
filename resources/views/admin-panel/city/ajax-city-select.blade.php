<option value="">Select City</option>
@if(!empty($city))
  @foreach($city as $key => $value)
    <option value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif