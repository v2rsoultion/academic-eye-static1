<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Model\Staff\Staff;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class StaffController extends Controller
{
    /**
     *  View page for staff
     *  @Shree on 3 Aug 2018
    **/
   
    public function index()
    {
        $loginInfo                      = get_loggedin_user_data();
        $arr_designation                = get_all_designations();
        $listData                       = [];
        $listData['arr_designation']    = add_blank_option($arr_designation, 'Select Designation');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/staff/view-staff'),
            'page_title'    => trans('language.view_staff'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.staff.index')->with($data);
    }

     /**
     *  Get Data for view staff page(Datatables)
     *  @Shree on 3 Aug 2018
    **/
    public function anyData(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $staff  	= Staff::where([['school_id', '=', $loginInfo['school_id']]])->orderBy('staff_id', 'DESC')->get();

        return Datatables::of($staff)
            ->addColumn('action', function ($staff)
            {
                $encrypted_staff_id = get_encrypted_value($staff->staff_id, true);
                if($staff->staff_status == 0) {
                    $status = 1;
                    $statusVal = "Deactive";
                } else {
                    $status = 0;
                    $statusVal = "Active";
                }
                return ' <div class="dropdown" >
                    <button class="btn btn-primary dropdown-toggle custom_btn" type="button" data-toggle="dropdown">Action
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="view-staff-profile/' . $encrypted_staff_id . '" ">View Profile</a></li>
                        <li><a href="add-staff/' . $encrypted_staff_id . '" ">Edit</a></li>
                        <li><a href="delete-staff/' . $encrypted_staff_id . '" onclick="return confirm('."'Are you sure?'".')" >Delete</a></li>
                        <li><a href="staff-status/'.$status.'/' . $encrypted_staff_id . '">'.$statusVal.'</a></li>
                    </ul>
                </div>';
               
                
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

     /**
     *  Add page for staff
     *  @Shree on 3 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $staff  = [];
        $data     = [];

        $loginInfo  = get_loggedin_user_data();
        
        if (!empty($id))
        {
            $decrypted_staff_id = get_decrypted_value($id, true);
            $staff              = get_student_signle_data($decrypted_staff_id);
            $staff              = isset($staff[0]) ? $staff[0] : [];
            if (!$staff)
            {
                return redirect('admin-panel/staff/add-staff')->withError('Staff not found!');
            }
            $encrypted_staff_id = get_encrypted_value($staff['staff_id'], true);
            $page_title         = trans('language.edit_staff');
            $save_url           = url('admin-panel/staff/save/' . $encrypted_student_id);
            $submit_button      = 'Update';
            $arr_country                        = get_all_country();
            $staff['arr_country']               = add_blank_option($arr_country, 'Select Country');
            $arr_state                          = get_all_states($staff['country_id']);
            $staff['arr_state']                 = add_blank_option($arr_state, 'Select State');
            $arr_city                           = get_all_city('state_id');
            $staff['arr_city']                  = add_blank_option($arr_city, 'Select City');
        }

        else
        {
            $page_title    = trans('language.add_staff');
            $save_url      = url('admin-panel/staff/save');
            $submit_button = 'Save';
            $arr_country                        = get_all_country();
        $staff['arr_country']               = add_blank_option($arr_country, 'Select Country');
        $arr_state                          = [];
        $staff['arr_state']                 = add_blank_option($arr_state, 'Select State');
        $arr_city                           = [];
        $staff['arr_city']                  = add_blank_option($arr_city, 'Select City');
        }
        $student['arr_session'] = [];
        $staff_gender             = \Config::get('custom.staff_gender');
        $arr_marital              = \Config::get('custom.staff_marital');

        $staff['arr_gender']        = $staff_gender;
        // p($staff_gender);
        $staff['arr_marital']       = $arr_marital;
        $arr_caste                     = get_caste();
        $arr_religion                  = get_religion();
        $arr_natioanlity               = get_nationality();
        $arr_staff_roles               = get_all_staff_roles();
        $arr_designations              = get_all_designations();
        $arr_document_category         = get_all_document_category(2);
      
        $staff['arr_caste']                 = add_blank_option($arr_caste, 'Select caste');
        $staff['arr_religion']              = add_blank_option($arr_religion, 'Select religion');
        $staff['arr_staff_roles']           = add_blank_option($arr_staff_roles, 'Select staff role');
        $staff['arr_designations']          = add_blank_option($arr_designations, 'Select designation');
        $staff['arr_natioanlity']           = add_blank_option($arr_natioanlity, 'Select nationality');
        $staff['arr_document_category']     = add_blank_option($arr_document_category, 'Document category');
        
        if (!empty($staff['staff_profile_img']))
        {
            $profile = check_file_exist($staff['staff_profile_img'], 'staff_profile');
            if (!empty($profile))
            {
                $staff['staff_profile'] = $profile;
            }
        }
        // p($staff['arr_state']);
        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'staff'         => $staff,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/staff/view-staff'),
        );
        return view('admin-panel.staff.add')->with($data);
    }

     /**
     *  view profile page for staff
     *  @Khushbu on 20 Sept 2018
    **/
    public function viewProfile() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
         return view('admin-panel.staff.profile')->with($data);
    }

    // public function save() {
    //     $staff = [];
    //     $staff['designation_id'] = '';
    //     $staff['arr_designations '] = "";
    //     $loginInfo   = get_loggedin_user_data();
    //     $data                 = array(
    //         'login_info'      => $loginInfo,
    //         'staff'             => $staff,
    //     );
    //      return view('admin-panel.staff._form')->with($data);
    // }
}
