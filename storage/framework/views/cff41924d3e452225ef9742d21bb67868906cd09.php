<?php $__env->startSection('content'); ?>
<style type="text/css">
    td {
        padding: 10px !important;
    }
</style>
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h2><?php echo trans('language.view_notes'); ?></h2>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/parent/view-profile'); ?>">My Profile</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/parent/children-details'); ?>">Online Content</a></li>
                    <li class="breadcrumb-item "><a href="<?php echo e(url('admin-panel/parent/children-details/online-content')); ?>"><?php echo trans('language.view_notes'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                <?php echo Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']); ?>

                                    <div class="row clearfix">                                     
                                       
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                <select class="form-control show-tick select_form1" id="subject_id">
                                                    <option value="">Select Subject</option>
                                                    <option value="1">Hindi</option>
                                                    <option value="2">English</option>
                                                    <option value="3">Computer </option>
                                                </select>
                                               
                                                <i class="arrow double"></i>
                                            </label>
                                            <?php if($errors->has('subject_id')): ?> <p class="help-block"><?php echo e($errors->first('subject_id')); ?></p> <?php endif; ?>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <?php echo Form::submit('Search', ['class' => 'btn btn-raised  btn-primary ','name'=>'Search']); ?>

                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <?php echo Form::button('Clear', ['class' => 'btn btn-raised  btn-primary ','name'=>'Clear', 'id' => "clearBtn"]); ?>

                                        </div>
                                    </div>
                                <?php echo Form::close(); ?>

                                <hr>
                                <div class="">
                                <table class="table m-b-0 c_list" id="" style="width:100%">
                                <?php echo e(csrf_field()); ?>

                                    <thead>
                                        <tr>
                                            <th><?php echo e(trans('language.s_no')); ?></th>
                                            
                                            <th><?php echo e(trans('language.subject')); ?></th>
                                            <th><?php echo e(trans('language.notes_unit')); ?></th>
                                            <th><?php echo e(trans('language.notes_topic')); ?></th>
                                            <th>Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Hindi</td>
                                            <td>Unit-1</td>
                                            <td>Chapter</td>
                                            <td>
                                                <a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" title="View" class="text-primary" style="color: #0275d8!important;" target="_blank">View </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                
                                            <td>Computer</td>
                                            <td>Unit-5</td>
                                            <td>Hardware</td>
                                            <td>
                                                <a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" title="View" class="text-primary" style="color: #0275d8!important;" target="_blank">View </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#notes-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
        
            ajax: {
                url: '<?php echo e(url('admin-panel/notes/data')); ?>',
                data: function (d) {
                    d.class_id   = $('select[name=class_id]').find(':selected').val();
                    d.subject_id = $('select[name=subject_id]').find(':selected').val();
                    // d.class_id = $('select[name="class_id"]').val();
                }
            },
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'online_note_name', name: 'online_note_name'},
                {data: 'class_section', name: 'class_section'},
                {data: 'class_subject', name: 'class_subject'},
                {data: 'online_note_unit', name: 'online_note_unit'},
                {data: 'online_note_topic', name: 'online_note_topic'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    targets: [ 0, 1, 2, 3, 4, 5, 6],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            $("select[name='class_id'").selectpicker('refresh');
            $("select[name='subject_id'").selectpicker('refresh');
            table.draw();
            e.preventDefault();
        })
    });


</script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>