@if(isset($brochure['brochure_id']) && !empty($brochure['brochure_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('brochure_id',old('brochure_id',isset($brochure['brochure_id']) ? $brochure['brochure_id'] : ''),['class' => 'gui-input', 'id' => 'brochure_id', 'readonly' => 'true']) !!}

<p class="red">
@if ($errors->any())
    {{$errors->first()}}
@endif
</p>
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.session_name') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('session_id', $brochure['arr_session'],old('session_id',isset($brochure['session_id']) ? $brochure['session_id'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'session_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.brochure_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('brochure_name', old('brochure_name',isset($brochure['brochure_name']) ? $brochure['brochure_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.brochure_name'), 'id' => 'brochure_name']) !!}
        </div>
        @if ($errors->has('brochure_name')) <p class="help-block">{{ $errors->first('brochure_name') }}</p> @endif
    </div>
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.brochure_file') !!} :</lable>
        <div class="form-group">
            <label for="file1" class="field file">
                <input type="file" class="gui-file" name="brochure_file" id="brochure_file" >
                <input type="hidden" class="gui-input" id="brochure_file" placeholder="Upload File" readonly>
            </label>
        </div>
        {!! Form::hidden('brochure_file1',old('brochure_file1',isset($brochure['brochure_file1']) ? $brochure['brochure_file1'] : ''),['class' => 'gui-input', 'id' => 'brochure_file1', 'readonly' => 'true']) !!}
        @if(isset($brochure['brochure_file1']))
            <a href="../../../{{$brochure['brochure_file1']}}" target="_blank">View Brochure File</a>
        @endif
        @if ($errors->has('brochure_file')) <p class="help-block">{{ $errors->first('brochure_file') }}</p> @endif
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("alphanumeric", function(value, element) {
            return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
        }, "Only alphanumeric characters");

        $("#brochure-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                session_id: {
                    required: true
                },
                brochure_name: {
                    required: true,
                    alphanumeric:true
                },
                brochure_file1: {
                    required: true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
        $('#holiday_end_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false });
        $('#holiday_start_date').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false}).on('change', function(e, date)
        {
        $('#holiday_end_date').bootstrapMaterialDatePicker('setMinDate', date);
        });
        
    });

    

</script>