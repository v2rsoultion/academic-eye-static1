
<?php $__env->startSection('content'); ?>
<style type="text/css">
  /*td{
    padding: 14px 10px !important;
  }*/
  .nav-tabs>.nav-item>.nav-link {
    width: 100px;
    margin-right: 5px;
        border: 1px solid #ccc !important;
  }
  #tabingclass .nav-tabs>.nav-item>.nav-link {
    border-radius: 4px !important;
    padding: 8px 25px;
  }
   #tabingclass .nav-link:hover {
        background: #6572b8 !important;
    color: #fff !important;
}
#tabingclass .nav-link:active {
        background: #6572b8 !important;
    color: #fff !important;
}
.idi .nav-tabs>.nav-item>.nav-link.active {
  background: #6572b8 !important;
    color: #fff !important;
}
.modal-dialog {
  max-width: 1130px !important;
}
.checkbox {
  float: left;
  margin-right: 20px;
}
.table-responsive {
  overflow-x: visible !important;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>HomeWork Group</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/student'); ?>">Student</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/student'); ?>">HomeWork Group</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/student/homework-group/view'); ?>">View</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 20px !important;">
                 
                     <form class="" action="" method=""  id="" style="width: 100%;">
                      <div class="row">
                      
                      <div class="col-lg-3">
                    <div class="form-group">
                      <!-- <lable class="from_one1" for="name">Class</lable> -->
                      <select class="form-control show-tick select_form1" name="classes" id="">
                        <option value="">Class</option>
                        <option value="1">Class-1</option>
                        <option value="2">Class-2</option>
                        <option value="3">Class-3</option>
                        <option value="4">Class-4</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <!-- <lable class="from_one1" for="name">Class</lable> -->
                      <select class="form-control show-tick select_form1" name="classes" id="">
                        <option value="">Section</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                      </select>
                    </div>
                  </div>
                  
                     <div class="col-lg-1">
                      <button type="submit" class="btn btn-raised btn-primary " title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Clear
                      </button>
                    </div>
                  
                    </div>
                   </form>
                <hr style="width: 100%">
                    <!--  DataTable for view Records  -->
                     <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                      <thead>
                        <tr>
                          <th>S No</th>
                          <th>Group Name</th>
                          <th>Class - Section</th>
                          <th>Details</th>
                          <th>Action</th>
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 10; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td>Group-<?php echo $counter; ?></td>
                          <td>Class-1 - A</td>
                          <td><a href="<?php echo e(url('admin-panel/student/homework-group/home-work-details')); ?>" class="btn btn-raised btn-primary">HomeWork Details</a></td>
                          <td><div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11" style="width: 170px !important;">
                                <li> <a title="Manage Students" href="<?php echo e(url('admin-panel/student/homework-group/manage-students')); ?>">Manage Students</a></li> 
                                <li> <a title="Manage Parents" href="<?php echo e(url('admin-panel/student/homework-group/manage-parents')); ?>">Manage Parents</a></li> 
                                <li> <a title="Manage Teachers" href="<?php echo e(url('admin-panel/student/homework-group/manage-teachers')); ?>">Manage Teachers</a></li> 
                            </ul> </div></td>
                           
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>

<!-- model -->
<div class="modal fade" id="homeWorkModel" tabindex="-1" role="dialog" style="padding: 40px;">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content send_messsage_button1">
            <div class="header header_model_2">
                <div class="header_model_1">
                    <h6 style="padding-left:20px;"><strong>Home-Work Details</strong></h6>
                </div>
                <ul class="header-dropdown" style="padding-right: 20px; ">
                    <li class="remove remove_cross_x_2 float-right" style="position: relative; top: 20px;">
                        <a role="button" class="button_1" data-dismiss="modal"><i class="zmdi zmdi-close zmdi_close1"></i></a>
                    </li>
                </ul>
            
            </div>
          <div class="tab-pane active" id="classlist">
              <div class="card " style="padding: 20px 10px;">
                  <div class="body">
                         <!-- <div class="table-responsive"> -->
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                      <thead>
                        <tr>
                          <th>S No</th>
                          <th>Date</th>
                          <th>Subject Name</th>
                          <th>Teacher Name</th>
                          <th>Home-Work</th>
                          
                          <!-- <th>Action</th> -->
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 6; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td>09-10-2018</td>
                          <td>Subject-<?php echo $counter; ?></td>
                          <td>Teacher-<?php echo $counter; ?></td>
                          <td style="width: 250px;">Lesson-1 Question Answer in written</td>
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  <!-- </div> -->
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>