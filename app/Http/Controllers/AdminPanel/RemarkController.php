<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RemarkController extends Controller
{
    public function addRemark()
    {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.remarks.add-remark')->with($data);
    }

    public function viewRemark()
    {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.remarks.view-remark')->with($data);
    }
}
