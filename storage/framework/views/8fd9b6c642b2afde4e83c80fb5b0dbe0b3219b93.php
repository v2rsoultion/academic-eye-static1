
<?php $__env->startSection('content'); ?>
<style type="text/css">
   .table-responsive {
    overflow-x: auto !important;
   }
   /*.card .body .table td, .cshelf1 {
    width: 100px !important;
   }*/
   td{
    padding: -1px !important;
   }
</style>
<!--  Main content here -->
<section class="content">
   <div class="block-header">
      <div class="row">
         <div class="col-lg-5 col-md-6 col-sm-12">
            <h2>Formwise Fees Report</h2>
         </div>
         <div class="col-lg-7 col-md-6 col-sm-12 line">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>">Reports</a></li>
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/report/view-reports'); ?>">View Reports</a></li>
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/report/view-reports/formwise-fees-report'); ?>">Formwise Fees Report</a></li>
            </ul>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row clearfix">
         <div class="col-lg-12" id="bodypadd">
            <div class="tab-content">
               <div class="tab-pane active" id="classlist">
                  <div class="card">
                  <div class="body form-gap">
                   <!-- <div class="row"> -->
                  
                  <div class="table-responsive position">
                  <!--  DataTable for view Records  -->
                  <table class="table  m-b-0 c_list table-bordered text-center" id="formwise-fees-report" style="width: 100%;">
                    <thead class="bold">
                    <tr>
                      <td>Sr No</td>
                      <td>Form Name</td>
                      <td>Class - Section - BOE - Medium</td>
                      <td>No of Applicant</td>
                      <td>Fees Collected Amount</td>
                      
                    </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Admission Form 2019-20</td>
                        <td>1 - A - RBSE - HINDI</td>
                        <td>10</td>
                        <td>10000</td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Admission Form 2019-20</td>
                        <td>1 - A - CBSE - ENGLISH</td>
                        <td>15</td>
                        <td>15000</td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Admission Form 2019-20</td>
                        <td>2 - A - RBSE - HINDI</td>
                        <td>10</td>
                        <td>10000</td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>Admission Form 2019-20</td>
                        <td>2 - A - CBSE - ENGLISH</td>
                        <td>13</td>
                        <td>13000</td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>Admission Form 2019-20</td>
                        <td>3 - A - CBSE - ENGLISH</td>
                        <td>15</td>
                        <td>15000</td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td> </td>
                        <td> </td>
                        <td>Total </td>
                        <td>63</td>
                        <td>63000 </td>
                      </tr>
                    </tfoot>
                  </table>
              </div>          
             </div>
            </div>
           </div>
          </div>
        </div>
       </div>
      </div>
  
</section>
<!-- Content end here  -->

<script type="text/javascript">
  $(document).ready(function () {
        var table = $('#formwise-fees-report').DataTable({
           fixed: {
            header: true,
            footer: true
        },
           dom: 'Blfrtip',
            buttons: [{ 
              extend: 'csvHtml5',
              footer: true,
              exportOptions: {
              columns: ':visible' }
            }],

        });
    });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>