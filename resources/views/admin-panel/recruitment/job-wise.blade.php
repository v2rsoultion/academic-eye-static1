@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive{
        overflow-x: visible !important;
    }
</style>
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.job_wise') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <!-- <a href="{!! url('admin-panel/recruitment/add-job') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a> -->
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/recruitment') !!}">{!! trans('language.menu_recruitment') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/recruitment') !!}">{!! trans('language.reports') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="">{!! trans('language.job_wise') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                   <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                   </div>      
                                @endif
                                <form>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('medium_type', $job['arr_medium'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                            @if($errors->has('medium_type')) <p class="help-block">{{ $errors->first('medium_type') }}</p> @endif
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('job_name', old('job_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.job_names'), 'id' => 'job_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised  btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised  btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                </form>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>Medium</th>
                                                <th>{{trans('language.job_names')}}</th>
                                                <th>{{trans('language.job_no_of_vacancy')}}</th>
                                                <th>{{trans('language.no_of_remaining_vacancy')}}</th>
                                                <th>{{trans('language.candidate_details')}} </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Hindi</td>
                                                <td>Accountant</td>
                                                <td>101</td>
                                                <td>17</td>
                                                <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="{!! url('admin-panel/recruitment/view-candidate-record') !!}">View Candidates</a></li> 
                                                </ul> </div></td>
                                                <!-- <td> <a href="{!! url('admin-panel/recruitment/view-candidate-record') !!}" class="btn btn-info view_profile">View Candidate</a>
                                                    </td> -->
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>English</td>
                                                <td>Accountant</td>
                                                <td>101</td>
                                                <td>17</td>
                                                <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="{!! url('admin-panel/recruitment/view-candidate-record') !!}">View Candidates</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>English</td>
                                                <td>Class Teacher</td>
                                                <td>101</td>
                                                <td>17</td>
                                                <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="{!! url('admin-panel/recruitment/view-candidate-record') !!}">View Candidates</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Hindi</td>
                                                <td>Class Teacher</td>
                                                <td>101</td>
                                                <td>17</td>
                                                <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="{!! url('admin-panel/recruitment/view-candidate-record') !!}">View Candidates</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>
@endsection
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#recruitment-job-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/recruitment/data')}}',
                data: function (d) {
                    d.medium_type = $('select[name="medium_type"]').val();
                    d.job_type    = $('select[name="job_type"]').val();
                    d.job_name    = $('input[name="job_name"]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'job_name', name: 'job_name'},
                {data: 'medium_type1', name: 'medium_type1'},
                {data: 'type', name: 'type'},
                {data: 'no_of_vacancy', name: 'no_of_vacancy'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 5, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }

</script>





