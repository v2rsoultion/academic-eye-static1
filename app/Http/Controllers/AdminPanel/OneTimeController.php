<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator; 
use Yajra\Datatables\Datatables;

class OneTimeController extends Controller
{
     /**
     *  View page for one time
     *  @Khushbu on 22 Sept 2018
    **/
    public function index()
    {
        // $job                      = [];
        $loginInfo 		    = get_loggedin_user_data();
        
        $data 				= array(
            'page_title'    => trans('language.view_one_time'),
            'redirect_url'  => url('admin-panel/fees-collection/one-time/view-one-time'),
            'login_info'    => $loginInfo,
            // 'job'           => $job,
        );
        return view('admin-panel.one-time.view_onetime')->with($data);
    }

    /**
     *  Add page for job
     *  @Khushbu on 22 Sept 2018   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        $onetime        = [];
        if (!empty($id))
        {
            $$decrypted_onetime_id = get_decrypted_value($id, true);
            $page_title             = trans('language.edit_one_time');
            $save_url               = url('admin-panel/fees-collection/one-time/save/' . $decrypted_onetime_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_one_time');
            $save_url      = url('admin-panel/fees-collection/one-time/save');
            $submit_button = 'Save';
        }

        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/fees-collection/one-time/view-one-time'),
        );
        return view('admin-panel.one-time.onetime')->with($data);
    }
}
