@if(isset($cupboard_shelf['book_cupboardshelf_id']) && !empty($cupboard_shelf['book_cupboardshelf_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
</style>

{!! Form::hidden('book_cupboardshelf_id',old('book_cupboardshelf_id',isset($cupboard_shelf['book_cupboardshelf_id']) ? $cupboard_shelf['book_cupboardshelf_id'] : ''),['class' => 'gui-input', 'id' => 'book_cupboardshelf_id', 'readonly' => 'true']) !!}

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.cupboard_shelf_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('cupboardshelf_name', old('cupboardshelf_name',isset($cupboard_shelf['book_cupboardshelf_name']) ? $cupboard_shelf['book_cupboardshelf_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.cupboard_shelf_name'), 'id' => 'book_cupboardshelf_name']) !!}
        </div>
        @if ($errors->has('cupboardshelf_name')) <p class="help-block">{{ $errors->first('cupboardshelf_name') }}</p> @endif
        @if($errors->any())<span class="text-danger">{{$errors->first()}}</span>@endif
    </div>

    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.cupboard_shelf_cup') !!} :</lable>
        <label class=" field select" style="width: 100%">
            {!!Form::select('cupboard_name', $listData['arr_cubboard'],isset($cupboard_shelf['book_cupboard_id']) ? $cupboard_shelf['book_cupboard_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'cupboard_name'])!!}
            <i class="arrow double"></i>
            @if($errors->has('cupboard_name')) <p class="help-block">{{ $errors->first('cupboard_name') }}</p> @endif
        </label>
    </div>    

    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.cupboard_shelf_capacity') !!} :</lable>
        <div class="form-group">
            {!! Form::number('book_cupboard_capacity', old('book_cupboard_capacity',isset($cupboard_shelf['book_cupboard_capacity']) ? $cupboard_shelf['book_cupboard_capacity']: ''), ['class' => 'form-control','placeholder'=>trans('language.cupboard_shelf_capacity'), 'id' => 'book_cupboard_capacity']) !!}
        </div>
        @if ($errors->has('book_cupboard_capacity')) <p class="help-block">{{ $errors->first('book_cupboard_capacity') }}</p> @endif
    </div>

    <div class="col-lg-12 col-md-12">
        <lable class="from_one1">{!! trans('language.cupboard_shelf_detail') !!} :</lable>
        <div class="form-group">
            <!-- <textarea rows="4" name="description" class="form-control no-resize" placeholder="Description"></textarea> -->
            {!! Form::textarea('book_cupboard_shelf_detail',old('book_cupboard_shelf_detail',isset($cupboard_shelf['book_cupboard_shelf_detail']) ? $cupboard_shelf['book_cupboard_shelf_detail']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.cupboard_shelf_detail'),'rows' => 3, 'cols' => 50)) !!}
        </div>
        @if ($errors->has('book_cupboard_shelf_detail')) <p class="help-block">{{ $errors->first('book_cupboard_shelf_detail') }}</p> @endif
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#cupboard-shelf-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                cupboardshelf_name: {
                    required: true,
                },
                cupboard_name: {
                    required: true,
                },
                book_cupboard_capacity: {
                    required: true,
                    min:0,
                },
                book_cupboard_shelf_detail: {
                    required: true,
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>