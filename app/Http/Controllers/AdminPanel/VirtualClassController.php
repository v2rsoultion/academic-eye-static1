<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\VirtualClass\VirtualClass; // Model 
use App\Model\VirtualClass\VirtualClassMapping; //Model
use Yajra\Datatables\Datatables;

class VirtualClassController extends Controller
{
    /**
     *  View page for Virtual Class
     *  @Pratyush on 21 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.manage_virtual_classes'),
            'redirect_url'  => url('admin-panel/virtual-class/view-virtual-classes'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.virtual-class.index')->with($data);
    }

    /**
     *  Add page for Virtual Class
     *  @Pratyush on 21 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $virtual_class  = [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_virtual_class_id 	= get_decrypted_value($id, true);
            $virtual_class     			= VirtualClass::Find($decrypted_virtual_class_id);
            if (!$virtual_class)
            {
                return redirect('admin-panel/virtual-class/add-virtual-class')->withError('Virtual class not found!');
            }
            $page_title             	= trans('language.edit_virtual_class');
            $encrypted_virtual_class_id	= get_encrypted_value($virtual_class->virtual_class_id, true);
            $save_url               	= url('admin-panel/virtual-class/save/' . $encrypted_virtual_class_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_virtual_class');
            $save_url      = url('admin-panel/virtual-class/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'virtual_class'		=> $virtual_class,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/virtual-class/view-virtual-classes'),
        );
        return view('admin-panel.virtual-class.add')->with($data);
    }

    /**
     *  Add and update Virtual Class's data
     *  @Pratyush on 21 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_virtual_class_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $virtual_class = VirtualClass::find($decrypted_virtual_class_id);

            if (!$virtual_class)
            {
                return redirect('/admin-panel/virtual-class/add-virtual-class/')->withError('Virtual Class not found!');
            }
            $success_msg = 'Virtual Class updated successfully!';
        }
        else
        {
            $virtual_class	= New VirtualClass;
            $success_msg 	= 'Virtual Class saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'virtual_class_name'   => 'required|unique:virtual_classes,virtual_class_name,' . $decrypted_virtual_class_id . ',virtual_class_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $virtual_class->admin_id       				= $loginInfo['admin_id'];
                $virtual_class->update_by      				= $loginInfo['admin_id'];
                $virtual_class->school_id      				= $loginInfo['school_id'];
                $virtual_class->virtual_class_name 	   		= Input::get('virtual_class_name');
                $virtual_class->virtual_class_description 	= Input::get('virtual_class_description');
                $virtual_class->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();

                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/virtual-class/view-virtual-classes')->withSuccess($success_msg);
    }

    /**
     *  Get Virtual Class's Data for view page(Datatables)
     *  @Pratyush on 21 July 2018.
    **/
    public function anyData()
    {
        $loginInfo 			    = get_loggedin_user_data();
        $temp_virtual_class     = VirtualClass::where([['school_id', '=', $loginInfo['school_id']]])->orderBy('virtual_class_id', 'DESC')->get();
        $virtual_class          = array();    
        foreach($temp_virtual_class as $tempkey2 => $tempvalue2){
            $virtual_class[$tempkey2] = (object)$tempvalue2;
        }
        
        return Datatables::of($virtual_class)
                ->addColumn('virtual_class_description', function ($virtual_class)
                {
                     return substr($virtual_class->virtual_class_description,0,50)." ...";
                    
                })
                ->addColumn('no_of_student', function ($virtual_class)
                {
                     return VirtualClassMapping::where('virtual_class_id',$virtual_class->virtual_class_id)->count();
                    
                })
        		->addColumn('action', function ($virtual_class)
                {
                    $encrypted_virtual_class_id = get_encrypted_value($virtual_class->virtual_class_id, true);
                    if($virtual_class->virtual_class_status == 0) {
                        $status = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                            <div class="dropdown">
                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="zmdi zmdi-label"></i>
                                <span class="caret"></span>
                                </button>
                                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                    <li>
                                        <a href="add-student/' . $encrypted_virtual_class_id . '" "">Add Student</a>
                                    </li>
                                    <li>
                                       <a href="student-list/' . $encrypted_virtual_class_id . '" "">View student</a></li>
                                    </li>
                                 </ul>
                             </div>
                            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="class-status/'.$status.'/' . $encrypted_virtual_class_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-virtual-class/' . $encrypted_virtual_class_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-class/' . $encrypted_virtual_class_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])
                ->addIndexColumn()->make(true);
    }

    /**
     *  Destroy Virtual Class's data
     *  @Pratyush on 21 July 2018.
    **/
    public function destroy($id)
    {
        $virtual_class_id 	= get_decrypted_value($id, true);
        $virtual_class 		= VirtualClass::find($virtual_class_id);
        if ($virtual_class)
        {
            $virtual_class->delete();
            $success_msg = "Virtual Class deleted successfully!";
            return redirect('admin-panel/virtual-class/view-virtual-classes')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Virtual Class not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Virtual Class's status
     *  @Pratyush on 21 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $virtual_class_id 	= get_decrypted_value($id, true);
        $virtual_class 		= VirtualClass::find($virtual_class_id);
        if ($virtual_class)
        {
            $virtual_class->virtual_class_status  = $status;
            $virtual_class->save();
            $success_msg = "Virtual Class status updated!";
            return redirect('admin-panel/virtual-class/view-virtual-classes')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Virtual Class not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page for Student List
     *  @Pratyush on 30 July 2018
    **/
    public function studentList($id)
    {

        $loginInfo                  = get_loggedin_user_data();
        $arr_class                  = get_all_classes();
        $arr_section                = get_class_section();
        $listData                   = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $virtual_class_id           = get_decrypted_value($id, true);
        $data = array(
            'page_title'        => trans('language.manage_virtual_classes'),
            'redirect_url'      => url('admin-panel/virtual-class/add-student'),
            'login_info'        => $loginInfo,
            'listData'          => $listData,
            'virtual_class_id'  => $virtual_class_id
        );

        return view('admin-panel.virtual-class.view_students')->with($data);
    }

    /**
     *  Get Student List Data for view student page(Datatables)
     *  @Pratyush on 30 July 2018
    **/
    public function studentListData(Request $request)
    {   
        
        $student     = [];
        $arr_students = get_all_student_list_for_virtual_class($request);
        
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        
        return Datatables::of($student)
            ->addColumn('class_section', function ($student)
                {
                     return $student->student_class.' - '.$student->student_session;
                    
                })
            ->addColumn('action', function ($student)
            {
                $encrypted_student_id       = get_encrypted_value($student->student_id, true);
                $encrypted_virtual_class_id = get_encrypted_value($student->virtual_class_id, true);
                
                return '<div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="../add-student-in-class/'.$encrypted_virtual_class_id.'/'. $encrypted_student_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
               
                
            })->rawColumns(['action' => 'action','class_section' => 'class_section'])->addIndexColumn()->make(true);
    }

    /**
     *  Add student to virtual class
     *  @Pratyush on 31 July 2018
    **/
    public function addStudentInClass($class_id,$student_id)
    {   
        $loginInfo          = get_loggedin_user_data();
        $virtual_class_id   = get_decrypted_value($class_id, true);
        $virtual_student_id = get_decrypted_value($student_id, true);
        $success_msg        = "Student has been added."; 
        $virtual_class_map  = New VirtualClassMapping;
        DB::beginTransaction();
            try
            {
                $virtual_class_map->admin_id                    = $loginInfo['admin_id'];
                $virtual_class_map->update_by                   = $loginInfo['admin_id'];
                $virtual_class_map->school_id                   = $loginInfo['school_id'];
                $virtual_class_map->virtual_class_id            = $virtual_class_id;
                $virtual_class_map->student_id                  = $virtual_student_id;
                $virtual_class_map->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                p($error_message);
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
            return redirect('admin-panel/virtual-class/add-student/'.$class_id)->withSuccess($success_msg);
        }
        
    /**
     *  View page for Delete Student
     *  @Pratyush on 31 July 2018
    **/
    public function studentDeleteList($id)
    {

        $loginInfo                  = get_loggedin_user_data();
        $arr_class                  = get_all_classes();
        $arr_section                = get_class_section();
        $listData                   = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $virtual_class_id           = get_decrypted_value($id, true);
        
        $data = array(
            'page_title'        => trans('language.manage_virtual_classes'),
            'redirect_url'      => url('admin-panel/virtual-class/add-student'),
            'login_info'        => $loginInfo,
            'listData'          => $listData,
            'virtual_class_id'  => $virtual_class_id
        );

        return view('admin-panel.virtual-class.delete_students')->with($data);
    }

    /**
     *  Get Student List Data for view student page(Datatables)
     *  @Pratyush on 30 July 2018
    **/
    public function studentDeleteListData(Request $request)
    {   
        
        $student     = [];
        $arr_students = get_all_student_list_for_virtual_class($request);
        
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }

        return Datatables::of($student)
            ->addColumn('class_section', function ($student)
                {
                     return $student->student_class.' - '.$student->student_session;
                    
                })    
            ->addColumn('action', function ($student)
            {
                $encrypted_student_id       = get_encrypted_value($student->student_id, true);
                $encrypted_virtual_class_id = get_encrypted_value($student->virtual_class_id, true);
                
               return '<div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="../delete-student-in-class/'.$encrypted_virtual_class_id.'/'. $encrypted_student_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                
            })->rawColumns(['action' => 'action','class_section' => 'class_section'])->addIndexColumn()->make(true);
    }

    /**
     *  Add student to virtual class
     *  @Pratyush on 31 July 2018
    **/
    public function destroyStudent($class_id,$student_id)
    {   
        $virtual_class_id   = get_decrypted_value($class_id, true);
        $virtual_student_id = get_decrypted_value($student_id, true);
        $virtual_class      = VirtualClassMapping::where('virtual_class_id',$virtual_class_id)->where('student_id',$virtual_student_id);
        if ($virtual_class)
        {
            $virtual_class->delete();
            $success_msg = "Student deleted successfully!";
            return redirect('admin-panel/virtual-class/student-list/'.$class_id)->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Student not found!";
            return redirect()->back()->withErrors($error_message);
        }
        }
    
}