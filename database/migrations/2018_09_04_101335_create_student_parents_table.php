<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'student_parents';
    protected $primaryKey = 'student_parent_id';
    public function up()
    {
        if (!Schema::hasTable('student_parents')) { 
            Schema::create('student_parents', function (Blueprint $table) {
                $table->increments('student_parent_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('reference_admin_id')->unsigned()->nullable();

                $table->string('student_login_name', 255)->nullable();
                $table->string('student_login_email', 255)->nullable();
                $table->string('student_login_contact_no', 255)->nullable();

                $table->string('student_father_name', 255)->nullable();
                $table->string('student_father_mobile_number', 255)->nullable();
                $table->string('student_father_email', 255)->nullable();
                $table->string('student_father_occupation', 255)->nullable();
                $table->string('student_father_annual_salary',255)->nullable();
                $table->string('student_mother_name', 255)->nullable();
                $table->string('student_mother_mobile_number', 255)->nullable();
                $table->string('student_mother_email', 255)->nullable();
                $table->string('student_mother_occupation', 255)->nullable();
                $table->string('student_mother_annual_salary',255)->nullable();
                $table->text('student_mother_tongue')->nullable();
                $table->string('student_guardian_name', 255)->nullable();
                $table->string('student_guardian_email', 255)->nullable();
                $table->string('student_guardian_mobile_number', 255)->nullable();
                $table->string('student_guardian_relation')->nullable();
                $table->tinyInteger('student_parent_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('student_parents', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('student_parents', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('student_parents', function($table) {
                $table->foreign('reference_admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
          
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_parents');
    }
}
