<?php $__env->startSection('content'); ?>
<!-- Main Content -->
<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Add Vehcile</h2>
            </div> 
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right ">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Transport<!-- <?php echo trans('language.staff'); ?> --></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Vehicle</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/transport/vehicle/add-vehicle'); ?>">Add Vehicle</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div>
                    <div class="body form-gap">
                        <form class="" action="" method="post"  id="form_validation">
                            <div class="row clearfix"> 
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1">Vehicle Name :</lable>
                                    <div class="form-group">
                                        <input type="text" name="vehcile_name" class="form-control" placeholder="Vehicle Name">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1">Vehicle Registration Number :</lable>
                                    <div class="form-group">
                                        <input type="text" name="vehcile_reg_number" class="form-control" placeholder="Vehicle Registration Number">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1">Type :</lable>
                                    <div class="form-group">
                                        <div class="radio" style="margin-top:6px !important;">
                                            <input type="radio" name="type" id="radio1" value="option1">
                                            <label for="radio1" class="document_staff">Owner</label>
                                            <input type="radio" name="type" id="radio2" value="option2" checked="">
                                            <label for="radio2" class="document_staff">Contract</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1">Capacity :</lable>
                                    <div class="form-group">
                                        <input type="text" name="capacity" class="form-control" placeholder="Capacity">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1">Contact person :</lable>
                                    <div class="form-group">
                                        <input  class="form-control positive-numeric-only" id="id-blah1" min="0" name="contact_person" type="number" value="" placeholder="Contact person">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1">Contact number :</lable>
                                    <div class="form-group">
                                        <input type="text" name="contact_number" class="form-control" placeholder="Contact Number">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-raised  btn-primary">Save</button>
                                    <button type="submit" class="btn btn-raised  btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<!-- <script src="assets/plugins/jquery-validation/jquery.validate.js"></script> 
<script src="assets/js/pages/forms/form-validation.js"></script> -->


<script>

function myFunction1() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "none";
    } else {
        x.style.display = "none";
    }
}
</script>

<style>
#myDIV {
    width: 100%;
}
</style>
<script type="text/javascript">

$('#form_validation').validate({
        rules: {
            'vehcile_name': {
                required: true
            },
            'vehcile_reg_number': {
                required: true
            },
            'type': {
                required: true
            },
            'capacity': {
                required: true
            },
            'contact_person': {
                required: true
            },
            'contact_number': {
                required: true
            }
        },

        /* @validation  error messages 
      ---------------------------------------------- */
      messages: {
        vehcile_name: {
          required: 'Please fill your required vehcile name'
        },
        vehcile_reg_number: {
          required: 'Please fill requerd vehcile reg number'
        },
        type: {
          required: 'Please fill requerd type'
        },
        capacity: {
          required: 'Please fill requerd capacity'
        },
        contact_person: {
          required: 'Please fill requerd contact person'
        },
        contact_number: {
          required: 'Please fill requerd contact number'
        }         
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

</script>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>