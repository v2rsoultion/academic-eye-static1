
<?php $__env->startSection('content'); ?>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>RTE Fees Head</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
           <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>">RTE Fees Head</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/rte-collection/rte-fee-head'); ?>">Add</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <!-- <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
                </div> -->
              <div class="body form-gap">
                <div class="row tabless" >
                  <div class="headingcommon  col-lg-12" style="padding-bottom: 0px !important">Add Form :-</div>
                  <form class="" action="" method="post" id="searchpanel" style="width: 100%;">
                    <div class="row" >
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Head Name</lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Head name">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="Amount">Amount</lable>
                          <input type="text" name="Amount" id="" class="form-control" placeholder="Amount">
                        </div>
                      </div>
                       <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Save">Save
                      </button>
                    </div>
                     <div class="col-lg-1 ">
                      <button type="reset" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Clear">Clear
                      </button>
                    </div>
                    </div>
                  </form>
                  <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div>
                  <div class="headingcommon  col-lg-12" style="padding-bottom: 0px !important">Search By :-</div>
                  <form class="" action="" method="post" id="searchpanel" style="width: 100%;">
                    <div class="row" >
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Head Name</lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Head name">
                        </div>
                      </div>
                      <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised  btn-primary" style="margin-top: 23px !important;" title="Submit">Search
                      </button>
                    </div>
                    </div>
                  </form>
                  <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div>
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <table class="table  m-b-0 c_list">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Head Name</th>
                        <th>Amount</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $count = 1; for ($i=0; $i < 15; $i++) {  ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td>Exam Fees</td>
                        <td>Rs: 50156</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <?php $count++; } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>