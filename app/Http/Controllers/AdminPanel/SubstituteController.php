<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubstituteController extends Controller
{
    public function manageSubstitute() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.substitute-management.manage_substitute')->with($data);
    }

    public function substituteSchedule() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.substitute-management.substitute_schedule')->with($data);
    }

     public function allocateTeacher() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.substitute-management.allocate_teacher')->with($data);
    }

    public function viewAllocateTeacher() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.substitute-management.view_allocate_teacher')->with($data);
    }
}
