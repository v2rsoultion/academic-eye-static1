@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_question_paper') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.question_paper') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_question_paper') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">                                     
                                       
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                <select class="form-control show-tick select_form1" id="subject_id">
                                                    <option value="">Select Subject</option>
                                                    <option value="1">Hindi</option>
                                                    <option value="2">English</option>
                                                    <option value="3">Computer </option>
                                                </select>
                                               
                                                <i class="arrow double"></i>
                                            </label>
                                            @if($errors->has('subject_id')) <p class="help-block">{{ $errors->first('subject_id') }}</p> @endif
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised  btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised  btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="">
                                <table class="table m-b-0 c_list" id="" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            
                                            <th>{{trans('language.subject')}}</th>
                                            <th>{{trans('language.notes_unit')}}</th>
                                            <th>{{trans('language.notes_topic')}}</th>
                                            <th>Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Hindi</td>
                                            <td>Unit-1</td>
                                            <td>Chapter</td>
                                            <td>
                                                <a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" title="View" class="text-primary" style="color: #0275d8!important;" target="_blank">View </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                
                                            <td>Computer</td>
                                            <td>Unit-5</td>
                                            <td>Hardware</td>
                                            <td>
                                                <a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" title="View" class="text-primary" style="color: #0275d8!important;" target="_blank">View </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<style type="text/css">

.profile_detail_span_6 a{
    text-decoration: none !important;
    color: #6572b8 !important;
}
.d1{
    margin-left: -98px !important;
}
.d1:before {
    display: inline-block;
    position: absolute;
    width: 0;
    height: 0;
    vertical-align: middle;
    content: "";
    top: -5px;
    left: 148px !important;
    right: auto;
    color: #fff;
    border-bottom: .4em solid;
    border-right: .4em solid transparent;
    border-left: .4em solid transparent;
}
</style>

<script>
    $(document).ready(function () {
        var table = $('#question-paper-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
        
            ajax: {
                url: "{{url('admin-panel/question-paper/data')}}",
                data: function (d) {
                    d.class_id   = $('select[name=class_id]').find(':selected').val();
                    d.subject_id = $('select[name=subject_id]').find(':selected').val();
                }
            },
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'question_paper_name', name: 'question_paper_name'},
                {data: 'class_section', name: 'class_section'},
                {data: 'class_subject', name: 'class_subject'},
                {data: 'profile', name: 'profile'},
                {data: 'exam_type', name: 'exam_type'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    targets: [ 0, 1, 2, 3, 4, 5, 6],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            $("select[name='class_id'").selectpicker('refresh');
            $("select[name='subject_id'").selectpicker('refresh');
            table.draw();
            e.preventDefault();
        })


        function getImg(data, type, full, meta) {
            if (data != '') {
                return '<img src="'+data+'" height="50" />';
            } else {
                return 'No Image';
            }
        }

    });


</script>
@endsection




