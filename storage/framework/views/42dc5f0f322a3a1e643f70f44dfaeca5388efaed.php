
<?php $__env->startSection('content'); ?>
<!--  Main content here -->
<!--  Main content here -->
<style type="text/css">
 
</style>
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Map Heads to Student</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>">Map Heads to Student</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/view-map-student'); ?>">Add Map Student</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <!-- <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
                </div> -->
              <div class="body form-gap">
                <div class="row tabless" >
                  <form class="" action="" method="" id="searchpanel" style="width: 100%;">
                    <div class="row">
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Name </lable>
                          <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Class</lable>
                          <select class="form-control show-tick select_form1" name="classes" id="">
                            <option value="">Select</option>
                            <option value="A">10th</option>
                            <option value="B">12th</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-primary" style = "margin-top: 23px !important;"
                      title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary" style= "margin-top: 23px !important;" title="Clear">Clear
                      </button>
                    </div>
                    </div>
                  </form>
                  <!--  DataTable for view Records  -->
                  <table class="table m-b-0 c_list">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th class="text-center">Fees heads name</th>
                        <th class="text-center">Applied on Classes</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $serial = 1;
                        for ($i=0; $i < 15 ; $i++) {  ?>
                      <tr>
                        <td><?php echo  $serial ?></td>
                        <td class="text-center"> Ankit Dave</td>
                        <td class="text-center">10th</td>
                        
                        <td class="text-center">
                          <a href="<?php echo e(url('/admin-panel/fees-collection/view-map-student-details')); ?>" class="btn btn-primary linkTag" title="Map Student">Map Student</a>
                        </td>
                      </tr>
                      <?php $serial++; } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>