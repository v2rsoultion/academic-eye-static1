<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Exam\Exam; // Model
use Yajra\Datatables\Datatables;

class ExamController extends Controller
{
    /**
     *  View page for Exam
     *  @Pratyush on 11 Aug 2018
    **/
    public function index()
    {
        $exam                   = [];
        $loginInfo              = get_loggedin_user_data();
        $arr_medium             = \Config::get('custom.medium_type');
        $exam['arr_medium']     = add_blank_option($arr_medium, 'Select Medium');
        $data = array(
            'page_title'    => trans('language.view_exam'),
            'redirect_url'  => url('admin-panel/examination/manage-exam'),
            'login_info'    => $loginInfo,
            'exam'          => $exam
        );
        return view('admin-panel.exam.index')->with($data);
    }

    /**
     *  Add page for Exam
     *  @Pratyush on 11 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    				= [];
        $exam 					= [];
        $loginInfo 				= get_loggedin_user_data();
        $terms 					= [];
        $listData['arr_terms']  = add_blank_option($terms, 'Select Term');
        if (!empty($id))
        {
            $decrypted_exam_id 	= get_decrypted_value($id, true);
            $exam      			= Exam::Find($decrypted_exam_id);
            if (!$exam)
            {
                return redirect('admin-panel/examination/manage-exam')->withError('Exam not found!');
            }
            $page_title             	= trans('language.add_exam');
            $encrypted_exam_id   		= get_encrypted_value($exam->exam_id, true);
            $save_url               	= url('admin-panel/examination/save-exam/' . $encrypted_exam_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_exam');
            $save_url      = url('admin-panel/examination/save-exam');
            $submit_button = 'Save';
        }
        $arr_medium             = \Config::get('custom.medium_type');
        $exam['arr_medium']     = add_blank_option($arr_medium, 'Select Medium');
        $data                   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'exam' 				=> $exam,
            'listData'			=> $listData,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/examination/manage-exam'),
        );
        return view('admin-panel.exam.add_manage_exam')->with($data);
    }

    /**
     *  Add and update Exam's data
     *  @Pratyush on 11 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_exam_id			= get_decrypted_value($id, true);
        $admin_id                   = $loginInfo['admin_id'];
        $medium_type = null;
        if (!empty($id))
        {
            $exam       = Exam::find($decrypted_exam_id);
            $admin_id   = $exam['admin_id'];
            if (!$exam)
            {
                return redirect('/admin-panel/examination/manage-exam/')->withError('Exam not found!');
            }
            $success_msg = 'Exam updated successfully!';
        }
        else
        {
            $exam     		= New Exam;
            $success_msg 	= 'Exam saved successfully!';
        }
        if ($request->has('medium_type'))
        {
            $medium_type = Input::get('medium_type');
        }
        $validatior = Validator::make($request->all(), [
                'exam_name'   => 'required|unique:exams,exam_name,' . $decrypted_exam_id . ',exam_id,medium_type,' . $medium_type,
                'medium_type'      => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $exam->admin_id       = $admin_id;
                $exam->update_by      = $loginInfo['admin_id'];
                $exam->medium_type 	  = Input::get('medium_type');
                $exam->exam_name 	  = Input::get('exam_name');
                $exam->term_exam_id   = Input::get('term_exam_id');
                $exam->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/examination/manage-exam')->withSuccess($success_msg);
    }

    /**
     *  Get Exam's Data for view page(Datatables)
     *  @Pratyush on 11 Aug 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 		= get_loggedin_user_data();
        if($request->get('medium_type') == null) {
            $exam = [];
        } else {
            $exam  			= Exam::where(function($query) use ($request) 
            {
                if (!empty($request) && !empty($request->get('name')))
                {
                    $query->where('exam_name', "like", "%{$request->get('name')}%");
                }
                if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null)
                {
                    $query->where('medium_type', "=", $request->get('medium_type'));
                }
            })->with('getTerm')->orderBy('exam_id', 'ASC')->get();
        }
        return Datatables::of($exam,$request)
                ->addColumn('medium_type', function ($exam)
                {
                    $arr_medium             = \Config::get('custom.medium_type');
                    return $arr_medium[$exam['medium_type']];
                })
        		->addColumn('term_exam_id', function ($exam)
                {
                	$term_name  = '----';
                	if($exam->term_exam_id != ''){
                		$term_name  = $exam['getTerm']->term_exam_name;
                	}
                    return $term_name;
                })
        		->addColumn('action', function ($exam) use($request)
                {

                    if($request->get('tempid') != '' && $request->get('tempid') != null){
                        $edit_manage_path = '../manage-exam';                        
                    }else{
                        $edit_manage_path = 'manage-exam';
                    }

                    $encrypted_exam_id = get_encrypted_value($exam->exam_id, true);
                    if($exam->exam_status == 0) {
                        $status = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                            <div class="pull-left"><a href="exam-status/'.$status.'/' . $encrypted_exam_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.$edit_manage_path.'/' . $encrypted_exam_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-exam/' . $encrypted_exam_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Destroy Exam's data
     *  @Pratyush on 11 Aug 2018.
    **/
    public function destroy($id)
    {
        $exam_id 		= get_decrypted_value($id, true);
        $exam 		  	= Exam::find($exam_id);
        if ($exam)
        {
            $exam->delete();
            $success_msg = "Exam deleted successfully!";
            return redirect('admin-panel/examination/manage-exam')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Exam not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Exam's status
     *  @Pratyush on 11 Aug 2018.
    **/
    public function changeStatus($status,$id)
    {
        $exam_id 		= get_decrypted_value($id, true);
        $exam 		  	= Exam::find($exam_id);
        if ($exam)
        {
            $exam->exam_status  = $status;
            $exam->save();
            $success_msg = "Exam status update successfully!";
            return redirect('admin-panel/examination/manage-exam')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Exam not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
