@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_bbook') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ URL::to('admin-panel/book/view-books') }}">{!! trans('language.bbook') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_bbook') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">{!! trans('language.title') !!}</a></li>
                                    <li class="nav-item"><a class="nav-link"  href="{{ url('admin-panel/title/add-title') }}">{!! trans('language.add_title') !!}</a></li>
                                </ul>                        
                            </div> -->
                            <div class="body">
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.bbook_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('publisher', old('publisher', ''), ['class' => 'form-control ','placeholder'=>trans('language.bbook_publisher'), 'id' => 'publisher']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('subtitle', old('subtitle', ''), ['class' => 'form-control ','placeholder'=>trans('language.bbook_subtitle'), 'id' => 'subtitle']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}                                

                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="book-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{trans('language.bbook_name')}}</th>
                                            <th>{{trans('language.bbook_publisher')}}</th>
                                            <th>{{trans('language.bbook_total_copies')}}</th>
                                            <th>{{trans('language.bbook_issue_copies')}}</th>
                                            <th>{{trans('language.bbook_cupboard_plus_shelf')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#book-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bFilter: false,
            // ajax: "{{url('admin-panel/book/data')}}",

            ajax: {
                url: "{{url('admin-panel/book/data')}}",
                data: function (d) {
                    d.book_name = $('input[name=name]').val();
                    d.publisher = $('input[name=publisher]').val();
                    d.subtitle  = $('input[name=subtitle]').val();
                }
            },
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'book_name', name: 'book_name'},
                {data: 'publisher_name', name: 'publisher_name'},
                {data: 'book_copies_no', name: 'book_copies_no'},
                {data: 'issued_copies', name: 'issued_copies'},
                {data: 'cupboard_shelf', name: 'cupboard_shelf'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 6, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })
    });


</script>
@endsection




