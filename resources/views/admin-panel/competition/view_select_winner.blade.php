@extends('admin-panel.layout.header')
@section('content')
{!! Html::script('public/admin/assets/js/jquery.modal.min.js') !!}
{!! Html::style('public/admin/assets/css/jquery.modal.min.css') !!}
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.comp_select_winner') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body">
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('enroll_no', old('enroll_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_enroll_no'), 'id' => 'enroll_no']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('student_name', old('student_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_student_name'), 'id' => 'student_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        {!! Form::hidden('competition_id',$competition_id) !!}
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('class_id', $listData['arr_class'],isset($listData['class_id']) ? $listData['class_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'class_id','onChange' => 'getSection(this.value)'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                            @if($errors->has('class_id')) <p class="help-block">{{ $errors->first('class_id') }}</p> @endif
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('section_id', $listData['arr_section'],isset($listData['section_id']) ? $listData['section_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'section_id','disabled'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                            @if($errors->has('section_id')) <p class="help-block">{{ $errors->first('section_id') }}</p> @endif
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1"> 
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="student-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.virtual_class_stu_enroll_no')}}</th>
                                                <th>{{trans('language.profile')}}</th>
                                                <th>{{trans('language.virtual_class_stu_name')}}</th>
                                                <th>{{trans('language.virtual_class_stu_f_name')}}</th>
                                                <th>{{trans('language.virtual_class_stu_class')}}</th>
                                                <!-- <th>{{trans('language.virtual_class_stu_session')}}</th> -->
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>

                             
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>
<!-- Model code start here -->

<p><a href="#ex1" id="openLink" rel="modal:open"></a></p>
<!-- Modal HTML embedded directly into document -->
<div id="ex1" class="modal">    
  <div class="container">
  <p><strong>Set Position</strong></p>
    {!! Form::open(['files'=>TRUE,'id' => 'rank-form' , 'class'=>'form-horizontal']) !!}
    <div id='displaymsg'></div>
    <div class="row">
        <div class="col-lg-9 col-md-9">
            <div class="input-group ">
                {!! Form::text('rank','', ['class' => 'form-control form_control1','placeholder'=>trans('language.comp_list_of_winner_rank'), 'id' => 'rank']) !!}
            </div>
        </div>
        {!! Form::hidden('student_id','', ['id' => 'student_id']) !!}
        <div class="col-lg-3 col-md-3">
            {!! Form::button('Submit', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'submit','id'=>'idsubmit','onclick'=>'return submitrankform()']) !!}
        </div>
        
      </div>
      {!! Form::close() !!}      
    </div>
    <a href="#" id="closemypopup" rel="modal:close"></a>
</div>

<style type="text/css">
    
.form_control1{
    background-color: #fff !important;
    border: 1px solid #e6d3d3 !important;
}
</style>
<!-- Model code ends here -->
<script>
    $(document).ready(function () {
        // For Pop-up
        $('#rank').val('');
        $('#displaymsg').html("");
        // For Pop-up
        var table = $('#student-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/competition/competition-select-winner-data')}}',
                data: function (d) {
                    d.enroll_no      = $('input[name=enroll_no]').val();
                    d.student_name   = $('input[name=student_name]').val();
                    d.father_name    = $('input[name=father_name]').val();
                    d.competition_id = $('input[name=competition_id]').val();
                    d.class_id          = $('select[name="class_id"]').val();
                    if($('select[name=section_id]').find(':selected').val() != 'Select section'){
                        d.section_id        = $('select[name=section_id]').find(':selected').val();
                    }else{
                        d.section_id        = '';
                    }

                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'student_enroll_number', name: 'student_enroll_number'},
                {data: 'profile', render: getImg},
                {data: 'student_name', name: 'student_name'},
                {data: 'student_father_name', name: 'student_father_name'},
                {data: 'class_section' , name: 'class_section'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 6, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4, 5, 6 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            $("select[name='section_id'").html('');
            $("select[name='section_id'").selectpicker('refresh');
            $("select[name='class_id'").selectpicker('refresh');
            table.draw();
            e.preventDefault();
        })

        $(document).on("click","#student_pop_id", function (e) {
            student_id = $(this).attr("student-id");
            $("#student_id").val(student_id);
            $('#openLink').trigger('click');
        })

        $(document).on("click","a.close-modal", function (e) {
            window.location.reload(true);
        })
        
    });

    function getImg(data, type, full, meta) {
        if (data != '') {
            return '<img src="'+data+'" height="50" />';
        } else {
            return 'No Image';
        }
    }
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    
    function submitrankform(){

        var rank            =  $('#rank').val();
        var competition_id  = $('input[name=competition_id]').val();   
        var student_id      = $("#student_id").val();                 
        
        if(rank !=''){
            $('.mycustloading').css('display','block');
            $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/competition/competition-set-position')}}",
            type: 'GET',
            data: {
                'rank'              : rank,
                'competition_id'    : competition_id,
                'student_id'        : student_id,
                'operation_type'    : 1
            },
            success: function (response){
                if(response.status == 1001){
                    $('#displaymsg').html("<span class='text-success'>"+response.message+"</span>");     
                }else if(response.status == 404){
                    $('#displaymsg').html("<span class='text-danger'>"+response.message+"</span>");     
                }   
                $("#idsubmit").attr("disabled", "disabled");
                $('.mycustloading').css('display','none');
            }
        });     
        }
    }

    function getSection(class_id)
    {
        if(class_id != ''){
            $('.mycustloading').css('display','block');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html('');
                    $("select[name='section_id'").html(data.options);
                    $("select[name='section_id'").removeAttr("disabled");
                    $("select[name='section_id'").selectpicker('refresh');
                    $('.mycustloading').css('display','none');
                }
            });
        }else{
            // $("select[name='section_id'").val('').change();
            // $("select[name='section_id'").get(0).selectedIndex = 0;
            $("select[name='section_id'").html('');
            $("select[name='section_id'").selectpicker('refresh');

        }
    }
</script>
@endsection