<!doctype html>
<html class="no-js " lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <title>Academic Eye || Admin Panel </title>
    <!-- <link rel="icon" href="assets/images/logo.svg" type="image/x-icon"> -->
    <!-- Favicon-->
    <link rel="stylesheet" href="{{url("/public/assets/plugins/bootstrap/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{url("/public/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css")}}">
    <link rel="stylesheet" href="{{url("/public/assets/plugins/morrisjs/morris.min.css")}}">
    <link rel="stylesheet" href="{{url("/public/assets/css/bootstrap-tagsinput.css")}}">
    <!-- Dropzone Css -->
    <link href="{{url("/public/assets/plugins/dropzone/dropzone.css")}}" rel="stylesheet">
    <!-- Bootstrap Select Css -->
    <link href="{{url("/public/assets/plugins/bootstrap-select/css/bootstrap-select.css")}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{url("/public/assets/css/stylesheet.css")}}">
    <link rel="stylesheet" href="{{url("/public/assets/css/main.css")}}">
    <link rel="stylesheet" href="{{url("/public/assets/css/style.css")}}">
    <link rel="stylesheet" href="{{url("/public/assets/css/color_skins.css")}}">

    <link rel="stylesheet" type="text/css" href="{{url("/public/assets/plugins/fullcalendar/fullcalendar.min.css")}}">
    <link rel="stylesheet" href="{{url("/public/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css")}}">
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{url("/public/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css")}}" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    {!! Html::style('public/admin/assets/css/font-awesome.min.css') !!}
    {!! Html::style('public/admin/assets/css/select2-bootstrap.css') !!}
    {!! Html::style('public/admin/assets/css/select2.min.css') !!}

    {!! Html::script('public/assets/js/jquery.min.js') !!}

   
  </head>
  <body class="theme-blush">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
      <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="{!! URL::to('public/admin/assets/images/logo.svg') !!}" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>
      </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Top Bar -->
    <nav class="navbar p-l-5 p-r-5">
      <ul class="nav navbar-nav navbar-left">
        <li>
          <div class="navbar-header">
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="index.html"><img src="{!! URL::to('public/admin/assets/images/logo.svg') !!}" width="30" alt="Oreo"><span class="m-l-10">Academic Eye</span></a>
          </div>
        </li>
        <li><a href="javascript:void(0);" class="ls-toggle-btn" data-close="true" style= "margin-top: 0px"><i class="zmdi zmdi-swap"></i></a></li>
        <li class="dropdown">
          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
            <i class="zmdi zmdi-notifications"></i>
            <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
          <ul class="dropdown-menu pullDown">
            <li class="body">
              <ul class="menu list-unstyled">
                <li>
                  <a href="javascript:void(0);">
                    <div class="media">
                      <img class="media-object" src="{!! URL::to('public/assets/images/xs/avatar2.jpg') !!}" alt="">
                      <div class="media-body">
                        <span class="name">Sophia <span class="time">30min ago</span></span>
                        <span class="message">There are many variations of passages</span>                                        
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0);">
                    <div class="media">
                      <img class="media-object" src="{!! URL::to('public/assets/images/xs/avatar3.jpg') !!}" alt="">
                      <div class="media-body">
                        <span class="name">Sophia <span class="time">31min ago</span></span>
                        <span class="message">There are many variations of passages of Lorem Ipsum</span>                                        
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0);">
                    <div class="media">
                      <img class="media-object" src="{!! URL::to('public/assets/images/xs/avatar4.jpg') !!}" alt="">
                      <div class="media-body">
                        <span class="name">Isabella <span class="time">35min ago</span></span>
                        <span class="message">There are many variations of passages</span>                                        
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0);">
                    <div class="media">
                      <img class="media-object" src="{!! URL::to('public/assets/images/xs/avatar5.jpg') !!}" alt="">
                      <div class="media-body">
                        <span class="name">Alexander <span class="time">35min ago</span></span>
                        <span class="message">Contrary to popular belief, Lorem Ipsum random</span>                                        
                      </div>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0);">
                    <div class="media">
                      <img class="media-object" src="{!! URL::to('public/assets/images/xs/avatar6.jpg') !!}" alt="">
                      <div class="media-body">
                        <span class="name">Grayson <span class="time">1hr ago</span></span>
                        <span class="message">There are many variations of passages</span>                                        
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </li>
            <li class="footer"> <a href="javascript:void(0);">View All</a> </li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
            <i class="zmdi zmdi-flag"></i>
            <div class="notify">
              <span class="heartbit"></span>
              <span class="point"></span>
            </div>
          </a>
          <ul class="dropdown-menu pullDown">
            <li class="header">Department</li>
            <li class="body">
              <ul class="menu tasks list-unstyled">
                <li>
                  <a href="javascript:void(0);">
                    <div class="progress-container progress-primary">
                      <span class="progress-badge">Computer</span>
                      <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="86" aria-valuemin="0" aria-valuemax="100" style="width: 86%;">
                          <span class="progress-value">86%</span>
                        </div>
                      </div>
                      <ul class="list-unstyled team-info">
                        <li class="m-r-15"><small class="text-muted">Team</small></li>
                        <li>
                          <img src="{!! URL::to('public/assets/images/xs/avatar2.jpg') !!}" alt="Avatar">
                        </li>
                        <li>
                          <img src="{!! URL::to('public/assets/images/xs/avatar3.jpg') !!}" alt="Avatar">
                        </li>
                        <li>
                          <img src="{!! URL::to('public/assets/images/xs/avatar4.jpg') !!}" alt="Avatar">
                        </li>
                      </ul>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0);">
                    <div class="progress-container progress-info">
                      <span class="progress-badge">Medical</span>
                      <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
                          <span class="progress-value">45%</span>
                        </div>
                      </div>
                      <ul class="list-unstyled team-info">
                        <li class="m-r-15"><small class="text-muted">Team</small></li>
                        <li>
                          <img src="{!! URL::to('public/assets/images/xs/avatar10.jpg') !!}" alt="Avatar">
                        </li>
                        <li>
                          <img src="{!! URL::to('public/assets/images/xs/avatar9.jpg') !!}" alt="Avatar">
                        </li>
                        <li>
                          <img src="{!! URL::to('public/assets/images/xs/avatar8.jpg') !!}" alt="Avatar">
                        </li>
                        <li>
                          <img src="{!! URL::to('public/assets/images/xs/avatar7.jpg') !!}" alt="Avatar">
                        </li>
                        <li>
                          <img src="{!! URL::to('public/assets/images/xs/avatar6.jpg') !!}" alt="Avatar">
                        </li>
                      </ul>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0);">
                    <div class="progress-container progress-warning">
                      <span class="progress-badge">Art & Design</span>
                      <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="29" aria-valuemin="0" aria-valuemax="100" style="width: 29%;">
                          <span class="progress-value">29%</span>
                        </div>
                      </div>
                      <ul class="list-unstyled team-info">
                        <li class="m-r-15"><small class="text-muted">Team</small></li>
                        <li>
                          <img src="{!! URL::to('public/assets/images/xs/avatar5.jpg') !!}" alt="Avatar">
                        </li>
                        <li>
                          <img src="{!! URL::to('public/assets/images/xs/avatar2.jpg') !!}" alt="Avatar">
                        </li>
                        <li>
                          <img src="{!! URL::to('public/assets/images/xs/avatar7.jpg') !!}" alt="Avatar">
                        </li>
                      </ul>
                    </div>
                  </a>
                </li>
              </ul>
            </li>
            <li class="footer"><a href="javascript:void(0);">View All</a></li>
          </ul>
        </li>
        <li class="hidden-sm-down">
          <div class="input-group">                
            <input type="text" class="form-control" id="header_search" placeholder="Search...">
            <span class="input-group-addon">
            <i class="zmdi zmdi-search"></i>
            </span>
          </div>
        </li>
         <li class="float-right">
                <a href="javascript:void(0);" class="fullscreen hidden-sm-down" data-provide="fullscreen" data-close="true"><i class="zmdi zmdi-fullscreen"></i></a>
                <a href="{{ url('/admin-panel/logout') }}" class="mega-menu" data-close="true"><i class="zmdi zmdi-power"></i></a>
                <a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a>
        </li>
      </ul>
    </nav>

<div class="mycustloading" style="display: none"><div class="mycustloadingBg"><i class="fa fa-spinner fa-spin"></i></div></div>
@extends('admin-panel.layout.sidebar')
@yield('content')
@extends('admin-panel.layout.footer') 