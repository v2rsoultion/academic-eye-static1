
<?php $__env->startSection('content'); ?>
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2><?php echo trans('language.menu_transport'); ?></h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>"><?php echo trans('language.menu_transport'); ?></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/Transport/Vehicle.svg'); ?>" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Vehicle</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="<?php echo e(url('admin-panel/transport/vehicle/add-vehicle')); ?>" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="<?php echo e(url('admin-panel/transport/vehicle/manage-vehicle')); ?>" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                        <!-- <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="#" title="">Option 1</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 2</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 3</a>
                              </li>
                            </ul>
                          </li>
                        </ul> -->
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="dashboard_div">
                        <div class="imgdash">
                          <img src="<?php echo URL::to('public/assets/images/Transport/Vehicle documents.svg'); ?>" alt="Student" >
                          </div>
                          <h4 class="">
                            <div class="tableCell" style="height: 64px;">
                              <div class="insidetable">Vehicle documents</div>
                            </div>
                          </h4>
                          <div class="clearfix"></div>
                          <a href="<?php echo e(url('admin-panel/transport/vehicle-documents/add-vehicle-document')); ?>" class="float-left" title="Add ">
                            <i class="fas fa-plus"></i> Add 
                          </a>
                          <a href="<?php echo e(url('admin-panel/transport/vehicle-documents/view-vehicle-documents')); ?>" class="float-right" title="View ">
                            <i class="fas fa-eye"></i>View 
                          </a>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="<?php echo URL::to('public/assets/images/Transport/Driver.svg'); ?>" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Driver</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="<?php echo e(url('admin-panel/transport/driver/add-driver')); ?>" class="float-left" title="Add ">
                              <i class="fas fa-plus"></i> Add 
                            </a>
                            <a href="<?php echo e(url('admin-panel/transport/driver/view-driver')); ?>" class="float-right" title="View ">
                              <i class="fas fa-eye"></i>View 
                            </a>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="<?php echo URL::to('public/assets/images/Transport/Conductor.svg'); ?>" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Conductor</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="<?php echo e(url('admin-panel/transport/conductor/add-conductor')); ?>" class="float-left" title="Add ">
                                <i class="fas fa-plus"></i> Add 
                              </a>
                              <a href="<?php echo e(url('admin-panel/transport/conductor/view-conductor')); ?>" class="float-right" title="View ">
                                <i class="fas fa-eye"></i>View 
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/Transport/Assign driver_conductor.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Assign driver/conductor</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                               <!--  <a href="" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a> -->
                                <a href="<?php echo e(url('admin-panel/transport/assign-driver-conductor/manage-driver-conductor')); ?>" class="cusa" style="width: 48%; margin: 15px auto;" title="View ">
                                  <i class="fas fa-eye"></i>Manage 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                           
                            <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Transport/Vehicle routes.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Vehicle routes</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <a href="<?php echo e(url('admin-panel/transport/vehicle-route/add-vehicle-route')); ?>" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a>
                                  <a href="<?php echo e(url('admin-panel/transport/vehicle-route/view-vehicle-routes')); ?>" class="float-right" title="View ">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                   <ul class="header-dropdown opendiv">
                                    <li class="dropdown">
                                      <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                      </button>
                                      <ul class="dropdown-menu dropdown-menu-right slideUp">
                                        <li>
                                          <a href="<?php echo e(url('admin-panel/transport/vehicle-route/map-route-vehicle')); ?>" title="">Map Route Vehicle</a>
                                        </li>
                                       
                                      </ul>
                                    </li>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Transport/Manage student staff vehicle.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Manage student/staff vehicle</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a>
                                  <a href="" class="float-right" title="View ">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                  <ul class="header-dropdown opendiv">
                                  <li class="dropdown">
                                  <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                  <i class="fas fa-ellipsis-v"></i>
                                  </button>
                                  <ul class="dropdown-menu dropdown-menu-right slideUp">
                                  <li>
                                     <a href="#" title="">Register Students</a>
                                  </li>
                                  <li>
                                    <a href="#" title="">Manage Students</a>
                                  </li>
                                  <li>
                                    <a href="#" title="">Manage Staff</a>
                                  </li>
                                   <li>
                                    <a href="#" title="">Report</a>
                                  </li>
                            </ul>
                          </li>
                        </ul>
                                </div>
                              </div>
                              <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Transport/One way transport.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">One way transport</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <a href="<?php echo e(url('admin-panel/transport/one-way-transport/request')); ?>" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Request 
                                  </a>
                                  <a href="<?php echo e(url('admin-panel/transport/one-way-transport/response')); ?>" class="float-right" title="View ">
                                    <i class="fas fa-eye"></i>Response 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              </div>
                              <div class="row">
                          <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/Transport/Track Vehicle.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Track Vehicle</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <!-- <a href="<?php echo e(url('admin-panel/transport/track-vehicle/add-track-vehicle')); ?>" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a> -->
                                <a href="<?php echo e(url('admin-panel/transport/track-vehicle/view-track-vehicle')); ?>" class="cusa" title="View " style="width: 48%; margin: 15px auto;">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                           
                            <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Transport/View history tracking.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">View history tracking</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                 <!--  <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="<?php echo e(url('admin-panel/transport/history-tracking/view-history-tracking')); ?>" class="cusa" title="View " style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Transport/Check student attendance.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Check student attendance</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                 <!--  <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="<?php echo e(url('admin-panel/transport/student-attendance/view-student-attendance')); ?>" class="cusa" title="View " style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Transport/Check report of vehicle.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Check report of vehicle</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <!-- <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="<?php echo e(url('admin-panel/transport/transport-report/check-vehicle-report')); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              </div>


                         <div class="row">
                          <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="<?php echo URL::to('public/assets/images/Transport/Manage Fees.svg'); ?>" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Manage Fees</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="<?php echo e(url('admin-panel/transport/manage-fees/add-fees-head')); ?>" class="float-left" title="Add ">
                                <i class="fas fa-plus"></i> Add Fees Head
                              </a>
                              <a href="<?php echo e(url('admin-panel/transport/manage-fees/view-fees-head')); ?>" class="float-right" title="View ">
                                <i class="fas fa-eye"></i>View Fees Head
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/Transport/Fees Report.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Fees Report</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <!-- <a href="" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a> -->
                                <a href="<?php echo e(url('admin-panel/transport/transport-report/view-fees-report')); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                           
                            <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Transport/Staff attendance report.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Staff attendance report</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                 <!--  <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="<?php echo e(url('admin-panel/transport/transport-report/view-staff-attendence-report')); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              
                              
                              </div>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>