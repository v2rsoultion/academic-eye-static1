<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\SubjectClassMapping\SubjectClassmapping; // Model 
use App\Model\Subject\Subject; // Model 
use App\Model\Classes\Classes; // Model
use Yajra\Datatables\Datatables;

class SubjectClassmappingController extends Controller
{
    /**
     *  View page for Subject Class mapping
     *  @Pratyush on 07 Aug 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $mapp  = $arr_class = [];
        $arr_medium         = \Config::get('custom.medium_type');
        $mapp['arr_medium'] = add_blank_option($arr_medium, 'Select Medium');
        $mapp['arr_class']  = add_blank_option($arr_class, 'Select Class');
        $data = array(
            'page_title'    => trans('language.view_map_subject'),
            'redirect_url'  => url('admin-panel/subject-class-mapping/view-subject-class-mapping'),
            'login_info'    => $loginInfo,
            'mapp'          => $mapp
        );
        return view('admin-panel.map-subject-to-class.index')->with($data);
    }

    /**
     *  Get Subject Class mapping's Data for view page(Datatables)
     *  @Pratyush on 07 Aug 2018.
    **/
    public function anyData(Request $request)
    {
        if($request->get('medium_type') == null && $request->get('class_id') == null) {
            $class = [];
        } else {
        $class = Classes::where(function($query) use ($request)  {
            if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
            {
                $query->where('class_id', "=", $request->get('class_id'));
            }
            if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null)
            {
                $query->where('medium_type', "=", $request->get('medium_type'));
            }
        })->with('getSections')->get();
        }
        return Datatables::of($class)
        ->addColumn('no_of_subject', function ($class)
        {
            return SubjectClassmapping::where('class_id',$class->class_id)->count();
            
        })
        ->addColumn('sections', function ($class)
        {
            $sections = '';
            foreach($class['getSections'] as $key => $value){
                $sections .= $value->section_name.',';
            }
            $sections = rtrim($sections,',');
            return $sections;
            
        })
        ->addColumn('action', function ($class)
        {
            $encrypted_class_id = get_encrypted_value($class->class_id, true);
                return '<div class="dropdown">
            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="zmdi zmdi-label"></i>
            <span class="caret"></span>
            </button>
                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                <li><a href="map-subject/' . $encrypted_class_id . '" ">Map Subject</a></li>
                <li><a href="view-report/' . $encrypted_class_id . '" ">View Report</a></li>
                </ul>
            </div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  View page for Subject Class mapping - Map Subject
     *  @Pratyush on 07 Aug 2018
    **/
    public function getMapSubject($id)
    {
        $loginInfo = get_loggedin_user_data();
        $class_name = get_class_name_by_id($id); 
        $data = array(
            'page_title'    => trans('language.map_subject'),
            'redirect_url'  => url('admin-panel/subject-class-mapping/view-subject-class-mapping'),
            'login_info'    => $loginInfo,
            'save_url'		=> url('admin-panel/class-subject-mapping/save'),
            'class_id'		=> $id,
            'class_name'    => $class_name
        );
        return view('admin-panel.map-subject-to-class.map_subject')->with($data);
    }

    /**
     *  Get Subject Class mapping's Data for view page(Datatables)
     *  @Pratyush on 07 Aug 2018.
    **/
    public function anyMapSubjectData(Request $request)
    {
    	
        $decrypted_class_id	= get_decrypted_value($request->get('class_id'), true);
        $class    = Classes::find($decrypted_class_id);
        if ($class)
        {
            $subject  			= Subject::where([['medium_type', '=', $class['medium_type']]])->orderBy('subject_id', 'ASC')->get();
            $subject_ids 		= SubjectClassmapping::select('subject_id')->where('class_id',$decrypted_class_id)->get();
            $subjectArr 		= array(); 
            $subject_ids_Arr 	= array();
            
            foreach($subject_ids as $subject_id){
                $subject_ids_Arr[] = $subject_id->subject_id;
            }
            
            foreach($subject as $key => $value){
                $subjectArr[$key] = $value;
                $subjectArr[$key]['subject_arr'] = $subject_ids_Arr;
            }
            
            return Datatables::of($subjectArr)
        		->addColumn('checkbox', function ($subjectArr)
                {
                	$check = '';
                	if(!empty($subjectArr['subject_arr'])){

                		if(in_array($subjectArr->subject_id, $subjectArr['subject_arr'])){
                			$check = 'checked';
                		}
                	}
                    return '<label class="option block mn">
                        <input type="checkbox" name="check[]" class="check" value="'.$subjectArr->subject_id.'" '.$check.'>
                        <span class="checkbox mn"></span>
                        </label>';
                    // return '<div class="checkbox"><input name="check[]" type="checkbox" class="check" value="'.$subjectArr->subject_id.'" '.$check.'><label for="checkbox10"></label></div>';
                    
                })
                ->addColumn('subjects', function ($subjectArr)
                {
        			return $subjectArr->subject_code.' - '.$subjectArr->subject_name;
                })->rawColumns(['checkbox' => 'checkbox','subjects' => 'subjects'])->addIndexColumn()
                ->make(true);
        }
        else
        {
            $error_message = "Class not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Add and update subject class mapping's data
     *  @Pratyush on 07 Aug 2018.
    **/
    public function save(Request $request)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_class_id			= get_decrypted_value($request->get('class_id'), true);
        $class    = Classes::find($decrypted_class_id);
        $admin_id = $loginInfo['admin_id'];
		if(!empty($request->get('class_id'))){
            if(empty(Input::has('check'))){
               SubjectClassmapping::where('class_id',$decrypted_class_id)->delete(); 
               $success_msg = 'Subjects removed successfully.';
               return redirect('admin-panel/class-subject-mapping/map-subject/'.$request->get('class_id'))->withSuccess($success_msg);
            }
			if(!empty(Input::has('check')) && count(Input::get('check')) != 0){
				$success_msg = 'Subjects assign successfully.';
				$previous_records		= SubjectClassmapping::where('class_id',$decrypted_class_id)->count();
				if($previous_records != 0){
					SubjectClassmapping::where('class_id',$decrypted_class_id)->delete();
				}	
					
				try
	            {

	            	foreach (Input::get('check') as $value){

	            		$subject_mapping = new SubjectClassmapping;		
	            		$subject_mapping->admin_id       = $admin_id;
		                $subject_mapping->update_by      = $loginInfo['admin_id'];
		                $subject_mapping->class_id 	 	 = $decrypted_class_id;
                        $subject_mapping->subject_id 	 = $value;
                        $subject_mapping->medium_type    = $class['medium_type'];
		                $subject_mapping->save();
	            	}
	                
	            }
	            catch (\Exception $e)
	            {
	                //failed logic here
	                DB::rollback();
	                $error_message = $e->getMessage();
	                return redirect()->back()->withErrors($error_message);
	            }
	            DB::commit();
				return redirect('admin-panel/class-subject-mapping/map-subject/'.$request->get('class_id'))->withSuccess($success_msg);
    		}else{
    			return redirect()->back();
    		}
    	}else{
    		return redirect()->back();
    	}
	}

	/**
     *  View page for view report
     *  @Pratyush on 07 Aug 2018
    **/
    public function getViewReport($id)
    {
        $loginInfo = get_loggedin_user_data();
        $class_name = get_class_name_by_id($id); 
        $data = array(
            'page_title'    => trans('language.map_subject'),
            'redirect_url'  => url('admin-panel/subject-class-mapping/view-subject-class-mapping'),
            'login_info'    => $loginInfo,
            'save_url'		=> url('admin-panel/class-subject-mapping/save'),
            'class_id'		=> $id,
            'class_name'    => $class_name
        );
        return view('admin-panel.map-subject-to-class.view_report')->with($data);
    }

    /**
     *  Get Subject for view report page(Datatables)
     *  @Pratyush on 07 Aug 2018.
    **/
    public function anyViewReportData(Request $request)
    {

        $loginInfo 				= get_loggedin_user_data();
        $decrypted_class_id		= get_decrypted_value($request->get('class_id'), true);
        $subject  				= SubjectClassmapping::where('class_id',$decrypted_class_id)->with('getSubjects')->orderBy('subject_class_map_id', 'ASC')->get();
        
        return Datatables::of($subject)
                ->addColumn('subjects', function ($subject)
                {
        			return $subject['getSubjects']->subject_code.' - '.$subject['getSubjects']->subject_name;
                })->rawColumns(['subjects' => 'subjects'])->addIndexColumn()
                ->make(true);
    }
}