@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  .nav_item7
  {
    min-height: 230px !important;
  }
  .imgclass:hover .active123 .tab_images123{
    left: 50px !important;
  }
  .imgclass:hover .tab_images123{
    left: 50px !important;
  }
  .nav_item3 {
    min-height: 255px !important;
  }
  .side-gap{
    width: 75% !important;
  }
</style>
<!--  Main content here -->

<section class="content">
      <div class="block-header">
        <div class="row">
          <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>View Profile</h2>
          </div>
          <div class="col-lg-5 col-md-6 col-sm-12 line">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/staff') !!}">{!! trans('language.staff') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff/view-staff') !!}">{!! trans('language.view_staff') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/staff/view-profile') !!}">{!! trans('language.profile') !!}</a></li>
            </ul>
          </div>
        </div>
      </div>

<div class="container-fluid">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card profile_card1">
              <div class="body text-center pro_body_center1 form-gap ">
                <div class="row clearfix">
                  <div class="col-lg-2 col-md-2 col-sm-12 text-left">
                    <div class="profile-image"> <img src="{!! URL::to('public/Wallpapers/1.jpg') !!}" alt="" class="profile_image"> </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="std1">
                      <div class="profile_detail_3">
                          <i class="far fa-hand-point-right"></i>
                        <label> <span>Name: </span> Mahesh Kumar</label>
                      </div>
                      <div class="profile_detail_3">
                          <i class="far fa-hand-point-right"></i>
                        <label> <span>Designation : </span> Teacher</label>
                      </div>   
                       <div class="profile_detail_3">
                          <i class="far fa-hand-point-right"></i>
                        <label> <span>Staff Role : </span> Teacher</label>
                      </div>
                      <div class="profile_detail_3">
                          <i class="far fa-hand-point-right"></i>
                        <label> <span> Email : </span> Maheshkumar44@gmail.com</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="std2">
                      
                      
                       <div class="profile_detail_3">
                          <i class="far fa-hand-point-right"></i>
                        <label> <span> DOB : </span> 30 July 1998</label>
                      </div>
                       <div class="profile_detail_3">
                          <i class="far fa-hand-point-right"></i>
                        <label> <span>Gender : </span> Male</label>
                      </div>
                      <div class="profile_detail_3">
                          <i class="far fa-hand-point-right"></i>
                        <label> <span>Mobile Number : </span> +91 9008090080</label>
                      </div>
                      
                    
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


<div class="container-fluid">
        <div class="row clearfix">
          <div class="col-lg-2 col-md-2 col-sm-12">
            <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 tab1">
                <div class="card side-gap">
                  <ul class="ul_underline_1">
                    <li class="nav-item imgclass">
                      <a class="nav-link  active active123" data-toggle="tab" href="#personal">
                        <div class="pro_image_tabs1">
                          <img src="{!! URL::to('public/Wallpapers/Personal-informastion.png') !!}" class="tab_images123">
                          <img src="{!! URL::to('public/Wallpapers/Personal-informastion1.png') !!}" class="tab_images2123">
                        </div>
                        Personal Information
                      </a>
                    </li>
                    <li class="nav-item imgclass">
                      <a class="nav-link active123 " data-toggle="tab" href="#address_info">
                        <div class="pro_image_tabs1">
                          <img src="{!! URL::to('public/Wallpapers/Parent-Information.png') !!}" class="tab_images123">
                          <img src="{!! URL::to('public/Wallpapers/Parent-Information1.png') !!}" class="tab_images2123">
                        </div>
                        Address
                      </a>
                    </li>
                  
                    <li class="nav-item imgclass">
                      <a class="nav-link active123 " data-toggle="tab" href="#documents">
                        <div class="pro_image_tabs1">
                          <img src="{!! URL::to('public/Wallpapers/Documents.png') !!}" class="tab_images123">
                          <img src="{!! URL::to('public/Wallpapers/Documents1.png') !!}" class="tab_images2123">
                        </div>
                        Documents
                      </a>
                    </li>
                    <li class="nav-item imgclass">
                      <a class="nav-link active123 " data-toggle="tab" href="#attendance">
                        <div class="pro_image_tabs1">
                          <img src="{!! URL::to('public/Wallpapers/Attendance.png') !!}" class="tab_images123">
                          <img src="{!! URL::to('public/Wallpapers/Attendance1.png') !!}" class="tab_images2123">
                        </div>
                        Attendance
                      </a>
                    </li>
                    <li class="nav-item imgclass">
                      <a class="nav-link active123" data-toggle="tab" href="#leave">
                        <div class="pro_image_tabs1">
                          <img src="{!! URL::to('public/Wallpapers/Leave-Application.png') !!}" class="tab_images123">
                          <img src="{!! URL::to('public/Wallpapers/Leave-Application1.png') !!}" class="tab_images2123">
                        </div>
                        Leave Application
                      </a>
                    </li>
                  
                    <li class="nav-item imgclass">
                      <a class="nav-link active123" data-toggle="tab" href="#marks">
                        <div class="pro_image_tabs1">
                          <img src="{!! URL::to('public/Wallpapers/Marks.png') !!}" class="tab_images123">
                          <img src="{!! URL::to('public/Wallpapers/Marks1.png') !!}" class="tab_images2123">
                        </div>
                        Shift
                      </a>
                    </li>
                    <li class="nav-item imgclass">
                      <a class="nav-link active123" data-toggle="tab" href="#remarks">
                        <div class="pro_image_tabs1">
                          <img src="{!! URL::to('public/Wallpapers/Remarks.png') !!}" class="tab_images123">
                          <img src="{!! URL::to('public/Wallpapers/Remarks1.png') !!}" class="tab_images2123">
                        </div>
                        Schedule
                      </a>
                    </li>
                    <!-- <li class="nav-item imgclass" id="">
                      <a class="nav-link active123" data-toggle="tab" href="#issued">
                        <div class="pro_image_tabs1">
                          <img src="{!! URL::to('public/Wallpapers/Issued-Certificates.png') !!}" class="tab_images123">
                          <img src="{!! URL::to('public/Wallpapers/Issued-Certificates1.png') !!}" class="tab_images2123">
                        </div>
                        Issued Certificates
                      </a>
                    </li> -->
                    <!-- <li class="nav-item"><a class="nav-link active3" data-toggle="tab" href="#parent_info">Parent Information</a></li>
                      <li class="nav-item"><a class="nav-link active3" data-toggle="tab" href="#academics">Academics Information</a></li>
                      <li class="nav-item"><a class="nav-link active3" data-toggle="tab" href="#documents">Documents</a></li>
                      <li class="nav-item"><a class="nav-link active3" data-toggle="tab" href="#attendance">Attendance</a></li>
                      <li class="nav-item"><a class="nav-link active3" data-toggle="tab" href="#leave">Leave Application</a></li>
                      <li class="nav-item"><a class="nav-link active3" data-toggle="tab" href="#fees">Fees</a></li>
                      <li class="nav-item"><a class="nav-link active3" data-toggle="tab" href="#marks">Marks</a>
                      </li>
                      <li class="nav-item"><a class="nav-link active3" data-toggle="tab" href="#remarks">Remarks</a></li> 
                      <li class="nav-item"><a class="nav-link active3" data-toggle="tab" href="#issued">Issued Certificates</a></li> -->
                  </ul>
                  <!--  <div class="ui_marks1"></div> -->
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-10 col-md-10 col-sm-12" style="margin-left:-73px;">
            <div class="">
              <div class="tab-content">
                <div class="tab-pane body active" id="personal" style="margin-left: 20px !important;">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content">
                          <div class="tab-pane active" id="classlist">
                            <div class="row clearfix">
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card border_info_tabs_1 card_top_border1" id="">
                                  <div class="body" >
                                    <!-- Nav tabs -->
                                    <!-- <ul class="nav nav-tabs" role="tablist">
                                      <li class="nav-item" style="margin-left: -10px;"><a class="nav-link active" data-toggle="tab" href="#home_with_icon_title"> <i class="zmdi zmdi-home"></i> Basic Info </a></li>
                                      <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile_with_icon_title"><i class="zmdi zmdi-account"></i> Other Info </a></li>
                                      <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#address_with_icon_title"> <i class="zmdi zmdi-home"></i> Address Info </a></li>
                                      <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#health_with_icon_title"><i class="zmdi zmdi-account"></i> Health Info </a></li>
                                      </ul> -->
                                    <!-- Tab panes -->
                                    <div class="tab-content" style="background-color: #fff;
    width: 1035px !important;">
                                      <div class="nav_item7">
                                        <div class="nav_item1">
                                          <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Basic Info </a>
                                        </div>
                                        <div class="nav_item2">
                                          <div role="tabpanel" class="tab-pane" id="health_with_icon_title">
                                            <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">Name :</b> <span class="profile_detail_span_6">Mahesh Kumar</span>
                                              </div>
                                              <!-- <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b class="profile_detail_bold_2">Height :</b> <span class="profile_detail_span_6">6 ft</span>
                                                </div> -->
                                            </div>



                                            <div  class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b class="profile_detail_bold_2">Designation :</b> <span class="profile_detail_span_6">Teacher</span>
                                              </div>
                                            </div>
                                            <div  class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b class="profile_detail_bold_2">Staff Role  :</b> <span class="profile_detail_span_6">Teacher</span>
                                              </div>
                                            </div>
                                            <div  class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b class="profile_detail_bold_2">Email  :</b> <span class="profile_detail_span_6">Maheshkumar44@gmail.com</span>
                                              </div>
                                            </div>
                                            <div  class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b class="profile_detail_bold_2">DOB  :</b> <span class="profile_detail_span_6">30 July 1998</span>
                                              </div>
                                            </div>
                                            <div  class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b class="profile_detail_bold_2">Gender  :</b> 
                                                <span class="profile_detail_span_6">
                                                Male
                                                </span>
                                              </div>
                                            </div>
                                            <div  class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b class="profile_detail_bold_2">Mobile Number  :</b> <span class="profile_detail_span_6">+91 9008090080</span>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="nav_item3">
                                        <div class="nav_item1">
                                          <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Other Info </a>
                                        </div>
                                        <div class="nav_item2">
                                          <div role="tabpanel" class="tab-pane" id="profile_with_icon_title">
                                            <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b  class="profile_detail_bold_2">Father Name/Husband Name :</b> <span class="profile_detail_span_6">Ankit Kumar</span>
                                              </div>
                                            </div>
                                            <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b  class="profile_detail_bold_2">Mother Name :</b> <span class="profile_detail_span_6">Asha </span>
                                              </div>
                                            </div>
                                            <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b  class="profile_detail_bold_2">Contact Number:</b> <span class="profile_detail_span_6">+91 9008290023</span>
                                              </div>
                                            </div>
                                            <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b  class="profile_detail_bold_2">Blood Group :</b> <span class="profile_detail_span_6">A+</span>
                                              </div>
                                            </div>
                                            <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b  class="profile_detail_bold_2">Caste :</b> <span class="profile_detail_span_6">Kumar</span>
                                              </div>
                                            </div>
                                            <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b  class="profile_detail_bold_2">Religion :</b> <span class="profile_detail_span_6">Hindu</span>
                                              </div>
                                            </div>
                                            <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b  class="profile_detail_bold_2">Nationality :</b> <span class="profile_detail_span_6">Indian</span>
                                              </div>
                                            </div>
                                           <!--  <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b  class="profile_detail_bold_2">Sibling Name :</b> <span class="profile_detail_span_6">11010</span>
                                              </div>
                                            </div>
                                            <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b  class="profile_detail_bold_2">Sibling Class :</b> <span class="profile_detail_span_6">XI</span>
                                              </div>
                                            </div> -->
                                           <!--  <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b  class="profile_detail_bold_2">Adhaar Card Number :</b> <span class="profile_detail_span_6">11010-552452-25</span>
                                              </div>
                                            </div> -->
                                            <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b  class="profile_detail_bold_2">Married Status:</b> <span class="profile_detail_span_6">Yes</span>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                              
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>


                <div class="tab-pane body" id="academics">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content">
                          <div class="tab-pane active" id="classlist">
                            <div class="">
                              <div class="card border_info_tabs_1 card_top_border1">
                                <div class="row clearfix">
                                  <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="card">
                                      <div class="body">
                                        <div class="nav_item3">
                                          <div class="nav_item1">
                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Academic Current Info </a>
                                          </div>
                                          <div class="nav_item2">
                                            <div role="tabpanel" class="tab-pane in active" id="academic_current_info">
                                              <div class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Admit Session :</b> <span class="profile_detail_span_6">2017-2018</span>
                                                </div>
                                              </div>
                                              <div class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Admit Class :</b> <span class="profile_detail_span_6">XII</span>
                                                </div>
                                              </div>
                                              <div class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Admit Section :</b> <span class="profile_detail_span_6">A</span>
                                                </div>
                                              </div>
                                              <div class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">House/Group  :</b> <span class="profile_detail_span_6">Group 1</span>
                                                </div>
                                              </div>
                                              <div class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Current Session :</b> <span  class="profile_detail_span_6">2016-2017</span>
                                                </div>
                                              </div>
                                              <div class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Current Class  :</b> <span class="profile_detail_span_6">XI</span>
                                                </div>
                                              </div>
                                              <div class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Current Section :</b> <span class="profile_detail_span_6">A</span>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="nav_item3">
                                          <div class="nav_item1">
                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Academic History Info </a>
                                          </div>
                                          <div class="nav_item2">
                                            <div role="tabpanel" class="tab-pane" id="academic_history_info">
                                              <div class="table-responsive">
                                                <table class="table table-hover m-b-0 c_list">
                                                  <thead>
                                                    <tr>
                                                      <th>S No</th>
                                                      <th>Name-section</th>
                                                      <th>Year</th>
                                                      <th>Rank</th>
                                                      <th>Percentage</th>
                                                      <th>CC Activity Winner</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                    <tr>
                                                      <td>1</td>
                                                      <td>VII-A</td>
                                                      <td>2017/7/17</td>
                                                      <td>11</td>
                                                      <td>60%</td>
                                                      <td>----</td>
                                                    </tr>
                                                    <tr>
                                                      <td>2</td>
                                                      <td>VII-A</td>
                                                      <td>2017/7/17</td>
                                                      <td>11</td>
                                                      <td>60%</td>
                                                      <td>----</td>
                                                    </tr>
                                                    <tr>
                                                      <td>3</td>
                                                      <td>VII-A</td>
                                                      <td>2017/7/17</td>
                                                      <td>11</td>
                                                      <td>60%</td>
                                                      <td>----</td>
                                                    </tr>
                                                    <tr>
                                                      <td>4</td>
                                                      <td>VII-A</td>
                                                      <td>2017/7/17</td>
                                                      <td>11</td>
                                                      <td>60%</td>
                                                      <td>----</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane body" id="address_info" style="margin-left: 20px !important;">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;
    width: 1035px !important;">
                          <div class="tab-pane active" id="classlist">
                            <div class="">
                              <div class="card border_info_tabs_1 card_top_border1">
                                <div class="row clearfix">
                                  <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="card">
                                      <div class="body">
                                        <div class="nav_item4">
                                        <div class="nav_item1">
                                          <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Address Info </a>
                                        </div>
                                        <div class="nav_item2">
                                          <div role="tabpanel" class="tab-pane" id="address_with_icon_title">
                                            <div class="parent_address_info_1">
                                              <div class="parent_address_temparay_info_1">
                                                <h6>Temporary Address :</h6>
                                                <div class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">Address :</b> <span class="profile_detail_span_6">Jodhpur,Rajasthan</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">City :</b> <span class="profile_detail_span_6">Jaipur</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">State :</b> <span class="profile_detail_span_6">Rajasthan</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">Country :</b> <span class="profile_detail_span_6">India</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">Pincode :</b> <span class="profile_detail_span_6">220123</span>
                                                  </div>
                                                </div>
                                                <hr>
                                              </div>
                                              <div style="" class="pi_1">
                                                <h6>Permanent Address :</h6>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">Address :</b> <span class="profile_detail_span_6">Jodhpur,Rajasthan</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">City :</b> <span class="profile_detail_span_6">Jaipur</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">State :</b> <span class="profile_detail_span_6">Rajasthan</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">Country :</b> <span class="profile_detail_span_6">India</span>
                                                  </div>
                                                </div>
                                                <div class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">Pincode :</b> <span class="profile_detail_span_6">220123</span>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                        <!-- <div class="nav_item5">
                                          <div class="nav_item1">
                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Father Info </a>
                                          </div>
                                          <div class="nav_item2">
                                            <div class="parent_father_info_1">
                                              <div class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Father Name :</b> <span class="profile_detail_span_6">Rajesh Kumar</span>
                                                </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Father Mobile Number :</b> <span class="profile_detail_span_6">91+ 124512154</span>
                                                </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Father Email Address :</b> <span class="profile_detail_span_6">rajeshkumar@gmail.com</span>
                                                </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Father Annual Salary :</b> <span class="profile_detail_span_6">2.5 Lac</span>
                                                </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Father's Occupation :</b> <span class="profile_detail_span_6">Army</span>
                                                </div>
                                              </div>
                                              <hr>
                                            </div>
                                          </div>
                                        </div> -->
                                        <!-- <div class="nav_item5">
                                          <div class="nav_item1">
                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Mother Info </a>
                                          </div>
                                          <div class="nav_item2">
                                            <div class="parent_mother_info_1">
                                              <h6></h6>
                                              <div  class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Mother Name :</b> <span class="profile_detail_span_6">Kiran Kumari</span>
                                                </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Mother Mobile Number :</b> <span class="profile_detail_span_6">91+ 124512154</span>
                                                </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Mother Email Address :</b> <span class="profile_detail_span_6">kirankumari@gmail.com</span>
                                                </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Mother Annual Salary :</b> <span class="profile_detail_span_6">2.5 Lac</span>
                                                </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Mother's Occupation :</b> <span class="profile_detail_span_6">Doctor</span>
                                                </div>
                                              </div>
                                              <hr>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="nav_item6">
                                          <div class="nav_item1">
                                            <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Guardian Info </a>
                                          </div>
                                          <div class="nav_item2">
                                            <div class="parent_guardian_info_1">
                                              <h6></h6>
                                              <div class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Guardian Name :</b> <span  class="profile_detail_span_6">Radha Kumari</span>
                                                </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Guardian Mobile Number :</b> <span class="profile_detail_span_6">91+ 124512154</span>
                                                </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Guardian Email Address :</b> <span class="profile_detail_span_6">radhakumari@gmail.com</span>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane body" id="marks">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content">
                          <div class="tab-pane active" id="classlist">
                            <div class="card border_info_tabs_1 card_top_border1">
                              <div class="body">
                                <!-- <div class="table-responsive">
                                  <table class="table table-hover m-b-0 c_list">
                                      <thead>
                                          <tr>
                                              <th>
                                                  <div class="">
                                                      <input id="checkbox1" type="checkbox" class="checkbox2">
                                                  </div>
                                              </th>
                                              <th>S NO</th>
                                              <th>Height</th>
                                              <th>Weight</th>
                                              <th>Blood Group</th>
                                              <th>Vision Right</th>
                                              <th>VIsion Left</th>
                                              <th>Medical Issue</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr>
                                              <td>
                                                  <div class="">
                                                      <input id="checkbox2" type="checkbox" class="checkbox1">
                                                  </div>
                                              </td>
                                              <td>1</td>
                                              <td>6 ft</td>
                                              <td>50 Kg</td>
                                              <td>A+</td>
                                              <td>?</td>
                                              <td>?</td>
                                              <td>None</td>
                                          </tr>                                           
                                      </tbody>
                                  </table>
                                  </div> -->
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane body" id="documents" style="margin-left: 20px !important;">
                  <!-- <div class=""> -->
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;
    width: 1035px !important;">
                          <div class="tab-pane active" id="classlist">
                            <div class="">
                              <div class="card border_info_tabs_1 card_top_border1">
                                <div class="body">
                                  <div class="nav_item4">
                                   <div class="nav_item1">
                                          <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Document Info </a>
                                        </div>
                                  <table class="table table-hover m-b-0 c_list">
                                    <thead>
                                      <tr>
                                        <th>S No</th>
                                        <th>Category</th>
                                        <th>Document Details</th>
                                        <th>Submit Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>aadhaar card</td>
                                        <td><button  class="btn btn-info search_button28"  data-toggle="modal" data-target="#largeModal3">View Details</button></td>
                                        <td>2201/20/1</td>
                                        <td>Pending</td>
                                        <td>
                                          <div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Disapproved"> <i class="fas fa-minus-circle"></i> </div>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>birth certificate</td>
                                        <td><button  class="btn btn-info search_button28"  data-toggle="modal" data-target="#largeModal3">View Details</button></td>
                                        <td>2201/20/1</td>
                                        <td>Pending</td>
                                        <td>
                                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>3</td>
                                        <td>birth certificate</td>
                                        <td><button  class="btn btn-info search_button28"  data-toggle="modal" data-target="#largeModal3">View Details</button></td>
                                        <td>2201/20/1</td>
                                        <td>Pending</td>
                                        <td>
                                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>4</td>
                                        <td>birth certificate</td>
                                        <td><button  class="btn btn-info search_button28"  data-toggle="modal" data-target="#largeModal3">View Details</button></td>
                                        <td>2201/20/1</td>
                                        <td>Pending</td>
                                        <td>
                                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>


                <div class="tab-pane body active" id="attendance" style="margin-left: 20px !important;">
                   <div class="tab-content" style="background-color: #fff;
    width: 1035px !important;">
                                      <div class="nav_item7">
                                  <div class="nav_item1">
                                          <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Attendance Info </a>
                                        </div>
                                      </div>
                                    </div>
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="card border_info_tabs_1 card_top_border1">
                          <div class="body">
                            <button class="btn btn-primary btn-sm btn-round waves-effect" id="change-view-today">today</button>
                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-day" >Day</button>
                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-week">Week</button>
                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-month">Month</button>
                            <div id="calendar" class="m-t-15"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- <div class="tab-pane body active" id="leave" style="margin-left: 20px !important;">
                   <div class="tab-content" style="background-color: #fff;
    width: 1035px !important;">
                                      <div class="nav_item7">
                                  <div class="nav_item1">
                                          <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Attendance Info </a>
                                        </div>
                                      </div>
                                    </div> -->
                 <!--  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content">
                          <div class="tab-pane active" id="classlist">
                            <div class="">
                              <div class="card border_info_tabs_1 card_top_border1">
                                <div class="body">
                                  <table class="table table-hover m-b-0 c_list">
                                    <thead>
                                      <tr>
                                        <th>S No</th>
                                        <th>Reason</th>
                                        <th>Date range</th>
                                        <th>Attachment</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Lorem Ipsum is simply ..</td>
                                        <td>1 july 2018 - 5 July 2018</td>
                                        <td>
                                          <a  href="http://v2rsolution.co.in/school/school-management/student-dairy-admin/assets/images/profile_av.jpg" class="acher_1">view attachment</a>
                                        </td>
                                        <td>Disapproved</td>
                                        <td>
                                          <div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Disapproved"> <i class="fas fa-minus-circle"></i> </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Lorem Ipsum is simply ..</td>
                                        <td>1 july 2018 - 5 July 2018</td>
                                        <td>
                                          <a  href="http://v2rsolution.co.in/school/school-management/student-dairy-admin/assets/images/profile_av.jpg" class="acher_1">view attachment</a>
                                        </td>
                                        <td>Approved</td>
                                        <td>
                                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>3</td>
                                        <td>Lorem Ipsum is simply ..</td>
                                        <td>1 july 2018 - 5 July 2018</td>
                                        <td>
                                          <a  href="http://v2rsolution.co.in/school/school-management/student-dairy-admin/assets/images/profile_av.jpg" class="acher_1">view attachment</a>
                                        </td>
                                        <td>Approved</td>
                                        <td>
                                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>4</td>
                                        <td>Lorem Ipsum is simply ..</td>
                                        <td>1 july 2018 - 5 July 2018</td>
                                        <td>
                                          <a  href="http://v2rsolution.co.in/school/school-management/student-dairy-admin/assets/images/profile_av.jpg" class="acher_1">view attachment</a>
                                        </td>
                                        <td>Disapproved</td>
                                        <td>
                                          <div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Disapproved"> <i class="fas fa-minus-circle"></i> </div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> -->
                <!-- </div> -->
                <!-- <div class="tab-pane body" id="fees">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content">
                          <div class="tab-pane active" id="classlist">
                            <div class="card border_info_tabs_1 card_top_border1">
                              <div class="body">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                      <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        Fees Detail
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                      <div class="body"  style="padding:0px;">
                                        <div class="table-responsive">
                                          <table class="table table-hover m-b-0 c_list">
                                            <thead>
                                              <tr>
                                                <th>S No</th>
                                                <th>Total Fees</th>
                                                <th>Total Paid</th>
                                                <th>Total Dues</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              <tr>
                                                <td>1</td>
                                                <td>10500</td>
                                                <td>10000</td>
                                                <td>500</td>
                                              </tr>
                                              <tr>
                                                <td>2</td>
                                                <td>10500</td>
                                                <td>10000</td>
                                                <td>500</td>
                                              </tr>
                                              <tr>
                                                <td>3</td>
                                                <td>10500</td>
                                                <td>10000</td>
                                                <td>500</td>
                                              </tr>
                                              <tr>
                                                <td>4</td>
                                                <td>10500</td>
                                                <td>10000</td>
                                                <td>500</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                      <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed">
                                        Payment Detail
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                      <div class="body" style="padding:0px;">
                                        <table class="table table-hover m-b-0 c_list">
                                          <thead>
                                            <tr>
                                              <th>S No</th>
                                              <th>Receipt No</th>
                                              <th>Particulars </th>
                                              <th>Amount</th>
                                              <th>Date</th>
                                              <th>Transaction Id</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                              <td>1</td>
                                              <td>22012100</td>
                                              <td>----</td>
                                              <td>15000</td>
                                              <td>2018/7/17</td>
                                              <td>FSGHAF464655</td>
                                            </tr>
                                            <tr>
                                              <td>2</td>
                                              <td>22012100</td>
                                              <td>----</td>
                                              <td>15000</td>
                                              <td>2018/7/17</td>
                                              <td>FSGHAF464655</td>
                                            </tr>
                                            <tr>
                                              <td>3</td>
                                              <td>22012100</td>
                                              <td>----</td>
                                              <td>15000</td>
                                              <td>2018/7/17</td>
                                              <td>FSGHAF464655</td>
                                            </tr>
                                            <tr>
                                              <td>4</td>
                                              <td>22012100</td>
                                              <td>----</td>
                                              <td>15000</td>
                                              <td>2018/7/17</td>
                                              <td>FSGHAF464655</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                      <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed">
                                        Due Amount
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                                      <div class="body" style="padding:0px;">
                                        <table class="table table-hover m-b-0 c_list">
                                          <thead>
                                            <tr>
                                              <th>S No</th>
                                              <th>Particulars </th>
                                              <th>Amount</th>
                                              <th>Date</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                              <td>1</td>
                                              <td>----</td>
                                              <td>17000</td>
                                              <td>2016/7/17</td>
                                            </tr>
                                            <tr>
                                              <td>2</td>
                                              <td>----</td>
                                              <td>17000</td>
                                              <td>2016/7/17</td>
                                            </tr>
                                            <tr>
                                              <td>3</td>
                                              <td>----</td>
                                              <td>17000</td>
                                              <td>2016/7/17</td>
                                            </tr>
                                            <tr>
                                              <td>4</td>
                                              <td>----</td>
                                              <td>17000</td>
                                              <td>2016/7/17</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFour">
                                      <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour" class="collapsed">
                                        fees details
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
                                      <div class="body" style="padding:0px;">
                                        <table class="table table-hover m-b-0 c_list">
                                          <thead>
                                            <tr>
                                              <th>S No</th>
                                              <th>Receipt No</th>
                                              <th>Particulars </th>
                                              <th>Amount</th>
                                              <th>Date</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                              <td>1</td>
                                              <td>11012201</td>
                                              <td>----</td>
                                              <td>21000</td>
                                              <td>2001/17/7</td>
                                            </tr>
                                            <tr>
                                              <td>2</td>
                                              <td>11012201</td>
                                              <td>----</td>
                                              <td>21000</td>
                                              <td>2001/17/7</td>
                                            </tr>
                                            <tr>
                                              <td>3</td>
                                              <td>11012201</td>
                                              <td>----</td>
                                              <td>21000</td>
                                              <td>2001/17/7</td>
                                            </tr>
                                            <tr>
                                              <td>4</td>
                                              <td>11012201</td>
                                              <td>----</td>
                                              <td>21000</td>
                                              <td>2001/17/7</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFive">
                                      <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive" class="collapsed">
                                        Applied Fees
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFive">
                                      <div class="body" style="padding:0px;">
                                        <table class="table table-hover m-b-0 c_list">
                                          <thead>
                                            <tr>
                                              <th>S No</th>
                                              <th>Particulars </th>
                                              <th>Amount</th>
                                              <th>Type</th>
                                              <th>Date</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                              <td>1</td>
                                              <td>-----</td>
                                              <td>1000</td>
                                              <td>Tuition</td>
                                              <td>2001/7/7</td>
                                            </tr>
                                            <tr>
                                              <td>3</td>
                                              <td>-----</td>
                                              <td>1000</td>
                                              <td>Tuition</td>
                                              <td>2001/7/7</td>
                                            </tr>
                                            <tr>
                                              <td>4</td>
                                              <td>-----</td>
                                              <td>1000</td>
                                              <td>Tuition</td>
                                              <td>2001/7/7</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->
                <!-- <div class="tab-pane body" id="remarks">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content">
                          <div class="tab-pane active" id="classlist">
                            <div class="card border_info_tabs_1 card_top_border1">
                              <div class="body">
                                <div class="table-responsive">
                                  <table class="table table-hover m-b-0 c_list">
                                    <thead>
                                      <tr>
                                        <th>S No</th>
                                        <th>Subject Name</th>
                                        <th>Teacher Name</th>
                                        <th>Remark Desc</th>
                                        <th>Remark Date</th>
                                        <th>Rating</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>English</td>
                                        <td>Radha Kumari</td>
                                        <td>Text of the printing .....</td>
                                        <td>2001/20/3</td>
                                        <td>5</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>English</td>
                                        <td>Radha Kumari</td>
                                        <td>Text of the printing .....</td>
                                        <td>2001/20/3</td>
                                        <td>5</td>
                                      </tr>
                                      <tr>
                                        <td>3</td>
                                        <td>English</td>
                                        <td>Radha Kumari</td>
                                        <td>Text of the printing .....</td>
                                        <td>2001/20/3</td>
                                        <td>5</td>
                                      </tr>
                                      <tr>
                                        <td>4</td>
                                        <td>English</td>
                                        <td>Radha Kumari</td>
                                        <td>Text of the printing .....</td>
                                        <td>2001/20/3</td>
                                        <td>5</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->
                <!-- <div class="tab-pane body" id="issued">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content">
                          <div class="tab-pane active"  id="h1">
                            <div class="card border_info_tabs_1 card_top_border1">
                              <div class="body">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingsix">
                                      <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix" class="collapsed">
                                        Academic Certificates (TC / CC)
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingsix">
                                      <div class="body" style="padding:0px;">
                                        <div class="">
                                          <table class="table table-hover m-b-0 c_list">
                                            <thead>
                                              <tr>
                                                <th>S No</th>
                                                <th>Book No</th>
                                                <th>Sr No </th>
                                                <th>Name</th>
                                                <th>Issued Date</th>
                                                <th>Action</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              <tr>
                                                <td>1</td>
                                                <td>10021</td>
                                                <td>1</td>
                                                <td>WsCube Tech</td>
                                                <td>2001/6/12</td>
                                                <td class="">
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Cancel"> <i class="material-icons">cancel</i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Print"> <i class="material-icons">print</i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="View"> <i class="material-icons">remove_red_eye</i></button>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>2</td>
                                                <td>10021</td>
                                                <td>1</td>
                                                <td>WsCube Tech</td>
                                                <td>2001/6/12</td>
                                                <td class="">
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Cancel"> <i class="material-icons">cancel</i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Print"> <i class="material-icons">print</i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="View"> <i class="material-icons">remove_red_eye</i></button>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>3</td>
                                                <td>10021</td>
                                                <td>1</td>
                                                <td>WsCube Tech</td>
                                                <td>2001/6/12</td>
                                                <td class="">
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Cancel"> <i class="material-icons">cancel</i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Print"> <i class="material-icons">print</i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="View"> <i class="material-icons">remove_red_eye</i></button>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>4</td>
                                                <td>10021</td>
                                                <td>1</td>
                                                <td>WsCube Tech</td>
                                                <td>2001/6/12</td>
                                                <td class="">
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Cancel"> <i class="material-icons">cancel</i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Print"> <i class="material-icons">print</i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="View"> <i class="material-icons">remove_red_eye</i></button>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingSeven">
                                      <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven" class="collapsed">
                                        Competition  Certificates
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapseSeven" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingSeven">
                                      <div class="body" style="padding:0px;">
                                        <table class="table table-hover m-b-0 c_list">
                                          <thead>
                                            <tr>
                                              <th>S No</th>
                                              <th>Certificate No</th>
                                              <th>Name</th>
                                              <th>Date</th>
                                              <th>Position</th>
                                              <th>Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                              <td>1</td>
                                              <td>11001</td>
                                              <td>WsCube Tech</td>
                                              <td>2011/2/18</td>
                                              <td>2</td>
                                              <td>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Cancel"> <i class="material-icons">cancel</i></button>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Print"> <i class="material-icons">print</i></button>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="View"> <i class="material-icons">remove_red_eye</i></button>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>2</td>
                                              <td>11001</td>
                                              <td>WsCube Tech</td>
                                              <td>2011/2/18</td>
                                              <td>2</td>
                                              <td>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Cancel"> <i class="material-icons">cancel</i></button>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Print"> <i class="material-icons">print</i></button>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="View"> <i class="material-icons">remove_red_eye</i></button>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>3</td>
                                              <td>11001</td>
                                              <td>WsCube Tech</td>
                                              <td>2011/2/18</td>
                                              <td>2</td>
                                              <td>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Cancel"> <i class="material-icons">cancel</i></button>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Print"> <i class="material-icons">print</i></button>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="View"> <i class="material-icons">remove_red_eye</i></button>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>4</td>
                                              <td>11001</td>
                                              <td>WsCube Tech</td>
                                              <td>2011/2/18</td>
                                              <td>2</td>
                                              <td>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Cancel"> <i class="material-icons">cancel</i></button>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Print"> <i class="material-icons">print</i></button>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="View"> <i class="material-icons">remove_red_eye</i></button>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>



<!-- Content end here  -->
@endsection