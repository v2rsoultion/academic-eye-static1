<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAcademicHistoryInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'student_academic_history_info';
    protected $primaryKey = 'student_academic_history_info_id';
    public function up()
    {
        if (!Schema::hasTable('student_academic_history_info')) { 
            Schema::create('student_academic_history_info', function (Blueprint $table) {
                $table->increments('student_academic_history_info_id')->unsigned();
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->integer('student_id')->unsigned()->default(0);
                $table->integer('student_academic_info_id')->unsigned()->default(0);
                $table->integer('session_id')->unsigned()->default(0);
                $table->integer('class_id')->unsigned()->default(0);
                $table->integer('section_id')->unsigned()->default(0);
                $table->string('percentage',255)->nullable();
                $table->integer('rank')->unsigned()->default(0);
                $table->text('activity_winner')->nullable();
                $table->timestamps();
            });

            Schema::table('student_academic_history_info', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('student_academic_history_info', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('student_academic_history_info', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
            Schema::table('student_academic_history_info', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');
            });
            Schema::table('student_academic_history_info', function($table) {
                $table->foreign('student_academic_info_id')->references('student_academic_info_id')->on('student_academic_info')->onDelete('cascade');
            });
            Schema::table('student_academic_history_info', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');
            });
            Schema::table('student_academic_history_info', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');
            });
            Schema::table('student_academic_history_info', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');
            });
           
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_academic_history_info');
    }
}
