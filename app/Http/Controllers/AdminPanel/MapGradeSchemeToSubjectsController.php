<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MapGradeSchemeToSubjectsController extends Controller
{
    public function addGradeScheme() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.map-grade-scheme-to-subjects.map_grade_scheme_to_subjects')->with($data);
    }

     public function viewGradeScheme() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.map-grade-scheme-to-subjects.view_map_grade_scheme_to_subjects')->with($data);
    }
}
