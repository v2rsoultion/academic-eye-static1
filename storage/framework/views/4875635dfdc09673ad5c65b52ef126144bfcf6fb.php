
<?php $__env->startSection('content'); ?>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Add Task</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>"><?php echo trans('language.menu_task_manager'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('admin-panel/task-manager/add-task')); ?>">Add Task</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
          
              <div class="body form-gap">
                <div class="row tabless" >
                  <div class="headingcommon  col-lg-12" style="padding-bottom: 0px !important">Assign Task :-</div>
                  <form class="" action="" method="" id="searchpanel" style="width: 100%;">
                    <div class="row" >
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-4">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Task</lable>
                          <input type="text" name="name" id="name" class="form-control" placeholder="Task">
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <lable class="from_one1" for="Amount">Priority</lable>
                          <input type="text" name="Caption" id="Caption" class="form-control"  placeholder="Priority">
                        </div>
                      </div>
                       <div class="col-lg-4">
                        <div class="form-group">
                          <lable class="from_one1" for="Amount">Task Date</lable>
                          <input type="text" name="taskDate" id="taskDate" class="form-control"  placeholder="Task Date">
                        </div>
                      </div>
                     
                        <div class="col-sm-4 text_area_desc">
                          <lable class="from_one1">Description</lable>
                          <div class="form-group">
                              <textarea rows="4" cols="30" name="father_occupation" class="form-control no-resize"  placeholder="Description"></textarea>
                          </div>
                      </div>
                       <div class="col-lg-4 padding-0" style="padding-left: 15px !important;">
                        <br>
                      <lable class="from_one1">Student</lable>
                      <div class="form-group formGrr">
                        <div class="radio" style="margin-top:15px !important;">
                          <input type="radio" name="checked" id="Single" value="Single" checked="">
                          <label for="Single" class="document_staff">Single</label>
                          
                          <input type="radio" name="checked" id="Multiple" value="Multiple">
                          <label for="Multiple" class="document_staff">Multiple</label>
                          
                          <input type="radio" name="checked" id="Group" value="Group">
                          <label for="Group" class="document_staff">Group</label>
                        </div>
                      </div>
                    </div>
                       <div class="col-lg-4 padding-0" style="padding-left: 15px !important;">
                        <br>
                      <lable class="from_one1">Employee </lable>
                      <div class="form-group formGrr">
                        <div class="radio" style="margin-top:15px !important;">
                          <input type="radio" name="checked1" id="Single1" value="Single" checked="">
                          <label for="Single1" class="document_staff">Single</label>
                          <input type="radio" name="checked1" id="Multiple1" value="Multiple" >
                          <label for="Multiple1" class="document_staff">Multiple</label>
                          
                          
                        </div>
                      </div>
                    </div>
                    </div>
                    <hr>
                     <div class="row" >
                       <!-- <div class="col-lg-10"></div> -->
                      <div class="col-lg-1 ">
                        <button type="submit" class="btn btn-raised btn-primary" title="Submit">Save
                        </button>
                      </div>
                       <div class="col-lg-1 ">
                        <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel
                        </button>
                      </div>
                    </div>
                  </form>
               
                
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>