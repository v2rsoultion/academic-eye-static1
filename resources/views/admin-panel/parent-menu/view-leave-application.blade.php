@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
        overflow-x: visible !important; 
    }
    .modal-dialog {
  max-width: 550px !important;
}
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h2>View Leaves Application</h2>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/parent/view-profile') !!}">My Profile</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/parent/children-details') !!}">Leave Management</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/parent/children-details/view-leave-application') !!}">View Leaves Application</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                <form>
                                    <div class="row">
                                   <div class="col-lg-3">
                                        <div class="form-group">
                                          <!-- <lable class="from_one1" for="name">Date</lable> -->
                                          <input type="text" name="name" id="date" class="form-control" placeholder="Date">
                                        </div>
                                    </div>
                                   
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-raised btn-primary " title="Search">Search</button>
                                </div>
                                <div class="col-lg-1">
                                    <button type="reset" class="btn btn-raised btn-primary " title="Clear">Clear</button>
                                </div>
                                </div>
                                </form>
        
                                
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="" style="width:100%">
                                    {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>S No</th>
                                            <th class="text-center">Date (From-To)</th>
                                            <th>{{trans('language.stud_leave_app_reason')}}</th>
                                            <th>File Url</th>
                                            <!-- <th>View Details</th> -->
                                            <th>{{trans('language.stud_leave_app_status')}}</th>
                                            <!-- <th>Action</th> -->
                                        </tr>
                                    </thead>
                                     <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td class="text-center">01-09-2018 - 09-09-2018</td>
                                            <td><button type="button" class="btn btn-raised btn-primary" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#viewReasonModel" >View Reason</button></td>
                                            <td><a href="#">File url</a></td>
                                            <td><span class="badge badge-success">Approved</span></td>
                                            
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td class="text-center">01-09-2018 - 09-09-2018</td>
                                            <td><button type="button" class="btn btn-raised btn-primary" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#viewReasonModel" >View Reason</button></td>
                                            <td><a href="#">File url</a></td>
                                            <td><span class="badge badge-success">Approved</span></td>
                                            
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td class="text-center">01-09-2018 - 09-09-2018</td>
                                            <td><button type="button" class="btn btn-raised btn-primary" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#viewReasonModel" >View Reason</button></td>
                                            <td><a href="#">File url</a></td>
                                            <td><span class="badge badge-success">Approved</span></td>
                                           
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td class="text-center">01-09-2018 - 09-09-2018</td>
                                            <td><button type="button" class="btn btn-raised btn-primary" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#viewReasonModel" >View Reason</button></td>
                                            <td><a href="#">File url</a></td>
                                            <td><span class="badge badge-success">Approved</span></td>
                                           
                                        </tr>
                            
                                     </tbody>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#designation-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: "{{url('admin-panel/student-leave-application/data')}}",
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'student_leave_reason', name: 'student_leave_reason'},
                {data: 'date_from', name: 'date_from'},
                {data: 'date_to', name: 'date_to'},
                {data: 'student_leave_attachment', name: 'student_leave_attachment'},
                {data: 'student_leave_status', name: 'student_leave_status'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 6, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });


</script>

<!-- Action Model  -->
<div class="modal fade" id="viewReasonModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">View Reason</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div style="width:500px;border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;">
         <div><b>Enroll No:</b> 1001A1</div>
         <div><b>Name:</b> Ankit Dave</div>
         <div><b>Leave Date:</b> 01-09-2018 - 09-09-2018</div>
         <div><b>Leave Days:</b> 9 Days</div>
         <div><b>Reason:</b> Work At Home</div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection