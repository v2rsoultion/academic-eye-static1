<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'book_copies_info';
    protected $primaryKey = 'book_info_id';
    public function up()
    {
        if (!Schema::hasTable('book_copies_info')){ 
            Schema::create('book_copies_info', function (Blueprint $table) {
                $table->increments('book_info_id');

                $table->integer('admin_id')->unsigned();
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');

                $table->integer('update_by')->unsigned();
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');

                $table->integer('school_id')->unsigned();
                 $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');

                $table->integer('book_id')->unsigned();
                $table->foreign('book_id')->references('book_id')->on('books')->onDelete('cascade');

                $table->string('book_unique_id', 255)->nullable();
                $table->tinyInteger('exclusive_for_staff')->default(1)->comment = '0=No,1=Yes';
                $table->tinyInteger('book_copy_status')->default(0)->comment = '0=Available,1=Issued';
                $table->timestamps();
            });

            // Schema::table('books', function($table) {
                    
                // });
            // Schema::table('books', function($table) {
                
            // });
            // Schema::table('books', function($table) {
               
            // });
            // Schema::table('books', function($table) {
               
            // });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_copies_info');
    }
}
