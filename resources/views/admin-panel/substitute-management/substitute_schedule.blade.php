@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  .tabless table tr td .opendiv ul li{
  padding: 0px 0px !important;
  }
  .footable .dropdown-menu>li>a{
  padding: 10px 15px !important;
  }
  .footable .dropdown-menu{
  padding: 0px 0px !important; 
  }
  #btnCu{
  background: transparent;
  height: auto;
  }
  #btnCu i{
  font-size: 23px;
  }
  #sendReply h5{
  font-size: 14px;
  }
  #viewReply h5{
    font-size: 14px;
  }
   table tr td{
    padding: 10px 10px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Substitute Schedule</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
            <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/menu/substitute-management') }}">Substitute Management</a></li>
           <li class="breadcrumb-item"><a href="{{ url('admin-panel/substitute-management/manage-substitute') }}">Manage Substitute</a></li>
            <li class="breadcrumb-item"><a href="{{ url('admin-panel/substitute-management/substitute-schedule') }}">Substitute Schedule</a></li>

        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <!--  <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
                </div> -->
              <div class="body form-gap" style="padding-top: 20px !important;">
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row">
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Teacher Name</lable>
                          <input type="text" name="name" id="name" class="form-control" placeholder="Teacher Name">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Date </lable>
                          <input type="text" name="name" id="date" class="form-control" placeholder="Date">
                        </div>
                      </div>
                      <div class="col-lg-1 ">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Submit">Search
                        </button>
                      </div>
                      <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                  <hr>
                  <!--  DataTable for view Records  -->
                  <table class="table m-b-0 c_list">
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Class Name</th>
                        <th>Day/Time </th>
                        <th>Teacher Name </th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $cou = 1; for ($i=0; $i < 5; $i++) {  ?> 
                      <tr>
                        <td><?php echo $cou; ?></td>
                        <td>10th</td>
                        <td>10 July - 15 July 2018 (10:30 AM)</td>
                        <td>Harendra Joshi</td>

                        <!-- for Pending use this commeted class -->
                        <!-- <td class="text-warning">Pending</td> -->
                          <td class="text-success">Allotted</td>
                           <td> <div class="dropdown">
                            <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                              <li>
                                  <a href="{{ url('admin-panel/substitute-management/substitute-schedule/allocate-teacher') }}" title="Allocate">Allocate</a>
                              </li>
                            
                            </ul>
                          </div></td>
                      </tr>
                      <tr>
                        <td><?php echo $cou; ?></td>
                        <td>10th</td>
                        <td>15 July - 20 July 2018 / 11:30 AM</td>
                        <td></td>

                        <!-- for Pending use this commeted class -->
                        <td class="text-warning">Pending</td>
                        <td> <div class="dropdown">
                            <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                              <li>
                                  <a href="{{ url('admin-panel/substitute-management/substitute-schedule/allocate-teacher') }}" title="Allocate">Allocate</a>
                              </li>
                            
                            </ul>
                          </div></td>
                      </tr>

                      <?php $cou++; } ?> 
                    </tbody>
                  </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
@endsection
<script type="text/javascript">
  $('#dropbtn').on('click', 'dropbtnAction', function () {
      $(this).toggleClass('active');
  });
  
</script>