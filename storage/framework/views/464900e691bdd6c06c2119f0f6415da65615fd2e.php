<style type="text/css">

 /*.sidebar .menu .list a.active {
  background-color: #f5f7f9 !important;
    border-left: 2px solid #6572b8 !important;
}*/
</style>
<aside id="leftsidebar" class="sidebar">
<div class="tab-content">
<div class="tab-pane stretchRight active" id="dashboard">
  <div class="menu">
    <ul class="list">
    
        <!-- <div class="user-info">
          <div class="image" style="padding-top: 8px !important;"><a href=""><img src="<?php echo URL::to('public/assets/images/logo_new1.png'); ?>" alt="User" width="70%" ></a></div>
        </div> -->
  <div class="user-info">
      <div class="image" style="margin-top: 7px"><a href="<?php echo e(url('admin-panel/admin-profile')); ?>" class=" waves-effect waves-block"><!-- <img src="http://via.placeholder.com/60x60" alt="User"> -->
        <img src="<?php echo URL::to('public/assets/images/admin_photo.jpg'); ?>" style="width: 60px; height: 60px; border: 3px solid #acadaf;">
      </a></div>
      <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
        <div class="detail"><b>Admin </b>
          <br>
            <small>Super Admin</small>
        </div>
      <?php endif; ?>
      <?php if($login_info['admin_type'] == 2): ?>                                                 
        <div class="detail"><b>Admin </b>
          <br>
            <small>Staff admin</small>
        </div>
      <?php endif; ?>
       <?php if($login_info['admin_type'] == 3): ?>                                                 
        <div class="detail"><b>Admin </b>
          <br>
            <small>Parent admin</small>
        </div>
      <?php endif; ?>
       <?php if($login_info['admin_type'] == 4): ?>                                                 
        <div class="detail"><b>Admin </b>
          <br>
            <small>Student admin</small>
        </div>
      <?php endif; ?>
  </div>
      <!-- </li> -->

      <!-- ======== Start here side menu bar ========== -->
      <div class="adminmenu" style="padding-top: 6px;">
        <ul class="text-center">
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
          <a href="<?php echo e(url('admin-panel/menu/configuration')); ?>" title="Configuration" class="icyLink">
            <li class="<?php if(Request::segment(3) == "configuration" || Request::segment(2) == "configuration" || Request::segment(2) == "school" || Request::segment(2) == "session" || Request::segment(2) == "holidays" || Request::segment(2) == "shift" || Request::segment(2) == "facilities" || Request::segment(2) == "document-category" || Request::segment(2) == "designation" || Request::segment(2) == "title" || Request::segment(2) == "caste" || Request::segment(2) == "religion" || Request::segment(2) == "nationality" || Request::segment(2) == "schoolgroup" || Request::segment(2) == "room-no" || Request::segment(2) == "stream" || Request::segment(2) == "country" || Request::segment(2) == "state" || Request::segment(2) == "city"): ?>  active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_configuration'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 2): ?>
          <a href="<?php echo e(url('admin-panel/staff/view-profile')); ?>" title="Staff Profile">
            <li class="<?php if(Request::segment(3) == "staff" || Request::segment(2) == "staff"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Profile/Staff.svg'); ?>" alt="">
              <span>My Profile</span> 
            </li>
          </a>
          <?php endif; ?>

          <?php if($login_info['admin_type'] == 3): ?>
          <a href="<?php echo e(url('admin-panel/parent/view-profile')); ?>" title="Staff Profile">
            <li class="<?php if(Request::segment(3) == "staff" || Request::segment(2) == "staff"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Profile/Parent.svg'); ?>" alt="">
              <span>My Profile</span> 
            </li>
          </a>
           <a href="<?php echo e(url('admin-panel/parent/children-details/view-attendance')); ?>" title="Student Attendance">
            <li class="<?php if(Request::segment(3) == "view-attendance"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Staff-Login/Attendence.svg'); ?>" alt="">
              <span>Attendance</span> 
            </li>
          </a>
           <a href="<?php echo e(url('admin-panel/parent/children-details/view-leave-application')); ?>" title="Student Attendance">
            <li class="<?php if(Request::segment(3) == "view-attendance"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Staff-Login/LeaveManagement.svg'); ?>" alt="">
              <span>Leave Management</span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/parent/children-details/view-remarks')); ?>" title="Student Remarks">
            <li class="<?php if(Request::segment(3) == "view-remarks"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Staff-Login/remarks.svg'); ?>" alt="">
              <span>Remarks</span> 
            </li>
          </a>
           <a href="<?php echo e(url('admin-panel/parent/children-details/view-homework')); ?>" title="Student Homework">
            <li class="<?php if(Request::segment(3) == "view-homework"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Staff-Login/homework.svg'); ?>" alt="">
              <span>Homework</span> 
            </li>
          </a>
           <a href="<?php echo e(url('admin-panel/parent/children-details/view-time-table')); ?>" title="Student Time Table">
            <li class="<?php if(Request::segment(3) == "view-time-table"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Staff-Login/TimeTable.svg'); ?>" alt="">
              <span>Time Table</span> 
            </li>
          </a>
           <a href="<?php echo e(url('admin-panel/parent/children-details/view-vehicle-tracks')); ?>" title="Vehicle Tracking">
            <li class="<?php if(Request::segment(3) == "view-vehicle-tracks"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Staff-Login/Track Vehicle.svg'); ?>" alt="">
              <span>Vehicle Tracking</span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/parent/children-details/online-content')); ?>" title="Online Content">
            <li class="<?php if(Request::segment(3) == "online-content"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Staff-Login/Online Content.svg'); ?>" alt="">
              <span>Online Content</span> 
            </li>
          </a>
          <?php endif; ?>

          <?php if($login_info['admin_type'] == 4): ?>
          <a href="<?php echo e(url('admin-panel/student/view-profile')); ?>" title="Student Profile">
            <li class="<?php if(Request::segment(2) == "student" && Request::segment(3) == "view-profile" ): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Student-Login/student.svg'); ?>" alt="">
              <span>My Profile</span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/student/view-attendance')); ?>" title="Student Attendance">
            <li class="<?php if(Request::segment(3) == "view-attendance"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Student-Login/Attendence.svg'); ?>" alt="">
              <span>My Attendance</span> 
            </li>
          </a>
           <a href="<?php echo e(url('admin-panel/student/view-time-table')); ?>" title="Student Time Table">
            <li class="<?php if(Request::segment(3) == "view-time-table"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Student-Login/TimeTable.svg'); ?>" alt="">
              <span>My Time Table</span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/student/view-homework')); ?>" title="Student Homework">
            <li class="<?php if(Request::segment(3) == "view-homework"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Student-Login/homework.svg'); ?>" alt="">
              <span>My Homework</span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/student/view-remarks')); ?>" title="Student Remarks">
            <li class="<?php if(Request::segment(3) == "view-remarks"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Student-Login/remarks.svg'); ?>" alt="">
              <span>My Remarks</span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/student/view-vehicle-tracks')); ?>" title="Vehicle Tracking">
            <li class="<?php if(Request::segment(3) == "view-vehicle-tracks"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Student-Login/Track Vehicle.svg'); ?>" alt="">
              <span>Vehicle Tracking</span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/student/online-content')); ?>" title="Online Content">
            <li class="<?php if(Request::segment(3) == "online-content"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Student-Login/Online Content.svg'); ?>" alt="">
              <span>Online Content</span> 
            </li>
          </a>
           <a href="<?php echo e(url('admin-panel/student/view-task')); ?>" title="Student Task">
            <li class="<?php if(Request::segment(3) == "view-task"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Student-Login/Task.svg'); ?>" alt="">
              <span>My Task</span> 
            </li>
          </a>
          <?php endif; ?>

          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
          <a href="<?php echo e(url('admin-panel/menu/academic')); ?>" title="Academic">
            <li class="<?php if(Request::segment(3) == "academic" || Request::segment(2) == "academic" || Request::segment(2) == "class" || Request::segment(2) == "section" || Request::segment(2) == "class-teacher-allocation" || Request::segment(2) == "subject" || Request::segment(2) == "competition" || Request::segment(2) == "time-table"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Academic/Academic.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_academic'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
           <a href="<?php echo e(url('admin-panel/menu/admission')); ?>" title="Admission">
            <li class="<?php if(Request::segment(3) == "admission" || Request::segment(2) == "admission" || Request::segment(2) == "admission-form"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Admission/Admission_SideBar.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_admission'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
          <a href="<?php echo e(url('admin-panel/menu/student')); ?>" title="Student">
            <li class="<?php if(Request::segment(3) == "student" || Request::segment(2) == "student" || Request::segment(2) == "student-leave-application"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Student/student.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_student'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
           <?php if($login_info['admin_type'] == 2 && $login_info['staff_role_id'] == 2): ?>
          <a href="<?php echo e(url('admin-panel/staff-menu/my-class')); ?>" title="Class">
            <li class="<?php if(Request::segment(3) == "my-class" && Request::segment(2) == "staff-menu"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Staff-Login/My Class.svg'); ?>" alt="">
              <span>My Class</span> 
            </li>
          </a>
           <a href="<?php echo e(url('admin-panel/staff-menu/my-subjects')); ?>" title="Subject">
            <li class="<?php if(Request::segment(3) == "my-subjects" && Request::segment(2) == "staff-menu"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Staff-Login/My Subjects.svg'); ?>" alt="">
              <span>My Subjects</span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/staff-menu/my-schedule')); ?>" title="Student">
            <li class="<?php if(Request::segment(3) == "my-schedule" && Request::segment(2) == "staff-menu"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Staff-Login/MY Schedule/My Schedule01.svg'); ?>" alt="">
              <span>My Schedule</span> 
            </li>
          </a>
          <?php endif; ?>

          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
          <a href="<?php echo e(url('admin-panel/menu/recruitment')); ?>" title="Recruitment">
            <li class="<?php if(Request::segment(3) == "recruitment" || Request::segment(2) == "recruitment"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/menu/Recruitment.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_recruitment'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1 || $login_info['staff_role_id'] == 5): ?>
          <a href="<?php echo e(url('admin-panel/menu/examination')); ?>" title="Examination">
            <li class="<?php if(Request::segment(3) == "examination" || Request::segment(2) == "examination"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/menu/Examination.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_examination'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1 || $login_info['staff_role_id'] == 9): ?>
          <a href="<?php echo e(url('admin-panel/menu/fees-collection')); ?>" title="Fees Collection">
            <li class="<?php if(Request::segment(3) == "fees-collection" || Request::segment(2) == "fees-collection"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/menu/Fees Collection.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_fee_collection'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
          <a href="<?php echo e(url('admin-panel/menu/staff')); ?>" title="Staff">
            <li class="<?php if(Request::segment(3) == "staff" || Request::segment(2) == "staff"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Staff.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_staff'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1 || $login_info['staff_role_id'] == 7): ?>
          <a href="<?php echo e(url('admin-panel/menu/hostel')); ?>" title="Hostel">
            <li class="<?php if(Request::segment(3) == "hostel" || Request::segment(2) == "hostel"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Hostel.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_hostel'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1 || $login_info['staff_role_id'] == 12): ?>
          <a href="<?php echo e(url('admin-panel/menu/library')); ?>" title="Library">
            <li class="<?php if(Request::segment(3) == "library" || Request::segment(2) == "library"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Library.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_library'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
          <a href="<?php echo e(url('admin-panel/menu/transport')); ?>" title="Transport">
            <li class="<?php if(Request::segment(3) == "transport" || Request::segment(2) == "transport"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Transport.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_transport'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1 || $login_info['staff_role_id'] == 11): ?>
          <a href="<?php echo e(url('admin-panel/menu/visitor')); ?>" title="Visitor">
              <li class="<?php if(Request::segment(3) == "visitor" || Request::segment(2) == "visitor"): ?> active <?php endif; ?>">
                 <img src="<?php echo URL::to('public/assets/images/Visitor/Visitor.svg'); ?>" alt="">
                <span><?php echo trans('language.menu_visitor'); ?></span> 
              </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
          <a href="<?php echo e(url('admin-panel/menu/notice-board')); ?>" title="Noticeboard">
            <li class="<?php if(Request::segment(3) == "notice-board" || Request::segment(2) == "notice-board"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Noticeboard.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_notice_board'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1 || $login_info['staff_role_id'] == 2): ?>
           <a href="<?php echo e(url('admin-panel/menu/online-content')); ?>" title="Online Content">
            <li class="<?php if(Request::segment(3) == "online-content" || Request::segment(2) == "online-content"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/OnlineContent/Online Content.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_online_content'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
          <a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>" title="Task Manager">
            <li class="<?php if(Request::segment(3) == "task-manager" || Request::segment(2) == "task-manager"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/Task Manager/Task Manager.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_task_manager'); ?></span> 
            </li>
          </a>
          <?php endif; ?>
          <!-- <a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>" title="Theme Option's">
            <li class="<?php if(Request::segment(3) == "" || Request::segment(2) == ""): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_theme_options'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>" title="University">
            <li class="<?php if(Request::segment(3) == "" || Request::segment(2) == ""): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_university'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>" title="Componets">
            <li class="<?php if(Request::segment(3) == "" || Request::segment(2) == ""): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Configuration.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_components'); ?></span> 
            </li>
          </a>
          <a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>" title="Extra">
            <li class="<?php if(Request::segment(3) == "" || Request::segment(2) == ""): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/admin/assets/images/menu/Extra.svg'); ?>" alt="">
              <span><?php echo trans('language.menu_extra'); ?></span> 
            </li>
          </a> -->
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
            <a href="<?php echo e(url('admin-panel/menu/substitute-management')); ?>" title="Subtstitute Management">
            <li class="<?php if(Request::segment(3) == "substitute-management" || Request::segment(2) == "substitute-management"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/substitute/substitute management.svg'); ?>" alt="">
              <span>Subtstitute Management</span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
            <a href="<?php echo e(url('admin-panel/menu/inventory')); ?>" title="Inventory">
            <li class="<?php if(Request::segment(3) == "inventory" || Request::segment(2) == "inventory"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/menu/Inventory.svg'); ?>" alt="">
              <span>Inventory</span> 
            </li>
          </a>
          <?php endif; ?>
          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
            <a href="<?php echo e(url('admin-panel/menu/account')); ?>" title="Account">
            <li class="<?php if(Request::segment(3) == "account" || Request::segment(2) == "account"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/menu/Account.svg'); ?>" alt="">
              <span>Accounts</span> 
            </li>
          </a>
          <?php endif; ?>
           <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
            <a href="<?php echo e(url('admin-panel/menu/payroll')); ?>" title="Payroll">
            <li class="<?php if(Request::segment(3) == "payroll" || Request::segment(2) == "payroll"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/menu/SideBar Payroll.svg'); ?>" alt="">
              <span>Payroll</span> 
            </li>
          </a>
          <?php endif; ?>
           <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
            <a href="<?php echo e(url('admin-panel/menu/certificate')); ?>" title="Certificate">
            <li class="<?php if(Request::segment(3) == "certificate" || Request::segment(2) == "certificate"): ?> active <?php endif; ?>">
              <img src="<?php echo URL::to('public/assets/images/menu/diploma (1).svg'); ?>" alt="">
              <span>Certificates</span> 
            </li>
          </a>
          <?php endif; ?>
        </ul>
        <br><br><br>
        <div class="clearfix"></div>
      </div>
    </ul>
  </div>
</div>
</div>
</aside>
