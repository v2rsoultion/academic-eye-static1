
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2><?php echo trans('language.menu_online_content'); ?></h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><?php echo trans('language.menu_online_content'); ?></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/OnlineContent/Notes.svg'); ?>" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Notes</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="<?php echo e(url('/admin-panel/notes/add-notes')); ?>" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="<?php echo e(url('/admin-panel/notes/view-notes')); ?>" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                       <!--  <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="#" title="">Option 1</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 2</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 3</a>
                              </li>
                            </ul>
                          </li>
                        </ul> -->
                        <div class="clearfix"></div>
                      </div>
                    </div>
                     <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/OnlineContent/QuestionPaper.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Question Paper</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="<?php echo e(url('/admin-panel/question-paper/add-question-paper')); ?>" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a>
                                <a href="<?php echo e(url('/admin-panel/question-paper/view-question-paper')); ?>" class="float-right" title="View ">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </section>
<!-- Content end here  -->



<?php echo $__env->make('admin-panel.layout.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('admin-panel.layout.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>