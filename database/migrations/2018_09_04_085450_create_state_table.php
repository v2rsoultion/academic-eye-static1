<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'state';
    protected $primaryKey = 'state_id';
    public function up()
    {
        if (!Schema::hasTable('state')) {
            Schema::create('state', function (Blueprint $table) {
                $table->increments('state_id');
                $table->integer('country_id')->unsigned()->default(0);
                $table->string('state_name', 255);
                $table->tinyInteger('state_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('state', function($table) {
                $table->foreign('country_id')->references('country_id')->on('country')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('state');
    }
}
