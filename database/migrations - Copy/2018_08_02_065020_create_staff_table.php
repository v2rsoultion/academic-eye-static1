<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'staff';
    protected $primaryKey = 'staff_id';
    public function up()
    {
        if (!Schema::hasTable('staff')) {
            Schema::create('staff', function (Blueprint $table) {
                $table->increments('staff_id');
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->integer('reference_admin_id')->unsigned()->default(0);
                $table->string('staff_name', 255)->nullable();
                $table->text('staff_profile_img')->nullable();
                $table->integer('designation_id')->unsigned()->nullable();
                $table->integer('staff_role_id')->unsigned()->nullable();
                $table->string('staff_email', 255)->nullable();
                $table->string('staff_mobile_number', 255)->nullable();
                $table->date('staff_dob')->nullable();
                $table->tinyInteger('staff_gender')->default(0)->comment = '0=Male,1=Female';
                $table->string('staff_father_name_husband_name')->nullable();
                $table->string('staff_father_husband_mobile_no',20)->nullable();
                $table->string('staff_mother_name', 255)->nullable();
                $table->string('staff_blood_group',255)->nullable();
                $table->tinyInteger('staff_marital_status')->default(0)->comment = '0=Unmarried,1=Married';
                $table->integer('caste_id')->unsigned()->nullable();
                $table->integer('nationality_id')->unsigned()->nullable();
                $table->integer('religion_id')->unsigned()->nullable();
                $table->string('staff_attendance_unique_id', 255)->nullable();
                $table->text('staff_qualification')->nullable()->comment = 'Qualifications are store with comma';
                $table->text('staff_specialization')->nullable()->comment = 'Specializations are store with comma';
                $table->text('staff_reference')->nullable();
                $table->text('staff_experience ')->nullable();
                $table->text('staff_temporary_address')->nullable();
                $table->string('staff_temporary_city', 255)->nullable();
                $table->string('staff_temporary_state', 255)->nullable();
                $table->string('staff_temporary_county', 255)->nullable();
                $table->string('staff_temporary_pincode', 20)->nullable();
                $table->text('staff_permanent_address')->nullable();
                $table->string('staff_permanent_city', 255)->nullable();
                $table->string('staff_permanent_state', 255)->nullable();
                $table->string('staff_permanent_county', 255)->nullable();
                $table->string('staff_permanent_pincode', 20)->nullable();
                $table->tinyInteger('staff_status')->default(1)->comment = '0=Leaved,1=Studying';
                $table->timestamps();
            });

            Schema::table('staff', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('staff', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('staff', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
            Schema::table('staff', function($table) {
                $table->foreign('reference_admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('staff', function($table) {
                $table->foreign('designation_id')->references('designation_id')->on('designations')->onDelete('cascade');
            });
            Schema::table('staff', function($table) {
                $table->foreign('staff_role_id')->references('staff_role_id')->on('staff_roles')->onDelete('cascade');
            });
            Schema::table('staff', function($table) {
                $table->foreign('caste_id')->references('caste_id')->on('caste');
            });
            Schema::table('staff', function($table) {
                $table->foreign('religion_id')->references('religion_id')->on('religions');
            });
            Schema::table('staff', function($table) {
                $table->foreign('nationality_id')->references('nationality_id')->on('nationality');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
