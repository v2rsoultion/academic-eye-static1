
<?php $__env->startSection('content'); ?>
<style type="text/css">
  .card .body .table td,
.cshelf1 {
    width: 100px;
    height: auto !important;
}

</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Print Invoice</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/inventory'); ?>">Inventory</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/inventory/manage-print-invoice'); ?>">Print Invoice</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
             <!--  <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div> -->
              <div class="body form-gap">
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Receipts :-</div> -->
                  <form class="" action="" method="" id="manage_print_invoice" style="width: 100%;">
                    <div class="row">
                      <div class="col-lg-3 col-md-3">
                        <div class="form-group">
                           <lable class="from_one1">Type: </lable>
                          <div class="radio" style="margin-top:6px !important;">
                               <input id="radio32" checked="checked" name="inventory_invoice" type="radio" value="0">
                                <label for="radio32" class="document_staff">Purchase</label>
                                <input id="radio31" name="inventory_invoice" type="radio" value="1">
                                <label for="radio31" class="document_staff">Sales</label>
                               
                          </div>
                        </div>
                      </div>
                       <div class="col-lg-2">
                        <div class="form-group">
                          <lable class="from_one1">From: </lable>
                          <input type="text" name="from_date" id="date" class="form-control" placeholder="From Date">
                        </div>
                      </div>
                        <div class="col-lg-2">
                        <div class="form-group">
                          <lable class="from_one1">To: </lable>
                          <input type="text" name="to_date" id="taskDate" class="form-control" placeholder="To Date">
                        </div>
                      </div>
                        <div class="col-lg-2">
                        <div class="form-group">
                          <lable class="from_one1">Invoice No: </lable>
                          <input type="text" name="invoice_no" id=" " value="" class="form-control" placeholder="Invoice No">
                        </div>
                      </div>
                       <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Search" onclick="showTable()">Search
                        </button>
                      </div>
                      <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary saveBtn" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                    
                      
                  </form>
                  <!--  DataTable for view Records  -->
                 
                    <div class="table-responsive" style="display: none;" id="show_table">
                      <br>
                    <table class="table table-bordered m-b-0 c_list" id="invoice_table" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Date</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
            
                      <tr>
                        <td>1</td>
                        <td>24/04/2018</td>
                         <!-- <td style="text-align: center;"><a href="<?php echo e(url('admin-panel/inventory/view-pdf-format')); ?>" class="btn btn-raised btn-primary">View</a></td> -->
                        <td style="text-align: center;"><a href="<?php echo e(route('pdfview',['download'=>'pdf'])); ?>" class="btn btn-raised btn-primary">View</a></td>
                      </tr>
                    </tbody>
                  </table>
            
                </div>
                     
              </div>
              

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
<!-- Content end here  -->

<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#invoice_table').DataTable({
           // dom: 'Blfrtip',
           //  buttons: [{ 
           //    extend: 'csvHtml5',
           //  }]
        });
    });

    function showTable() {
      var from_date = $('#from_date').val();
      var to_date = $('#to_date').val();
      var voucher_no = $('#voucher_no').val();
      if('from_date' != "" && 'to_date' != "" && 'voucher_no' != "") {
       document.getElementById('show_table').style.display = "block";
      }
    }
</script>
<?php $__env->stopSection(); ?>








<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>