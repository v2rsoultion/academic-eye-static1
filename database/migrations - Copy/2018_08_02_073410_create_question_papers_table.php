<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionPapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'question_papers';
    protected $primaryKey = 'question_paper_id';
    public function up()
    {
        if (!Schema::hasTable('question_papers')) {
            Schema::create('question_papers', function (Blueprint $table) {
                $table->increments('question_paper_id')->unsigned();
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->string('question_paper_name', 255)->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('subject_id')->unsigned()->nullable();
                $table->integer('exam_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->text('question_paper_file')->nullable();
                $table->tinyInteger('question_paper_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('question_papers', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('question_papers', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('question_papers', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
            Schema::table('question_papers', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');
            });
            Schema::table('question_papers', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');
            });
            Schema::table('question_papers', function($table) {
                $table->foreign('subject_id')->references('subject_id')->on('subjects')->onDelete('cascade');
            });
            Schema::table('question_papers', function($table) {
                $table->foreign('exam_id')->references('exam_id')->on('exams')->onDelete('cascade');
            });
            Schema::table('question_papers', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_papers');
    }
}
