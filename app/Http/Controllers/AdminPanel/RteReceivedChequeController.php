<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RteReceivedChequeController extends Controller
{
    public function rteChequeDetail() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.rte-received-cheque.add_rti_cheque_detail')->with($data);
    }

    public function viewRteChequeDetail() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.rte-received-cheque.view_rti_cheque_details')->with($data);
    }
}
