<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'admission_forms';
    protected $primaryKey = 'admission_form_id';
    public function up()
    {
        if (!Schema::hasTable('admission_forms')) {
            Schema::create('admission_forms', function (Blueprint $table) {
                $table->increments('admission_form_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('form_name',255)->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->tinyInteger('medium_type')->default(1)->comment = '0=Hindi,1=English';
                $table->integer('no_of_intake')->nullable();
                $table->text('form_url')->nullable();
                $table->string('form_prefix',255)->nullable();
                $table->text('form_instruction')->nullable();
                $table->text('form_information')->nullable();
                $table->tinyInteger('form_type')->default(0)->comment = '0=Admission,1=Enquiry';
                $table->integer('brochure_id')->unsigned()->nullable();
                $table->date('form_last_date')->nullable();
                $table->integer('form_fee_amount')->unsigned()->nullable();
                $table->string('form_pay_mode',255)->nullable();
                $table->tinyInteger('form_email_receipt')->default(0)->comment = '0=No,1=Yes';
                $table->text('form_success_message')->nullable();
                $table->text('form_failed_message')->nullable();
                $table->tinyInteger('form_message_via')->default(0)->comment = '0=Email,1=Phone,2=Both';
                $table->tinyInteger('form_status')->default(0)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('admission_forms', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins');
            });
            Schema::table('admission_forms', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins');
            });
            Schema::table('admission_forms', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes');
            });
            Schema::table('admission_forms', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions');
            });
            Schema::table('admission_forms', function($table) {
                $table->foreign('brochure_id')->references('brochure_id')->on('brochures');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_forms');
    }
}
