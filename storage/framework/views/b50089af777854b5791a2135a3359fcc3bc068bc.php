﻿
<?php $__env->startSection('content'); ?>
<style type="text/css">
    .table-responsive {
        overflow-x: visible !important;
    }
</style>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Staff Attendance Report</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Transport</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/transport/transport-report/view-staff-attendance-report'); ?>">Staff Attendance Report</a></li>
                </ul>                
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">

                                    <div class="row clearfix">
                                    
                                        <div class="col-lg-3 col-md-3 col-sm-12">
                                            <div class="form-group">
                                                <!-- <lable class="from_one1" for="name">Start Date </lable> -->
                                                <input type="text" class="form-control" value="" placeholder="Start Date" id="date_start">
                                                <!-- <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span> -->
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12">
                                            <div class="form-group">
                                                <!-- <lable class="from_one1" for="name">End Date </lable> -->
                                                <input type="text" class="form-control" value="" placeholder="End Date" id="date_end">
                                                <!-- <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span> -->
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="class_name">
                                                <option value="">Select Staff</option>
                                                <option value="1">Rajesh Kumar</option>
                                                <option value="2">Kiran Kumar</option>
                                                <option value="3">Kajal Kumar</option>
                                                <option value="4">Mahesh Kumar</option>
                                            </select>
                                        </div>  
                                        <div class="col-lg-1 col-md-1 col-sm-12">
                                            <button class="btn btn-raised btn-primary">Search</button>
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-12">
                                            <button class="btn btn-raised btn-primary">Clear</button>
                                        </div>
                                    </div>
                            
                                    <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Name</th>
                                                <th>Designation</th>
                                                <th>Dates</th>
                                                <!-- <th>Action</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Rajesh Kumar</td>
                                                <td>Conductor</td>
                                                <td>
                                                    <!-- <div class="staff_attendence1"> <span>Absent</span> </div> -->
                                                    <div class="staff_attendence2"> <span>Present</span> </div>
                                                </td>
                                               
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Rajesh Kumar</td>
                                                <td>Conductor</td>
                                                <td>
                                                    <!-- <div class="staff_attendence1"> <span>Absent</span> </div> -->
                                                    <div class="staff_attendence2"> <span>Present</span> </div>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Rajesh Kumar</td>
                                                <td>Driver</td>
                                                <td>
                                                    <div class="staff_attendence1"> <span>Absent</span> </div>
                                                    <!-- <div class="staff_attendence2"> <span>Present</span> </div> -->
                                                </td>
                                               
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Rajesh Kumar</td>
                                                <td>Driver</td>
                                                <td>
                                                    <div class="staff_attendence1"> <span>Absent</span> </div>
                                                    <!-- <div class="staff_attendence2"> <span>Present</span> </div> -->
                                               
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<?php $__env->stopSection(); ?>

<style type="text/css">

.table tr .trip1{
   white-space: inherit !important;
}
#DataTables_Table_0_filter{
    float: right !important;
}
#DataTables_Table_0_paginate{
    float: right !important;
}


</style>

<!-- status fuction -->

<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {

    document.getElementById("myDropdown").classList.toggle("show");
}
function myFunction2() {

    document.getElementById("myDropdown2").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            block_name: {
                validators: {
                        stringLength: {
                        min: 4,
                    },
                        notEmpty: {
                        message: 'Please fill required block name'
                    }
                }
            },
            hostel_name: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required hostel name'
                    }
                }
            },
            floor_no: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required floor no'
                    }
                }
            },
            room_no: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required room no'
                    }
                }
            },
            room_alias: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required room alias'
                    }
                }
            },
            capacity: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required capacity'
                    }
                }
            },
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>