
<?php $__env->startSection('content'); ?>
<style type="text/css">
  
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Add Cheque Details</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>">Apply Fees on RTE Head</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/rte-received-cheque/add-rte-cheque-detail'); ?>">Add Cheque Details</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
              </div>
              <div class="body form-gap">
                  <div class="row tabless" >
                       <div class="headingcommon  col-lg-12">Add Cheque Details :-</div>
                    <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                    <div class="col-lg-3">
                      <div class="form-group">
                        <lable class="from_one1" for="name">Bank</lable>
                        <select class="form-control show-tick select_form1" name="classes" id="">
                          <option value="">Select Bank</option>
                          <option>Bank Of Baroda</option>
                          <option>State Bank of India</option>
                          <option>Bank of India</option>
                          <option>Punjab National Bank</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3">
                       <div class="form-group">
                          <lable class="from_one1" for="name">Cheque No. </lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Cheque no.">
                       </div>
                    </div>
                     <div class="col-lg-3">
                       <div class="form-group">
                          <lable class="from_one1" for="name">Cheque Date </lable>
                          <input type="text" name="name" id="chequeDate" class="form-control" placeholder="Cheque date">
                       </div>
                    </div>
                     <div class="col-lg-3">
                       <div class="form-group">
                          <lable class="from_one1" for="name">Amount </lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Amounts">
                       </div>
                    </div>
                     <div class="col-lg-10"></div>
                   <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised  btn-primary" style="margin-top: 23px !important; margin-left: 90px !important;" title="Save">Save
                      </button>
                    </div>
                  </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>



<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>