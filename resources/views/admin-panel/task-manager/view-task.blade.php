@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  .tabless table tr td .opendiv ul li{
    padding: 0px 0px !important;
  }
  .footable .dropdown-menu>li>a{
    padding: 10px 15px !important;
  }
  .footable .dropdown-menu{
    padding: 0px 0px !important; 
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Task</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <a href="{{ url('admin-panel/task-manager/add-task') }}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/menu/task-manager') }}">{!! trans('language.menu_task_manager') !!}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/task-manager/view-task') }}">View Task</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                 <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row">
                      <div class="col-lg-3">
                        <div class="form-group">
                          <!-- <lable class="from_one1" for="name">Task</lable> -->
                          <input type="text" name="name" id="name" class="form-control" placeholder="Task">
                        </div>
                      </div>
                     <div class="col-lg-3">
                        <div class="form-group">
                          <!-- <lable class="from_one1" for="name">Task Date</lable> -->
                          <input type="Date" name="" id="" class="form-control dateClass" placeholder="Task Date">
                        </div>
                      </div>
                     
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary" title="Submit">Search
                        </button>
                      </div>
                      <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                  <hr>
                  <!--  DataTable for view Records  -->
                  <table class="table  m-b-0 c_list">
                   {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Task</th>
                        <th>Priority</th>
                        <th>Task Date</th>
                        <th>Description</th>
                        <th class="text-center">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php $cou = 1; for ($i=0; $i < 10; $i++) {  ?> 
                      <tr>
                        <td><?php echo $cou; ?></td>
                        <td>Lorem Ipsum is... </td>
                        <td>High</td>
                        <td>30 July 2018 </td>
                        <td>Lorem Ipsum is printing ...  </td>
                       <!--  <td class="text-center"><a href="{{url('admin-panel/task-manager/view-task/view-responses')}}" title="View Response">View</a></td> -->
                        <td style="width: 200px;">
                          <div class="dropdown">
                            <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" id="btnCu">
                            <i class="fas fa-plus-circle" style="color: green;"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                              <li>
                                <a href="#" title="Open">Open</a>
                              </li>
                              <li>
                                <a href="#" title="On Hold">On Hold</a>
                              </li>
                              <li>
                                <a href="" title="Resolved">Resolved</a>
                              </li>
                              <li>
                                <a href="" title="Closed">Closed</a>
                              </li>
                            </ul>
                          </div>
                          <div class="dropdown">
                            <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" id="btnCu"><i class="zmdi zmdi-label"></i><span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                              <li>
                                <a href="{{url('admin-panel/task-manager/view-task/view-responses')}}" title="View Responses">View Responses</a>
                              </li>
                              <li>
                                <a href="{{url('admin-panel/task-manager/view-task/mapping')}}" title="Mapping">Mapping</a>
                              </li>
                            </ul>
                          </div>
                           <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>

                        </td>
                       
                      </tr>
                    <?php $cou++; } ?> 
                    </tbody>
                  </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
@endsection
<script type="text/javascript">
  $('#dropbtn').on('click', 'dropbtnAction', function () {
      $(this).toggleClass('active');
  });
</script>