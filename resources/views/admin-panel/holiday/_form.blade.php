@if(isset($holiday['holiday_id']) && !empty($holiday['holiday_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
    
.theme-blush .btn-primary {
    margin-top: 2px !important;
}
</style>

{!! Form::hidden('holiday_id',old('holiday_id',isset($holiday['holiday_id']) ? $holiday['holiday_id'] : ''),['class' => 'gui-input', 'id' => 'holiday_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.holiday_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('holiday_name', old('holiday_name',isset($holiday['holiday_name']) ? $holiday['holiday_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.holiday_name'), 'id' => 'holiday_name']) !!}
        </div>
        @if ($errors->has('holiday_name')) <p class="help-block">{{ $errors->first('holiday_name') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.holiday_start_date') !!} :</lable>
        <div class="form-group">
            {!! Form::text('holiday_start_date', old('holiday_start_date',isset($holiday['holiday_start_date']) ? $holiday['holiday_start_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.holiday_start_date'), 'id' => 'holiday_start_date']) !!}
        </div>
        @if ($errors->has('holiday_start_date')) <p class="help-block">{{ $errors->first('holiday_start_date') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.holiday_end_date') !!} :</lable>
        <div class="form-group">
            {!! Form::text('holiday_end_date', old('holiday_end_date',isset($holiday['holiday_end_date']) ? $holiday['holiday_end_date']: ''), ['class' => 'form-control ','placeholder'=>trans('language.holiday_end_date'), 'id' => 'holiday_end_date']) !!}
        </div>
        @if ($errors->has('holiday_end_date')) <p class="help-block">{{ $errors->first('holiday_end_date') }}</p> @endif
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
 <script type="text/javascript" src="http://v2rsolution.co.in/school/school-management/admin-panel/assets/plugins/momentjs/moment.js"></script>
<script type="text/javascript" src="http://v2rsolution.co.in/school/school-management/admin-panel/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script type="text/javascript">

     $('#holiday_end_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false }).on('change', function(e, date){ $(this).valid(); });
        $('#holiday_start_date').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false}).on('change', function(e, date) {  $(this).valid();  $('#holiday_end_date').bootstrapMaterialDatePicker('setMinDate', date);  });

   //      $('#holiday_start_date').bootstrapMaterialDatePicker({
   //   weekStart: 0, format: 'DD/MM/YYYY',time : false
   // });
        
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#holiday-form").validate({
            /* @validation states + elements  ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules ------------------------------------------ */
            rules: {
                holiday_name: {
                    required: true,
                    lettersonly:true
                },
                holiday_start_date: {
                    required: true
                },
                holiday_end_date: {
                    required: true
                },
            },
            /* @validation highlighting + error placement  ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        
    });

    

</script>