@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 14px 10px !important;
  }
  .nav-tabs>.nav-item>.nav-link {
    width: 100px;
    margin-right: 5px;
        border: 1px solid #ccc !important;
  }
  #tabingclass .nav-tabs>.nav-item>.nav-link {
    border-radius: 4px !important;
    padding: 8px 25px;
  }
   #tabingclass .nav-link:hover {
        background: #6572b8 !important;
    color: #fff !important;
}
#tabingclass .nav-link:active {
        background: #6572b8 !important;
    color: #fff !important;
}
.idi .nav-tabs>.nav-item>.nav-link.active {
  background: #6572b8 !important;
    color: #fff !important;
}
.modal-dialog {
  max-width: 550px !important;
}
.checkbox {
  float: left;
  margin-right: 20px;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Dairy Remarks</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/student') !!}">Student</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/student') !!}">Dairy Remarks</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student/dairy-remarks/view') !!}">View</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 20px !important;">
                 
                     <form class="" action="" method=""  id="" style="width: 100%;">
                      <div class="row">
                      
                      <div class="col-lg-2">
                    <div class="form-group">
                      <!-- <lable class="from_one1" for="name">Class</lable> -->
                      <select class="form-control show-tick select_form1" name="classes" id="">
                        <option value="">Class</option>
                        <option value="1">Class-1</option>
                        <option value="2">Class-2</option>
                        <option value="3">Class-3</option>
                        <option value="4">Class-4</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <!-- <lable class="from_one1" for="name">Class</lable> -->
                      <select class="form-control show-tick select_form1" name="classes" id="">
                        <option value="">Section</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                      </select>
                    </div>
                  </div>
                   <div class="col-lg-2">
                    <div class="form-group">
                      <!-- <lable class="from_one1" for="name">Class</lable> -->
                      <select class="form-control show-tick select_form1" name="classes" id="">
                        <option value="">Subject</option>
                        <option value="A">Subject-1</option>
                        <option value="B">Subject-2</option>
                        <option value="C">Subject-3</option>
                        <option value="D">Subject-4</option>
                      </select>
                    </div>
                  </div>
                   <div class="col-lg-2">
                    <div class="form-group">
                      <!-- <lable class="from_one1" for="name">Class</lable> -->
                      <select class="form-control show-tick select_form1" name="classes" id="">
                        <option value="">Teacher</option>
                        <option value="A">Teacher-1</option>
                        <option value="B">Teacher-2</option>
                        <option value="C">Teacher-3</option>
                        <option value="D">Teacher-4</option>
                      </select>
                    </div>
                  </div>
                   <div class="col-lg-2">
                        <div class="form-group">
                         <input type="text" name="enroll_no" placeholder="Enroll No" class="form-control">
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                         <input type="text" name="name" placeholder="Student Name" class="form-control">
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                         <input type="text" name="date" id="taskDate" placeholder="Date" class="form-control">
                        </div>
                      </div>
                     <div class="col-lg-1">
                      <button type="submit" class="btn btn-raised btn-primary " title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Clear
                      </button>
                    </div>
                  
                    </div>
                   </form>
                <hr style="width: 100%">
                    <!--  DataTable for view Records  -->
                     <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                      <thead>
                        <tr>
                          <th>S No</th>
                          <th>Enroll No</th>
                          <th>Student Name</th>
                          <th>Class - Section</th>
                          <th>Remarks</th>
                          <th>Subject</th>
                          <th>Teacher</th>
                          <th>Date</th>
                          <!-- <th>Action</th> -->
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 10; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td>1001A<?php echo $counter; ?></td>
                          <td style="width: 1px"><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%"> Ankit Dave</td>
                          <td>Class-1 - A</td>
                          <td><button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#RemarksModel">
                          Remark</button></td>
                          <!-- <td style="width: 250px;">All the Student of Group SG-1 hereby inform that These Students are participating in drama Competition.</td> -->
                          <td>Subject-<?php echo $counter; ?></td>
                          <td>Teacher-<?php echo $counter; ?></td>
                          <td>09-10-2018</td>
                           
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->


<!-- model -->
<div class="modal fade" id="sendMessageModel" tabindex="-1" role="dialog" style="padding: 40px;">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content send_messsage_button1">
            <div class="header header_model_2">
                <div class="header_model_1">
                    <h6 style="padding-left:20px;"><strong>Send Message</strong></h6>
                </div>
                <ul class="header-dropdown" style="padding-right: 20px; ">
                    <li class="remove remove_cross_x_2 float-right" style="position: relative; top: 20px;">
                        <a role="button" class="button_1" data-dismiss="modal"><i class="zmdi zmdi-close zmdi_close1"></i></a>
                    </li>
                </ul>
            
            </div>
          <div class="tab-pane active" id="classlist">
              <div class="card form-gap" style="padding: 20px 40px;">
                  <div class="body">
                        <form class="" action="" method=""  id="">
                            <div class="row clearfix">
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                    <lable class="from_one1">Subject :</lable>
                                    <div class="form-group">
                                       <input type="text" name="subject" class="form-control" placeholder="Subject">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <lable class="from_one1">Message :</lable>
                                    <div class="form-group">
                                       <textarea class="form-control" class="form-control" placeholder="Message"></textarea>
                                    </div>
                                </div>
                            </div>
                           <!--  <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <lable class="from_one1">Send Via :</lable>
                                    <div class="form-group">
                                        <div class="radio" style="margin:10px 4px !important;">
                                            <input type="radio" name="radio_option" id="radio1" value="option1">
                                            <label for="radio1" class="document_staff">Email</label>
                                            <input type="radio" name="radio_option" id="radio2" value="option2">
                                            <label for="radio2" class="document_staff">SMS</label>
                                            <input type="radio" name="radio_option" id="radio4" value="option4" checked="">
                                            <label for="radio4" class="document_staff">Both</label>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row clearfix">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-raised  btn-primary waves-effect float-right" data-dismiss="modal">Cancel</button>    
                                    <button type="submit" class="btn btn-raised  btn-primary float-right" id="send_msg" onclick="showMessage()">Send</button>
                                </div>
                            </div>
                        </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<!-- Model -->
<div class="modal fade" id="RemarksModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">View Remarks</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div style="width:500px;border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;">
             <div><b>Enroll No:</b> 1001A1</div>
             <div><b>Student Name:</b> Ankit Dave</div>
             <div><b>Subject:</b> Subject-1</div>
             <div><b>Remark:</b> All the Student of Group SG-1 hereby inform that These Students are participating in drama Competition.</div>
             </div>
      </div>
    </div>
  </div>
</div>
@endsection