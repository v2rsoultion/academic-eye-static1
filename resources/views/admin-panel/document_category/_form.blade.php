@if(isset($document_category['document_category_id']) && !empty($document_category['document_category_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif
<style type="text/css">
    .theme-blush .btn-primary {
    margin-top: 2px !important;
}
</style>
{!! Form::hidden('document_category_id',old('document_category_id',isset($document_category['document_category_id']) ? $document_category['document_category_id'] : ''),['class' => 'gui-input', 'id' => 'document_category', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.name_of_doc') !!} :</lable>
        <div class="form-group">
            {!! Form::text('document_category_name', old('doc_cate_name',isset($document_category['document_category_name']) ? $document_category['document_category_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.name_of_doc'), 'id' => 'shift_name']) !!}
        </div>
        @if ($errors->has('document_category_name')) <p class="help-block">{{ $errors->first('document_category_name') }}</p> @endif
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12">
        <label class="from_one1">{!! trans('language.document_category_for') !!} :</label>
        <div class="form-group">
            <div class="radio">

                <?php $student = '';?>
                    @if(isset($document_category['document_category_for']) && $document_category['document_category_for'] == 0)
                       <?php $student = 'checked'; ?>
                    @endif
                <?php $staff = ''; ?>
                    @if(isset($document_category['document_category_for']) && $document_category['document_category_for'] == 1)
                        <?php $staff = 'checked'; ?>
                    @endif
                <?php $both = ''; ?>
                    @if(isset($document_category['document_category_for']) && $document_category['document_category_for'] == 2)
                        <?php $both = 'checked'; ?>
                    @endif                    
                @if(!isset($document_category['document_category_for']))
                        <?php $student = 'checked'; ?>
                    @endif                                        

                {!! Form::radio('doc_cate_for','0',$student,['class' => 'form-control','id'=>'doc_cate_for1']) !!}
                <label for="doc_cate_for1">{!! trans('language.document_category_student') !!}</label>
                {!! Form::radio('doc_cate_for','1',$staff,['class' => 'form-control','id'=>'doc_cate_for2']) !!}
                <label for="doc_cate_for2">{!! trans('language.document_category_staff') !!}</label>
                {!! Form::radio('doc_cate_for','2',$both,['class' => 'form-control','id'=>'doc_cate_for3']) !!}
                <label for="doc_cate_for3">{!! trans('language.document_category_both') !!}</label>
            </div>
        </div>
        @if ($errors->has('doc_cate_for')) <p class="help-block">{{ $errors->first('doc_cate_for') }}</p> @endif 
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#document-category-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                document_category_name: {
                    required: true,
                    lettersonly:true
                },
                doc_cate_for: {
                    required: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
    });

    

</script>