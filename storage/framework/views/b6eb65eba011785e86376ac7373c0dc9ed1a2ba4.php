
<?php $__env->startSection('content'); ?>
<!--  Main content here -->

<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Free By Rte</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>">Map Free Student</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/view-rates'); ?>">Free By Rte</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
            <!--   <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
              </div> -->
              <div class="body form-gap">
                  <div class="row tabless" >
                  <form class="" action="" method="post"  id="searchpanel" style="width: 100%;">
                    <div class="row">
                    <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                    <div class="col-lg-2">
                      <div class="form-group">
                        <lable class="from_one1" for="name">Class</lable>
                        <select class="form-control show-tick select_form1" name="classes" id="">
                          <option value="">Class</option>
                          <option value="1">1st</option>
                          <option value="2">2nd</option>
                          <option value="3">3rd</option>
                          <option value="4">4th</option>
                          <option value="10th"> 10th</option>
                          <option value="11th">11th</option>
                          <option value="12th">12th</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-2">
                      <div class="form-group">
                        <lable class="from_one1" for="name">Section</lable>
                        <select class="form-control show-tick select_form1" name="classes" id="">
                          <option value="">Section</option>
                          <option value="A">A</option>
                          <option value="B">B</option>
                          <option value="C">C</option>
                          <option value="D">D</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised  btn-primary" style="margin-top: 23px !important;" title="Submit">Search
                      </button>
                    </div>
                     <div class="col-lg-1 ">
                      <button type="reset" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Clear">Clear
                      </button>
                    </div>
                    <div class="col-lg-2 "> </div>
                    <div class="col-lg-2 paddingtopclass"><b>Total Strength : </b> 2598</div>
                    <div class="col-lg-2 paddingtopclass "><b>No. of Applied : </b> 258956</div>
                    </div>
                </form>
                    <div class="clearfix"></div>
                    <!--  DataTable for view Records  -->
                    <table class="table m-b-0 c_list">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Roll No</th>
                          <th>Enroll No</th>
                          <th>Student Name</th>
                          <th>Father Name</th>
                          <th>Status</th>
                          <th class="text-center">Action</th>
                       </tr>
                      </thead>
                      <tbody>
                         <tr>
                          <td>1</td>
                          <td>156895</td>
                          <td>ABC20156</td>
                          <td>
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">
                           Ankit Dave
                        </td>
                          <td>Akash Dave</td>
                          <td class="text-success">  Free By RTE</td>
                          <td class="text-center">
                            <button type="button" class="btn btn-primary" title="Apply Free By RTE">Apply Free By RTE</button>
                          </td>
                        </tr>
                     
                        <tr>
                          <td>2</td>
                          <td>156895</td>
                          <td>ABC20156</td>
                          <td>
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">
                           Ankit Dave
                        </td>
                          <td>Akash Dave</td>
                        <td class="text-danger">NA</td>
                          <td class="text-center">
                            <button type="button" class="btn btn-primary" title="Apply Free By RTE">Apply Free By RTE</button>
                          </td>
                        </tr>
                   
                        <?php $conter = 3; for ($i=0; $i < 10 ; $i++) {  ?>
                        
                        <tr>
                          <td><?php echo $conter; ?></td>
                          <td>156895</td>
                          <td>ABC20156</td>
                          <td>
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">
                           Ankit Dave
                        </td>
                          <td>Akash Dave</td>
                          <td class="text-success">  Free By RTE</td>
                          <td class="text-center">
                            <button type="button" class="btn btn-primary" title="Apply Free By RTE">Apply Free By RTE</button>
                          </td>
                        </tr>
                       
                       <?php $conter++; } ?>
                      </tbody>
                    </table>
                  </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>