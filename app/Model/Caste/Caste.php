<?php

namespace App\Model\Caste;

use Illuminate\Database\Eloquent\Model;

class Caste extends Model
{
    protected $table      = 'caste';
    protected $primaryKey = 'caste_id';
}
