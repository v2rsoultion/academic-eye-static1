﻿@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .card .body .table td, .cshelf1 {
        width: 50px !important;
    }
    .pp{
        width: 800px !important;
    }
</style>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Free Space Room Report</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/hostel') !!}">{!! trans('language.menu_hostel') !!}</a></li>
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/hostel') !!}">Reports</a></li>
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/hostel/reports/free-space-room-report') !!}">Free Space Room Report</a></li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">
                                <!-- <div class="container-fluid"> -->
                                    <div class="row clearfix">
                                       <div class="col-lg-2 col-md-2 col-sm-12">
                                <lable class="from_one1">Hostel Name :</lable>                 
                                    <select class="form-control show-tick select_form1" name="">
                                        <option value="">Hostel Name</option>
                                        <option value="VII">Hostel-1</option>
                                        <option value="X">Hostel-2</option>
                                    
                                    </select>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12">
                                <lable class="from_one1">Block Name :</lable>                 
                                    <select class="form-control show-tick select_form1" name="block_name">
                                        <option value="">Block Name</option>
                                        <option value="1">Block-1</option>
                                        <option value="0">Block-2</option>
                                        <option value="0">Block-3</option>
                                        <option value="0">Block-4</option>

                                    </select>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12">
                                <lable class="from_one1">Floor No :</lable>                 
                                    <select class="form-control show-tick select_form1" name="floor_no">
                                        <option value="">Floor No</option>
                                        <option value="VII">Floor-1</option>
                                        <option value="X">Floor-2</option>
                                        <option value="X">Floor-3</option>
                                        <option value="X">Floor-4</option>
                                    </select>
                                </div>
                                 <div class="col-lg-2 col-md-2 col-sm-12">
                                <lable class="from_one1">Room No :</lable>                 
                                    <select class="form-control show-tick select_form1" name="room_no">
                                        <option value="">Room No</option>
                                        <option value="VII">Room-1</option>
                                        <option value="X">Room-2</option>
                                        <option value="X">Room-3</option>
                                        <option value="X">Room-4</option>
                                        <option value="X">Room-5</option>
                                        <option value="X">Room-6</option>
                                    </select>
                                </div>
                                        <div class="col-lg-1">
                                            <button class="btn btn-raised btn-primary saveBtn">Search</button>
                                        </div>
                                        <div class="col-lg-1">
                                            <button class="btn btn-raised btn-primary cancelBtn">Clear</button>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-lg-1" style="margin-left: 73px;">
                                            <button class="btn btn-raised btn-primary saveBtn">Export</button>
                                        </div>
                                    </div>
                                <!-- </div> -->
                                <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Hostel Name</th>
                                                <th>Block Name</th>
                                                <th>Floor No</th>
                                                <th>Room No</th>
                                                <th>Capacity</th>
                                                <th>Space Remaining</th>
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Hostel-1</td>
                                                <td>Block-1</td>
                                                <td>Floor-1</td>
                                                <td>Room-2</td>
                                                <td>4</td>
                                                <td>2</td>
                                                <td><button type="button" class="btn btn-raised btn-primary" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#studentDetailsModel" >View Details</button></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Hostel-1</td>
                                                <td>Block-2</td>
                                                <td>Floor-2</td>
                                                <td>Room-3</td>
                                                <td>10</td>
                                                <td>2</td>
                                                <td><button type="button" class="btn btn-raised btn-primary" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#studentDetailsModel" >View Details</button></td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Hostel-2</td>
                                                <td>Block-1</td>
                                                <td>Floor-10</td>
                                                <td>Room-21</td>
                                                <td>4</td>
                                                <td>4</td>
                                                <td><button type="button" class="btn btn-raised btn-primary" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#studentDetailsModel" >View Details</button></td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Hostel-6</td>
                                                <td>Block-4</td>
                                                <td>Floor-4</td>
                                                <td>Room-28</td>
                                                <td>5</td>
                                                <td>3</td>
                                                <td><button type="button" class="btn btn-raised btn-primary" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#studentDetailsModel" >View Details</button></td>
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


<!-- Action Model  -->
<div class="modal fade" id="studentDetailsModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Student Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row tabless">
        <table class="table  m-b-0 c_list pp">
            <thead>
              <tr>
                <th style="width: 60px;">S No</th>
                <th style="width: 140px;">Enroll No</th>
                <th>Name</th>
                <th style="width: 260px;">Class-Section</th>
              </tr>
            </thead>
            <tbody>
              <?php $count= 1; for ($i=0; $i < 5; $i++) {  ?>
              <tr>
                <td><?php echo $count; ?></td>
                <td>100A1</td>
                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%"> <a href="{{ url('/admin-panel/student/student-profile')}}" title="Link_to_profile">Ankit Dave</a></td>
                <td>XII - A</td>
              </tr>
              <?php $count++; } ?>
            </tbody>
          </table>
          
        </div>
      </div>
    </div>
  </div>
</div>