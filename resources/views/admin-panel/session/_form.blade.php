@if(isset($session['session_id']) && !empty($session['session_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
    .theme-blush .btn-primary {
    margin-top: 2px !important;
}
.btn-round.btn-simple {
    padding: 5px 22px !important;
}
</style>

{!! Form::hidden('session_id',old('session_id',isset($session['session_id']) ? $session['session_id'] : ''),['class' => 'gui-input', 'id' => 'session_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.session_name') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('session_name', old('session_name',isset($session['session_name']) ? $session['session_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.session_name'), 'id' => 'session_name']) !!}
        </div>
        @if ($errors->has('session_name')) <p class="help-block">{{ $errors->first('session_name') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.session_start_date') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('session_start_date', old('session_start_date',isset($session['session_start_date']) ? $session['session_start_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.session_start_date'), 'id' => 'session_start_date']) !!}
        </div>
        @if ($errors->has('session_start_date')) <p class="help-block">{{ $errors->first('session_start_date') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.session_end_date') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group">
            {!! Form::text('session_end_date', old('session_end_date',isset($session['session_end_date']) ? $session['session_end_date']: ''), ['class' => 'form-control ','placeholder'=>trans('language.session_end_date'), 'id' => 'session_end_date']) !!}
        </div>
        @if ($errors->has('session_end_date')) <p class="help-block">{{ $errors->first('session_end_date') }}</p> @endif
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("alphanumeric", function(value, element) {
            return this.optional(element) || /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/i.test(value);
        }, "Please enter session name like 2017-2018");

        $("#session-form").validate({
            /* @validation states + elements  ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules  ------------------------------------------ */
            rules: {
                session_name: {
                    required: true,
                    alphanumeric:true
                },
                session_start_date: {
                    required: true
                },
                session_end_date: {
                    required: true
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        
        
        
    });

    

</script>