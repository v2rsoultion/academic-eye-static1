@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-6 col-sm-12">
                <h2>{!! trans('language.manage_virtual_classes') !!}</h2>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.virtual_class') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.manage_virtual_classes') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
               
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">{!! trans('language.virtual_class') !!}</a></li>
                                    <li class="nav-item"><a class="nav-link"  href="{{ url('admin-panel/virtual-class/add-virtual-class') }}">{!! trans('language.add_virtual_class') !!}</a></li>
                                </ul>                        
                            </div> -->
                            <div class="body">
                                <div class="">
                                <table class="table m-b-0 c_list" id="designation-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.virtual_class_sr_no')}}</th>
                                            <th>{{trans('language.virtual_class_name')}}</th>
                                            <th>{{trans('language.virtual_class_description')}}</th>
                                            <th>{{trans('language.virtual_class_no_of_student')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#designation-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: '{{url('admin-panel/virtual-class/data')}}',
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'virtual_class_name', name: 'virtual_class_name'},
                {data: 'virtual_class_description', name: 'virtual_class_description'},
                {data: 'no_of_student', name: 'no_of_student'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 4, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });


</script>
@endsection




