<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\BookCupboard\BookCupboard; // Model
use Yajra\Datatables\Datatables;

class BookCupboardController extends Controller
{
    /**
     *  View page for Cub Board
     *  @Pratyush on 13 Aug 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_cupboard'),
            'redirect_url'  => url('admin-panel/book-cupboard/view-book-cupboard'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.cupboard.index')->with($data);
    }

    /**
     *  Add page for Cub Board
     *  @Pratyush on 13 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $cupboard		= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_cupboard_id 	= get_decrypted_value($id, true);
            $cupboard      			= BookCupboard::Find($decrypted_cupboard_id);
            if (!$cupboard)
            {
                return redirect('admin-panel/book-cupboard/add-book-cupboard')->withError('CupBoard not found!');
            }
            $page_title             	= trans('language.edit_cupboard');
            $encrypted_book_cupboard_id	= get_encrypted_value($cupboard->book_cupboard_id, true);
            $save_url               	= url('admin-panel/book-cupboard/save/' . $encrypted_book_cupboard_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_cupboard');
            $save_url      = url('admin-panel/book-cupboard/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'cupboard' 			=> $cupboard,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/book-cupboard/view-book-cupboard'),
        );
        return view('admin-panel.cupboard.add')->with($data);
    }

    /**
     *  Add and update CupBoard's data
     *  @Pratyush on 13 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_cupboard_id		= get_decrypted_value($id, true);
        if (!empty($id))
        {
            $cupboard = BookCupboard::find($decrypted_cupboard_id);

            if (!$cupboard)
            {
                return redirect('/admin-panel/book-cupboard/add-book-cupboard/')->withError('CupBoard not found!');
            }
            $success_msg = 'CupBoard updated successfully!';
        }
        else
        {
            $cupboard     	= New BookCupboard;
            $success_msg 	= 'CupBoard saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'book_cupboard_name'   => 'required|unique:book_cupboards,book_cupboard_name,' . $decrypted_cupboard_id . ',book_cupboard_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $cupboard->admin_id       		= $loginInfo['admin_id'];
                $cupboard->update_by      		= $loginInfo['admin_id'];
                $cupboard->school_id      		= $loginInfo['school_id'];
                $cupboard->book_cupboard_name 	= Input::get('book_cupboard_name');
                $cupboard->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/book-cupboard/view-book-cupboard')->withSuccess($success_msg);
    }

    /**
     *  Get Cupboard's Data for view page(Datatables)
     *  @Pratyush on 13 Aug 2018.
    **/
    public function anyData()
    {
        $loginInfo 			= get_loggedin_user_data();
        $cupboard  			= BookCupboard::where([['school_id', '=', $loginInfo['school_id']]])->orderBy('book_cupboard_id', 'ASC')->get();

        return Datatables::of($cupboard)
        		->addColumn('action', function ($cupboard)
                {
                    $encrypted_cupboard_id = get_encrypted_value($cupboard->book_cupboard_id, true);
                    if($cupboard->book_cupboard_status == 0) {
                        $status = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="book-cupboard-status/'.$status.'/' . $encrypted_cupboard_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-book-cupboard/' . $encrypted_cupboard_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-book-cupboard/' . $encrypted_cupboard_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Destroy CupBoard's data
     *  @Pratyush on 13 Aug 2018.
    **/
    public function destroy($id)
    {
        $cupboard_id	= get_decrypted_value($id, true);
        $cupboard	  	= BookCupboard::find($cupboard_id);
        if ($cupboard)
        {
            $cupboard->delete();
            $success_msg = "CupBoard deleted successfully!";
            return redirect('admin-panel/book-cupboard/view-book-cupboard')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "CupBoard not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Title's status
     *  @Pratyush on 20 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $cupboard_id	= get_decrypted_value($id, true);
        $cupboard	  	= BookCupboard::find($cupboard_id);
        if ($cupboard)
        {
            $cupboard->book_cupboard_status  = $status;
            $cupboard->save();
            $success_msg = "CupBoard status updated!";
            return redirect('admin-panel/book-cupboard/view-book-cupboard')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "CupBoard not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
