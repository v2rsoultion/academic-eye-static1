@if(isset($stream['stream_id']) && !empty($stream['stream_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
    /*.theme-blush .btn-primary {
    margin-top: 4px !important;
}*/
</style>

{!! Form::hidden('stream_id',old('stream_id',isset($stream['stream_id']) ? $stream['stream_id'] : ''),['class' => 'gui-input', 'id' => 'stream_id', 'readonly' => 'true']) !!}
<p class="red">
@if ($errors->any())
    {{$errors->first()}}
@endif
</p>
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.stream_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('stream_name', old('stream_name',isset($stream['stream_name']) ? $stream['stream_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.stream_name'), 'id' => 'stream_name']) !!}
        </div>
        @if ($errors->has('stream_name')) <p class="help-block">{{ $errors->first('stream_name') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.medium_type') !!} : </lable>
        <label class=" field select" style="width: 100%">
            {!!Form::select('medium_type', $stream['arr_medium'],isset($stream['medium_type']) ? $stream['medium_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type', 'data-live-search'=>'true'])!!}
            <i class="arrow double"></i>
        </label>
        <div class="clearfix"></div>
        @if ($errors->has('medium_type')) <p class="help-block">{{ $errors->first('medium_type') }}</p> @endif
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#stream-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                stream_name: {
                    required: true,
                    lettersonly:true
                },
                medium_type: {
                    required: true,
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>