
<?php $__env->startSection('content'); ?>
<style type="text/css">
  td{
    /*padding: 14px 10px !important;*/
  }
  .nav-tabs>.nav-item>.nav-link {
    width: 100px;
    margin-right: 5px;
        border: 1px solid #ccc !important;
  }
  #tabingclass .nav-tabs>.nav-item>.nav-link {
    border-radius: 4px !important;
    padding: 8px 25px;
  }
   #tabingclass .nav-link:hover {
        background: #6572b8 !important;
    color: #fff !important;
}
#tabingclass .nav-link:active {
        background: #6572b8 !important;
    color: #fff !important;
}
.idi .nav-tabs>.nav-item>.nav-link.active {
  background: #6572b8 !important;
    color: #fff !important;
}
.modal-dialog {
  max-width: 700px !important;
}
.checkbox {
  float: left;
  margin-right: 20px;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-12">
        <h2>Parent Group</h2>
      </div>
      <div class="col-lg-8 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/student'); ?>"><?php echo trans('language.menu_student'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/student'); ?>">Group</a></li>
          <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/student/group/parent-group'); ?>">Parent Group</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 20px !important;">
                 
                     <form class="" action="" method=""  id="" style="width: 100%;">
                      <div class="row">
                       <div class="col-lg-3">
                         <div class="form-group">
                         <!--  <lable class="from_one1">Exam Name</lable>
                          <input type="text" name="title" placeholder="Title" class="form-control"> -->
                          <select class="form-control show-tick select_form1" name="class_name">
                            <option value="">Select Group</option>
                            <option value="1">PG-1</option>
                            <option value="2">PG-2</option>
                            <option value="3">PG-3</option>
                            <option value="4">PG-4</option>
                        </select>
                         </div>
                      </div>
                     <div class="col-lg-1">
                      <button type="submit" class="btn btn-raised btn-primary " title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Clear
                      </button>
                    </div>
                    </div>
                   </form>
                <hr style="width: 100%">
                    <!--  DataTable for view Records  -->
                     <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                      <thead>
                        <tr>
                          <th>S No</th>
                          <th>Name</th>
                          <th>Description</th>
                          <th>No of Parents</th>
                          <th>Action</th>
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 10; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td>PG-<?php echo $counter; ?></td>
                          <td style="width: 300px">All the Student of Group SG-1 hereby inform that These Students are participating in drama Competition.</td>
                          <td>6</td>
                          <td> <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li> <a title="View Candidates" href="<?php echo url('admin-panel/student/group/parent-group/manage-parent'); ?>">Manage Parent</a></li> 
                                <li> <a title="View Candidates" href="<?php echo url('admin-panel/student/group/parent-group/parent-list'); ?>">Send Message</a></li> 
                            </ul> </div>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit Group"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete Group"><i class="zmdi zmdi-delete"></i></button></td>
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>


<!-- Action Model  -->
<div class="modal fade" id="noticeInfoModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Notice Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="headingcommon  col-lg-12" style="padding: 5px 2px;">Class</div>
        <div style="width:645px;border: 1px solid #ccc;font-weight: bold; border-radius: 5px;padding: 5px 10px;">
           Class: Class-1, Class-2, Class-3, Class-4, Class-5, Class-6, Class-7, Class-8, Class-9, Class-10
        </div> 
         <div class="headingcommon  col-lg-12" style="padding: 5px 2px;">Student-Group</div>
        <div style="width:645px;border: 1px solid #ccc;font-weight: bold; border-radius: 5px;padding: 5px 10px;">
           Student-Group: SGroup-1, SGroup-2, SGroup-3, SGroup-4
        </div> 
         <div class="headingcommon  col-lg-12" style="padding: 5px 2px;">Parent-Group</div>
        <div style="width:645px;border: 1px solid #ccc;font-weight: bold; border-radius: 5px;padding: 5px 10px;">
           Parent-Group: PGroup-1, PGroup-2, PGroup-3, PGroup-4
        </div> 
                
      </div>
    </div>
  </div>
</div>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>