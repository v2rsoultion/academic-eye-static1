<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    protected $table      = 'staff_leaves';
    protected $primaryKey = 'staff_leave_id';
    public function up()
    {
        if (!Schema::hasTable('staff_leaves')) {
            Schema::create('staff_leaves', function (Blueprint $table) {
                $table->increments('staff_leave_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->text('staff_leave_reason')->nullable();
                $table->string('staff_leave_type',255)->nullable();
                $table->date('staff_leave_from_date')->nullable();
                $table->date('staff_leave_to_date')->nullable();
                $table->text('staff_leave_attachment')->nullable();
                $table->tinyInteger('staff_leave_status')->default(0)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('staff_leaves', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('staff_leaves', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('staff_leaves', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_leaves');
    }
}
