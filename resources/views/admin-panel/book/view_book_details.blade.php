@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_bbook') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ URL::to('admin-panel/book/view-books') }}">{!! trans('language.bbook') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_bbook') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">{!! trans('language.title') !!}</a></li>
                                    <li class="nav-item"><a class="nav-link"  href="{{ url('admin-panel/title/add-title') }}">{!! trans('language.add_title') !!}</a></li>
                                </ul>                        
                            </div> -->
                            <div class="body">
                                {!! Form::hidden('id',$id,['readonly' => 'true']) !!}
                                <div class="profile_detail_1">
                                    <b class="profile_detail_bold_1">Book :</b><span class="profile_detail_span_1"> {{ $book->book_name }}</span>
                                </div>                                    
                                <div class="profile_detail_1">
                                    <b class="profile_detail_bold_1">Subtitle:</b><span class="profile_detail_span_1"> {{ $book->book_subtitle }}</span>
                                </div>
                                <div class="profile_detail_1">
                                    <b class="profile_detail_bold_1">Author :</b><span class="profile_detail_span_1"> {{ $book->author_name }}</span>
                                </div>                                                                        
                                <div class="profile_detail_1">
                                    <b class="profile_detail_bold_1">Publisher :</b><span class="profile_detail_span_1"> {{ $book->publisher_name }}</span>
                                </div>                                    
                                <div class="profile_detail_1">
                                    <b class="profile_detail_bold_1">Edition :</b><span class="profile_detail_span_1"> {{ $book->edition }}</span>
                                </div>                                    
                                <div class="profile_detail_1">
                                    <b class="profile_detail_bold_1">book_price :</b><span class="profile_detail_span_1"> {{ $book->book_price }}</span>
                                </div>                                    
                                <div class="profile_detail_1">
                                    <b class="profile_detail_bold_1">Remark :</b><span class="profile_detail_span_1"> {{ $book->book_remark }}</span>
                                </div>                                    
                                
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="book-detail-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{trans('language.bbook_unique_no')}}</th>
                                            <th>{{trans('language.bbook_issue_available')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#book-detail-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            
            // ajax: "{{url('admin-panel/book/single-book-data')}}",
            ajax: {
                url: "{{url('admin-panel/book/single-book-data')}}",
                data: function (d) {
                    d.b_id = $('input[name=id]').val();
                }
            },
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'book_unique_id', name: 'book_unique_id'},
                {data: 'issued_copies', name: 'issued_copies'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 2, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });


</script>
@endsection




