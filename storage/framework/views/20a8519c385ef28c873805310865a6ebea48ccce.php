<!-- <?php if(isset($staff['staff_id']) && !empty($staff['staff_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?> -->

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
  /*  .theme-blush .btn-primary {
    margin-top: 3px !important;
}*/

 input[type='radio']:after {
        width: 18px;
        height: 18px;
        border-radius: 100%;
        top: -2px;
        left: -3px;
        position: relative;
        border: 1px solid #d1d3d1 !important;
        background-color: #fff;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }

    .red input[type='radio']:checked:after {
        width: 18px;
        height: 18px;
        border-radius: 100%;
        top: -2px;
        left: -3px;
        position: relative;
        background-color: #e80b15;
        border: 1px solid #e80b15 !important;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
      }

      .green input[type='radio']:checked:after {
        width: 18px;
        height: 18px;
        border-radius: 15px;
        top: -2px;
        left: -3px;
        position: relative;
        background-color: #235409;
        border: 1px solid #235409 !important;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
      }
      .document_staff {
        padding-left: 12px;
        margin-right: 30px;
      }
</style>

<!-- <?php echo Form::hidden('staff_id',old('staff_id',isset($staff['staff_id']) ? $staff['staff_id'] : ''),['class' => 'gui-input', 'id' => 'staff_id', 'readonly' => 'true']); ?> -->

<div class="alert alert-success" role="alert" id="message" style="display: none; margin-top: 20px">
    Attendance successfully added for today - 18 Oct 2018
    <button style="margin-top: 10px" type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<!-- Basic Info section --> 
<div class="headingcommon  col-lg-12">Date : 31-10-2018</div>   
    <div class="float-right">
      <button type="button" class="btn btn-raised btn-primary" style="margin-right: 10px !important" title="Save" onclick="showMessage()">Save
      </button>
    </div>
   <div class="table-responsive">
    <table class="table m-b-0 c_list" id="#" style="width:100%">
    <?php echo e(csrf_field()); ?>

        <thead>
            <tr>
                <th><?php echo e(trans('language.s_no')); ?></th>
                <th>Staff Id</th>
                <th style="width: 330px;">Staff Name</th>
                <th>Attendance</th>
            </tr>
        </thead>
        <tbody>
                <tr>
                  <td>1</td>
                  <td>Staff101</td>
                   <td >
                   <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                   Ankit Dave
                    </td>
                   <!--  <td><div class="radio" style="margin-top:6px !important;"><input id="radio31" name="staff_marital_status" type="radio" value="1" multiple="multiple">
                    <label for="radio31" class="document_staff">Present</label>
                    <input id="radio32" checked="checked" name="staff_marital_status" type="radio" value="0">
                    <label for="radio32" class="document_staff">Absent</label> </div></td> -->

                    <td>
                      <div class="green float-left" style="margin-top:6px !important;">
                      <input id="attendance_status11" name="attendance_status1" checked="checked" type="radio" value="1" >
                      <label for="attendance_status11" class="document_staff">Present</label>
                    </div>

                    <div class="red float-left" style="margin-top:6px !important;">
                      <input id="attendance_status12" name="attendance_status1" type="radio" value="0">
                      <label for="attendance_status12" class="document_staff">Absent</label> 
                    </div>
                  </td>
                </tr>
                  <tr>
                  <td>2</td>
                  <td>Staff102</td>
                   <td>
                   <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                   Ankit Dave
                    </td>
                   <!--  <td><div class="radio" style="margin-top:6px !important;"><input id="radio33" name="staff_marital_status" type="radio" value="1" multiple="multiple">
                    <label for="radio33" class="document_staff">Present</label>
                    <input id="radio34" checked="checked" name="staff_marital_status" type="radio" value="0">
                    <label for="radio34" class="document_staff">Absent</label> </div></td> -->
                    <td><div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status21" name="attendance_status2" checked="checked" type="radio" value="1" >
                    <label for="attendance_status21" class="document_staff">Present</label></div>
                    <div class="red float-left" style="margin-top:6px !important;">
                    <input id="attendance_status22" name="attendance_status2" type="radio" value="0">
                    <label for="attendance_status22" class="document_staff">Absent</label> </div></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Staff103</td>
                   <td>
                   <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                   Ankit Dave
                    </td>
                    <td><div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status31" name="attendance_status3" checked="checked" type="radio" value="1" >
                    <label for="attendance_status31" class="document_staff">Present</label></div>
                    <div class="red float-left" style="margin-top:6px !important;">
                    <input id="attendance_status32" name="attendance_status3" type="radio" value="0">
                    <label for="attendance_status32" class="document_staff">Absent</label> </div></td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>Staff104</td>
                   <td>
                   <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                   Ankit Dave
                    </td>
                    <td><div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status41" name="attendance_status4" checked="checked" type="radio" value="1" >
                    <label for="attendance_status41" class="document_staff">Present</label></div>
                    <div class="red float-left" style="margin-top:6px !important;">
                    <input id="attendance_status42" name="attendance_status4" type="radio" value="0">
                    <label for="attendance_status42" class="document_staff">Absent</label> </div></td>
                </tr>
               <tr>
                  <td>5</td>
                  <td>Staff105</td>
                   <td>
                   <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                   Ankit Dave
                    </td>
                   <td><div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status51" name="attendance_status5" checked="checked" type="radio" value="1">
                    <label for="attendance_status51" class="document_staff">Present</label></div>
                    <div class="red float-left" style="margin-top:6px !important;">
                    <input id="attendance_status52"  name="attendance_status5" type="radio" value="0">
                    <label for="attendance_status52" class="document_staff">Absent</label> </div></td>
                </tr>
                  <tr>
                  <td>6</td>
                  <td>Staff106</td>
                   <td>
                   <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                   Ankit Dave
                    </td>
                  <td><div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status61" name="attendance_status6" checked="checked" type="radio" value="1" >
                    <label for="attendance_status61" class="document_staff">Present</label></div>
                    <div class="red float-left" style="margin-top:6px !important;">
                    <input id="attendance_status62" name="attendance_status6" type="radio" value="0">
                    <label for="attendance_status62" class="document_staff">Absent</label> </div></td>
                </tr>
                  <tr>
                  <td>7</td>
                  <td>Staff107</td>
                   <td>
                   <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                   Ankit Dave
                    </td>
                  <td><div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status71" name="attendance_status7" checked="checked" type="radio" value="1">
                    <label for="attendance_status71" class="document_staff">Present</label></div>
                    <div class="red float-left" style="margin-top:6px !important;">
                    <input id="attendance_status72" name="attendance_status7" type="radio" value="0">
                    <label for="attendance_status72" class="document_staff">Absent</label> </div></td>
                </tr>
                  <tr>
                  <td>8</td>
                  <td>Staff108</td>
                   <td >
                   <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                   Ankit Dave
                    </td>
                  <td><div class="green float-left" style="margin-top:6px !important;"><input id="attendance_status81" name="attendance_status8" checked="checked" type="radio" value="1">
                    <label for="attendance_status81" class="document_staff">Present</label></div>
                    <div class="red float-left" style="margin-top:6px !important;">
                    <input id="attendance_status82" name="attendance_status8" type="radio" value="0">
                    <label for="attendance_status82" class="document_staff">Absent</label> </div></td>
                </tr>
        </tbody>
    </table>
</div>

<hr>
<div class="float-right">
      <button type="button" class="btn btn-raised btn-primary" style="margin-top: 10px !important; margin-right: 10px !important;" title="Save" onclick="showMessage()">Save
      </button>
</div>
<script type="text/javascript">
  function showMessage() {
    document.getElementById('message').style.display = "block";
  }
</script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#staff-attendance-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation  rules 
             ------------------------------------------ */

            rules: {
                staff_name: {
                    required: true
                },
                staff_profile_img: {
                    required: true
                },
                student_name: {
                    required: true
                },
                student_email: {
                    required: true,
                    email: true
                },  
            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                     element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
        $('#staff_dob').bootstrapMaterialDatePicker({ weekStart : 0,time: false });
        
    });
</script>