
<?php $__env->startSection('content'); ?>
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2><?php echo trans('language.menu_student'); ?></h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/student'); ?>"><?php echo trans('language.menu_student'); ?></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/Student/ViewList.svg'); ?>" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">View List</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="<?php echo e(url('/admin-panel/student/add-student')); ?>" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="<?php echo e(url('/admin-panel/student/view-list')); ?>" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                        <!-- <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="#" title="">Option 1</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 2</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 3</a>
                              </li>
                            </ul>
                          </li>
                        </ul> -->
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
                    <div class="col-md-3">
                      <div class="dashboard_div">
                        <div class="imgdash">
                          <img src="<?php echo URL::to('public/assets/images/Student/ParentInformation.svg'); ?>" alt="Student" >
                          </div>
                          <h4 class="">
                            <div class="tableCell" style="height: 64px;">
                              <div class="insidetable">Parent Information</div>
                            </div>
                          </h4>
                          <div class="clearfix"></div>
                        
                          <a href="<?php echo e(url('/admin-panel/student/parent-information')); ?>" class="cusa" style="width: 48%; margin: 17px auto;" title="View ">
                            <i class="fas fa-eye"></i>View 
                          </a>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <?php endif; ?>
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="<?php echo URL::to('public/assets/images/Student/Attendence.svg'); ?>" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Attendence</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="<?php echo e(url('admin-panel/student/student-attendance/add')); ?>" class="float-left" title="Add ">
                              <i class="fas fa-plus"></i> Add 
                            </a>
                            <a href="<?php echo e(url('admin-panel/student/student-attendance/view')); ?>" class="float-right" title="View ">
                              <i class="fas fa-eye"></i>View 
                            </a>
                            <div class="clearfix"></div>
                             <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="<?php echo e(url('/admin-panel/student/student-attendance/edit-student-attendance')); ?>" title="">Edit Attendance</a>
                              </li> 
                            </ul>
                          </li>
                        </ul>
                        <div class="row clearfix"></div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="<?php echo URL::to('public/assets/images/Student/LeaveManagement.svg'); ?>" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Leave Management</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="<?php echo e(url('/admin-panel/student-leave-application/add-leave-application')); ?>" class="float-left" title="Add ">
                                <i class="fas fa-plus"></i> Add 
                              </a>
                              <a href="<?php echo e(url('/admin-panel/student-leave-application/view-leave-application')); ?>" class="float-right" title="View ">
                                <i class="fas fa-eye"></i>View 
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                          <?php if($login_info['admin_type'] == 2): ?>
                            <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/Staff/Performance.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Remarks</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="<?php echo e(url('admin-panel/student/remarks/add-remark')); ?>" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a>
                                <a href="<?php echo e(url('admin-panel/student/remarks/view-remark')); ?>" class="float-right" title="View ">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="row">
                          <?php if($login_info['admin_type'] == 2): ?>
                           <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/Staff/homework.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Home Work</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="<?php echo e(url('admin-panel/student/homework/add-homework')); ?>" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a>
                                <a href="<?php echo e(url('admin-panel/student/homework/view-homework')); ?>" class="float-right" title="View ">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          <?php endif; ?>
                           <?php if($login_info['admin_type'] == 2 ): ?>
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="<?php echo URL::to('public/assets/images/Staff/Performance.svg'); ?>" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Marks</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="" class="float-left" title="Add ">
                              <i class="fas fa-plus"></i> Add 
                            </a>
                            <a href="" class="float-right" title="View ">
                              <i class="fas fa-eye"></i>View 
                            </a>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        <?php endif; ?>
                          <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
                          <div class="col-md-3">
                            <div class="dashboard_div" >
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/Student/Group.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Groups</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="<?php echo e(url('admin-panel/student/group/add-group')); ?>" class="cusa" title="Add" style="width: 50%; margin: 20px auto;">
                                  <i class="fas fa-plus"></i>Add Group 
                                </a>
                            
                                <ul class="header-dropdown opendiv">
                                  <li class="dropdown">
                                    <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                      <i class="fas fa-ellipsis-v"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right slideUp">
                                      <li>
                                        <a href="<?php echo e(url('admin-panel/student/group/student-group')); ?>" title="">Student Group </a>
                                      </li> 
                                      <li>
                                        <a href="<?php echo e(url('admin-panel/student/group/parent-group')); ?>" title="">Parent Group  </a>
                                      </li> 
                                    </ul>
                                  </li>
                                </ul>
                                <div class="row clearfix"></div>
                              </div>
                            </div>
                            <?php endif; ?>
                            <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
                            <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Student/DairyRemarks.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Dairy Remarks</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                
                                  <a href="<?php echo e(url('admin-panel/student/dairy-remarks/view')); ?>" class="cusa" title="View " style="width: 38%; margin: 20px auto;">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              <?php endif; ?>
                              <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
                              <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Staff/homework.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">HomeWork Group</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                
                                  <a href="<?php echo e(url('admin-panel/student/homework-group/view')); ?>" class="cusa" title="View " style="width: 38%; margin: 20px auto;">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              <?php endif; ?>
                              <?php if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1): ?>
                              <div class="col-md-3">
                                <div class="dashboard_div">
                                  <div class="imgdash">
                                    <img src="<?php echo URL::to('public/assets/images/Student/Report.svg'); ?>" alt="Student">
                                    </div>
                                    <h4 class="">
                                      <div class="tableCell" style="height: 64px;">
                                        <div class="insidetable">Report</div>
                                      </div>
                                    </h4>
                                    <div class="clearfix"></div>
                                    <a href="" class="cusa" title="View" style="width: 58%; margin:20px auto;">
                                      <i class="fas fa-eye"></i>View Reports
                                    </a>
                                    <div class="clearfix"></div>
                                  </div>
                                </div>
                                <?php endif; ?>
                                <!-- <div class="col-md-3">
                                  <div class="dashboard_div">
                                    <div class="imgdash">
                                      <img src="<?php echo URL::to('public/assets/images/Student/ParentInformation.svg'); ?>" alt="Student">
                                      </div>
                                      <h4 class="">
                                        <div class="tableCell" style="height: 64px;">
                                          <div class="insidetable">Virtual Class</div>
                                        </div>
                                      </h4>
                                      <div class="clearfix"></div>
                                      <a href="" class="float-left" title="Add ">
                                        <i class="fas fa-plus"></i> Add 
                                      </a>
                                      <a href="" class="float-right" title="View ">
                                        <i class="fas fa-eye"></i>View 
                                      </a>
                                      <div class="clearfix"></div>
                                    </div>
                                  </div> -->
                                </div>
                                
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>