<?php if(isset($state['state_id']) && !empty($state['state_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?>

<?php echo Form::hidden('state_id',old('state_id',isset($state['state_id']) ? $state['state_id'] : ''),['class' => 'gui-input', 'id' => 'state_id', 'readonly' => 'true']); ?>

<style type="">

/*.theme-blush .btn-primary {
    margin-top: 4px !important;
}*/
</style>

<?php if($errors->any()): ?>
    <div class="alert alert-danger" role="alert">
    <?php echo e($errors->first()); ?>

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.country_name'); ?> :</lable>
        <label class=" field select" style="width: 100%">
            <?php echo Form::select('country_id', $state['arr_country'],isset($state['country_id']) ? $state['country_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'country_id', 'onChange'=>'getClassSection(this.value)']); ?>

            <i class="arrow double"></i>
        </label>
        <?php if($errors->has('country_id')): ?> <p class="help-block"><?php echo e($errors->first('country_id')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1"><?php echo trans('language.state_name'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('state_name', old('state_name',isset($state['state_name']) ? $state['state_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.state_name'), 'id' => 'state_name']); ?>

        </div>
        <?php if($errors->has('state_name')): ?> <p class="help-block"><?php echo e($errors->first('state_name')); ?></p> <?php endif; ?>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']); ?>

        <a href="<?php echo url('admin-panel/dashboard'); ?>" ><?php echo Form::button('Cancel', ['class' => 'btn btn-raised btn-round']); ?></a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#state-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            // errorLabelContainer: '.errorTxt',

            /* @validation  rules 
             ------------------------------------------ */
            rules: {
                country_id: {
                    required: true
                },
                state_name: {
                    required: true,
                    lettersonly:true
                },
            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });   
    });
</script>