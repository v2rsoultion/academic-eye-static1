<?php $__env->startSection('content'); ?>
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }
    
    .para{
        width: 20%;
        /*word-wrap: break-word !important;*/
    }
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>View Visitors</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <a href="<?php echo url('admin-panel/visitor/add-visitor'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/visitor'); ?>"><?php echo trans('language.menu_visitor'); ?></a></li>
                   <!--  <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/visitor'); ?>"><?php echo trans('language.visitor_list'); ?></a></li> -->
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/visitor/view-visitor'); ?>">View Visitors</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap ">
                                <form>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-12">
                                         <div class="form-group">
                                            <!-- <lable class="from_one1" for="name">Name </lable> -->
                                            <input type="text" name="name" id="" class="form-control" placeholder="Name">
                                         </div>
                                      </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12">
                                         <div class="form-group">
                                            <!-- <lable class="from_one1" for="name">Date </lable> -->
                                            <input type="text" name="Visit_date" id="visitDate" class="form-control" placeholder="Visit Date">
                                         </div>
                                      </div>
                                                                            
                      <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-primary"  title="Search">Search
                      </button>
                    </div>
                     <div class="col-lg-1 ">
                      <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                      </button>
                    </div>
                                    </div>
                                </form>
                                <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th><?php echo e(trans('language.s_no')); ?></th>
                                                <th>Name</th>
                                                <th>Email ID</th>
                                                <th>Contact No</th>
                                                <th>Visit Date</th>
                                                <th>Purpose</th>
                                                <th>Visit Time</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Rajesh Kumar</td>
                                                <td>rajeshkumar@gmail.com</td>
                                                <td>+91 9008090080</td>
                                                <td>09-09-2018</td>
                                                <td class="para">Come to know more about this school. Come to know more about this school</td>
                                                <td>10:00AM - 12:30PM</td>
                                                <td> <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div></td>
                                            </tr>
                                            <tr>
                                                 <td>2</td>
                                                <td>Rajesh Kumar</td>
                                                <td>rajeshkumar@gmail.com</td>
                                                <td>+91 9008090080</td>
                                                <td>09-09-2018</td>
                                                <td class="para">Come to know more about this school. Come to know more about this school</td>
                                                <td>10:00AM - 12:30PM</td>
                                                <td> <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div></td>
                                            </tr>
                                            <tr>
                                                 <td>3</td>
                                                <td>Rajesh Kumar</td>
                                                <td>rajeshkumar@gmail.com</td>
                                                <td>+91 9008090080</td>
                                                <td>09-09-2018</td>
                                                <td class="para">Come to know more about this school. 
                                                    Come to know more about this school</td>
                                                <td>10:00AM - 12:30PM</td>
                                                <td> <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#staff-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '<?php echo e(url('admin-panel/staff/data')); ?>',
                data: function (d) {
                    d.class_id = $('input[name="name"]').val();
                    d.section_id = $('select[name="designation_id"]').val();
                }
            },
            //ajax: '<?php echo e(url('admin-panel/class/data')); ?>',
           
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'staff_name', name: 'staff_name'},
                {data: 'staff_designation_name', name: 'staff_designation_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

</script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>