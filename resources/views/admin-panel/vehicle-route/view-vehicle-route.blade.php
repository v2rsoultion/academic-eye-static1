﻿@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive{
        overflow-x: visible !important;
    }
</style>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>View Vehicle Route</h2>
            </div>
             <div class="col-lg-7 col-md-6 col-sm-12 line">
                 <a href="{!! url('admin-panel/transport/vehicle/add-vehicle') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right ">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/transport') !!}">Transport<!-- {!! trans('language.staff') !!} --></a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/transport') !!}">Vehicle Route</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/vehicle-route/view-vehicle-route') !!}">View Vehicle Route</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">
                                <form>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('vehicle_name', old('vehicle_name', ''), ['class' => 'form-control ','placeholder'=>'Vehicle Name', 'id' => 'vehicle_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('vehicle_reg_number', old('vehicle_reg_number', ''), ['class' => 'form-control ','placeholder'=>'Vehicle Registration Number', 'id' => 'vehicle_reg_number']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('contact_number', old('contact_number', ''), ['class' => 'form-control ','placeholder'=>'Contact Number', 'id' => 'contact_number']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 ">
                                          <button type="submit" class="btn btn-raised btn-primary"  title="Search">Search
                                          </button>
                                        </div>
                                         <div class="col-lg-1 ">
                                          <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                                          </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Route Name</th>
                                                <th>Route Description</th>
                                                <th>Location</th>
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>ratanada-pavta</td>
                                                <td>going direct ratanada to  pavta</td>
                                                <td>Pickup</td>
                                                <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                <li> <a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#addLocationModel">Add Pickup/Drop Location</a>

                                                <li> <a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#viewLocationModel">View Pickup/Drop Location</a></li> 

                                                <li> <a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#viewRouteModal">View Route Map</a></li>
                                                </ul> </div></td>
                                            </tr>
                                            <tr>
                                            <tr>
                                                <td>2</td>
                                                <td>ratanada-pavta</td>
                                                <td>going direct ratanada to  pavta</td>
                                                <td>Pickup</td>
                                                <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                <li> <a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#addLocationModel">Add Pickup/Drop Location</a>

                                                <li> <a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#viewLocationModel">View Pickup/Drop Location</a>
                                                 </li>
                                                 <li> <a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#viewRouteModal">View Route Map</a></li> 
                                                </ul> </div></td>
                                            </tr> <tr>
                                                <td>3</td>
                                                <td>ratanada-pavta</td>
                                                <td>going direct ratanada to  pavta</td>
                                                <td>Pickup</td>
                                                 <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                <li> <a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#addLocationModel">Add Pickup/Drop Location</a>

                                                <li> <a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#viewLocationModel">View Pickup/Drop Location</a>
                                                 </li> 
                                                 <li> <a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#viewRouteModal">View Route Map</a></li>
                                                </ul> </div></td> 
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


<div class="modal fade" id="addLocationModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Location </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form class="" action="" method=""  id="form_validation">
            <div class="row clearfix"> 
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <lable class="from_one1">Sequence :</lable>
                    <div class="form-group">
                        <input type="text" name="sequence" class="form-control" placeholder="Sequence">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <lable class="from_one1">Location Lat & Log :</lable>
                    <div class="form-group">
                        <input type="text" name="location_lat" class="form-control" placeholder="Location Latitude ">
                    </div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12">
                    <lable class="from_one1">Location Name :</lable>
                    <div class="form-group">
                        <input type="text" name="location_name" class="form-control" placeholder="Location Name ">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12" style="margin-top: 28px !important;">
                    <div class="form-group">
                         <input type="text" name="location_log" class="form-control" placeholder="Location Longitude ">
                    </div>
                </div>
                
                
                <div class="col-lg-6 col-md-3 col-sm-12">
                    <lable class="from_one1">ETA Pickup :</lable>
                    <div class="form-group">
                        <input type="text" name="eta_pickup" class="form-control" placeholder="ETA Pickup ">
                    </div>
                </div>
                 <div class="col-lg-6 col-md-3 col-sm-12 ">
                    <lable class="from_one1">ETA Drop :</lable>
                    <div class="form-group">
                        <input type="text" name="eta_drop" class="form-control" placeholder="ETA Drop">
                    </div>
                </div>
                
            </div>
          
            <div class="row clearfix">                            
                <div class="col-sm-12">
                    <hr />
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-raised  btn-primary">Save</button>
                     <button type="submit" class="btn btn-raised  btn-primary waves-effect" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
      </div>
      </div>
    </div>
    </div>

    <div class="modal fade" id="viewLocationModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">View Locations </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" action="" method=""  id="contact_form">
            <div class="row clearfix">
                 <div class="col-lg-6 col-md-6 col-sm-12">
                    <lable class="from_one1">Location Name :</lable>
                    <div class="form-group">                       
                        <select class="form-control show-tick select_form1" name="name">
                            <option value="">Select Location </option>
                            <option value="1">Location-1</option>
                            <option value="2">Location-2</option>
                            <option value="3">Location-3</option>
                            <option value="4">Location-4</option>
                            <option value="5">Location-5</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-1"></div>
                  <div class="col-lg-2">
                      <button type="submit" class="btn btn-raised btn-primary" style = "margin-top: 29px !important;"
                      title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-2">
                      <button type="reset" class="btn btn-raised btn-primary" style= "margin-top: 29px !important;" title="Clear">Clear
                      </button>
                    </div>
            </div>
        </form>
     <table class="table m-b-0 c_list">
            <thead>
              <tr>
                <th>#</th>
                <th class="text-center">Location Name</th>
                <th class="text-center">Location Lat & Log</th>
                <th class="text-center">ETA Pickup</th>
                <th class="text-center">ETA Drop</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $counter = 1; for ($i=0; $i < 8; $i++) {  ?>
              <tr>
                <td><?php echo $counter; ?></td>
                <td class="text-center">Ratanada</td>
                <td class="text-center">26.90719 - 72.83413</td>
               <td class="text-center">15 min</td>
               <td class="text-center">20 min</td>
                <td class="text-center"><div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit" ></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete" ></i></a></div></td>
              </tr>
              <?php $counter++; } ?>
            </tbody>
          </table>
      </div>
      </div>
    </div>
    </div>

<!-- model data -->

<div class="modal fade" id="viewRouteModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="header header_model_2">
                <div class="header_model_1">
                    <h6><strong>Route on Map</strong></h6>
                </div>
                <ul class="header-dropdown">
                    <li class="remove remove_cross_x_2">
                        <a role="button" class="btn button_1" data-dismiss="modal"><i class="zmdi zmdi-close zmdi_close1"></i></a>
                    </li>
                </ul>
            </div>
            <div class="tab-pane active" id="classlist">
                <div class="card">
                    <div class="body">
                        <div class="">
                           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d228974.9965040397!2d72.89045762709743!3d26.270284781485415!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39418c4eaa06ccb9%3A0x8114ea5b0ae1abb8!2sJodhpur%2C+Rajasthan!5e0!3m2!1sen!2sin!4v1531227746026" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default btn-round waves-effect">SAVE CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div> -->

        </div>
    </div>
</div>    
<style type="text/css">

#DataTables_Table_0_filter{
    float: right !important;
}
#DataTables_Table_0_paginate{
    float: right !important;
}
.table tr .v1{
       white-space: normal;
}

</style>

<!-- status fuction -->

<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {

    document.getElementById("myDropdown").classList.toggle("show");
}
function myFunction2() {

    document.getElementById("myDropdown2").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            block_name: {
                validators: {
                        stringLength: {
                        min: 4,
                    },
                        notEmpty: {
                        message: 'Please fill required block name'
                    }
                }
            },
            hostel_name: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required hostel name'
                    }
                }
            },
            floor_no: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required floor no'
                    }
                }
            },
            room_no: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required room no'
                    }
                }
            },
            room_alias: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required room alias'
                    }
                }
            },
            capacity: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required capacity'
                    }
                }
            },
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>
