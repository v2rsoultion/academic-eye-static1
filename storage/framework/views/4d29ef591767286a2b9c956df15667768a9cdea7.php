
<?php $__env->startSection('content'); ?>
<style type="text/css">
 /* td{
    padding: 14px 10px !important;
  }*/
  .nav-tabs>.nav-item>.nav-link {
    width: 100px;
    margin-right: 5px;
        border: 1px solid #ccc !important;
  }
  #tabingclass .nav-tabs>.nav-item>.nav-link {
    border-radius: 4px !important;
    padding: 8px 25px;
  }
   #tabingclass .nav-link:hover {
        background: #6572b8 !important;
    color: #fff !important;
}
#tabingclass .nav-link:active {
        background: #6572b8 !important;
    color: #fff !important;
}
.idi .nav-tabs>.nav-item>.nav-link.active {
  background: #6572b8 !important;
    color: #fff !important;
}
.modal-dialog {
  max-width: 550px !important;
}
.checkbox {
  float: left;
  margin-right: 20px;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Map Exam to class subjects</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/examination'); ?>">Examination And Report Cards</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/examination'); ?>">Map Exam to Class Subjects</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/examination/map-exam-to-class-subjects/add'); ?>">Add</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 20px !important;">
                 
                     <form class="" action="" method=""  id="" style="width: 100%;">
                      <div class="row">
                       <div class="col-lg-3">
                         <div class="form-group">
                          <!-- <lable class="from_one1">Exam Name</lable> -->
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Exam</option>
                              <option value="1">Exam-1</option>
                              <option value="2">Exam-2</option>
                              <option value="3">Exam-3</option>
                              <option value="4">Exam-4</option>
                            </select>
                         </div>
                      </div>
                     <div class="col-lg-1">
                      <button type="submit" class="btn btn-raised btn-primary " title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Clear
                      </button>
                    </div>
                    </div>
                   </form>
                <hr style="width: 100%">
                    <!--  DataTable for view Records  -->
                     <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                      <thead>
                        <tr>
                          <th>S No</th>
                          <th>Exam-Name</th>
                          <th>Mapped Class</th>
                          <th class="text-center">Action</th>
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 10; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td>Half Yearly</td>
                          <td><a href="<?php echo e(url('admin-panel/examination/map-exam-to-class-subjects/mapped-class')); ?>" class="btn btn-raised btn-primary">Mapped Class</a></td>
                          <td class="text-center"><a href="<?php echo e(url('admin-panel/examination/map-exam-to-class-subjects/add-class')); ?>" class="btn btn-raised btn-primary">Add Classes</a></td>
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>


<!-- Action Model  -->
<div class="modal fade" id="mappedClassModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Mapped Class</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      <div class="body ">
                  <div class="row tabless" >
            
                    <div class="body idi" id="tabingclass"> 
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <?php
                         $counter=1; for ($i=1; $i < 5 ; $i++) {  
                          if ($counter == 1) {
                            $addClass = 'active show';
                          }else{
                            $addClass = '';
                          }
                          ?>
                          <li class="nav-item" onclick="showSubjects(this.value)" id="show_subjects_class"><a class="nav-link <?php echo $addClass?>" data-toggle="tab" href="#class<?php echo $counter;  ?>"> <b>Class<?php echo $counter;?> </b></a></li>
                       <?php $counter++; } ?>
                       <i class="fas fa-minus-circle" style="margin: 20px 20px; color: #6572b8"></i>
                    </ul>

                    <hr>
                    <!-- Tab panes -->
                    <div class="tab-content"  id="show_subjects" style="display: none;">

                       <?php 
                       $counter1 = 1;
                       $counter12 = 1;
                       $counter123 = 1;
                       $counter1234 = 1;
                  
                        for ($i=1; $i < 5 ; $i++) {  
                          if ($counter1 == 1) {
                            $addClass1 = 'active';
                          }else{
                            $addClass1 = '';
                          }
                        ?>
                        <div role="tabpanel" class="tab-pane tabingclasss <?php echo $addClass1;?>"  > 
                          
                               <div class="checkbox">
                                 <input id="checkbox<?php echo $counter1;  ?>" type="checkbox">
                                 <label for="checkbox<?php echo $counter1;  ?>">Subject<?php echo $counter1;  ?></label>
                              </div>
                              <div class="checkbox">
                                 <input id="checkbox1<?php echo $counter12;  ?>" type="checkbox">
                                 <label for="checkbox1<?php echo $counter12;  ?>">Subject<?php echo $counter1;  ?></label>
                              </div>
                              <div class="checkbox">
                                 <input id="checkbox2<?php echo $counter123;  ?>" type="checkbox">
                                 <label for="checkbox2<?php echo $counter123;  ?>">Subject<?php echo $counter1;  ?></label>
                              </div>
                               <div class="checkbox">
                                 <input id="checkbox3<?php echo $counter1234;  ?>" type="checkbox">
                                 <label for="checkbox3<?php echo $counter1234;  ?>">Subject<?php echo $counter1;  ?></label>
                              </div>
                              <div class="clearfix"></div>
                           
                            </div>
                              <?php $counter1++; $counter12++; $counter123++; $counter1234++; } ?>
                              <hr>
                                 <div class="col-lg-1 padding-0 ">
                            <button type="submit" class="btn btn-raised btn-primary float-left" title="Save">Save
                            </button>
                           
                          </div>
                        </div>
                    </div>
                  </div>
              </div>

      </div>
    </div>
  </div>
</div>


<!-- Action Model  -->
<div class="modal fade" id="addClassModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Mapped Classes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <!-- <div class="body "> -->
          <!-- <div class="row tabless" > -->
          <form class="searchpanel11" action="" method=""  id="" style="width: 100%;">
            <div class="row " style="margin:20px 0px; ">
               <div class="col-lg-6 padding-0">
                         <div class="form-group">
                            <lable class="from_one1" for="name">Class</lable>
                            <select class="form-control show-tick select_form1" name="classes" id="">
                               <option value="">Class</option>
                               <option value="1">1st</option>
                               <option value="2">2nd</option>
                               <option value="3">3rd</option>
                               <option value="4">4th</option>
                               <option value="10th"> 10th</option>
                               <option value="11th">11th</option>
                               <option value="12th">12th</option>
                            </select>
                         </div>
                      </div>
            </div>
            <div class="row">
             
            

             <?php 
                $counter1 = 1;
              
                
                 for ($i=1; $i < 5 ; $i++) {  
                   if ($counter1 == 1) {
                     $addClass1 = 'active';
                   }else{
                     $addClass1 = '';
                   }
                 ?>
                  <div class="col-lg-2">
                   <div class="checkbox">
                    <input id="checkbox<?php echo $counter1;  ?>" type="checkbox">
                    <label for="checkbox<?php echo $counter1;  ?>">Subject<?php echo $counter1;  ?></label>
                  </div>
                </div>
              
              <?php $counter1++; } ?>

               <div class="clearfix"></div>
                <div class="col-lg-10 "></div>
                  <div class="col-lg-2 ">
                    <button type="submit" class="btn btn-raised btn-primary float-right searchBtn" title="Save">Save
                    </button>
                  </div>
                  </div>
                </form>
            <!-- </div> -->
      <!-- </div> -->

      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function showSubjects($counter) {
     document.getElementById('show_subjects').style.display = "block";
  }
</script>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>