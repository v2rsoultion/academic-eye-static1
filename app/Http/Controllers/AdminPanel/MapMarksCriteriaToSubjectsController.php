<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MapMarksCriteriaToSubjectsController extends Controller
{
    public function addMarksCriteria() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.map-marks-criteria-to-subjects.map_marks_criteria_to_subjects')->with($data);
    }

     public function viewMarksCriteria() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.map-marks-criteria-to-subjects.view_map_marks_criteria_to_subjects')->with($data);
    }
}
