@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
  {!! Form::hidden('tempid','',['class' => 'gui-input', 'id' => 'tempid', 'readonly' => 'true']) !!}
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Manage Category</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">Inventory</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/inventory/manage-category') !!}">Manage Category</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card" style="margin-bottom: 20px;">
               <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body form-gap1">
                  {!! Form::open(['files'=>TRUE,'id' => 'manage_category' , 'class'=>'form-horizontal','url' =>$save_url, 'submit_button' => $submit_button]) !!}
                    <div class="row" >
                      <div class="col-lg-3 col-md-3">
                        <lable class="from_one1">Category Level</lable>
                        <div class="form-group m-bottom-0">
                          <label class=" field select" style="width: 100%">
                            {!!Form::select('category', $getCategoryData,isset($category->category_level) ? $category->category_level : '', ['class' => 'form-control show-tick select_form1 ','id'=>''])!!}
                            <i class="arrow double"></i>
                          </label>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Category Name</lable>
                          {!! Form::text('category_name',isset($category->category_name) ? $category->category_name : '',['class' => 'form-control','placeholder'=>'Category Name', 'id' => 'category_name']) !!}
                         <!--  <input type="text" name="category_name" id="" class="form-control" placeholder="Category Name"> -->
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-1 ">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
                        </button>
                      </div>
                      <div class="col-lg-1 ">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
                        </button>
                      </div>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
              
              <div class="card">
                <div class="body form-gap1">
                  <div class="headingcommon  col-lg-12" style="margin-left: -13px">Search By :-</div>
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <!-- {!! Form::open(['files'=>TRUE,'id' => '' , 'class'=>'form-horizontal']) !!} -->
                    <div class="row" >
                      <div class="col-lg-3 col-md-3">
                        <!-- <lable class="from_one1">Category Level</lable> -->
                        <div class="form-group m-bottom-0">
                          <label class=" field select" style="width: 100%">
                            {!!Form::select('search_category', $getCategoryData11, '', ['class' => 'form-control show-tick select_form1 ','id'=>'search_category'])!!}
                            <i class="arrow double"></i>
                          </label>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <!-- <lable class="from_one1" for="name">Category Name</lable> -->
                          {!! Form::text('categoryName','',['class' => 'form-control','placeholder'=>'Category Name', 'id' => 'categoryName']) !!}
                         <!--  <input type="text" name="categoryName" id="" class="form-control" placeholder="Category Name"> -->
                        </div>
                      </div>
                      
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary" title="Search" name="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <a class="btn btn-raised btn-primary" style="color:#fff;" href="{{ url('/admin-panel/inventory/manage-category') }}">Clear
                        </a>
                      </div>
                    </div>
                    <!-- {!! Form::close() !!} -->
                  </form>
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="category-table" style="width:100%">
                    {{ csrf_field() }}
                      <thead>
                        <tr>
                          <th>S No</th>
                          <th>Category Level</th>
                          <th>Category</th>
                          <th class="text-center">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
        var table = $('#category-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/inventory/manage-category-view')}}",
              data: function (f) {
                  f.search_category = $('#search_category').val();
                  f.categoryName = $('#categoryName').val();
              }
          },
          columns: [

            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
            {data: 'category_level', name: 'category_level'},
            {data: 'category_name', name: 'category_name'},
            {data: 'action', name: 'action'}
        ] 
        });
    });

  // function getdata() {
  //   var aa = $('$tempid').val(this);
  //   alert(aa);
  // }
</script>
@endsection