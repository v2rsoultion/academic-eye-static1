@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_title') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.book_vendor') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.view_book_vendor') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">{!! trans('language.title') !!}</a></li>
                                    <li class="nav-item"><a class="nav-link"  href="{{ url('admin-panel/title/add-title') }}">{!! trans('language.add_title') !!}</a></li>
                                </ul>                        
                            </div> -->
                            <div class="body">
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="designation-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.book_vendor_name')}}</th>
                                            <th>{{trans('language.book_vendor_no')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#designation-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: "{{url('admin-panel/book-vendor/data')}}",
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'book_vendor_name', name: 'book_vendor_name'},
                {data: 'book_vendor_number', name: 'book_vendor_number'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });


</script>
@endsection




