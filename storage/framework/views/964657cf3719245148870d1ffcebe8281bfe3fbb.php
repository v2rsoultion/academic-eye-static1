<?php if(isset($scholastic['co_scholastic_type_id']) && !empty($scholastic['co_scholastic_type_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?>

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
    
</style>

<?php echo Form::hidden('co_scholastic_type_id',old('co_scholastic_type_id',isset($scholastic['co_scholastic_type_id']) ? $scholastic['co_scholastic_type_id'] : ''),['class' => 'gui-input', 'id' => 'co_scholastic_type_id', 'readonly' => 'true']); ?>

<p class="red">
<?php if($errors->any()): ?>
    <?php echo e($errors->first()); ?>

<?php endif; ?>
</p>
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1"><?php echo trans('language.co_scholastic_type_name'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('co_scholastic_type_name', old('co_scholastic_type_name',isset($scholastic['co_scholastic_type_name']) ? $scholastic['co_scholastic_type_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.co_scholastic_type_name'), 'id' => 'co_scholastic_type_name']); ?>

        </div>
        <?php if($errors->has('co_scholastic_type_name')): ?> <p class="help-block"><?php echo e($errors->first('co_scholastic_type_name')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-8 col-md-8">
        <lable class="from_one1"><?php echo trans('language.co_scholastic_type_description'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::textarea('co_scholastic_type_description', old('co_scholastic_type_description',isset($scholastic['co_scholastic_type_description']) ? $scholastic['co_scholastic_type_description']: ''), ['class' => 'form-control','placeholder'=>trans('language.co_scholastic_type_description'), 'id' => 'co_scholastic_type_description', 'rows'=> 3]); ?>

        </div>
        <?php if($errors->has('co_scholastic_type_description')): ?> <p class="help-block"><?php echo e($errors->first('co_scholastic_type_description')); ?></p> <?php endif; ?>
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']); ?>

        <a href="<?php echo url('admin-panel/dashboard'); ?>" ><?php echo Form::button('Cancel', ['class' => 'btn btn-raised btn-round']); ?></a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#scholastic-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation  rules 
             ------------------------------------------ */

            rules: {
                co_scholastic_type_name: {
                    required: true,
                    lettersonly:true
                },
                co_scholastic_type_description: {
                    required: true,
                }
            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        
        
    });

    

</script>