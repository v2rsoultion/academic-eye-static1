@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  .tabless table tr td {
  padding: 10px 10px !important;
  }
 

</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Pay Extra</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/menu/substitute-management') }}">Substitute Management</a></li>
           <li class="breadcrumb-item"><a href="{{ url('admin-panel/menu/substitute-management') }}">Report</a></li>
           <li class="breadcrumb-item"><a href="{{ url('admin-panel/substitute-management/pay-extra-report') }}">Pay Extra</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <!--  <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
                </div> -->
              <div class="body form-gap" style="padding-top: 20px !important;">
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row">
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Date </lable>
                          <input type="text" name="datetimes" class="form-control" placeholder="Date" />
                        </div>
                      </div>
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Submit">Search
                        </button>
                      </div>
                      <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                  <hr>
                  <!--  DataTable for view Records  -->
                  <table class="table m-b-0 c_list">
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Teacher Name</th>
                        <th>Date/Time</th>
                        <th>Class</th>
                        <th>Pay Extra</th>
                        <!-- <th>Action</th> -->
                      </tr>
                    </thead>
                    <tbody>
                      <?php $cou = 1; for ($i=0; $i < 5; $i++) {  ?> 
                      <tr>
                        <td><?php echo $cou; ?></td>
                        <td> Rahul Kumar</td>
                       <td>10/16 05:00 PM - 10/18 01:00 AM</td>                        
                        <td class="">
                          10th
                        </td>
                        <td>Per Day</td>
                      </tr>
                      <tr>
                        <td><?php echo $cou; ?></td>
                        <td> Rahul Kumar</td>
                       <td>10/16 05:00 PM - 10/18 01:00 AM</td>                        
                        <td class="">
                          10th
                        </td>
                        <td> Per Period</td>
                      </tr>
                      <?php $cou++; } ?> 
                    </tbody>
                  </table>  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
@endsection
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript">
  $(function() {
  $('input[name="datetimes"]').daterangepicker({
   timePicker: true,
   startDate: moment().startOf('hour'),
   endDate: moment().startOf('hour').add(32, 'hour'),
   locale: {
     format: 'M/DD hh:mm A'
   }
  });
  });
</script>