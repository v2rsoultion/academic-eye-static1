<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageFeesController extends Controller
{
    /**
     *  View page for Manage Fees
     *  @Khushbu on 01 Oct 2018   
    **/
    public function index()
    {
        $loginInfo 		    = get_loggedin_user_data();
        
        $data 				= array(
            'page_title'    => trans('language.view_fees_head'),
            'redirect_url'  => url('admin-panel/transport/manage-fees/view-fees-head'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.manage-fees.view-fees-head')->with($data);
    }

    /**
     *  Add page for Manage Fees
     *  @Khushbu on 01 Oct 2018   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        if (!empty($id))
        {
            $page_title             = trans('language.edit_fees_head');
            $save_url               = url('admin-panel/transport/manage-fees/save/' . $decrypted_job_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_fees_head');
            $save_url      = url('admin-panel/transport/manage-fees/save');
            $submit_button = 'Save';
        }

        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/manage-fees/view-fees-head'),
        );
        return view('admin-panel.manage-fees.add-fees-head')->with($data);
    }
}
