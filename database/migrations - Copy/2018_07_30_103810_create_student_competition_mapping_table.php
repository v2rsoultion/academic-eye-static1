<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentCompetitionMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'student_competition_mapping';
    protected $primaryKey = 'student_competition_map_id';
    public function up()
    {
        if (!Schema::hasTable('student_competition_mapping')) { 
            Schema::create('student_competition_mapping', function (Blueprint $table) {
                $table->increments('student_competition_map_id')->unsigned();
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->integer('competition_id')->unsigned()->default(0);
                $table->integer('student_id')->unsigned()->default(0);
                $table->string('position_rank',255)->nullable(0);
                $table->timestamps();
            });
            Schema::table('student_competition_mapping', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('student_competition_mapping', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('student_competition_mapping', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
            Schema::table('student_competition_mapping', function($table) {
                $table->foreign('competition_id')->references('competition_id')->on('competitions')->onDelete('cascade');
            });
            Schema::table('student_competition_mapping', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_competition_mapping');
    }
}
