<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransferStudentController extends Controller
{
    public function transferStudent() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.transfer-student.view-transfer-student')->with($data);
    }
}
