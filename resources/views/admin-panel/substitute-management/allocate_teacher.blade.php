@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  .chechBox .checkbox label{
  line-height: 19px;
  }
  .chechBox label::before, .chechBox label::after{
  width: 19px;
  height: 19px;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-12">
        <h2>Allocate</h2>
      </div>
      <div class="col-lg-9 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/menu/substitute-management') }}">Substitute Management</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/substitute-management/manage-substitute') }}">Manage Substitute</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/substitute-management/substitute-schedule') }}">Substitute Schedule</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/substitute-management/substitute-schedule/allocate-teacher') }}">Allocate</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                <div class="row tabless" >
                  <div class="headingcommon  col-lg-12" style="padding-bottom: 0px !important">Add Allocate :-</div>
                  <form class="" action="" method="" id="searchpanel" style="width: 100%;">
                    <div class="row" >
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-3 chechBox">
                        <br>
                        <div class="checkbox">
                          <input id="checkbox" type="checkbox">
                          <label for="checkbox">Appoint as Class Teacher</label>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Date </lable>
                          <input type="text" name="datetimes" id="datetimes" class="form-control" placeholder="Date" />
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Class</lable>
                          <select class="form-control show-tick select_form1" name="classes" id="">
                            
                            <option value="1">1st</option>
                            <option value="2">2nd</option>
                            <option value="3">3rd</option>
                            <option value="4">4th</option>
                            <option value="10th"> 10th</option>
                            <option value="11th">11th</option>
                            <option value="12th">12th</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Period </lable>
                          <select class="form-control show-tick select_form1" name="classes" id="">
                            
                            <option value="1">1st</option>
                            <option value="2">2nd</option>
                            <option value="3">3rd</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Teachers </lable>
                          <select class="form-control show-tick select_form1" name="classes" id="">
                          
                            <option value="1">Ramesh</option>
                            <option value="2">Suresh</option>
                            <option value="3">Harish</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Pay Extra</lable>
                          <select class="form-control show-tick select_form1" name="classes" id="">
                            <option value="">NA  </option>
                            <option value="1">Per Day</option>
                            <option value="2">Per Period</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="row" >
                      <!-- <div class="col-lg-8"></div> -->
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary" title="Submit">Save
                        </button>
                      </div>
                       <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary " title="Submit">Cancel
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
@endsection


