
<?php $__env->startSection('content'); ?>

<style type="text/css">
  .modal-dialog {
  max-width: 800px;
  margin: 30px auto;
  }
 .card .body .table td, .cshelf1 {
  width: 100px;
 }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Concession</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>">Concession</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/concession/view-concession-details'); ?>">View concession</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                <div class="row tabless">
                  <form class="" action="" method="" id="searchpanel" style="width: 100%;">
                    <div class="row">
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Class</lable>
                          <select class="form-control show-tick select_form1" name="classes" id="">
                            <option value="">Select Class</option>
                            <option value="1">1st</option>
                            <option value="2">2nd</option>
                            <option value="3">3rd</option>
                            <option value="4">4th</option>
                            <option value="10th"> 10th</option>
                            <option value="11th">11th</option>
                            <option value="12th">12th</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Name </lable>
                          <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                        </div>
                      </div>
                      <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Submit">Search
                      </button>
                    </div>
                     <div class="col-lg-1 ">
                      <button type="reset" class="btn btn-raised  btn-primary" style="margin-top: 23px !important;" title="Clear">Clear
                      </button>
                    </div>
                    </div>
                  </form>
                  <!--   <div class="col-lg-2 paddingtopclass"><b>Total Strength : </b> 2598</div>
                    <div class="col-lg-2 paddingtopclass "><b>No. of Applied : </b> 258956</div> -->
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <table class="table m-b-0 c_list">
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Enroll No</th>
                        <th style="width: 135px;">Student Name</th>
                        <th>Father Name</th>
                        <th>Reason </th>
                        <th>Total Fees</th>
                        <th>Amount</th>
                        <th>Payable Fees</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $count= 1; for ($i=0; $i < 15; $i++) {  ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td>ABC20156</td>
                        <td>
                          <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%"> Ankit Dave
                        </td>
                        <td>Akash Dave</td>
                        <td>Staff Child</td>
                        <td>36542</td>
                        <td>2589</td>
                        <td>33953</td>
                        <td class="text-center">
                          <button type="button" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#addConcessionModel" style="background: transparent !important;">
                          <i class="fas fa-plus-circle"></i>
                          </button>
                           <button type="button" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#viewConcessionModel" style="background: transparent !important;">
                           <i class="fas fa-eye"></i>
                          </button>
                          
                        </td>
                      </tr>
                      <?php $count++; } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Action Model  -->
<div class="modal fade" id="addConcessionModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Concession</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row tabless">
        
          <table class="table  m-b-0 c_list">
            <thead>
              <tr>
                <th>S No</th>
                <th>Fees Head</th>
                <th>Amount</th>
                <th>Concession Amount</th>
                <th>Total Paid Amount</th>
              </tr>
            </thead>
            <tbody>
              <?php $count= 1; for ($i=0; $i < 5; $i++) {  ?>
              <tr>
                <td><?php echo $count; ?></td>
                <td>Tuition Fees</td>
                <td>10000</td>
                <td><input type="text" name="name" id="name" class="form-control" placeholder="Amount" value="2000"></td>
                <td>8000</td>
              </tr>
              <?php $count++; } ?>
            </tbody>
          </table>
        </div>
         <div class="col-lg-12">
            <button type="submit" class="btn btn-raised  btn-primary waves-effect float-right" data-dismiss="modal">Cancel</button>  
          <button type="submit" class="btn btn-raised  btn-primary float-right" title="Save" style="margin-right: 10px;">Save
          </button>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- Action Model  -->

<!-- Action Model  -->
<div class="modal fade" id="viewConcessionModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">View Concession</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row clearfix">
        <div class="col-lg-4">
          <div class="form-group">
            <lable class="from_one1" for="name">Fees Head </lable>
            <input type="text" name="name" id="name" class="form-control" placeholder="Name">
          </div>
        </div>
        <div class="col-lg-2 ">
        <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 30px !important;" title="Submit">Search
        </button>
      </div>
       <div class="col-lg-2">
        <button type="reset" class="btn btn-raised  btn-primary" style="margin-top: 30px !important;" title="Clear">Clear
        </button>
      </div>
    </div>
        <div class="row tabless">
        
          <table class="table  m-b-0 c_list">
            <thead>
              <tr>
                <th>#</th>
                <th>Fees Head</th>
                <th>Amount</th>
                <th>Concession Amount</th>
                <th>Total Paid Amount</th>
              </tr>
            </thead>
            <tbody>
              <?php $count= 1; for ($i=0; $i < 5; $i++) {  ?>
              <tr>
                <td><?php echo $count; ?></td>
                <td>Tuition Fees</td>
                <td>10000</td>
                <td>2000</td>
                <td>8000</td>
              </tr>
              <?php $count++; } ?>
            </tbody>
          </table>
        </div>
        
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>