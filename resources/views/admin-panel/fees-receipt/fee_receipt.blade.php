@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  #bodypadd .card .body {
    padding-top: 20px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2>View Receipts</h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
           <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">{!! trans('language.menu_fee_collection') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">Receipts</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection/fees-receipt/view-fees-receipt') !!}">View</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="">
                  <div class="row tabless" >
                     <!--   <div class="headingcommon  col-lg-12">Add Cheque Details :-</div> -->
                    <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                    <div class="col-lg-2">
                       <div class="form-group">
                          <lable class="from_one1" for="name">Enroll No. </lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Enroll no.">
                       </div>
                    </div>
                     <div class="col-lg-2">
                       <div class="form-group">
                          <lable class="from_one1" for="name">Student Name</lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Student name">
                       </div>
                    </div>
                    <div class="col-lg-2">
                       <div class="form-group">
                          <lable class="from_one1" for="name">Father Name</lable>
                          <input type="text" name="fname" id="" class="form-control" placeholder="Father name">
                       </div>
                    </div>
                     <div class="col-lg-2">
                       <div class="form-group">
                          <lable class="from_one1" for="name"> Receipt Number</lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Receipt number">
                       </div>
                    </div>
                    <div class="col-lg-2">
                       <div class="form-group">
                          <lable class="from_one1" for="name"> Receipt Date Range</lable>
                          <input type="text" name="name" id="receipt_date_range" class="form-control" placeholder="Receipt number">
                       </div>
                    </div>
                   <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised  btn-primary" style="margin-top: 23px !important;" title="Search">Search
                      </button>
                    </div>

                     <table class="table m-b-0 c_list">
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Enroll No.</th>
                        <th style="width: 150px">Name</th>
                        <th>Father Name</th>
                        <th>Class/Section</th>
                        <th>Receipt Date</th>
                        <th>Receipt Amount</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php for ($i=1; $i < 15; $i++) {  ?> 
                      <tr>
                       <td><?php echo $i; ?></td>
                        <td>485962</td>
                        <td>
                          <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="25%">Ankit Dave
                        </td>
                        <td>Ashish Kumar</td>
                        <td>10th A </td>
                        <td>20 July 2018</td>
                        <td>Rs: 25658</td>
                        <td  class="text-success">Active</td>
                        <td class="text-center">
                          <div class="dropdown">
                              <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="zmdi zmdi-label"></i>
                                <span class="zmdi zmdi-caret-down"></span>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li>
                                  <a href="#" title="Print">Print</a>
                                </li>
                                <li>
                                  <a href="#" title="Cancel">Cancel</a>
                                </li>
                              </ul>
                            </div>
                        </td>
                      </tr>
                    <?php } ?> 
                    </tbody>
                  </table>
                  </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
@endsection

