@extends('admin-panel.layout.header')
@section('content')

<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_students') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.student') !!}</a></li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.parent_information') !!}</a></li>
                    <li class="breadcrumb-item active">{!! $parent['student_name'] !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="card profile-header">
                    <div class="body text-center">
                        <div class="row clearfix">
                            
                            <div class="col-lg-3 col-md-12">
                                <div class="profile-image"> 
                                    <img src="{!! URL::to('public/admin/assets/images/profile_av.jpg') !!}" alt="{!! $parent['student_name'] !!}">
                                </div> 
                            </div>
                            <div class="col-lg-5 col-md-12">
                                <div>
                                    <hr>
                                    <div class="profile_detail_1">
                                       <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                          <b class="profile_detail_bold_1">{!! trans('language.student_father_name') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_father_name'] !!}</span>
                                       </div>
                                    </div>
                                    <div class="profile_detail_1">
                                       <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                          <b class="profile_detail_bold_1">{!! trans('language.student_father_mobile_number') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_father_mobile_number'] !!}</span>
                                       </div>
                                    </div>
                                    <div class="profile_detail_1">
                                       <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                          <b class="profile_detail_bold_1">{!! trans('language.student_father_email') !!} :</b>
                                          <span  class="profile_detail_span_1">{!! $parent['student_father_email'] !!}</span>
                                       </div>
                                    </div>
                                    <div class="profile_detail_1">
                                       <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                          <b class="profile_detail_bold_1">{!! trans('language.student_father_occupation') !!} :</b>
                                          <span  class="profile_detail_span_1">{!! $parent['student_father_occupation'] !!}</span>
                                       </div>
                                    </div>
                                      
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                 
            </div>                                        
        </div>
    </div>

    <div class="clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="tab-pane active" id="classlist">
                <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="card border_info_tabs_1">
                                <div class="body"> 
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item" style="margin-left: -14px;"><a class="nav-link active2  active" data-toggle="tab" href="#personal_info"> <i class="zmdi zmdi-home"></i> Personal Info </a></li>
                                        <li class="nav-item"><a class="nav-link active2 " data-toggle="tab" href="#guardian_info"><i class="zmdi zmdi-account"></i> Guardian Info </a></li>
                                        <li class="nav-item"><a class="nav-link active2 " data-toggle="tab" href="#child_info"><i class="zmdi zmdi-account"></i> Child Info </a></li>
                                       
                                    </ul>
                                    
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane in active" id="personal_info">
                                            
                                            <div class="parent_father_info_1 clearfix">
                                                <h6>Father Info :</h6>
                                                <div class="profile_detail_1">
                                                    <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_1">{!! trans('language.student_father_name') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_father_name'] !!}</span>
                                                    </div>
                                                </div>
                                                <div class="profile_detail_1">
                                                    <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_1">{!! trans('language.student_father_mobile_number') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_father_mobile_number'] !!}</span>
                                                    </div>
                                                </div>
                                                <div class="profile_detail_1">
                                                    <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_1">{!! trans('language.student_father_email') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_father_email'] !!}</span>
                                                    </div>
                                                </div>
                                                <div class="profile_detail_1">
                                                    <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_1">{!! trans('language.student_father_annual_salary') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_father_annual_salary'] !!}</span>
                                                    </div>
                                                </div>
                                                <div class="profile_detail_1">
                                                    <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_1">{!! trans('language.student_father_occupation') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_father_occupation'] !!}</span>
                                                    </div>
                                                </div>
                                                
                                                <hr> 

                                                <div class="parent_mother_info_1">
                                                    <h6>Mother Info :</h6>
                                                    <div class="profile_detail_1">
                                                        <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                        <b class="profile_detail_bold_1">{!! trans('language.student_mother_name') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_mother_name'] !!}</span>
                                                        </div>
                                                    </div>
                                                    <div class="profile_detail_1">
                                                        <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                        <b class="profile_detail_bold_1">{!! trans('language.student_mother_mobile_number') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_mother_mobile_number'] !!}</span>
                                                        </div>
                                                    </div>
                                                    <div class="profile_detail_1">
                                                        <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                        <b class="profile_detail_bold_1">{!! trans('language.student_mother_email') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_mother_email'] !!}</span>
                                                        </div>
                                                    </div>
                                                    <div class="profile_detail_1">
                                                        <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                        <b class="profile_detail_bold_1">{!! trans('language.student_mother_annual_salary') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_mother_annual_salary'] !!}</span>
                                                        </div>
                                                    </div>
                                                    <div class="profile_detail_1">
                                                        <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                        <b class="profile_detail_bold_1">{!! trans('language.student_mother_tongue') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_mother_tongue'] !!}</span>
                                                        </div>
                                                    </div>
                                                    <div class="profile_detail_1">
                                                        <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                        <b class="profile_detail_bold_1">{!! trans('language.student_mother_occupation') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_mother_occupation'] !!}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />

                                            </div>
                                        </div>

                                        <div role="tabpanel" class="tab-pane" id="guardian_info">
                                            <div class="parent_mother_info_1">
                                                <h6>Guardian Info :</h6>
                                                <div class="profile_detail_1">
                                                    <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_1">{!! trans('language.student_guardian_name') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_guardian_name'] !!}</span>
                                                    </div>
                                                </div>
                                                <div class="profile_detail_1">
                                                    <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_1">{!! trans('language.student_guardian_email') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_guardian_email'] !!}</span>
                                                    </div>
                                                </div>
                                                <div class="profile_detail_1">
                                                    <div class="demo-google-material-icon"> <i class="material-icons personal_info_icon_1 profile_detail_icon_1">beenhere</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_1">{!! trans('language.student_guardian_mobile_number') !!} :</b> <span class="profile_detail_span_1">{!! $parent['student_guardian_mobile_number'] !!}</span>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <hr />
                                        </div>

                                        <div role="tabpanel" class="tab-pane" id="child_info">
                                            <div class="border_info_tabs_1">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                                        <thead>
                                                            <tr>
                                                                <th>S No</th>
                                                                <th>Class - Section</th>
                                                                <th>Enroll No</th>
                                                                <th>Roll No</th>
                                                                <th>Name</th>
                                                                <th>View Profile</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach ($parent['child_info'] as $childKey=>$child_info)
                                                        @php $childKey++ @endphp
                                                            <tr>
                                                                <td>{!! $childKey !!}</td>
                                                                <td>{!! $child_info['student_class_section'] !!}</td>
                                                                <td>{!! $child_info['student_enroll_number'] !!}</td>
                                                                <td>{!! $child_info['student_roll_no'] !!}</td>
                                                                <td>{!! $child_info['student_name'] !!}</td>
                                                                <td><a href="{{ url('/admin-panel/student/student-profile/'.$child_info['encrypted_student_id']) }}" target="_blank"><button class="btn btn-primary custom_btn" type="button" >View Profile</button></td>
                                                            </tr>
                                                        @endforeach
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                          
        </div>
    </div>
    </section>
  
@endsection




