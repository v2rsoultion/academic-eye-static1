@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }
    td{
      padding: 10px 10px !important;
    }
    .modal-dialog {
  max-width: 550px !important;
}
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>View Remark</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff-menu/my-subjects') !!}">My Subjects</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/staff-menu/my-subjects/view-remark') !!}">View Remark</a></li>
                   
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           <div class="body form-gap ">
                               <form>
                                    <div class="row clearfix">
                                      <div class="col-lg-2">
                                      <div class="form-group">
                                        <!-- <lable class="from_one1" for="name">Subject</lable> -->
                                        <select class="form-control show-tick select_form1" name="classes" id="">
                                          <option value="">Subject</option>
                                          <option value="A">Subject-1</option>
                                          <option value="B">Subject-2</option>
                                          <option value="C">Subject-3</option>
                                          <option value="D">Subject-4</option>
                                        </select>
                                      </div>
                                    </div>
                                     
                                      
                                      <div class="col-lg-2">
                                       <div class="form-group">
                                        <input type="text" name="task_date" id="taskDate" placeholder="Date" class="form-control">
                                       </div>
                                    </div>
                                        <div class="col-lg-1 col-md-1">
                                          <button type="submit" class="btn btn-raised btn-primary" title="Search">Search
                                          </button>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <button type="submit" class="btn btn-raised btn-primary" title="Clear">Clear
                                          </button>
                                        </div>
                                    </div>
                                </form>
                                <hr>
                                <div style="width:100%;border: 1px solid #ccc;border-radius: 5px;font-size:13px;padding: 5px 10px;margin-left: 0px;">

                                    <div><b>Class:</b> Class-1 - A</div>
                                   <div><b>Student Name:</b> Ankit Dave</div>
                                
                                  <div><b>Subject Name:</b> Sub-1 - Sub-101</div></div>
                                  <hr>
                                <div class="table-responsive">
                                   
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                            {{ csrf_field() }}
                                <thead>
                                    <tr>
                                        <th>{{trans('language.s_no')}}</th>
                                        <!-- <th>Class-Section</th>
                                        <th>Subject Name</th> -->
                                        <th>Date</th>
                                        <th>Remark</th>
                                        <th>Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                          <td>1</td>
                                         <!--  <td>Class-1 - A</td>
                                          <td>Sub-1 - Sub-101</td> -->
                                          <td>31-10-2018</td>
                                          <td>Average marks in this subjects</td>
                                          <!-- <td><button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#RemarksModel"> View Remark</button></td> -->
                                          <td>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div>
                                          </td>
                                        </tr>
                                          <tr>
                                          <td>2</td>
                                          <!-- <td>Class-2 - A</td>
                                          <td>Sub-2 - Sub-102</td> -->
                                          <td>31-10-2018</td>
                                          <td>Average marks in this subjects</td>
                                          <!-- <td><button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#RemarksModel"> View Remark</button></td> -->
                                          <td>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td>3</td>
                                          <!-- <td>Class-3 - A</td>
                                          <td>Sub-3 - Sub-103</td> -->
                                          <td>09-10-2018</td>
                                          <td>Average marks in this subjects</td>
                                          <!--  <td><button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#RemarksModel"> View Remark</button></td> -->
                                          <td>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td>4</td>
                                          <!-- <td>Class-4 - A</td>
                                          <td>Sub-4 - Sub-104</td> -->
                                          <td>09-10-2018</td>
                                          <td>Average marks in this subjects</td>
                                          <!-- <td><button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#RemarksModel"> View Remark</button></td> -->
                                          <td>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div>
                                          </td>
                                        </tr>
                                       <tr>
                                          <td>5</td>
                                          <!-- <td>Class-5 - A</td>
                                          <td>Sub-5 - Sub-105</td> -->
                                          <td>09-10-2018</td>
                                          <td>Average marks in this subjects</td>
                                          <!--  <td><button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#RemarksModel"> View Remark</button></td> -->
                                          <td>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div>
                                          </td>
                                        </tr>
                                          <tr>
                                          <td>6</td>
                                          <!-- <td>Class-6 - C</td>
                                          <td>Sub-6 - Sub-106</td> -->
                                          <td>09-10-2018</td>
                                          <td>Average marks in this subjects</td>
                                          <!-- <td><button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#RemarksModel"> View Remark</button></td> -->
                                          <td>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div>
                                          </td>
                                        </tr>
                                          <tr>
                                          <td>7</td>
                                          <!-- <td>Class-7 - A</td>
                                          <td>Sub-7 - Sub-107</td> -->
                                          <td>09-10-2018</td>
                                          <td>Average marks in this subjects</td>
                                           <!-- <td><button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#RemarksModel"> View Remark</button></td> -->
                                          <td>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div>
                                          </td>
                                        </tr>
                                          <tr>
                                          <td>8</td>
                                          <!-- <td>Class-8 - A</td>
                                          <td>Sub-8 - Sub-108</td> -->
                                          <td>09-10-2018</td>
                                          <td>Average marks in this subjects</td>
                                         <!-- <td><button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#RemarksModel"> View Remark</button></td> -->
                                          <td>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div>
                                          </td>
                                        </tr>
                                </tbody>
                            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#staff-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/staff/data')}}',
                data: function (d) {
                    d.class_id = $('input[name="name"]').val();
                    d.section_id = $('select[name="designation_id"]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'staff_name', name: 'staff_name'},
                {data: 'staff_designation_name', name: 'staff_designation_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

</script>
<!-- 
<div class="modal fade" id="RemarksModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">View Remark</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div style="width:500px;border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;">
             <div><b>Student Name:</b> Ankit Dave</div>
            
             <div><b>Date:</b> 09-10-2018</div>
             <div><b>Remark:</b><br>Average marks in this subjects</div>
             </div>
      </div>
    </div>
  </div>
</div> -->

@endsection

