<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\Datatables\Datatables;

class JobController extends Controller
{
    /**
     *  View page for job
     *  @Khushbu on 15 Sept 2018
    **/
    public function index()
    {
        $job                      = [];
        $loginInfo 			      = get_loggedin_user_data();
        $arr_medium               = \Config::get('custom.medium_type');
        $job['arr_medium']        = add_blank_option($arr_medium, 'Select Medium');
        $job['job_durations']   = array(
                                    ''  => "Durations",   
                                    '1' => "1 Month",
                                    '2' => "2 Months",
                                    '3' => "3 Months",
        );

        $data 				= array(
            'page_title'    => trans('language.view_job'),
            'redirect_url'  => url('admin-panel/job/view-job'),
            'login_info'    => $loginInfo,
            'job'           => $job,
        );
        return view('admin-panel.recruitment.index')->with($data);
    }

    /**
     *  Add page for job
     *  @Khushbu on 15 Sept 2018   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        $job        = [];
        if (!empty($id))
        {
            $decrypted_job_id = get_decrypted_value($id, true);
            $page_title             = trans('language.edit_job');
            $decrypted_job_id     = get_encrypted_value($shift->shift_id, true);
            $save_url               = url('admin-panel/recruitment/save/' . $decrypted_job_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_job');
            $save_url      = url('admin-panel/recruitment/save');
            $submit_button = 'Save';
        }
        $arr_medium                    = \Config::get('custom.medium_type');
        $job['arr_medium']      = add_blank_option($arr_medium, 'Select Medium');
        $job['job_durations']   = array(
                                    ''  => "Durations",   
                                    '1' => "1 Month",
                                    '2' => "2 Months",
                                    '3' => "3 Months",

        );
        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'job'           => $job,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/recruitment/view-job'),
        );
        return view('admin-panel.recruitment.add')->with($data);
    }
    /**
     *  Get Data for view page(Datatables)
     *  @Khushbu on 15 Sept 2018
    **/
    public function anyData(Request $request)
    {
        $job     = [];

        return Datatables::of($job)       
            ->addColumn('action', function ($job)
            {
                $encrypted_job_id = get_encrypted_value($job->job_id, true);
                if($job->job_status == 0) {
                    $status    = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status    = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                        <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="job-status/'.$status.'/' . $encrypted_job_id . '">'.$statusVal.'</a></div>
                        <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-job/' . $encrypted_job_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                        <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-job/' . $encrypted_job_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    public function viewInterviewScheduleData() {
        $job                      = [];
        $loginInfo                = get_loggedin_user_data();
        $data               = array(
            'page_title'    => trans('language.view_interview_schedule'),
            'redirect_url'  => url('admin-panel/ recruitment/view-interview-schedule'),
            'login_info'    => $loginInfo,
            'job'           => $job,
        );

        return view('admin-panel.recruitment.view-interview-schedule')->with($data);
    }

    public function viewCandidateListData() {
        $job                      = [];
        $loginInfo                = get_loggedin_user_data();
        $data               = array(
            'page_title'    => trans('language.view_candidate_list'),
            'redirect_url'  => url('admin-panel/ recruitment/view-candidate-list'),
            'login_info'    => $loginInfo,
            'job'           => $job,
        );

        return view('admin-panel.recruitment.view-candidate-list')->with($data);

    }

    public function jobWiseData() {
        $job                      = [];
        $loginInfo                = get_loggedin_user_data();
        $arr_medium                    = \Config::get('custom.medium_type');
        $job['arr_medium']      = add_blank_option($arr_medium, 'Select Medium');
        $data               = array(
            'page_title'    => trans('language.job_wise'),
            'redirect_url'  => url('admin-panel/ recruitment/view-report-job-wise'),
            'login_info'    => $loginInfo,
            'job'           => $job,
        );

        return view('admin-panel.recruitment.job-wise')->with($data);

    }

    public function viewCandidateRecord() {
        $job                      = [];
        $loginInfo                = get_loggedin_user_data();
        $data               = array(
            'page_title'    => trans('language.view_candidate'),
            'redirect_url'  => url('admin-panel/ recruitment/view-candidate-record'),
            'login_info'    => $loginInfo,
            'job'           => $job,
        );

        return view('admin-panel.recruitment.view-candidate-record')->with($data);
    }

    public function viewCandidateData() {
        $job                      = [];
        $loginInfo                = get_loggedin_user_data();
        $data               = array(
            'page_title'    => trans('language.view_candidate'),
            'redirect_url'  => url('admin-panel/ recruitment/view-candidate'),
            'login_info'    => $loginInfo,
            'job'           => $job,
        );

        return view('admin-panel.recruitment.view-candidate')->with($data);
    }

    public function viewAppliedCandidate() 
    {
         $loginInfo                = get_loggedin_user_data();
        $data               = array(
            'login_info'    => $loginInfo,
        );

        return view('admin-panel.recruitment.view-applied-candidate')->with($data);
    }
 
}
