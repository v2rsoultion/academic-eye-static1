<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MapFreeStudentController extends Controller
{
    public function viewFreeByRate() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.map-free-student.free_by_rte')->with($data);
    }

    public function viewFreeByManagement() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.map-free-student.free_by_management')->with($data);	
    }
}
