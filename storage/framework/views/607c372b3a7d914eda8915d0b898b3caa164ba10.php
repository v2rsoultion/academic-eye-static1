<?php $__env->startSection('content'); ?>

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h2><?php echo trans('language.view_allocation'); ?></h2>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-12">
                <a href="<?php echo url('admin-panel/class-teacher-allocation/allocate-class'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><?php echo trans('language.menu_academic'); ?></li>
                    <li class="breadcrumb-item"><a href="#"><?php echo trans('language.class_teacher_allocation'); ?></a></li>
                    <li class="breadcrumb-item active"><a href=""><?php echo trans('language.view_allocation'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
               
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                <?php if(session()->has('success')): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?php echo e(session()->get('success')); ?>

                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif; ?>
                                <?php if($errors->any()): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?php echo e($errors->first()); ?>

                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif; ?>
                                <?php echo Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']); ?>

                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                <?php echo Form::select('class_id', $listData['arr_class'],isset($listData['class_id']) ? $listData['class_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'class_id','onChange' => 'getSection(this.value)']); ?>

                                                <i class="arrow double"></i>
                                            </label>
                                            <?php if($errors->has('class_id')): ?> <p class="help-block"><?php echo e($errors->first('class_id')); ?></p> <?php endif; ?>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                <?php echo Form::select('section_id', $listData['arr_section'],isset($listData['section_id']) ? $listData['section_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'section_id','disabled']); ?>

                                                <i class="arrow double"></i>
                                            </label>
                                            <?php if($errors->has('section_id')): ?> <p class="help-block"><?php echo e($errors->first('section_id')); ?></p> <?php endif; ?>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                <?php echo Form::select('teacher_id', $listData['arr_staff'],isset($staff_class_allocation['staff_id']) ? $staff_class_allocation['staff_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'teacher_id']); ?>

                                                <i class="arrow double"></i>
                                            </label>
                                            <?php if($errors->has('teacher_id')): ?> <p class="help-block"><?php echo e($errors->first('teacher_id')); ?></p> <?php endif; ?>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <?php echo Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']); ?>

                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <?php echo Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]); ?>

                                        </div>
                                    </div>
                                <?php echo Form::close(); ?>                                        
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="designation-table" style="width:100%">
                                <?php echo e(csrf_field()); ?>

                                    <thead>
                                        <tr>
                                            <th><?php echo e(trans('language.s_no')); ?></th>
                                            <th><?php echo e(trans('language.allot_class_name')); ?></th>
                                            <th><?php echo e(trans('language.allot_teacher_name')); ?></th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#designation-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            //ajax: '<?php echo e(url('admin-panel/class-teacher-allocation/data')); ?>',
            ajax: {
                url: "<?php echo e(url('admin-panel/class-teacher-allocation/data')); ?>",
                data: function (d) {
                    d.staff_id          = $('select[name="teacher_id"]').val();
                    d.class_id          = $('select[name="class_id"]').val();
                    if($('select[name=section_id]').find(':selected').val() != 'Select section'){
                        d.section_id    = $('select[name=section_id]').find(':selected').val();
                    }else{
                        d.section_id    = '';
                    }
                }
            },
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'class_section', name: 'class_section'},
                {data: 'staff_name', name: 'staff_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "30%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            $("select[name='section_id'").html('');
            $("select[name='section_id'").selectpicker('refresh');
            $("select[name='class_id'").selectpicker('refresh');
            $("select[name='teacher_id'").selectpicker('refresh');
            table.draw();
            e.preventDefault();
        })
    });

    function getSection(class_id)
    {
        if(class_id != ''){
            $('.mycustloading').css('display','block');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/student/get-section-data')); ?>",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html('');
                    $("select[name='section_id'").html(data.options);
                    $("select[name='section_id'").removeAttr("disabled");
                    $("select[name='section_id'").selectpicker('refresh');
                    $('.mycustloading').css('display','none');
                }
            });
        }else{
            $("select[name='section_id'").html('');
            $("select[name='section_id'").selectpicker('refresh');
        }            
    }


</script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>