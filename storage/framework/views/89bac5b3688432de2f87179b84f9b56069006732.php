<?php $__env->startSection('content'); ?>
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2><?php echo trans('language.view_staff_role'); ?></h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12 line">
                <!-- <a href="<?php echo url('admin-panel/staff/staff-role/add-staff-role'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a> -->
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/staff'); ?>"><?php echo trans('language.staff'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/staff'); ?>"><?php echo trans('language.staff_role'); ?></a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/staff/staff-role/view-staff-roles'); ?>"><?php echo trans('language.view_staff_role'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#"><?php echo trans('language.holidays'); ?></a></li>
                                    <li class="nav-item"><a class="nav-link"  href="<?php echo e(url('/admin-panel/holidays/add-holiday')); ?>"><?php echo trans('language.add_holiday'); ?></a></li>
                                </ul>                        
                            </div> -->
                            <div class="body form-gap">
                                <form>
                                    <div class="row clearfix">     
                                      <div class="col-lg-3">
                                       <div class="form-group">
                                          <!-- <lable class="from_one1" for="name">Staff Roles</lable> -->
                                          <input type="text" name="staff_roles" placeholder="Staff Roles" class="form-control">
                                       </div>
                                    </div>
                                    <div class="col-lg-1 ">
                                      <button type="" class="btn btn-raised btn-primary"  title="Search">Search
                                      </button>
                                    </div>
                                     <div class="col-lg-1 ">
                                      <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                                      </button>
                                    </div>
                                    </div>
                               </form>
                               <hr>
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="#" style="width:100%">
                                <?php echo e(csrf_field()); ?>

                                    <thead>
                                        <tr>
                                            <th><?php echo e(trans('language.s_no')); ?></th>
                                            <th>Staff Roles</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>School Principal (Admin) </td>
                                            <td>    
                                              <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Class Teacher </td>
                                            <td>    
                                              <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Subject Teacher</td>
                                            <td>    
                                              <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Marks Manager</td>
                                            <td>    
                                              <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Exam Manager</td>
                                            <td>    
                                              <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Accountant</td>
                                            <td>    
                                              <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Hostel Warden</td>
                                            <td>    
                                              <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>HR Manager</td>
                                            <td>    
                                              <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Finance Head</td>
                                            <td>    
                                              <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Admission Officer / Public Relation Officer</td>
                                            <td>    
                                              <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Guard for Visitor Management</td>
                                            <td>    
                                              <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Librarian </td>
                                            <td>    
                                              <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Website Manager</td>
                                            <td>    
                                              <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                            </td>  
                                        </tr>
                                    </tbody>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#staff-role-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: '<?php echo e(url('admin-panel/staff/staff-role/data')); ?>',
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'staff_role_name', name: 'staff_role_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });


</script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>