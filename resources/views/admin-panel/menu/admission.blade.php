@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2>{!! trans('language.menu_admission') !!}</h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item">{!! trans('language.menu_admission') !!}</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Admission/admission.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Admission</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('/admin-panel/admission-form/add-admission-form') }}" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="{{ url('/admin-panel/admission-form/view-admission-forms') }}" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                        <!-- <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="#" title="">Option 1</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 2</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 3</a>
                              </li>
                            </ul>
                          </li>
                        </ul> -->
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Admission/Brochure.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Brochure</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('/admin-panel/admission-form/add-admission-form') }}" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="{{ url('/admin-panel/admission-form/view-admission-forms') }}" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                     <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Admission/Admission form.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Admission form</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('/admin-panel/admission-form/add-admission-form') }}" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="{{ url('/admin-panel/admission-form/view-admission-forms') }}" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                     <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Admission/Manage Admissions.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Manage Admissions</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('/admin-panel/admission-form/add-admission-form') }}" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="{{ url('/admin-panel/admission-form/view-admission-forms') }}" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <div class="row">

                     <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Admission/Manage Enquires.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Manage Enquires</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('/admin-panel/admission-form/add-admission-form') }}" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="{{ url('/admin-panel/admission-form/view-admission-forms') }}" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Admission/student.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Selected Students</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('/admin-panel/admission-form/add-admission-form') }}" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="{{ url('/admin-panel/admission-form/view-admission-forms') }}" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Admission/file.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Admission Form Templates</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ route('admin-panel/admission-form/view-offline-form',['download'=>'pdf']) }}" class="float-left" title="Add ">
                          <i class="fas fa-eye"></i> Offline Form 
                        </a>
                        <a href="{{ url('/admin-panel/admission-form/view-online-form') }}" class="float-right" title="View ">
                          <i class="fas fa-eye"></i> Online Form 
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </section>
@endsection

