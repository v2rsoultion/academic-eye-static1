<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentHealthInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'student_health_info';
    protected $primaryKey = 'student_health_info_id';
    public function up()
    {
        if (!Schema::hasTable('student_health_info')) { 
            Schema::create('student_health_info', function (Blueprint $table) {
                $table->increments('student_health_info_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->string('student_height',255)->nullable();
                $table->string('student_weight',255)->nullable();
                $table->string('student_blood_group',255)->nullable();
                $table->text('student_vision_left')->nullable();
                $table->text('student_vision_right')->nullable();
                $table->longText('medical_issues')->nullable();
                $table->timestamps();
            });
            Schema::table('student_health_info', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('student_health_info', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('student_health_info', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_health_info');
    }
}
