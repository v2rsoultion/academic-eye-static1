-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2018 at 01:13 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `admin_type` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0: super admin,1: school admin,2: staff admin,3: parent admin,4: student admin',
  `admin_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_profile_img` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_type`, `admin_name`, `email`, `password`, `admin_profile_img`, `remember_token`, `created_at`, `updated_at`) VALUES
(17, 0, 'Super Admin', 'admin@gmail.com', '$2y$10$ivvyJkg.XjVg1IhMrQlezuNIFBgMLh3M0UnpRHetyVuMABHhN.CwK', NULL, 'tGpwGfPC0tnXNWsY3s1T8YztjYG715OOFNe8hjnOIPfSUKkxRB18cyZI0dVG', NULL, NULL),
(18, 1, 'Bal Niketan', 'balniketan@gmail.com', '$2y$10$KeJwEnsklOaF9OukH0BnAeDkqAw2GdXzihXqnx1TGUWvqbmGA5gBu', NULL, 'tOahMNiE3mF7CIFAN0Z1cNjXoRu0fMUyPMLhOsGMAXBPxWZBzixH79fmpnjn', '2018-07-13 05:46:05', '2018-07-17 07:17:42');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `designation_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `designation_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation_description` text COLLATE utf8mb4_unicode_ci,
  `designation_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `document_category`
--

CREATE TABLE `document_category` (
  `document_category_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `document_category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_category_for` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=student,1=staff,2=both',
  `document_category_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `facility_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `facility_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facility_fees_applied` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=not apply,1=apply',
  `facility_fees` decimal(18,2) NOT NULL DEFAULT '0.00',
  `facility_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `holiday_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `session_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `holiday_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `holiday_start_date` date DEFAULT NULL,
  `holiday_end_date` date DEFAULT NULL,
  `holiday_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`holiday_id`, `admin_id`, `update_by`, `school_id`, `session_id`, `holiday_name`, `holiday_start_date`, `holiday_end_date`, `holiday_status`, `created_at`, `updated_at`) VALUES
(1, 18, 18, 1, 4, 'holiday', '2018-07-18', '2018-07-20', 0, '2018-07-17 23:54:16', '2018-07-18 05:38:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_07_13_073741_create_admins_table', 1),
(2, '2018_07_13_074017_create_schools_table', 2),
(3, '2018_07_16_071108_create_sessions_table', 3),
(4, '2018_07_17_131027_create_holidays_table', 4),
(5, '2018_07_18_054732_create_shifts_table', 5),
(6, '2018_07_18_105806_create_facilities_table', 6),
(7, '2018_07_18_110518_create_document_category_table', 7),
(8, '2018_07_18_111015_create_designation_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `school_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school_registration_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_sno_numbers` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'SNO numbers are stored with comma',
  `school_description` text COLLATE utf8mb4_unicode_ci,
  `school_address` text COLLATE utf8mb4_unicode_ci,
  `school_district` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_pincode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_board_of_exams` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_medium` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_class_from` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_class_to` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_total_students` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_total_staff` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_fee_range_from` double(18,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `school_fee_range_to` double(18,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `school_facilities` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Facilities are stored with comma',
  `school_url` text COLLATE utf8mb4_unicode_ci,
  `school_logo` text COLLATE utf8mb4_unicode_ci,
  `school_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school_mobile_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_telephone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_fax_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`school_id`, `admin_id`, `update_by`, `school_name`, `school_registration_no`, `school_sno_numbers`, `school_description`, `school_address`, `school_district`, `school_city`, `school_state`, `school_pincode`, `school_board_of_exams`, `school_medium`, `school_class_from`, `school_class_to`, `school_total_students`, `school_total_staff`, `school_fee_range_from`, `school_fee_range_to`, `school_facilities`, `school_url`, `school_logo`, `school_email`, `school_mobile_number`, `school_telephone`, `school_fax_number`, `school_status`, `created_at`, `updated_at`) VALUES
(1, 18, 18, 'Bal Niketan', '987456', '987,654,321,102', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English.', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'Jodhpur', 'Jodhpur', 'Rajasthan', '342006', 'RBSC', 'Hindi', 1, 12, 0, 0, 10000.00, 100000.00, 'Facility1,Facility2,facility3', 'https://www.balniketan.com', 'profile_av775521531831662.jpg', 'balniketan@gmail.com', '9874563210', '+2912542084', '02931-281416', 1, '2018-07-13 05:46:05', '2018-07-17 07:17:42');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `session_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_start_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_end_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `admin_id`, `update_by`, `school_id`, `session_name`, `session_start_date`, `session_end_date`, `session_status`, `created_at`, `updated_at`) VALUES
(2, 18, 18, 1, '2018-2019', '2018-07-01', '2019-06-30', 1, '2018-07-17 02:32:50', '2018-07-18 04:16:26'),
(4, 18, 18, 1, '2019-2020', '2019-07-01', '2020-06-30', 0, '2018-07-17 02:32:50', '2018-07-18 04:17:51');

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `shift_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `shift_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shift_start_time` date DEFAULT NULL,
  `shift_end_time` date DEFAULT NULL,
  `shift_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `admins_admin_name_unique` (`admin_name`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`designation_id`),
  ADD KEY `designations_admin_id_foreign` (`admin_id`),
  ADD KEY `designations_update_by_foreign` (`update_by`),
  ADD KEY `designations_school_id_foreign` (`school_id`);

--
-- Indexes for table `document_category`
--
ALTER TABLE `document_category`
  ADD PRIMARY KEY (`document_category_id`),
  ADD KEY `document_category_admin_id_foreign` (`admin_id`),
  ADD KEY `document_category_update_by_foreign` (`update_by`),
  ADD KEY `document_category_school_id_foreign` (`school_id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`facility_id`),
  ADD KEY `facilities_admin_id_foreign` (`admin_id`),
  ADD KEY `facilities_update_by_foreign` (`update_by`),
  ADD KEY `facilities_school_id_foreign` (`school_id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`holiday_id`),
  ADD KEY `holidays_admin_id_foreign` (`admin_id`),
  ADD KEY `holidays_update_by_foreign` (`update_by`),
  ADD KEY `holidays_session_id_foreign` (`session_id`),
  ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`school_id`),
  ADD KEY `schools_admin_id_foreign` (`admin_id`),
  ADD KEY `schools_update_by_foreign` (`update_by`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `admin_id` (`admin_id`),
  ADD KEY `update_by` (`update_by`),
  ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`shift_id`),
  ADD KEY `shifts_admin_id_foreign` (`admin_id`),
  ADD KEY `shifts_update_by_foreign` (`update_by`),
  ADD KEY `shifts_school_id_foreign` (`school_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `designation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `document_category`
--
ALTER TABLE `document_category`
  MODIFY `document_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `facility_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `holiday_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `school_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `session_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `shift_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `designations`
--
ALTER TABLE `designations`
  ADD CONSTRAINT `designations_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designations_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designations_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `document_category`
--
ALTER TABLE `document_category`
  ADD CONSTRAINT `document_category_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `document_category_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `document_category_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `facilities`
--
ALTER TABLE `facilities`
  ADD CONSTRAINT `facilities_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `facilities_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `facilities_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `holidays`
--
ALTER TABLE `holidays`
  ADD CONSTRAINT `holidays_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `holidays_ibfk_1` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`),
  ADD CONSTRAINT `holidays_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `holidays_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `schools`
--
ALTER TABLE `schools`
  ADD CONSTRAINT `schools_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `schools_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `sessions_ibfk_2` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `sessions_ibfk_3` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`);

--
-- Constraints for table `shifts`
--
ALTER TABLE `shifts`
  ADD CONSTRAINT `shifts_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `shifts_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `shifts_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
