<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParentMenuController extends Controller
{
    public function myProfile() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.parent-menu.parent-profile')->with($data);
    }

    public function studentDetails() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.parent-menu.my-class')->with($data);
    }

    public function viewAttendance() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.parent-menu.view-attendance')->with($data);
    }

     public function addLeaveApplication() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.parent-menu.add-leave-application')->with($data);
    }

    public function viewLeaveApplication() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.parent-menu.view-leave-application')->with($data);
    }

    public function viewRemarks() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.parent-menu.view-remark')->with($data);
    }

    public function viewHomeWork() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.parent-menu.view-homework')->with($data);
    }

    public function viewTimeTable() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.parent-menu.view-time-table')->with($data);
    }

    public function viewVehicleTracks() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.parent-menu.view-track-vehicle')->with($data);
    }

    public function viewOnlineContent() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.parent-menu.view-notes')->with($data);
    }
}
