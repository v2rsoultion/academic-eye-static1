<?php

namespace App\Model\School;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table      = 'school';
    protected $primaryKey = 'school_id';

    public function last()
    {
        return $this->hasOne('App\Model\School\School','school_id');
    }
}
