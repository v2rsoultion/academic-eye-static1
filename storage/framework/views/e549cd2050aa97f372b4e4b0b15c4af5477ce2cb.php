﻿
<?php $__env->startSection('content'); ?>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2> View Student Attendance</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">              
                <ul class="breadcrumb float-md-right">
                   <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Transport</a></li>
                    
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/transport/vehicle-documents/view-vehicle-documents'); ?>">View Student Attendance</a></li>
                </ul>                
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body form-gap">
                        <form class="" action="" method="post"  id="contact_form">
                            <div class="row clearfix">
                                 <div class="col-lg-3 col-md-3 col-sm-12">
                                    <!-- <lable class="from_one1">Vehicle :</lable> -->
                                    <div class="form-group">                       
                                        <select class="form-control show-tick select_form1" name="name">
                                            <option value="">Select Vehicle </option>
                                            <option value="1">Vehicle-1</option>
                                            <option value="2">Vehicle-2</option>
                                            <option value="3">Vehicle-3</option>
                                            <option value="4">Vehicle-4</option>
                                            <option value="5">Vehicle-5</option>
                                        </select>
                                    </div>
                                </div>
                          
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <!-- <lable class="from_one1"> Date :</lable> -->
                                    <div class="form-group">
                                        <input type="text" name="expiry_date" class="form-control" id="expiry_date" placeholder="Date">
                                    </div>
                                </div>
                            
                                <div class="col-lg-1 col-md-2 col-sm-12">
                                    <button type="submit" class="btn btn-raised btn-primary ">Search</button>
                                </div>
                                <div class="col-lg-1 col-md-2 col-sm-12">
                                    <button type="submit" class="btn btn-raised btn-primary ">Clear</button>
                                </div>
                            </div>
                        </form>
                   <hr>
            <div class="table-responsive">
                 <table class="table m-b-0 c_list">
                    <thead>
                      <tr>
                      
                        <th>Enroll No.</th>
                        <th>Name</th>
                        <th>Class/Section</th>
                        <th>Father Name</th>
                        <th>In Attendance</th>
                        <th>Out Attendance</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php for ($i=0; $i < 15; $i++) {  ?> 
                      <tr>
                       
                        <td>485962</td>
                        <td>
                          <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">Ankit Dave
                        </td>
                         <td>10th A </td>
                        <td>Ashish Kumar</td>
                        <td> <div class="staff_attendence2"> <span>Present</span> </div></td>
                        <td> <div class="staff_attendence1"> <span>Absent</span> </div></td>
                       
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="
                            <?php echo e(url('admin-panel/transport/student-attendance/student-attendance')); ?>"><i class="zmdi zmdi-edit"></i></a></button>
                         <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                    <?php } ?> 
                    </tbody>
                  </table>
                </div>
                  </div>
                </div>
            </div>
        </div>
    </div>          

</section>
<?php $__env->stopSection(); ?>


<script type="text/javascript">
// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                        stringLength: {
                        min: 4,
                    },
                        notEmpty: {
                        message: 'Please fill required document category'
                    }
                }
            },
            expiry_date: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required expiry date'
                    }
                }
            },
            document_file: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required document file'
                    }
                }
            }
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>