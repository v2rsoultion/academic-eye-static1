<?php

namespace App\Model\IssueBook;

use Illuminate\Database\Eloquent\Model;

class IssueBookHistory extends Model
{
    protected $table      = 'issued_books_history';
    protected $primaryKey = 'history_id';
}
