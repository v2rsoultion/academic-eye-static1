@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>PF/ESI Setting</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/payroll') !!}">Payroll</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-pf-esi-setting') !!}">PF/ESI Setting</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body" style="padding: 0px 20px 20px 20px;">
                <form class="" action="" id="" style="width: 100%;">
                    <div class="row">
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Month/Year</lable>
                          <input type="text" name="" id="date" class="form-control" placeholder="Date">
                        </div>
                      </div>
                    </div>
                    <hr class="row col-lg-12 padding-0" style="margin-bottom: -1px;width: 98.6%;margin-left: 0px;">
                    <div class="row" style="border-left: 1px solid #e6e6e6; margin-left: 0px;">
                      <div class="row col-lg-8" style="border-right: 1px solid #e6e6e6;">
                      <div class="row col-lg-12">
                        <div class="headingcommon  col-lg-12" style="margin-left: -13px">Provident Fund</div>
                      <div class="row col-lg-5">
                        <div class="col-lg-6">
                          <lable class="from_one1">EPF(A) - (%)</lable>
                        </div>
                        <div class="col-lg-6 padding-0">
                          <div class="form-group">
                            <input type="text" name="january" id="" class="form-control" placeholder="Amount">
                          </div>
                        </div>  
                      </div>
                      <div class="row col-lg-7">
                        <div class="col-lg-6 ">
                          <lable class="from_one1">Cut off</lable>
                        </div>
                        <div class="col-lg-6 padding-0">
                          <div class="form-group">
                            <input type="text" name="january" id="" class="form-control" placeholder="Amount" value="15,000.00">
                          </div>
                        </div>  
                      </div>
                      </div>
                      <div class="row col-lg-12">
                        
                      <div class="row col-lg-5">
                        <div class="col-lg-2">
                          <div class="checkbox">
                            <input id="checkbox" type="checkbox" onclick="chkFunction()">
                            <label class="from_one1" for="checkbox" style= "margin-bottom: 4px !important;"></label>
                          </div>
                        </div>
                        <div class="col-lg-9 padding-0">
                          <lable class="from_one1">Apply Special Cut off</lable>
                        </div>  
                      </div>
                      <div class="row col-lg-7" id="specialCutOff" style="display: none;">
                        <div class="col-lg-6">
                          <lable class="from_one1">Special Cut off</lable></div>
                          <div class="col-lg-6">
                          <div class="form-group">
                            <input type="text" name="january" id="" class="form-control" placeholder="Amount" value="15,000.00">
                          </div> 
                          </div> 
                      </div>
                    </div>
                      <hr class="col-lg-12" style="padding-right: 0px; margin-bottom: -1px;">
                        <div class="row col-lg-6" style="border-right: 1px solid #e6e6e6;">
                        <div class=" col-lg-12">
                           <div style="font-weight: bold; font-size: 14px; margin: 10px 0px;">Employer's Share (%)</div>
                        </div>
                        <div class="row col-lg-12">
                          <div class="col-lg-7">
                            <lable class="from_one1">Pension Fund(B)</lable>
                          </div>
                          <div class="col-lg-5 padding-0">
                            <div class="form-group">
                              <input type="text" name="" class="form-control" value="8.33">
                            </div>
                          </div> 
                        </div>
                         <div class="row col-lg-12">
                          <div class="col-lg-7">
                            <lable class="from_one1">EPF(A-B)</lable>
                          </div>
                          <div class="col-lg-5 padding-0">
                            <div class="form-group">
                              <input type="text" name="" class="form-control" value="3.67">
                            </div>
                          </div> 
                        </div>

                      </div>
                      <div class="col-lg-6" style="margin-top: 10px;">
                        <div class="row col-lg-12">
                          <div class="col-lg-6">
                            <lable class="from_one1">Acc. No. 02 - (%)</lable>
                          </div>
                          <div class="col-lg-6 padding-0">
                            <div class="form-group">
                              <input type="text" name="" class="form-control" value="0.85000">
                            </div>
                          </div> 
                        </div>
                        <div class="row col-lg-12">
                          <div class="col-lg-6">
                            <lable class="from_one1">Acc. No. 21 - (%)</lable>
                          </div>
                          <div class="col-lg-6 padding-0">
                            <div class="form-group">
                              <input type="text" name="" class="form-control" value="0.50000">
                            </div>
                          </div> 
                        </div>
                        <div class="row col-lg-12">
                          <div class="col-lg-6">
                            <lable class="from_one1">Acc. No. 22 - (%)</lable>
                          </div>
                          <div class="col-lg-6 padding-0">
                            <div class="form-group">
                              <input type="text" name="" class="form-control" value="0.01000">
                            </div>
                          </div> 
                        </div>
                      </div>
                      <hr class="col-lg-12" style="padding-right: 0px; margin-top: -1px;margin-bottom: -1px;">
                      <div class="row col-lg-4" style="border-right: 1px solid #e6e6e6;">
                        <div class=" col-lg-12">
                           <div style="font-size: 14px; margin: 10px 0px;">Round Off</div>
                        </div>
                        <div class="row col-lg-12">
                          <div class="col-lg-12">
                            <div class="form-group">
                             <div class="radio" style="margin-top:6px !important;">
                           <input id="radio32" name="inventory_invoice" type="radio" value="1" checked="true">
                            <label for="radio32" class="document_staff">Nearest Rupee</label>
                            </div>
                          </div>
                        </div>
                         <div class="col-lg-12">
                            <div class="form-group">
                             <div class="radio" style="margin-top:6px !important;">
                           <input id="radio33" name="inventory_invoice" type="radio" value="1">
                            <label for="radio33" class="document_staff">Higher Rupee</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- <div class="col-lg-8">
                      <div class="row col-lg-12" style="margin: 20px 0px;">
                        <div class="col-lg-11">
                          <lable class="from_one1">Restrict Employer Share</lable>
                        </div>
                        <div class="col-lg-1 padding-0">
                          <div class="checkbox">
                            <input id="checkbox33" type="checkbox">
                            <label class="from_one1" for="checkbox33" style= "margin-bottom: 4px !important;"></label>
                          </div>
                        </div>
                      </div>
                       <div class="row col-lg-12" style="margin: 20px 0px;">
                        <div class="col-lg-11">
                          <lable class="from_one1">Restrict Employee Share to Employer Share</lable>
                        </div>
                        <div class="col-lg-1 padding-0">
                          <div class="checkbox">
                            <input id="checkbox34" type="checkbox">
                            <label class="from_one1" for="checkbox34" style= "margin-bottom: 4px !important;"></label>
                          </div>
                        </div>
                      </div>
                    </div> -->
                    <!-- <hr class="col-lg-12" style="padding-right: 0px;margin-top: -1px;"> -->

                      </div>

                      <div class=" col-lg-4"  style="border-right: 1px solid #e6e6e6; padding-right: 0px">
                        <div class="headingcommon  col-lg-12" style="margin-left: -13px">Employee State Insurance</div>
                        <div class="row col-lg-12">
                          <div class="col-lg-6">
                            <lable class="from_one1">Employee (%)</lable>
                          </div>
                          <div class="col-lg-6 padding-0">
                            <div class="form-group">
                              <input type="text" name="" class="form-control" value="1.75">
                            </div>
                          </div> 
                        </div>
                        <div class="row col-lg-12">
                          <div class="col-lg-6">
                            <lable class="from_one1">Employer (%)</lable>
                          </div>
                          <div class="col-lg-6 padding-0">
                            <div class="form-group">
                              <input type="text" name="" class="form-control" value="4.75">
                            </div>
                          </div> 
                        </div>
                        <div class="row col-lg-12">
                          <div class="col-lg-6">
                            <lable class="from_one1">Cut Off</lable>
                          </div>
                          <div class="col-lg-6 padding-0">
                            <div class="form-group">
                              <input type="text" name="" class="form-control" value="15,000.00">
                            </div>
                          </div> 
                        </div>
                        <div class="row col-lg-12">
                          <div class="col-lg-8">
                            <lable class="from_one1">Min. limit (Daily wage)</lable>
                          </div>
                          <div class="col-lg-4 padding-0">
                            <div class="form-group">
                              <input type="text" name="" class="form-control" value="">
                            </div>
                          </div> 
                        </div>
                        <hr>
                        <div class="row col-lg-12">
                        <div class=" col-lg-12">
                           <div style="font-size: 14px; margin: 10px 0px;">Round Off</div>
                        </div>
                        <div class="row col-lg-12">
                          <!-- <div class="col-lg-12">
                            <div class="form-group">
                             <div class="radio" style="margin-top:6px !important;">
                           <input id="radio33" name="inventory_invoice" type="radio" value="1">
                            <label for="radio33" class="document_staff" onclick="checkForAllowance('1')">Higher Five Paise</label>
                            </div>
                          </div>
                        </div> -->
                         <div class="col-lg-12">
                            <div class="form-group">
                             <div class="radio" style="margin-top:6px !important;">
                           <input id="radio34" name="roundoff" type="radio" checked="true">
                            <label for="radio34" class="document_staff">Highest Rupee</label>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                             <div class="radio" style="margin-top:6px !important;">
                           <input id="radio35" name="roundoff" type="radio">
                            <label for="radio35" class="document_staff">Nearest Rupee</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                      </div>

                    </div>
                    <hr class="row col-lg-12 padding-0" style="margin-top: -1px; width: 98.6%;margin-left: 0px;">
                    <div class="row">
                      <div class="col-lg-3">
                        <button class="btn btn-raised btn-primary">New</button>
                      </div>
                      <div class="col-lg-3">
                        <button class="btn btn-raised btn-primary">Save</button>
                      </div>
                      <div class="col-lg-3">
                        <button class="btn btn-raised btn-primary">Delete</button>
                      </div>
                      <div class="col-lg-3">
                        <button class="btn btn-raised btn-primary">Close</button>
                      </div>
                    </div>

                      <!-- <hr> -->
                      <br>
                  </form>
                  <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>Month/Year</th>
                        <th>EPF (A) (%)</th>
                        <th>Pension Fund (B)</th>
                        <th>EPF(A-B)</th>
                        <th>PF Cut Off</th>
                        <th>Acc. No. 02</th>
                        <th>Acc. No. 21</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Apr/2018</td>
                        <td>12.00</td>
                        <td>8.33</td>
                        <td>3.67</td>
                        <td>15,000.00</td>
                        <td>0.65</td>
                        <td>0.50</td>
                      </tr>
                      <tr>
                        <td>Apr/2017</td>
                        <td>12.00</td>
                        <td>8.33</td>
                        <td>3.67</td>
                        <td>15,000.00</td>
                        <td>0.65</td>
                        <td>0.50</td>
                      </tr>
                      <tr>
                        <td>Jan/2018</td>
                        <td>12.00</td>
                        <td>8.33</td>
                        <td>3.67</td>
                        <td>15,000.00</td>
                        <td>0.65</td>
                        <td>0.50</td>
                      </tr>
                      <tr>
                        <td>Apr/2016</td>
                        <td>12.00</td>
                        <td>8.33</td>
                        <td>3.67</td>
                        <td>15,000.00</td>
                        <td>0.65</td>
                        <td>0.50</td>
                      </tr>
                    </tbody>
                  </table>
                  </div>

              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  
  function chkFunction() {
  // Get the checkbox
  var checkBox = document.getElementById("checkbox");
  // Get the output text
  var text = document.getElementById("specialCutOff");

  // If the checkbox is checked, display the output text
  if (checkBox.checked == true){
    text.style.display = "block";
  } else {
    text.style.display = "none";
  }
}

</script>

@endsection