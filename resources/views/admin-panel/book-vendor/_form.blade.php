@if(isset($book_vendor['book_vendor_id']) && !empty($book_vendor['book_vendor_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
</style>

{!! Form::hidden('book_vendor_id',old('book_vendor_id',isset($book_vendor['book_vendor_id']) ? $book_vendor['book_vendor_id'] : ''),['class' => 'gui-input', 'id' => 'book_vendor_id', 'readonly' => 'true']) !!}

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.book_vendor_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('book_vendor_name', old('book_vendor_name',isset($book_vendor['book_vendor_name']) ? $book_vendor['book_vendor_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.book_vendor_name'), 'id' => 'book_vendor_name']) !!}
        </div>
        @if ($errors->has('book_vendor_name')) <p class="help-block">{{ $errors->first('book_vendor_name') }}</p> @endif
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.book_vendor_no') !!} :</lable>
        <div class="form-group">
            {!! Form::text('book_vendor_number', old('book_vendor_number',isset($book_vendor['book_vendor_number']) ? $book_vendor['book_vendor_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.book_vendor_no'), 'id' => 'book_vendor_number']) !!}
        </div>
        @if ($errors->has('book_vendor_number')) <p class="help-block">{{ $errors->first('book_vendor_number') }}</p> @endif
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.book_vendor_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('book_vendor_email', old('book_vendor_email',isset($book_vendor['book_vendor_email']) ? $book_vendor['book_vendor_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.book_vendor_email'), 'id' => 'book_vendor_email']) !!}
        </div>
        @if ($errors->has('book_vendor_email')) <p class="help-block">{{ $errors->first('book_vendor_email') }}</p> @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <lable class="from_one1">{!! trans('language.book_vendor_address') !!} :</lable>
        <div class="form-group">
            <!-- <textarea rows="4" name="description" class="form-control no-resize" placeholder="Description"></textarea> -->
            {!! Form::textarea('book_vendor_address',old('book_vendor_address',isset($book_vendor['book_vendor_address']) ? $book_vendor['book_vendor_address']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.book_vendor_address'),'rows' => 3, 'cols' => 50)) !!}
        </div>
        @if ($errors->has('book_vendor_address')) <p class="help-block">{{ $errors->first('book_vendor_address') }}</p> @endif
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#book-vendor-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                book_vendor_name: {
                    required: true,
                },
                book_vendor_number: {
                    required: true,
                },
                book_vendor_email: {
                    required: true,
                },
                book_vendor_address: {
                    required: true,
                }                
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>