@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive{
        overflow-x: visible !important;
    }
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>View Students</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    @if($login_info['admin_type'] == 0 || $login_info['admin_type'] == 1)
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/student') !!}">{!! trans('language.student') !!}</a></li>
                    @endif
                    @if($login_info['admin_type'] == 2)
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff-menu/my-class') !!}">My Class</a></li>
                    @endif
                   
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/student/view-students') !!}">View Students</a> </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <p class="green">
                                        {{ session()->get('success') }}
                                    </p>
                                @endif
                                @if($errors->any())
                                    <p class="red">{{$errors->first()}}</p>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('roll_no', old('roll_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_roll_no'), 'id' => 'roll_no']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('enroll_no', old('enroll_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_enroll_no'), 'id' => 'enroll_no']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('student_name', old('student_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_student_name'), 'id' => 'student_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                {!! Form::text('father_name', old('father_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_father_name'), 'id' => 'father_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        {!! Form::hidden('section_id', old('section_id', $studentListData['section_id']), ['id' => 'section_id']) !!}
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
    
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list " id="student-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.student_roll_no')}}</th>
                                                <th>{{trans('language.student_enroll_number')}}</th>
                                                <th style="width: 200px;">Student Name</th>
                                                <th>{{trans('language.student_father_name')}}</th>
                                                <th>{{trans('language.medium_type')}}</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>101</td>
                                                <td>1001A1</td>
                                                <td width="20px" >
                                                   <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%">
                                                   Ankit Dave
                                                </td>
                                                <td>Father Name</td>
                                                <td>Hindi</td>
                                                 <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="Approved" href="{{url('admin-panel/student/student-profile')}}">View Profile</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>101</td>
                                                <td>1001A1</td>
                                                <td width="20px" >
                                                   <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%">
                                                   Ankit Dave
                                                </td>
                                                <td>Father Name</td>
                                                <td>Hindi</td>
                                                 <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="Approved" href="{{url('admin-panel/student/student-profile')}}">View Profile</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#student-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/student/student-list-data')}}',
                data: function (d) {
                    d.roll_no = $('input[name=roll_no]').val();
                    d.enroll_no = $('input[name=enroll_no]').val();
                    d.student_name = $('input[name=student_name]').val();
                    d.father_name = $('input[name=father_name]').val();
                    d.section_id = $('input[name=section_id]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
           
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'student_roll_no', name: 'student_roll_no'},
                {data: 'student_enroll_number', name: 'student_enroll_number'},
                {data: 'profile', render: getImg},
                {data: 'student_name', name: 'student_name'},
                {data: 'student_father_name', name: 'student_father_name'},
                {data: 'medium_type', name: 'medium_type'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "className": "text-center",
                    "width": "4%"
                },
                {
                    "targets": 1, // your case first column
                    "className": "text-center",
                    "width": "10%"
                },
                {
                    "targets": 6, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })

        
    });

    function getImg(data, type, full, meta) {
        if (data != '') {
            return '<img src="'+data+'" height="50" />';
        } else {
            return 'No Image';
        }
    }
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    
    

</script>
@endsection




