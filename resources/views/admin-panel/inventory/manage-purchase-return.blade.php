@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  .card .body .table td,
.cshelf1 {
    width: 100px;
    height: auto !important;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Manage Purchase Return</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">Inventory</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/inventory/manage-purchase-return') !!}">Manage Purchase Return</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body form-gap1">
               
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Purchase Return :-</div> -->
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Entry No: </lable>
                          <input type="text" name="entry_no" id=" " value="PR-10" class="form-control" placeholder="Entry No" readonly="true">
                        </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Invoice No: </lable>
                          <input type="text" name="invoice_no" onchange="showTable()" value="PI-" class="form-control" placeholder="Entry No">
                        </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Date: </lable>
                          <input type="text" name="date" id="date" class="form-control" placeholder="Date">
                        </div>
                      </div>

                           <!-- <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Payment Mode</lable>
                            <select class="form-control show-tick select_form1" name="payment_mode" id="">
                              <option value="">Select Payment</option>
                              <option value="1">Cash</option>
                               <option value="1">Bank Names</option>
                                <option value="1">Credit</option>
                            </select>
                         </div>
                      </div> -->
                     
                       <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Vendor</lable>
                            <select class="form-control show-tick select_form1" name="vendor" id="">
                              <option value="">Select Vendor</option>
                              <?php $count = 1; for ($i=0; $i < 4 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Vendor <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div>
                      </div>
                    
                     
                    </div>
                    
                  </form>
                  
                  <!--  DataTable for view Records  -->
                 
                    <div class="table-responsive" style="display: none;" id="show_table">
                    <table class="table table-bordered m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Item Names</th>
                        <th>Invoice Quantity</th>
                        <th>Stock Quantity</th>
                        <th>Available</th>
                        <th>Rate(Per item)</th>
                        <th>Return Quantity</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                    <tbody>
            
                      <tr>
                      <td style="width: 1px;">1</td>
                      <td>Pen Black</td>
                      <td>10</td>
                      <td>10</td>
                      <td>20</td>
                      <td>10</td>
                      <td><input type="text" name="rate" id=" " class="form-control" placeholder="" onchange="showAmount()"></td>
                      <td id="amount" class="text-center"> </td>  
                      </tr>
                    </tbody>
                  </table>
              
                </div>
                <br>
              <form id="manage_purchase">
                  <div class="row">
                    <div class="col-lg-10"></div>
                    <div class="col-lg-2" style="font-size: 16px;">
                      <input type="text" name="" class="form-control" id="totalAmount" readonly="true" value="Total:">
                    </div>
                  </div>
                  <div class="row float-right" style="margin: 20px -14px;">
                  <div class="col-lg-2">
                    <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
                    </button>
                  </div>
                </div>
              </form>
                
                     
              </div>
              

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
<!-- Content end here  -->

<script type="text/javascript">
    function showTable() {
       document.getElementById('show_table').style.display = "block";
    }

    function showAmount() {
      $("#amount").html('100');
      $("#totalAmount").val('Total:Total:100');
    }
</script>
@endsection