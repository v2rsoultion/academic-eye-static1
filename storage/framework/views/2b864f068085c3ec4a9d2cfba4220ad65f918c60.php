
<?php if($errors->any()): ?>
<div class="alert alert-danger" role="alert">
    <?php echo e($errors->first()); ?>

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
<!-- Basic Info section -->
<div class="row" >
  <div class="col-lg-3">
    <div class="form-group">
      <lable class="from_one1"><?php echo trans('language.inventory_vendor_name'); ?></lable>
      <?php echo Form::text('vendor_name',isset($vendor->vendor_name) ? $vendor->vendor_name : '',['class' => 'form-control','placeholder' => trans('language.inventory_vendor_name'), 'id' => 'vendor_name']); ?>

    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group">
      <lable class="from_one1"><?php echo trans('language.inventory_form_gstin'); ?></lable>
      <?php echo Form::text('vendor_gstin',isset($vendor->vendor_gstin) ? $vendor->vendor_gstin : '',['class' => 'form-control','placeholder'=>trans('language.inventory_form_gstin'), 'id' => 'gstin']); ?>

    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group">
      <lable class="from_one1"><?php echo trans('language.inventory_form_contact_no'); ?></lable>
      <?php echo Form::text('vendor_contact_no',isset($vendor->vendor_contact_no) ? $vendor->vendor_contact_no : '',['class' => 'form-control','placeholder'=>trans('language.inventory_form_contact_no'), 'id' => 'contact_no', 'maxlength' =>10,'minlength' =>10]); ?>

    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group">
      <lable class="from_one1"><?php echo trans('language.inventory_form_email'); ?></lable>
      <?php echo Form::email('vendor_email',isset($vendor->vendor_email) ? $vendor->vendor_email : '',['class' => 'form-control','placeholder'=>trans('language.inventory_form_email'), 'id' => 'email']); ?>

    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="form-group">
      <lable class="from_one1"><?php echo trans('language.inventory_form_address'); ?></lable>
      <?php echo Form::textarea('vendor_address',isset($vendor->vendor_address) ? $vendor->vendor_address : '',['class' => 'form-control','placeholder'=>trans('language.inventory_form_address'), 'id' => 'address', 'rows' => '2']); ?>

    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-1 ">
    <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
    </button>
  </div>
  <div class="col-lg-1 ">
    <a href="<?php echo e(url('admin-panel/inventory/manage-vendor')); ?>" class="btn btn-raised btn-primary" title="Cancel">Cancel
    </a>
  </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9-_ ]+$/i.test(value);
        }, "Please use only alphanumeric values");

        jQuery.validator.addMethod("emailvalid", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i.test(value);
        }, "Please enter a valid email address"); 
        $("#manage_vendor").validate({

            /* @validation  states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation  rules 
             ------------------------------------------ */

            rules: {
                vendor_name: {
                    required: true
                },
                vendor_gstin: {
                    required: true
                },
                vendor_contact_no: {
                    required: true,
                     digits: true
                },
                vendor_email: {
                    required: true,
                    email: true,
                    emailvalid: true
                },
                vendor_address: {
                    required: true
                }
            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

</script>