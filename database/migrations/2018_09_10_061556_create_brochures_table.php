<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrochuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'brochures';
    protected $primaryKey = 'brochure_id';
    public function up()
    {
        if (!Schema::hasTable('brochures')) {
            Schema::create('brochures', function (Blueprint $table) {
                $table->increments('brochure_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->string('brochure_name',255)->nullable();
                $table->date('brochure_upload_date')->nullable();
                $table->text('brochure_file')->nullable();
                $table->tinyInteger('brochure_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('brochures', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('brochures', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('brochures', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brochures');
    }
}
