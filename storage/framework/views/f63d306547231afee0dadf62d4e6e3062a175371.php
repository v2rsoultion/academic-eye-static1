
<?php $__env->startSection('content'); ?>
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Ledger Vouchers</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/account'); ?>">Accounts</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/account/ledger-vouchers'); ?>">Ledger Vouchers</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
              <div class="card">
                <div class="body form-gap"> 
                  
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row">
                      <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Main Heads</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Head</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Head- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Sub Heads</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Subhead</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Subhead- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                            <lable class="from_one1">Group</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Group</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Group- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                          <lable class="from_one1">Head Name</lable>
                          <input type="text" name="head_name" id="" class="form-control" placeholder="Head Name">
                        </div>
                      </div>
                    
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Main Head</th>
                        <th>Sub Head</th>
                        <th>Groups</th>
                        <th>Head Name</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                       
                      <tr>
                        <td>1</td>
                        <td>Head- 1</td>
                        <td>Subhead- 1</td>
                        <td>Group-1</td>
                        <td>H1</td>
                        <td class="text-center">
                           <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                              <li> <a title="Profit & Loss A/c" href="<?php echo e(url('admin-panel/account/ledger-vouchers/view-ledger')); ?>">View Ledger</a></li>   
                            </ul> </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Head- 2</td>
                        <td>Subhead- 2</td>
                        <td>Group-2</td>
                        <td>H2</td>
                        <td class="text-center">
                          <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                              <li> <a title="Profit & Loss A/c" href="<?php echo e(url('admin-panel/account/ledger-vouchers/view-ledger')); ?>">View Ledger</a></li>
                            </ul> </div>
                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Head- 1</td>
                        <td>Subhead- 3</td>
                        <td>Group-3</td>
                        <td>H3</td>
                        <td class="text-center">
                           <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                              <li> <a title="Profit & Loss A/c" href="<?php echo e(url('admin-panel/account/ledger-vouchers/view-ledger')); ?>">View Ledger</a></li>
                            </ul> </div>
                        </td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>Head- 4</td>
                        <td>Subhead- 4</td>
                        <td>Group-4</td>
                        <td>H4</td>
                        <td class="text-center">
                           <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                              <li> <a title="Profit & Loss A/c" href="<?php echo e(url('admin-panel/account/ledger-vouchers/view-ledger')); ?>">View Ledger</a></li>
                            </ul> </div>
                        </td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>Head- 2</td>
                        <td>Subhead- 3</td>
                        <td>Group-5</td>
                        <td>H5</td>
                        <td class="text-center">
                           <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                              <li> <a title="Profit & Loss A/c" href="<?php echo e(url('admin-panel/account/ledger-vouchers/view-ledger')); ?>">View Ledger</a></li>
                            </ul> </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>