<!DOCTYPE html>
<html>
<head>
	<title>Character Certificate</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">

	<style type="text/css">
	body {
		width: 720px;
		margin: auto;
		margin-top: 20px;
		font-size: 30px;
	}

	.content_size {
		font-size: 17px;
		font-style: italic;
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		border-radius: 10px;
		border: 5px solid #1f2f60;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 20px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 0px;
	}
	.rl {
		padding: 0px;
	}
	table{
		height: 40px;

	}
	</style>

</head>
<body>
	<div class="header">
	
  		<h4 class="align header_gap">APEX SENIOR SECONDARY SCHOOL</h4>
  		<h5 class="align" style="font-size:16px;margin-top:10px;">An English medium Co-education Sr. Secondary School</h5>
		<div class="align" style="margin-top: 15px;margin-bottom: 5px;">
		<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="60px"></div>
		<div class="align" style="width: 720px;margin-bottom:15px; font-size:13px; text-align: center;"><b>SESSION 2018-2019</b></div>
		<div class="align" style="width: 400px;margin:auto; font-size:25px;"><b>CHARACTER CERTIFICATE</b></div>


		<p class="align content_size"></p>
		<table>
			<tr>
				<td style ="width: 260px !important;" class="align_right content_size"> Certify that I have known Mr/Ms &nbsp;</td> 
				<td style="width: 405px" class="text"> Khushbu Vaishnav</td>
				 
			</tr>
		</table>
		<table>
			<tr>
				<td style="width: 179px" class="align_right content_size"> Son/daughter of Shri &nbsp;</td>
				<td style="width:412px;" class=" text"> Nemi Chand Vaishnav</td>
				<td style="width: 92px" class="content_size">, native of </td></tr>
		</table>
		<table>
			<tr>
				<td style="width: 25px;"> </td>
				<td style="width:432px;" class=" text"> Ratanada, Jodhpur</td>
				<td style="width: 92px" class="content_size"> &nbsp; for the last </td>
				<td style="width: 50px;" class="align text"> 2 </td>
				<td style="width: 50px" class="align content_size"> &nbsp;&nbsp; years </td>
			</tr>
		</table>
		<table>
			<tr>
				<td style="width: 25px;"> </td>
				<td style="width: 62px;" class="align text"> 09</td>
				<td style="width: 592px" class="content_size"> &nbsp; months and that to the best of my knowledge and belief he/she bears a reputable </td>
			</tr>
		</table>
		<table>
			<tr>
				<td style="width: 25px;"> </td>
				<td style="width: 592px" class="content_size">character and has no antecedents which render him/her unsuitable for Government </td>
			</tr>
		</table>
		<table>
			<tr>
				<td style="width: 25px;"> </td>
				<td style="width: 592px" class="content_size"> employment. He/she is not related to me.</td>
			</tr>
		</table>
		<br><br>
		<table>
			<tr>
				<td style="width: 145px" class="align_right content_size sign">Date: 24-10-2018</td>
				<td style="width: 420px" class="align content_size sign"> </td>
				<td style="width: 145px" class="align content_size sign">Signature</td>
			</tr>
	
				
		</table>
	</div>
</body>
</html>

<script type="text/javascript">
	window.print();
</script>