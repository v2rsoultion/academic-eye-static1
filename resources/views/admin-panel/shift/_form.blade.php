@if(isset($shift['shift_id']) && !empty($shift['shift_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif
<style type="text/css">
    .theme-blush .btn-primary {
    margin-top: 2px !important;
}
</style>
{!! Form::hidden('shift_id',old('shift_id',isset($shift['shift_id']) ? $shift['shift_id'] : ''),['class' => 'gui-input', 'id' => 'shift_id', 'readonly' => 'true']) !!}

@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.shift_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('shift_name', old('shift_name',isset($shift['shift_name']) ? $shift['shift_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.shift_name'), 'id' => 'shift_name']) !!}
        </div>
        @if ($errors->has('shift_name')) <p class="help-block">{{ $errors->first('shift_name') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.shift_start_time') !!} :</lable>
        <div class="form-group">
            {!! Form::text('shift_start_time', old('shift_start_time',isset($shift['shift_start_time']) ? $shift['shift_start_time']: ''), ['class' => 'form-control','placeholder'=>trans('language.shift_start_time'), 'id' => 'shift_start_time']) !!}
        </div>
        @if ($errors->has('shift_start_time')) <p class="help-block">{{ $errors->first('shift_start_time') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.shift_end_time') !!} :</lable>
        <div class="form-group">
            {!! Form::text('shift_end_time', old('shift_end_time',isset($shift['shift_end_time']) ? $shift['shift_end_time']: ''), ['class' => 'form-control ','placeholder'=>trans('language.shift_end_time'), 'id' => 'shift_end_time']) !!}
        </div>
        @if ($errors->has('shift_end_time')) <p class="help-block">{{ $errors->first('shift_end_time') }}</p> @endif
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

         jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
        }, "Please use only alphanumeric values");
        $("#shift-form").validate({
            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules   ------------------------------------------ */
            rules: {
                shift_name: {
                    required: true,
                    lettersonly:true
                },
                shift_start_time: {
                    required: true
                },
                shift_end_time: {
                    required: true
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });   
        
    });

    

</script>