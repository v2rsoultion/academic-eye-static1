
<?php $__env->startSection('content'); ?>
<style type="text/css">
  
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View One Time </h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <a href="<?php echo url('admin-panel/fees-collection/one-time/add-one-time'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
        <ul class="breadcrumb float-md-right">
            <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>">One Time </a></li>

          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/one-time/view-one-time'); ?>">View One Time</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                  <div class="row tabless" >
                     <form class="" action="" method=""  id="searchpanel" style="width: 100%;">
                      <div class="row">
                       <div class="col-lg-3">
                         <div class="form-group">
                            <lable class="from_one1" for="name">Name</lable>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                         </div>
                      </div>
                      <div class="col-lg-3">
                       <div class="form-group">
                          <lable class="from_one1" for="name">Class</lable>
                          <select class="form-control show-tick select_form1" name="classes" id="">
                             <option value="">Class</option>
                             <option value="1">1st</option>
                             <option value="2">2nd</option>
                             <option value="3">3rd</option>
                             <option value="4">4th</option>
                             <option value="10th"> 10th</option>
                             <option value="11th">11th</option>
                             <option value="12th">12th</option>
                          </select>
                       </div>
                    </div>
                     <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-primary" style = "margin-top: 23px !important;"
                      title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary" style= "margin-top: 23px !important;" title="Clear">Clear
                      </button>
                    </div>
                    </div>
                   </form>
                
                    <!--  DataTable for view Records  -->
                    <table class="table  m-b-0 c_list">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Alias</th>
                          <th>Class</th>
                          <th>Amount</th>
                          <th>Effective Date</th>
                          <th class="text-center">Refundable</th>
                          <th class="text-center">Action</th>
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 15; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td>Ankit Dave</td>
                          <td>26654A55</td>
                          <td>10th</td>
                          <td>Rs: 125<?php echo $counter; ?></td>
                          <td>20 July 2018</td>
                          <td class="text-success text-center">Yes</td>
                          <td class="text-center"><div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="#"><i class="zmdi zmdi-edit"></i></a></div>
                                                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="#"><i class="zmdi zmdi-delete"></i></a></div></td>
                           <!-- <td class="text-danger">No</td> -->
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Action Model  -->

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="row tabless" >
                    <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                    <div class="col-lg-12">
                      <div class="form-group">
                        <lable class="from_one1" for="name">Class</lable>
                        <select class="form-control show-tick select_form1" name="classes" id="" multiple="multiple
                        ">
                          <option value="">Select Class</option>
                          <option value="1">1st</option>
                          <option value="2">2nd</option>
                          <option value="3">3rd</option>
                          <option value="4">4th</option>
                          <option value="10th"> 10th</option>
                          <option value="11th">11th</option>
                          <option value="12th">12th</option>
                        </select>
                      </div>
                    </div>
                  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>