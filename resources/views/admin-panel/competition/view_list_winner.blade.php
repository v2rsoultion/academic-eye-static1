@extends('admin-panel.layout.header')
@section('content')
{!! Html::script('public/admin/assets/js/jquery.modal.min.js') !!}
{!! Html::style('public/admin/assets/css/jquery.modal.min.css') !!}
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.comp_list_of_winner') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">{!! trans('language.view_students') !!}</a></li>
                                    <li class="nav-item"><a class="nav-link"  href="{{ url('/admin-panel/student/add-student') }}">{!! trans('language.add_student') !!}</a></li>
                                </ul>                        
                            </div> -->
                            <div class="body">
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                       {!! Form::hidden('competition_id',$competition_id) !!}
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('class_id', $listData['arr_class'],isset($listData['class_id']) ? $listData['class_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'class_id','onChange' => 'getSection(this.value)'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                            @if($errors->has('class_id')) <p class="help-block">{{ $errors->first('class_id') }}</p> @endif
                                        </div>
                                        {!! Form::hidden('competition_id',$competition_id) !!}
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('section_id', $listData['arr_section'],isset($listData['section_id']) ? $listData['section_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'section_id','disabled'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                            @if($errors->has('section_id')) <p class="help-block">{{ $errors->first('section_id') }}</p> @endif
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="">
                                    <table class="table m-b-0 c_list" id="list-of-winner" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.comp_list_of_winner_rank')}}</th>
                                                <th>{{trans('language.comp_list_of_winner_name')}}</th>
                                                <th>{{trans('language.comp_list_of_winner_class')}}</th>
                                                <!-- <th>{{trans('language.comp_list_of_winner_section')}}</th> -->
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>
<!-- Model code start here -->

<p><a href="#ex2" id="rankpop" rel="modal:open"></a></p>
<!-- Modal HTML embedded directly into document -->
<div id="ex2" class="modal">    
  <p><strong>Set Position</strong></p>
  <div class="container">
    {!! Form::open(['files'=>TRUE,'id' => 'rank-form' , 'class'=>'form-horizontal']) !!}
    <div id='displaymsg'></div>
    <div class="row">
        <div class="col-lg-9 col-md-9">
            <div class="input-group ">
                {!! Form::text('rank','', ['class' => 'form-control form_control1','placeholder'=>trans('language.comp_list_of_winner_rank'), 'id' => 'rank']) !!}
            </div>
        </div>
        {!! Form::hidden('compe_map_id','', ['id' => 'compe_map_id']) !!}
        <div class="col-lg-3 col-md-3">
            {!! Form::button('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'Search','onclick'=>'return submitrankform()']) !!}
        </div>
        
      </div>
      {!! Form::close() !!}      
    </div>
    <a href="#" id="closemypopup" rel="modal:close"></a>
</div>
<script>
    $(document).ready(function () {
        var table = $('#list-of-winner').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/competition/competition-list-of-winner-data')}}',
                data: function (d) {
                    d.section_id        = $('select[name=section_id]').find(':selected').val();
                    d.competition_id    = $('input[name=competition_id]').val();
                    d.class_id          = $('select[name="class_id"]').val();
                    if($('select[name=section_id]').find(':selected').val() != 'Select section'){
                        d.section_id        = $('select[name=section_id]').find(':selected').val();
                    }else{
                        d.section_id        = '';
                    }
                }
            },
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'position_rank', name: 'position_rank'},
                {data: 'student_name', name: 'student_name'},
                // {data: 'section_name', name: 'section_name'},
                {data: 'class_name', name: 'class_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 4, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

        $(document).on("click","#rank_pop_id", function (e) {
            var com_map_id  = $(this).attr("com-map-id");
            var rank        = $(this).attr("rank-id");
            $("#compe_map_id").val(com_map_id);
            $("#rank").val(rank);
            $('#rankpop').trigger('click');
        })

        $(document).on("click","a.close-modal", function (e) {
            window.location.reload(true);
        })

         $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            $("select[name='section_id'").html('');
            $("select[name='section_id'").selectpicker('refresh');
            $("select[name='class_id'").selectpicker('refresh');
            table.draw();
            e.preventDefault();
        })
        
    });

    function getSection(class_id)
    {
        if(class_id != ''){
            $('.mycustloading').css('display','block');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html('');
                    $("select[name='section_id'").html(data.options);
                    $("select[name='section_id'").removeAttr("disabled");
                    $("select[name='section_id'").selectpicker('refresh');
                    $('.mycustloading').css('display','none');
                }
            });
        }else{
            $("select[name='section_id'").html('');
            $("select[name='section_id'").selectpicker('refresh');
        }
    }

    function submitrankform(){

        var rank                = $('#rank').val();
        var competition_map_id  = $('#compe_map_id').val();   
        
        if(rank !=''){
            $('.mycustloading').css('display','block');
            $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/competition/competition-set-position')}}",
            type: 'GET',
            data: {
                'rank'                  : rank,
                'competition_map_id'    : competition_map_id,
                'operation_type'        : 2
            },
            success: function (response){
                if(response.status == 1001){
                    $('#displaymsg').html("<span class='text-success'>"+response.message+"</span>");     
                }else if(response.status == 404){
                    $('#displaymsg').html("<span class='text-danger'>"+response.message+"</span>");     
                }   
                $('.mycustloading').css('display','none');
            }
        });     
        }
    }
   
</script>
@endsection