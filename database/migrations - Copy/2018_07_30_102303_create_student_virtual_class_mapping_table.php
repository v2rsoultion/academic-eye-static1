<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentVirtualClassMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'student_virtual_class_mapping';
    protected $primaryKey = 'student_virtual_class_map_id';
    public function up()
    {
        if (!Schema::hasTable('student_virtual_class_mapping')) { 
            Schema::create('student_virtual_class_mapping', function (Blueprint $table) {
                $table->increments('student_virtual_class_map_id')->unsigned();
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->integer('virtual_class_id')->unsigned()->default(0);
                $table->integer('student_id')->unsigned()->default(0);
                $table->timestamps();
            });

            Schema::table('student_virtual_class_mapping', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('student_virtual_class_mapping', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('student_virtual_class_mapping', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
            Schema::table('student_virtual_class_mapping', function($table) {
                $table->foreign('virtual_class_id')->references('virtual_class_id')->on('virtual_classes')->onDelete('cascade');
            });
            Schema::table('student_virtual_class_mapping', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_virtual_class_mapping');
    }
}
