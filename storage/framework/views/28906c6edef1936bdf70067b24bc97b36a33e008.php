
<?php $__env->startSection('content'); ?>
<style type="text/css">
  td{
    padding: 12px !important;
  }
   .tabing {
    /*width: 100%;*/
    padding: 0px !important;
    border: 1px solid #ccc;
    border-radius: 5px;
    margin-bottom: 20px;
    /*background: #959ecf;*/
   }
   .theme-blush .nav-tabs .nav-link.active{
      color: #fff !important;
    border-radius: 0px !important;
    background: #1f2f60 !important;
    width: 217px;
    text-align: center;
   }
  /* li {
      border-right: 1px solid #fff;
   }*/
</style>
<!-- <style type="text/css">
  /* page layout */
.proposal{width:1024px;margin:0 auto;}
/*.post-content-left {
  float:left;
  width: 250px;
  margin: 0 20px 0 0;
}
.post-content-right {
  float:left;
  width:750px;
}*/

/* ui */
.ui-tabs-vertical { width: 55em; }
  .ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left; width: 12em; }
  .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
  .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
  .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; border-right-width: 1px; }
  .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
</style>
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css"> -->
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Salary Generation</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/payroll'); ?>">Payroll</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/payroll/manage-salary-generation'); ?>">Salary Generation</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body" style="padding: 0px 20px 20px 20px;" id="tabs">
               
                  <div class="headingcommon  col-lg-12" style="margin-left: -13px">Salary Generation :-</div>
                   <ul class="nav nav-tabs tabing">
                       <li class="nav-item" id="nav-item1"><a class="nav-link active" data-toggle="tab" href="#section-1">Attendance Step</a></li>
                       <li class="nav-item" id="nav-item2"><a class="nav-link" data-toggle="tab" href="#section-2">Overtime Calculation Step</a></li>
                       <li class="nav-item" id="nav-item3"><a class="nav-link" data-toggle="tab" href="#section-3">Loan & Advance Step</a></li>
                       <li class="nav-item" id="nav-item4"><a class="nav-link" data-toggle="tab" href="#section-4">Bonus & Arrears Step</a></li>
                       <li class="nav-item" id="nav-item5"><a class="nav-link" data-toggle="tab" href="#section-5">Salary Sheet</a></li>
                    </ul>
                  <!-- <hr> -->
                  
                
                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <div class="clearfix"></div>
                    
                  <div class="tab-content">

                    <div class="tab-pane active" id="section-1">
                      <div class="headingcommon  col-lg-12" style="margin-left: -13px">Attendance Step :-</div>
                      <div class="row">
                        <div class="col-lg-3">
                          <lable class="from_one1">Date</lable>
                          <div class="form-group">
                            <input type="text" name="date" id="date" placeholder="Date" class="form-control">
                          </div>
                        </div>
                        <div class="col-lg-1">
                          <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Search">Search</button>
                        </div>
                        <div class="col-lg-1">
                          <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear"> Clear</button>
                        </div>
                      </div> 
                      <!-- <hr> -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th>S No</th>
                        <th style="width: 200px">Employee Name</th>
                        <th>Total Working Days</th>
                        <th>Paid Leaves</th>
                        <th>Loss of Pay Leaves</th>
                        <th>Salaried Days</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>
                          <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="18%"> Ankit Dave
                        </td>
                        <td>24</td>
                        <td>4</td>
                        <td>2</td>
                        <td>22</td>
                        <td>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <!-- <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button> -->
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="18%"> Ankit Dave
                        </td>
                        <td>28</td>
                        <td>2</td>
                        <td>2</td>
                        <td>28</td>
                        <td>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <!-- <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button> -->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <hr>
                <!-- <div class="row col-lg-12"> -->
                  <div class="row col-lg-1  float-right">
                    <button class="btn btn-raised btn-primary" title="Next" onclick="shownext(2)" >Next</button>
                  </div>
                <!-- </div> -->
              </div>

              <div class="tab-pane fade" id="section-2">
                      <div class="headingcommon  col-lg-12" style="margin-left: -13px">OverTime Calculation Step :-</div>
                      <!-- <div class="row">
                        <div class="col-lg-3">
                          <lable class="from_one1">Date</lable>
                          <div class="form-group">
                            <input type="text" name="date" id="date" placeholder="Date" class="form-control">
                          </div>
                        </div>
                        <div class="col-lg-1">
                          <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Search">Search</button>
                        </div>
                        <div class="col-lg-1">
                          <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear"> Clear</button>
                        </div>
                      </div>  -->
                      <!-- <hr> -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th>S No</th>
                        <th style="width: 200px">Employee Name</th>
                        <th>Overtime Days</th>
                        <th>Overtime Period</th>
                        <th>Amount/Day</th>
                        <th>Amount/Period</th>
                        <th>Overtime Amount</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>
                          <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="18%"> Ankit Dave
                        </td>
                        <td><input type="text" name="overtime_days" class="form-control" value="2"></td>
                        <td><input type="text" name="overtime_days" class="form-control" value="8"></td>
                        <td><input type="text" name="overtime_days" class="form-control" value="1000"></td>
                        <td><input type="text" name="overtime_days" class="form-control" value="300"></td>
                        <td>2400</td>
                        <td>
                          <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li> <a title="View Details" href="<?php echo e(url('admin-panel/payroll')); ?>">View Details</a></li> 
                            </ul> </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="18%"> Ankit Dave
                        </td>
                         <td><input type="text" name="overtime_days" class="form-control" value="2"></td>
                        <td><input type="text" name="overtime_days" class="form-control" value="8"></td>
                        <td><input type="text" name="overtime_days" class="form-control" value="1000"></td>
                        <td><input type="text" name="overtime_days" class="form-control" value="300"></td>
                        <td>2400</td>
                        <td>
                          <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li> <a title="View Details" href="<?php echo e(url('admin-panel/payroll')); ?>">View Details</a></li> 
                            </ul> </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>


                </div>
                  <div class="row col-lg-1  float-right">
                    <button class="btn btn-raised btn-primary" title="Next" onclick="shownext(3)" >Next</button>
                  </div>
              </div>  

              <div class="tab-pane fade" id="section-3">
                      <div class="headingcommon  col-lg-12" style="margin-left: -13px">Loan & Advance Step :-</div>
                      <!-- <div class="row">
                        <div class="col-lg-3">
                          <lable class="from_one1">Date</lable>
                          <div class="form-group">
                            <input type="text" name="date" id="date" placeholder="Date" class="form-control">
                          </div>
                        </div>
                        <div class="col-lg-1">
                          <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Search">Search</button>
                        </div>
                        <div class="col-lg-1">
                          <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear"> Clear</button>
                        </div>
                      </div>  -->
                      <!-- <hr> -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th>S No</th>
                        <th style="width: 200px">Employee Name</th>
                        <th>Type</th>
                        <th>Remaining Amount</th>
                        <th>Suggestive Deduct Amount</th>
                        <th>Deducting Amount</th>
                        <th>Amount After Deduction</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>
                          <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="18%"> Ankit Dave
                        </td>
                        <td>Loan</td>
                        <td>20000</td>
                        <td>1000</td>
                        <td><input type="text" class="form-control" value="870"></td>
                        <td>19130</td>
                        <!-- <td>
                          <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li> <a title="View Details" href="<?php echo e(url('admin-panel/payroll')); ?>">View Details</a></li> 
                            </ul> </div>
                        </td> -->
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="18%"> Ankit Dave
                        </td>
                        <td>Advance</td>
                        <td>20000</td>
                        <td>1000</td>
                        <td><input type="text" class="form-control" value="870"></td>
                        <td>19130</td>
                       
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="row col-lg-1  float-right">
                    <button class="btn btn-raised btn-primary" title="Next" onclick="shownext(4)" >Next</button>
                  </div>
              </div> 

               <div class="tab-pane fade" id="section-4">
                      <div class="headingcommon  col-lg-12" style="margin-left: -13px">Bonus & Arrears Step :-</div>
                      <!-- <div class="row">
                        <div class="col-lg-3">
                          <lable class="from_one1">Date</lable>
                          <div class="form-group">
                            <input type="text" name="date" id="date" placeholder="Date" class="form-control">
                          </div>
                        </div>
                        <div class="col-lg-1">
                          <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Search">Search</button>
                        </div>
                        <div class="col-lg-1">
                          <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear"> Clear</button>
                        </div>
                      </div>  -->
                      <!-- <hr> -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th>S No</th>
                        <th style="width: 200px">Employee Name</th>
                        <th>Total Bonus Allowed</th>
                        <th>Total Arrears Allowed</th>
                        <th>Bonus Amount Paying</th>
                        <th>Arrear Amount Paying</th>
                        <th>Remaining Bonus</th>
                        <th>Remaining Arrear</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>
                          <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="18%"> Ankit Dave
                        </td>
                        <td>1000</td>
                        <td>800</td>
                        <td><input type="text" class="form-control" value="700"></td>
                        <td><input type="text" class="form-control" value="470"></td>
                        <td>300</td>
                        <td>330</td>
                        <!-- <td>
                          <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li> <a title="View Details" href="<?php echo e(url('admin-panel/payroll')); ?>">View Details</a></li> 
                            </ul> </div>
                        </td> -->
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="18%"> Ankit Dave
                        </td>
                        <td>1000</td>
                        <td>800</td>
                        <td><input type="text" class="form-control" value="700"></td>
                        <td><input type="text" class="form-control" value="470"></td>
                        <td>300</td>
                        <td>330</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="row col-lg-1  float-right">
                    <button class="btn btn-raised btn-primary" title="Next" onclick="shownext(5)" >Next</button>
                  </div>
              </div> 

              <div class="tab-pane fade" id="section-5">
                <div class="headingcommon  col-lg-12" style="margin-left: -13px">Salary Sheet :-</div>
                    <div class="headingcommon  col-lg-12" style="text-align: center;">Salary Sheet Report for the Month of May/2016</div>
                    <table class="table-bordered">
                      <tr style="font-weight: bold; text-align: center;">
                        <td rowspan="2">SL No.</td>
                        <td rowspan="2" style="width: 120px">Employee Name<br>Ref. No. <br>Designation</td>
                        <td rowspan="2">Pay Days</td>
                        <td colspan="3">Earnings</td>
                        <td rowspan="2">Total Earnings</td>
                        <td colspan="3">Deductions</td>
                        <td rowspan="2">Total Deductions</td>
                        <td rowspan="2">Net Payable</td>
                        <td rowspan="2">Remarks</td>
                      </tr>
                      <tr style="font-weight: bold; text-align: center;">
                        <td>BASIC<br>CONV<br>washing<br>Arr.Earn</td>
                        <td>DA<br>EDU<br>Bonus.Exgr<br>Reim.LTA</td>
                        <td>HRA<br>SPL<br>Special</td>
                        <td>PF<br>PFVOL<br>Loan<br>SpL. Ded<br>SUP.PFVOL</td>
                        <td>ESI<br>TDS<br>SSS<br>MESS</td>
                        <td>PT<br>Insurance<br>Advance<br>SUP.PF</td>
                      </tr>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>KRISHNA MURTHY R.<br>ABC001<br>Team Leader</td>
                          <td>31.00</td>
                          <td style="text-align: right;">15,000.00<br>800.00</td>
                          <td style="text-align: right;">3,000.00</td>
                          <td style="text-align: right;">7,200.00<br>4000.00</td>
                          <td style="text-align: right;">30,000.00</td>
                          <td style="text-align: right;">2160.00</td>
                          <td style="text-align: right;">210.00<br>41.00</td>
                          <td style="text-align: right;">200.00</td>
                          <td style="text-align: right;">2611.00</td>
                          <td style="text-align: right;">27,389.00</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>LAXMAN REDDY<br>ABC002<br>Managing Director</td>
                          <td>31.00</td>
                          <td style="text-align: right;">50,000.00<br>800.00</td>
                          <td style="text-align: right;">10,000.00</td>
                          <td style="text-align: right;">24,000.00<br>15,200.00</td>
                          <td style="text-align: right;">1,00,000.00</td>
                          <td style="text-align: right;">7200.00</td>
                          <td style="text-align: right;">13,407.00<br>103.00</td>
                          <td style="text-align: right;">200.00</td>
                          <td style="text-align: right;">20,910.00</td>
                          <td style="text-align: right;">79,090.00</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>RAMESH.PC<br>ABC003<br>Operator</td>
                          <td>31.00</td>
                          <td style="text-align: right;">12,500.00<br>800.00</td>
                          <td style="text-align: right;">2,500.00<br>20,000.00</td>
                          <td style="text-align: right;">6,000.00<br>3,200.00<br>15,000.00</td>
                          <td style="text-align: right;">60,000.00</td>
                          <td style="text-align: right;">1,800.00</td>
                          <td style="text-align: right;">123.00</td>
                          <td style="text-align: right;">200.00</td>
                          <td style="text-align: right;">2,123.00</td>
                          <td style="text-align: right;">57,877.00</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>4</td>
                          <td>PRAGATHI.P<br>ABC004<br>Receptionist</td>
                          <td>31.00</td>
                          <td style="text-align: right;">10,000.00<br>800.00</td>
                          <td style="text-align: right;">2,000.00<br>2,500.00</td>
                          <td style="text-align: right;">4,800.00<br>2,400.00</td>
                          <td style="text-align: right;">22,500.00</td>
                          <td style="text-align: right;">1,440.00</td>
                          <td style="text-align: right;">164.00</td>
                          <td style="text-align: right;">200.00</td>
                          <td style="text-align: right;">1,804.00</td>
                          <td style="text-align: right;">20,096.00</td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
              </div>

            </div>  

          </div>
<!-- <style type="text/css">
    body {
  background-color: #333;
  font-family: 'Luckiest Guy', cursive;
  font-size: 35px;
}

path {
  fill: transparent;
}

text {
  fill: #FF9800;
}
  </style>

<svg viewBox="0 0 500 500">
    <path id="curve" d="M73.2,148.6c4-6.1,65.5-96.8,178.6-95.6c111.3,1.2,170.8,90.3,175.1,97" />
    <text width="500">
      <textPath xlink:href="#curve">
        Apex Senior Secondary School
      </textPath>
    </text>
  </svg> -->
 




            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
 <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

 <script type="text/javascript">
   
   function shownext(id){
      if (id==2) {
        $("#section-1").removeClass("active");
        $("#nav-item1 a").removeClass('active');
        $("#section-2").addClass("active").removeClass('fade');
         $("#nav-item2 a").addClass('active');
      }
       if (id==3) {
        $("#section-2").removeClass("active");
        $("#nav-item2 a").removeClass('active');
        $("#section-3").addClass("active").removeClass('fade');
         $("#nav-item3 a").addClass('active');
      }
       if (id==4) {
        $("#section-3").removeClass("active");
        $("#nav-item3 a").removeClass('active');
        $("#section-4").addClass("active").removeClass('fade');
         $("#nav-item4 a").addClass('active');
      }
       if (id==5) {
        $("#section-4").removeClass("active");
        $("#nav-item4 a").removeClass('active');
        $("#section-5").addClass("active").removeClass('fade');
         $("#nav-item5 a").addClass('active');
      }
   }
 </script>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>