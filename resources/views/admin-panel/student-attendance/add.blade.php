@extends('admin-panel.layout.header')
@section('content')
<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Add Student Attendance</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <a href="{!! url('admin-panel/student/student-attendance/view') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-eye"></i> </a>
                <ul class="breadcrumb float-md-right ">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/student') !!}">{!! trans('language.menu_student') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/student') !!}">Attendance</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/student/student-attendance/add') !!}">Add Student Attendance</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <!-- <div class="header"> -->
                        <!-- <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2> -->
                    <!-- </div> -->
                    <div class="body form-gap">
                   <form>
                    @include('admin-panel.student-attendance._form')
                    </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

