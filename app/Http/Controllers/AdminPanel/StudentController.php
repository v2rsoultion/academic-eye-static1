<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Model\Section\Section;
use App\Model\Student\Student;
use App\Model\Student\StudentParent;
use App\Model\Student\StudentAcademic;
use App\Model\Student\StudentHealth;
use App\Model\Student\StudentDocument;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;


class StudentController extends Controller
{
    /**
     *  View page for students
     *  @Shree on 24 July 2018
    **/
   
    public function index()
    {
        $loginInfo                  = get_loggedin_user_data();
        $arr_medium                 = \Config::get('custom.medium_type');
        $arr_class                  = [];
        $arr_section                = [];
        $listData                   = [];
        $listData['arr_medium']     = add_blank_option($arr_medium, 'Select Medium');
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_section']    = add_blank_option($arr_section, 'Select Section');
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/student/view-list'),
            'page_title'    => trans('language.view_student_list'),
            'listData'      => $listData,
        );
        
        return view('admin-panel.student.index')->with($data);
    }

    /**
     *  View page for students
     *  @Shree on 28 July 2018
    **/
    public function view_students($sectionid = NULL)
    {
        // if(!empty($sectionid)) {
            $loginInfo                                  = get_loggedin_user_data();
            $decrypted_section_id                       = get_decrypted_value($sectionid, true);
            $arr_class_section_info                     = get_class_section_info($decrypted_section_id);
            $arr_section                                = get_class_section();
            $studentListData                            = [];
            $studentListData['arr_class_section_info']  = $arr_class_section_info;
            $studentListData['section_id']              = $decrypted_section_id;
            $data = array(
                'login_info'            => $loginInfo,
                'page_title'            => trans('language.view_students'),
                'studentListData'       => $studentListData,
            );
            return view('admin-panel.student.view_students')->with($data);
        // } else {
        //     return redirect('admin-panel/student/view-list'); 
        // }
    }

    /**
     *  View page for students
     *  @Shree on 28 July 2018
    **/
    public function parent_information()
    {
        $loginInfo      = get_loggedin_user_data();
        $data = array(
            'login_info'        => $loginInfo,
            'redirect_url'      => url('admin-panel/student/parent-information'),
            'page_title'        => trans('language.parent_information'),
        );
        return view('admin-panel.student.parent_information')->with($data);
        
    }

    /**
     *  View page for students
     *  @Shree on 28 July 2018
    **/
    public function student_profile($id = NULL)
    {
        // if(!empty($id)) {
            $loginInfo                  = get_loggedin_user_data();
            // $decrypted_student_id       = get_decrypted_value($id, true);
            $student                    = [];
            // $student                    = get_student_signle_data($decrypted_student_id);
            // $student                    = isset($student[0]) ? $student[0] : [];
            $data = array(
                'login_info'            => $loginInfo,
                'page_title'            => trans('language.student_profile'),
                'student'               => $student,
            );
            //p($student_data);
            return view('admin-panel.student.student_profile')->with($data);
        // } else {
        //     return redirect('admin-panel/student/view-list'); 
        // }
    }

     /**
     *  Add page for student
     *  @Shree on 24 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $student        = [];
        $data           = [];
        $arr_city_p     = [];
        $arr_city_t     = [];
        $arr_state_p    = [];
        $arr_state_t    = [];
        $arr_class      = [];
        $arr_stream     = [];
        $class_id = null;
        $admit_class_id = null;
        $loginInfo  = get_loggedin_user_data();
        
        if (!empty($id))
        {
            $decrypted_student_id = get_decrypted_value($id, true);
            $student              = get_student_signle_data($decrypted_student_id);
            $student              = isset($student[0]) ? $student[0] : [];
            $admit_class_id       = $student['admission_class_id'];
            $class_id             = $student['current_class_id'];
            $arr_state_p          = get_all_states($student['student_permanent_county']);
            $arr_city_p           = get_all_city($student['student_permanent_state']);
            $arr_state_t          = get_all_states($student['student_permanent_county']);
            $arr_city_t           = get_all_city($student['student_permanent_state']);
            $arr_class            = get_all_classes($student['medium_type']);
            $arr_stream           = get_all_streams($student['medium_type']);
            
            if (!$student)
            {
                return redirect('admin-panel/add-student')->withError('Student not found!');
            }
            $encrypted_student_id = get_encrypted_value($student['student_id'], true);
            $page_title           = 'Edit Student';
            $save_url             = url('admin-panel/student/save/' . $encrypted_student_id);
            $submit_button        = 'Update';
        }
        else
        {
            
            $page_title    = trans('language.add_student');
            $save_url      = url('admin-panel/student/save');
            $submit_button = 'Save';
        }
        $student['arr_session'] = [];
        $arr_medium             = \Config::get('custom.medium_type');
        $arr_student_type       = \Config::get('custom.student_type');
        $arr_gender             = \Config::get('custom.student_gender');
        $arr_category           = \Config::get('custom.student_category');
        $arr_annual_salary      = \Config::get('custom.annual_salary');

        $arr_country                   = get_all_country();
        
        $arr_caste                     = get_caste();
        $arr_religion                  = get_religion();
        $arr_natioanlity               = get_nationality();
        $admit_arr_section             = get_class_section($admit_class_id);
        $arr_section                   = get_class_section($class_id);
        $arr_session                   = get_session();
        $arr_group                     = get_student_groups();
        $arr_document_category         = get_all_document_category(0);
        
        $student['arr_gender']         = $arr_gender;
        $student['arr_category']       = $arr_category;
        $student['arr_annual_salary']  = $arr_annual_salary;
        $student['arr_student_type']   = $arr_student_type;
        $student['arr_country']        = add_blank_option($arr_country, 'Select Country');
        $student['arr_state_p']        = add_blank_option($arr_state_p, 'Select State');
        $student['arr_city_p']         = add_blank_option($arr_city_p, 'Select City');
        $student['arr_state_t']        = add_blank_option($arr_state_t, 'Select State');
        $student['arr_city_t']         = add_blank_option($arr_city_t, 'Select City');
        $student['arr_medium']         = add_blank_option($arr_medium, 'Select Medium');
        $student['arr_session']        = add_blank_option($arr_session, 'Select session');
        $student['arr_class']          = add_blank_option($arr_class, 'Select class');
        $student['arr_stream']         = add_blank_option($arr_stream, 'Select Stream');
        $student['arr_caste']          = add_blank_option($arr_caste, 'Select caste');
        $student['arr_religion']       = add_blank_option($arr_religion, 'Select religion');
        $student['arr_section']        = add_blank_option($arr_section, 'Select section');
        $student['admit_arr_section']  = add_blank_option($admit_arr_section, 'Select section');
        $student['arr_natioanlity']    = add_blank_option($arr_natioanlity, 'Select nationality');
        $student['arr_group']          = add_blank_option($arr_group, 'Select house/group');
        $student['arr_document_category']   = add_blank_option($arr_document_category, 'Document category');
        // p($student);
        if (!empty($student['student_image']))
        {
            $profile = check_file_exist($student['student_image'], 'student_image');
            if (!empty($profile))
            {
                $student['profile'] = $profile;
            }
        }
        // p($student);
        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'student'       => $student,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/studebt/student-list'),
        );
        return view('admin-panel.student.add')->with($data);
    }

    /**
     *  Save Student Data
     *  @Shree on 27 July 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $student_id           = null;
        $decrypted_student_id = null;
        $loginInfo = get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_student_id   = get_decrypted_value($id, true);
            $student                = Student::find($decrypted_student_id);
            $student_parent         = StudentParent::Find($student->student_parent_id);
            $parentAdmin            = Admin::Find($student_parent->reference_admin_id);
            $studentAdmin           = Admin::Find($student->reference_admin_id);
            $student_academic       = StudentAcademic::where('student_id', $student->student_id)->first();
            $student_health         = StudentHealth::where('student_id', $student->student_id)->first();
            
            if (!$student)
            {
                return redirect('/admin-panel/student/add-student')->withError('Student not found!');
            }
            $success_msg = 'Student updated successfully!';
        }
        else
        {
            $student            = New Student;
            $studentAdmin       = new Admin();
            $student_parent     = new StudentParent();
            $student_academic   = new StudentAcademic();
            $student_health     = new StudentHealth();
            
            $success_msg        = 'Student saved successfully!';
        }
        
        $arr_input_fields = [
            'medium_type'               => 'required',
            'student_enroll_number'     => 'required|unique:students,student_enroll_number,' . $decrypted_student_id . ',student_id',
            'student_name'              => 'required',
            'student_email'             => 'required|unique:students,student_email,' . $decrypted_student_id . ',student_id',
            'student_reg_date'          => 'required',
            'student_dob'               => 'required',
            'student_category'          => 'required',
            'student_type'          => 'required',
            'caste_id'                  => 'required',
            'religion_id'               => 'required',
            'nationality_id'            => 'required',
            'student_permanent_address' => 'required',
            'student_permanent_city'    => 'required',
            'student_permanent_state'   => 'required',
            'student_permanent_county'  => 'required',
            'student_permanent_pincode' => 'required',
            'admission_session_id'      => 'required',
            'admission_class_id'        => 'required',
            'admission_section_id'      => 'required',
            'current_session_id'        => 'required',
            'current_class_id'          => 'required',
            'current_section_id'        => 'required',
            'student_blood_group'       => 'required',
            'medical_issues'            => 'required',
            'student_father_name'       => 'required',
            'student_father_mobile_number' => 'required',
            'student_father_email'         => 'required',
            'student_father_occupation'    => 'required',
            'student_mother_name'          => 'required',
            'student_mother_mobile_number' => 'required',
            'student_privious_school'      => 'required',
            'student_privious_class'       => 'required',
            'student_privious_tc_no'       => 'required',
            'student_privious_tc_date'     => 'required',
            'student_privious_result'      => 'required',
            'student_login_name'           => 'required',
            'student_login_email'          => 'required',
            'student_login_contact_no'     => 'required',
        ];
        $validatior = Validator::make($request->all(), $arr_input_fields);
       
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction(); //Start transaction!

            try
            {
                //inserting data in multiples tables
                $email = Input::get('student_login_email');
                $mobile_no = Input::get('student_login_contact_no');
                $parentAdmin = check_parents_exist($email,$mobile_no);
               
                if(empty($parentAdmin)) {
                    $parentAdmin        = new Admin();
                    $parentAdmin->admin_name = Input::get('student_login_name');
                    $parentAdmin->email = $email;
                    $parentPassword = str_random(8);
                    $parentAdmin->admin_type = 3;
                    $parentAdmin->password = Hash::make($parentPassword);
                    $parentAdmin->save();
                    $message = "";
                    $subject = "Welcome to ".trans('language.project_title');
                    $emailData = [];
                    $emailData['sender'] = trans('language.email_sender');
                    $emailData['email'] = $email;
                    $emailData['password'] = $parentPassword;
                    $emailData['receiver'] = Input::get('student_father_name');
                    Mail::send('mails.login_credentials', $emailData, function ($message) use($emailData,$subject)
                    {
                        $message->to($emailData['email'], $emailData['receiver'])->subject($subject);
                    
                    });
                } else {
                    $student_parent       = StudentParent::Find($parentAdmin->student_parent_id);
                }

                if (!empty($parentAdmin->admin_id))
                {
                    if (empty($id))
                    {
                        $studentAdmin->admin_name = Input::get('student_name');
                        $studentAdmin->email = Input::get('student_email');
                        $studentPassword = str_random(8);
                        $studentAdmin->admin_type = 4;
                        $studentAdmin->password = Hash::make($studentPassword);
                        $studentAdmin->save();
                        $message = "";
                        $subject = "Welcome to ".trans('language.project_title');
                        $emailData = [];
                        $emailData['sender'] = trans('language.email_sender');
                        $emailData['email'] = Input::get('student_email');
                        $emailData['password'] = $studentPassword;
                        $emailData['receiver'] = Input::get('student_name');
                        Mail::send('mails.login_credentials', $emailData, function ($message) use($emailData,$subject)
                        {
                            $message->to($emailData['email'], $emailData['receiver'])->subject($subject);
                        
                        });
                    }
                    // student parent table data
                    $student_parent->admin_id                        = $loginInfo['admin_id'];
                    $student_parent->update_by                       = $loginInfo['admin_id'];
                    $student_parent->reference_admin_id              = $parentAdmin->admin_id;
                    $student_parent->student_login_name              = Input::get('student_login_name');
                    $student_parent->student_login_email             = Input::get('student_login_email');
                    $student_parent->student_login_contact_no        = Input::get('student_login_contact_no');
                    $student_parent->student_father_name             = Input::get('student_father_name');
                    $student_parent->student_father_mobile_number    = Input::get('student_father_mobile_number');
                    $student_parent->student_father_email            = Input::get('student_father_email');
                    $student_parent->student_father_occupation       = Input::get('student_father_occupation');
                    $student_parent->student_mother_name             = Input::get('student_mother_name');
                    $student_parent->student_mother_mobile_number    = Input::get('student_mother_mobile_number');
                    $student_parent->student_mother_email            = Input::get('student_mother_email');
                    $student_parent->student_mother_occupation       = Input::get('student_mother_occupation');
                    $student_parent->student_mother_tongue           = Input::get('student_mother_tongue');
                    $student_parent->student_guardian_name           = Input::get('student_guardian_name');
                    $student_parent->student_guardian_email          = Input::get('student_guardian_email');
                    $student_parent->student_guardian_mobile_number  = Input::get('student_guardian_mobile_number');
                    $student_parent->student_father_annual_salary    = Input::get('student_father_annual_salary');
                    $student_parent->student_mother_annual_salary    = Input::get('student_mother_annual_salary');
                    $student_parent->student_guardian_relation    = Input::get('student_guardian_relation');
                    $student_parent->save();

                    // student table data
                    $student->admin_id                      = $loginInfo['admin_id'];
                    $student->update_by                     = $loginInfo['admin_id'];
                    $student->reference_admin_id            = $studentAdmin->admin_id;
                    $student->student_parent_id             = $student_parent->student_parent_id;
                    $student->student_enroll_number         = Input::get('student_enroll_number');
                    $student->student_type                  = Input::get('student_type');
                    $student->medium_type                   = Input::get('medium_type');
                    $student->student_email                 = Input::get('student_email');
                    $student->student_reg_date              = Input::get('student_reg_date');
                    $student->student_name                  = Input::get('student_name');
                    $student->student_gender                = Input::get('student_gender');
                    $student->student_dob                   = Input::get('student_dob');
                    $student->student_adhar_card_number     = Input::get('student_adhar_card_number');
                    $student->student_category              = Input::get('student_category');
                    $student->caste_id                      = Input::get('caste_id');
                    $student->religion_id                   = Input::get('religion_id');
                    $student->nationality_id                = Input::get('nationality_id');
                    $student->student_sibling_name          = Input::get('student_sibling_name');
                    $student->student_sibling_class_id      = Input::get('student_sibling_class_id');
                    $student->student_temporary_address     = Input::get('student_temporary_address');
                    $student->student_temporary_city        = Input::get('student_temporary_city');
                    $student->student_temporary_state       = Input::get('student_temporary_state');
                    $student->student_temporary_county      = Input::get('student_temporary_county');
                    $student->student_temporary_pincode     = Input::get('student_temporary_pincode');
                    $student->student_permanent_address     = Input::get('student_permanent_address');
                    $student->student_permanent_city        = Input::get('student_permanent_city');
                    $student->student_permanent_state       = Input::get('student_permanent_state');
                    $student->student_permanent_county      = Input::get('student_permanent_county');
                    $student->student_permanent_pincode     = Input::get('student_permanent_pincode');
                    
                    $student->student_privious_school       = Input::get('student_privious_school');
                    $student->student_privious_class        = Input::get('student_privious_class');
                    $student->student_privious_tc_no        = Input::get('student_privious_tc_no');
                    $student->student_privious_tc_date      = Input::get('student_privious_tc_date');
                    $student->student_privious_result       = Input::get('student_privious_result');
                   
                    if ($request->hasFile('student_image'))
                    {
                        if (!empty($id)){
                            $profile = check_file_exist($student->student_image, 'student_image');
                            if (!empty($profile))
                            {
                                unlink($profile);
                            } 
                        }
                        $file                          = $request->file('student_image');
                        $config_upload_path            = \Config::get('custom.student_image');
                        $destinationPath               = public_path() . $config_upload_path['upload_path'];
                        $ext                           = substr($file->getClientOriginalName(),-4);
                        $name                          = substr($file->getClientOriginalName(),0,-4);
                        $filename                      = $name.mt_rand(0,100000).time().$ext;
                        $file->move($destinationPath, $filename);
                        $student->student_image = $filename;
                    }

                    $student->save();
                    // student academic table data
                    $student_academic->admin_id               = $loginInfo['admin_id'];
                    $student_academic->update_by              = $loginInfo['admin_id'];
                    $student_academic->student_id             = $student->student_id;
                    $student_academic->admission_session_id   = Input::get('admission_session_id');
                    $student_academic->admission_class_id     = Input::get('admission_class_id');
                    $student_academic->admission_section_id   = Input::get('admission_section_id');
                    $student_academic->current_session_id     = Input::get('current_session_id');
                    $student_academic->current_class_id       = Input::get('current_class_id');
                    $student_academic->current_section_id     = Input::get('current_section_id');
                    $student_academic->student_unique_id      = Input::get('student_enroll_number').'_'.$student->student_id;
                    $student_academic->group_id               = Input::get('group_id');
                    $student_academic->stream_id              = Input::get('stream_id');
                    $student_academic->save();
                    // student health table data
                    $student_health->admin_id               = $loginInfo['admin_id'];
                    $student_health->update_by              = $loginInfo['admin_id'];
                    $student_health->student_id             = $student->student_id;
                    $student_health->student_height         = Input::get('student_height');
                    $student_health->student_weight         = Input::get('student_weight');
                    $student_health->student_blood_group    = Input::get('student_blood_group');
                    $student_health->student_vision_left    = Input::get('student_vision_left');
                    $student_health->student_vision_right   = Input::get('student_vision_right');
                    $student_health->medical_issues         = Input::get('medical_issues');
                    $student_health->save();
                    $document_list = [];

                    $documentKey = 0;
                    foreach($request->get('documents') as $documents){ 
                        $fileData = $request->file('documents');
                        if(isset($documents['document_category_id']) && !empty($documents['document_category_id'])) {
                            if(isset($documents['student_document_id']) ) {
                                $student_document_update       = StudentDocument::where('student_document_id', $documents['student_document_id'])->first();
                                $student_document_update->admin_id                 = $loginInfo['admin_id'];
                                $student_document_update->update_by                = $loginInfo['admin_id'];
                                $student_document_update->student_id               = $student->student_id;
                                $student_document_update->document_category_id     = $documents['document_category_id'];
                                $student_document_update->student_document_details = $documents['student_document_details'];
                                if (isset($fileData[$documentKey]['student_document_file']))
                                {
                                    if (!empty($student_document_update->student_document_file)){
                                        $student_document_file = check_file_exist($student_document_update->student_document_file, 'student_document_file');
                                        if (!empty($student_document_file))
                                        {
                                            unlink($student_document_file);
                                        } 
                                    }
                                    $file                          = $fileData[$documentKey]['student_document_file'];
                                    $config_document_upload_path   = \Config::get('custom.student_document_file');
                                    $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                                    $ext                           = substr($file->getClientOriginalName(),-4);
                                    $name                          = substr($file->getClientOriginalName(),0,-4);
                                    $filename                      = $name.mt_rand(0,100000).time().$ext;
                                    $file->move($destinationPath, $filename);
                                    $student_document_update->student_document_file = $filename;
                                }
                                
                                $student_document_update->save();
                            } else {
                                $student_document   = new StudentDocument();
                                $student_document->admin_id                 = $loginInfo['admin_id'];
                                $student_document->update_by                = $loginInfo['admin_id'];
                                $student_document->student_id               = $student->student_id;
                                $student_document->document_category_id     = $documents['document_category_id'];
                                $student_document->student_document_details = $documents['student_document_details'];
                                if (isset($fileData[$documentKey]['student_document_file']))
                                {
                                    $file                          = $fileData[$documentKey]['student_document_file'];
                                    $config_document_upload_path   = \Config::get('custom.student_document_file');
                                    $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                                    $ext                           = substr($file->getClientOriginalName(),-4);
                                    $name                          = substr($file->getClientOriginalName(),0,-4);
                                    $filename                      = $name.mt_rand(0,100000).time().$ext;
                                    $file->move($destinationPath, $filename);
                                    $student_document->student_document_file = $filename;
                                }
                                $student_document->save();
                                $document_list[] = $student_document;
                            }
                        $documentKey++;
                        }
                    }
                    
                }
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/student/view-list')->withSuccess($success_msg);
            
    }

    /**
     *  Change Student's status
     *  @Shree on 31 July 2018
    **/
    public function changeStatus($status,$id)
    {
        $student_id = get_decrypted_value($id, true);
        $student    = Student::find($student_id);
        if ($student)
        {
            $student->student_status  = $status;
            $student->save();
            $success_msg = "student status update successfully!";
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "student not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Destroy student data
     *  @Shree on 31 July 2018
    **/
    public function destroy($id)
    {
        $student_id = get_decrypted_value($id, true);
        $student    = Student::find($student_id)->with('getParent')->get();
       
        if (!empty($student[0]['student_image']))
        {
            $profile = check_file_exist($student[0]['student_image'], 'student_image');
            if (!empty($profile))
            {
                $student[0]['profile'] = $profile;
                if(file_exists($profile)){
                    @unlink($profile);
                }
            }
        }
        $checkSibling = check_sibling($student[0]['getParent']['student_parent_id'],$student_id);
        if ($student)
        {
            if($checkSibling == 'false'){
                $parentAdmin    = Admin::Find($student[0]['getParent']['reference_admin_id']);
                $parentAdmin->delete();
            }
            $studentAdmin         = Admin::Find($student[0]['reference_admin_id']);
            $studentAdmin->delete();
            $success_msg = "Student deleted successfully!";
            return redirect()->back()->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Student not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get sections data according class
     *  @Shree on 27 July 2018
    **/
    public function getSectionData()
    {
        $class_id = Input::get('class_id');
        $section = get_class_section($class_id);
        $data = view('admin-panel.class-teacher-allocation.ajax-select',compact('section'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Get Data for view list page(Datatables)
     *  @Shree on 28 July 2018
    **/
    public function listData(Request $request)
    {
        $classSectionData     = [];
        $arr_class_section = $this->getListData("",$request);
        foreach ($arr_class_section as $key => $arr_class_section_data)
        {
            $classSectionData[$key] = (object) $arr_class_section_data;
        }
        
        return Datatables::of($classSectionData)
            ->addColumn('medium_type', function ($classSectionData)
            {
                $arr_medium   = \Config::get('custom.medium_type');
                $medium_type  = $arr_medium[$classSectionData->medium_type];
                return $medium_type;
            })
            ->addColumn('action', function ($classSectionData)
            {
                $encrypted_section_id = get_encrypted_value($classSectionData->section_id, true);
                
                return ' 
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="View Students" href="view-students/' . $encrypted_section_id . '" "">View Students</a>
                    </li>
                    <li>
                        <a title="Assign Roll Numbers" onclick="return confirm('."'Are you sure?'".')" href="assign-roll-no/' . $encrypted_section_id . '" "">Assign Roll Numbers</a>
                    </li>
                    </ul>
                </div>';
               
                
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Get list data
     *  @Shree on 28 July 2018
    **/
    public function getListData($data = array(),$request)
    {
        $loginInfo = get_loggedin_user_data();
        $data_return   = [];
        $data          = [];
      
        $arr_class_section_data = Section::where(function($query) use ($loginInfo,$request) 
            {
                
                if (!empty($loginInfo['school_id']))
                {
                    $query->where('school_id', "=", $loginInfo['school_id']);
                }
                if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null && $request->get('section_id') != "Select section")
                {
                    $query->where('section_id', "=", $request->get('section_id'));
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null &&  $request->get('class_id') != "Select class")
                {
                    $query->where('class_id', "=", $request->get('class_id'));
                }
            })->with('sectionClass')->get();
        if (!empty($arr_class_section_data))
        {
            foreach ($arr_class_section_data as $key => $class_section_data)
            {
                $totalStudents = 0;
                $totalStudents = StudentAcademic::where(function($query) use ($loginInfo,$class_section_data) 
                {
                    if (!empty($loginInfo['school_id']))
                    {
                        $query->where('school_id', "=", $loginInfo['school_id']);
                    }
                    $query->where('current_class_id', "=", $class_section_data['class_id']);
                    $query->where('current_section_id', "=", $class_section_data['section_id']);
                   
                })->get()->count();
                $list_arr = array(
                    'section_id'        => $class_section_data['section_id'],
                    'section_name'      => $class_section_data['section_name'],
                    'section_order'     => $class_section_data['section_order'],
                    'medium_type'       => $class_section_data['medium_type'],
                    'section_status'    => $class_section_data['section_status'],
                    'class_id'          => $class_section_data['class_id'],
                    'total_students'    => $totalStudents,
                );
                if (isset($class_section_data['sectionClass']['class_name']))
                {
                    $list_arr['class_name'] = $class_section_data['sectionClass']['class_name'];
                }
                $data_return[] = $list_arr;
            }
        }
        return $data_return;
    }
    /**
     *  Get Student List Data for view student page(Datatables)
     *  @Shree on 28 July 2018
    **/
    public function studentListData(Request $request)
    {
        
        $student     = [];
        $arr_students = get_student_list_by_section_id($request);
        foreach ($arr_students as $key => $arr_student)
        {
            $student[$key] = (object) $arr_student;
        }
        return Datatables::of($student)
            ->addColumn('action', function ($student)
            {
                $encrypted_student_id = get_encrypted_value($student->student_id, true);
                if($student->student_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Leave"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Study"><i class="fas fa-plus-circle"></i></div>';
                }
                // <li><a href="../delete-student/' . $encrypted_student_id . '" onclick="return confirm('."'Are you sure?'".')" >Delete</a></li>
                return ' 
                <div class="dropdown">
                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-label"></i>
                <span class="caret"></span>
                </button>
                 <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a href="../student-profile/' . $encrypted_student_id . '" "">View Profile</a>
                    </li>
                 </ul>
             </div>
             
            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="../add-student/' . $encrypted_student_id . '"><i class="zmdi zmdi-edit"></i></a></div>
            ';
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Get Data for parent information page(Datatables)
     *  @Shree on 1 Aug 2018
    **/
    public function parentListData(Request $request)
    {
        $parentData     = [];
        $arr_parent = get_parent_list("",$request);
        foreach ($arr_parent as $key => $arr_parent_data)
        {
            $parentData[$key] = (object) $arr_parent_data;
        }
        return Datatables::of($parentData)
            
            ->addColumn('action', function ($parentData)
            {
                $encrypted_student_parent_id = get_encrypted_value($parentData->student_parent_id, true);
                // view-parent-detail/' . $encrypted_student_parent_id . '" "
                return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                        <li>
                            <a title="View Parent Detail" href="#">View Profile</a>
                        </li>
                    </ul>
                </div>';
                
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

     /**
     *  View page for students
     *  @Shree on 28 July 2018
    **/
    public function view_parent_detail($id = NULL)
    {
        if(!empty($id)) {
            $loginInfo                      = get_loggedin_user_data();
            $decrypted_student_parent_id    = get_decrypted_value($id, true);
            $parent                         = [];
            $parent                         = get_parent_list($decrypted_student_parent_id,"");
            $parent                         = isset($parent[0]) ? $parent[0] : [];
            $child                          = get_child_list($decrypted_student_parent_id);
            $parent['child_info']           = $child;
            //p($parent);
            $data = array(
                'login_info'      => $loginInfo,
                'page_title'      => trans('language.view_parent_detail'),
                'parent'          => $parent,
            );
            //p($student_data);
            return view('admin-panel.student.parent_detail')->with($data);
        } else {
            return redirect('admin-panel/student/parent-information'); 
        }
    }

    public function check_parent_existance(Request $request){
        if($request->has('mobile_no') ) {
            $parent_info = check_parents_exist("",$request->get('mobile_no'));
            if(isset($parent_info['student_parent_id'])) {
                $parent_info['exist_status']  = 1;
            } else {
                $parent_info['exist_status']  = 0;
            }
        } else {
            $parent_info['exist_status']  = 0;
        }
        
        return $parent_info;
    }

    /**
     *  Edit page for edit student document 
     *  @Pratyush on 10 Aug 2018
    **/
    public function edit_document(Request $request, $id = NULL)
    {

        $loginInfo  = get_loggedin_user_data();
        $decrypted_document_id  = get_decrypted_value($id, true);
        $document               = StudentDocument::find($decrypted_document_id);
        if (!empty($id))
        {
            if (!$document)
            {
                return redirect('admin-panel/student/view-students')->withError('Student not found!');
            }
                
            $page_title             = 'Edit Document';
            $save_url               = url('admin-panel/student/save-document/' . $id);
            $submit_button          = 'Update';
        }
        $arr_document_category              = get_all_document_category(2);
        $document['arr_document_category']   = add_blank_option($arr_document_category, 'Document category');

       
        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'document'      => $document,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/studebt/student-list'),
        );
        return view('admin-panel.student.edit_document')->with($data);
    }

    /**
     *  Save page for edit student document 
     *  @Pratyush on 10 Aug 2018
    **/
    public function save_document(Request $request){
        // dd($request->hasFile('documents'));
        
        $student_document   = StudentDocument::find($request->get('student_document_id'));
        
            if (!$student_document)
            {
                return redirect('/admin-panel/student/view-students/')->withError('Document not found!');
            }
            $success_msg = 'Document updated successfully!';
            
            
            $previous_doc_details = StudentDocument::where('student_id',$student_document->student_id)->where('document_category_id',Input::get('student_document_category'))->where('student_document_id','!=',$request->get('student_document_id'))->count();

            if($previous_doc_details != 0){
                $encrypted_student_document_id                       = get_encrypted_value($request->get('student_document_id'), true);   
                return redirect('/admin-panel/student/save-document/'.$encrypted_student_document_id)->withError('Document not found!');   
            }

        $validatior = Validator::make($request->all(), [

                'documents'                 => 'mimes:pdf,jpg,jpeg,png',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }else{    

            $encrypted_student_id                       = get_encrypted_value($student_document->student_id, true);   
            $student_document->document_category_id     = Input::get('student_document_category');
            $student_document->student_document_details = Input::get('student_document_details');
            if ($request->hasFile('documents'))
            {
                $file                          = $request->File('documents');
                $config_document_upload_path   = \Config::get('custom.student_document_file');
                $destinationPath               = public_path() . $config_document_upload_path['upload_path'];
                $ext                           = substr($file->getClientOriginalName(),-4);
                $name                          = substr($file->getClientOriginalName(),0,-4);
                $filename                      = $name.mt_rand(0,100000).time().$ext;
                $file->move($destinationPath, $filename);
                $student_document->student_document_file = $filename;
            }
            $student_document->save();
            return redirect('admin-panel/student/student-profile/'.$encrypted_student_id)->withSuccess($success_msg);
        }
    }

    /**
     *  Student document's status
     *  @Pratyush on 10 Aug 2018.
    **/
    public function changeDocumentStatus($status,$id)
    {
        $student_document_id    = get_decrypted_value($id, true);
        $student_document       = StudentDocument::find($student_document_id);
        if($student_document){
            $student_document->student_document_status  = $status;
            $student_document->save();
            $success_msg = "Document status updated!";
            $encrypted_student_id = get_encrypted_value($student_document->student_id, true);   
            return redirect('admin-panel/student/student-profile/'.$encrypted_student_id)->withSuccess($success_msg);
        }else{
            $error_message = "Document not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Destroy Student Document's data
     *  @Pratyush on 10 Aug 2018.
    **/
    public function destroyDocument($id,$type)
    {
        if($type == 1){
            $student_document_id    = $id;
        } else {
            $student_document_id    = get_decrypted_value($id,true);
        }
        $student_document       = StudentDocument::find($student_document_id);
        if($student_document){
            if (!empty($student_document['student_document_file'])){
                $student_document_file = check_file_exist($student_document['student_document_file'], 'student_document_file');
                if (!empty($student_document_file))
                {
                    unlink($student_document_file);
                } 
            }
            $student_document->delete();
            $success_msg = "Document deleted successfully!";
            if($type == 1){
                p($success_msg);
            } 
            
            $encrypted_student_id = get_encrypted_value($student_document->student_id, true);   
            return redirect('admin-panel/student/student-profile/'.$encrypted_student_id)->withSuccess($success_msg);
        }else{
            $error_message = "Document not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function checkSectionCapacity(){
        $section_id = Input::get('section_id');
        $status = check_section_capacity($section_id);
        return $status;
    }

    public function assign_roll_numbers($sectionid){
        $section_id     = get_decrypted_value($sectionid,true);
        $status         = assign_roll_no_by_section($section_id);
        if($status == "Success") {
            $success_msg = "Roll No assigned";
            return redirect()->back()->withSuccess($success_msg);
        } else {
            $error_message = "Some error occured!!";
            return redirect()->back()->withErrors($error_message);
        }
       
    }

}
