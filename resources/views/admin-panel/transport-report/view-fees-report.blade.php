﻿@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
        overflow-x: visible !important;
    }
</style>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2> Fees Report</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/transport') !!}">Transport</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/transport/transport-report/view-fees-report') !!}">Fees Report </a></li>
                </ul>                
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="level">
                                                <option value="">Class</option>
                                                <option value="1">XII</option>
                                                <option value="2">XI</option>
                                                <option value="3">X</option>
                                                <option value="4">VIII</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="level">
                                                <option value="">Section</option>
                                                <option value="1">A</option>
                                                <option value="2">B</option>
                                                <option value="3">C</option>
                                                <option value="4">D</option>
                                            </select>
                                        </div>
                                        <div class="col-md-1 col-lg-1 col-sm-12">
                                            <button class="btn btn-raised btn-primary">Search</button>
                                        </div>
                                        <div class="col-md-1 col-lg-1 col-sm-12">
                                            <button class="btn btn-raised btn-primary">Clear</button>
                                        </div>
                                        <!-- <div class="col-lg-3"></div> -->
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <button class="btn btn-raised btn-primary float-right">Default</button>
                                        </div>
                                    </div>
                                    <hr>

                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Enroll</th>
                                                <th>Name</th>
                                                <th>Class-section</th>
                                                <th>Vehicle No</th>
                                                <th>Fees Paid</th>
                                                <th>Fees Due</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>11010</td>
                                                <td style="width: 210px;"><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%"> Ankit Dave</td>
                                                
                                                <td>X-A</td>
                                                <td>AP-02-BK-1084</td>
                                                <td>10000</td>
                                                <td>500</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>11010</td>
                                                <td style="width: 210px;"><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%">  Ankit Dave</td>
                                                
                                                <td>X-A</td>
                                                <td>AP-02-BK-1084</td>
                                                <td>10000</td>
                                                <td>500</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>11010</td>
                                                <td style="width: 210px;"><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%">  Ankit Dave</td>
                                                <td>X-A</td>
                                                <td>AP-02-BK-1084</td>
                                                <td>10000</td>
                                                <td>500</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>11010</td>
                                                <td style="width: 210px;"><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%">  Ankit Dave</td>
                                                <td>X-A</td>
                                                <td>AP-02-BK-1084</td>
                                                <td>10000</td>
                                                <td>500</td>
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection

<script type="text/javascript">
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {

    document.getElementById("myDropdown").classList.toggle("show");
}
function myFunction2() {

    document.getElementById("myDropdown2").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>
