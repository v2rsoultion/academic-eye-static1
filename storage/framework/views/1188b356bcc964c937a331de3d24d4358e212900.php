
<?php $__env->startSection('content'); ?>
<style type="text/css">
  td{
    padding: 14px 10px !important;
  }
  .nav-tabs>.nav-item>.nav-link {
    width: 100px;
    margin-right: 5px;
        border: 1px solid #ccc !important;
  }
  #tabingclass .nav-tabs>.nav-item>.nav-link {
    border-radius: 4px !important;
    padding: 8px 25px;
  }
   #tabingclass .nav-link:hover {
        background: #6572b8 !important;
    color: #fff !important;
}
#tabingclass .nav-link:active {
        background: #6572b8 !important;
    color: #fff !important;
}
.idi .nav-tabs>.nav-item>.nav-link.active {
  background: #6572b8 !important;
    color: #fff !important;
}
.modal-dialog {
  max-width: 700px !important;
}
.checkbox {
  float: left;
  margin-right: 20px;
}
select {
  height: 120px !important;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Map Marks Criteria to Subjects</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/examination'); ?>">Examination And Report Cards</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/examination'); ?>">Map Marks Criteria to Subjects</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/examination/map-exam-to-class-subjects/view'); ?>">Add</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 20px !important;">
                 
                     <form class="" action="" method=""  id="" style="width: 100%;">
                      <div class="row">
                       <div class="col-lg-3">
                         <div class="form-group">
                          <!-- <lable class="from_one1">Exam Name</lable> -->
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select class</option>
                              <option value="1">Class-1</option>
                              <option value="2">Class-2</option>
                              <option value="3">Class-3</option>
                              <option value="4">Class-4</option>
                            </select>
                         </div>
                      </div>
                          <div class="col-lg-3">
                         <div class="form-group">
                          <!-- <lable class="from_one1">Exam Name</lable> -->
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Section</option>
                              <option value="A">A</option>
                              <option value="B">B</option>
                              <option value="C">C</option>
                              <option value="D">D</option>
                            </select>
                         </div>
                      </div>
                     <div class="col-lg-1">
                      <button type="submit" class="btn btn-raised btn-primary " title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Clear
                      </button>
                    </div>
                    </div>
                   </form>
                <hr style="width: 100%">
                    <!--  DataTable for view Records  -->
                     <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Exam-Name</th>
                          <th>No. of Subjects</th>
                          <th>Action</th>
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 10; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td>Half Yearly</td>
                          <td>6</td>
                          <td><button type="button" class="btn btn-raised btn-primary" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#viewSubjectsModel" >Manage Criteria</button></td>
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>
<script type="text/javascript">
  $("select").height("120px");
</script>

<!-- Action Model  -->
<div class="modal fade" id="viewSubjectsModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">List of Subjects</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div style="width:652px;border: 1px solid #ccc;font-weight: bold; border-radius: 5px; margin: 5px 0px; padding: 2px 10px;">
             <div>Class - Section: X - A</div>
             <div>Exam Name: Half Yearly</div>
             </div>
        <!--  <form>
            <div class="row">
             <div class="col-lg-4">
               <div class="form-group">
                <lable class="from_one1">Exam Name</lable>
                  <select class="form-control show-tick select_form1" name="" id="">
                    <option value="">Select Class</option>
                    <option value="1">Class-1</option>
                    <option value="2">Class-2</option>
                    <option value="3">Class-3</option>
                    <option value="4">Class-4</option>
                  </select>
               </div>
            </div>
            <div class="col-lg-4">
               <div class="form-group">
                <lable class="from_one1">Exam Name</lable>
                  <select class="form-control show-tick select_form1" name="" id="">
                    <option value="">Select Exam</option>
                    <option value="1">Exam-1</option>
                    <option value="2">Exam-2</option>
                    <option value="3">Exam-3</option>
                    <option value="4">Exam-4</option>
                  </select>
               </div>
            </div>
           <div class="col-lg-2">
            <button type="submit" class="btn btn-raised btn-primary " title="Search">Search
            </button>
          </div>
          <div class="col-lg-2">
            <button type="reset" class="btn btn-raised btn-primary " title="Clear">Clear
            </button>
          </div>
          </div>
        </form>  -->
        <div id="table-wrapper">
          <div id="table-scroll">
        <div class="">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                      <thead>
                        <tr>
                          <th>S No</th>
                          <th>Subject Name </th>
                          <th>Marks Criteria</th>
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i <5 ; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td style="vertical-align: middle;">Subject-<?php echo $counter; ?></td>
                          <td>  
                          <div class="col-lg-8">
                           <div class="form-group" style="margin-bottom: 0px;">
                              <select class="form-control show-tick select_form1" name="criteria<?php echo $counter; ?>"" id="criteria<?php echo $counter; ?>">
                                <option value="">Select Criteria</option>
                                <option value="1">Criteria-1</option>
                                <option value="2">Criteria-2</option>
                                <option value="3">Criteria-3</option>
                                <option value="4">Criteria-4</option>
                              </select>
                           </div>
                          </div></td>
                         
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div></div></div>
                  <hr>
                  <div class="col-lg-1 float-right" style="margin-right: 48px">
                    <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel
                    </button>
                  </div>
                   <div class="col-lg-1 float-right" style="margin-right: 30px">
                    <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
                    </button>
                  </div>
                   
      </div>
    </div>
  </div>
</div>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>