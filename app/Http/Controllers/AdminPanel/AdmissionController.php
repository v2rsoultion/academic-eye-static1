<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Admission\AdmissionForm;

use Yajra\Datatables\Datatables;

class AdmissionController extends Controller
{
    /**
     *  View page for Admission form
     *  @Shree on 11 Sept 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $arr_sessions = get_arr_session();
        $admissionForm = [];
        $arr_medium   = \Config::get('custom.medium_type');
        $admissionForm['arr_session'] = $arr_sessions;
        $admissionForm['arr_medium'] = $arr_medium;
        $data = array(
            'page_title'    => trans('language.view_admission_form'),
            'redirect_url'  => url('admin-panel/admission-form/view-admission-forms'),
            'login_info'    => $loginInfo,
            'admissionForm' => $admissionForm
        );
        return view('admin-panel.admission-form.index')->with($data);
    }

    /**
     *  Add page for admission
     *  @Shree on 11 Sept 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data               = [];
        $admission          = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_sessions       = get_arr_session();
        $arr_brochure       = [];
        $arr_class          = [];
        $arr_medium         = \Config::get('custom.medium_type');
        $arr_form_type      = \Config::get('custom.form_type');
        $arr_email_receipt  = \Config::get('custom.email_receipt');
        $arr_message_via    = \Config::get('custom.message_via');
        $arr_payment_mode    = \Config::get('custom.payment_mode');
        if (!empty($id))
        {
            $decrypted_admission_form_id = get_decrypted_value($id, true);
            
            $admission              = AdmissionForm::Find($decrypted_admission_form_id);
            if (!$admission)
            {
                return redirect('admin-panel/admission-form/add-form')->withError('Admission form not found!');
            }
            $page_title           = trans('language.edit_admission_form');
            $decrypted_admission_form_id = get_encrypted_value($admission->admission_form_id, true);
            $save_url             = url('admin-panel/admission-form/save/' . $decrypted_admission_form_id);
            $submit_button        = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_admission_form');
            $save_url      = url('admin-panel/admission-form/save');
            $submit_button = 'Save';
        }
        $admission['arr_session']       = $arr_sessions;
        $admission['arr_class']         = add_blank_option($arr_class, 'Select Class');
        $admission['arr_medium']        = add_blank_option($arr_medium, 'Select Medium');
        $admission['arr_form_type']     = add_blank_option($arr_form_type, 'Form Type');
        $admission['arr_email_receipt'] = $arr_email_receipt;
        $admission['arr_message_via']   = $arr_message_via;
        $admission['arr_brochure']      = add_blank_option($arr_brochure, 'Select Brochure');
        $admission['arr_payment_mode']  = add_blank_option($arr_payment_mode, 'Payment Mode');
       
        $data                           = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'admission'     => $admission,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/admission-form/view-admission-forms'),
        );
        return view('admin-panel.admission-form.add')->with($data);
    }

    
    /**
     *  Add and update Admission form's data
     *  @Shree on 10 Sept 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo                      = get_loggedin_user_data();
        $decrypted_admission_form_id    = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $admissionform = AdmissionForm::find($decrypted_admission_form_id);

            if (!$admissionform)
            {
                return redirect('/admin-panel/admission-form/add-admission-form/')->withError('Admission form not found!');
            }
            $success_msg = 'Admission form updated successfully!';
        }
        else
        {
            $admissionform     = New AdmissionForm;
            $success_msg = 'Admission form saved successfully!';
            $decrypted_admission_form_id = null;
        }
        $validatior = Validator::make($request->all(), [
                'form_name'             => 'required|unique:admission_forms,form_name,' . $decrypted_admission_form_id . ',admission_form_id',
                'session_id'            => 'required',
                'class_id'              => 'required',
                'medium_type'           => 'required',
                'no_of_intake'          => 'required',
                'form_prefix'           => 'required',
                'form_type'             => 'required',
                'form_success_message'  => 'required',
                'form_failed_message'   => 'required',
                'form_message_via'      => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $admissionform->admin_id                = $loginInfo['admin_id'];
                $admissionform->update_by               = $loginInfo['admin_id'];
                $admissionform->form_name               = Input::get('form_name');
                $admissionform->medium_type             = Input::get('medium_type');
                $admissionform->session_id              = Input::get('session_id');
                $admissionform->class_id                = Input::get('class_id');
                $admissionform->form_prefix             = Input::get('form_prefix');
                $admissionform->form_type               = Input::get('form_type');
                $admissionform->form_message_via        = Input::get('form_message_via');
                $admissionform->brochure_id             = Input::get('brochure_id');
                $admissionform->form_last_date          = Input::get('form_last_date');
                $admissionform->form_fee_amount         = Input::get('form_fee_amount');
                $admissionform->form_pay_mode           = implode(",",Input::get('form_pay_mode'));
                $admissionform->form_email_receipt      = Input::get('form_email_receipt');
                $admissionform->form_instruction        = Input::get('form_instruction');
                $admissionform->form_information        = Input::get('form_information');
                $admissionform->form_success_message    = Input::get('form_success_message');
                $admissionform->form_failed_message     = Input::get('form_failed_message');
                $admissionform->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/admission-form/view-admission-forms')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 12 Sept 2018
    **/
    public function anyData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $admissionForm = AdmissionForm::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('session_id')) && $request->get('session_id') != null)
            {
                $query->where('session_id', "=", $request->get('session_id'));
            }
            if (!empty($request) && !empty($request->get('medium_type')) && $request->get('medium_type') != null)
            {
                $query->where('medium_type', "=", $request->get('medium_type'));
            }
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('form_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('admission_form_id', 'ASC')->with('getSessionInfo')->with('getClassInfo')->get();
        
        return Datatables::of($admissionForm)
       
        ->addColumn('session_name', function ($admissionForm)
        {
           return $admissionForm->getSessionInfo['session_name'];
        }) 
        ->addColumn('class_name', function ($admissionForm)
        {
           return $admissionForm->getClassInfo['class_name'];
        }) 
        ->addColumn('form_type', function ($admissionForm)
        {
           $arr_form_type      = \Config::get('custom.form_type');
           return $arr_form_type[$admissionForm->form_type];
        }) 
       
        ->addColumn('action', function ($admissionForm)
        {
            $encrypted_admission_form_id = get_encrypted_value($admissionForm->admission_form_id, true);
            if($admissionForm->form_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                <div class="dropdown">
                    <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                    <li>
                        <a title="Manage Fields" href="manage-fields/' . $encrypted_admission_form_id . '" "">Manage Fields</a>
                    </li>
                   
                    </ul>
                </div>
                <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="brochure-status/'.$status.'/' . $encrypted_admission_form_id . '">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-brochure/' . $encrypted_admission_form_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-admission-form/' . $encrypted_admission_form_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Destroy admission form's data
     *  @Shree on 12 Sept 2018
    **/
    public function destroy($id)
    {
        $admission_form_id = get_decrypted_value($id, true);
        $admissionForm    = AdmissionForm::find($admission_form_id);
        if ($admissionForm)
        {
            $admissionForm->delete();
            $success_msg = "Admission form deleted successfully!";
            return redirect('admin-panel/admission-form/view-admission-forms')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Admission form not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change admission form's status
     *  @Shree on 12 Sept 2018
    **/
    public function changeStatus($status,$id)
    {
        $admission_form_id = get_decrypted_value($id, true);
        $admissionForm    = AdmissionForm::find($admission_form_id);
        if ($admissionForm)
        {
            $admissionForm->form_status  = $status;
            $admissionForm->save();
            $success_msg = "Admission form status updated successfully!";
            return redirect('admin-panel/admission-form/view-admision-forms')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Admission form not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function manageFieldsView($id){
        $loginInfo  = get_loggedin_user_data();
        $admission_form_id = get_decrypted_value($id, true);
        $admissionForm    = AdmissionForm::find($admission_form_id);
        if ($admissionForm)
        {
            $arr_student_fields   = \Config::get('custom.student_fields');
            $admissionForm['arr_student_fields'] = $arr_student_fields;
            $save_url             = url('admin-panel/admission-form/fields-save/' . $admission_form_id);
            $data = array(
                'page_title'    => trans('language.view_admission_form'),
                'redirect_url'  => url('admin-panel/admission-form/view-admission-forms'),
                'login_info'    => $loginInfo,
                'admissionForm' => $admissionForm,
                'save_url'      => $save_url,
            );
            
            p($admissionForm);   
            return view('admin-panel.admission-form.manage-fields')->with($data);
        }  else {
            $error_message = "Admission form not found!";
            return redirect('admin-panel/admission-form/view-admission-forms')->withErrors($error_message);
        }
    }
}
