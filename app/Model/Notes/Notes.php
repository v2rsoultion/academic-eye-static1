<?php

namespace App\Model\Notes;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    protected $table      = 'online_notes';
    protected $primaryKey = 'online_note_id';

    // For Class
    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes','class_id');
    }

    // For Section
    public function getSection()
    {
        return $this->belongsTo('App\Model\Section\Section','section_id');
    }

    // For Subject
    public function getSubject()
    {
        return $this->belongsTo('App\Model\Subject\Subject','subject_id');
    }
}
