<?php

namespace App\Model\Nationality;

use Illuminate\Database\Eloquent\Model;

class Nationality extends Model
{
    protected $table      = 'nationality';
    protected $primaryKey = 'nationality_id';
}
