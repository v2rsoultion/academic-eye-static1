﻿@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .card .body .table td, .cshelf1 {
        width: 50px !important;
    }
    .pp{
        width: 800px !important;
    }
    .table-responsive {
        overflow-x: visible !important;
    }
</style>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Leave Student Report</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/hostel') !!}">{!! trans('language.menu_hostel') !!}</a></li>
                 <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/hostel') !!}">Reports</a></li>
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/hostel/reports/leave-student-report') !!}">Leave Student Report</a></li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">

                                <!-- <div class="container-fluid"> -->
                                    <div class="row clearfix">
                                         <div class="col-lg-2 col-md-2 col-sm-12">
                                <!-- <lable class="from_one1">Hostel Name :</lable>                  -->
                                    <select class="form-control show-tick select_form1" name="">
                                        <option value="">Hostel Name</option>
                                        <option value="VII">Hostel-1</option>
                                        <option value="X">Hostel-2</option>
                                    
                                    </select>
                                </div>
                                      <div class="col-lg-2 col-md-2 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="class_name">
                                                <option value="">Class</option>
                                                <option value="VII">XII</option>
                                                <option value="X">VIII</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="Class_section">
                                                <option value="">Section</option>
                                                <option value="VII">A</option>
                                                <option value="X">B</option>
                                            </select>
                                        </div>
                                          
                                        <div class="col-lg-2 col-md-2 col-sm-12">
                                            <div class="input-group ">
                                                <input type="text" class="form-control" value="" placeholder="Student Name">
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <button class="btn btn-raised btn-primary">Search</button>
                                        </div>
                                        <div class="col-md-1">
                                            <button class="btn btn-raised btn-primary">Clear</button>
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-1">
                                            <button class="btn btn-raised btn-primary float-right">Export</button>
                                        </div>
                                    </div>
                                <!-- </div> -->
                                <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Enroll No</th>
                                                <th style="width: 120px">Student Name</th>
                                                <th>Class - Section</th>                      
                                                <th style="width: 100px">Father Name</th>
                                                <th>Contact No</th>
                                                <th style="width: 105px">Hostel - Block</th>
                                                <th style="width: 105px">Floor - Room</th>
                                                <th class="text-center" style="width: 135px">Date<br> (Joining - Leaving)</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>I1000I1A</td>
                                                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%"> <a href="#" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>XII-A</td>
                                                <td>Mahesh Kumar</td>
                                                <td>9008090080</td>
                                                <td>Hostel-1 - Block-1</td>
                                                <td>Floor-1 - Room-1</td>
                                                <td>2015-03-22 - 2016-03-22</td>
                                            </tr>
                                            <tr>
                                               <td>2</td>
                                                <td>I2000I2A</td>
                                                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%"> <a href="#" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>XII-A</td>
                                                <td>Mahesh Kumar</td>
                                                <td>9008090080</td>
                                                <td>Hostel-1 - Block-1</td>
                                                <td>Floor-1 - Room-2</td>
                                                <td>2015-03-22 - 2016-03-22</td>
                                            <tr>
                                                <td>3</td>
                                                <td>I3000I3A</td>
                                                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%"> <a href="#" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>XII-A</td>
                                                <td>Mahesh Kumar</td>
                                                <td>9008090080</td>
                                                <td>Hostel-1 - Block-1</td>
                                                <td>Floor-1 - Room-3</td>
                                                <td>2015-03-22 - 2016-03-22</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>I4000I4A</td>
                                                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%"> <a href="#" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>XII-A</td>
                                                <td>Mahesh Kumar</td>
                                                <td>9008090080</td>
                                                <td>Hostel-1 - Block-1</td>
                                                <td>Floor-1 - Room-4</td>
                                                <td>2015-03-22 - 2016-03-22</td>
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection