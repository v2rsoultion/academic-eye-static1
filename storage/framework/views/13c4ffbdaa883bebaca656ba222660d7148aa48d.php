
<?php $__env->startSection('content'); ?>
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Department</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/payroll'); ?>">Payroll</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/payroll/manage-department'); ?>">Department</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card" style="margin-bottom: 20px;">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body" style="padding: 0px 20px 20px 20px;">
               
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Add Department :-</div> -->
                  <form class="" action="" id="manage_vendor" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-4">
                        <div class="form-group">
                          <lable class="from_one1">Name</lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Name">
                        </div>
                         <?php if($errors->has('name')): ?> <p class="help-block"><?php echo e($errors->first('name')); ?></p> <?php endif; ?>
                      </div>

                      <div class="col-lg-8">
                        <div class="form-group">
                          <lable class="from_one1">Description</lable>
                          <textarea name="description" class="form-control" placeholder="Description"></textarea>
                        </div>
                         <?php if($errors->has('description')): ?> <p class="help-block"><?php echo e($errors->first('description')); ?></p> <?php endif; ?>
                      </div>
                    </div>
                      <!-- <hr> -->
                      <div class="row">
                      <div class="col-lg-1 ">
                        <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
                        </button>
                      </div>
                      <div class="col-lg-1 ">
                        <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel
                        </button>
                      </div>
                    
                    </div>
                  </form>
                </div>
              </div>
                  <!-- <hr> -->
                  <div class="card">
                    <div class="body form-gap">
                  <div class="headingcommon  col-lg-12" style="margin-left: -13px">Search By :-</div>
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Name</lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Name">
                        </div>
                      </div>
                     
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                
                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <hr>
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>No of Staff Mapped</th>
                        <th>Not Mapped </th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                       
                      <tr>
                        <td>1</td>
                        <td>Department- 1</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                        <td>5</td>
                        <td>15</td>
                        <td class="text-center">
                          <a href="<?php echo e(url('admin-panel/payroll/manage-department/view-map-employees')); ?>" class="btn btn-raised btn-primary">Map Employee</a>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Department- 2</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                        <td>7</td>
                        <td>5</td>
                        <td class="text-center">
                          <a href="<?php echo e(url('admin-panel/payroll/manage-department/view-map-employees')); ?>" class="btn btn-raised btn-primary">Map Employee</a>
                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Department- 3</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                        <td>15</td>
                        <td>2</td>
                        <td class="text-center">
                          <a href="<?php echo e(url('admin-panel/payroll/manage-department/view-map-employees')); ?>" class="btn btn-raised btn-primary">Map Employee</a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>