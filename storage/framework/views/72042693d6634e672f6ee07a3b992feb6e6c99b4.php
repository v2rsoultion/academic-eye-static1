<?php if(isset($allocation['staff_class_allocation_id']) && !empty($allocation['staff_class_allocation_id'])): ?>
<?php  $readonly = true; $disabled = ''; ?>
<?php else: ?>
<?php $readonly = false; $disabled='disabled'; ?>
<?php endif; ?>

<?php echo Form::hidden('staff_class_allocation_id',old('staff_class_allocation_id',isset($allocation['staff_class_allocation_id']) ? $allocation['staff_class_allocation_id'] : ''),['class' => 'gui-input', 'id' => 'staff_class_allocation_id', 'readonly' => 'true']); ?>

<style type="text/css">
    .theme-blush .btn-primary {
    margin-top: 2px !important;
}
</style>
<?php if($errors->any()): ?>
<div class="alert alert-danger" role="alert">
    <?php echo e($errors->first()); ?>

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.medium_type'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('medium_type', $allocation['arr_medium'],isset($allocation['medium_type']) ? $allocation['medium_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type', 'onChange'=> 'getClass(this.value),getStaff(this.value)']); ?>

                <i class="arrow double"></i>
            </label>
            <?php if($errors->has('medium_type')): ?> <p class="help-block"><?php echo e($errors->first('medium_type')); ?></p> <?php endif; ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.allot_class_name'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('class_id', $allocation['arr_class'],isset($allocation['class_id']) ? $allocation['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id','onChange' => 'getSection(this.value)',$disabled]); ?>

                <i class="arrow double"></i>
            </label>
            <?php if($errors->has('class_id')): ?> <p class="help-block"><?php echo e($errors->first('class_id')); ?></p> <?php endif; ?>
        </div>
    </div>        
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.allot_section_name'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('section_id', $allocation['arr_section'],isset($allocation['section_id']) ? $allocation['section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id','required',$disabled]); ?>

                <i class="arrow double"></i>
            </label>
            <?php if($errors->has('section_id')): ?> <p class="help-block"><?php echo e($errors->first('section_id')); ?></p> <?php endif; ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.allot_teacher_name'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('teacher_id', $allocation['arr_staff'],isset($allocation['staff_id']) ? $allocation['staff_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'teacher_id',$disabled]); ?>

                <i class="arrow double"></i>
            </label>
            <?php if($errors->has('teacher_id')): ?> <p class="help-block"><?php echo e($errors->first('teacher_id')); ?></p> <?php endif; ?>
        </div>
    </div>    
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']); ?>

        <a href="<?php echo url('admin-panel/dashboard'); ?>" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {

        $("#class-teacher-allocation-form").validate({
            /* @validation  states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation  rules ------------------------------------------ */
            rules: {
                medium_type: {
                    required: true
                },
                class_id: {
                    required: true
                },
                section_id:{
                    required: true,
                },
                teacher_id:{
                    required: true
                }
            },
            /* @validation  highlighting + error placement ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });
    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/class/get-class-data')); ?>",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='class_id'").html(data.options);
                    $("select[name='class_id'").removeAttr("disabled");
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='class_id'").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }
    function getStaff(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/staff/get-staff-data')); ?>",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='teacher_id'").html(data.options);
                    $("select[name='teacher_id'").removeAttr("disabled");
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='teacher_id'").html('<option value="">Select Teacher</option>');
            $(".mycustloading").hide();
        }
    }
    function getSection(class_id)
    {
        if(class_id != ''){
            $('.mycustloading').css('display','block');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/student/get-section-data')); ?>",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $("select[name='section_id'").removeAttr("disabled");
                    $('.mycustloading').css('display','none');
                }
            });
        } else {
            $("select[name='section_id'").html('');
        }
    }
    

</script>