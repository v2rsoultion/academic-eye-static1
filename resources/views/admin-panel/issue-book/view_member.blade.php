@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.book_issued') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.book_issued') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">{!! trans('language.view_students') !!}</a></li>
                                    <li class="nav-item"><a class="nav-link"  href="{{ url('/admin-panel/student/add-student') }}">{!! trans('language.add_student') !!}</a></li>
                                </ul>                        
                            </div> -->
                            <div class="body">
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('member_type', $listData,isset($listData) ? $listData : '1', ['class' => 'form-control show-tick select_form1','id'=>'class_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                            @if($errors->has('class_id')) <p class="help-block">{{ $errors->first('class_id') }}</p> @endif
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <div class="input-group ">
                                                {!! Form::text('member_name', old('member_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.book_issued_name'), 'id' => 'member_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="member-staff-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{trans('language.book_issued_name')}}</th>
                                                <th>{{trans('language.book_issued_profile')}}</th>
                                                <th>{{trans('language.book_issued_father_name')}}</th>
                                                <th>{{trans('language.book_issued_book_left')}}</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#member-staff-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: "{{url('admin-panel/issue-book-data')}}",
                data: function (d) {
                    d.member_name   = $('input[name=member_name]').val();
                    d.member_type   = $('select[name="member_type"]').val();
                }
            },
            //ajax: '{{url('admin-panel/class/data')}}',
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'profile', render: getImg},
                {data: 'name', name: 'name'},
                {data: 'father_name', name: 'father_name'},
                {data: 'no_of_book_left', name: 'no_of_book_left'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0,
                    "orderable": false
                },
                {
                    "targets": 5, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            $("select[name='member_type'").selectpicker('refresh');
            table.draw();
            e.preventDefault();
        })

        function getImg(data, type, full, meta) {
        if (data != '') {
            return '<img src="'+data+'" height="50" />';
        } else {
            return 'No Image';
        }
    }
        
    });
  
</script>
@endsection