
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="School management.">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{!!trans('language.reset_password')!!} | {!!trans('language.project_title') !!}</title>

 
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Styles -->
    {!! Html::style('public/admin/assets/plugins/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('public/admin/assets/css/main.css') !!}
    {!! Html::style('public/admin/assets/css/authentication.css') !!}
    {!! Html::style('public/admin/assets/css/color_skins.css') !!}

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="theme-blush authentication sidebar-collapse">

<!-- Main Content -->
<div class="page-header">
    <div class="page-header-image" style="background-image:url({!! URL::to('public/admin/assets/images/login.jpg') !!}"></div>
    <div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                {!! Form::open(array('url' => '/admin-panel/password/email', 'files' => true)) !!}
                    <div class="header">
                        <div class="logo-container">
                            <img src="{!! URL::to('public/admin/assets/images/logo.svg') !!}" alt="">
                        </div>
                        <h5>Log in</h5>
                    </div>
                    
                    <div class="content">                                                
                        <div class="input-group input-lg">
                            <input type="email" name="email" id="email" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" placeholder="Email Address">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-account-circle"></i>
                            </span>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                   
                    <div class="footer text-center">
                        <button type="submit" class="btn btn-primary btn-round btn-lg btn-block">Send Password Reset Link</button>
                        <h5><a href="{{ url('/admin-panel') }}" class="link">Login?</a></h5>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

    <!-- Jquery Core Js -->
    {!! Html::script('public/admin/assets/bundles/libscripts.bundle.js') !!}
    {!! Html::script('public/admin/assets/bundles/vendorscripts.bundle.js') !!} <!-- Lib Scripts Plugin Js -->

    <script>
        $(".navbar-toggler").on('click',function() {
            $("html").toggleClass("nav-open");
        });
        //=============================================================================
        $('.form-control').on("focus", function() {
            $(this).parent('.input-group').addClass("input-group-focus");
        }).on("blur", function() {
            $(this).parent(".input-group").removeClass("input-group-focus");
        });
    </script>
    </body>
</html>
