<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HostelBlockController extends Controller
{
     public function hostelBlock() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.hostel-configuration.view-hostel-block')->with($data);
    }
}
