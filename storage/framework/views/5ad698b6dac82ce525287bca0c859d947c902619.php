<?php $__env->startSection('content'); ?>

<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2><?php echo trans('language.parent_information'); ?></h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <a href="<?php echo url('admin-panel/student/parent-information'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><a href=""><?php echo trans('language.menu_student'); ?></a></li>
                    <li class="breadcrumb-item active"><a href=""><?php echo trans('language.parent_information'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <?php if(session()->has('success')): ?>
                                <p class="green">
                                    <?php echo e(session()->get('success')); ?>

                                </p>
                            <?php endif; ?>
                            <?php if($errors->any()): ?>
                                <p class="red"><?php echo e($errors->first()); ?></p>
                            <?php endif; ?>
                            <div class="body form-gap">
                                <?php echo Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']); ?>

                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                <?php echo Form::text('roll_no', old('roll_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_roll_no'), 'id' => 'roll_no']); ?>

                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                <?php echo Form::text('enroll_no', old('enroll_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_enroll_no'), 'id' => 'enroll_no']); ?>

                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                <?php echo Form::text('student_name', old('student_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_student_name'), 'id' => 'student_name']); ?>

                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="input-group ">
                                                <?php echo Form::text('father_name', old('father_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.search_by_father_name'), 'id' => 'father_name']); ?>

                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                       
                                        <div class="col-lg-1 col-md-1">
                                            <?php echo Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']); ?>

                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <?php echo Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]); ?>

                                        </div>
                                    </div>
                                <?php echo Form::close(); ?>

                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="parent-table" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th><?php echo e(trans('language.s_no')); ?></th>
                                                <th><?php echo e(trans('language.student_enroll_number')); ?></th>
                                                <th><?php echo e(trans('language.student_roll_no')); ?></th>
                                                <th><?php echo e(trans('language.student_name')); ?></th>
                                                <th><?php echo e(trans('language.student_father_name')); ?></th>
                                                <th><?php echo e(trans('language.student_contact')); ?></th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#parent-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '<?php echo e(url('admin-panel/student/parent-list')); ?>',
                data: function (d) {
                    d.roll_no = $('input[name=roll_no]').val();
                    d.enroll_no = $('input[name=enroll_no]').val();
                    d.student_name = $('input[name=student_name]').val();
                    d.father_name = $('input[name=father_name]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'student_enroll_number', name: 'student_enroll_number'},
                {data: 'student_roll_no', name: 'student_roll_no'},
                {data: 'student_name', name: 'student_name'},
                {data: 'student_father_name', name: 'student_father_name'},
                {data: 'student_father_mobile_number', name: 'student_father_mobile_number'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "className": "text-center",
                    "width": "8%"
                },
                {
                    "targets": 6, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3,4 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })

        
    });

    function getImg(data, type, full, meta) {
        if (data != '') {
            return '<img src="'+data+'" height="50" />';
        } else {
            return 'No Image';
        }
    }
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    
    

</script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>