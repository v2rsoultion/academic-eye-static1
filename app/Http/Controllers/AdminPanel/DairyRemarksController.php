<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DairyRemarksController extends Controller
{
    public function dairyRemarks() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.dairy-remarks.dairy-remarks')->with($data);
    }
}
