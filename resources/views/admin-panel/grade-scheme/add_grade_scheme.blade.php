@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">

</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Grade Scheme</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/examination') !!}">Examination And Report Cards</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/examination') !!}">Grade Scheme</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination/grade-scheme/add-grade-scheme') !!}">Add</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
              </div>
              <div class="body form-gap">
                <form class="" action="" method="" id="" style="width: 100%;">
                  <div class="row">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <lable class="from_one1" for="name">Scheme Name</lable>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Scheme Name">
                      </div>
                    </div>
                     <div class="col-lg-3">
                      <lable class="from_one1">Grade Type :</lable>
                      <div class="form-group">
                       <select class="form-control show-tick select_form1" name="" id="">
                        <option value="">Grade Type</option>
                         <option value="1">Percent</option>
                         <option value="2">Marks</option>
                        
                       </select>
                      </div>
                    </div>
                  </div>
                  <div class="row rowAddmore">
                    <div class="headingcommon  col-lg-12">Grades :-</div>
                   
                    <div class="col-lg-3 col-sm-12">
                      <lable class="from_one1">Maximum :</lable>
                      <div class="form-group">
                        <input class="form-control positive-numeric-only" id="id-blah1" min="0" name="maximum" type="number" value="" placeholder="Maximum">
                      </div>
                    </div>
                    <div class="col-lg-3 col-sm-12">
                      <lable class="from_one1">Minimum :</lable>
                      <div class="form-group">
                        <input class="form-control positive-numeric-only" id="id-blah1" min="0" name="minimum" type="number" value="" placeholder="Minimum">
                      </div>
                    </div>
                     <div class="col-lg-3 col-sm-12">
                      <lable class="from_one1">Grade :</lable>
                      <div class="form-group">
                        <input type="text" name="grade" class="form-control" placeholder="Grade">
                      </div>
                    </div>
                    <div class="col-lg-2"></div>
                    <div class="col-lg-1 btnForAdd" style="margin-top: 21px; padding:0px;">
                      <button type="button" class=" btn btn-primary add_grade_button" style="padding: 8px 14px;"><i class="fas fa-plus-circle"></i> Add</button>
                    </div>
                   
                    <div class="col-lg-3">
                      <lable class="from_one1">Grade Point :</lable>
                      <div class="form-group">
                        <input class="form-control positive-numeric-only" id="id-blah1" min="0" name="grade_point" type="number" value="" placeholder="Grade Point">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <lable class="from_one1">Remark</lable>
                      <div class="form-group">
                        <textarea class="form-control" placeholder="Remark"></textarea>
                      </div>
                    </div>
                  
                    <div id="sectiontable-body" class="row seprateclass"  >
                      <input type="hidden" id="hide" value="0" />
                      
                      <div class="input_grade_wrap">
                      </div>
                    </div>
                    <!--  Submit button -->
                    <div class="clearfix"></div>
                 
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary" title="Save">Save</button>
                      </div>
                      <div class="col-lg-1">
                        <button class="btn btn-raised btn-primary" title="Cancel">Cancel
                        </button>
                      </div>
                    </div>
                  </form>
                
            
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
@endsection


<script type="text/javascript">
  $(document).ready(function() {
        $('.select2').select2();
        $('.selectpicker').selectpicker('refresh');
    });
</script>