<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'competitions';
    protected $primaryKey = 'competition_id';
    public function up()
    {
        if (!Schema::hasTable('competitions')) { 
            Schema::create('competitions', function (Blueprint $table) {
                $table->increments('competition_id')->unsigned();
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->string('competition_name', 255)->nullable();
                $table->text('competition_description')->nullable();
                $table->date('competition_date')->nullable();
                $table->tinyInteger('competition_level')->default(0)->comment = '0=School,1=Class';
                $table->text('competition_class_ids')->nullable();
                $table->tinyInteger('competition_issue_participation_certificate')->default(0)->comment = '0=No,1=Yes';
                $table->tinyInteger('competition_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('competitions', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('competitions', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('competitions', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competitions');
    }
}
