<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table      = 'students';
    protected $primaryKey = 'student_id';

    // For Student Parent
    public function getParent()
    {
        return $this->belongsTo('App\Model\Student\StudentParent','student_parent_id');
    }

    // For student Academic Info
    public function getStudentAcademic()
    {
        return $this->hasOne('App\Model\Student\StudentAcademic','student_id');
    }

    // For Student Health
    public function getStudentHealth()
    {
        return $this->hasOne('App\Model\Student\StudentHealth','student_id');
    }

    // For Caste
    public function getStudentCaste()
    {
        return $this->belongsTo('App\Model\Caste\Caste','caste_id');
    }

    // For Religion
    public function getStudentReligion()
    {
        return $this->belongsTo('App\Model\Religion\Religion','religion_id');
    }

    // For Nationality
    public function getStudentNationality()
    {
        return $this->belongsTo('App\Model\Nationality\Nationality','nationality_id');
    }

    // For Sibling Class 
    public function getSiblingClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'student_sibling_class_id');
    }

    public function documents()
    {
        return $this->hasMany('App\Model\Student\StudentDocument', 'student_id');
    }


}
