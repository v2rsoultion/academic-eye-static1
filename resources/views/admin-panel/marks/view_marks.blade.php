@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 14px 10px !important;
  }
  .nav-tabs>.nav-item>.nav-link {
    width: 100px;
    margin-right: 5px;
        border: 1px solid #ccc !important;
  }
  #tabingclass .nav-tabs>.nav-item>.nav-link {
    border-radius: 4px !important;
    padding: 8px 25px;
  }
   #tabingclass .nav-link:hover {
        background: #6572b8 !important;
    color: #fff !important;
}
#tabingclass .nav-link:active {
        background: #6572b8 !important;
    color: #fff !important;
}
.idi .nav-tabs>.nav-item>.nav-link.active {
  background: #6572b8 !important;
    color: #fff !important;
}
.modal-dialog {
  max-width: 700px !important;
}
.checkbox {
  float: left;
  margin-right: 20px;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Marks</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <a href="{!! url('admin-panel/examination/marks/add') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/examination') !!}">Examination And Report Cards</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/examination') !!}">Marks</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination/marks/view') !!}">View Marks</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 20px !important;">
                 
                     <form class="" action="" method=""  id="" style="width: 100%;">
                      <div class="row">
                         <div class="col-lg-3">
                         <div class="form-group">
                          <!-- <lable class="from_one1">Exam Name</lable> -->
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Exam</option>
                              <option value="1">Exam-1</option>
                              <option value="2">Exam-2</option>
                              <option value="3">Exam-3</option>
                              <option value="4">Exam-4</option>
                            </select>
                         </div>
                      </div>
                       <div class="col-lg-3">
                         <div class="form-group">
                          <!-- <lable class="from_one1">Class Name</lable> -->
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Class</option>
                              <option value="1">Class-1</option>
                              <option value="2">Class-2</option>
                              <option value="3">Class-3</option>
                              <option value="4">Class-4</option>
                            </select>
                         </div>
                      </div>
                       <div class="col-lg-3">
                         <div class="form-group">
                          <!-- <lable class="from_one1">Section</lable> -->
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Section</option>
                              <option value="1">A</option>
                              <option value="2">B</option>
                              <option value="3">C</option>
                              <option value="4">D</option>
                            </select>
                         </div>
                      </div>
                     <div class="col-lg-1">
                      <button type="submit" class="btn btn-raised btn-primary " title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Clear
                      </button>
                    </div>
                    </div>
                   </form>
                <hr style="width: 100%">
                    <!--  DataTable for view Records  -->
                     <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                      <thead>
                        <tr>
                          <th>S No</th>
                          <th style="text-align: center;">Subject Name</th>
                          <th style="text-align: center;">Total Students</th>
                          <th style="text-align: center;">Remaining Student</th>
                          <th style="text-align: center;">Absent Student</th>
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 10; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td style="text-align: center;">Subject-<?php echo $counter; ?></td>
                          <td style="text-align: center;">40</td>
                          <td  style="text-align: center;">20</td>
                          <td  style="text-align: center;">20</td>
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
@endsection


<!-- Action Model  -->
<div class="modal fade" id="viewSubjectsModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">List of Subjects</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                      <thead>
                        <tr>
                          <th>S No</th>
                          <th>Criteria Name </th>
                          <th>Maximum Marks</th>
                          <th>Passing Marks</th>
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i <5 ; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td>Criteria-<?php echo $counter; ?></td>
                         <td>100</td>
                         <td>40</td>
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div>
                
      </div>
    </div>
  </div>
</div>