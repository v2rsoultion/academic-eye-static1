﻿
<?php $__env->startSection('content'); ?>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2> Request</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Transport</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Request</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/transport/one-way-transport/request'); ?>">Add </a></li>
                </ul>                
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
            
                            <div class="body form-gap">
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="level">
                                                <option value="">Vehicle</option>
                                                <option value="1">Mahindra NuvoSport</option>
                                                <option value="2">Mahindra Bolero</option>
                                                <option value="3">KUV100 NXT</option>
                                                <option value="4">Mahindra Bolero Power+</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="level">
                                                <option value="">Class</option>
                                                <option value="VII">XII</option>
                                                <option value="X">XI</option>
                                                <option value="X">X</option>
                                                <option value="X">VIII</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="level">
                                                <option value="">Section</option>
                                                <option value="VII">A</option>
                                                <option value="X">B</option>
                                                <option value="X">C</option>
                                                <option value="X">D</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12">
                                            <div class="input-group">
                                                <input type="text" class="form-control" value="" placeholder="Enroll No">
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12">
                                            <div class="input-group">
                                                <input type="text" class="form-control" value="" placeholder="Name">
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <button class="btn btn-raised btn-primary ">Search</button>
                                        </div>
                                        <div class="col-md-1 col-lg-1 col-sm-12">
                                            <button class="btn btn-raised btn-primary ">Clear</button>
                                        </div>
                                    </div>
                                <!-- </div> -->
                                <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Enroll</th>
                                               <!--  <th>Student Photo</th> -->
                                                <th style="width: 250px;">Student Name</th>
                                                <th>Trip</th>
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>11010</td>
                                                <td> <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%"> Ankit Dave</td>
                                                <td class="trip1">K.V School, Jaipur - St.Peter Private School, Jaipur</td>
                                                 <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/transport/one-way-transport/request'); ?>">Add Request</a></li> 
                                                </ul> </div></td> 
                                            </tr> 
                                            <tr>
                                                <td>2</td>
                                                <td>11010</td>
                                                <td> <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%"> Ankit Dave</td>
                                                <td class="trip1">K.V School, Jaipur - St.Peter Private School, Jaipur</td>
                                                <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/transport/one-way-transport/request'); ?>">Add Request</a></li> 
                                                </ul> </div></td> 
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>11010</td>
                                                <td> <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%"> Ankit Dave</td>
                                                <td class="trip1">K.V School, Jaipur - St.Peter Private School, Jaipur</td>
                                                <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/transport/one-way-transport/request'); ?>">Add Request</a></li> 
                                                </ul> </div></td>  
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>11010</td>
                                                <td> <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%"> Ankit Dave</td>
                                                <td class="trip1">K.V School, Jaipur - St.Peter Private School, Jaipur</td>
                                                <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/transport/one-way-transport/request'); ?>">Add Request</a></li> 
                                                </ul> </div></td> 
                                            </tr>                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>