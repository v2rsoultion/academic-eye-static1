@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Marks Criteria</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/examination') !!}">Examination And Report Cards</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/examination') !!}">Marks Criteria</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination/grade-scheme/add-grade-scheme') !!}">Add</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <!-- <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
                </div> -->
              <div class="body form-gap">
                <!-- <div class="row tabless" > -->
                  <div class="headingcommon  col-lg-12" style="padding-bottom: 0px !important">Add Marks Criteria :-</div>
                  <form class="" action="" method="" id="searchpanel" style="width: 100%;">
                    <div class="row" >
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Criteria Name </lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Criteria Name">
                        </div>
                      </div>
                        <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Maximum Marks</lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Maximum Marks">
                        </div>
                      </div>
                        <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Passing Marks </lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Passing Marks ">
                        </div>
                      </div>
                      <div class="col-lg-1 ">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
                        </button>
                      </div>
                      <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel</button>
                      </div> 
                    
                    </div>
                  </form>

                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <hr>
                  <div class="headingcommon  col-lg-12" style="padding-bottom: 0px !important">Search By :-</div>
                  <form class="" action="" method="" id="searchpanel" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Criteria Name</lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Criteria Name">
                        </div>
                      </div>
                  
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                  <hr>
                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                   <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Criteria Name</th>
                        <th>Maximum Marks</th>
                        <th>Passing Marks </th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $count = 1; for ($i=0; $i < 15; $i++) {  ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td> Criteria 100</td>
                        <td> 450</td>
                        <td>250</td>
                        <td>
                           <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <?php $count++; } ?>
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
@endsection