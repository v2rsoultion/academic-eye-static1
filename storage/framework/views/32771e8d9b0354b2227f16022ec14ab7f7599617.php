﻿
<?php $__env->startSection('content'); ?>
<style type="text/css">
    .card .body .table td, .cshelf1 {
        width: 100px !important;
    }
    .table-responsive {
      overflow-x: visible !important; 
    }
</style>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>View Register Student</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/hostel'); ?>"><?php echo trans('language.menu_hostel'); ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/hostel'); ?>">Register Student</a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/hostel/register-student/view'); ?>">View</a></li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">
                                <!-- <div class="container-fluid"> -->
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="class_name">
                                                <option value="">Class</option>
                                                <option value="VII">XII</option>
                                                <option value="X">VIII</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="Class_section">
                                                <option value="">Section</option>
                                                <option value="VII">A</option>
                                                <option value="X">B</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12">
                                            <div class="input-group ">
                                                <input type="text" class="form-control" value="" placeholder="Enroll No">
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12">
                                            <div class="input-group ">
                                                <input type="text" class="form-control" value="" placeholder="Student Name">
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <button class="btn btn-raised btn-primary">Search</button>
                                        </div>
                                        <div class="col-md-1">
                                            <button class="btn btn-raised btn-primary">Clear</button>
                                        </div>
                                    </div>
                                <!-- </div> -->
                                <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Student Name</th>
                                                <th>Father Name</th>
                                                <th>Class - section</th>
                                                <th style="width: 180px;">Status</th>
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                               <td>
                                                  <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%"> <a href="<?php echo e(url('/admin-panel/student/student-profile')); ?>" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>Mahesh Kumar</td>
                                                <td>XII-A</td>
                                                 <td class="text-success">Registered </td>
                                                <td>
                                                <div class="dropdown">
                                                  <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                  <i class="zmdi zmdi-label"></i>
                                                    <span class="zmdi zmdi-caret-down"></span>
                                                  </button>
                                                  <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a href="#" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#exampleModalCenter" style="background: transparent !important;">Register</a>
                                                    </li>
                                                  </ul>
                                                </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                               <td>
                                                  <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">  <a href="<?php echo e(url('/admin-panel/student/student-profile')); ?>" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>Mahesh Kumar</td>
                                                <td>XII-A</td>
                                                <td class="text-success">Registered </td>
                                                <td>
                                                <div class="dropdown">
                                                  <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                  <i class="zmdi zmdi-label"></i>
                                                    <span class="zmdi zmdi-caret-down"></span>
                                                  </button>
                                                  <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li>
                                                       <a href="#" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#exampleModalCenter" style="background: transparent !important;">Register</a>
                                                    </li>
                                                  </ul>
                                                </div>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td>3</td>
                                               <td>
                                                  <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">  <a href="<?php echo e(url('/admin-panel/student/student-profile')); ?>" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>Mahesh Kumar</td>
                                                <td>XII-A</td>
                                                <td class="text-danger">NA </td>
                                                <td>
                                                <div class="dropdown">
                                                  <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                  <i class="zmdi zmdi-label"></i>
                                                    <span class="zmdi zmdi-caret-down"></span>
                                                  </button>
                                                  <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a href="#" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#exampleModalCenter" style="background: transparent !important;">Register</a>
                                                    </li>
                                                  </ul>
                                                </div>
                                                </td>
                                            </tr>   
                                             <tr>
                                                <td>4</td>
                                               <td>
                                                  <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">  <a href="<?php echo e(url('/admin-panel/student/student-profile')); ?>" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>Mahesh Kumar</td>
                                                <td>XII-A</td>
                                                <td class="text-success">Registered </td>
                                                <td>
                                                <div class="dropdown">
                                                  <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                  <i class="zmdi zmdi-label"></i>
                                                    <span class="zmdi zmdi-caret-down"></span>
                                                  </button>
                                                  <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li>
                                                       <a href="#" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#exampleModalCenter" style="background: transparent !important;">Register</a>
                                                    </li>
                                                  </ul>
                                                </div>
                                                </td>
                                            </tr>                                   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>


<!-- Model Data -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row tabless">
         <p style="margin-left: 20px;">Please Collect fees for registration - Still want to Register</p>
          
        </div>
        <div class="col-lg-12">
          <button type="button" class="btn btn-raised btn-primary float-right">No</button>
          <button type="button" class="btn btn-raised btn-primary float-right" style="margin-right: 10px;">Yes</button>
          
        </div>
      </div>
    </div>
  </div>
</div>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>