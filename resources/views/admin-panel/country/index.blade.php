@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_country') !!}</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <a href="{!! url('admin-panel/country/add-country') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.menu_configuration') !!}</li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.country') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="">{!! trans('language.view_country') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">

                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">

                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        {{$errors->first()}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('country_name', old('country_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.country_name'), 'id' => 'country_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="country-table" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>{{trans('language.country_name')}}</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#country-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/country/data')}}',
                data: function (d) {
                    d.country_name = $('input[name="country_name"]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'country_name', name: 'country_name'},
                {data: 'action', name: 'action'}
            ],
             columnDefs: [
                {
                    "targets": 2, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

</script>
@endsection




