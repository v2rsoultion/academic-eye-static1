@extends('admin-panel.layout.header')
@section('content')
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Assign Driver/ Conductor</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/transport') !!}">Transport</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/transport/assign-driver-conductor/manage-driver-conductor') !!}">Assign Driver/ Conductor</a></li>
                </ul>                
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="headingcommon  col-lg-12" style="padding-bottom: 0px !important">Add Assign Driver / Conductor :-</div>
                            <div class="body form-gap">
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-12 ">
                                            <select class="form-control show-tick select_form1" name="class_name">
                                                <option value="">Select Vehicle</option>
                                                <option value="VII">Mahindra NuvoSport</option>
                                                <option value="X">KUV100 NXT</option>
                                                <option value="X">Mahindra Bolero</option>
                                                <option value="X">Mahindra Bolero Power+</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 ">     
                                         <select class="form-control show-tick select_form1" name="Class_section">
                                                <option value="">Select Driver</option>
                                                <option value="VII">Dev Shah</option>
                                                <option value="X">Deepack Shah</option>
                                                <option value="X">Mahesh Kumar</option>
                                                <option value="X">Raj</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 ">      
                                         <select class="form-control show-tick select_form1" name="Class_section">
                                                <option value="">Select Conductor</option>
                                                <option value="VII">Raj Pal</option>
                                                <option value="X">Ajay pal</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-lg-1">
                                            <button class="btn btn-raised  btn-primary" >Save</button>
                                        </div>
                                        <div class="col-lg-1">
                                            <button class="btn btn-raised  btn-primary ">Cancel</button>
                                        </div>
    
                                                                    </div>
                        
                                    <hr>
                                    <div class="row">
                                         <div class="headingcommon  col-lg-12" style="padding-bottom: 2px !important">Search By :-</div>
                                         <div class="col-lg-2 col-md-2 col-sm-12 ">
                                            <select class="form-control show-tick select_form1" name="class_name">
                                                <option value="">Select Vehicle</option>
                                                <option value="VII">Mahindra NuvoSport</option>
                                                <option value="X">KUV100 NXT</option>
                                                <option value="X">Mahindra Bolero</option>
                                                <option value="X">Mahindra Bolero Power+</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 ">     
                                         <select class="form-control show-tick select_form1" name="Class_section">
                                                <option value="">Select Driver</option>
                                                <option value="VII">Dev Shah</option>
                                                <option value="X">Deepack Shah</option>
                                                <option value="X">Mahesh Kumar</option>
                                                <option value="X">Raj</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 ">      
                                         <select class="form-control show-tick select_form1" name="Class_section">
                                                <option value="">Select Conductor</option>
                                                <option value="VII">Raj Pal</option>
                                                <option value="X">Ajay pal</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-lg-1">
                                            <button class="btn btn-raised  btn-primary" >Search</button>
                                        </div>
                                        <div class="col-lg-1">
                                            <button class="btn btn-raised  btn-primary ">Clear</button>
                                        </div>
                                    </div>
                                    <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Vehicle</th>
                                                <th>Driver</th>
                                                <th>Conductor</th>
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Mahindra Bolero</td>
                                                <td>Deepack Shah</td>
                                                <td>Ajay pal</td>
                                                  <td> 
                                               <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Mahindra Bolero</td>
                                                <td>Deepack Shah</td>
                                                <td>Ajay pal</td>
                                                 <td> 
                                               <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Mahindra Bolero</td>
                                                <td>Deepack Shah</td>
                                                <td>Ajay pal</td>
                                                  <td> 
                                               <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Mahindra Bolero</td>
                                                <td>Deepack Shah</td>
                                                <td>Ajay pal</td>
                                                  <td> 
                                               <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td> 
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection

<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            block_name: {
                validators: {
                        stringLength: {
                        min: 4,
                    },
                        notEmpty: {
                        message: 'Please fill required block name'
                    }
                }
            },
            hostel_name: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required hostel name'
                    }
                }
            },
            
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>
