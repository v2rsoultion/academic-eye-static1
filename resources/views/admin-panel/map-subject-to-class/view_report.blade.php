@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.map_subject') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/class-subject-mapping/view-subject-class-mapping') !!}">{!! trans('language.map_subject_to_class') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.map_subject') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                                <div class="body">
                                    <span class="classname_padding">Class Name : <strong>{{$class_name}}</strong></span>
                                    {!! Form::hidden('class_name',$class_name,array('readonly' => 'true','id'=>"mainclassname")) !!}
                                    {!! Form::hidden('class_id',$class_id,array('readonly' => 'true')) !!}
                                    <div class="table-responsive">
                                        <table class="table m-b-0 c_list" id="view-report-table" style="width:100%">
                                        {{ csrf_field() }}
                                            <thead>
                                                <tr>
                                                    <th>{{trans('language.s_no')}}</th>
                                                    <th>{{trans('language.ms_subject_name')}}</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var class_details = $('#mainclassname').val();
        
        var table = $('#view-report-table').DataTable({
            dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            buttons: [
                {
                    extend: 'csvHtml5',
                    messageTop: 'Class : ' + class_details,
                    customize: function(doc){
                        return "Class : \t" + class_details + "\n\n" + doc;
                        }
                },
                {
                extend: 'print',
                messageTop: 'Class : ' + class_details,
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                    $(win.document.body).find('h1').css('font-size', '20px');                        
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                    }
                }
            ],
            ajax: {
                url: "{{url('admin-panel/class-subject-mapping/view-report-data')}}",
                data: function (d) {
                    d.class_id = $('input[name=class_id]').val();
                }
            },
            // ajax: "{{url('admin-panel/class-subject-mapping/view-report-data')}}",
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'subjects', name: 'subjects'},
            ],
             columnDefs: [
                {
                "targets": 0,
                "orderable": false
                },
                {
                    targets: [ 0, 1],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
    });

</script>
@endsection




