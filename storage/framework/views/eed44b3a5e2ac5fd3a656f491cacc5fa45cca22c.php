<?php $__env->startSection('content'); ?>
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>My Attendance</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
               
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/student/view-attendance'); ?>">My Attendance</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           <div class="body form-gap ">
                               <form>
                                    <div class="row clearfix">
                                      
                                      <div class="col-lg-3">
                                        <div class="form-group">
                                          <!-- <lable class="from_one1" for="name">Date</lable> -->
                                          <input type="text" name="name" id="date" class="form-control" placeholder="Date">
                                        </div>
                                      </div>
                                        <div class="col-lg-1 col-md-1">
                                          <button type="submit" class="btn btn-raised btn-primary" title="Search">Search
                                          </button>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <button type="submit" class="btn btn-raised btn-primary" title="Clear">Clear
                                          </button>
                                        </div>
                                    </div>
                                </form>
                            
                                <div class="table-responsive">
                                   
                                <table class="table m-b-0 c_list" id="#" style="width:100%">
                                <?php echo e(csrf_field()); ?>

                                <thead>
                                    <tr>
                                        <th><?php echo e(trans('language.s_no')); ?></th>
                                        <th>Date</th>
                                        <th>Attendance</th>
                                        <!-- <th>Action </th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                          <td>1</td>
                                          <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                               <!--  <td>
                                                  <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff-menu/my-class/view-student-attendance-profile'); ?>">View More</a></li> 
                                                </ul> </div>

                                                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="<?php echo e(url('admin-panel/staff-menu/my-class/edit-student-attendance')); ?>"><i class="zmdi zmdi-edit"></i></a></div>
                                              </td> -->
                                        </tr>
                                          <tr>
                                          <td>2</td>
                                         <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                            
                                        </tr>
                                        <tr>
                                          <td>3</td>
                                          <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                        </tr>
                                        <tr>
                                          <td>4</td>
                                          <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                        </tr>
                                       <tr>
                                          <td>5</td>
                                         <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                        </tr>
                                          <tr>
                                          <td>6</td>
                                          <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                        </tr>
                                          <tr>
                                          <td>7</td>
                                          <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                        </tr>
                                          <tr>
                                          <td>8</td>
                                          <td>20 July 2018</td>
                        <td><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                                        </tr>
                                </tbody>
                            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#staff-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '<?php echo e(url('admin-panel/staff/data')); ?>',
                data: function (d) {
                    d.class_id = $('input[name="name"]').val();
                    d.section_id = $('select[name="designation_id"]').val();
                }
            },
            //ajax: '<?php echo e(url('admin-panel/class/data')); ?>',
           
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'staff_name', name: 'staff_name'},
                {data: 'staff_designation_name', name: 'staff_designation_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

</script>


<div class="modal fade" id="attendanceModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ankit Dave </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <table class="table m-b-0 c_list">
                    <thead>
                      <tr>
                        <th class="text-center">S no</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Attendance</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">20 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                      </tr>
                     <tr>
                        <td class="text-center">2</td>
                        <td class="text-center">21 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: red;"></i> (Absent) </td>
                      </tr>
                      <tr>
                        <td class="text-center">3</td>
                        <td class="text-center">22 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                      </tr>
                      <tr>
                        <td class="text-center">4</td>
                        <td class="text-center">23 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                      </tr>
                      <tr>
                        <td class="text-center">5</td>
                        <td class="text-center">24 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                      </tr>
                      <tr>
                        <td class="text-center">6</td>
                        <td class="text-center">25 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: red;"></i> (Absent) </td>
                      </tr>
                      <tr>
                        <td class="text-center">7</td>
                        <td class="text-center">26 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: red;"></i> (Absent) </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>