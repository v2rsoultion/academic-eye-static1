
<?php $__env->startSection('content'); ?>
<style type="text/css">
  .tabless table tr td .opendiv ul li{
  padding: 0px 0px !important;
  }
  .footable .dropdown-menu>li>a{
  padding: 10px 15px !important;
  }
  .footable .dropdown-menu{
  padding: 0px 0px !important; 
  }
  #btnCu{
  background: transparent;
  height: auto;
  }
  #btnCu i{
  font-size: 23px;
  }
  #sendReply h5{
  font-size: 14px;
  }
  #viewReply h5{
    font-size: 14px;
  }
  #viewReply table tr td{
    padding: 10px 10px;
  }
  .card .body .table td, .cshelf1{
    width: auto !important;
  }
  .modal-dialog {
    max-width: 500px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Responses</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('admin-panel/menu/task-manager')); ?>"><?php echo trans('language.menu_task_manager'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('admin-panel/task-manager/view-task')); ?>">View Task</a></li>
          <li class="breadcrumb-item"><a href="<?php echo e(url('admin-panel/task-manager/view-task/view-responses')); ?>">View Responses</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card gap-m-bottom">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
              </div>
              <div class="body form-gap1">
                <form class="" action="" method="" id="manage_responses" style="width: 100%;">
                  <div class="row" >
                    <div class="col-lg-3">
                      <div class="form-group">
                        <lable class="from_one1">User Name</lable>
                        <input type="text" name="user_name" id="user_name" class="form-control" placeholder="User Name">
                      </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                        <lable class="from_one1">Response File</lable>
                        <input type="File" name="" id="">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <lable class="from_one1">Response Text</lable>
                        <textarea name="user_name" id="user_name" class="form-control" placeholder="Response Text"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-1">
                      <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <a href="<?php echo e(url('admin-panel/task-manager/view-task/view-responses')); ?>" class="btn btn-raised btn-primary" title="Cancel">Cancel
                      </a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="container-fluid">
    <div class="card">
      <div class="body form-gap1">
        <div class="headingcommon col-lg-12">Search By :-</div>
          <form class="" action="" method="" id="" style="width: 100%;">
            <div class="row">
              <div class="col-lg-3">
                <div class="form-group">
                  <lable class="from_one1" for="name">User Name</lable>
                  <input type="text" name="name" id="name" class="form-control" placeholder="User Name">
                </div>
              </div>
              
              <div class="col-lg-1 ">
                <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Submit">Search
                </button>
              </div>
              <div class="col-lg-1 ">
                <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear">Clear
                </button>
              </div>
            </div>
          </form>
          <!--  DataTable for view Records  -->
          <table class="table m-b-0 c_list">
            <thead>
              <tr>
                <th>S No</th>
                <th>User Name </th>
                <th>Response </th>
                <th>Response File</th>
                <th>Status </th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $cou = 1; for ($i=0; $i < 10; $i++) {  ?> 
              <tr>
                <td><?php echo $cou; ?></td>
                <td>Narendra Kumar</td>
                <td>Lorem Ipsum is simply dumm... </td>
                <td>
                  <a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" title="View File" class="text-primary" style="color: #0275d8!important;" target="_blank">View File</a>
                </td>
                <td>
                  On Hold
                </td>
                <!-- For Completed -->
                <!-- <td class="text-success">Completed</td> -->  
                <td>
                
                  <div class="dropdown">
                    <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="zmdi zmdi-label"></i>
                    <span class="zmdi zmdi-caret-down"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                      <li>
                        <a href="" title="Send Reply" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#sendReply">Send Reply</a>
                      </li>
                      <li>
                        <a href="" title="View Reply" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#viewReply">View Reply</a>
                        
                      </li>
                    </ul>
                  </div>
                <!--   <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button> -->
                </td>
              </tr>
              <?php $cou++; } ?> 
            </tbody>
          </table>
              </div>
            </div>
          </div>
        
</section>
<!-- Action Model  -->
<div class="modal fade" id="sendReply" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="">SEND REPLY</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row tabless" >
          <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
          <div class="col-lg-12">
            <div class="form-group">
              <lable class="from_one1" for="Amount">Date</lable>
              <input type="text" name="taskDate" id="taskDate" class="form-control dateClass" placeholder="Date">
            </div>
          </div>
          <div class="col-sm-12 text_area_desc">
            <lable class="from_one1">Add Reply</lable>
            <div class="form-group">
              <textarea rows="4" cols="30" name="" class="form-control no-resize" placeholder="Reply"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>

<!--  View reply popup -->

<div class="modal fade" id="viewReply" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 700px !important;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="">VIEW REPLY</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row tabless" style="margin: 0px;" >
          <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
         <table class="table m-b-0 c_list" style="margin: 0px ;">
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Date </th>
                        <th>Reply</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                     <?php $cou = 1; for ($i=0; $i < 5; $i++) {  ?> 
                      <tr>
                        <td><?php echo $cou; ?></td>
                        <td>20-July-2018</td>
                        <td>
                          Lorem Ipsum is simply dummy text of the printing... 
                        </td>
                      </tr>
                    <?php $cou++; } ?> 
                    </tbody>
                  </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Content end here  -->
<?php $__env->stopSection(); ?>
<script type="text/javascript">
  $('#dropbtn').on('click', 'dropbtnAction', function () {
      $(this).toggleClass('active');
  });
  
</script>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>