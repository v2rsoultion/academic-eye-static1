@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Add Arrears</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/payroll') !!}">Payroll</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/add-arrears') !!}">Add Arrears</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card" style="margin-bottom: 20px;">
               <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body" style="padding: 0px 20px 20px 20px;">
               
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Add Arrear :-</div> -->
                  <form class="" action="" id="manage_vendor" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1"> Name</lable>
                          <input type="text" name="arrear_name" id="" class="form-control" placeholder=" Name">
                        </div>
                         @if ($errors->has('arrear_name')) <p class="help-block">{{ $errors->first('arrear_name') }}</p> @endif
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">From Date</lable>
                          <input type="text" name="from_date" id="date" class="form-control" placeholder="Form Date">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">To Date</lable>
                          <input type="text" name="to_date" id="taskDate" class="form-control" placeholder="To Date">
                        </div>
                      </div>
                      
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Amount (Per Month)</lable>
                          <input type="text" name="amount" id="" class="form-control" placeholder="Amount">
                        </div>
                          @if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif  
                      </div> 

                     

                    </div>
                    <div class="row">
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Total Amount</lable>
                          <input type="text" name="total_amount" id="" class="form-control" placeholder="Total Amount">
                        </div>
                          @if ($errors->has('total_amount')) <p class="help-block">{{ $errors->first('total_amount') }}</p> @endif  
                      </div> 
                       <div class="col-lg-4">
                        <div class="form-group">
                          <lable class="from_one1">Round Off</lable>
                          <div class="radio" style="margin-top:6px !important;">
                           <input id="radio33" name="calculation_basic" type="radio">
                            <label for="radio33" class="document_staff"> Highest Rupee</label>
                            <input id="radio34" name="calculation_basic" type="radio">
                            <label for="radio34" class="document_staff"> Nearest Rupee</label>
                          </div>
                        </div>
                      </div>
                    </div>
                      <!-- <hr> -->
                      <div class="row">
                      <div class="col-lg-1 ">
                        <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
                        </button>
                      </div>
                      <div class="col-lg-1 ">
                        <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel
                        </button>
                      </div>
                    
                    </div>
                  </form>
                </div>
              </div>
                  <!-- <hr> -->
                  <div class="card">
                    <div class="body form-gap">
                  <div class="headingcommon  col-lg-12" style="margin-left: -13px">Search By :-</div>
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Arrear Name</lable>
                          <input type="text" name="arrear_name" id="" class="form-control" placeholder="Arrear Name">
                        </div>
                      </div>
                     
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                
                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <hr>
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Arrear Name</th>
                        <th>Date Range</th>
                        <th>Amount</th>
                        <th>Total Amount</th>
                        <th>Round Off</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Arrear - 1</td>
                        <td>10-11-2017 - 12-11-2018</td>
                        <td>20000</td>
                        <td>1000000</td>
                        <td>Nearest Rupee</td>
                        <td>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                          <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li> <a title="Manage Arrears" href="{{url('admin-panel/payroll/manage-arrears')}}">Manage Arrear</a></li> 
                            </ul> </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Arrear - 2</td>
                        <td>10-11-2016 - 11-11-2020</td>
                        <td>50000</td>
                        <td>2000000</td>
                        <td>Highest Rupee</td>
                        <td>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                          <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li> <a title="Manage Arrears" href="{{url('admin-panel/payroll/manage-arrears')}}">Manage Arrear</a></li> 
                            </ul> </div>
                        </td>
                      </tr>
                    
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->

<script type="text/javascript">
  function checkForAmount(val) {
        var x = document.getElementById("AmountMode");
        if(val == 0) {
            if (x.style.display === "none") {
                x.style.display = "none";
            } else {
                x.style.display = "none";
            }
        } else {
            if (x.style.display === "block") {
                x.style.display = "block";
            } else {
                x.style.display = "block";
            }
        }

        var y = document.getElementById("BasicPayMode");
        if(val == 1) {
            if (y.style.display === "none") {
                y.style.display = "none";
            } else {
                y.style.display = "none";
            }
        } else {
            if (y.style.display === "block") {
                y.style.display = "block";
            } else {
                y.style.display = "block";
            }
        }
    }
</script>

@endsection