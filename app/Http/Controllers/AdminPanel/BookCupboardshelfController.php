<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\BookCupboardshelf\BookCupboardshelf; // Model
use Yajra\Datatables\Datatables;

class BookCupboardshelfController extends Controller
{
    /**
     *  View page for CupBoard Shelf
     *  @Pratyush on 13 Aug 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_cupboard_shelf'),
            'redirect_url'  => url('admin-panel/cupboard-shelf/view-cupboard-shelf'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.cupboard-shelf.index')->with($data);
    }

    /**
     *  Add page for CupBoard Shelf
     *  @Pratyush on 13 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    					= [];
        $cupboard_shelf 			= [];
        $listData                   = [];
        $loginInfo 					= get_loggedin_user_data();
        $cupboard_arr				= get_all_cupbpard_data();
        $listData['arr_cubboard']  	= add_blank_option($cupboard_arr, 'Select CupBoard');
        
        if (!empty($id))
        {
            $decrypted_cupboard_shelf_id 	= get_decrypted_value($id, true);
            $cupboard_shelf      			= BookCupboardshelf::Find($decrypted_cupboard_shelf_id);
            if (!$cupboard_shelf)
            {
                return redirect('admin-panel/cupboard-shelf/add-cupboard-shelf')->withError('Title not found!');
            }
            $page_title             	= trans('language.edit_cupboard_shelf');
            $encrypted_title_id   		= get_encrypted_value($cupboard_shelf->title_id, true);
            $save_url               	= url('admin-panel/cupboard-shelf/save/' . $encrypted_title_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_cupboard_shelf');
            $save_url      = url('admin-panel/cupboard-shelf/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'cupboard_shelf' 	=> $cupboard_shelf,
            'listData'			=> $listData,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/cupboard-shelf/view-cupboard-shelf'),
        );
        return view('admin-panel.cupboard-shelf.add')->with($data);
    }

    /**
     *  Add and update Title's data
     *  @Pratyush on 20 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      				= get_loggedin_user_data();
        $decrypted_cupboard_shelf_id	= get_decrypted_value($id, true);
        if (!empty($id))
        {
            $cupboard_shelf = BookCupboardshelf::find($decrypted_cupboard_shelf_id);

            if (!$cupboard_shelf)
            {
                return redirect('/admin-panel/cupboard-shelf/add-cupboard-shelf/')->withError('CupBoard shelf not found!');
            }
            $previous_record = BookCupboardshelf::where('book_cupboardshelf_name', "like", "%{$request->get('cupboardshelf_name')}%")->where('book_cupboard_id',Input::get('cupboard_name'))->where('book_cupboardshelf_id','!=',$decrypted_cupboard_shelf_id)->count();
            if($previous_record != 0){
        		return redirect()->back()->withInput()->withErrors('CupBoard Shelf name with same cupboard already exist.');
        	}
            $success_msg = 'CupBoard shelf updated successfully!';
        }
        else
        {
        	$previous_record = BookCupboardshelf::where('book_cupboardshelf_name', "like", "%{$request->get('cupboardshelf_name')}%")->where('book_cupboard_id',Input::get('cupboard_name'))->count();
        	if($previous_record != 0){
        		return redirect()->back()->withInput()->withErrors('CupBoard Shelf name with same cupboard already exist.');
        	}

            $cupboard_shelf  = New BookCupboardshelf;
            $success_msg 	 = 'CupBoard shelf saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'cupboardshelf_name'   		 => 'required',
                'cupboard_name'   			 => 'required',
                'book_cupboard_capacity'   	 => 'required',
                'book_cupboard_shelf_detail' => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $cupboard_shelf->admin_id       			= $loginInfo['admin_id'];
                $cupboard_shelf->update_by      			= $loginInfo['admin_id'];
                $cupboard_shelf->school_id      			= $loginInfo['school_id'];
                $cupboard_shelf->book_cupboardshelf_name 	= Input::get('cupboardshelf_name');
                $cupboard_shelf->book_cupboard_id 			= Input::get('cupboard_name');
                $cupboard_shelf->book_cupboard_capacity 	= Input::get('book_cupboard_capacity');
                $cupboard_shelf->book_cupboard_shelf_detail	= Input::get('book_cupboard_shelf_detail');
                $cupboard_shelf->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/cupboard-shelf/view-cupboard-shelf')->withSuccess($success_msg);
    }

    /**
     *  Get CupBoard Shelf's Data for view page(Datatables)
     *  @Pratyush on 13 Aug 2018.
    **/
    public function anyData()
    {
        $loginInfo 			= get_loggedin_user_data();
        $cupboard_shelf		= BookCupboardshelf::where([['school_id', '=', $loginInfo['school_id']]])->with('getCupBoard')->orderBy('book_cupboardshelf_id', 'ASC')->get();
        
        return Datatables::of($cupboard_shelf)
        		->addColumn('details', function ($cupboard_shelf)
                {
                     return substr($cupboard_shelf->book_cupboard_shelf_detail,0,50)." ...";
                    
                })
        		->addColumn('cupboard_name', function ($cupboard_shelf)
                {
                    
                    return $cupboard_shelf['getCupBoard']->book_cupboard_name;
                })
        		->addColumn('action', function ($cupboard_shelf)
                {
                    $encrypted_cupboardshelf_id = get_encrypted_value($cupboard_shelf->book_cupboardshelf_id, true);
                    if($cupboard_shelf->book_cupboardshelf_status == 0) {
                        $status = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="cupboard-shelf-status/'.$status.'/' . $encrypted_cupboardshelf_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-cupboard-shelf/' . $encrypted_cupboardshelf_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-cupboard-shelf/' . $encrypted_cupboardshelf_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Destroy CupBoard Shelf's data
     *  @Pratyush on 13 Aug 2018.
    **/
    public function destroy($id)
    {
        $cupboard_shelf_id 	= get_decrypted_value($id, true);
        $cupboard_shelf		= BookCupboardshelf::find($cupboard_shelf_id);
        if ($cupboard_shelf)
        {
            $cupboard_shelf->delete();
            $success_msg = "CupBoard Shelf deleted successfully!";
            return redirect('admin-panel/cupboard-shelf/view-cupboard-shelf')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "CupBoard Shelf not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change CupBoard Shelf's status
     *  @Pratyush on 13 Aug 2018.
    **/
    public function changeStatus($status,$id)
    {
        $cupboard_shelf_id 	= get_decrypted_value($id, true);
        $cupboard_shelf		= BookCupboardshelf::find($cupboard_shelf_id);
        if($cupboard_shelf)
        {
            $cupboard_shelf->book_cupboardshelf_status  = $status;
            $cupboard_shelf->save();
            $success_msg = "CupBoard Shelf status updated!";
            return redirect('admin-panel/cupboard-shelf/view-cupboard-shelf')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "CupBoard Shelf not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
