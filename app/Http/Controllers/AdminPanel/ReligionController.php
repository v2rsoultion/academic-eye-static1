<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Religion\Religion; // Model
use Yajra\Datatables\Datatables;

class ReligionController extends Controller
{
    /**
     *  View page for Religion
     *  @Pratyush on 20 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_religion'),
            'redirect_url'  => url('admin-panel/religion/view-religions'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.religion.index')->with($data);
    }

    /**
     *  Add page for Religion
     *  @Pratyush on 20 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $religion 	    = [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_religion_id 	    = get_decrypted_value($id, true);
            $religion      			    = Religion::Find($decrypted_religion_id);
            if (!$religion)
            {
                return redirect('admin-panel/religion/add-religion')->withError('Religion not found!');
            }
            $page_title             	= trans('language.edit_religion');
            $encrypted_religion_id      = get_encrypted_value($religion->religion_id, true);
            $save_url               	= url('admin-panel/religion/save/' . $encrypted_religion_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_religion');
            $save_url      = url('admin-panel/religion/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'religion' 		    => $religion,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/religion/view-designations'),
        );
        return view('admin-panel.religion.add')->with($data);
    }

    /**
     *  Add and update Religion's data
     *  @Pratyush on 20 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_religion_id	    = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $religion = Religion::find($decrypted_religion_id);

            if (!$religion)
            {
                return redirect('/admin-panel/religion/add-religion')->withError('Religion not found!');
            }
            $success_msg = 'Religion updated successfully!';
        }
        else
        {
            $religion        = New religion;
            $success_msg     = 'Religion saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'religion_name'   => 'required|unique:religions,religion_name,' . $decrypted_religion_id . ',religion_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $religion->admin_id            	= $loginInfo['admin_id'];
                $religion->update_by           	= $loginInfo['admin_id'];
                $religion->religion_name 		= Input::get('religion_name');
                $religion->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/religion/view-religions')->withSuccess($success_msg);
    }

    /**
     *  Get Religion's Data for view page(Datatables)
     *  @Pratyush on 20 July 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $religion  		    = Religion::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('religion_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('religion_id', 'DESC')->get();

        return Datatables::of($religion)
        ->addColumn('action', function ($religion)
        {
            
            $encrypted_religion_id = get_encrypted_value($religion->religion_id, true);
            if($religion->religion_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="religion-status/'.$status.'/' . $encrypted_religion_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-religion/' . $encrypted_religion_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-religion/' . $encrypted_religion_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Religion's data
     *  @Pratyush on 20 July 2018.
    **/
    public function destroy($id)
    {
        $religion_id 		     = get_decrypted_value($id, true);
        $religion 		  	     = Religion::find($religion_id);
        if ($religion)
        {
            $religion->delete();
            $success_msg = "Religion deleted successfully!";
            return redirect('admin-panel/religion/view-religions')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Religion not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Religion's status
     *  @Pratyush on 20 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $religion_id             = get_decrypted_value($id, true);
        $religion                = Religion::find($religion_id);
        if ($religion)
        {
            $religion->religion_status  = $status;
            $religion->save();
            $success_msg = "Religion status updated!";
            return redirect('admin-panel/religion/view-religions')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Religion not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
