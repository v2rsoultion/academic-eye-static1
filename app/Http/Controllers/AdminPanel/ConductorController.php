<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConductorController extends Controller
{
    /**
     *  View page for Conductor
     *  @Khushbu on 28 Sept 2018   
    **/
    public function index()
    {
        $loginInfo 		    = get_loggedin_user_data();
        
        $data 				= array(
            'page_title'    => trans('language.view_conductor'),
            'redirect_url'  => url('admin-panel/transport/conductor/view-conductor'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.conductor.view-conductor')->with($data);
    }

    /**
     *  Add page for Vehicle Conductor
     *  @Khushbu on 28 Sept 2018   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        // $job        = [];
        if (!empty($id))
        {
            $page_title             = trans('language.edit_conductor');
            $save_url               = url('admin-panel/transport/conductor/save/' . $decrypted_job_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_conductor');
            $save_url      = url('admin-panel/transport/conductor/save');
            $submit_button = 'Save';
        }

        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/conductor/view-conductor'),
        );
        return view('admin-panel.conductor.add-conductor')->with($data);
    }
}
