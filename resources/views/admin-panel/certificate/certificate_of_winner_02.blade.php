<!DOCTYPE html>
<html>
<head>
	<title>Winner Certificate</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">

	<style type="text/css">
	body {
		width: 720px;
		margin: auto;
		margin-top: 20px;
		font-size: 30px;
	}

	.content_size {
		font-size: 17px;
		font-style: italic;
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		border-radius: 10px;
		border: 5px solid #1f2f60;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 20px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 0px;
	}
	.rl {
		padding: 0px;
	}
	table{
		height: 40px;

	}
	</style>
	<!-- <style type="text/css">
		body {
  background-color: #333;
  font-family: 'Luckiest Guy', cursive;
  font-size: 30px;
}

path {
  fill: transparent;
}

text {
  fill: #FF9800;
}
	</style> -->
	<!-- M73.2,148.6c4-6.1,65.5-96.8,178.6-95.6c111.3,1.2,170.8,90.3,175.1,97 -->
	<!-- edited: M158.2,138.6c4-6.1,67.5-96.8,204.6-95.6c111.3,1.2,170.8,90.3,175.1,97 -->
</head>
<body>
	<!-- <div style="border-radius: 10px;margin: 10px;
		border: 5px solid #1f2f60;"> -->
	<div class="header">
	<!-- <svg viewBox="0 0 700 200">
    	<path id="curve" d="M158.2,138.6c4-6.1,67.5-96.8,204.6-95.6c111.3,1.2,170.8,90.3,175.1,97" />
    		<text width="100">
      			<textPath xlink:href="#curve">APEX SR. SECONDARY SCHOOL 
      			</textPath>
    		</text>
  	</svg> -->

  	<!-- <svg viewBox="0 0 700 200" style="font-size:14px;margin-top: -130px;">
    	<path id="curve" d="M193.2,138.6c4-6.1,67.5-96.8,194.6-95.6c111.3,1.2,170.8,90.3,175.1,97" />
    		<text width="100">
      			<textPath xlink:href="#curve">An English medium Co-education sr. secondary school
      			</textPath>
    		</text>
  	</svg> -->
  		<h4 class="align header_gap">APEX SENIOR SECONDARY SCHOOL</h4>
  		<h5 class="align" style="font-size:16px;margin-top:10px;">An English medium Co-education Sr. Secondary School</h5>
		<div class="align" style="margin: 15px 0px;">
		<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="60px"></div>
		<div class="align" style="width: 400px;margin:auto; font-size:25px;"><b>Inter - School English Debate</b></div>


		<p class="align content_size"></p>
		<table>
			<tr>
				<td style ="width: 268px !important;" class="align_right content_size"> This is to certify that Master/Miss &nbsp;&nbsp;</td> 
				<td style="width: 400px" class="text"> Khushbu Vaishnav</td>
			</tr>
		</table>
		<table>
			<tr>
				<td style ="width: 180px !important;" class="align_right content_size">son/daughter of Shri  &nbsp;&nbsp;</td>
				<td style="width: 464px" class="text"> Nemi Chand Vaishnav</td>
				<td style ="width: 23px !important;" class="align content_size">of</td>
			</tr>
		</table>
		<table>
			<tr>
				<td style ="width: 74px !important;" class="align_right content_size">class  &nbsp;&nbsp;</td>
				<td style="width: 90px" class="align text"> XI</td>
				<td style ="width: 30px !important;" class="align content_size">of</td>
				<td style="width: 413px" class="text"> Apex Senior Secondary</td>
				<td style ="width: 40px !important;" class="align content_size">School</td>
			</tr>
		</table>
		<table>
			<tr>
				<td style ="width: 72px !important;" class="align_right content_size"> stood  &nbsp;</td> 
				<td style="width: 90px" class="align text"> First </td>
				<td style ="width: 63px !important;" class="align_right content_size"> in the  &nbsp;&nbsp;</td> 
				<td style="width: 435px" class="text"> Inter - School English Debate</td>
			</tr>
		</table>
		<!-- <table>
			<tr>
				<td  style ="width: 80px !important; " class="align_right content_size"> On the &nbsp;&nbsp;&nbsp;</td> 
				<td style="width: 120px" class="text"> 24</td>
				<td  style ="width: 80px !important; " class="align_right content_size"> Day of &nbsp;&nbsp;&nbsp;</td> 
				<td style="width: 160px" class="text"> November</td>
				<td  style ="width: 80px !important; " class="align_right content_size"> In the Year &nbsp;&nbsp;&nbsp;</td> 
				<td style="width: 117px" class="text"> 2018</td>
			</tr>
		</table> -->
		<!-- <table>
			<tr>
				<td  style ="width: 78px !important; " class="align_right content_size"> At: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> 
				<td style="width: 602px" class="text"> Apex Senior Secondary School</td>
				
			</tr>
		</table>
			 -->
		<!-- <div class="row col-lg-12">
			<div class="col-lg-2 rr">
				<p class="left_gap content_size">At:</p>
			</div>
			<div class="col-lg-10 rl">
				<p class="text">Apex Senior Secondary School</p>
			</div>
		</div>  -->

		<!-- <p class="align content_size">Participated in <b style="text-decoration: underline; font-style: normal;">Cultural Activity</b></p>
 -->
		<!-- <p class="content_size align_right sign">Signed,</p>
		<p class="content_size align_right sign">_______________________</p> -->
		<br><br><br>
		<table>
			<tr>
				<td style="width: 145px" class="align_right content_size sign">Date: 24-10-2018</td>
				<td style="width: 420px" class="align content_size sign"> </td>
				<td style="width: 145px" class="align content_size sign">Signature</td>
			</tr>
	
				
		</table>
	</div>
<!-- </div> -->
</body>
</html>

<script type="text/javascript">
	window.print();
</script>