<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Subject\Subject;
use App\Model\Classes\Classes;

use Yajra\Datatables\Datatables;

class SubjectController extends Controller
{
    /**
     *  View page for subject
     *  @Shree on 21 July 2018
    **/
    public function index()
    {
        $loginInfo                      = get_loggedin_user_data();
        $subject = [];
        $arr_medium                     = \Config::get('custom.medium_type');
        $subject['arr_medium']          = add_blank_option($arr_medium, 'Select Medium');
        $data = array(
            'page_title'    => trans('language.view_subject'),
            'redirect_url'  => url('admin-panel/subject/view-subjects'),
            'login_info'    => $loginInfo,
            'subject'       => $subject
        );
        return view('admin-panel.subject.index')->with($data);
    }

     /**
     *  Add page for subject
     *  @Shree on 21 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $subject    = [];
        $loginInfo  = get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_subject_id = get_decrypted_value($id, true);
            $subject              = $this->getSubjectData($decrypted_subject_id,"");
            $subject              = isset($subject[0]) ? $subject[0] : [];
            //p($subject);
            if (!$subject)
            {
                return redirect('admin-panel/subject/add-subject')->withError('Subject not found!');
            }
            $arr_class              = get_all_classes($subject['medium_type']);
            $page_title             = trans('language.edit_subject');
            $decrypted_subject_id   = get_encrypted_value($subject['subject_id'], true);
            $save_url               = url('admin-panel/subject/save/' . $decrypted_subject_id);
            $submit_button          = 'Update';
        }
        else
        {
            $arr_class                      = [];
            $page_title    = trans('language.add_subject');
            $save_url      = url('admin-panel/subject/save');
            $submit_button = 'Save';
        }
        $arr_medium                     = \Config::get('custom.medium_type');
        $subject['arr_medium']          = add_blank_option($arr_medium, 'Select Medium');
        $arr_co_scholastic              = get_all_co_scholastic();
        $subject['arr_class']           = add_blank_option($arr_class, 'Select Classes');
        $subject['arr_co_scholastic']   = add_blank_option($arr_co_scholastic, 'Select Type of Co-Scholastic');
        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'subject'       => $subject,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/subject/view-subjects'),
        );
        return view('admin-panel.subject.add')->with($data);
    }

    /**
     *  Destroy subject data
     *  @Shree on 21 July 2018
    **/
    public function destroy($id)
    {
        $subject_id = get_decrypted_value($id, true);
        $subject    = Subject::find($subject_id);
        if ($subject)
        {
            $subject->delete();
            $success_msg = "Subject deleted successfully!";
            return redirect('admin-panel/subject/view-subjects')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Subject not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Save subject data
     *  @Shree on 21 July 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo = get_loggedin_user_data();
        $decrypted_subject_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $subject = Subject::find($decrypted_subject_id);
            if (!$subject)
            {
                return redirect('admin-panel/subject/view-subjects/')->withError('Subject not found!');
            }
            $success_msg = 'Subject updated successfully!';
        }
        else
        {
            $subject     = New Subject;
            $success_msg = 'Subject saved successfully!';
        }
        
        $class_ids = null;
        $co_scholastic_type_id = null;
        if ($request->has('class_ids'))
        {
            $class_ids = Input::get('class_ids');
        }
        if ($request->has('co_scholastic_type_id'))
        {
            $co_scholastic_type_id = Input::get('co_scholastic_type_id');
        }
        $validatior = Validator::make($request->all(), [
                'subject_name' => 'required|unique:subjects,subject_name,' . $decrypted_subject_id . ',subject_id',
                'subject_code'     => 'required',
                'medium_type'     => 'required',
        ]);
        
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $subject->admin_id                  = $loginInfo['admin_id'];
                $subject->update_by                 = $loginInfo['admin_id'];
                $subject->medium_type               = Input::get('medium_type');
                $subject->subject_name              = Input::get('subject_name');
                $subject->subject_code              = Input::get('subject_code');
                $subject->subject_is_coscholastic   = Input::get('subject_is_coscholastic');
                if(Input::get('subject_is_coscholastic') == 1) {
                    $subject->class_ids                 = implode(Input::get('class_ids'),",");
                    $subject->co_scholastic_type_id     = Input::get('co_scholastic_type_id');
                } else {
                    $subject->class_ids                 = "";
                    $subject->co_scholastic_type_id     = null;
                }
                $subject->save();
            }
            catch (\Exception $e)
            {
                
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }

        return redirect('admin-panel/subject/view-subjects')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 21 July 2018
    **/
    public function anyData(Request $request)
    {
        $subject     = [];
        $arr_subject = $this->getSubjectData("",$request);
        
        foreach ($arr_subject as $key => $subject_data)
        {
            $subject[$key] = (object) $subject_data;
        }
        return Datatables::of($subject)
            
            ->addColumn('action', function ($subject)
            {
                $encrypted_subject_id = get_encrypted_value($subject->subject_id, true);
                if($subject->subject_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                
                <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="subject-status/'.$status.'/' . $encrypted_subject_id . '">'.$statusVal.'</a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-subject/' . $encrypted_subject_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-subject/' . $encrypted_subject_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
               
                
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Change Subject's status
     *  @Shree on 21 July 2018
    **/
    public function changeStatus($status,$id)
    {
        $subject_id = get_decrypted_value($id, true);
        $subject    = Subject::find($subject_id);
        if ($subject)
        {
            $subject->subject_status  = $status;
            $subject->save();
            $success_msg = "Subject deleted successfully!";
            return redirect('admin-panel/subject/view-subjects')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Subject not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get subject data
     *  @Shree on 21 July 2018
    **/
    public function getSubjectData($subject_id = array(),$request)
    {
        $subject_return   = [];
        $subject          = [];
        $classInfo        = [];
        $arr_medium             = \Config::get('custom.medium_type');
        $arr_subject_data = Subject::where(function($query) use ($subject_id,$request) 
            {
                if (!empty($subject_id))
                {
                    $query->where('subject_id', $subject_id);
                }
                if (!empty($request) && !empty($request->get('name')))
                {
                    $query->where('subject_name', "like", "%{$request->get('name')}%");
                }
                if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null)
                {
                    $query->where('medium_type', "=", $request->get('medium_type'));
                }
            })->with('scholasticSubjects')->get();
            
            if (!empty($arr_subject_data))
            {
                
                foreach ($arr_subject_data as $key => $subject_data)
                {
                    $subject_is_coscholastic_status = "No";
                    if($subject_data['subject_is_coscholastic'] == 1){
                        $subject_is_coscholastic_status = "Yes";
                    }
                    $subject = array(
                        'subject_id'                        => $subject_data['subject_id'],
                        'subject_name'                      => $subject_data['subject_name'],
                        'subject_code'                      => $subject_data['subject_code'],
                        'medium_type1'                      => $arr_medium[$subject_data['medium_type']],
                        'subject_display_name'              => $subject_data['subject_name'].' - '.$subject_data['subject_code'],
                        'subject_is_coscholastic'           => $subject_data['subject_is_coscholastic'],
                        'subject_is_coscholastic_status'    => $subject_is_coscholastic_status,
                        'class_ids'                         => explode(",",$subject_data['class_ids']),
                        'co_scholastic_type_id'             => $subject_data['co_scholastic_type_id'],
                        'subject_status'                    => $subject_data['subject_status'],
                    );  
                    if(!empty($subject_data['class_ids'])){
                        $classInfo = [];
                        $classIds = explode(",",$subject_data['class_ids']);
                        for($i = 0; $i < COUNT($classIds); $i++){
                            
                            $arr_class_data = Classes::where(function($query) use ($classIds, $i)
                            {
                                if (!empty($classIds[$i]))
                                {
                                    $query->where('class_id', $classIds[$i]);
                                }
                            })->get();
                            $classInfo[] = $arr_class_data[0]['class_name'];
                        }
                        
                        $subject['class_name'] = implode($classInfo,",");
                    } else {
                        $subject['class_name'] = "----";
                    }
                    if (isset($subject_data['scholasticSubjects']['co_scholastic_type_name']))
                    {
                        $subject['co_scholastic_type_name'] = $subject_data['scholasticSubjects']['co_scholastic_type_name'];
                    } else {
                        $subject['co_scholastic_type_name'] = "----";
                    }
                    $subject_return[] = $subject;
                }
        }
        return $subject_return;
    }
}
