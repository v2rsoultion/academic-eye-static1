<?php

namespace App\Model\SubjectTeacherMapping;

use Illuminate\Database\Eloquent\Model;

class SubjectTeachermapping extends Model
{
    protected $table      = 'class_subject_staff_mapping';
    protected $primaryKey = 'class_subject_staff_map_id';

    public function getStaff()
    {
        return $this->belongsTo('App\Model\Staff\Staff', 'staff_ids');
    }

    public function getClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }

    public function getSubject()
    {
        return $this->belongsTo('App\Model\Subject\Subject', 'subject_id');
    }
}
