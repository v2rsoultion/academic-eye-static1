<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class CertificateController extends Controller
{
     public function viewParticipation() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.certificate.certificate_of_participation_02')->with($data);
    }

     public function pdfviewp(Request $request)
    {
        // if($request->has('download')){
        //     $pdf = PDF::loadView('admin-panel/certificate/certificate_of_participation_02');
        //     return $pdf->stream('participation.pdf');
        // }


        return view('admin-panel.certificate.certificate_of_participation_02');
    }


     public function viewWinner() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.certificate.certificate_of_winner_02')->with($data);
    }

     public function pdfview1(Request $request)
    {
        // if($request->has('download')){
        //     $pdf = PDF::loadView('admin-panel/certificate/certificate_of_winner_02');
        //     return $pdf->stream('winner.pdf');
        // }


        return view('admin-panel.certificate.certificate_of_winner_02');
    }

     public function pdfview2(Request $request)
    {
        // if($request->has('download')){
        //     $pdf = PDF::loadView('admin-panel/certificate/certificate_of_winner_02');
        //     return $pdf->stream('winner.pdf');
        // }


        return view('admin-panel.certificate.certificate_of_character_02');
    }

    public function pdfview3(Request $request)
    {
        return view('admin-panel.certificate.certificate_of_character_03');
    }

    public function pdfview4(Request $request)
    {
        return view('admin-panel.certificate.certificate_of_tc_02');
    }

    public function pdfview5(Request $request)
    {
        return view('admin-panel.certificate.certificate_of_tc_03');
    }

    public function pdfview6(Request $request)
    {
        return view('admin-panel.certificate.certificate_of_tc_04');
    }

    public function pdfview7(Request $request)
    {
       if($request->has('download')){
            $pdf = PDF::loadView('admin-panel/certificate/certificate_of_schoolname');
            return $pdf->stream('schoolname.pdf');
        }
        return view('admin-panel.certificate.certificate_of_schoolname');
    }

    public function pdfview8(Request $request)
    {
        return view('admin-panel.certificate.certificate_of_reportcard');
    }
    public function pdfview9(Request $request)
    {
        return view('admin-panel.certificate.certificate_of_reportcard_02');
    }

    public function pdfview10(Request $request)
    {
        return view('admin-panel.certificate.certificate_of_reportcard_03');
    }

    public function pdfview11(Request $request)
    {
        if($request->has('download')){
            $pdf = PDF::loadView('admin-panel/certificate/certificate_of_fee_receipt_02');
            return $pdf->stream('fee_receipt.pdf');
        }
        return view('admin-panel.certificate.certificate_of_fee_receipt_02');
    }
}
