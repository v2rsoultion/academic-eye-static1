@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Ledger Vouchers</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/account') !!}">Accounts</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/account/ledger-vouchers') !!}">Ledger Vouchers</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/account/ledger-vouchers/view-ledger') !!}">View Ledger Vouchers</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card gap_bottom">
              <div class="body form-gap">
                  <div class="table-responsive">
                    <table style="border:1px solid #424242;padding:10px;font-size: 14px;">
                      <tr>
                        <td style="width: 876px;">Ledger: <b>Fees</b></td>
                        <td style="width: 210px;"><b>1-Mar-2019 to 31-Mar-2019</b></td>
                    </table>
                    <table class="bold border" style="width:100%;float:left;">
                    {{ csrf_field() }}
                    <thead style="font-size: 14px;">
                      <tr class="border">
                        <th style="width: 100px;" class="gap_left">Date</th>
                        <th style="width: 550px;">Particulars</th>
                        <th>Vch Type</th>
                        <th style="width: 145px;" class="text-right">Vch No.</th>
                        <th class="text-right gap_right">Debit</th>
                        <th class="text-right gap_right">Credit</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>31-3-2019</td>
                        <td>Cash Account</td>
                        <td>Receipt</td>
                        <td class="text-right">1</td>
                        <td> </td>
                        <td class="text-right border_right"> 10,00,00,000.00</td>
                      </tr>
              
                      <tr>
                        <td>31-3-2019</td>
                        <td>Stationary</td>
                        <td>Journal</td>
                        <td class="text-right">1</td>
                        <td>2,000.00</td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td class="border_right"> </td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr class="border_top">
                        <td colspan="3"> </td>
                        <td class="text-right">Opening Balance :</td>
                        <td> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr class="border_bottom">
                        <td colspan="3"> </td>
                        <td class="text-right border_bottom">Current Total :</td>
                        <td class="text-right">2,000.00</td>
                        <td class="text-right">10,00,00,000.00</td>
                      </tr>
                      <tr>
                        <td colspan="3"> </td>
                        <td class="text-right">Closing Balance: </td>
                        <td> </td>
                        <td class="text-right"> 9,99,98,000.00</td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->


@endsection