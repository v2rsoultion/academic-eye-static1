<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubjectRemarkController extends Controller
{
    public function addRemark()
    {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.subject-remarks.add-remark')->with($data);
    }

    public function viewRemark()
    {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.subject-remarks.view-remark')->with($data);
    }
}
