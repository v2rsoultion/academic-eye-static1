@if(isset($leave_application['student_leave_id']) && !empty($leave_application['student_leave_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        /*margin-bottom: 10px;*/
        width: 100%;
    }
/*.theme-blush .btn-primary {
    margin-top: 4px !important;
}*/
</style>

{!! Form::hidden('leave_application',old('student_leave_id',isset($leave_application['student_leave_id']) ? $leave_application['student_leave_id'] : ''),['class' => 'gui-input', 'id' => 'student_leave_id', 'readonly' => 'true']) !!}

<!-- Basic Info section -->
<div class="row">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">Roll No :</lable>
        <div class="form-group">
            <input type="text" name="rollno" placeholder="Roll No" class="form-control">
        </div>
    </div>
     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">Name :</lable>
        <div class="form-group">
            <input type="text" name="name" placeholder="Name" class="form-control">
        </div>
    </div>
     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">Father Name :</lable>
        <div class="form-group">
            <input type="text" name="father_name" placeholder="Father Name" class="form-control">
        </div>
    </div>
     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">Class :</lable>
        <div class="form-group">
           <select class="form-control show-tick select_form1">
               <option value="">Select Class</option>
               <option value="1">Class-1</option>
               <option value="2">Class-2</option>
               <option value="3">Class-3</option>
               <option value="4">Class-4</option>
           </select>
        </div>
    </div>
</div>
<div class="row clearfix">
     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">Section :</lable>
        <div class="form-group">
           <select class="form-control show-tick select_form1">
               <option value="">Section</option>
               <option value="A">A</option>
               <option value="B">B</option>
               <option value="C">C</option>
               <option value="D">D</option>
           </select>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.stud_leave_app_date_to') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_leave_from_date', old('student_leave_from_date',isset($leave_application['student_leave_from_date']) ? $leave_application['student_leave_from_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.stud_leave_app_date_to'), 'id' => 'student_leave_from_date']) !!}
        </div>
        @if ($errors->has('student_leave_from_date')) <p class="help-block">{{ $errors->first('student_leave_from_date') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.stud_leave_app_date_from') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_leave_to_date', old('student_leave_to_date',isset($leave_application['student_leave_to_date']) ? $leave_application['student_leave_to_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.stud_leave_app_date_from'), 'id' => 'student_leave_to_date'] ) !!}
        </div>
        @if ($errors->has('student_leave_to_date')) <p class="help-block">{{ $errors->first('student_leave_to_date') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">Document File :</lable>
        <div class="form-group">
            <label for="file1" class="field file">
                <input type="file" class="gui-file" name="documents" id="student_document_file" >
                <input type="hidden" class="gui-input" id="student_document_file" placeholder="Upload Photo" readonly>
            </label>
        </div>
        @if ($errors->has('documents')) <p class="help-block">{{ $errors->first('documents') }}</p> @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <lable class="from_one1">{!! trans('language.stud_leave_app_reason') !!} :</lable>
        <div class="form-group">
            <!-- <textarea rows="4" name="description" class="form-control no-resize" placeholder="Description"></textarea> -->
            {!! Form::textarea('student_leave_reason',old('student_leave_reason',isset($leave_application['student_leave_reason']) ? $leave_application['student_leave_reason']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.stud_leave_app_reason'),'rows' => 3, 'cols' => 50)) !!}
        </div>
        @if ($errors->has('student_leave_reason')) <p class="help-block">{{ $errors->first('student_leave_reason') }}</p> @endif
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-primary','name'=>'save']) !!}
        {!! Form::button('Cancel', ['class' => 'btn btn-raised btn-primary']) !!}
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("greaterThan", 
        function(value, element, params) {

            if (!/Invalid|NaN/.test(value)) {

                return value >= $('#student_leave_from_date').val();
            }

            return isNaN(value) && isNaN($(params).val()) 
                || (Number(value) > Number($(params).val())); 
        },'End date must be greater than start date.');

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        jQuery.validator.addMethod("extension", function(a, b, c) {
            return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif|pdf", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
        }, jQuery.validator.format("Only pdf,jpg,png format allowed."));

        $("#student-leave-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                student_leave_reason: {
                    required: true,
                },
                student_leave_from_date: {
                    required: true,  
                },
                student_leave_to_date: {
                    required: true,  
                    greaterThan: $('#student_leave_from_date').val(),
                },
                documents:{
                    extension: true,
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

       
    });

    // function hello(){
    //     var startdate   = $('#student_leave_from_date').val(); 
    //     var enddate     = $('#student_leave_to_date').val();
    //     if(startdate != ''){
    //         alert(enddate);
    //         if(startdate > enddate){
    //             alert('hello');
    //         }
    //     }
    // }
    

</script>