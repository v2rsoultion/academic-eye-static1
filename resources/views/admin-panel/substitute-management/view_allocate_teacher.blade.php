@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  .tabless table tr td .opendiv ul li{
  padding: 0px 0px !important;
  }
  .footable .dropdown-menu>li>a{
  padding: 10px 15px !important;
  }
  .footable .dropdown-menu{
  padding: 0px 0px !important; 
  }
  #btnCu{
  background: transparent;
  height: auto;
  }
  #btnCu i{
  font-size: 23px;
  }
  #sendReply h5{
  font-size: 14px;
  }
  #viewReply h5{
    font-size: 14px;
  }
  #viewReply table tr td{
    padding: 10px 10px;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Allocation</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/menu/substitute-management') }}">Substitute Management</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/menu/substitute-management') }}">Allocation</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/substitute-management/view') }}">Allocation</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <!--  <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
                </div> -->
              <div class="body form-gap" style="padding-top: 20px !important;">
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row">
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                     <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Class</lable>
                          <select class="form-control show-tick select_form1" name="classes" id="">
                            <option value="">Class</option>
                            <option value="1">1st</option>
                            <option value="2">2nd</option>
                            <option value="3">3rd</option>
                            <option value="4">4th</option>
                            <option value="10th"> 10th</option>
                            <option value="11th">11th</option>
                            <option value="12th">12th</option>
                          </select>
                        </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Date </lable>
                          <input type="text" name="datetimes" class="form-control" placeholder="Date" />
                        </div>
                      </div>
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Submit">Search
                        </button>
                      </div>
                      <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                  <hr>
                  <!--  DataTable for view Records  -->
                  <table class="table m-b-0 c_list">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Class </th>
                        <th>Period</th>
                        <th>From - To</th>
                        <th>Teacher Name</th>
                        <th>Pay Extra</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $cou = 1; for ($i=0; $i < 10; $i++) {  ?> 
                      <tr>
                        <td><?php echo $cou; ?></td>
                        <td>10th</td>
                        <td>3rd</td>
                        <td>10/16 10:00 AM - 10/17 06:00 PM</td>
                        <td>Ramesh Kumar</td>
                        <td>Per Day</td>
                     
                        <!-- For Completed -->
                        <!-- <td class="text-success">Completed</td> -->  
                        <td>

                         
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <?php $cou++; } ?> 
                    </tbody>
                  </table>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
@endsection
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript">
  // Date Picker      
  $('#taskDate').bootstrapMaterialDatePicker({
  weekStart: 0, format: 'DD/MM/YYYY',time : false
  });
  
  $(function() {
  $('input[name="datetimes"]').daterangepicker({
   timePicker: true,
   startDate: moment().startOf('hour'),
   endDate: moment().startOf('hour').add(32, 'hour'),
   locale: {
     format: 'M/DD hh:mm A'
   }
  });
  });
</script>