<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeWorkGroupController extends Controller
{
    public function homeWorkGroup() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.homework-group.view-list')->with($data);
    }

     public function homeWorkDetails() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.homework-group.home-work-details')->with($data);
    }

    public function manageStudents() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.homework-group.manage-student')->with($data);
    }

    public function manageParents() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.homework-group.manage-parent')->with($data);
    }

    public function manageTeachers() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.homework-group.manage-teacher')->with($data);
    }
}
