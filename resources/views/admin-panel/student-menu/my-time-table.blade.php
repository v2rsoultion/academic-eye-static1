@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 14px 10px !important;
  }
  .nav-tabs>.nav-item>.nav-link {
    width: 100px;
    margin-right: 5px;
        border: 1px solid #ccc !important;
  }
  #tabingclass .nav-tabs>.nav-item>.nav-link {
    border-radius: 4px !important;
    padding: 8px 25px;
  }
   #tabingclass .nav-link:hover {
        background: #6572b8 !important;
    color: #fff !important;
}
#tabingclass .nav-link:active {
        background: #6572b8 !important;
    color: #fff !important;
}
.idi .nav-tabs>.nav-item>.nav-link.active {
  background: #6572b8 !important;
    color: #fff !important;
}
.modal-dialog {
  max-width: 550px !important;
}
.checkbox {
  float: left;
  margin-right: 20px;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>My Time Table</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student/view-time-table') !!}">My Time Table</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 20px !important;">
                 <div class="headingcommon col-lg-12 show" style="padding: 15px 0px;">Time Table:-</div>

                 <table class="table table-bordered tb1">
                                        <thead>
                                            <tr>
                                                <th width="30">Period no.</th>
                                                <th>Monday</th>
                                                <th>Tuesday</th>
                                                <th>Wenesday</th>
                                                <th>Thursday</th>
                                                <th>Friday</th>
                                                <th>Saturday</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Period-1</td>
                                                <td class="data_table1">
                                                    07:30 AM - 08:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                </td>
                                                <td class="data_table1">
                                                    07:30 AM - 08:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                </td>
                                                
                                                <td>----</td>
                                                <td class="data_table1">
                                                    07:30 AM - 08:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                     07:30 AM - 08:00 AM <br>
                                                    <strong>Sub. :</strong> Economic <br>
                                                    <strong>Teacher :</strong> Suman Tak
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Period-2</td>
                                                <td class="data_table1">
                                                    08:00 AM - 08:30 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar 
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    08:00 AM - 08:30 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar 
                                                </td>
                                                <td class="data_table1">
                                                    08:00 AM - 08:30 AM<br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar 
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    08:00 AM - 08:30 AM <br>
                                                    <strong>Sub. :</strong> Business <br>
                                                    <strong>Teacher :</strong> Ravi Mehra 
                                                     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Period-3</td>
                                                <td class="data_table1">
                                                    08:30 AM - 09:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                     
                                                </td>
                                                 <td class="data_table1">
                                                    08:30 AM - 09:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    08:30 AM - 09:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                     
                                                </td>
                                                <td class="data_table1">
                                                     08:30 AM - 09:00 AM  <br>
                                                    <strong>Sub. :</strong> Computer <br>
                                                    <strong>Teacher :</strong> Sangita Mathur
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    08:30 AM - 09:00 AM <br>
                                                    <strong>Sub. :</strong> English <br>
                                                    <strong>Teacher :</strong> Sangita Mathur
                                                     
                                                </td>                                                <td>----</td>
                                            </tr>
                                            <tr>
                                                <td>Period-4</td>
                                                <td class="data_table1">
                                                    09:00 AM - 09:30 AM <br>
                                                    <strong>Sub. :</strong> Economic <br>
                                                    <strong>Teacher :</strong> Suman Tak
                                                     
                                                </td>
                                                <td class="data_table1">
                                                   09:00 AM - 09:30 AM  <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    09:00 AM - 09:30 AM <br>
                                                    <strong>Sub. :</strong> Economic <br>
                                                    <strong>Teacher :</strong> Suman Tak
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    09:00 AM - 09:30 AM <br>
                                                    <strong>Sub. :</strong> Account <br>
                                                    <strong>Teacher :</strong> Sumit Mishra
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    09:00 AM - 09:30 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                     
                                                </td>
                                                <td>----</td>
                                            </tr>
                                            <tr>
                                                <td>Period-5</td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    10:00 AM - 10:30 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    10:00 AM - 10:30 AM <br>
                                                    <strong>Sub. :</strong> Account <br>
                                                    <strong>Teacher :</strong> Sumit Mishra
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    10:00 AM - 10:30 AM <br>
                                                    <strong>Sub. :</strong> Business <br>
                                                    <strong>Teacher :</strong> Ravi Mehra 
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    10:00 AM - 10:30 AM <br>
                                                    <strong>Sub. :</strong> Account <br>
                                                    <strong>Teacher :</strong> Sumit Mishra
                                                     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Period-6</td>
                                                 <td class="data_table1">
                                                    10:30 AM - 11:00 AM <br>
                                                    <strong>Sub. :</strong> Business <br>
                                                    <strong>Teacher :</strong> Ravi Mehra 
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    10:30 AM - 11:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    10:30 AM - 11:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                     
                                                </td>
                                                <td>----</td>
                                            </tr>
                                            <tr>
                                                <td>Period-7</td>
                                                <td>----</td>
                                                 <td class="data_table1">
                                                    11:00 AM - 11:30 AM <br>
                                                    <strong>Sub. :</strong> Business <br>
                                                    <strong>Teacher :</strong> Ravi Mehra 
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    11:00 AM - 11:30 AM <br>
                                                    <strong>Sub. :</strong> Economic <br>
                                                    <strong>Teacher :</strong> Suman Tak
                                                     
                                                </td>
                                                <td>----</td>
                                                <td>----</td>
                                            </tr> 
                                            <tr>
                                                <td>Period-8</td>
                                                <td class="data_table1">
                                                    11:30 AM - 12:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    11:30 AM - 12:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                </td>
                                                
                                                <td class="data_table1">
                                                    11:30 AM - 12:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Teacher :</strong> Mahesh Kumar
                                                     
                                                </td>
                                                <td>----</td>
                                                <td>----</td>
                                            </tr>                                            
                                        </tbody>
                                    </table>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
@endsection
