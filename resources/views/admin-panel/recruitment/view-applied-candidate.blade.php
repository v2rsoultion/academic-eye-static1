@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
        overflow-x: visible !important; 
    }
</style>
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <h2>View Applied Candidate</h2>
            </div>
            <div class="col-lg-9 col-md-6 col-sm-12 line">
                <!-- <a href="{!! url('admin-panel/recruitment/add-job') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a> -->
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/recruitment') !!}">{!! trans('language.menu_recruitment') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/recruitment/view-candidate-record') !!}">View Applied Candidate</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                 <form>
                               <div class="row">
                                 <!-- <div class="headingcommon  col-lg-12">Search By  :-</div> -->
                                  <div class="col-lg-3 col-md-3 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Job Name"  name="job_name">
                                    </div>
                                </div>
                                 <div class="col-lg-3 col-md-3 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Name"  name="name">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Form No"  name="form_no">
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-raised btn-primary">Search</button>
                                </div>
                                <div class="col-lg-1">
                                    <button  class="btn btn-raised btn-primary ">Clear</button>
                                </div>
                               </div> 
                           </form>
                           <hr>

                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    {{ csrf_field() }}
                                         <thead>
                                            <tr>
                                                <th>{{trans('language.s_no')}}</th>
                                                <th>Form No</th>
                                                <th>Name</th>
                                                <th>Applied for Job</th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>1001</td>
                                                <td>Rajesh Kumar</td>
                                                <td>Class Teacher</td>
                                                <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#largeModal">View Form</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>1002</td>
                                                <td>Raj Singh</td>
                                                <td>Class Teacher</td>
                                                 <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#largeModal">View Form</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>1003</td>
                                                <td>Mahesh Kumar</td>
                                               <td>Class Teacher</td>
                                                 <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#largeModal">View Form</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>1004</td>
                                                <td>Rakesh Kumar</td>
                                                <td>Warden</td>
                                                 <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#largeModal">View Form</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>
@endsection
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#recruitment-job-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '{{url('admin-panel/recruitment/data')}}',
                data: function (d) {
                    d.medium_type = $('select[name="medium_type"]').val();
                    d.job_type    = $('select[name="job_type"]').val();
                    d.job_name    = $('input[name="job_name"]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'job_name', name: 'job_name'},
                {data: 'medium_type1', name: 'medium_type1'},
                {data: 'type', name: 'type'},
                {data: 'no_of_vacancy', name: 'no_of_vacancy'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 5, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }

</script>





