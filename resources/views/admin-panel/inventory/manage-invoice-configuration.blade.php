@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Invoice Configuration</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">Inventory</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/inventory/manage-vendor') !!}">Invoice Configuration</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">

            <div class="card" style="margin-bottom: 20px;">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body form-gap1 ">
                <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Manage Purchase :-</div> -->
                  <form class="" action="" id="manage_vendor" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Prefix</lable>
                          <input type="text" name="prefix" id="prefix" class="form-control" placeholder="Prefix">
                        </div>
                         @if ($errors->has('prefix')) <p class="help-block">{{ $errors->first('prefix') }}</p> @endif
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <lable class="from_one1">Date</lable>
                          <div class="checkbox" style="margin-top:11px;">

                            <input id="checkbox5" type="checkbox"> 
                            <label for="checkbox5" style="margin-right: 28px;">D(Day)</label>

                            <input id="checkbox6" type="checkbox"> 
                            <label for="checkbox6" style="margin-right: 28px;">M(Month)</label>

                            <input id="checkbox7" type="checkbox">
                            <label for="checkbox7" style="margin-right: 28px;">Y(Year)</label>
                           
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Type</lable>
                          <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Type</option>
                             <option value="1">Purchase</option>
                             <option value="3">Purchase Return</option>
                             <option value="2">Sales</option>
                             <option value="4">Sales Return</option>
                            </select>
                        </div>
                      </div>
                     
                    </div>
                      <div class="row">
                         <div class="col-lg-1 ">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
                        </button>
                      </div>
                      <div class="col-lg-1 ">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
                        </button>
                      </div>
                      </div>
                  </form>
                </div>
              </div>
              
              

                  <div class="card">
                    <div class="body form-gap1">
                  <div class="headingcommon  col-lg-12" style="margin-left: -13px">Search By :-</div>
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                     <div class="col-lg-3">
                        <div class="form-group">
                          <!-- <lable class="from_one1">Type</lable> -->
                          <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Type</option>
                             <option value="1">Purchase</option>
                             <option value="3">Purchase Return</option>
                             <option value="2">Sales</option>
                             <option value="4">Sales Return</option>
                            </select>
                        </div>
                      </div>
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary" title="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Prefix</th>
                        <th>Type</th>
                        <th>Format</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                       
                      <tr>
                        <td>1</td>
                        <td>PI</td>
                        <td>Purchase</td>
                        <td>PI-131220181</td>
                       
                        <td class="text-center">
                         
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>SI</td>
                        <td>Sales</td>
                        <td>SI-141220182</td>
                        <td class="text-center">
                         
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>PR</td>
                        <td>Purchase Return</td>
                        <td>PR-101220183</td>
                        <td class="text-center">
                         
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                       <tr>
                        <td>4</td>
                        <td>SR</td>
                        <td>Sales Return</td>
                        <td>SR-111220184</td>
                        <td class="text-center">
                          
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->


@endsection