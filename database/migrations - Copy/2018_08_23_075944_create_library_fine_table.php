<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryFineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'library_fine';
    protected $primaryKey = 'library_fine_id';
    public function up()
    {
        if (!Schema::hasTable('library_fine')){
            Schema::create('library_fine', function (Blueprint $table) {
                $table->increments('library_fine_id');
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->integer('member_id')->unsigned()->default(0);
                $table->tinyInteger('member_type')->default(1)->comment = '1=student,2=staff';
                $table->decimal('fine_amount',18,2)->default(0.00);
                $table->date('fine_date')->nullable();
                $table->text('remark')->nullable();
                $table->tinyInteger('fine_status')->default(1)->comment = '1=due,2=paid';
                $table->timestamps();
            });

            Schema::table('library_fine', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('library_fine', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('library_fine', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
            Schema::table('library_fine', function($table) {
                $table->foreign('member_id')->references('library_member_id')->on('library_members')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library_fine');
    }
}
