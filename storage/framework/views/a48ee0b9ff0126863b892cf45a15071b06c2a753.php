
<?php $__env->startSection('content'); ?>
<style type="text/css">
   .tabing {
    /*width: 100%;*/
    padding: 0px !important;
    border: 1px solid #ccc;
    border-radius: 5px;
    margin-bottom: 20px;
    /*background: #959ecf;*/
   }
   .theme-blush .nav-tabs .nav-link.active{
      color: #fff !important;
    border-radius: 0px !important;
    background: #1f2f60 !important;
    width: 190px;
   }
  /* li {
      border-right: 1px solid #fff;
   }*/
</style>
<!--  Main content here -->
<section class="content">
   <div class="block-header">
      <div class="row">
         <div class="col-lg-5 col-md-6 col-sm-12">
            <h2>Fee Counter</h2>
         </div>
         <div class="col-lg-7 col-md-6 col-sm-12 line">
            <ul class="breadcrumb float-md-right">
             <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
               <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>">Fee Counter</a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/fee-counter/add-fee-counter'); ?>">Add Fee Counter</a></li>
            </ul>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row clearfix">
         <div class="col-lg-12" id="bodypadd">
            <div class="tab-content">
               <div class="tab-pane active" id="classlist">
                  <div class="card">
                     <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
                     </div>
                     <div class="body form-gap">
                     
                        <ul class="nav nav-tabs tabing">
                           <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#student1"><i class="zmdi zmdi-account"></i>
                           Mahendra - 10th A</a></li>
                           <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#student2"><i class="zmdi zmdi-account"></i>
                           Akash Dave - 12th B</a></li>
                           <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#student3"><i class="zmdi zmdi-account"></i>
                           Amit - 12th A</a></li>
                           <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#student4"><i class="zmdi zmdi-account"></i>
                           Aman - 12th A</a></li>
                        </ul>

                        <div class="tab-content">
                            <!-- <div class="tab-pane active" id="classlist">
                            <div class="card border_info_tabs_1 card_top_border1"> -->
                           <div class="tab-pane active" id="student1">
                           <div class="row feecounterclass">
                              <div class="col-lg-3  float-md-left">
                                 <img src="http://www.eljadida24.com/ar/public/admin/assets/images/demo/avatar.png">
                              </div>  
                              <div class="col-lg-8 feecountergap"> 
                              <label><b>Enroll</b> : 25982122 </label> 
                              <label><b>Name</b> : Ankit Dave</label>
                              <label><b>Father Name</b> : Rakesh Kumar</label>
                              <label><b>Class</b> : 10th A</label>
                              </div>
                                
                           </div>
                            <div class="row tablefee">
                           <form action="" method="" style="width: 100%">
                            <div id="table-wrapper">
                          <div id="table-scroll">
                              <table  class="table">
                                 <thead>
                                    <tr>
                                       <th>#</th>
                                       <th>Effective Date1</th>
                                       <th>Particular</th>
                                       <th>Amount</th>
                                       <th></th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                   <?php for ($i=0; $i < 14 ; $i++) {  ?>
                                    <tr>
                                       <td>
                                          <div class="" style="margin-bottom: 0px !important;">
                                             <input id="checkbox" type="checkbox">
                                             <label for="checkbox"></label>
                                          </div>
                                       </td>
                                       <td>20 July 2018</td>
                                       <td>Exam Fees Jan</td>
                                       <td>2000</td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                    </tr>

                                    <tr>
                                       <td>
                                          <div class="" style="margin-bottom: 0px !important;">
                                             <input id="checkbox" type="checkbox">
                                             <label for="checkbox"></label>
                                          </div>
                                       </td>
                                       <td>20 July 2018</td>
                                       <td>Exam Fees Jan</td>
                                       <td>2000</td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                    </tr>
                                    <?php   } ?>
                                  </tbody>
                              </table>
                            </div>
                          </div>
                               <table class="table-borderd" width="100%" id="tableCutm">
                              
                                 <tbody>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td ><b>Total Chargeable Fee</b></td>
                                       <td class="text-center">10,000</td>
                                    </tr>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td ><b>Paying Amount</b></td>
                                       <td class="text-center">8,000</td>
                                    </tr>
                                    <tr>
                                       <td></td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox4" type="checkbox">
                                             <label for="checkbox4">Wallet - 5000</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox5" type="checkbox">
                                             <label for="checkbox5">Discount - 100</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td rowspan="1" ></td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox6" type="checkbox">
                                             <label for="checkbox6">Pay-later</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                       <td > Payment Mode</td>
                                       <td class="text-center">
                                          <select class="form-control show-tick select_form1" name="classes" id="selectcheque">
                                             <option value=""> Select</option>
                                             <option value="Online"> Online</option>
                                             <option value="Offline">Offline</option>
                                             <option value="Cheque">Cheque</option>
                                          </select>
                                       </td>
                                    </tr>
                                    <tr id="showbank" style="display:none;">
                                       <div >
                                          <td></td>
                                          <td >
                                             <input type="text" name="" class="form-control" placeholder="Bank Name">
                                          </td>
                                          <td>
                                             <input type="text" name="" class="form-control" placeholder=" Cheque No">
                                          </td>
                                          <td>
                                             <input type="text" name="" class="form-control" placeholder=" Cheque Date" id="pdate">
                                          </td>
                                          <td>
                                             <input type="text" name="" class="form-control" placeholder="  Cheque Amount">
                                          </td>
                                         
                                       </div>
                                    </tr>
                                    <tr>
                                       <td></td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox7" type="checkbox">
                                             <label for="checkbox7"> Add Excessive in Wallet</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="0.00">
                                       </td>
                                   
                                 
                                       <td > 
                                          Select Date
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" id="date_start" placeholder="Date">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td > 
                                          Add Description
                                       </td>
                                       <td class="text-center">
                                          <textarea class="form-control" placeholder="Description"></textarea>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td > 
                                       </td>
                                       <td class="text-center">
                                          <button type="submit" class="btn btn-raised btn-primary" title="Charge Fees">Charge Fees
                                        </button>
                                       </td>
                                    
                                    </tr>
                                 </tbody>
                              </table>

                           </form>
                        </div>
                           </div>
                       <!-- </div></div> -->

                           <div class="tab-pane fade" id="student2">
                           <div class="row feecounterclass">
                              <div class="col-lg-3  float-md-left">
                                <img src="http://www.eljadida24.com/ar/public/admin/assets/images/demo/avatar.png">
                              </div>
                                 <div class="col-lg-8 feecountergap"> 

                                 <label><b>Enroll</b> : 25982122 </label> 
                                 <label><b>Name</b> : Akash Dave </label>
                                 <label><b>Father Name</b> : Rakesh Kumar</label>
                                 <label><b>Class</b> : 12th B</label>
                                 </div>    
                           </div>
                            <div class="row tablefee">
                           <form action="" method="" style="width: 100%">
                            <div id="table-wrapper">
                          <div id="table-scroll">
                              <table  class="table">
                                 <thead>
                                    <tr>
                                       <th>#</th>
                                       <th>Effective Date2</th>
                                       <th>Particular</th>
                                       <th>Amount</th>
                                       <th></th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                   <?php for ($i=0; $i < 14 ; $i++) {  ?>
                                    <tr>
                                       <td>
                                          <div class="" style="margin-bottom: 0px !important;">
                                             <input id="checkbox" type="checkbox">
                                             <label for="checkbox"></label>
                                          </div>
                                       </td>
                                       <td>20 July 2018</td>
                                       <td>Exam Fees Jan</td>
                                       <td>2000</td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                    </tr>

                                    <tr>
                                       <td>
                                          <div class="" style="margin-bottom: 0px !important;">
                                             <input id="checkbox" type="checkbox">
                                             <label for="checkbox"></label>
                                          </div>
                                       </td>
                                       <td>20 July 2018</td>
                                       <td>Exam Fees Jan</td>
                                       <td>2000</td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                    </tr>
                                    <?php   } ?>
                                  </tbody>
                              </table>
                            </div>
                          </div>
                               <table class="table-borderd" width="100%" id="tableCutm">
                              
                                 <tbody>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td ><b>Total Chargeable Fee</b></td>
                                       <td class="text-center">10,000</td>
                                    </tr>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td ><b>Paying Amount</b></td>
                                       <td class="text-center">8,000</td>
                                    </tr>
                                    <tr>
                                       <td></td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox4" type="checkbox">
                                             <label for="checkbox4">Wallet - 5000</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox5" type="checkbox">
                                             <label for="checkbox5">Discount - 100</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td rowspan="1" ></td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox6" type="checkbox">
                                             <label for="checkbox6">Pay-later</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                       <td > Payment Mode</td>
                                       <td class="text-center">
                                          <select class="form-control show-tick select_form1" name="classes" id="selectcheque">
                                             <option value=""> Select</option>
                                             <option value="Online"> Online</option>
                                             <option value="Offline">Offline</option>
                                             <option value="Cheque">Cheque</option>
                                          </select>
                                       </td>
                                    </tr>
                                    <tr id="showbank" style="display:none;">
                                       <div >
                                          <td></td>
                                          <td >
                                             <input type="text" name="" class="form-control" placeholder="Bank Name">
                                          </td>
                                          <td>
                                             <input type="text" name="" class="form-control" placeholder=" Cheque No">
                                          </td>
                                          <td>
                                             <input type="text" name="" class="form-control" placeholder=" Cheque Date" id="pdate">
                                          </td>
                                          <td>
                                             <input type="text" name="" class="form-control" placeholder="  Cheque Amount">
                                          </td>
                                         
                                       </div>
                                    </tr>
                                    <tr>
                                       <td></td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox7" type="checkbox">
                                             <label for="checkbox7"> Add Excessive in Wallet</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="0.00">
                                       </td>
                                   
                                 
                                       <td > 
                                          Select Date
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" id="date_start" placeholder="Date">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td > 
                                          Add Description
                                       </td>
                                       <td class="text-center">
                                          <textarea class="form-control" placeholder="Description"></textarea>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td > 
                                       </td>
                                       <td class="text-center">
                                          <button type="submit" class="btn btn-raised btn-primary" title="Charge Fees">Charge Fees
                                        </button>
                                       </td>
                                       <!-- <td> <div class="col-lg-12"><button type="submit" class="btn btn-raised btn-round btn-primary" style=""  title="Charge Fees">Charge Fees </button></div> </td> -->
                                    </tr>
                                 </tbody>
                              </table>

                           </form>
                        </div>
                           </div>
                   
                           <div class=" tab-pane fade" id="student3">
                           <div class="row feecounterclass">
                              <div class="col-lg-3 float-md-left">
                                 <img src="http://www.eljadida24.com/ar/public/admin/assets/images/demo/avatar.png">
                              </div>
                                 <div class="col-lg-8 feecountergap"> 

                                 <label><b>Enroll</b> : 25982122 </label> 
                                 <label><b>Name</b> : Amit</label>
                                 <label><b>Father Name</b> : Rakesh Kumar</label>
                                 <label><b>Class</b> : 12th A</label>
                                 </div>    
                           </div>
                            <div class="row tablefee">
                           <form action="" method="" style="width: 100%">
                            <div id="table-wrapper">
                          <div id="table-scroll">
                              <table  class="table">
                                 <thead>
                                    <tr>
                                       <th>#</th>
                                       <th>Effective Date3</th>
                                       <th>Particular</th>
                                       <th>Amount</th>
                                       <th></th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                   <?php for ($i=0; $i < 14 ; $i++) {  ?>
                                    <tr>
                                       <td>
                                          <div class="" style="margin-bottom: 0px !important;">
                                             <input id="checkbox" type="checkbox">
                                             <label for="checkbox"></label>
                                          </div>
                                       </td>
                                       <td>20 July 2018</td>
                                       <td>Exam Fees Jan</td>
                                       <td>2000</td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                    </tr>

                                    <tr>
                                       <td>
                                          <div class="" style="margin-bottom: 0px !important;">
                                             <input id="checkbox" type="checkbox">
                                             <label for="checkbox"></label>
                                          </div>
                                       </td>
                                       <td>20 July 2018</td>
                                       <td>Exam Fees Jan</td>
                                       <td>2000</td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                    </tr>
                                    <?php   } ?>
                                  </tbody>
                              </table>
                            </div>
                          </div>
                               <table class="table-borderd" width="100%" id="tableCutm">
                              
                                 <tbody>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td ><b>Total Chargeable Fee</b></td>
                                       <td class="text-center">10,000</td>
                                    </tr>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td ><b>Paying Amount</b></td>
                                       <td class="text-center">8,000</td>
                                    </tr>
                                    <tr>
                                       <td></td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox4" type="checkbox">
                                             <label for="checkbox4">Wallet - 5000</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox5" type="checkbox">
                                             <label for="checkbox5">Discount - 100</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td rowspan="1" ></td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox6" type="checkbox">
                                             <label for="checkbox6">Pay-later</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                       <td > Payment Mode</td>
                                       <td class="text-center">
                                          <select class="form-control show-tick select_form1" name="classes" id="selectcheque">
                                             <option value=""> Select</option>
                                             <option value="Online"> Online</option>
                                             <option value="Offline">Offline</option>
                                             <option value="Cheque">Cheque</option>
                                          </select>
                                       </td>
                                    </tr>
                                    <tr id="showbank" style="display:none;">
                                       <div >
                                          <td></td>
                                          <td >
                                             <input type="text" name="" class="form-control" placeholder="Bank Name">
                                          </td>
                                          <td>
                                             <input type="text" name="" class="form-control" placeholder=" Cheque No">
                                          </td>
                                          <td>
                                             <input type="text" name="" class="form-control" placeholder=" Cheque Date" id="pdate">
                                          </td>
                                          <td>
                                             <input type="text" name="" class="form-control" placeholder="  Cheque Amount">
                                          </td>
                                         
                                       </div>
                                    </tr>
                                    <tr>
                                       <td></td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox7" type="checkbox">
                                             <label for="checkbox7"> Add Excessive in Wallet</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="0.00">
                                       </td>
                                   
                                 
                                       <td > 
                                          Select Date
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" id="date_start" placeholder="Date">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td > 
                                          Add Description
                                       </td>
                                       <td class="text-center">
                                          <textarea class="form-control" placeholder="Description"></textarea>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td > 
                                       </td>
                                       <td class="text-center">
                                          <button type="submit" class="btn btn-raised btn-primary" title="Charge Fees">Charge Fees
                                        </button>
                                       </td>
                                       <!-- <td> <div class="col-lg-12"><button type="submit" class="btn btn-raised btn-round btn-primary" style=""  title="Charge Fees">Charge Fees </button></div> </td> -->
                                    </tr>
                                 </tbody>
                              </table>

                           </form>
                        </div>
                           </div>

                           <div class=" tab-pane fade" id="student4">
                           <div class="row feecounterclass">
                              <div class="col-lg-3 float-md-left">
                                 <img src="http://www.eljadida24.com/ar/public/admin/assets/images/demo/avatar.png">
                              </div>
                                 <div class="col-lg-8 feecountergap"> 

                                 <label><b>Enroll</b> : 25982122 </label> 
                                 <label><b>Name</b> : Aman</label>
                                 <label><b>Father Name</b> : Rakesh Kumar</label>
                                 <label><b>Class</b> : 12th A</label>
                                 </div>    
                           </div>
                            <div class="row tablefee">
                           <form action="" method="" style="width: 100%">
                            <div id="table-wrapper">
                          <div id="table-scroll">
                              <table  class="table">
                                 <thead>
                                    <tr>
                                       <th>#</th>
                                       <th>Effective Date4</th>
                                       <th>Particular</th>
                                       <th>Amount</th>
                                       <th></th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                   <?php for ($i=0; $i < 14 ; $i++) {  ?>
                                    <tr>
                                       <td>
                                          <div class="" style="margin-bottom: 0px !important;">
                                             <input id="checkbox" type="checkbox">
                                             <label for="checkbox"></label>
                                          </div>
                                       </td>
                                       <td>20 July 2018</td>
                                       <td>Exam Fees Jan</td>
                                       <td>2000</td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                    </tr>

                                    <tr>
                                       <td>
                                          <div class="" style="margin-bottom: 0px !important;">
                                             <input id="checkbox" type="checkbox">
                                             <label for="checkbox"></label>
                                          </div>
                                       </td>
                                       <td>20 July 2018</td>
                                       <td>Exam Fees Jan</td>
                                       <td>2000</td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                    </tr>
                                    <?php   } ?>
                                  </tbody>
                              </table>
                            </div>
                          </div>
                               <table class="table-borderd" width="100%" id="tableCutm">
                              
                                 <tbody>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td ><b>Total Chargeable Fee</b></td>
                                       <td class="text-center">10,000</td>
                                    </tr>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td ><b>Paying Amount</b></td>
                                       <td class="text-center">8,000</td>
                                    </tr>
                                    <tr>
                                       <td></td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox4" type="checkbox">
                                             <label for="checkbox4">Wallet - 5000</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox5" type="checkbox">
                                             <label for="checkbox5">Discount - 100</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td rowspan="1" ></td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox6" type="checkbox">
                                             <label for="checkbox6">Pay-later</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="2000">
                                       </td>
                                       <td > Payment Mode</td>
                                       <td class="text-center">
                                          <select class="form-control show-tick select_form1" name="classes" id="selectcheque">
                                             <option value=""> Select</option>
                                             <option value="Online"> Online</option>
                                             <option value="Offline">Offline</option>
                                             <option value="Cheque">Cheque</option>
                                          </select>
                                       </td>
                                    </tr>
                                    <tr id="showbank" style="display:none;">
                                       <div >
                                          <td></td>
                                          <td >
                                             <input type="text" name="" class="form-control" placeholder="Bank Name">
                                          </td>
                                          <td>
                                             <input type="text" name="" class="form-control" placeholder=" Cheque No">
                                          </td>
                                          <td>
                                             <input type="text" name="" class="form-control" placeholder=" Cheque Date" id="pdate">
                                          </td>
                                          <td>
                                             <input type="text" name="" class="form-control" placeholder="  Cheque Amount">
                                          </td>
                                         
                                       </div>
                                    </tr>
                                    <tr>
                                       <td></td>
                                       <td >
                                          <div class="checkbox" id="customid">
                                             <input id="checkbox7" type="checkbox">
                                             <label for="checkbox7"> Add Excessive in Wallet</label>
                                          </div>
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" placeholder="0.00">
                                       </td>
                                   
                                 
                                       <td > 
                                          Select Date
                                       </td>
                                       <td class="text-center">
                                          <input type="text" name="" class="form-control" id="date_start" placeholder="Date">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td > 
                                          Add Description
                                       </td>
                                       <td class="text-center">
                                          <textarea class="form-control" placeholder="Description"></textarea>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="3"></td>
                                       <td > 
                                       </td>
                                       <td class="text-center">
                                          <button type="submit" class="btn btn-raised btn-primary" title="Charge Fees">Charge Fees
                                        </button>
                                       </td>
                                       <!-- <td> <div class="col-lg-12"><button type="submit" class="btn btn-raised btn-round btn-primary" style=""  title="Charge Fees">Charge Fees </button></div> </td> -->
                                    </tr>
                                 </tbody>
                              </table>

                           </form>
                        </div>
                           </div>


                        </div>
                       </div>
                       
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>