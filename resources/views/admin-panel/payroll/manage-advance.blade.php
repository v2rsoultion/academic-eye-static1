@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Manage Advance</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/payroll') !!}">Payroll</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-advance') !!}">Manage Advance</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card" style="margin-bottom: 20px;">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body" style="padding: 0px 20px 20px 20px;">
               
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Manage Advance :-</div> -->
                  <form class="" action="" id="manage_vendor" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1"> Employee</lable>
                          <select class="form-control show-tick select_form1" name="" id="">
                                <option value="">Select Employee</option>
                                <option value="1">Employee-1</option>
                                <option value="2">Employee-2</option>
                                <option value="3">Employee-3</option>
                                <option value="4">Employee-4</option>
                              </select>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Advance Amount</lable>
                          <input type="text" name="advance_amount" id="" class="form-control" placeholder="Advance Amount">
                        </div>
                          @if ($errors->has('advance_amount')) <p class="help-block">{{ $errors->first('advance_amount') }}</p> @endif  
                      </div> 
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Deduct Amount (Per Month)</lable>
                          <input type="text" name="deduct_amount" id="" class="form-control" placeholder="Debuct Amount">
                        </div>
                          @if ($errors->has('deduct_amount')) <p class="help-block">{{ $errors->first('deduct_amount') }}</p> @endif  
                      </div> 
                    </div>
                    
                      <!-- <hr> -->
                      <div class="row">
                      <div class="col-lg-1 ">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
                        </button>
                      </div>
                      <div class="col-lg-1 ">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
                        </button>
                      </div>
                    
                    </div>
                  </form>
                </div>
              </div>
                  <!-- <hr> -->
                  <div class="card">
                    <div class="body form-gap">
                  <div class="headingcommon  col-lg-12" style="margin-left: -13px">Search By :-</div>
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                       <div class="col-lg-3">
                        <div class="form-group">
                          <!-- <lable class="from_one1"> Employee</lable> -->
                          <select class="form-control show-tick select_form1" name="" id="">
                                <option value="">Select Employee</option>
                                <option value="1">Employee-1</option>
                                <option value="2">Employee-2</option>
                                <option value="3">Employee-3</option>
                                <option value="4">Employee-4</option>
                              </select>
                        </div>
                      </div>
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary" title="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                
                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <!-- <hr> -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th style="width: 200px">Employee Name</th>
                        <th>Advance Amount</th>
                        <th>Deduct Month</th>
                        <th>Paid Amount</th>
                        <th>Remaining Amount</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="18%">
                           Ankit Dave
                        </td>
                        <td>20000</td>
                        <td>900</td>
                        <td>100000</td>
                        <td>1900000</td>
                        <td>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="18%">
                           Ankit Dave
                        </td>
                        <td>12000</td>
                        <td>1200</td>
                        <td>100000</td>
                        <td>1900000</td>
                        <td>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                    
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->

<script type="text/javascript">
  function checkForAmount(val) {
        var x = document.getElementById("AmountMode");
        if(val == 0) {
            if (x.style.display === "none") {
                x.style.display = "none";
            } else {
                x.style.display = "none";
            }
        } else {
            if (x.style.display === "block") {
                x.style.display = "block";
            } else {
                x.style.display = "block";
            }
        }

        var y = document.getElementById("BasicPayMode");
        if(val == 1) {
            if (y.style.display === "none") {
                y.style.display = "none";
            } else {
                y.style.display = "none";
            }
        } else {
            if (y.style.display === "block") {
                y.style.display = "block";
            } else {
                y.style.display = "block";
            }
        }
    }
</script>

@endsection