
<?php $__env->startSection('content'); ?>
<style type="text/css">
  td{
    /*padding: 12px !important;*/
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>PT Setting</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/payroll'); ?>">Payroll</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/payroll/manage-pt-setting'); ?>">PT Setting</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card" style="margin-bottom: 20px;">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body" style="padding: 0px 20px 20px 20px;">
                  
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Add Salary Heads :-</div> -->
                  <form class="" action="" id="manage_vendor" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Salary Range(Min)</lable>
                          <input type="text" name="salary_min" id="" class="form-control" placeholder="Min">
                        </div>
                      </div>

                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Salary Range(Max)</lable>
                          <input type="text" name="salary_max" id="" class="form-control" placeholder="Max">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="headingcommon  col-lg-12" style="margin-left: -1px">Amount For Monthly :-</div>
                      <div class="col-lg-6" style="border-right: 1px solid #e6e6e6;">
                      <div class="row col-lg-12">
                        <div class="col-lg-4">
                          <lable class="from_one1">January</lable>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="text" name="january" id="" class="form-control" placeholder="Amount">
                          </div>
                        </div>  
                      </div>
                      <div class="row col-lg-12">
                        <div class="col-lg-4">
                          <lable class="from_one1">Febuary</lable>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="text" name="febuary" id="" class="form-control" placeholder="Amount">
                          </div>
                        </div>  
                      </div>
                      <div class="row col-lg-12">
                        <div class="col-lg-4">
                          <lable class="from_one1">March</lable>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="text" name="march" id="" class="form-control" placeholder="Amount">
                          </div>
                        </div>  
                      </div>
                      <div class="row col-lg-12">
                        <div class="col-lg-4">
                          <lable class="from_one1">April</lable>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="text" name="april" id="" class="form-control" placeholder="Amount">
                          </div>
                        </div>  
                      </div>
                      <div class="row col-lg-12">
                        <div class="col-lg-4">
                          <lable class="from_one1">May</lable>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="text" name="may" id="" class="form-control" placeholder="Amount">
                          </div>
                        </div>  
                      </div>
                      <div class="row col-lg-12">
                        <div class="col-lg-4">
                          <lable class="from_one1">June</lable>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="text" name="june" id="" class="form-control" placeholder="Amount">
                          </div>
                        </div>  
                      </div>
                      
                    </div>
                    <div class="col-lg-6"> 
                      <div class="row col-lg-12">
                        <div class="col-lg-4">
                          <lable class="from_one1">July</lable>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="text" name="july" id="" class="form-control" placeholder="Amount">
                          </div>
                        </div>  
                      </div>
                      <div class="row col-lg-12">
                        <div class="col-lg-4">
                          <lable class="from_one1">August</lable>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="text" name="august" id="" class="form-control" placeholder="Amount">
                          </div>
                        </div>  
                      </div>
                      <div class="row col-lg-12">
                        <div class="col-lg-4">
                          <lable class="from_one1">September</lable>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="text" name="september" id="" class="form-control" placeholder="Amount">
                          </div>
                        </div>  
                      </div>
                      <div class="row col-lg-12">
                        <div class="col-lg-4">
                          <lable class="from_one1">October</lable>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="text" name="october" id="" class="form-control" placeholder="Amount">
                          </div>
                        </div>  
                      </div>
                      <div class="row col-lg-12">
                        <div class="col-lg-4">
                          <lable class="from_one1">November</lable>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="text" name="november" id="" class="form-control" placeholder="Amount">
                          </div>
                        </div>  
                      </div>
                      <div class="row col-lg-12">
                        <div class="col-lg-4">
                          <lable class="from_one1">December</lable>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <input type="text" name="december" id="" class="form-control" placeholder="Amount">
                          </div>
                        </div>  
                      </div>
                      
                      </div>
                    </div>
                      <!-- <hr> -->
                      <div class="row">
                      <div class="col-lg-1 ">
                        <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
                        </button>
                      </div>
                      <div class="col-lg-1 ">
                        <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel
                        </button>
                      </div>
                    
                    </div>
                  </form>
                </div>
                </div> 
                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <!-- <hr> -->
                  <div class="card">

                    <div class="body form-gap">
                      <div class="headingcommon  col-lg-12" style="margin-left: -13px">View PT Settings:-</div> 
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Salary Range(Min-Max)</th>
                        <th>Amount</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>10000-20000</td>
                        <td><a href="#" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#AmountModel">View Amount</a></td>
                        <td class="text-center">
                         <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>10000-15000</td>
                        <td><a href="#" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#AmountModel">View Amount</a></td>
                        <td class="text-center">
                         <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>20000-30000</td>
                        <td><a href="#" class="btn btn-raised btn-primary" data-toggle="modal" data-target="#AmountModel">View Amount</a></td>
                        <td class="text-center">
                         <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->

<!-- Action Model  -->
<div class="modal fade" id="AmountModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">View Amount</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Month</th>
                        <th>Amount</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>January</td>
                        <td>10000</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Febuary</td>
                        <td>10000</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>March</td>
                        <td>10000</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>April</td>
                        <td>10000</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>May</td>
                        <td>10000</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>6</td>
                        <td>June</td>
                        <td>10000</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>7</td>
                        <td>July</td>
                        <td>10000</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>8</td>
                        <td>August</td>
                        <td>10000</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>9</td>
                        <td>September</td>
                        <td>10000</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>10</td>
                        <td>October</td>
                        <td>10000</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>11</td>
                        <td>November</td>
                        <td>10000</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>12</td>
                        <td>December</td>
                        <td>10000</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            
         </div>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>