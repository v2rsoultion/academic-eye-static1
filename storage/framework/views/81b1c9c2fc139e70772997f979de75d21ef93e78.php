
<?php $__env->startSection('content'); ?>
<style type="text/css">
   .table-responsive {
    overflow-x: visible !important;
   }
</style>
<!--  Main content here -->
<section class="content">
   <div class="block-header">
      <div class="row">
         <div class="col-lg-5 col-md-6 col-sm-12">
            <h2>Common Entry</h2>
         </div>
         <div class="col-lg-7 col-md-6 col-sm-12 line">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.prepaid_account'); ?></a></li>
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/prepaid-account/manage-account'); ?>">Common Entry</a></li>
            </ul>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row clearfix">
         <div class="col-lg-12" id="bodypadd">
            <div class="tab-content">
               <div class="tab-pane active" id="classlist">
                  <div class="card">
                  <div class="body form-gap">
                   <div class="row tabless" >
                  <div class="headingcommon  col-lg-12" style="padding-bottom: 0px !important">Add Common Entry :-</div>
                  <form class="" action="" method="" id="searchpanel" style="width: 100%;">
                    <div class="row">
                       <div class="col-lg-3">
                        <div class="form-group">
                           <lable class="from_one1" for="name">Account</lable>
                           <select class="form-control show-tick select_form1" name="classes" id="">
                              <option value="">Select Account</option>
                              <option value="1">rajeshkumar@gmail.com</option>
                              <option value="2">maheshkumar@gmail.com</option>
                              <option value="3">rajkumar@gmail.com</option>
                              <option value="4">rajukumar@gmail.com</option>
                           </select>
                        </div>
                     </div>
              
                     <div class="col-lg-3">
                        <div class="form-group">
                           <lable class="from_one1" for="name">Class</lable>
                           <select class="form-control show-tick select_form1" name="classes" id="">
                              <option value="">Select Class</option>
                              <option value="1"> 1st</option>
                              <option value="2">2nd</option>
                              <option value="3">3rd</option>
                              <option value="4">4th</option>
                              <option value="10th"> 10th</option>
                              <option value="11th">11th</option>
                              <option value="12th">12th</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="form-group">
                           <lable class="from_one1" for="name">Section</lable>
                           <select class="form-control show-tick select_form1" name="classes" id="">
                              <option value="">Select Section</option>
                              <option value="A">A</option>
                              <option value="B">B</option>
                              <option value="C">C</option>
                              <option value="D">D</option>
                              
                           </select>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                     </div> 
                     <hr>
                     <div class="row clearfix ">                            
                        
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <button type="submit" class="btn btn-raised  btn-primary">Search</button>
                            <button type="submit" class="btn btn-raised  btn-primary">Clear</button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary float-right" data-toggle="modal" data-target="#exampleModalCenter">
                            Proceed
                          </button>
                        </div>
                    </div>
                  </form>
                  <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div>
                  
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <div class="table-responsive">
                  <table class="table  m-b-0 c_list">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Father Name</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $count = 1; for ($i=0; $i < 15; $i++) {  ?>
                      <tr>
                        <td>
                            <div class="checkbox">
                               <input id="checkbox<?php echo $count; ?>" type="checkbox">
                               <label class="from_one1" for="checkbox<?php echo $count; ?>" style="margin-bottom: 4px !important;"></label>
                            </div>
                          </td>
                         <td style="width: 300px">
                          <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%"> Ankit Dave
                        </td>
                        <td>Mahesh Kumar</td>
                         <!-- <td class="text-center">
                            <div class="dropdown">
                              <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="zmdi zmdi-label"></i>
                                <span class="zmdi zmdi-caret-down"></span>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li>
                                  <a href="#" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#exampleModalCenter" style="background: transparent !important;">Add More</a>
                                </li>
                              </ul>
                            </div>
                        </td> -->
                      </tr>
                      <?php $count++; } ?>
                    </tbody>
                  </table>
                </div>
                </div>
              </div>          
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   
  
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>
<!-- Model Data -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Amount</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row tabless">
          <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
          <div class="col-lg-4">
            <div class="form-group">
              <lable class="from_one1" for="name">Amount </lable>
              <input type="text" name="name" id="name" class="form-control" placeholder="Amount">
            </div>
          </div>
          <div class="col-lg-4">
            <div class="form-group">
              <lable class="from_one1" for="name">Date </lable>
               <input type="text" name="name" id="amount_Date" class="form-control" placeholder="Date">
            </div>
          </div>
           
          <div class="col-lg-2">
            <button type="submit" class="btn btn-raised  btn-primary" style="margin-top: 30px !important;" title="Save">Save
            </button>
          </div>
          <div class="col-lg-2">
            <button type="submit" style="margin-top: 30px !important;" class="btn btn-raised  btn-primary waves-effect" data-dismiss="modal">Cancel</button>  
          </div>

          <!-- <div class="col-lg-3">
            <button style="margin-top: 26px;" type="submit" class="btn btn-raised btn-primary float-right searchBtn" title="Save">Save
            </button>
          </div> -->
       
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>