<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentGroupController extends Controller
{
	
    public function studentGroup()
    {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.student-group.add-student-group')->with($data);
    }
    public function manageStudent() 
     {
     	 $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.student-group.manage-student')->with($data);
     }

     public function addStudent() 
     {
     	 $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.student-group.add-student')->with($data);
     }

      public function studentList() 
     {
     	 $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.student-group.student-send-message')->with($data);
     }
}
