<?php $__env->startSection('content'); ?>

<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2><?php echo trans('language.view_city'); ?></h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <a href="<?php echo url('admin-panel/state/add-state'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><?php echo trans('language.menu_configuration'); ?></li>
                    <li class="breadcrumb-item"><a href="#"><?php echo trans('language.city'); ?></a></li>
                    <li class="breadcrumb-item active"><a href=""><?php echo trans('language.view_city'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                <?php if(session()->has('success')): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?php echo e(session()->get('success')); ?>

                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif; ?>
                                <?php if($errors->any()): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?php echo e($errors->first()); ?>

                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif; ?>
                                <?php echo Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']); ?>

                                    <div class="row clearfix">
                                         <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                <?php echo Form::select('country_id', $city['arr_country'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'country_id', 'onChange'=>'getCountryState(this.value)']); ?>

                                                <i class="arrow double"></i>
                                            </label>
                                            <?php if($errors->has('country_id')): ?> <p class="help-block"><?php echo e($errors->first('country_id')); ?></p> <?php endif; ?>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                <?php echo Form::select('state_id', $city['arr_state'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'state_id']); ?>

                                                <i class="arrow double"></i>
                                            </label>
                                            <?php if($errors->has('state_id')): ?> <p class="help-block"><?php echo e($errors->first('state_id')); ?></p> <?php endif; ?>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                <?php echo Form::text('city_name', old('city_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.city_name'), 'id' => 'city_name']); ?>

                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <?php echo Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']); ?>

                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <?php echo Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]); ?>

                                        </div>
                                    </div>
                                <?php echo Form::close(); ?>

                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="city-table" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th><?php echo e(trans('language.s_no')); ?></th>
                                                <th><?php echo e(trans('language.country_name')); ?></th>
                                                <th><?php echo e(trans('language.state_name')); ?></th>
                                                <th><?php echo e(trans('language.city_name')); ?></th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#city-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '<?php echo e(url('admin-panel/city/data')); ?>',
                data: function (d) {
                    d.country_id = $('select[name="country_id"]').val();
                    d.state_id   = $('select[name="state_id"]').val();
                    d.city_name  = $('input[name="city_name"]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'country_name', name: 'country_name'},
                {data: 'state_name', name: 'state_name'},
                {data: 'city_name', name: 'city_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                },
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }

    function getCountryState(country_id)
    {
        if(country_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/state/get-country-state-data')); ?>",
                type: 'GET',
                data: {
                    'country_id': country_id
                },
                success: function (data) {
                    $("select[name='state_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='state_id'").html('<option value="">Select State</option>');
            $(".mycustloading").hide();
        }
    }

</script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>