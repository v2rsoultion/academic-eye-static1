<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
</style>

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-2 col-md-2 col-sm-12">
        <lable class="from_one1">Name :</lable>
        <div class="form-group">
            <input type="text" name="visitor_name" class="form-control" placeholder="Name">
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12">
        <lable class="from_one1">Email :</lable>
        <div class="form-group">
            <input type="Email" name="email" class="form-control" placeholder="Email ID">
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12">
        <lable class="from_one1">Contact No :</lable>
        <div class="form-group">
            <input type="number" name="contact_no" class="form-control" placeholder="Contact No">
        </div>
    </div>
    <div class="col-lg-2 col-md-3 col-sm-12">
     <div class="form-group">
        <lable class="from_one1" for="name">Check-In Time</lable>
        <input type="text" name="Visit_date" id="checkInTime" class="form-control" placeholder="In Time">
     </div>
  </div>
   <div class="col-lg-2 col-md-3 col-sm-12">
     <div class="form-group">
        <lable class="from_one1" for="name">Check-Out Time</lable>
        <input type="text" name="Visit_date" id="checkOutTime" class="form-control" placeholder="Out Time">
     </div>
  </div>
   <!--  <div class="col-lg-3 col-md-3 col-sm-12">
     <div class="form-group">
        <lable class="from_one1" for="name">Visit Date </lable>
        <input type="text" name="Visit_date" id="visitDate" class="form-control" placeholder="Visit Date">
     </div>
  </div> -->
</div>
<div class="row clearfix">
    
     <div class="col-lg-12 col-md-12 col-sm-12">
        <lable class="from_one1">Purpose :</lable>
        <div class="form-group">
            <textarea name="purpose" class="form-control" placeholder="Purpose"></textarea>
        </div>
    </div>
      
</div>
<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
     <div class="col-lg-1 ">
      <button type="button" class="btn btn-raised btn-primary " title="Save">Save
      </button>
    </div>
     <div class="col-lg-1 ">
      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Cancel
      </button>
    </div>
   
</div>


<script type="text/javascript">

    $(document).ready(function() {
        $('.select2').select2();
    });

    $(document).ready(function () {
         jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#visitor-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation  rules 
             ------------------------------------------ */

            rules: {
                staff_name: {
                    required: true
                },
                staff_profile_img: {
                    required: true
                },
                student_name: {
                    required: true
                },
                student_email: {
                    required: true,
                    email: true
                },  
            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                     element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

     

     
    });

</script>