<script type="text/javascript" src="http://v2rsolution.co.in/school/school-management/admin-panel/assets/plugins/momentjs/moment.js"></script>
<script type="text/javascript" src="http://v2rsolution.co.in/school/school-management/admin-panel/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
 
  {!! Html::script('public/assets/js/bootstrap-tagsinput.min.js') !!}
  {!! Html::script('public/assets/bundles/libscripts.bundle.js') !!} 

  {!! Html::script('public/assets/bundles/vendorscripts.bundle.js') !!}
  {!! Html::script('public/assets/plugins/autosize/autosize.js') !!}
    
  {!! Html::script('public/assets/plugins/dropzone/dropzone.js') !!}
  {!! Html::script('public/assets/plugins/momentjs/moment.js') !!}
    
  {!! Html::script('public/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') !!}

  {!! Html::script('public/assets/bundles/fullcalendarscripts.bundle.js') !!}
  {!! Html::script('public/assets/bundles/mainscripts.bundle.js') !!}
  {!! Html::script('public/assets/js/pages/calendar/calendar.js') !!}

  {!! Html::script('public/admin/assets/js/select2.full.js') !!}

  <!-- Jquery DataTable Plugin Js --> 
  {!! Html::script('public/assets/bundles/datatablescripts.bundle.js') !!}
  {!! Html::script('public/assets/js/pages/tables/jquery-datatable.js') !!}
  {!! Html::script('public/assets/js/pages/forms/basic-form-elements.js') !!}
  {!! Html::script('public/assets/plugins/jquery-validation/jquery.validate.js') !!}
  {!! Html::script('public/assets/plugins/jquery-validation/additional-methods.js') !!}
  {!! Html::script('public/assets/plugins/bootstrap-validator/bootstrapvalidator.min.js') !!}
  {!! Html::script('public/assets/js/daterangepicker.min.js') !!}

  {!! Html::script('public/assets/datatables/dataTables.buttons.min.js') !!}
  {!! Html::script('public/assets/datatables/jszip.min.js') !!}
  {!! Html::script('public/assets/datatables/pdfmake.min.js') !!}
  {!! Html::script('public/assets/datatables/vfs_fonts.js') !!}
  {!! Html::script('public/assets/datatables/buttons.html5.min.js') !!}
  {!! Html::script('public/assets/datatables/buttons.flash.min.js') !!}
  {!! Html::script('public/assets/datatables/buttons.print.min.js') !!}

<script type="text/javascript">

  jQuery(document).ready(function () {
    $("#item_info").change(function(){
        var item = $('#item_info').val();
        if(item == "Pen")
        {
            $('#ItemsDetailsInfoModal').modal('show');
        }
        if(item == "Notebooks")
        {
            $('#NotebookDetailsInfoModal').modal('show');
        }
    });
  });
  
</script>

<script type="text/javascript">

  jQuery(document).ready(function () {
    $("#student_name").change(function(){
    $('#transferStudentModel').modal('hide');
    $('#studentsdetails').show();
    $('#user').val('12345');
    $("#userunknown").hide();
    $("#show_table").hide();
    $("#no").hide();
    $('#staffsdetails').hide();
    });
  });
  
  jQuery(document).ready(function () {
    $("#staff_name").change(function(){
    $('#transferStudentModel').modal('hide');
    $('#staffsdetails').show();
    $('#user').val('288');
    $("#userunknown").hide();
    $("#show_table").hide();
    $("#no").hide();
    $('#staffsdetails').show();
    });
  });

  jQuery(document).ready(function () {
    $("#studentDetails").change(function(){
    $('#PersonModel').modal('hide');
    $("#studentdetails").show();
    $("#staffdetails").hide();
    $("#person").val('12345');
    });
  });
  jQuery(document).ready(function () {
    $("#staffDetails").change(function(){
    $('#PersonModel').modal('hide');
    $("#staffdetails").show();
    $("#studentdetails").hide();
    $("#person").val('288');
    });
  });
</script>
<script type="text/javascript">

 $('#staff_date').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false ,maxDate : new Date() }).on('change', function(e, date)
        {
        $("#show_date").text($(this).val());
        });
</script>
    <style type="text/css">
      .dataTables_filter{
      float: right !important;
      }
      #DataTables_Table_0_paginate{
      float: right;
      }
     
    </style>

    <!-- status fuction -->
    <script>
      /* When the user clicks on the button, 
      toggle between hiding and showing the dropdown content */
      function myFunction() {
      
          document.getElementById("myDropdown").classList.toggle("show");
      }
      
      // Close the dropdown if the user clicks outside of it
      window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {
      
          var dropdowns = document.getElementsByClassName("dropdown-content");
          var i;
          for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
              openDropdown.classList.remove('show');
            }
          }
        }
      }
    </script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#manage_purchase").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                entry_no: {
                    required: true,
                },
                date: {
                    required: true,
                },
                vendor: {
                    required: true,
                },
                category: {
                    required: true,
                },
                subcategory: {
                    required: true,
                },
                payment_mode: {
                    required: true,
                },
               
                
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

</script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#manage_print_invoice").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                from_date: {
                    required: true,
                },
                to_date: {
                    required: true,
                },
                voucher_no: {
                    required: true,
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });

    });

</script>
<script type="text/javascript"> 
   
  $(document).ready(function() {
   var max_fields      = 100; //maximum input boxes allowed
   var wrapper         = $(".input_fields_wrap"); //Fields wrapper
   var add_button      = $(".add_field_button"); //Add button ID
   var x = 1; //initlal text box count
   $(add_button).on("click",function(e){ //on add input button click
     e.preventDefault();
     if(x < max_fields){ //max input box allowed
         x++; //text box increment
         $(wrapper).append('<div> <div class="row seprateclass"><div class="col-lg-3 "><div class="form-group"><lable class="from_one1" for="name">Name of installment</lable><input type="text" name="name" id="name" class="form-control" placeholder= "Name of installment"></div></div><div class="col-lg-3 "><div class="form-group"><lable class="from_one1" for="name">Effective date of installment</lable><input type="text" name="name" id="date_installation" class="form-control" placeholder="Effective date of installment"></div></div><div class="col-lg-3 "><div class="form-group"><lable class="from_one1" for="name">Amount of installment</lable><input type="text" name="name" id="name" class="form-control" placeholder = "Amount of installment"></div></div><button type="button" class="remove_field btn btn-danger float-right "><i class="fas fa-minus-circle"></i> Remove</button></div></div>'); //add input box
     }
   });
   
   $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
     e.preventDefault(); $(this).parent('div').remove(); x--;
   })
   });
   
   
</script>
<script type="text/javascript"> 
   
  $(document).ready(function() {
   var max_fields      = 100; //maximum input boxes allowed
   var wrapper         = $(".input_documents_wrap"); //Fields wrapper
   var document_more      = $(".add_document_more"); //Add button ID
   var x = 1; //initlal text box count
   $(document_more).on("click",function(e){ //on add input button click
     e.preventDefault();
     if(x < max_fields){ //max input box allowed
         x++; //text box increment
         $(wrapper).append('<div> <div class="row clearfix"><div class="col-lg-3"><div class="form-group"><span class="from_one1">Document Category :</span><select class="form-control show-tick select_form1" name="name"><option value="">Document Category </option><option value="1">Action & Advanture</option><option value="2">Drama</option><option value="3">Mystery</option><option value="4">Romance</option><option value="5">Horror</option></select></div></div><div class="col-lg-3"><div class="form-group"><span class="from_one1">Expiry Date:</span><input type="text" name="expiry_date" class="form-control" id="expiry_date" placeholder="Expiry Date"></div></div><div class="col-lg-3"><span class="from_one1">Document Upload :</span><label for="file1" class="field file"><div class="form-group"><input type="file" class="gui-file" name="document_file" id="student_document_file"><input type="hidden" class="gui-input" id="student_document_file" placeholder="Upload Photo" readonly=""></div></label></div><button type="button" style="width:110px!important;height:40px!important;padding:0px!important;margin-top: 20px!important;" class="remove_field btn btn-danger float-right" ><i class="fas fa-minus-circle"></i> Remove</button></div></div>'); //add input box
     }
   });
   
   $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
     e.preventDefault(); $(this).parent('div').remove(); x--;
   })
   });
   
   
</script>

<script type="text/javascript"> 
   
  $(document).ready(function() {
   var max_fields      = 100; //maximum input boxes allowed
   var wrapper         = $(".input_floor_wrap"); //Fields wrapper
   var document_more      = $(".add_floor_button"); //Add button ID
   var x = 1; //initlal text box count
   $(document_more).on("click",function(e){ //on add input button click
     e.preventDefault();
     if(x < max_fields){ //max input box allowed
         x++; //text box increment
         $(wrapper).append('<div class="row seprateclass" style="width:1115px;margin-left:1px;">  <div class="col-lg-2 col-md-2 col-sm-12"><lable class="from_one1">Floor No :</lable><div class="form-group"><input class="form-control positive-numeric-only" id="id-blah1" min="0" name="floor_no" type="number" value="" placeholder="Floor No"></div></div><div class="col-lg-2 col-md-2 col-sm-12"><lable class="from_one1">No Of Room :</lable><div class="form-group"><input class="form-control positive-numeric-only" id="id-blah1" min="0" name="no_of_room" type="number" value="" placeholder="No Of Room"></div></div><div class="col-lg-6 col-md-5 col-sm-12 "><lable class="from_one1">Description of Floor :</lable><div class="form-group"><textarea rows="2" name="description" class="form-control no-resize" placeholder="Description of Floor"></textarea></div></div><button type="button" style="width:110px!important;height:40px!important;padding:0px!important;margin-top: 20px!important;" class="remove_field btn btn-danger float-right" ><i class="fas fa-minus-circle"></i> Remove</button></div>'); //add input box
     }
   });
   
   $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
     e.preventDefault(); $(this).parent('div').remove(); x--;
   })
   });
   
   
</script>

<script type="text/javascript">
  // Date Picker      
   $('#date_installation').bootstrapMaterialDatePicker({
   weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
   
  $(document).ready(function() {
   var max_fields      = 100; //maximum input boxes allowed
   var wrapper         = $(".input_grade_wrap"); //Fields wrapper
   var add_button      = $(".add_grade_button"); //Add button ID
   
   var x = 1; //initlal text box count
   $(add_button).on("click",function(e){ //on add input button click
     e.preventDefault();
     if(x < max_fields){ //max input box allowed
         x++; //text box increment
         $(wrapper).append('<div class="row rowAddmore" style="width:1115px;margin-left:0px;"><div class="headingcommon  col-lg-12">Grades :-</div><div class="col-lg-3 col-sm-12"><lable class="from_one1">Maximum :</lable><div class="form-group"><input class="form-control positive-numeric-only" id="id-blah1" min="0" name="maximum" type="number" value="" placeholder="Maximum"></div></div><div class="col-lg-3 col-sm-12"><lable class="from_one1">Minimum :</lable><div class="form-group"><input class="form-control positive-numeric-only" id="id-blah1" min="0" name="minimum" type="number" value="" placeholder="Minimum"></div></div><div class="col-lg-3 col-sm-12"><lable class="from_one1">Grade :</lable><div class="form-group"><input type="text" name="grade" class="form-control" placeholder="Grade"></div></div><div class="col-lg-1"></div><button style="width:107px;height:42px;margin-left:63px;margin-top:21px;" class="remove_field btn btn-danger float-right "><i class="fas fa-minus-circle"></i> Remove</button> <div class="col-lg-3"><lable class="from_one1">Grade Point :</lable><div class="form-group"><input class="form-control positive-numeric-only" id="id-blah1" min="0" name="grade_point" type="number" value="" placeholder="Grade Point"></div></div><div class="col-lg-6"><lable class="from_one1">Remark</lable><div class="form-group"><textarea class="form-control" placeholder="Remark"></textarea></div></div></div>'); //add input box


        $('#selectpicker'+x).selectpicker('refresh');
     }
   });
   
   $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
     e.preventDefault(); $(this).parent('div').remove(); x--;
   })
   });
   
   
</script>

<script type="text/javascript">
   
  $(document).ready(function() {
   var max_fields      = 100; //maximum input boxes allowed
   var wrapper         = $(".input_experience_wrap"); //Fields wrapper
   var add_button      = $(".add_experience_block"); //Add button ID
   
   var x = 1; //initlal text box count
   $(add_button).on("click",function(e){ //on add input button click
     e.preventDefault();
     if(x < max_fields){ //max input box allowed
         x++; //text box increment
         $(wrapper).append('<div class="row rowAddmore" style="width:1115px;margin-left:0px;"> <div class="col-lg-10 col-md-6 col-sm-12 text_area_desc"><lable class="from_one1">{!! trans('language.staff_experience') !!} :</lable><div class="form-group">{!! Form::textarea('staff_experience', old('staff_experience',isset($staff['staff_experience']) ? $staff['staff_experience']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_experience'), 'id' => 'staff_experience', 'rows'=> 1]) !!}</div>@if ($errors->has('staff_experience')) <p class="help-block">{{ $errors->first('staff_experience') }}</p> @endif</div><button style="width:115px;height:42px;margin-left:50px;margin-top:36px;" class="remove_field btn btn-danger float-right "><i class="fas fa-minus-circle"></i> Remove</button></div>'); //add input box


        $('#selectpicker'+x).selectpicker('refresh');
     }
   });
   
   $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
     e.preventDefault(); $(this).parent('div').remove(); x--;
   })
   });
   
   
</script>
<script type="text/javascript"> 
   
  $(document).ready(function() {
   var max_fields      = 100; //maximum input boxes allowed
   var wrapper         = $(".input_class_wrap"); //Fields wrapper
   var add_button      = $(".add_class_button"); //Add button ID
   var x = 1; //initlal text box count
   $(add_button).on("click",function(e){ //on add input button click
     e.preventDefault();
     if(x < max_fields){ //max input box allowed
         x++; //text box increment
         count= x;
         $(wrapper).append('<div><div class="row" style="width:1110px;margin:0px;margin-top:15px;"><div class="col-lg-3 padding-0"><div class="form-group"><lable class="from_one1" for="name">Class</lable><select class="form-control show-tick select_form1 select2" name="classes" id="show_class_subjects'+x+'" onchange="showSubjects(count)"><option value="">Class</option><option value="1">1st</option><option value="2">2nd</option><option value="3">3rd</option><option value="4">4th</option><option value="10th">10th</option><option value="11th">11th</option><option value="12th">12th</option></select></div></div><button type="button" style="margin-left: 14px;height: 40px;margin-top: 23px;padding:0px 15px;" class="remove_field btn btn-danger float-right "><i class="fas fa-minus-circle"></i> Remove</button><div class="col-lg-9"></div><div id="show_subjects'+x+'" style="display: none;"><div class="row"><?php $counter1 = 1;for ($i=1; $i < 5 ; $i++){if ($counter1 == 1){$addClass1 = 'active';}else{$addClass1 = '';}?><div class="col-lg-3" style="margin-top:30px;"><div class="checkbox"><input id="checkbox<?php echo $counter1; ?>" type="checkbox"><label for="checkbox<?php echo $counter1;  ?>">Subject<?php echo $counter1;  ?></label></div></div><?php $counter1++; } ?></div></div></div></div>'); //add input box

          $('#show_class_subjects'+x).selectpicker('refresh');
     }
   });
   
   $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
     e.preventDefault(); $(this).parent('div').remove(); x--;
   })
   });
  function showSubjects(count) {
    document.getElementById("show_subjects"+count).style.display = "block";
  }

</script>
<script type="text/javascript"> 
   
  $(document).ready(function() {
   var max_fields      = 100; //maximum input boxes allowed
   var wrapper         = $(".input_schedule_wrap"); //Fields wrapper
   var add_button      = $(".add_schedule_button"); //Add button ID
   var x = 1; //initlal text box count
   $(add_button).on("click",function(e){ //on add input button click
     e.preventDefault();
     if(x < max_fields){ //max input box allowed
         x++; //text box increment
         $(wrapper).append('<div><div class="row" style="width:1120px;"><div class="col-lg-2"><div class="form-group"><lable class="from_one1">Subject Name</lable><input type="text" name="subject_name" placeholder="Subject Name" class="form-control"></div></div><div class="col-lg-2"><div class="form-group"><lable class="from_one1">Teacher Name</lable><input type="text" name="teacher_name" placeholder="Teacher Name" class="form-control"></div></div><div class="col-lg-2"><lable class="from_one1">Date :</lable><div class="form-group"><input type="text" name="start_schedule_date" class="form-control" id="start_schedule_date" placeholder="Start Date"></div></div><div class="col-lg-2"><lable class="from_one1">Start Time :</lable><div class="form-group"><input type="text" name="start_schedule_date" class="form-control" id="start_schedule_date" placeholder="Start Date"></div></div><div class="col-lg-2"><lable class="from_one1">End Time :</lable><div class="form-group"><input type="text" name="start_schedule_date" class="form-control" id="start_schedule_date" placeholder="Start Date"></div></div><button type="button" style="height: 50px;margin-top: 15px;" class="remove_field btn btn-danger float-right "><i class="fas fa-minus-circle"></i> Remove</button></div></div></div>'); //add input box
     }
   });
   
   $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
     e.preventDefault(); $(this).parent('div').remove(); x--;
   })
   });
  $()
</script>

<script type="text/javascript"> 
   $(document).ready(function() {
   var max_fields      = 100; //maximum input boxes allowed
   // var add_button      = $(".add_row_details"); //Add button ID

   var x = 0; //initlal text box count
   $(document).on("click",'.add_row_details',function(e){ //on add input button click
     e.preventDefault();
     var item = $("#value12").val(); 
     var rate = $("#rate").val();
     var quantity = $("#quantity").val();
     var amount = rate*quantity;
     $("#lbltotal").text(amount+'.00');
        $("#txttax").val('0.00');
        $("#lbltax").text('0.00');
        $("#txtdiscount").val('0.00');
        $("#lbldiscount").text('0.00');
        $("#lblgross").text(amount+'.00');
        $("#lblnet").text(amount+'.00');
      
  

     if(x < max_fields && item != ""){ //max input box allowed
         x++; //text box increment
         $("#show_table11 tbody").append('<tr id="row-'+x+'"><td style="width: 290px;">'+item+'</td><td class="text-center"> '+rate+'</td><td class="text-center">'+quantity+'</td><td class="text-center amount">'+amount+'</td><td class="text-center"><button rel='+x+' class="btn btn-icon btn-neutral btn-icon-mini remove_field" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button></td></tr>'); 
         //add input box
            var item = $("#value12").val("").trigger('change');
            var unit = $("#unit").text(''); 
            var available = $("#num").text('');
            var rate = $("#rate").val('');
            var quantity = $("#quantity").val('');
            var amount = $("#amount").val('');

             $(".remove_field").on("click", function(e){ //user click on remove text
                var id = $(this).attr('rel');    
                $("#row-"+id).remove(); 
             });
     }

        var amount23 = 0;
     $('.amount').each(function(e) {
        var amount1 = parseInt($(this).text());
        amount23 = amount23 + amount1;

        $("#lbltotal").text(amount23+'.00');
        $("#txttax").val('0.00');
        $("#lbltax").text('0.00');
        $("#txtdiscount").val('0.00');
        $("#lbldiscount").text('0.00');
        $("#lblgross").text(amount23+'.00');
        $("#lblnet").text(amount23+'.00');
     });
 });
             $('#round_off').on('change', function(e) {
                var round = $('#round_off').val();
                var test = $("#lblnet").text();
                var total_round = parseInt(round) + parseInt(test);
                if(round != "") 
                {
                   $("#lblnet").text(total_round+'.00');
                }
                else
                {                    
                    var demo = $("#lblgross").text();
                    // $("#lblnet").text(' ');   
                    $("#lblnet").text(demo);   
                }
                
             });
   // });
});
</script>

<script type="text/javascript"> 
   $(document).ready(function() {
   var max_fields      = 100; //maximum input boxes allowed
   var add_button      = $(".add_sales_details"); //Add button ID

   var x = 0; //initlal text box count
   $(add_button).on("click",function(e){ //on add input button click
     e.preventDefault();
     var main_item = $("#item_info").val(); 
     var item = $("#hidden_item_store").val();       
     var quantity = $("#quantity").val(); 
     var rate = $("#rate").text();
     var amount = $("#amount").text();
     $("#lbltotal").text(amount+'.00');
     $("#txttax").val('0.00');
     $("#lbltax").text('0.00');
     $("#txtdiscount").val('0.00');
     $("#lbldiscount").text('0.00');
     $("#lblgross").text(amount+'.00');
     $("#roundoff").val('0.00');
     $("#lblnet").text(amount+'.00');
     if(x < max_fields && item != ""){ //max input box allowed
         x++; //text box increment
         $("#show_sales_table tbody").append('<tr id="row-'+x+'"><td style="width: 290px;">'+item+'</td><td class="text-center">'+quantity+'</td><td class="text-center">'+rate+'</td><td class="text-center amount">'+amount+'</td><td class="text-center"><button rel='+x+' class="btn btn-icon btn-neutral btn-icon-mini remove_field" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button></td></tr>'); 
         //add input box
            var main_item = $("#item_info").val('').trigger('change'); 
            var item = $("#hidden_item_store").val(''); 
            var unit = $("#unit").text(''); 
            var available = $("#num").text('');
            var rate = $("#rate").text('');
            var quantity = $("#quantity").val('');
            var amount = $("#amount").text('');

             $(".remove_field").on("click", function(e){ //user click on remove text
                var id = $(this).attr('rel');    
                $("#row-"+id).remove(); 
             });
     }

      var amount23 = 0;
     $('.amount').each(function(e) {
        var amount1 = parseInt($(this).text());
        amount23 = amount23 + amount1;

        $("#lbltotal").text(amount23+'.00');
        $("#txttax").val('0.00');
        $("#lbltax").text('0.00');
        $("#txtdiscount").val('0.00');
        $("#lbldiscount").text('0.00');
        $("#lblgross").text(amount23+'.00');
        $("#lblnet").text(amount23+'.00');
     });

   });
});
</script>

<script type="text/javascript"> 
   
  $(document).ready(function() {
   var max_fields      = 100; //maximum input boxes allowed
   // var wrapper         = $(".input_journal_entries_wrap"); //Fields wrapper
   var add_button      = $(".add_journal_entries"); //Add button ID
   var x = 1; //initlal text box count
   $(add_button).on("click",function(e){ //on add input button click
     e.preventDefault();
     if(x < max_fields){ //max input box allowed
         x++; //text box increment
         $("#dummy tbody").append('<tr id="row-'+x+'"><td>'+x+'</td><td><div class="row"><div class="col-lg-3"><div class="form-group"><lable class="from_one1">Main Heads</lable><select class="form-control show-tick select_form1 select2" name="" id=""><option value="">Select Head</option><?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?><option value="<?php echo $count; ?>">Head- <?php echo $count; ?></option><?php $count++; } ?></select></div></div><div class="col-lg-3"><div class="form-group"><lable class="from_one1">Sub Heads</lable><select class="form-control show-tick select_form1" name="" id=""><option value="">Select Subhead</option><?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?><option value="<?php echo $count; ?>">Subhead- <?php echo $count; ?></option><?php $count++; } ?></select></div></div><div class="col-lg-3"><div class="form-group"><lable class="from_one1">Group</lable><select class="form-control show-tick select_form1 select2" name="" id=""><option value="">Select Group</option><?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?><option value="<?php echo $count; ?>">Group- <?php echo $count; ?></option><?php $count++; } ?></select></div></div><div class="col-lg-3"><div class="form-group"><lable class="from_one1">Head</lable><select class="form-control show-tick select_form1 select2" name="" id=""><option value="">Select Head</option><?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?><option value="<?php echo $count; ?>">Head- <?php echo $count; ?></option><?php $count++; } ?></select></div></div></div></td><td><div class="form-group"><input type="text" name="" class="form-control  saveBtn debit" placeholder="Debit" id="debit'+x+'" counter='+x+' rel='+x+'></div></td><td><div class="form-group"><input type="text" name="" class="form-control saveBtn credit" placeholder="Credit" counter='+x+' id="credit'+x+'" rel='+x+'></div></td><td><button rel='+x+' class="btn btn-icon btn-neutral btn-icon-mini remove_field" title="Delete"><i class="zmdi zmdi-delete"></i></button></td></tr>'); 
         //add input box

             $('.debit').on("change", function(e) {
                var id = $(this).attr('counter');
                $("#credit"+id).val('0.00');

             });

             $(".credit").on("change", function(e) {
                var id = $(this).attr('counter');
                $("#debit"+id).val('0.00');

             });

             $(".remove_field").on("click", function(e){ //user click on remove text
                var id = $(this).attr('rel');    
                $("#row-"+id).remove(); 
             });
     }

   });

   
    $('.debit').on("change", function(e) {
        var id = $(this).attr('rel');
        $("#credit"+id).val('0.00');

    });

    $(".credit").on("change", function(e) {
        var id = $(this).attr('rel');
        $("#debit"+id).val('0.00');

    });
   });
   
   
   
</script>

<script type="text/javascript">
 
  $('#student_reg_date').bootstrapMaterialDatePicker({
     weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
   $('#student_dob').bootstrapMaterialDatePicker({
     weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
   $('#cheque_date').bootstrapMaterialDatePicker({
     weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
   
   $('#expiry_date').bootstrapMaterialDatePicker({
     weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
   $('#student_privious_tc_date').bootstrapMaterialDatePicker({
     weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
   $('#staff_dob').bootstrapMaterialDatePicker({
     weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
    $('#vehicle_date').bootstrapMaterialDatePicker({
     weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
  $('#date_installation').bootstrapMaterialDatePicker({
   weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
  $('#amount_Date').bootstrapMaterialDatePicker({
   weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
  $('#visitDate').bootstrapMaterialDatePicker({
   weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
  $('#checkInTime').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid(); });
        $('#checkOutTime').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid(); });
        
  $('#shift_start_time').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid(); });
        $('#shift_end_time').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid(); });
    $('#Form_Date').bootstrapMaterialDatePicker({
     weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
    $('#To_Date').bootstrapMaterialDatePicker({
     weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
    $('#receipt_date_range').bootstrapMaterialDatePicker({
     weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
    $('#session_end_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false}) .on('change', function(ev) {
            $(this).valid();  
        });
        $('#session_start_date').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false}).on('change', function(e, date)
        {
        $('#session_end_date').bootstrapMaterialDatePicker('setMinDate', date);
        $(this).valid();  
        });

         $('#student_leave_to_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false });
        $('#student_leave_from_date').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false}).on('change', function(e, date)
        {
        $('#student_leave_to_date').bootstrapMaterialDatePicker('setMinDate', date);
        });
        $('#effectiveDate').bootstrapMaterialDatePicker({
   weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
        // Date picker
        $('#competition_date').bootstrapMaterialDatePicker({ date: true,time: false });


        $('#leavingDate').bootstrapMaterialDatePicker({ weekStart : 0,time: false}) .on('change', function(ev) {
            $(this).valid();  
        });
        $('#joiningDate').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false}).on('change', function(e, date)
        {
        $('#leavingDate').bootstrapMaterialDatePicker('setMinDate', date);
        $(this).valid();  
        });

         $('#end_schedule_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false}) .on('change', function(ev) {
            $(this).valid();  
        });
        $('#start_schedule_date').bootstrapMaterialDatePicker({ weekStart : 0 ,time: false}).on('change', function(e, date)
        {
        $('#end_schedule_date').bootstrapMaterialDatePicker('setMinDate', date);
        $(this).valid();  
        });
         $('.schedule_date').bootstrapMaterialDatePicker({
     weekStart: 0, format: 'DD/MM/YYYY',time : false
   });


  $('#start_schedule_time').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid(); });
  $('#end_schedule_time').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid(); });
</script>
<script type="text/javascript">
   // Date Picker      
   $('#taskDate').bootstrapMaterialDatePicker({
   weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
   $('#date').bootstrapMaterialDatePicker({
   weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
</script>
<script type="text/javascript">
   $('#date_start').bootstrapMaterialDatePicker({
     weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
    $('#pdate').bootstrapMaterialDatePicker({
     weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
   
   $('#selectcheque').on('change', function() {
   
   if(this.value == "Cheque") {
     $('#showbank').show();
   
   } else {
     $('#showbank').hide();
   
   }
   });
   
</script>
<script type="text/javascript">
$('#date_start').bootstrapMaterialDatePicker({
    weekStart: 0, format: 'DD/MM/YYYY',time : false
}).on('change', function(e, date) {
    $('#date_end').bootstrapMaterialDatePicker('setMinDate', date);
});
$('#date_end').bootstrapMaterialDatePicker({
    weekStart: 0, format: 'DD/MM/YYYY',time: false
});

</script>
<script type="text/javascript">
   // Date Picker      
   $('#chequeDate').bootstrapMaterialDatePicker({
   weekStart: 0, format: 'DD/MM/YYYY',time : false
   });
   
</script>
<script type="text/javascript">
  $('#dropbtn').on('click', 'dropbtnAction', function () {
      $(this).toggleClass('active');
  });
</script>

<script type="text/javascript">
   $('#fees_particular_name').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
});
</script>

 <script type="text/javascript">
    
        $(function() {
            $("#check_all").on("click", function() {
                $(".check").prop("checked", $(this).prop("checked"));
            });     

            $(".check").on("click", function() {
                var flag = ( $(".check:checked").length == $(".check").length) ? true : false
                $("#check_all").prop("checked", flag);   
            });
        });
</script>
<script type="text/javascript">
  $(function() {
    
  $('input[name="datetimes"]').daterangepicker({

   timePicker: true,
   startDate: moment().startOf('hour'),
   endDate: moment().startOf('hour').add(32, 'hour'),
   locale: {
     format: 'M/DD hh:mm A'
   }
  });
  });
</script>   

</body>
</html>