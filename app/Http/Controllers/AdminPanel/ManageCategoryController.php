<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Category\Category; // Model
use Yajra\Datatables\Datatables;

class ManageCategoryController extends Controller
{
    /** 
	 *  Add Page of Category
	**/
	public function add(Request $request, $id = NULL) 
	{
	 	$category 		     = [];
	 	$getCategoryData     = [];
        $getCategoryData11   = [];
	 	$loginInfo           = get_loggedin_user_data();
        $getCategoryList     = Category::get();
        // p($getCategoryList);
	 	// $getCategoryData = get_all_category(0);
        // p($getCategoryData);
	 	if(!empty($id))
	 	{
	 		$decrypted_category_id 	= get_decrypted_value($id, true);
        	$category      			= Category::Find($decrypted_category_id);
        	$encrypted_category_id  = get_encrypted_value($category->category_id, true);
        	$save_url    			= url('admin-panel/inventory/manage-category-save/'. $encrypted_category_id);
        	$submit_button  		= 'Update';
	 	}
	 	else 
	 	{
	 		$save_url    	= url('admin-panel/inventory/manage-category-save');
	 		$submit_button  = 'Save';
	 	}
        $getCategoryData                    = get_all_category(0);
	 	$getCategoryData11['']  = add_blank_option($getCategoryData, 'Select Category');
        $data                 = array(
            'login_info'      => $loginInfo,
            'save_url'        => $save_url,  
            'category'		  => $category,	
            'submit_button'	  => $submit_button,
            'getCategoryData' => $getCategoryData,
            'getCategoryData11'=> $getCategoryData11
        );
        return view('admin-panel.inventory.manage-category')->with($data);
    }   

    /**
     *	Add & Update of Category
    **/
    public function save(Request $request, $id = NULL)
    {
    	$loginInfo      			= get_loggedin_user_data();
        $decrypted_category_id		= get_decrypted_value($id, true);
        $admin_id 					= $loginInfo['admin_id'];
        $category_id 				= Category::Find($decrypted_category_id);
        if(!empty($id))
        {
        	$category = array(
            'category_level'	=> $request['category'],
            'category_name' 	=> $request['category_name']
        );
        Category::where('category_id', $category_id['category_id'])->update($category);
        return redirect('admin-panel/inventory/manage-category');
        }
        else
        {

	        $category = New Category;
	        $category->admin_id       = $admin_id;
	        $category->update_by      = $admin_id;
	        $category->category_level = $request['category'];
	        $category->category_name  = $request['category_name'];
	        $category->save();
	    }    
        return redirect()->back();
    }
     	
    /**
     *	Get Category's Data fo view page
    **/
    public function anyData(Request $request)
    {

    	$loginInfo 			= get_loggedin_user_data();
        $getCategoryData    = get_all_category(0);
    	$category 			= Category::where(function($query) use ($request) 
        {
            $search_category = $request->get('search_category');
            $categoryName    = $request->get('categoryName');
            if ($search_category !='')
            {
                $query->where('category_level', '=', $search_category);
            }
           if (!empty($categoryName))
            {
                $query->where('category_name', 'Like', $categoryName.'%');
            }
        })->get();
        $all_categories = get_all_category(0);
        // p($category);
    	return Datatables::of($category, $all_categories)
        ->addColumn('category_level', function($category) use($all_categories) {
            return $all_categories[$category->category_level];
        })
    	->addColumn('action', function($category) use($request) {
    		$encrypted_category_id  = get_encrypted_value($category->category_id, true);
            
    		if($category->category_status == 0) {

                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"> <i class="fas fa-plus-circle"></i> </div>';
            }
      		return '<div class="text-center">
      				<a href="manage-category-status/'.$status .'/'.$encrypted_category_id.'">'.$statusVal.'</a>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.url('admin-panel/inventory/manage-category/'.$encrypted_category_id.'').'"><i class="zmdi zmdi-edit"></i></a></button>
      				<button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-manage-category/' . $encrypted_category_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></button></div>
      			';

    	})->rawColumns(['action' => 'action'])->addIndexColumn()
    	->make(true); 
    	return redirect('/inventory/manage-category');

    } 

    /** 
     *	Change Status of Category
	**/
	public function changeStatus($status,$id) 
	{
		$category_id 		= get_decrypted_value($id, true);
        $category 		  	= Category::find($category_id);
        if ($category)
        {
            $category->category_status  = $status;
            $category->save();
            $success_msg = "Category status update successfully!";
            return redirect('admin-panel/inventory/manage-category')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Category not found!";
            return redirect()->back()->withErrors($error_message);
        }
	}

	/**
	 *	Destroy Data of Category
	**/
	public function destroy($id) {
		$category_id 		= get_decrypted_value($id, true);
        $category 		  	= Category::find($category_id);
        if ($category)
        {
            $category->delete();
            $success_msg = "Category deleted successfully!";
            return redirect('admin-panel/inventory/manage-category')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Category not found!";
            return redirect()->back()->withErrors($error_message);
        }
	} 
}
