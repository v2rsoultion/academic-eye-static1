
<?php $__env->startSection('content'); ?>
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2><?php echo trans('language.menu_staff'); ?></h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right ">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/staff'); ?>"><?php echo trans('language.menu_staff'); ?></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/Staff/information.svg'); ?>" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Staff</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="<?php echo e(url('admin-panel/staff/add-staff')); ?>" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="<?php echo e(url('admin-panel/staff/view-staff')); ?>" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                        <!-- <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="#" title="">Option 1</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 2</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 3</a>
                              </li>
                            </ul>
                          </li>
                        </ul> -->
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="dashboard_div">
                        <div class="imgdash">
                          <img src="<?php echo URL::to('public/assets/images/Staff/Attendence.svg'); ?>" alt="Student" >
                          </div>
                          <h4 class="">
                            <div class="tableCell" style="height: 64px;">
                              <div class="insidetable">Attendance</div>
                            </div>
                          </h4>
                          <div class="clearfix"></div>
                          <a href="<?php echo e(url('admin-panel/staff/staff-attendance/add-staff-attendance')); ?>" class="float-left" title="Add ">
                            <i class="fas fa-plus"></i> Add 
                          </a>
                          <a href="<?php echo e(url('admin-panel/staff/staff-attendance/view-staff-attendance')); ?>" class="float-right" title="View ">
                            <i class="fas fa-eye"></i>View 
                          </a>
                          <div class="clearfix"></div>
                        <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="<?php echo e(url('/admin-panel/staff/staff-attendance/edit-staff-attendance')); ?>" title="">Edit Attendance</a>
                              </li>
                              
                              
                            </ul>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                        
                      </div>
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="<?php echo URL::to('public/assets/images/Staff/LeaveManagement.svg'); ?>" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Leave Management</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="<?php echo e(url('admin-panel/staff/staff-leave/add-staff-leave')); ?>" class="float-left" title="Add ">
                              <i class="fas fa-plus"></i> Add 
                            </a>
                            <a href="<?php echo e(url('admin-panel/staff/staff-leave/view-staff-leave')); ?>" class="float-right" title="View ">
                              <i class="fas fa-eye"></i>View 
                            </a>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="<?php echo URL::to('public/assets/images/Student.svg'); ?>" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Role Assignment</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="<?php echo e(url('admin-panel/staff/staff-role/add-staff-role')); ?>" class="float-left" title="Add ">
                                <i class="fas fa-plus"></i> Add 
                              </a>
                              <a href="<?php echo e(url('admin-panel/staff/staff-role/view-staff-roles')); ?>" class="float-right" title="View ">
                                <i class="fas fa-eye"></i>View 
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/Staff/Performance.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Performance</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <!-- <a href="" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a> -->
                                <a href="" class="cusa" title="View ">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <!-- <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Staff/Report.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Report</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a>
                                  <a href="" class="float-right" title="View ">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div> -->
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>