<?php

namespace App\Model\RoomNo;

use Illuminate\Database\Eloquent\Model;

class RoomNo extends Model
{
    protected $table      = 'room_no';
    protected $primaryKey = 'room_no_id';
}
