<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Holiday\Holiday;

use Yajra\Datatables\Datatables;

class HolidayController extends Controller
{
    /**
     *  View page for holidays
     *  @Shree on 17 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'   => trans('language.view_holiday'),
            'redirect_url' => url('admin-panel/holidays/view-holidays'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.holiday.index')->with($data);
    }

    /**
     *  Add page for holiday
     *  @Shree on 17 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    = [];
        $holiday = [];
        $loginInfo = get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_holiday_id = get_decrypted_value($id, true);
            
            $holiday              = Holiday::Find($decrypted_holiday_id);
            if (!$holiday)
            {
                return redirect('admin-panel/holidays/add-holiday')->withError('Holiday not found!');
            }
            $page_title           = trans('language.edit_holiday');
            $decrypted_holiday_id = get_encrypted_value($holiday->holiday_id, true);
            $save_url             = url('admin-panel/holidays/save/' . $decrypted_holiday_id);
            $submit_button        = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_holiday');
            $save_url      = url('admin-panel/holidays/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'holiday'       => $holiday,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/holidays/view-holidays'),
        );
        return view('admin-panel.holiday.add')->with($data);
    }

    /**
     *  Add and update holiday's data
     *  @Shree on 17 July 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo      = get_loggedin_user_data();
        $session        = get_current_session();
        $admin_id = $loginInfo['admin_id'];
        $decrypted_holiday_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $holiday = Holiday::find($decrypted_holiday_id);
            $admin_id = $holiday['admin_id'];
            if (!$holiday)
            {
                return redirect('/admin-panel/holidays/add-holiday/')->withError('Holiday not found!');
            }
            $success_msg = 'Holiday updated successfully!';
        }
        else
        {
            $holiday     = New Holiday;
            $success_msg = 'Holiday saved successfully!';
        }
        
        $session_id = $session['session_id'];
        
        $validatior = Validator::make($request->all(), [
                'holiday_name'   => 'required|unique:holidays,holiday_name,' . $decrypted_holiday_id . ',holiday_id,session_id,'.$session_id,
                'holiday_start_date'     => 'required|date|date_format:Y-m-d',
                'holiday_end_date'       => 'required|date|date_format:Y-m-d'
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {   
            DB::beginTransaction();
            try
            {
                $holiday->admin_id              = $admin_id;
                $holiday->update_by             = $loginInfo['admin_id'];
                $holiday->session_id            = $session['session_id'];
                $holiday->holiday_name          = Input::get('holiday_name');
                $holiday->holiday_start_date    = date('Y-m-d', strtotime(Input::get('holiday_start_date')));
                $holiday->holiday_end_date      = date('Y-m-d', strtotime(Input::get('holiday_end_date')));
                $holiday->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }

        return redirect('admin-panel/holidays/view-holidays')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 17 July 2018
    **/
    public function anyData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $holiday = Holiday::where(function($query) use ($request) 
        {
            $session       = get_current_session();
            $query->where('session_id', "=", $session['session_id']);
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('holiday_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('holiday_start_date', 'ASC')->get();
        return Datatables::of($holiday)
                
                ->addColumn('action', function ($holiday)
                {
                    $encrypted_holiday_id = get_encrypted_value($holiday->holiday_id, true);
                    if($holiday->holiday_status == 0) {
                        $status = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                            <div class="pull-left" ><a href="holiday-status/'.$status.'/' . $encrypted_holiday_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-holiday/' . $encrypted_holiday_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-holiday/' . $encrypted_holiday_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Destroy holiday's data
     *  @Shree on 17 July 2018
    **/
    public function destroy($id)
    {
        $holiday_id = get_decrypted_value($id, true);
        $holiday    = Holiday::find($holiday_id);
        if ($holiday)
        {
            $holiday->delete();
            $success_msg = "Holiday deleted successfully!";
            return redirect('admin-panel/holidays/view-holidays')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Holiday not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change holiday's status
     *  @Shree on 18 July 2018
    **/
    public function changeStatus($status,$id)
    {
        $holiday_id = get_decrypted_value($id, true);
        $holiday    = Holiday::find($holiday_id);
        if ($holiday)
        {
            $holiday->holiday_status  = $status;
            $holiday->save();
            $success_msg = "Holiday Status updated successfully!";
            return redirect('admin-panel/holidays/view-holidays')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Holiday not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
