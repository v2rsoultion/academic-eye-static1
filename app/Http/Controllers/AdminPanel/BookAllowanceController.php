<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\BookAllowance\BookAllowance; // Model
use Yajra\Datatables\Datatables;

class BookAllowanceController extends Controller
{
    /**
     *  Add/Edit page for Book Allowance
     *  @Pratyush on 13 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $book_allowance	= [];
        $loginInfo 		= get_loggedin_user_data();

        $book_allowance = BookAllowance::first();

        $page_title    					= trans('language.book_allowance');
        $encrypted_book_allowance_id  	= get_encrypted_value($book_allowance->book_allowance_id, true);
        $save_url               		= url('admin-panel/book-allowance/save/' . $encrypted_book_allowance_id);
        $submit_button = 'Save';
        
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'book_allowance' 	=> $book_allowance,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/book-allowance/edit-book-category'),
        );
        return view('admin-panel.book-allowance.add')->with($data);
    }

    /**
     *  Add and update Title's data
     *  @Pratyush on 20 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
    	
        $loginInfo      				= get_loggedin_user_data();
        $decrypted_book_allowance_id	= get_decrypted_value($id, true);
        if (!empty($id))
        {
            $book_allowance = BookAllowance::find($decrypted_book_allowance_id);

            if (!$book_allowance)
            {
                return redirect('/admin-panel/book-allowance/edit-book-category/')->withError('Book Allowance not found!');
            }
            $success_msg = 'Book Allowance updated successfully!';
        }
        else
        {
            $book_allowance     = New BookAllowance;
            $success_msg 		= 'Book Allowance saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'allow_for_staff'   	=> 'required',
                'allow_for_student'   	=> 'required',

        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $book_allowance->admin_id       		= $loginInfo['admin_id'];
                $book_allowance->update_by      		= $loginInfo['admin_id'];
                $book_allowance->school_id      		= $loginInfo['school_id'];
                $book_allowance->allow_for_staff 		= Input::get('allow_for_staff');
                $book_allowance->allow_for_student 		= Input::get('allow_for_student');
                $book_allowance->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/book-allowance/edit-book-category')->withSuccess($success_msg);
    }
}
