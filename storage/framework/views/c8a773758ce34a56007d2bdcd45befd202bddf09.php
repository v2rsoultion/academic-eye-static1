
<?php $__env->startSection('content'); ?>

<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>List of Cheques</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
           <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/cheque-details/view-cheque-details'); ?>">List of Cheques</a></li>
          <!-- <li class="breadcrumb-item"><a href="javascript:void(0);">Add</a></li> -->
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
             <!--  <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
              </div> -->
              <div class="body form-gap">
                <div class="row tabless" >
                 <form class="" action="" method="post" id="searchpanel" style="width: 100%;">
                    <div class="row">
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                     
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Date</lable>
                          <input type="text" name="name" id="cheque_date" class="form-control" placeholder="Date">
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Bank</lable>
                          <select class="form-control show-tick select_form1" name="classes" id="">
                            <option value="">Select</option>
                            <option value="A">Bank Of Baroda</option>
                            <option value="B">State Bank of India</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Submit">Search
                      </button>
                    </div>
                     <div class="col-lg-1 ">
                      <button type="reset" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Clear">Clear
                      </button>
                    </div>
                    </div>
                  </form>
                  <!--  DataTable for view Records  -->
                  <table class="table m-b-0 c_list">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Bank</th>
                        <th>Cheque No.</th>
                        <th>Cheque Date</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <tr>
                        <td>1</td>
                        <td>Bank Of Baroda</td>
                        <td>SBI24565</td>
                        <td>20 July 2018</td>
                        <td>Rs - 85452</td>
                        <td class="text-success">Cleared </td>
                        <td class="text-center">
                         <!-- <ul class="opendiv"> -->
                            <div class="dropdown">
                              <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="zmdi zmdi-label"></i>
                                <span class="zmdi zmdi-caret-down"></span>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li>
                                  <a href="#" title="">Pending</a>
                                </li>
                                <li>
                                  <a href="#" title="">Cancelled</a>
                                </li>
                                 <li>
                                  <a href="#" title="">Cleared</a>
                                </li>
                                 <li>
                                  <a href="#" title="">Bounce</a>
                                </li>
                              </ul>
                            </div>
                          <!-- </ul> -->
                        </td>
                          
                      
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Bank Of Baroda</td>
                        <td>SBI24565</td>
                        <td>20 July 2018</td>
                        <td>Rs - 85452</td>
                        <td class="text-warning">Pending  </td>
                        <td class="text-center">
                         <!-- <ul class="opendiv"> -->
                            <div class="dropdown">
                              <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="zmdi zmdi-label"></i>
                                <span class="zmdi zmdi-caret-down"></span>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li>
                                  <a href="#" title="">Pending</a>
                                </li>
                                <li>
                                  <a href="#" title="">Cancelled</a>
                                </li>
                                 <li>
                                  <a href="#" title="">Cleared</a>
                                </li>
                                 <li>
                                  <a href="#" title="">Bounce</a>
                                </li>
                              </ul>
                            </div>
                          <!-- </ul> -->
                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Bank Of Baroda</td>
                        <td>SBI24565</td>
                        <td>20 July 2018</td>
                        <td>Rs - 85452</td>
                        <td class="text-danger">Cancelled   </td>
                        <td class="text-center">
                         <!-- <ul class="opendiv"> -->
                            <div class="dropdown">
                              <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="zmdi zmdi-label"></i>
                                <span class="zmdi zmdi-caret-down"></span>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li>
                                  <a href="#" title="">Pending</a>
                                </li>
                                <li>
                                  <a href="#" title="">Cancelled</a>
                                </li>
                                 <li>
                                  <a href="#" title="">Cleared</a>
                                </li>
                                 <li>
                                  <a href="#" title="">Bounce</a>
                                </li>
                              </ul>
                            </div>
                          <!-- </ul> -->
                        </td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>Bank Of Baroda</td>
                        <td>SBI24565</td>
                        <td>20 July 2018</td>
                        <td>Rs - 85452</td>
                        <td class="text-danger">Bounce    </td>
                        <td class="text-center">
                         <!-- <ul class="opendiv"> -->
                            <div class="dropdown">
                              <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="zmdi zmdi-label"></i>
                                <span class="zmdi zmdi-caret-down"></span>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li>
                                  <a href="#" title="">Pending</a>
                                </li>
                                <li>
                                  <a href="#" title="">Cancelled</a>
                                </li>
                                 <li>
                                  <a href="#" title="">Cleared</a>
                                </li>
                                 <li>
                                  <a href="#" title="">Bounce</a>
                                </li>
                              </ul>
                            </div>
                          <!-- </ul> -->
                        </td>
                      </tr>
                     <?php $cou = 5; for ($i=0; $i < 15; $i++) {  ?> 
                      <tr>
                        <td><?php echo $cou; ?></td>
                        <td>Bank Of Baroda</td>
                        <td>SBI24565</td>
                        <td>20 July 2018</td>
                        <td>Rs - 85452</td>
                        <td class="text-success">Cleared </td>
                        <td class="text-center">
                         <!-- <ul class="opendiv"> -->
                            <div class="dropdown">
                              <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="zmdi zmdi-label"></i>
                                <span class="zmdi zmdi-caret-down"></span>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li>
                                  <a href="#" title="">Pending</a>
                                </li>
                                <li>
                                  <a href="#" title="">Cancelled</a>
                                </li>
                                 <li>
                                  <a href="#" title="">Cleared</a>
                                </li>
                                 <li>
                                  <a href="#" title="">Bounce</a>
                                </li>
                              </ul>
                            </div>
                          <!-- </ul> -->
                        </td>
                      </tr>
                    <?php $cou++; } ?> 
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Action Model  -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row tabless" >
          <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
          <div class="col-lg-12">
            <div class="form-group">
              <lable class="from_one1" for="name">Class</lable>
              <select class="form-control show-tick select_form1" name="classes" id="" multiple="multiple
                ">
                <option value="">Select Class</option>
                <option value="1">1st</option>
                <option value="2">2nd</option>
                <option value="3">3rd</option>
                <option value="4">4th</option>
                <option value="10th"> 10th</option>
                <option value="11th">11th</option>
                <option value="12th">12th</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>