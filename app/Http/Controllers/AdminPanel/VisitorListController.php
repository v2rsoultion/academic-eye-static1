<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VisitorListController extends Controller
{
     /**
     *  View page for Visitor List
     *  @Khushbu on 04 Sept 2018   
    **/
    public function index()
    {
        $loginInfo 		    = get_loggedin_user_data();
        
        $data 				= array(
            'page_title'    => trans('language.view_visitor'),
            'redirect_url'  => url('admin-panel/visitor/add-visitor'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.visitor.index')->with($data);
    }

    /**
     *  Add page for Visitor List
     *  @Khushbu on 04 Sept 2018   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        // $job        = [];
        if (!empty($id))
        {
            $page_title             = trans('language.edit_visitor');
            $save_url               = url('admin-panel/visitor/save/' . $decrypted_job_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_visitor');
            $save_url      = url('admin-panel/visitor/save');
            $submit_button = 'Save';
        }

        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/visitor/view-visitor'),
        );
        return view('admin-panel.visitor.add')->with($data);
    }
}
