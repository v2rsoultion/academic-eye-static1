<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddFieldStudentParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('student_parents')) { 
            Schema::table('student_parents', function (Blueprint $table) {
                $table->string('student_father_annual_salary',255)->nullable();
                $table->string('student_mother_annual_salary',255)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('student_parents')) { 
            Schema::table('student_parents', function (Blueprint $table) {
                $table->dropColumn('student_father_annual_salary');
                $table->dropColumn('student_mother_annual_salary');
            });
        }
    }
}
