
<?php $__env->startSection('content'); ?>
<style type="text/css">
  .card .body .table td,
.cshelf1 {
    width: 100px;
    height: auto !important;
}
.tabing {
    width: 25%;
    padding: 0px !important;
    border: 1px solid #ccc;
    border-radius: 5px;
    margin-bottom: 20px;
    /*background: #959ecf;*/
   }
   .theme-blush .nav-tabs .nav-link.active{
      color: #fff !important;
    border-radius: 0px !important;
    background: #1f2f60 !important;
    width: 94px;
   }
   .nav-tabs>.nav-item>.nav-link {
    width: 94px;
   }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Manage Sales Return</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/inventory'); ?>">Inventory</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/inventory/manage-sales-return'); ?>">Manage Sales Return</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body form-gap1">
               
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Sales Return :-</div> -->
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Entry No: </lable>
                          <input type="text" name="entry_no" id=" " value="SR-10" class="form-control" placeholder="Entry No" readonly="true">
                        </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Invoice No: </lable>
                          <input type="text" name="invoice_no" onchange="showTable()" value="SI-" class="form-control" placeholder="Entry No">
                        </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Date: </lable>
                          <input type="text" name="date" id="date" class="form-control" placeholder="Date">
                        </div>
                      </div>

                           <!-- <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Payment Mode</lable>
                            <select class="form-control show-tick select_form1" name="payment_mode" id="">
                              <option value="">Select Payment</option>
                              <option value="1">Cash</option>
                               <option value="1">Bank Names</option>
                                <option value="1">Credit</option>
                            </select>
                         </div>
                      </div> -->
                      
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">User: </lable>
                          <input type="text" name="" id="user" class="form-control" data-toggle="modal" data-target="#transferStudentModel" onchange="showInfo()">
                        </div>
                      </div>
                    </div>
                    <div class="row" id="no">
                      <div class="col-lg-12">
                        <div class="form-group">
                          <textarea class="form-control" rows="1" readonly="true"> </textarea>
                        </div>
                      </div>
                    </div>
                    <div class="row" id="userunknown" style="display: none;">
                      <div class="col-lg-12">
                        <div class="form-group">
                          <textarea class="form-control" readonly="true">User: Unknown</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="row" id="studentsdetails" style="display: none;">
                      <div class="col-lg-12" style="font-size: 16px;">
                        <div class="form-group">
                          <textarea class="form-control" rows="1" readonly="true">Class: 2 B Name: yusuf</textarea>
                        </div>
                        <br>
                        <p style="color: #a94442;">No Data</p>
                      </div>
                    </div>
                    <div class="row" id="staffsdetails" style="display: none;">
                      <div class="col-lg-12" style="font-size: 16px;">
                        <div class="form-group">
                          <textarea class="form-control" rows="1" readonly="true">Role:Principal    Name: P P MOHAMMED</textarea>
                        </div>
                        <br>
                        <p style="color: #a94442;">No Data</p>
                      </div>
                    </div>
                  </form>
          
                  <!--  DataTable for view Records  -->
                 
                    <div class="table-responsive" style="display: none;" id="show_table">
                    <table class="table table-bordered m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th style="width: 200px">No</th>
                        <th>Item</th>
                        <th style="width: 120px">Available Quantity</th>
                        <!-- <th style="width: 120px">Total:Stock Quantity</th> -->
                        <!-- <th>Total:Available</th> -->
                        <th>Rate(Per item)</th>
                        <th>Return Quantity</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                    <tbody>
            
                      <tr>
                        <td>1</td>
                      <td>Pen Black</td>
                      <td>10</td>
                     <!--  <td>10</td>
                      <td>0</td> -->
                      <td>10</td>
                      <td><input type="text" name="rate" id=" " class="form-control" placeholder="" onchange="showAmount()"></td>
                      <td id="amount"> </td>  
                      </tr>
                    </tbody>
                  </table>
                  <br>
                  <div class="row " id="totalAmount" style="display: none;">
                    <div class="col-lg-12 text-right" style="font-size: 16px;">
                      <label class="form-control" id="total" > </label>
                    </div>
                  </div>
                </div>
              <form id="manage_purchase">
                  <div class="row float-right" style="margin: 20px 0px;">
                  <div class="col-lg-2">
                    <button type="submit" class="btn btn-raised btn-primary" title="Save">Save
                    </button>
                  </div>
                </div>
              </form>
                
                     
              </div>
              

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
<!-- Content end here  -->

<script type="text/javascript">
    function showTable() {
       document.getElementById('show_table').style.display = "block";
       document.getElementById('userunknown').style.display = "block";
       document.getElementById('no').style.display = "none";
       document.getElementById('studentsdetails').style.display = "none";
       document.getElementById('staffsdetails').style.display = "none";
       $('#user').val('');
    }

    function showAmount() {
      $("#amount").html('30.00');
      $("#totalAmount").show();
      $("#total").html('Total: 30.00');
    }
</script>

<!-- Action Model  -->
<div class="modal fade" id="transferStudentModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Person</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <ul class="nav nav-tabs tabing">
         <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#student">Student</a></li>
         <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#staff">Staff</a></li>

      </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="student">
      <div class="row feecounterclass">
          <!-- <div class="col-lg-4">
            <div class="form-group">
                <lable class="from_one1">Department</lable>
                <select class="form-control show-tick select_form1" name="category" id="">
                  <option value="">--Select--</option>
                  <?php $count = 1; for ($i=0; $i < 4 ; $i++) {  ?>
                   <option value="<?php echo $count; ?>">Department <?php echo $count; ?></option>
                 <?php $count++; } ?>
                </select>
             </div>
          </div> -->
          <div class="col-lg-4">
            <div class="form-group">
                <lable class="from_one1">Class</lable>
                <select class="form-control show-tick select_form1" name="category" id="">
                  <option value="">--Select--</option>
                  <?php $count = 1; for ($i=0; $i < 4 ; $i++) {  ?>
                   <option value="<?php echo $count; ?>">Class <?php echo $count; ?></option>
                 <?php $count++; } ?>
                </select>
             </div>
          </div>
            
          <div class="col-lg-4">
            <div class="form-group">
                <lable class="from_one1">Name</lable>
                <select class="form-control show-tick select_form1" name="" id="student_name">
                  <option value="">--Select--</option>
                  <option>Amit</option>
                  <option>Aman</option>
                  <option>Sumit</option>
                </select>
             </div>
          </div>
      </div> 
    </div>
      <div class="tab-pane fade" id="staff">
      <div class="row feecounterclass">
          <div class="col-lg-4">
            <div class="form-group">
                <lable class="from_one1">Roles</lable>
                <select class="form-control show-tick select_form1" name="category" id="">
                  <option value="">--Select--</option>
                  <option>Principal</option>
                  <option>Vice Principal</option>
                  <option>Clerk</option>
                  <option>PTA</option>
                  <option>Data Ebtry OPERATOR</option>
                  <option>Management</option>
                </select>
             </div>
          </div>
           <div class="col-lg-4">
            <div class="form-group">
                <lable class="from_one1">Name</lable>
                <select class="form-control show-tick select_form1" name="category" id="staff_name">
                  <option value="">--Select--</option>
                  <option>Amit</option>
                  <option>Aman</option>
                  <option>Sumit</option>
                  <option>Pooja</option>
                </select>
             </div>
          </div>
      </div> 
    </div>
  </div>

      </div>
    </div>
  </div>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>