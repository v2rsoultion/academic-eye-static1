<?php $__env->startSection('content'); ?>
<style type="text/css">
    .table-responsive{
        overflow-x: visible !important;
    }
</style>
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>View Job List</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <!-- <a href="<?php echo url('admin-panel/recruitment/add-job'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a> -->
                <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/recruitment'); ?>"><?php echo trans('language.menu_recruitment'); ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/recruitment'); ?>">Select Candidates</a></li>
                <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/recruitment/view-job-list'); ?>">View Job List</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                             <!-- <div class="headingcommon  col-lg-12">View Job List :-</div> -->
                            <div class="body form-gap">
                                   <form>
                                    <div class="row clearfix">
                                     <div class="col-lg-3 col-md-3 col-sm-12">
                                        <!-- <lable class="from_one1">Select Medium:</lable>                  -->
                                            <select class="form-control show-tick select_form1" name="">
                                                <option value="">Select Medium</option>
                                                <option value="VII">Hindi</option>
                                                <option value="X">English</option>
                                            
                                            </select>
                                        </div>
                        
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                <?php echo Form::text('job_name', old('job_name', ''), ['class' => 'form-control ','placeholder'=>trans('language.job_names'), 'id' => 'job_name']); ?>

                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <?php echo Form::submit('Search', ['class' => 'btn btn-raised btn-primary ','name'=>'Search']); ?>

                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <?php echo Form::button('Clear', ['class' => 'btn btn-raised btn-primary ','name'=>'Clear', 'id' => "clearBtn"]); ?>

                                        </div>
                                    </div>
                               </form>
                                <hr>
                                
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th><?php echo e(trans('language.s_no')); ?></th>
                                                <th>Form </th>
                                                <th>Medium</th>
                                                <th><?php echo e(trans('language.job_names')); ?></th>
                                                <th><?php echo e(trans('language.job_no_of_vacancy')); ?></th>
                                                <th><?php echo e(trans('language.no_of_remaining_vacancy')); ?></th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Form-1</td>
                                                <td>Hindi</td>
                                                <td>Class Teacher</td>
                                                <td>447</td>
                                                <td>147</td>
                                                <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/recruitment/view-candidate'); ?>">View Candidates</a></li> 
                                                </ul> </div></td>
                                                <!-- <td>
                                                    <div id="myDropdown" class="dropdown-content list_dropdown1">
                                                    <a href="<?php echo url('admin-panel/recruitment/view-candidate'); ?>" class="btn btn-info view_profile">View Candidate</a>
                                                    </div>
                                                </td> -->
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Form-2</td>
                                                <td>English</td>
                                                <td>Accountant</td>
                                                <td>447</td>
                                                <td>147</td>
                                                 <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/recruitment/view-candidate'); ?>">View Candidates</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Form-2</td>
                                                <td>English</td>
                                                <td>Accountant</td>
                                                <td>447</td>
                                                <td>147</td>
                                                 <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/recruitment/view-candidate'); ?>">View Candidates</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Form-2</td>
                                                <td>Hindi</td>
                                                <td>Accountant</td>
                                                <td>447</td>
                                                <td>147</td>
                                                <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/recruitment/view-candidate'); ?>">View Candidates</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#recruitment-job-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '<?php echo e(url('admin-panel/recruitment/data')); ?>'
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'job_name', name: 'job_name'},
                {data: 'no_of_vacancy', name: 'no_of_vacancy'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }

</script>

<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {

    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>