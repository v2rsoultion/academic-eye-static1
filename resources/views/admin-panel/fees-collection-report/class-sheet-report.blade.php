@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
   .table-responsive {
    overflow-x: auto !important;
   }
   /*.card .body .table td, .cshelf1 {
    width: 100px !important;
   }*/
   td{
    padding: -1px !important;
   }
   
</style>
<!--  Main content here -->
<section class="content">
   <div class="block-header">
      <div class="row">
         <div class="col-lg-5 col-md-6 col-sm-12">
            <h2>Class Wise Report</h2>
         </div>
         <div class="col-lg-7 col-md-6 col-sm-12 line">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">{!! trans('language.menu_fee_collection') !!}</a></li>
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">Reports</a></li>
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection/report/view-reports') !!}">View Reports</a></li>
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection/report/view-reports/class-sheet-report') !!}">Class Wise Report</a></li>
            </ul>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row clearfix">
         <div class="col-lg-12" id="bodypadd">
            <div class="tab-content">
               <div class="tab-pane active" id="classlist">
                  <div class="card">
                  
                  <div class="body form-gap">
                   <!-- <div class="row"> -->
                  
                  <div class="table-responsive position">
                    
                  <!--  DataTable for view Records  -->
                  <table class="table  m-b-0 c_list table-bordered text-center" id="class-report" style="width: 100%;">
                    <thead class="bold">
                    <tr>
                      <td rowspan="2">CLASS</td>
                      <td colspan="5">JANUARY '18</td>
                      <td colspan="5">FEBRUARY '18</td>
                      <td colspan="5">MARCH '18</td>
                      <td colspan="5">APRIL '18</td>
                      <td colspan="5">MAY '18</td>
                      <td colspan="5">JUNE '18</td>
                      <td colspan="5">JULY '18</td>
                      <td colspan="5">AUGUST '18</td>
                      <td colspan="5">SEPTEMBER '18</td>
                      <td colspan="5">OCTOBER '18</td>
                      <td colspan="5">NOVEMBER '18</td>
                      <td colspan="5">DECEMBER '18</td>
                    </tr>
                    <tr>
                      <td>Jan - Total Paid</td>
                      <td>Jan - Total Due</td>
                      <td>Jan - Concession</td>
                      <td>Jan - Total Fine</td>
                      <td>Jan - Total Fees</td>

                      <td>Feb - Total Paid</td>
                      <td>Feb - Total Due</td>
                      <td>Feb - Concession</td>
                      <td>Feb - Total Fine</td>
                      <td>Feb - Total Fees</td>

                      <td>Mar - Total Paid</td>
                      <td>Mar - Total Due</td>
                      <td>Mar - Concession</td>
                      <td>Mar - Total Fine</td>
                      <td>Mar - Total Fees</td>

                      <td>Apr - Total Paid</td>
                      <td>Apr - Total Due</td>
                      <td>Apr - Concession</td>
                      <td>Apr - Total Fine</td>
                      <td>Apr - Total Fees</td>

                      <td>May - Total Paid</td>
                      <td>May - Total Due</td>
                      <td>May - Concession</td>
                      <td>May - Total Fine</td>
                      <td>May - Total Fees</td>

                      <td>Jun - Total Paid</td>
                      <td>Jun - Total Due</td>
                      <td>Jun - Concession</td>
                      <td>Jun - Total Fine</td>
                      <td>Jun - Total Fees</td>

                      <td>Jul - Total Paid</td>
                      <td>Jul - Total Due</td>
                      <td>Jul - Concession</td>
                      <td>Jul - Total Fine</td>
                      <td>Jul - Total Fees</td>

                      <td>Aug - Total Paid</td>
                      <td>Aug - Total Due</td>
                      <td>Aug - Concession</td>
                      <td>Aug - Total Fine</td>
                      <td>Aug - Total Fees</td>

                      <td>Sep - Total Paid</td>
                      <td>Sep - Total Due</td>
                      <td>Sep - Concession</td>
                      <td>Sep - Total Fine</td>
                      <td>Sep - Total Fees</td>

                      <td>Oct - Total Paid</td>
                      <td>Oct - Total Due</td>
                      <td>Oct - Concession</td>
                      <td>Oct - Total Fine</td>
                      <td>Oct - Total Fees</td>

                      <td>Nov - Total Paid</td>
                      <td>Nov - Total Due</td>
                      <td>Nov - Concession</td>
                      <td>Nov - Total Fine</td>
                      <td>Nov - Total Fees</td>

                      <td>Dec - Total Paid</td>
                      <td>Dec - Total Due</td>
                      <td>Dec - Concession</td>
                      <td>Dec - Total Fine</td>
                      <td>Dec - Total Fees</td>
                    </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Class 1 A</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                      </tr>
                      <tr>
                        <td>Class 2 B</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                        <td>10000</td>
                        <td>5000</td>
                        <td>320</td>
                        <td>100</td>
                        <td>4780</td>
                      </tr>
                    </tbody>
                  </table>
                
              </div>          
             </div>
            </div>
           </div>
          </div>
        </div>
       </div>
      </div>
  
</section>
<!-- Content end here  -->

<script type="text/javascript">
  $(document).ready(function () {
        var table = $('#class-report').DataTable({
           dom: 'Blfrtip',
            buttons: [
                'csvHtml5'
            ]
            
        });
    });

</script>
@endsection
