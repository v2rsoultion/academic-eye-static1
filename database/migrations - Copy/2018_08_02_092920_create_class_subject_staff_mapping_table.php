<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassSubjectStaffMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'class_subject_staff_mapping';
    protected $primaryKey = 'class_subject_staff_map_id';
    public function up()
    {
        if (!Schema::hasTable('class_subject_staff_mapping')) {
            Schema::create('class_subject_staff_mapping', function (Blueprint $table) {
                $table->increments('class_subject_staff_map_id');
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('subject_id')->unsigned()->nullable();
                $table->integer('staff_ids')->unsigned()->nullable();
                $table->timestamps();
            });
            Schema::table('class_subject_staff_mapping', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('class_subject_staff_mapping', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('class_subject_staff_mapping', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
            Schema::table('class_subject_staff_mapping', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');
            });
            Schema::table('class_subject_staff_mapping', function($table) {
                $table->foreign('subject_id')->references('subject_id')->on('subjects')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_subject_staff_mapping');
    }
}
