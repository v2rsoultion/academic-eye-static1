﻿
<?php $__env->startSection('content'); ?>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2> Response</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Transport</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Response</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/transport/one-way-transport/response'); ?>">View </a></li>
                </ul>                
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                           
                            <div class="body form-gap">
                                 <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <!-- <th>Student Photo</th> -->
                                                <th>Student Name</th>
                                                <th>Class-section</th>
                                                <th>Vehicle No</th>
                                                <th>Trip</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td> <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%"> Ankit Dave</td>
                                                <td>X-A</td>
                                                <td>AP-02-BK-1084</td>
                                                <td class="trip1">K.V School, Jaipur - St.Peter Private School, Jaipur</td>
                                                <td><span class="badge badge-success">Approved</span></td>
                                                <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                     <li> <a title="Approved">Approved</a></li> 
                                                     <li> <a title="disapproved">Disapproved</a></li> 
                                                </ul> </div></td>   
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td> <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%"> Ankit Dave</td>
                                                <td>X-A</td>
                                                <td>AP-02-BK-1084</td>
                                                <td class="trip1">K.V School, Jaipur - St.Peter Private School, Jaipur</td>
                                                <td><span class="badge badge-success">Approved</span></td>
                                                 <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                     <li> <a title="Approved">Approved</a></li> 
                                                     <li> <a title="disapproved">Disapproved</a></li> 
                                                </ul> </div></td> 
                                            </tr> 
                                            <tr>
                                                <td>3</td>
                                               <td> <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%"> Ankit Dave</td>
                                                <td>X-A</td>
                                                <td>AP-02-BK-1084</td>
                                                <td class="trip1">K.V School, Jaipur - St.Peter Private School, Jaipur</td>
                                                <td><span class="badge badge-success">Approved</span></td>
                                                 <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="Approved">Approved</a></li> 
                                                     <li> <a title="disapproved">Disapproved</a></li> 
                                                </ul> </div></td> 
                                            </tr> 
                                            <tr>
                                                <td>4</td>
                                                <td> <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%"> Ankit Dave</td>
                                                <td>X-A</td>
                                                <td>AP-02-BK-1084</td>
                                                <td class="trip1">K.V School, Jaipur - St.Peter Private School, Jaipur</td>
                                                <td><span class="badge badge-success">Approved</span></td>
                                                 <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="Approved">Approved</a></li> 
                                                     <li> <a title="disapproved">Disapproved</a></li> 
                                                </ul> </div></td>  
                                            </tr>                                             
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>