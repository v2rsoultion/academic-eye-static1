<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\File\File;
use Illuminate\Support\Facades\Hash;
use App\Model\StaffRoles\StaffRoles;
use Session as LSession;

class DashboardController extends Controller
{
    public function __construct()
    {
        
    }
    public function index()
    {
        // $a = Hash::make('parent@123!');
        // p($a);
        $loginInfo = get_loggedin_user_data();
        $get_staff_role = StaffRoles::where('staff_role_id', '=', $loginInfo['staff_role_id'])->first();
        // p($get_staff_role);
        $data = array(
            'page_title' => trans('language.dashboard'),
            'login_info' => $loginInfo,
            'get_staff_role' => $get_staff_role,
        );
        // p($loginInfo);
        if($loginInfo['admin_type'] == 0 || $loginInfo['admin_type'] == 1)
        {
            return view('admin-panel.menu.configuration')->with($data);
        }
        if($loginInfo['admin_type'] == 2 || $loginInfo['admin_type'] == 3 || $loginInfo['admin_type'] == 4)
        {
           
            return view('admin-panel.dashboard.index')->with($data);
        }
    }
}
