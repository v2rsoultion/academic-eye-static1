<?php

    namespace App\Http\Controllers\AdminPanel;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\backend\School;
    use App\Model\backend\Bank;
    use Symfony\Component\HttpFoundation\File\File;
    use Validator;
    use Auth;
    use Illuminate\Support\Facades\Input;
    use Datatables;
    use Illuminate\Support\Facades\DB;

    class BankController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index()
        {
            $data = array(
                'page_title' => trans('language.list_bank'),
                'redirect_url' => url('admin/bank/add'),
            );
            return view('backend.bank.index')->with($data);
        }


        public function add(Request $request, $id = NULL)
        {
a();
            $bank = [];
            $data = [];
            if (!empty($id))
            {
                $bank = Bank::Find($id);
                if (!$bank)
                {
                    return redirect('admin/bank')->withError('Bank not found!');
                }
                $page_title    = trans();
                $save_url      = url('admin/bank/save/' . $bank->bank_id);
                $submit_button = 'Update';
            }
            else
            {
                $page_title    = 'Create Bank';
                $save_url      = url('admin/bank/save');
                $submit_button = 'Save';
            }

            $data = array(
                'page_title'    => $page_title,
                'save_url'      => $save_url,
                'submit_button' => $submit_button,
                'bank'          => $bank,
                'redirect_url'       => url('admin/bank/'),
            );
            return view('backend.bank.add')->with($data);
        }

        public function save(Request $request, $id = NULL)
        {
            if (!empty($id))
            {
                $bank = Bank::find($id);

                if (!$bank)
                {
                    return redirect('/admin/bank/')->withError('Bank not found!');
                }
                $success_msg = 'Bank updated successfully!';
            }
            else
            {
                $bank        = New Bank;
                $success_msg = 'Bank saved successfully!';
            }

            $validatior = Validator::make($request->all(), [
                    'bank_name'   => 'required',
                    'ifsc_code'   => 'required|unique:banks,ifsc_code,' . $id . ',bank_id',
                    'bank_branch' => 'required|unique:banks,bank_branch,' . $id . ',bank_id',
            ]);

            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                $bank->bank_name   = Input::get('bank_name');
                $bank->ifsc_code   = Input::get('ifsc_code');
                $bank->bank_alias  = Input::get('bank_alias');
                $bank->bank_branch = Input::get('bank_branch');
                $bank->save();
            }

            return redirect('admin/bank')->withSuccess($success_msg);
        }

        public function anyData()
        {
            $bank      = Bank::orderBy('bank_name', 'ASC')->get();
            return Datatables::of($bank)
                    ->addColumn('action', function ($bank)
                    {
                        return '<a href="bank/add/' . $bank->bank_id . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>'
                            . ' <button id="' . $bank->bank_id . '" class="btn btn-xs btn-danger delete-button"><i class="glyphicon glyphicon-remove"></i>Delete</button>';
                    })->make(true);
        }

    }
    