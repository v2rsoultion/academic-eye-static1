<?php if(isset($class['class_id']) && !empty($class['class_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?>

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
   
</style>

<?php echo Form::hidden('class_id',old('class_id',isset($class['class_id']) ? $class['class_id'] : ''),['class' => 'gui-input', 'id' => 'class_id', 'readonly' => 'true']); ?>


<p class="red">
<?php if($errors->any()): ?>
    <?php echo e($errors->first()); ?>

<?php endif; ?>
</p>
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1"><?php echo trans('language.class_name'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('class_name', old('class_name',isset($class['class_name']) ? $class['class_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.class_name'), 'id' => 'class_name']); ?>

        </div>
        <?php if($errors->has('class_name')): ?> <p class="help-block"><?php echo e($errors->first('class_name')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.medium_type'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('medium_type', $class['arr_medium'],isset($class['medium_type']) ? $class['medium_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type']); ?>

                <i class="arrow double"></i>
            </label>
            <?php if($errors->has('medium_type')): ?> <p class="help-block"><?php echo e($errors->first('medium_type')); ?></p> <?php endif; ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.class_order'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::number('class_order', old('class_order',isset($class['class_order']) ? $class['class_order']: 0), ['class' => 'form-control ','placeholder'=>trans('language.class_order'), 'id' => 'class_order', 'min' => 0]); ?>

        </div>
        <?php if($errors->has('class_order')): ?> <p class="help-block"><?php echo e($errors->first('class_order')); ?></p> <?php endif; ?>
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']); ?>

        <a href="<?php echo url('admin-panel/dashboard'); ?>" ><?php echo Form::button('Cancel', ['class' => 'btn btn-raised btn-round']); ?></a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#class-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            // errorLabelContainer: '.errorTxt',

            /* @validation  rules 
             ------------------------------------------ */

            rules: {
                class_name: {
                    required: true,
                    lettersonly:true
                },
                medium_type: {
                    required: true,
                }
            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
    });

    

</script>