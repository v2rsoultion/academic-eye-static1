<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookCupboardshelfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'book_cupboardshelfs';
    protected $primaryKey = 'book_cupboardshelf_id';
    public function up()
    {
        if (!Schema::hasTable('book_cupboardshelfs')){ 
            Schema::create('book_cupboardshelfs', function (Blueprint $table) {
                $table->increments('book_cupboardshelf_id');
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->integer('book_cupboard_id')->unsigned()->default(0);
                $table->string('book_cupboardshelf_name', 255)->nullable();
                $table->integer('book_cupboard_capacity')->unsigned()->default(0);
                $table->text('book_cupboard_shelf_detail')->nullable();
                $table->tinyInteger('book_cupboardshelf_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('book_cupboardshelfs', function($table) {
                    $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
                });
            Schema::table('book_cupboardshelfs', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('book_cupboardshelfs', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_cupboardshelfs');
    }
}
