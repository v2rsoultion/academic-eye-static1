@if(isset($student['student_id']) && !empty($student['student_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

@if(!isset($student['admission_class_id']) && empty($student['admission_class_id']))
<?php $admit_section_readonly = false; $admit_section_disabled='disabled'; ?>
@else
<?php $admit_section_readonly = false; $admit_section_disabled=''; ?>
@endif

@if(!isset($student['current_class_id']) && empty($student['current_class_id']))
<?php $current_section_readonly = false; $current_section_disabled='disabled'; ?>
@else
<?php $current_section_readonly = false; $current_section_disabled=''; ?>
@endif

{!! Form::hidden('student_id',old('student_id',isset($student['student_id']) ? $student['student_id'] : ''),['class' => 'gui-input', 'id' => 'student_id', 'readonly' => 'true']) !!}
<style type="">

/*.theme-blush .btn-primary {
    margin-top: 4px !important;
}*/

</style>
<p class="red" id="error-block">
@if ($errors->any())
    {{$errors->first()}}
@endif
</p>
<!-- Academic Information -->
<div class="header">
    <h2><strong>Academic</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.medium_type') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <div class="form-group">
                {!!Form::select('medium_type', $student['arr_medium'],isset($student['medium_type']) ? $student['medium_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type','onChange' => 'getClass(this.value)'])!!}
                <i class="arrow double"></i>
            </div>
            </label>
        </div>
        @if ($errors->has('medium_type')) <p class="help-block">{{ $errors->first('medium_type') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.admission_session_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('admission_session_id', $student['arr_session'],isset($student['admission_session_id']) ? $student['admission_session_id'] : null, ['class' => 'form-control show-tick select_form1 select2','id'=>'admission_session_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('admission_session_id')) <p class="help-block">{{ $errors->first('admission_session_id') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.admission_class_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('admission_class_id', $student['arr_class'],isset($student['admission_class_id']) ? $student['admission_class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'admission_class_id','onChange' => 'getSection(this.value,"1")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('admission_class_id')) <p class="help-block">{{ $errors->first('admission_class_id') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.admission_section_id') !!} <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%" >
                {!!Form::select('admission_section_id', $student['arr_section'],isset($student['admission_section_id']) ? $student['admission_section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'admission_section_id',$admit_section_disabled])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('admission_section_id')) <p class="help-block">{{ $errors->first('admission_section_id') }}</p> @endif
    </div>

   
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.current_session_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('current_session_id', $student['arr_session'],isset($student['current_session_id']) ? $student['current_session_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'current_session_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('current_session_id')) <p class="help-block">{{ $errors->first('current_session_id') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.current_class_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('current_class_id', $student['arr_class'],isset($student['current_class_id']) ? $student['current_class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'current_class_id','onChange' => 'getSection(this.value,"2")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('current_class_id')) <p class="help-block">{{ $errors->first('current_class_id') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.current_section_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('current_section_id', $student['arr_section'],isset($student['current_section_id']) ? $student['current_section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'current_section_id',$current_section_disabled,'onChange'=> 'checkSectionCapacity(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('current_section_id')) <p class="help-block">{{ $errors->first('current_section_id') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.stream_name') !!}  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('stream_id', $student['arr_stream'],isset($student['stream_id']) ? $student['stream_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'stream_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('stream_id')) <p class="help-block">{{ $errors->first('stream_id') }}</p> @endif
    </div>
</div>

<div class="header">
    <h2><strong>Basic</strong> Information</h2>
</div>

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_enroll_number') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group field select">
            {!! Form::text('student_enroll_number', old('student_enroll_number',isset($student['student_enroll_number']) ? $student['student_enroll_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_enroll_number'), 'id' => 'student_enroll_number']) !!}
        </div>
        @if ($errors->has('student_enroll_number')) <p class="help-block">{{ $errors->first('student_enroll_number') }}</p> @endif
    </div>
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_name', old('student_name',isset($student['student_name']) ? $student['student_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_name'), 'id' => 'student_name']) !!}
        </div>
        @if ($errors->has('student_name')) <p class="help-block">{{ $errors->first('student_name') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_type') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_type', $student['arr_student_type'],isset($student['student_type']) ? $student['student_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_type'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('student_type')) <p class="help-block">{{ $errors->first('student_type') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <span class="radioBtnpan">{{trans('language.student_image')}} </span>
            <label for="file1" class="field file">
                
                <input type="file" class="gui-file" name="student_image" id="file1" onChange="document.getElementById('student_image').value = this.value;">
                <input type="hidden" class="gui-input" id="student_image" placeholder="Upload Photo" readonly>
            </label>
            <span>Photo size will be 180X180</span>
            @if ($errors->has('student_image')) <p class="help-block">{{ $errors->first('student_image') }}</p> @endif
            @if(isset($student['student_image']))<br /><a href="{{url($student['student_image'])}}" target="_blank" >View Image</a>@endif
    </div>

</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_reg_date') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_reg_date', old('student_reg_date',isset($student['student_reg_date']) ? $student['student_reg_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_reg_date'), 'id' => 'student_reg_date']) !!}
        </div>
        @if ($errors->has('student_reg_date')) <p class="help-block">{{ $errors->first('student_reg_date') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_dob') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_dob', old('student_dob',isset($student['student_dob']) ? $student['student_dob']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_dob'), 'id' => 'student_dob']) !!}
        </div>
        @if ($errors->has('student_dob')) <p class="help-block">{{ $errors->first('student_dob') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_email') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_email', old('student_email',isset($student['student_email']) ? $student['student_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_email'), 'id' => 'student_email', $disabled]) !!}
        </div>
        @if ($errors->has('student_email')) <p class="help-block">{{ $errors->first('student_email') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_gender') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_gender', $student['arr_gender'],isset($student['student_gender']) ? $student['student_gender'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_gender'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('student_gender')) <p class="help-block">{{ $errors->first('student_gender') }}</p> @endif
    </div>

</div>

<!-- Previous school Information -->
<div class="header">
    <h2><strong>Previous School</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_privious_school') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_privious_school', old('student_privious_school',isset($student['student_privious_school']) ? $student['student_privious_school']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_school'), 'id' => 'student_privious_school']) !!}
        </div>
        @if ($errors->has('student_privious_school')) <p class="help-block">{{ $errors->first('student_privious_school') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_privious_class') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_privious_class', old('student_privious_class',isset($student['student_privious_class']) ? $student['student_privious_class']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_class'), 'id' => 'student_privious_class']) !!}
        </div>
        @if ($errors->has('student_privious_class')) <p class="help-block">{{ $errors->first('student_privious_class') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_privious_tc_no') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_privious_tc_no', old('student_privious_tc_no',isset($student['student_privious_tc_no']) ? $student['student_privious_tc_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_tc_no'), 'id' => 'student_privious_tc_no']) !!}
        </div>
        @if ($errors->has('student_privious_tc_no')) <p class="help-block">{{ $errors->first('student_privious_tc_no') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_privious_tc_date') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_privious_tc_date', old('student_privious_tc_date',isset($student['student_privious_tc_date']) ? $student['student_privious_tc_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_tc_date'), 'id' => 'student_privious_tc_date']) !!}
        </div>
        @if ($errors->has('student_privious_tc_date')) <p class="help-block">{{ $errors->first('student_privious_tc_date') }}</p> @endif
    </div>

</div>

<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_privious_result') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::textarea('student_privious_result', old('student_privious_result',isset($student['student_privious_result']) ? $student['student_privious_result']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_privious_result'), 'id' => 'student_privious_result', 'rows'=> 1]) !!}
        </div>
        @if ($errors->has('student_privious_result')) <p class="help-block">{{ $errors->first('student_privious_result') }}</p> @endif
    </div>
</div>

<!-- Other Information -->
<div class="header">
    <h2><strong>Other</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_category') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_category', $student['arr_category'],isset($student['student_category']) ? $student['student_category'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_category', 'data-live-search'=>'true'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('student_category')) <p class="help-block">{{ $errors->first('student_category') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.caste_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('caste_id', $student['arr_caste'],isset($student['caste_id']) ? $student['caste_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'caste_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('caste_id')) <p class="help-block">{{ $errors->first('caste_id') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.religion_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('religion_id', $student['arr_religion'],isset($student['religion_id']) ? $student['religion_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'religion_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('religion_id')) <p class="help-block">{{ $errors->first('religion_id') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.nationality_id') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('nationality_id', $student['arr_natioanlity'],isset($student['nationality_id']) ? $student['nationality_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'nationality_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('nationality_id')) <p class="help-block">{{ $errors->first('nationality_id') }}</p> @endif
    </div>

</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_sibling_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_sibling_name', old('student_sibling_name',isset($student['student_sibling_name']) ? $student['student_sibling_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_sibling_name'), 'id' => 'student_sibling_name']) !!}
        </div>
        @if ($errors->has('student_sibling_name')) <p class="help-block">{{ $errors->first('student_sibling_name') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_sibling_class_id') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_sibling_class_id', $student['arr_class'],isset($student['student_sibling_class_id']) ? $student['student_sibling_class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_sibling_class_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('student_sibling_class_id')) <p class="help-block">{{ $errors->first('student_sibling_class_id') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_adhar_card_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_adhar_card_number', old('student_adhar_card_number',isset($student['student_adhar_card_number']) ? $student['student_adhar_card_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_adhar_card_number'), 'id' => 'student_adhar_card_number']) !!}
        </div>
        @if ($errors->has('student_adhar_card_number')) <p class="help-block">{{ $errors->first('student_adhar_card_number') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.group_id') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('group_id', $student['arr_group'],isset($student['group_id']) ? $student['group_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'group_id'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('group_id')) <p class="help-block">{{ $errors->first('group_id') }}</p> @endif
    </div>
</div>


<!-- Guardian Information -->
<div class="header">
    <h2><strong>Login</strong> Credentials</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_login_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_login_name', old('student_login_name',isset($student['student_login_name']) ? $student['student_login_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_login_name'), 'id' => 'student_login_name']) !!}
        </div>
        @if ($errors->has('student_login_name')) <p class="help-block">{{ $errors->first('student_login_name') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_login_email') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_login_email', old('student_login_email',isset($student['student_login_email']) ? $student['student_login_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_login_email'), 'id' => 'student_login_email', $disabled]) !!}
        </div>
        @if ($errors->has('student_login_email')) <p class="help-block">{{ $errors->first('student_login_email') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_login_contact_no') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_login_contact_no', old('student_login_contact_no',isset($student['student_login_contact_no']) ? $student['student_login_contact_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_login_contact_no'), 'id' => 'student_login_contact_no', 'maxlength'=> '10']) !!}
        </div>
        @if ($errors->has('student_login_contact_no')) <p class="help-block">{{ $errors->first('student_login_contact_no') }}</p> @endif
    </div>
    

</div>

<!-- Father Information -->
<div class="header">
    <h2><strong>Father</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_father_name', old('student_father_name',isset($student['student_father_name']) ? $student['student_father_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_name'), 'id' => 'student_father_name']) !!}
        </div>
        @if ($errors->has('student_father_name')) <p class="help-block">{{ $errors->first('student_father_name') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_mobile_number') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_father_mobile_number', old('student_father_mobile_number',isset($student['student_father_mobile_number']) ? $student['student_father_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_mobile_number'), 'id' => 'student_father_mobile_number', 'maxlength'=> '10', 'onkeyup' => "check_parent_existance()"]) !!}
        </div>
        @if ($errors->has('student_father_mobile_number')) <p class="help-block">{{ $errors->first('student_father_mobile_number') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_email') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_father_email', old('student_father_email',isset($student['student_father_email']) ? $student['student_father_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_email'), 'id' => 'student_father_email']) !!}
        </div>
        @if ($errors->has('student_father_email')) <p class="help-block">{{ $errors->first('student_father_email') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_father_annual_salary') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_father_annual_salary', $student['arr_annual_salary'],isset($student['student_father_annual_salary']) ? $student['student_father_annual_salary'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_father_annual_salary'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('student_father_annual_salary')) <p class="help-block">{{ $errors->first('student_father_annual_salary') }}</p> @endif
    </div>

</div>

<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_father_occupation') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::textarea('student_father_occupation', old('student_father_occupation',isset($student['student_father_occupation']) ? $student['student_father_occupation']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_father_occupation'), 'id' => 'student_father_occupation', 'rows'=> 2]) !!}
        </div>
        @if ($errors->has('student_father_occupation')) <p class="help-block">{{ $errors->first('student_father_occupation') }}</p> @endif
    </div>
</div>

<!-- Mother Information -->
<div class="header">
    <h2><strong>Mother</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_name') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_name', old('student_mother_name',isset($student['student_mother_name']) ? $student['student_mother_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_name'), 'id' => 'student_mother_name']) !!}
        </div>
        @if ($errors->has('student_mother_name')) <p class="help-block">{{ $errors->first('student_mother_name') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_mobile_number') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_mobile_number', old('student_mother_mobile_number',isset($student['student_mother_mobile_number']) ? $student['student_mother_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_mobile_number'), 'id' => 'student_mother_mobile_number', 'maxlength'=> '10']) !!}
        </div>
        @if ($errors->has('student_mother_mobile_number')) <p class="help-block">{{ $errors->first('student_mother_mobile_number') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_email', old('student_mother_email',isset($student['student_mother_email']) ? $student['student_mother_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_email'), 'id' => 'student_mother_email']) !!}
        </div>
        @if ($errors->has('student_mother_email')) <p class="help-block">{{ $errors->first('student_mother_email') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_annual_salary') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_mother_annual_salary', $student['arr_annual_salary'],isset($student['student_mother_annual_salary']) ? $student['student_mother_annual_salary'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_mother_annual_salary'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('student_mother_annual_salary')) <p class="help-block">{{ $errors->first('student_mother_annual_salary') }}</p> @endif
    </div>

     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_mother_tongue') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_mother_tongue', old('student_mother_tongue',isset($student['student_mother_tongue']) ? $student['student_mother_tongue']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_tongue'), 'id' => 'student_mother_tongue']) !!}
        </div>
        @if ($errors->has('student_mother_tongue')) <p class="help-block">{{ $errors->first('student_mother_tongue') }}</p> @endif
    </div>

    <div class="col-lg-9 col-md-9 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_mother_occupation') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('student_mother_occupation', old('student_mother_occupation',isset($student['student_mother_occupation']) ? $student['student_mother_occupation']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_mother_occupation'), 'id' => 'student_mother_occupation', 'rows'=> 2]) !!}
        </div>
        @if ($errors->has('student_mother_occupation')) <p class="help-block">{{ $errors->first('student_mother_occupation') }}</p> @endif
    </div>

</div>

<!-- Guardian Information -->
<div class="header">
    <h2><strong>Guardian</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_name', old('student_guardian_name',isset($student['student_guardian_name']) ? $student['student_guardian_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_name'), 'id' => 'student_guardian_name']) !!}
        </div>
        @if ($errors->has('student_guardian_name')) <p class="help-block">{{ $errors->first('student_guardian_name') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_mobile_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_mobile_number', old('student_guardian_mobile_number',isset($student['student_guardian_mobile_number']) ? $student['student_guardian_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_mobile_number'), 'id' => 'student_guardian_mobile_number', 'maxlength'=> '10']) !!}
        </div>
        @if ($errors->has('student_guardian_mobile_number')) <p class="help-block">{{ $errors->first('student_guardian_mobile_number') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_email', old('student_guardian_email',isset($student['student_guardian_email']) ? $student['student_guardian_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_email'), 'id' => 'student_guardian_email']) !!}
        </div>
        @if ($errors->has('student_guardian_email')) <p class="help-block">{{ $errors->first('student_guardian_email') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_guardian_relation') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_guardian_relation', old('student_guardian_relation',isset($student['student_guardian_relation']) ? $student['student_guardian_relation']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_guardian_relation'), 'id' => 'student_guardian_relation']) !!}
        </div>
        @if ($errors->has('student_guardian_relation')) <p class="help-block">{{ $errors->first('student_guardian_relation') }}</p> @endif
    </div>

</div>

<!-- Temporary Address Information -->
<div class="header">
    <h2><strong>Temporary Address</strong> Information</h2>
</div>

<!-- Address section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_temporary_address') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('student_temporary_address', old('student_temporary_address',isset($student['student_temporary_address']) ? $student['student_temporary_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_temporary_address'), 'id' => 'student_temporary_address', 'rows'=> 2]) !!}
        </div>
        @if ($errors->has('student_temporary_address')) <p class="help-block">{{ $errors->first('student_temporary_address') }}</p> @endif
    </div>
</div>

<!-- City section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_temporary_county') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_temporary_country', $student['arr_country'],isset($student['student_temporary_country']) ? $student['student_temporary_country'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_temporary_country', 'onChange'=> 'showStates(this.value,"student_temporary_state")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('student_temporary_country')) <p class="help-block">{{ $errors->first('student_temporary_country') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_temporary_state') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_temporary_state', $student['arr_state_t'],isset($student['student_temporary_state']) ? $student['student_temporary_state'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_temporary_state' , 'onChange'=> 'showCity(this.value,"student_temporary_city")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('student_temporary_state')) <p class="help-block">{{ $errors->first('student_temporary_state') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_temporary_city') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_temporary_city', $student['arr_city_t'],isset($student['student_temporary_city']) ? $student['student_temporary_city'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_temporary_city'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('student_temporary_city')) <p class="help-block">{{ $errors->first('student_temporary_city') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_temporary_pincode') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_temporary_pincode', old('student_temporary_pincode',isset($student['student_temporary_pincode']) ? $student['student_temporary_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_temporary_pincode'), 'id' => 'student_temporary_pincode', 'maxlength'=> '6']) !!}
        </div>
        @if ($errors->has('student_temporary_pincode')) <p class="help-block">{{ $errors->first('student_temporary_pincode') }}</p> @endif
    </div>

</div>

<!-- Permanent Address Information -->
<div class="header">
    <h2><strong>Permanent Address</strong> Information</h2>
</div>

<!-- Address section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.student_permanent_address') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::textarea('student_permanent_address', old('student_permanent_address',isset($student['student_permanent_address']) ? $student['student_permanent_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_permanent_address'), 'id' => 'student_permanent_address', 'rows'=> 2]) !!}
        </div>
        @if ($errors->has('student_permanent_address')) <p class="help-block">{{ $errors->first('student_permanent_address') }}</p> @endif
    </div>
</div>

<!-- City section -->
<div class="row clearfix">
<div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_permanent_county') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_permanent_county', $student['arr_country'],isset($student['student_permanent_county']) ? $student['student_permanent_county'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_permanent_county' , 'onChange'=> 'showStates(this.value,"student_permanent_state")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('student_permanent_county')) <p class="help-block">{{ $errors->first('student_permanent_county') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_permanent_state') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_permanent_state', $student['arr_state_p'],isset($student['student_permanent_state']) ? $student['student_permanent_state'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_permanent_state', 'onChange'=> 'showCity(this.value,"student_permanent_city")'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('student_permanent_state')) <p class="help-block">{{ $errors->first('student_permanent_state') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_permanent_city') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('student_permanent_city', $student['arr_city_p'],isset($student['student_permanent_city']) ? $student['student_permanent_city'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'student_permanent_city'])!!}
                <i class="arrow double"></i>
            </label>
        </div>
        @if ($errors->has('student_permanent_city')) <p class="help-block">{{ $errors->first('student_permanent_city') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_permanent_pincode') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_permanent_pincode', old('student_permanent_pincode',isset($student['student_permanent_pincode']) ? $student['student_permanent_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_permanent_pincode'), 'id' => 'student_permanent_pincode', 'maxlength'=> '6']) !!}
        </div>
        @if ($errors->has('student_permanent_pincode')) <p class="help-block">{{ $errors->first('student_permanent_pincode') }}</p> @endif
    </div>

</div>

<!-- Health Information -->
<div class="header">
    <h2><strong>Health</strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_height') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_height', old('student_height',isset($student['student_height']) ? $student['student_height']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_height'), 'id' => 'student_height']) !!}
        </div>
        @if ($errors->has('student_height')) <p class="help-block">{{ $errors->first('student_height') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_weight') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_weight', old('student_weight',isset($student['student_weight']) ? $student['student_weight']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_weight'), 'id' => 'student_weight']) !!}
        </div>
        @if ($errors->has('student_weight')) <p class="help-block">{{ $errors->first('student_weight') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_blood_group') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('student_blood_group', old('student_blood_group',isset($student['student_blood_group']) ? $student['student_blood_group']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_blood_group'), 'id' => 'student_blood_group']) !!}
        </div>
        @if ($errors->has('student_blood_group')) <p class="help-block">{{ $errors->first('student_blood_group') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_vision_left') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_vision_left', old('student_vision_left',isset($student['student_vision_left']) ? $student['student_vision_left']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_vision_left'), 'id' => 'student_vision_left']) !!}
        </div>
        @if ($errors->has('student_vision_left')) <p class="help-block">{{ $errors->first('student_vision_left') }}</p> @endif
    </div>

</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.student_vision_right') !!} :</lable>
        <div class="form-group">
            {!! Form::text('student_vision_right', old('student_vision_right',isset($student['student_vision_right']) ? $student['student_vision_right']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_vision_right'), 'id' => 'student_vision_right']) !!}
        </div>
        @if ($errors->has('student_vision_right')) <p class="help-block">{{ $errors->first('student_vision_right') }}</p> @endif
    </div>
    <div class="col-lg-9 col-md-9">
        <lable class="from_one1">{!! trans('language.medical_issues') !!} <span class="red-text">*</span>  :</lable>
        <div class="form-group">
            {!! Form::text('medical_issues', old('medical_issues',isset($student['medical_issues']) ? $student['medical_issues']: ''), ['class' => 'form-control','placeholder'=>trans('language.medical_issues_p'), 'id' => 'medical_issues']) !!}
        </div>
        @if ($errors->has('medical_issues')) <p class="help-block">{{ $errors->first('medical_issues') }}</p> @endif
    </div>
</div>

<!-- Document Information -->
<div class="header">
    <h2><strong>Document</strong> Information</h2>
</div>

{!! Form::hidden('hide',isset($student['documents']) ? COUNT($student['documents']) : '1',['class' => 'gui-input', 'id' => 'hide']) !!}
<div id="sectiontable-body">
    <p class="green" id="document_success"></p>
    @if(isset($student['documents']))
    @php $key = 0; @endphp
    @foreach($student['documents'] as $documentKey => $documents)
    @php 
        $student_document_category = "documents[".$key."][document_category_id]";  
        $student_document_id = "documents[".$key."][student_document_id]"; 
    @endphp
    <div id="document_block{{$documentKey}}">
        {!! Form::hidden($student_document_id,$documents['student_document_id'],['class' => 'gui-input']) !!}
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.student_document_category') !!} :</lable>
                <div class="form-group m-bottom-0">
                    <label class=" field select" style="width: 100%">
                        {!!Form::select($student_document_category, $student['arr_document_category'],$documents['document_category_id'], ['class' => 'form-control show-tick select_form1 select2','id'=>'document_category_id'])!!}
                        <i class="arrow double"></i>
                    </label>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            <span class="radioBtnpan">{{trans('language.student_document_file')}}</span>
                <label for="file1" class="field file">
                    
                    <input type="file" class="gui-file" name="documents[{{$key}}][student_document_file]" id="student_document_file{{$key}}" >
                    <input type="hidden" class="gui-input" id="student_document_file{{$key}}" placeholder="Upload Photo" readonly>
                    
                    @if(isset($documents['student_document_file']) && !empty($documents['student_document_file']))<a href="{{url($documents['student_document_file'])}}" target="_blank" >View Image</a>@endif
                </label>
            
            </div>
            <div class="col-lg-3 col-md-3">&nbsp;</div>
            <div class="col-lg-3 col-md-3 text-right">
                @if($documentKey == 0)
                <button class="btn btn-primary custom_btn" type="button" onclick="addDocumentBlock()" >Add More</button>
                @else 
                <button class="btn btn-primary custom_btn red" type="button" onclick="remove_block({{$documentKey}}),remove_record({{$documents['student_document_id']}})" >Remove</button>
                @endif
            </div>
        </div>

        <!-- Document detail section -->
        <div class="row clearfix">
            <div class="col-sm-12 text_area_desc">
                <lable class="from_one1">{!! trans('language.student_document_details') !!} :</lable>
                <div class="form-group">
                    <textarea name="documents[{{$key}}][student_document_details]" id="" class="form-control" placeholder="{{trans('language.student_document_details')}}" rows="2">{!! $documents['student_document_details'] !!}</textarea>
                   
                </div>
                
            </div>
        </div>
    </div>
    @php $key++ @endphp
    @endforeach
    @else
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.student_document_category') !!} :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    {!!Form::select('documents[0][document_category_id]', $student['arr_document_category'],isset($student['document_category_id']) ? $student['document_category_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'document_category_id'])!!}
                    <i class="arrow double"></i>
                </label>
            </div>
            @if ($errors->has('document_category_id')) <p class="help-block">{{ $errors->first('document_category_id') }}</p> @endif
        </div>
        <div class="col-lg-3 col-md-3">
        <span class="radioBtnpan">{{trans('language.student_document_file')}}</span>
            <label for="file1" class="field file">
                
                <input type="file" class="gui-file" name="documents[0][student_document_file]" id="student_document_file" onChange="document.getElementById('student_document_file').value = this.value;">
                <input type="hidden" class="gui-input" id="student_document_file" placeholder="Upload Photo" readonly>
               
            </label>
           
        </div>
        <div class="col-lg-3 col-md-3">&nbsp;</div>
        <div class="col-lg-3 col-md-3 text-right"><button class="btn btn-primary custom_btn" style="width: 119px !important;" type="button" onclick="addDocumentBlock()" ><i class="fas fa-plus-circle"></i> Add More</button></div>
    </div>

    <!-- Document detail section -->
    <div class="row clearfix">
        <div class="col-sm-12 text_area_desc">
            <lable class="from_one1">{!! trans('language.student_document_details') !!} :</lable>
            <div class="form-group">
                {!! Form::textarea('documents[0][student_document_details]', old('student_document_details',isset($student['student_document_details']) ? $student['student_document_details']: ''), ['class' => 'form-control','placeholder'=>trans('language.student_document_details'), 'id' => 'student_document_details', 'rows'=> 2]) !!}
            </div>
            @if ($errors->has('student_document_details')) <p class="help-block">{{ $errors->first('student_document_details') }}</p> @endif
        </div>
    </div>
    @endif
</div>
<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-primary']) !!}</a>
        <!-- <button type="submit" class="btn btn-raised btn-round">Cancel</button> -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {
        
        $("#student-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                student_enroll_number: {
                    required: true
                },
                student_name: {
                    required: true
                },
                student_email: {
                    required: true,
                    email: true
                },
                student_reg_date: {
                    required: true
                },
                student_dob: {
                    required: true
                },
                student_category: {
                    required: true
                },
                caste_id: {
                    required: true
                },
                religion_id: {
                    required: true
                },
                nationality_id: {
                    required: true
                },
                student_permanent_address: {
                    required: true
                },
                student_permanent_city: {
                    required: true
                },
                student_permanent_state: {
                    required: true
                },
                student_permanent_county: {
                    required: true
                },
                student_permanent_pincode: {
                    required: true
                },
                medium_type: {
                    required: true
                },
                student_type: {
                    required: true
                },
                admission_session_id: {
                    required: true
                },
                admission_class_id: {
                    required: true
                },
                admission_section_id: {
                    required: true
                },
                current_session_id: {
                    required: true
                },
                current_class_id: {
                    required: true
                },
                current_section_id: {
                    required: true
                },
                student_blood_group: {
                    required: true
                },
                medical_issues: {
                    required: true
                },
                student_father_name: {
                    required: true
                },
                student_father_mobile_number: {
                    required: true,
                    digits: true
                },
                student_father_email: {
                    required: true,
                    email: true
                },
                student_father_occupation: {
                    required: true
                },
                student_mother_name: {
                    required: true
                },
                student_mother_mobile_number: {
                    required: true,
                    digits: true
                },
                student_guardian_mobile_number: {
                    digits: true
                },
                student_guardian_email: {
                    email: true
                },
                student_mother_email: {
                    email: true
                },
                student_temporary_pincode: {
                    digits: true
                },
                student_permanent_pincode: {
                    digits: true,
                    required: true
                },
                student_privious_school: {
                    required: true
                },
                student_privious_class: {
                    required: true
                },
                student_privious_tc_no: {
                    required: true
                },
                student_privious_tc_date: {
                    required: true
                },
                student_privious_result: {
                    required: true
                },
                student_login_name: {
                    required: true
                },
                student_login_email: {
                    required: true,
                    email: true
                },
                student_login_contact_no: {
                    digits: true,
                    required: true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
        $('#student_dob').bootstrapMaterialDatePicker({ weekStart : 0,time: false });
        $('#student_reg_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false });
        $('#student_privious_tc_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false });
    });

    function getSection(class_id,type)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/get-section-data')}}",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    if(type == 1) {
                        $("select[name='admission_section_id'").html('');
                        $("select[name='admission_section_id'").html(data.options);
                        $("select[name='admission_section_id'").removeAttr("disabled");
                        $("select[name='admission_section_id'").selectpicker('refresh');
                        $(".mycustloading").hide();
                    } else if(type == 2){
                        $("select[name='current_section_id'").html('');
                        $("select[name='current_section_id'").html(data.options);
                        $("select[name='current_section_id'").removeAttr("disabled");
                        $("select[name='current_section_id'").selectpicker('refresh');
                        $(".mycustloading").hide();
                    }
                }
            });
        } else {
            if(type == 1) {
                $("select[name='admission_section_id'").html('');
                $("select[name='admission_section_id'").selectpicker('refresh');
                $(".mycustloading").hide();
            } else if(type == 2){
                $("select[name='current_section_id'").html('');
                $("select[name='current_section_id'").selectpicker('refresh');
                $(".mycustloading").hide();
            }
        }
    }

    function check_parent_existance(){
        var mobile_no = $("#student_father_mobile_number").val().toString();
        if(mobile_no.length == 10){
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/check-parent-existance')}}",
                type: 'GET',
                data: {
                    'mobile_no': mobile_no
                },
                success: function (data) {
                    $(".mycustloading").hide();
                    if(data.exist_status != ""){
                        if(data.exist_status == 1){
                            $("#student_login_name").val(data.student_login_name);
                            $("#student_login_email").val(data.student_login_email);
                            $("#student_login_contact_no").val(data.student_login_contact_no);
                            $("#student_father_name").val(data.student_father_name);
                            $("#student_father_email").val(data.student_father_email);
                            $("#student_father_occupation").val(data.student_father_occupation);
                            $("#student_father_annual_salary").val(data.student_father_annual_salary);
                            $("#student_mother_name").val(data.student_mother_name);
                            $("#student_mother_mobile_number").val(data.student_mother_mobile_number);
                            $("#student_mother_email").val(data.student_mother_email);
                            $("#student_mother_occupation").val(data.student_mother_occupation);

                            $("#student_mother_tongue").val(data.student_mother_tongue);
                            $("#student_guardian_name").val(data.student_guardian_name);
                            $("#student_guardian_email").val(data.student_guardian_email);
                            $("#student_guardian_mobile_number").val(data.student_guardian_mobile_number);
                            $("#student_mother_annual_salary").val(data.student_mother_annual_salary);
                            $("select[name='student_father_annual_salary'").selectpicker('refresh');
                            $("select[name='student_mother_annual_salary'").selectpicker('refresh');
                        }
                    }
                }
            });
        }
    }

    function addDocumentBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b) - 1;
        
        $("#sectiontable-body").append('<div id="document_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans("language.student_document_category") !!} :</lable><label class=" field select" style="width: 100%">  <select class="form-control show-tick select_form1" id="document_category_id'+arr_key+'" name="documents['+arr_key+'][document_category_id]">@foreach ($student["arr_document_category"] as $document_key => $document_category) <option value="{{ $document_key}}">{!! $document_category !!}</option>@endforeach</select><i class="arrow double"></i></label></div><div class="col-lg-3 col-md-3"><span class="radioBtnpan">{{trans("language.student_document_file")}}</span><label for="file1" class="field file"><input type="file" class="gui-file" name="documents['+arr_key+'][student_document_file]" id="file'+arr_key+'" ><input type="hidden" class="gui-input" id="student_document_file'+arr_key+'" placeholder="Upload Photo" readonly></label></div><div class="col-lg-3 col-md-3">&nbsp;</div><div class="col-lg-3 col-md-3 text-right"><button class="btn btn-danger custom_btn red" type="button" onclick="remove_block('+arr_key+')" ><i class="fas fa-minus-circle"></i> Remove</button></div></div><div class="row clearfix"><div class="col-sm-12 text_area_desc"><lable class="from_one1">{!! trans("language.student_document_details") !!} :</lable><div class="form-group"><textarea name="documents['+arr_key+'][student_document_details]" class="form-control" placeholder="{{trans("language.student_document_details")}}" id="student_document_details'+arr_key+'" rows="2" ></textarea></div> </div></div></div>');
        $("select[name='documents["+arr_key+"][document_category_id]'").selectpicker('refresh');
    }
    function remove_block(id){
        $("#document_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }
    function remove_record(student_document_id){
        var r = confirm("Are you sure to remove this record ?");
        if (r == true) {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/delete-student-document/')}}/"+student_document_id+'/1',
                success: function (data) {
                    $("#document_success").html(data);
                    $(".mycustloading").hide();
                }
            });
        }
    }

    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='admission_class_id'").html(data.options);
                    $("select[name='student_sibling_class_id'").html(data.options);
                    $("select[name='current_class_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='admission_class_id'").html('<option value="">Select Class</option>');
            $("select[name='current_class_id'").html('<option value="">Select Class</option>');
            $("select[name='student_sibling_class_id'").html(data.options);
            $(".mycustloading").hide();
        }
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/stream/get-stream-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='stream_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='stream_id'").html('<option value="">Select Stream</option>');
            $(".mycustloading").hide();
        }
    }

    function checkSectionCapacity(section_id){
        if(section_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/student/check-section-capacity')}}",
                type: 'GET',
                data: {
                    'section_id': section_id
                },
                success: function (data) {
                    if(data == 'False'){
                        $('#error-block').html("Sorry, We have already exceed section's capacity. Please add student into different section.");
                        $(':input[type="submit"]').prop('disabled', true);
                    } else {
                        $('#error-block').html('');
                        $(':input[type="submit"]').prop('disabled', false);
                    }
                    $(".mycustloading").hide();
                }
            });
        } 
    }

    function showStates(country_id,name){
        if(country_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/state/show-state')}}",
                type: 'GET',
                data: {
                    'country_id': country_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select State</option>');
            $(".mycustloading").hide();
        }
    }

    function showCity(state_id,name){
        if(state_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/city/show-city')}}",
                type: 'GET',
                data: {
                    'state_id': state_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select City</option>');
            $(".mycustloading").hide();
        }
    }

</script>

