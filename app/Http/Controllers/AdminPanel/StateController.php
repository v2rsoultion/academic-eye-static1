<?php

namespace App\Http\Controllers\AdminPanel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use App\Model\Country\Country; // Model 
use App\Model\State\State; // Model 
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Datatables;

class StateController extends Controller
{
    /**
     *  Get all states according country id
     *  @Khushbu on 08 Sept 2018
    **/
    public function showStates(){
        $country_id = Input::get('country_id');
        $countries = get_all_states($country_id);
        $data = view('admin-panel.state.ajax-state-select',compact('countries'))->render();
        return response()->json(['options'=>$data]);
    }
    /**
     *  View page for state
     *  @Khushbu on 08 Sept 2018
    **/
    public function index()
    {
        $state               	= [];
        $loginInfo           	= get_loggedin_user_data();
        $arr_country 			= get_all_country();
        $state['arr_country']   = add_blank_option($arr_country, 'Select Country');
        $data 			        = array(
            'page_title'        => trans('language.view_state'),
            'redirect_url'      => url('admin-panel/state/view-state'),
            'login_info'        => $loginInfo,
            'state'             => $state
        );
        return view('admin-panel.state.index')->with($data);
    }
	/**
     *  Add page for state
     *  @Khushbu on 08 Sept 2018
    **/
    public function add(Request $request, $id = null) 
    {
    	$data    		= [];
        $state	    	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_state_id    = get_decrypted_value($id, true);
            $state                 = State::join('country', 'country.country_id', '=', 'state.country_id')->select('state.*','country.*')->where('state_id', $decrypted_state_id)->get();
            $state                 = isset($state[0]) ? $state[0] : [];
            if (!$state)
            {
                return redirect('admin-panel/state/add-state')->withError('State not found!');
            }
            $page_title               = trans('language.edit_state');
            $encrypted_state_id       = get_encrypted_value($state['state_id'], true);
            $save_url                 = url('admin-panel/state/save/' . $encrypted_state_id);
            $submit_button            = 'Update';
            $arr_state                = get_all_states($state);
            $state['arr_state']   	  = $arr_state;
        }
        else
        {
            $page_title               = trans('language.add_state');
            $save_url                 = url('admin-panel/state/save');
            $submit_button            = 'Save';
        }
        $arr_country 			      = get_all_country();
        $state['arr_country']         = add_blank_option($arr_country, 'Select Country');
        $data                         = array(
            'page_title'    	      => $page_title,
            'save_url'      	      => $save_url,
            'submit_button' 	      => $submit_button,
            'state'        		      => $state,
            'login_info'    	      => $loginInfo,
            'redirect_url'  	      => url('admin-panel/state/view-state'),
        );
        return view('admin-panel.state.add')->with($data);
    }
    /**
     *  Add and update state data
     *  @Khushbu on 08 Sept 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo            = get_loggedin_user_data();
        $decrypted_state_id   = null;
        $decrypted_state_id   = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $state = State::find($decrypted_state_id);
            if (!$state)
            {
                return redirect('admin-panel/state/view-state/')->withError('State not found!');
            }
            $success_msg = 'State updated successfully!';
        }
        else
        { 
            $state      	 = New State;
            $success_msg     = 'State saved successfully!';
        }
        $country_id          = null;
        if ($request->has('country_id'))
        {
            $country_id      = Input::get('country_id');
        }
        $validatior            = Validator::make($request->all(), [
                'country_id'   => 'required',
                'state_name'   => 'required|unique:state,state_name,' . $decrypted_state_id . ',state_id,country_id,' . $country_id,
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $state->country_id     = Input::get('country_id');
                $state->state_name     = Input::get('state_name');
                $state->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/state/view-state')->withSuccess($success_msg);
    }
    /**
     *  Get Data for view page(Datatables)
     *  @Khushbu on 08 Sept 2018 
    **/
    public function anyData(Request $request, $state_id = array())
    {
        $state          = [];
        $loginInfo      = get_loggedin_user_data();
        $state          = State::where(function($query) use ($state_id,$loginInfo,$request) 
            {
                if (!empty($state_id))
                {
                    $query->where('state_id', $state_id);
                }
                if (!empty($request) && !empty($request->get('state_name')))
                {
                    $query->where('state_name', "like", "%{$request->get('state_name')}%");
                }
            })
            ->join('country', function($join) use ($request){
                $join->on('country.country_id', '=', 'state.country_id');
                if (!empty($request) && !empty($request->get('country_id')) && $request->get('country_id') != null)
                {
                    $join->where('country.country_id', '=', $request->get('country_id'));
                }
            })->get();
            return Datatables::of($state)
                ->addColumn('action', function ($state)
                {
                    $encrypted_state_id = get_encrypted_value($state->state_id, true);
                    if($state->state_status == 0) {
                        $status    = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status    = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                                <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="state-status/'.$status.'/' . $encrypted_state_id . '">'.$statusVal.'</a></div>
                                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-state/' . $encrypted_state_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                                <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-state/' . $encrypted_state_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }
    /**
     *  Destroy State data
     *  @Khushbu on 08 Sept 2018 
    **/
    public function destroy($id)
    {
        $state_id = get_decrypted_value($id, true);
        $state    = State::find($state_id);
        if ($state)
        {
            $state->delete();
            $success_msg = "State deleted successfully!";
            return redirect('admin-panel/state/view-state')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "State not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    /**
     *  Change State's status
     *  @Khushbu on 08 Sept 2018 
    **/
    public function changeStatus($status,$id)
    {
        $state_id = get_decrypted_value($id, true);
        $state    = State::find($state_id);
        if ($state)
        {
            $state->state_status  = $status;
            $state->save();
            $success_msg = "State status updated successfully!";
            return redirect('admin-panel/state/view-state')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "State not found!";
            return redirect('admin-panel/state/view-state')->withErrors($error_message);
        }
    }
}
