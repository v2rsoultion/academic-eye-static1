﻿@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive{
        overflow-x: visible !important;
    }
</style>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>View Vehicle Route</h2>
            </div>
             <div class="col-lg-7 col-md-6 col-sm-12 line">
                 <a href="{!! url('admin-panel/transport/vehicle/add-vehicle') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right ">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/transport') !!}">Transport<!-- {!! trans('language.staff') !!} --></a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/transport') !!}">Vehicle Route</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/vehicle-route/view-vehicle-route') !!}">View Vehicle Route</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">
                                <form>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-12">
                                    <!-- <lable class="from_one1">Vehicle :</lable> -->
                                    <div class="form-group">                       
                                        <select class="form-control show-tick select_form1" name="name">
                                            <option value="">Select Vehicle </option>
                                            <option value="1">Vehicle-1</option>
                                            <option value="2">Vehicle-2</option>
                                            <option value="3">Vehicle-3</option>
                                            <option value="4">Vehicle-4</option>
                                            <option value="5">Vehicle-5</option>
                                        </select>
                                    </div>
                                </div>
                                        <div class="col-lg-1 ">
                                          <button type="submit" class="btn btn-raised btn-primary"  title="Search">Search
                                          </button>
                                        </div>
                                         <div class="col-lg-1 ">
                                          <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                                          </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Bus No</th>
                                                <th>Driver Name</th>
                                                <th>Route Name</th>
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Bus-01</td>
                                                <td>Raj Pal</td>
                                                <td>Route-1</td>
                                                <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                <li> <a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#changeRouteModel">Edit Mapping</a></li>

                                                </ul> </div></td>
                                            </tr>
                                            <tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Bus-10</td>
                                                <td>Raju Paliwal</td>
                                                <td>Route-5</td>
                                                <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                <li> <a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#changeRouteModel">Edit Mapping</a></li>

                                                </ul> </div></td>
                                            </tr> <tr>
                                                <td>3</td>
                                                <td>Bus-12</td>
                                                <td>Rajesh Singh</td>
                                                <td>Route-10</td>
                                                <td class="text-center footable-last-visible"> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                <li> <a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#changeRouteModel">Edit Mapping</a></li>

                                                </ul> </div></td> 
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


<div class="modal fade" id="changeRouteModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Routes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form class="" action="" method=""  id="form_validation">
            <div class="row clearfix"> 
                 <div class="col-lg-6 col-md-6 col-sm-12">
                    <lable class="from_one1">Route Name :</lable>
                    <div class="form-group">                       
                        <select class="form-control show-tick select_form1" name="name">
                            <option value="">Select Route </option>
                            <option value="1">Route-1</option>
                            <option value="2">Route-2</option>
                            <option value="3">Route-3</option>
                            <option value="4">Route-4</option>
                            <option value="5">Route-5</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12" style="margin-top: 5px !important;">
                    <button type="submit" class="btn btn-raised  btn-primary saveBtn">Save</button>
                    <button type="submit" class="btn btn-raised  btn-primary cancelBtn waves-effect" data-dismiss="modal">Cancel</button>
                </div>
            </div>
          
           
                
            </div>
        </form>
      </div>
      </div>
    </div>
    </div>
 
<style type="text/css">

#DataTables_Table_0_filter{
    float: right !important;
}
#DataTables_Table_0_paginate{
    float: right !important;
}
.table tr .v1{
       white-space: normal;
}

</style>

<!-- status fuction -->

<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {

    document.getElementById("myDropdown").classList.toggle("show");
}
function myFunction2() {

    document.getElementById("myDropdown2").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            block_name: {
                validators: {
                        stringLength: {
                        min: 4,
                    },
                        notEmpty: {
                        message: 'Please fill required block name'
                    }
                }
            },
            hostel_name: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required hostel name'
                    }
                }
            },
            floor_no: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required floor no'
                    }
                }
            },
            room_no: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required room no'
                    }
                }
            },
            room_alias: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required room alias'
                    }
                }
            },
            capacity: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required capacity'
                    }
                }
            },
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>
