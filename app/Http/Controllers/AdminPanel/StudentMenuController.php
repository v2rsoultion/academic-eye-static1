<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentMenuController extends Controller
{
    public function myProfile() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.student-menu.my-profile')->with($data);
    }

    public function myAttendance() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.student-menu.my-attendance')->with($data);
    }
    public function myTimeTable() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.student-menu.my-time-table')->with($data);
    }
    public function myHomework() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.student-menu.my-homework')->with($data);
    }
    public function myRemarks() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.student-menu.my-remarks')->with($data);
    }
    public function vehicleTracks() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.student-menu.view-track-vehicle')->with($data);
    }

    public function viewTask() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.student-menu.view-task')->with($data);
    }

     public function viewResponses() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.student-menu.view-responses ')->with($data);
    }

     public function onlineContent() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.student-menu.onlinecontent')->with($data);
    }

     public function viewNotes() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.student-menu.view-notes')->with($data);
    }
}
