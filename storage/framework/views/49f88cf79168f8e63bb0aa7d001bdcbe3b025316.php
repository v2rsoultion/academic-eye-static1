﻿
<?php $__env->startSection('content'); ?>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2> View Vehicle Documents</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">   
             <a href="<?php echo url('admin-panel/transport/vehicle-documents/add-vehicle-document'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>             
                <ul class="breadcrumb float-md-right">
                   <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Transport</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Vehicle Document</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/transport/vehicle-documents/view-vehicle-documents'); ?>">View Vehicle Document</a></li>
                </ul>                
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="headingcommon  col-lg-12" style="padding-bottom: 0px !important">Manage Vehicle Document :-</div>
                    <div class="body form-gap">
                         
                        <form class="" action="" method=""  id="contact_form">
                            <div class="row clearfix">
                                 <div class="col-lg-3 col-md-3 col-sm-12">
                                    <!-- <lable class="from_one1">Vehicle :</lable> -->
                                    <div class="form-group">                       
                                        <select class="form-control show-tick select_form1" name="name">
                                            <option value="">Select Vehicle </option>
                                            <option value="1">Vehicle-1</option>
                                            <option value="2">Vehicle-2</option>
                                            <option value="3">Vehicle-3</option>
                                            <option value="4">Vehicle-4</option>
                                            <option value="5">Vehicle-5</option>
                                        </select>
                                    </div>
                                </div>
                            <!-- </div>
                            <div class="row clearfix"> -->
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <!-- <lable class="from_one1">Document Category :</lable> -->
                                    <div class="form-group">                       
                                        <select class="form-control show-tick select_form1" name="name">
                                            <option value="">Document Category </option>
                                            <option value="1">Action & Advanture</option>
                                            <option value="2">Drama</option>
                                            <option value="3">Mystery</option>
                                            <option value="4">Romance</option>
                                            <option value="5">Horror</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <!-- <lable class="from_one1">Expiry Date (option) :</lable> -->
                                    <div class="form-group">
                                        <input type="text" name="expiry_date" class="form-control" id="expiry_date" placeholder="Expiry Date">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-12" style="margin-top: -20px;">
                                    <span class="from_one1">Document Upload :</span>
                                    <label for="file1" class="field file">
                                        <div class="form-group"> 
                                            <input type="file" class="gui-file" name="document_file" id="student_document_file">
                                            <input type="hidden" class="gui-input" id="student_document_file" placeholder="Upload Photo" readonly="">
                                        </div>
                                    </label>
                                </div>
                            <!-- /div>
                            <div class="row clearfix"> -->
                             <div class="col-lg-1" style=" padding: 0px 0px;">
                                 <button type="button" class="btn btn-primary add_document_more"><i class="fas fa-plus-circle"></i> Add</button>
                              </div>
                            </div>
                               <div class="input_documents_wrap">
                              </div>
                            <div class="row clearfix">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-raised btn-primary">Save</button>
                                    <button type="submit" class="btn btn-raised btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>
                  <!--   </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                        
                            <div class="body form-gap"> -->
                                <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Document Category</th>
                                                <th>Document Url</th>
                                                <th>Expiry Date</th>
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Action & Adventure</td>
                                                <td>----</td>
                                                <td>2305/20/11</td>
                                                <td>
                                                 <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>  
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Action & Adventure</td>
                                                <td>----</td>
                                                <td>2305/20/11</td>
                                                 <td> 
                                                 <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>   
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>  
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Action & Adventure</td>
                                                <td>----</td>
                                                <td>2305/20/11</td>
                                                 <td> 
                                                  <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div> 
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>  
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Action & Adventure</td>
                                                <td>----</td>
                                                <td>2305/20/11</td>
                                                 <td> 
                                                  <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>   
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>  
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>


<script type="text/javascript">
// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                        stringLength: {
                        min: 4,
                    },
                        notEmpty: {
                        message: 'Please fill required document category'
                    }
                }
            },
            expiry_date: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required expiry date'
                    }
                }
            },
            document_file: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required document file'
                    }
                }
            }
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>