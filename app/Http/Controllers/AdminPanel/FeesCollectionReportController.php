<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeesCollectionReportController extends Controller
{
    /**
     *  View page for Fees Collection Report
     *  @Khushbu on 03 Oct 2018   
    **/ 
    public function viewReport() {
        $loginInfo          = get_loggedin_user_data();
        
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.fees-collection-report.view-report')->with($data);
    }

    public function classSheet() {
        $loginInfo          = get_loggedin_user_data();
        
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.fees-collection-report.class-sheet-report')->with($data);
    }

    public function studentSheet() {
        $loginInfo          = get_loggedin_user_data();
        
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.fees-collection-report.student-sheet-report')->with($data);
    }

    public function viewReports() {
        $loginInfo          = get_loggedin_user_data();
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.fees-collection-report.view-reports')->with($data);
    }

    public function classwiseAdmissionReport() {
        $loginInfo          = get_loggedin_user_data();
        
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.fees-collection-report.classwise-admission-report')->with($data);
    }

    public function formwiseFeesReport() {
        $loginInfo          = get_loggedin_user_data();
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.fees-collection-report.formwise-fees-report')->with($data);
    }

    public function attendanceReport() {
        $loginInfo          = get_loggedin_user_data();
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.fees-collection-report.attendance-report')->with($data);
    }

    public function staffAttendanceReport() {
        $loginInfo          = get_loggedin_user_data();
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.fees-collection-report.staff-attendance-report')->with($data);
    }

    public function feesWiseReport() {
        $loginInfo          = get_loggedin_user_data();
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.fees-collection-report.fees-wise-report')->with($data);
    }

    public function subjectWiseMarksReport() {
        $loginInfo          = get_loggedin_user_data();
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.fees-collection-report.subjects-wise-marks-report')->with($data);
    }

    public function examWiseMarksReport() {
        $loginInfo          = get_loggedin_user_data();
        $data               = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.fees-collection-report.exam-wise-marks-report')->with($data);
    }
}
