<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VehicleController extends Controller
{
    /**
     *  View page for Vehicle
     *  @Khushbu on 27 Sept 2018   
    **/
    public function index()
    {
        $loginInfo 		    = get_loggedin_user_data();
        
        $data 				= array(
            'page_title'    => trans('language.view_vehicle'),
            'redirect_url'  => url('admin-panel/transport/vehicle/manage-vehicle'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.vehicle.view-vehicle')->with($data);
    }

    /**
     *  Add page for Vehicle
     *  @Khushbu on 27 Sept 2018   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        // $job        = [];
        if (!empty($id))
        {
            $page_title             = trans('language.edit_vehicle');
            $save_url               = url('admin-panel/transport/vehicle/save/' . $decrypted_job_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_vehicle');
            $save_url      = url('admin-panel/transport/vehicle/save');
            $submit_button = 'Save';
        }

        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/vehicle/manage-vehicle'),
        );
        return view('admin-panel.vehicle.add-vehicle')->with($data);
    }

}
