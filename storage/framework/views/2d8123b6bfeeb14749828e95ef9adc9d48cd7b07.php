﻿
<?php $__env->startSection('content'); ?>
<style type="text/css">
    .headingcommon {
            font-size: 13px;
    color: #6572b8;
    font-weight: 800;
    text-transform: uppercase;
    padding: 10px 0px;
    }
</style>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2> Hostel Details</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/hostel'); ?>"><?php echo trans('language.menu_hostel'); ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/hostel'); ?>">Configuration</a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/hostel/configuration/hostel-details'); ?>">Hostel Details</a></li>
                </ul>                
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body form-gap">
                        <div class="headingcommon  col-lg-12">Add Hostel Details :-</div>
                        <form class="" action="" method=""  id="contact_form">
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <lable class="from_one1">Hostel Name :</lable>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Hostel Name"  name="name">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                <lable class="from_one1">Hostel Type :</lable>                 
                                    <select class="form-control show-tick select_form1" name="type">
                                       
                                        <option value="">Hostel Type</option>
                                        <option value="VII">Boys</option>
                                        <option value="X">Girls</option>
                                    </select>
                                </div>
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-raised btn-primary saveBtn">Save</button>
                                </div>
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-raised btn-primary cancelBtn">Cancel</button>
                                </div>
                            </div> 
                            <div class="row clearfix" style="margin-top:10px;">                            
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <hr />
                                </div>
                               
                            </div>
                        </form>
                        <div class="clearfix"></div>
                         <div class="headingcommon  col-lg-12" style="padding: 10px 0px;">Search By :-</div>
                          <div class="row ">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <lable class="from_one1">Hostel Name :</lable>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Hostel Name"  name="name">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                <lable class="from_one1">Hostel Type :</lable>                 
                                    <select class="form-control show-tick select_form1" name="type">
                                        <option value="">Hostel Type</option>
                                        <option value="VII">Boys</option>
                                        <option value="X">Girls</option>
                                    </select>
                                </div>
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-raised btn-primary saveBtn">Search</button>
                                </div>
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-raised btn-primary cancelBtn">Clear</button>
                                </div>
                            </div> 

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">
                                <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Hostel Name</th>
                                                <th>Hostel Type</th>
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Hostel-1</td>
                                                <td>Boys</td>
                                                <td>
                                                     <!-- <button class="btn btn-success btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-plus-circle"></i></button> -->
                                                    <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Hostel-2</td>
                                                <td>Boys</td>
                                                <td>
                                                     <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>
                                                 <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Hostel-3</td>
                                                <td>Girls</td>
                                                <td>
                                                     <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Hostel-4</td>
                                                <td>Girls</td>
                                                <td>
                                                     <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                        stringLength: {
                        min: 4,
                    },
                        notEmpty: {
                        message: 'Please fill required name'
                    }
                }
            },
            type: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required type'
                    }
                }
            },
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>

<style type="text/css">

.dataTables_filter{
    float: right !important;
}
#DataTables_Table_0_paginate{
    float: right;
}

</style>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>