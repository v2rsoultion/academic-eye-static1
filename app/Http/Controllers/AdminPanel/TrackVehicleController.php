<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrackVehicleController extends Controller
{
     /**
     *  View page for Track Vehicle
     *  @Khushbu on 01 Oct 2018   
    **/
    public function index()
    {
        $loginInfo 		    = get_loggedin_user_data();
        
        $data 				= array(
            'page_title'    => trans('language.view_track_vehicle'),
            'redirect_url'  => url('admin-panel/transport/track-vehicle/view-track-vehicle'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.track-vehicle.view-track-vehicle')->with($data);
    }

    /**
     *  Add page for Track Vehicle
     *  @Khushbu on 01 Oct 2018   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        if (!empty($id))
        {
            $page_title             = trans('language.edit_track_vehicle');
            $save_url               = url('admin-panel/transport/track-vehicle/save/' . $decrypted_job_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_track_vehicle');
            $save_url      = url('admin-panel/transport/track-vehicle/save');
            $submit_button = 'Save';
        }

        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/track-vehicle/view-track-vehicle'),
        );
        return view('admin-panel.track-vehicle.add-track-vehicle')->with($data);
    }
}
