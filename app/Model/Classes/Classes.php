<?php

namespace App\Model\Classes;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $table      = 'classes';
    protected $primaryKey = 'class_id';

    public function getSections()
    {
        return $this->hasMany('App\Model\Section\Section', 'class_id');
    }

    public function getTimeTable()
    {
        return $this->hasMany('App\Model\TimeTable\TimeTable', 'class_id');
    }
}
