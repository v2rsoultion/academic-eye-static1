@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Add RTE Cheque Details</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <a href="{!! url('admin-panel/fees-collection/rte-received-cheque/view-rte-cheque-detail') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-eye"></i> </a>
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">{!! trans('language.menu_fee_collection') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">RTE Received cheque</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection/rte-received-cheque/add-rte-cheque-detail') !!}">Add RTE Cheque Details</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
              </div>
              <div class="body form-gap">
                  <div class="row tabless" >
                       <!-- <div class="headingcommon  col-lg-12">Add Cheque Details :-</div> -->
                    <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                    <div class="col-lg-3">
                      <div class="form-group">
                        <lable class="from_one1" for="name">Bank</lable>
                        <select class="form-control show-tick select_form1" name="classes" id="">
                          <option value="">Select Bank</option>
                          <option>Bank Of Baroda</option>
                          <option>State Bank of India</option>
                          <option>Bank of India</option>
                          <option>Punjab National Bank</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3">
                       <div class="form-group">
                          <lable class="from_one1" for="name">Cheque No. </lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Cheque no.">
                       </div>
                    </div>
                     <div class="col-lg-3">
                       <div class="form-group">
                          <lable class="from_one1" for="name">Cheque Date </lable>
                          <input type="text" name="name" id="chequeDate" class="form-control" placeholder="Cheque date">
                       </div>
                    </div>
                     <div class="col-lg-3">
                       <div class="form-group">
                          <lable class="from_one1" for="name">Amount </lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Amounts">
                       </div>
                    </div>
                     <!-- <div class="col-lg-10"></div> -->
                     <!-- <div class="col-lg-12" style="border: 1px solid #ccc;"></div> -->
                     <div class="col-lg-12">
                       <hr>
                     </div>
                   <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
                      </button>
                    </div>
              </div>
                  </div>
                   
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
@endsection


