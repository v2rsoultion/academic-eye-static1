<?php $__env->startSection('content'); ?>

<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Add Conductor</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <a href="<?php echo url('admin-panel/transport/conductor/view-conductor'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-eye"></i> </a> 
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Transport</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Conductor</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/transport/conductor/add-conductor'); ?>">Add Conductor</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div>
                    <div class="body form-gap">
                        <form class="" action="" method=""  id="form_validation">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <lable class="from_one1">Vehicle Name :</lable>
                                    <div class="form-group">
                                        <input type="text" name="vehicle_name" class="form-control" placeholder="Vehicle Name">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <lable class="from_one1">Conductor Name :</lable>
                                    <div class="form-group">
                                        <input type="text" name="conductor_name" class="form-control" placeholder="Conductor Name">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-raised btn-primary">Save</button>
                                    <button type="submit" class="btn btn-raised btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@enssection

<script type="text/javascript">

$('#form_validation').validate({
        rules: {
            'vehicle_name': {
                required: true
            },
            'conductor_name': {
                required: true
            }
        },

        /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        vehcile_name: {
          required: 'Please fill your required vehcile name'
        },
        conductor_name: {
          required: 'Please fill requerd conductor name'
        }         
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

</script>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>