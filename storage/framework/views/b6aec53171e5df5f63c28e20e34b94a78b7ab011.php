<?php if($errors->any()): ?>
<div class="alert alert-danger" role="alert">
    <?php echo e($errors->first()); ?>

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<?php endif; ?>

<style type="text/css">
    .select2-container{
        margin: 0px auto;
    width: 240px !important;
    }
    .theme-blush .btn-primary {
    margin-top: 2px !important;
}
</style> 
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.job_name'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('job_name', old('job_name',isset($job['job_name']) ? $job['job_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.job_name'), 'id' => 'job_name']); ?>

        </div>
        <?php if($errors->has('job_name')): ?> <p class="help-block"><?php echo e($errors->first('job_name')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-3 col-md-3" style="position: relative;">
        <lable class="from_one1"><?php echo trans('language.medium_type'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('medium_type', $job['arr_medium'],isset($job['medium_type']) ? $job['medium_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type']); ?>

                <i class="arrow double"></i>
            </label>
            <?php if($errors->has('medium_type')): ?> <p class="help-block"><?php echo e($errors->first('medium_type')); ?></p> <?php endif; ?>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">Type:</lable>
        <div class="form-group">
            <div class="radio" style="margin-top:6px !important;">
                <?php if(isset($job['temporary'])): ?> 
                    <?php echo e(Form::radio('temporary', '1',  $job['temporary'] == '1', array('id'=>'radio12'))); ?>

                    <?php echo e(Form::label('radio12', 'Temporary', array('onclick'=>'checkForTemporary("1")', 'class' => 'document_staff'))); ?>


                    <?php echo e(Form::radio('temporary', '0',  $job['temporary'] == '0', array('id'=>'radio2'))); ?>

                <?php echo e(Form::label('radio2', 'Permament', array('onclick'=>'checkForTemporary("0")', 'class' => 'document_staff'))); ?>

                <?php else: ?>
                <?php echo e(Form::radio('temporary', '1',  false, array('id'=>'radio12'))); ?>

                <?php echo e(Form::label('radio12', 'Temporary', array('onclick'=>'checkForTemporary("1")', 'class' => 'document_staff'))); ?>

                <?php echo e(Form::radio('temporary', '0',  true, array('id'=>'radio2'))); ?>

                <?php echo e(Form::label('radio2', 'Permament', array('onclick'=>'checkForTemporary("0")', 'class' => 'document_staff'))); ?>

                <?php endif; ?>
                
            </div>
        </div>
    </div>    
    <div class="col-lg-3 col-md-3" style="<?php if(isset($job['temporary']) && $job['temporary'] == '1'): ?> display:block; <?php else: ?>  display:none; <?php endif; ?>" id="jobTemporaryBlock">
            <lable class="from_one1"><?php echo trans('language.job_durations'); ?> :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    <?php echo Form::select('job_durations', $job['job_durations'],isset($job['job_durations']) ? $job['job_durations'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'job_durations', 'required']); ?>

                    <i class="arrow double"></i>
                </label>
            </div>
            <?php if($errors->has('job_durations')): ?> <p class="help-block"><?php echo e($errors->first('job_durations')); ?></p> <?php endif; ?>
    </div> 
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.job_no_of_vacancy'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::number('job_no_of_vacancy', old('job_no_of_vacancy',isset($job['job_no_of_vacancy']) ? $job['job_no_of_vacancy']: 0), ['class' => 'form-control ','placeholder'=>trans('language.job_no_of_vacancy'), 'id' => 'job_no_of_vacancy', 'min' => 0]); ?>

        </div>
        <?php if($errors->has('job_no_of_vacancy')): ?> <p class="help-block"><?php echo e($errors->first('job_no_of_vacancy')); ?></p> <?php endif; ?>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <lable class="from_one1"><?php echo trans('language.job_recruitment'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::textarea('job_recruitment', old('job_recruitment',isset($scholastic['job_recruitment']) ? $scholastic['job_recruitment']: ''), ['class' => 'form-control','placeholder'=>trans('language.job_recruitment'), 'id' => 'job_recruitment', 'rows'=> 3]); ?>

        </div>
        <?php if($errors->has('job_recruitment')): ?> <p class="help-block"><?php echo e($errors->first('job_recruitment')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-12 col-md-12">
        <lable class="from_one1"><?php echo trans('language.job_description'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::textarea('job_description', old('job_description',isset($scholastic['job_description']) ? $scholastic['job_description']: ''), ['class' => 'form-control','placeholder'=>trans('language.job_description'), 'id' => 'job_description', 'rows'=> 3]); ?>

        </div>
        <?php if($errors->has('job_description')): ?> <p class="help-block"><?php echo e($errors->first('job_description')); ?></p> <?php endif; ?>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-lg-1 ">
      <button type="submit" class="btn btn-raised btn-primary"  title="Save">Save
      </button>
    </div>
     <div class="col-lg-1 ">
      <button type="reset" class="btn btn-raised btn-primary" title="Clear">Cancel
      </button>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {

         jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
        }, "Please use only alphanumeric values");
        $("#job-form").validate({
            /* @validation  states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation  rules   ------------------------------------------ */
            rules: {
                job_name: {
                    required: true
                },
                medium_type: {
                    required: true
                },
                job_no_of_vacancy: {
                    required: true
                },
                job_durations: {
                    required: true
                },
                 job_recruitment: {
                    required: true
                },
                 job_description: {
                    required: true
                },

            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        //Added later by Pratyush on 18 July 2018
        $('#shift_start_time').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid(); });
        $('#shift_end_time').bootstrapMaterialDatePicker({ date: false,shortTime: true,format: 'hh:mm a' }).on('change', function(e, date){ $(this).valid(); });
        
    });

    function checkForTemporary(val) {
        var x = document.getElementById("jobTemporaryBlock");
        if(val == 0) {
            if (x.style.display === "none") {
                x.style.display = "none";
            } else {
                x.style.display = "none";
            }
        } else {
            if (x.style.display === "block") {
                x.style.display = "block";
            } else {
                x.style.display = "block";
            }
        }
    }

</script>