
<?php $__env->startSection('content'); ?>
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Trial Balance</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/account'); ?>">Accounts</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/account/view-trial-balance'); ?>">View Trial Balance</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card gap_bottom">
              <div class="body form-gap">
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">View Journal Entries :-</div> -->
                  <div class="table-responsive">
                    <table class="bold border" style="width:100%;float:left;">
                    <?php echo e(csrf_field()); ?>

                    <thead style="font-size: 14px;">
                      <tr class="border">
                        <th rowspan="3" style="width: 780px;" class="gap_left border_right">Particulars</th>
                        <th class="text-center" colspan="3" style="width: 300px;">Chirag & Co <br> 1-Apr-2018 to 31-Mar-2019</th>
                      </tr>
                      <tr class="border">
                        <th  class="text-center border_right" colspan="2">Closing Balance</th>
                      </tr>
                      <tr class="border">
                        <th class="text-center border_right">Debit</th>
                        <th class="text-center border_right">Credit</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Cash Account</td>
                        <td class="text-right">9,99,99,600.00</td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td>Fees</td>
                        <td> </td>
                        <td class="text-right border_right">9,99,98,000.00</td>
                      </tr>
                      <tr>
                        <td>Stationary</td>
                        <td> </td>
                        <td class="text-right border_right">1,600.00</td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td> </td>
                        <td class="border_right"> </td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr class="border">
                        <td>Grand Total</td>
                        <td class="text-right">9,99,99,600.00</td>
                        <td class="text-right border_right">9,99,99,600.00</td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>