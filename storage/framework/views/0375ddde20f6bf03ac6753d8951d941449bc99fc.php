<?php if(isset($city['city_id']) && !empty($city['city_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?>

<?php echo Form::hidden('city_id',old('city_id',isset($city['city_id']) ? $city['city_id'] : ''),['class' => 'gui-input', 'id' => 'city_id', 'readonly' => 'true']); ?>


<?php if($errors->any()): ?>
    <div class="alert alert-danger" role="alert">
    <?php echo e($errors->first()); ?>

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.country_name'); ?> :</lable>
        <label class=" field select" style="width: 100%">
            <?php echo Form::select('country_id', $city['arr_country'],isset($city['country_id']) ? $city['country_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'country_id', 'onChange'=>'getCountryState(this.value)']); ?>

            <i class="arrow double"></i>
        </label>
        <?php if($errors->has('country_id')): ?> <p class="help-block"><?php echo e($errors->first('country_id')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.state_name'); ?> :</lable>
        <label class=" field select" style="width: 100%">
            <?php echo Form::select('state_id', $city['arr_state'],isset($city['state_id']) ? $city['state_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'state_id']); ?>

            <i class="arrow double"></i>
        </label>
        <?php if($errors->has('state_id')): ?> <p class="help-block"><?php echo e($errors->first('state_id')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1"><?php echo trans('language.city_name'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('city_name', old('city_name',isset($city['city_name']) ? $city['city_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.city_name'), 'id' => 'city_name']); ?>

        </div>
        <?php if($errors->has('city_name')): ?> <p class="help-block"><?php echo e($errors->first('city_name')); ?></p> <?php endif; ?>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']); ?>

        <a href="<?php echo url('admin-panel/dashboard'); ?>" ><?php echo Form::button('Cancel', ['class' => 'btn btn-raised btn-round']); ?></a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#city-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            // errorLabelContainer: '.errorTxt',

            /* @validation  rules 
             ------------------------------------------ */
            rules: {
                country_id: {
                    required: true
                },
                state_id: {
                    required: true
                },
                city_name: {
                    required: true,
                    lettersonly:true
                },
            },
            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });  
    });

    function getCountryState(country_id)
    {
        if(country_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/state/get-country-state-data')); ?>",
                type: 'GET',
                data: {
                    'country_id': country_id
                },
                success: function (data) {
                    $("select[name='state_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='state_id'").html('<option value="">Select State</option>');
            $(".mycustloading").hide();
        }
    }
</script>