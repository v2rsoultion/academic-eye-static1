-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 24, 2018 at 06:22 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school_shree`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `admin_type` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0: super admin,1: school admin,2: staff admin,3: parent admin,4: student admin',
  `admin_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_password` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_profile_img` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_type`, `admin_name`, `email`, `password`, `default_password`, `admin_profile_img`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 0, 'Super Admin', 'admin@gmail.com', '$2y$10$ivvyJkg.XjVg1IhMrQlezuNIFBgMLh3M0UnpRHetyVuMABHhN.CwK', '', NULL, 'WZ74uPQCun2AdKXcrT7TzMFVGV2xSZiJ1EsC6yNxGKjfMvbq3e30xN8kEQSN', '2018-07-23 05:20:32', '2018-07-23 05:20:32'),
(2, 1, 'Bal Niketan', 'balniketan@gmail.com', '$2y$10$mIKbs5dzmDc1WObpTNZdqO8eBPAJHQjLh3VYYRBYy.ZUbJZ74/d6K', '', NULL, 'zMFb44VGNCDMGuCyXX6hkeRWN1pkH66tmPNQHNIOsWBZu9x7S6shFQdsB7KZ', '2018-07-23 16:31:09', '2018-07-23 16:31:09'),
(7, 3, 'Shrikanth', 'shrikanth@gmail.com', '$2y$10$zn77Itt6P0DIAfv9IGFALOCXpYHp5Yob0CfHQJRXwVoFOzDERaceG', '$2y$10$lOP0D7UHxnLcjSe4IZzzh.dUy5aglETUgCb4d4oOvME/PBKU7Fmvq', NULL, NULL, '2018-07-28 01:53:31', '2018-07-28 01:53:31'),
(8, 4, 'Jay', 'jay@gmail.com', '$2y$10$ExO1Ghzd0U0mPuGxcyzc1ug3xPUck0V4PlI03jPZCyJn9uEs9xDwC', '$2y$10$koEGdnjRS.trveTFKkMyqOfOzhxHnHPS81dPq8VSqhEp99k2WKHyi', NULL, NULL, '2018-07-28 01:53:32', '2018-07-28 01:53:32'),
(9, 3, 'a', 'p@p.com', '$2y$10$ncw7NMOcDTkZEx.oBy9Ox.SxWbH9PKEwYDH.pyT1JoVRyF7oAKsa6', '$2y$10$wft/IpQMP5Pl7iog0TODz.a.L5XbdB/X/cocMwgj/V2COqJJNktBW', NULL, NULL, '2018-07-30 06:17:16', '2018-07-30 06:17:16'),
(10, 4, 'suresh', 'pp@p.com', '$2y$10$42Oq97t.bFKnUHly4EOQV.NXSX7gQTDl3G3pEZ03Gq4.WLgqXxw66', '$2y$10$ruy5aR5z3TC5DUlN2jfoTeRIya4bZiIDxu26.Kgyl6JSjPbBrMvFK', NULL, NULL, '2018-07-30 06:17:16', '2018-07-30 06:17:16'),
(11, 3, 'a', 'p@p.com', '$2y$10$Po2.vZmwVcoSfL/MnX8/QOodErXltWvwevGhzCq38NVWYNZudrV7K', '$2y$10$OguD.oLnuZjvVEQD.Dy7h.FEqH5ADSVF9fBiAuBDrOr0SaYTQ4k86', NULL, NULL, '2018-07-30 06:19:28', '2018-07-30 06:19:28'),
(12, 4, 'ramesh', 'prp@p.com', '$2y$10$rETVyUBObQ0F0MhTxq4Ope9BlIEh3QD8t64BsUGPhUg4Tjz1vhyBS', '$2y$10$JL1EgkeW2vJaM.JjiwG84ehSGz6pW8cOhxW6mTyScJtrAwumX9ecy', NULL, NULL, '2018-07-30 06:19:28', '2018-07-30 06:19:28'),
(13, 3, 'q', 'q@gmail.com', '$2y$10$hjC5FH7aSWDhFvea2NduQuUV4nkBQ19ukWi4JZkbmsL44slrmSrqW', '$2y$10$4RFXmqTgOrFlRPBaX4lIcOfab/P0Ph3wHs6AV4lWdlTUFG7syit6y', NULL, NULL, '2018-07-30 06:21:14', '2018-07-30 06:21:14'),
(14, 4, 'gajesh', 'prrp@p.com', '$2y$10$kKdE./vZLtkEM1qiM1ATjeQ0B3SVi9rZYZXkyxVsqlJxxc.SICkta', '$2y$10$COXSEyh0tQbZ2KRH5YDckeiIzvoGe73YROmmDPdVnNKNUleh65C.S', NULL, NULL, '2018-07-30 06:21:14', '2018-07-30 06:21:14'),
(16, 4, 'Pratyush', 'test.pratyush@v2rsolution.co.in', '$2y$10$mIKbs5dzmDc1WObpTNZdqO8eBPAJHQjLh3VYYRBYy.ZUbJZ74/d6K', '$2y$10$uS7d4sjqpPUptqkD8WG4l.ZUPCkh0IKLPTkHlB5ojm64XI8lR5hGa', NULL, 'kVSAc3f4Uc2LskJcyiqeKQctPpVn65DgE0cDMzvj2Uh0V624XwfAYzeNAcXe', '2018-08-09 00:43:46', '2018-08-09 00:43:46'),
(17, 4, 'Rajat Dave', 'test.pratyush1@v2rsolution.co.in', '$2y$10$Y1pFP845f0JqzT9TTzSb0u/oX3ukNzdR1w0burTv1Rvu0vS59CVSe', '$2y$10$nQn5GoMblq0CaDQwMVVSpO3P0/zt7ut0k72IHQ8LUSt5EV9kJ5DP6', NULL, NULL, '2018-08-10 05:37:43', '2018-08-10 05:37:43'),
(18, 3, 'q', 'q@gmail.com', '$2y$10$AlOjETnYkVYFtslIzcO.UONqLvEfGFgifIV4KxrLTmIf5y9sDLFYu', '$2y$10$T27bSW2irjfSryd522YxceSHHweKOseE6Vn7.IPwNtECBgDfxEe2S', NULL, NULL, '2018-08-13 02:55:31', '2018-08-13 02:55:31'),
(19, 4, '5252', '879897pp@p.com', '$2y$10$.Iw34fwxq4iL4a5NdRcQzeAmuOcvVriWZTOpb2eDXRl3EMpwEaTmW', '$2y$10$awBSkm/qwSKU7yFfMzslpeYmGL61CIVldfg3WWZjGvvZtJVkVDi.q', NULL, NULL, '2018-08-13 02:55:36', '2018-08-13 02:55:36'),
(21, 3, 'e', 'e', '$2y$10$f6yM2vFM8fLBxQjbETYTReWcdnfZ4SKe1Dblb1C6idlKbUzCNKsx.', '$2y$10$kmgAkrtrOjq2MXmqKn2ySuvGhzPq6Gh2mOMYd1C17uSVUqyg.NO8i', NULL, NULL, '2018-08-23 03:55:38', '2018-08-23 03:55:38'),
(22, 4, '23232', 'sdasdsadpp@p.com', '$2y$10$QapLwMx9ybMPy0IEYk8g4eeVfgjiI7AXxHi//bqxsjffsjYlk6s7y', '$2y$10$qpEPN/uSnNkVWefnqt.fh.1LiN1GITrS2WDDqyov53Yk5bhLPqCRK', NULL, NULL, '2018-08-23 03:55:38', '2018-08-23 03:55:38');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `book_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_category_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=General ,1=Barcoded',
  `book_subtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_isbn_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publisher_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_copies_no` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_cupboard_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_cupboardshelf_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_price` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `admin_id`, `update_by`, `school_id`, `book_category_id`, `book_name`, `book_type`, `book_subtitle`, `book_isbn_no`, `author_name`, `publisher_name`, `edition`, `book_vendor_id`, `book_copies_no`, `book_cupboard_id`, `book_cupboardshelf_id`, `book_price`, `book_remark`, `book_status`, `created_at`, `updated_at`) VALUES
(4, 2, 2, 1, 2, 'Godaan', 1, 'Godaan', '123456789123', 'Munshi Premchand', 'Amazon', 'second', 1, 2, 4, 3, 351, NULL, 1, '2018-08-22 02:34:33', '2018-08-22 02:34:33'),
(5, 2, 2, 1, 2, 'Nirmala', 1, 'Nirmala', '8975432154', 'Munshi Premchand', 'Penguin Random', 'Third', 2, 6, 4, 3, 680, NULL, 1, '2018-08-22 02:37:27', '2018-08-22 02:37:27');

-- --------------------------------------------------------

--
-- Table structure for table `book_allowance`
--

CREATE TABLE `book_allowance` (
  `book_allowance_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `allow_for_staff` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `allow_for_student` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_category_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_allowance`
--

INSERT INTO `book_allowance` (`book_allowance_id`, `admin_id`, `update_by`, `school_id`, `allow_for_staff`, `allow_for_student`, `book_category_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 10, 5, 1, '2018-08-13 05:30:38', '2018-08-22 02:03:03');

-- --------------------------------------------------------

--
-- Table structure for table `book_categories`
--

CREATE TABLE `book_categories` (
  `book_category_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_category_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_categories`
--

INSERT INTO `book_categories` (`book_category_id`, `admin_id`, `update_by`, `school_id`, `book_category_name`, `book_category_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Non Fiction', 1, '2018-08-13 04:44:08', '2018-08-13 04:51:26'),
(2, 2, 2, 1, 'Fiction', 1, '2018-08-14 07:24:14', '2018-08-14 07:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `book_copies_info`
--

CREATE TABLE `book_copies_info` (
  `book_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `update_by` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `book_id` int(10) UNSIGNED NOT NULL,
  `book_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exclusive_for_staff` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=No,1=Yes',
  `book_copy_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Available,1=Issued',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_copies_info`
--

INSERT INTO `book_copies_info` (`book_info_id`, `admin_id`, `update_by`, `school_id`, `book_id`, `book_unique_id`, `exclusive_for_staff`, `book_copy_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 4, 'godaan124', 1, 1, '2018-08-22 02:34:33', '2018-08-22 04:53:30'),
(2, 2, 2, 1, 4, 'godaan135', 0, 0, '2018-08-22 02:34:33', '2018-08-22 02:34:33'),
(4, 2, 2, 1, 5, 'nirmala012', 0, 0, '2018-08-22 02:37:27', '2018-08-23 02:09:31'),
(5, 2, 2, 1, 5, 'nirmala013', 0, 0, '2018-08-22 02:37:27', '2018-08-23 02:13:09'),
(6, 2, 2, 1, 5, 'nirmala014', 0, 0, '2018-08-22 02:37:27', '2018-08-22 02:37:27'),
(7, 2, 2, 1, 5, 'nirmala015', 0, 0, '2018-08-22 02:37:27', '2018-08-22 02:37:27'),
(8, 2, 2, 1, 5, 'nirmala016', 1, 0, '2018-08-22 02:37:27', '2018-08-22 02:37:27'),
(9, 2, 2, 1, 5, 'nirmala017', 1, 0, '2018-08-22 02:37:27', '2018-08-22 02:37:27');

--
-- Triggers `book_copies_info`
--
DELIMITER $$
CREATE TRIGGER `add_book_copies` BEFORE INSERT ON `book_copies_info` FOR EACH ROW UPDATE books SET book_copies_no  = book_copies_no + 1 WHERE books.book_id = new.book_id
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `minus_book_copies` BEFORE DELETE ON `book_copies_info` FOR EACH ROW UPDATE books SET book_copies_no = book_copies_no - 1 WHERE books.book_id = old.book_id
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `book_cupboards`
--

CREATE TABLE `book_cupboards` (
  `book_cupboard_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_cupboard_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_cupboard_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_cupboards`
--

INSERT INTO `book_cupboards` (`book_cupboard_id`, `admin_id`, `update_by`, `school_id`, `book_cupboard_name`, `book_cupboard_status`, `created_at`, `updated_at`) VALUES
(2, 2, 2, 1, 'AXWY', 1, '2018-08-13 07:17:02', '2018-08-13 07:17:02'),
(3, 2, 2, 1, 'ADCX', 1, '2018-08-13 07:17:15', '2018-08-13 07:17:15'),
(4, 2, 2, 1, 'SSKD', 1, '2018-08-13 07:17:21', '2018-08-13 07:17:21'),
(5, 2, 2, 1, 'AAASD', 1, '2018-08-13 08:46:37', '2018-08-13 08:46:37');

-- --------------------------------------------------------

--
-- Table structure for table `book_cupboardshelfs`
--

CREATE TABLE `book_cupboardshelfs` (
  `book_cupboardshelf_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_cupboard_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_cupboardshelf_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_cupboard_capacity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_cupboard_shelf_detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `book_cupboardshelf_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_cupboardshelfs`
--

INSERT INTO `book_cupboardshelfs` (`book_cupboardshelf_id`, `admin_id`, `update_by`, `school_id`, `book_cupboard_id`, `book_cupboardshelf_name`, `book_cupboard_capacity`, `book_cupboard_shelf_detail`, `book_cupboardshelf_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 3, 'shelf 1', 150, 'details', 1, '2018-08-13 08:07:41', '2018-08-13 08:07:41'),
(3, 2, 2, 1, 4, 'shelf 1 edit', 40, '040', 1, '2018-08-13 08:37:26', '2018-08-13 08:37:26');

-- --------------------------------------------------------

--
-- Table structure for table `book_vendors`
--

CREATE TABLE `book_vendors` (
  `book_vendor_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_vendor_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_vendors`
--

INSERT INTO `book_vendors` (`book_vendor_id`, `admin_id`, `update_by`, `school_id`, `book_vendor_name`, `book_vendor_number`, `book_vendor_email`, `book_vendor_address`, `book_vendor_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Ram Lal', '9988556655', 'p@gmail.com', 'aaa', 1, '2018-08-14 08:15:09', '2018-08-14 08:15:09'),
(2, 2, 2, 1, 'Shyam Lal', '11111111111', 'p1@gmail.com', 'dddd', 1, '2018-08-14 08:15:39', '2018-08-14 08:15:39');

-- --------------------------------------------------------

--
-- Table structure for table `caste`
--

CREATE TABLE `caste` (
  `caste_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `caste_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caste_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `caste`
--

INSERT INTO `caste` (`caste_id`, `admin_id`, `update_by`, `school_id`, `caste_name`, `caste_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Rajput', 1, '2018-07-23 17:03:12', '2018-07-23 17:03:12'),
(2, 2, 2, 1, 'Sain', 1, '2018-07-23 17:03:21', '2018-07-23 17:03:21'),
(3, 2, 2, 1, 'Jain', 0, '2018-07-23 17:03:28', '2018-07-23 17:04:03');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `class_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `class_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_intake` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'capacity/seats',
  `class_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `class_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`class_id`, `admin_id`, `update_by`, `school_id`, `class_name`, `class_intake`, `class_order`, `class_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Class I', 40, 1, 1, '2018-07-23 17:10:37', '2018-07-23 17:10:37'),
(2, 2, 2, 1, 'Class II', 40, 2, 1, '2018-07-23 17:11:05', '2018-07-23 17:11:05'),
(3, 2, 2, 1, 'Class III', 40, 3, 1, '2018-07-23 17:11:50', '2018-07-24 12:57:17');

-- --------------------------------------------------------

--
-- Table structure for table `class_subject_staff_mapping`
--

CREATE TABLE `class_subject_staff_mapping` (
  `class_subject_staff_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_ids` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `class_subject_staff_mapping`
--

INSERT INTO `class_subject_staff_mapping` (`class_subject_staff_map_id`, `admin_id`, `update_by`, `school_id`, `class_id`, `subject_id`, `staff_ids`, `created_at`, `updated_at`) VALUES
(16, 2, 2, 1, 1, 5, 1, '2018-08-14 05:39:43', '2018-08-14 05:39:43'),
(17, 2, 2, 1, 1, 5, 3, '2018-08-14 05:39:43', '2018-08-14 05:39:43'),
(19, 2, 2, 1, 2, 1, 1, '2018-08-14 05:40:05', '2018-08-14 05:40:05'),
(20, 2, 2, 1, 2, 2, 1, '2018-08-14 05:40:08', '2018-08-14 05:40:08'),
(21, 2, 2, 1, 2, 2, 3, '2018-08-14 05:40:08', '2018-08-14 05:40:08'),
(23, 2, 2, 1, 2, 5, 1, '2018-08-14 05:40:19', '2018-08-14 05:40:19'),
(24, 2, 2, 1, 2, 5, 2, '2018-08-14 05:40:19', '2018-08-14 05:40:19'),
(25, 2, 2, 1, 2, 5, 3, '2018-08-14 05:40:19', '2018-08-14 05:40:19'),
(46, 2, 2, 1, 1, 1, 2, '2018-08-17 07:50:53', '2018-08-17 07:50:53'),
(47, 2, 2, 1, 1, 1, 3, '2018-08-17 07:50:53', '2018-08-17 07:50:53'),
(48, 2, 2, 1, 1, 2, 1, '2018-08-17 07:50:59', '2018-08-17 07:50:59'),
(49, 2, 2, 1, 1, 2, 2, '2018-08-17 07:50:59', '2018-08-17 07:50:59'),
(50, 2, 2, 1, 1, 2, 3, '2018-08-17 07:50:59', '2018-08-17 07:50:59');

-- --------------------------------------------------------

--
-- Table structure for table `competitions`
--

CREATE TABLE `competitions` (
  `competition_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `competition_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `competition_description` text COLLATE utf8mb4_unicode_ci,
  `competition_date` date DEFAULT NULL,
  `competition_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=School,1=Class',
  `competition_class_ids` text COLLATE utf8mb4_unicode_ci,
  `competition_issue_participation_certificate` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `competition_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `competitions`
--

INSERT INTO `competitions` (`competition_id`, `admin_id`, `update_by`, `school_id`, `competition_name`, `competition_description`, `competition_date`, `competition_level`, `competition_class_ids`, `competition_issue_participation_certificate`, `competition_status`, `created_at`, `updated_at`) VALUES
(3, 2, 2, 1, 'My Competition one', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2018-08-15', 1, '1,2', 1, 1, '2018-08-03 05:10:04', '2018-08-03 05:10:04'),
(4, 2, 2, 1, 'My Competition two', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2018-08-15', 0, NULL, 0, 1, '2018-08-03 05:10:29', '2018-08-03 05:10:29');

-- --------------------------------------------------------

--
-- Table structure for table `co_scholastic_types`
--

CREATE TABLE `co_scholastic_types` (
  `co_scholastic_type_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `co_scholastic_type_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `co_scholastic_type_description` text COLLATE utf8mb4_unicode_ci,
  `co_scholastic_type_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `co_scholastic_types`
--

INSERT INTO `co_scholastic_types` (`co_scholastic_type_id`, `admin_id`, `update_by`, `school_id`, `co_scholastic_type_name`, `co_scholastic_type_description`, `co_scholastic_type_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Discipline', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters', 1, '2018-07-24 13:00:37', '2018-07-24 13:00:37'),
(2, 2, 2, 1, 'Co-scholastic', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters', 1, '2018-07-24 13:00:56', '2018-07-24 13:00:56'),
(3, 2, 2, 1, 'Life Skill', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters', 1, '2018-07-24 13:01:27', '2018-07-24 13:01:27'),
(4, 2, 2, 1, 'Work education', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters', 1, '2018-07-24 13:01:41', '2018-07-24 13:01:41'),
(5, 2, 2, 1, 'Health and physical education', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,', 1, '2018-07-24 14:06:15', '2018-07-24 14:06:15'),
(6, 2, 2, 1, 'Attitudes and Values', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia', 1, '2018-07-24 14:06:39', '2018-07-24 14:06:39'),
(7, 2, 2, 1, 'Co-Curricular Activities', 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form', 1, '2018-07-24 14:07:19', '2018-07-24 14:07:19');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `designation_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `designation_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation_description` text COLLATE utf8mb4_unicode_ci,
  `designation_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`designation_id`, `admin_id`, `update_by`, `school_id`, `designation_name`, `designation_description`, `designation_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Teacher', NULL, 1, '2018-08-05 21:50:43', '2018-08-05 21:50:43');

-- --------------------------------------------------------

--
-- Table structure for table `document_category`
--

CREATE TABLE `document_category` (
  `document_category_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `document_category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_category_for` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=student,1=staff,2=both',
  `document_category_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `document_category`
--

INSERT INTO `document_category` (`document_category_id`, `admin_id`, `update_by`, `school_id`, `document_category_name`, `document_category_for`, `document_category_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Policies and Procedures', 2, 1, '2018-07-23 17:00:35', '2018-07-23 17:00:35');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `exam_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `exam_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `term_exam_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`exam_id`, `admin_id`, `update_by`, `school_id`, `exam_name`, `term_exam_id`, `exam_status`, `created_at`, `updated_at`) VALUES
(3, 2, 2, 1, 'Half year', 1, 0, '2018-08-11 02:36:20', '2018-08-15 22:46:12'),
(4, 2, 2, 1, 'New Test Exam Edited', 1, 1, '2018-08-15 22:46:06', '2018-08-15 22:46:06'),
(5, 2, 2, 1, 'yeyey- 123', NULL, 1, '2018-08-15 22:57:02', '2018-08-15 22:57:02');

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `facility_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `facility_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facility_fees_applied` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=not apply,1=apply',
  `facility_fees` decimal(18,2) NOT NULL DEFAULT '0.00',
  `facility_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`facility_id`, `admin_id`, `update_by`, `school_id`, `facility_name`, `facility_fees_applied`, `facility_fees`, `facility_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Libraries', 0, '0.00', 1, '2018-07-23 16:57:50', '2018-07-23 16:57:50'),
(2, 2, 2, 1, 'Sports Facilities', 1, '1000.00', 1, '2018-07-23 16:58:17', '2018-07-23 16:58:44');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`group_id`, `admin_id`, `update_by`, `school_id`, `group_name`, `group_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Group A', 1, '2018-07-23 17:08:49', '2018-07-23 17:08:49'),
(2, 2, 2, 1, 'Group B', 1, '2018-07-23 17:09:02', '2018-07-23 17:09:02'),
(3, 2, 2, 1, 'Group C', 1, '2018-07-23 17:09:53', '2018-07-23 17:09:53');

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `holiday_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `session_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `holiday_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `holiday_start_date` date DEFAULT NULL,
  `holiday_end_date` date DEFAULT NULL,
  `holiday_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`holiday_id`, `admin_id`, `update_by`, `school_id`, `session_id`, `holiday_name`, `holiday_start_date`, `holiday_end_date`, `holiday_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 'Summer Break', '2018-05-17', '2018-07-08', 1, '2018-07-23 16:36:19', '2018-07-23 16:36:19'),
(2, 2, 2, 1, 1, 'Autumn Break', '2018-08-17', '2018-08-21', 0, '2018-07-23 16:37:25', '2018-07-23 16:40:10'),
(3, 2, 2, 1, 1, 'Diwali Break', '2018-11-04', '2018-11-11', 1, '2018-07-23 16:40:42', '2018-07-23 16:40:42');

-- --------------------------------------------------------

--
-- Table structure for table `issued_books`
--

CREATE TABLE `issued_books` (
  `issued_book_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `member_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `member_type` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_copies_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `issued_from_date` date DEFAULT NULL,
  `issued_to_date` date DEFAULT NULL,
  `issued_book_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=issued,2=returned',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `issued_books`
--

INSERT INTO `issued_books` (`issued_book_id`, `admin_id`, `update_by`, `school_id`, `book_id`, `member_id`, `member_type`, `book_copies_id`, `issued_from_date`, `issued_to_date`, `issued_book_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 5, 8, 1, 4, '2018-08-26', '2018-08-31', 2, '2018-08-22 03:59:07', '2018-08-23 02:09:31'),
(2, 2, 2, 1, 4, 5, 2, 1, '2018-09-01', '2018-09-19', 1, '2018-08-22 04:53:30', '2018-08-23 02:12:42'),
(3, 2, 2, 1, 5, 5, 2, 5, '2018-08-14', '2019-08-30', 2, '2018-08-22 04:53:47', '2018-08-23 02:13:09');

-- --------------------------------------------------------

--
-- Table structure for table `issued_books_history`
--

CREATE TABLE `issued_books_history` (
  `history_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `member_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `member_type` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_copies_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `issued_from_date` date DEFAULT NULL,
  `issued_to_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `issued_books_history`
--

INSERT INTO `issued_books_history` (`history_id`, `admin_id`, `update_by`, `school_id`, `book_id`, `member_id`, `member_type`, `book_copies_id`, `issued_from_date`, `issued_to_date`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 5, 8, 1, 4, '2018-08-21', '2018-08-25', '2018-08-23 01:58:49', '2018-08-23 01:58:49'),
(2, 2, 2, 1, 4, 5, 2, 1, '2018-08-20', '2018-08-29', '2018-08-23 02:11:39', '2018-08-23 02:11:39'),
(3, 2, 2, 1, 4, 5, 2, 1, '2018-08-24', '2018-08-31', '2018-08-23 02:12:42', '2018-08-23 02:12:42');

-- --------------------------------------------------------

--
-- Table structure for table `library_fine`
--

CREATE TABLE `library_fine` (
  `library_fine_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `member_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `member_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=student,2=staff',
  `fine_amount` decimal(18,2) NOT NULL DEFAULT '0.00',
  `fine_date` date DEFAULT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci,
  `fine_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=due,2=paid',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `library_fine`
--

INSERT INTO `library_fine` (`library_fine_id`, `admin_id`, `update_by`, `school_id`, `member_id`, `member_type`, `fine_amount`, `fine_date`, `remark`, `fine_status`, `created_at`, `updated_at`) VALUES
(2, 2, 2, 1, 6, 2, '150.00', '2018-08-23', 'ha', 2, '2018-08-23 09:27:33', '2018-08-23 09:27:33');

-- --------------------------------------------------------

--
-- Table structure for table `library_members`
--

CREATE TABLE `library_members` (
  `library_member_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `member_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `library_member_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=student,2=staff',
  `member_member_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `library_members`
--

INSERT INTO `library_members` (`library_member_id`, `admin_id`, `update_by`, `school_id`, `member_id`, `library_member_type`, `member_member_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 1, '2018-08-21 03:47:52', '2018-08-21 03:47:52'),
(2, 2, 2, 1, 2, 1, 1, '2018-08-21 03:48:00', '2018-08-21 03:48:00'),
(3, 2, 2, 1, 3, 1, 1, '2018-08-21 03:48:00', '2018-08-21 03:48:00'),
(4, 2, 2, 1, 5, 1, 1, '2018-08-21 03:48:00', '2018-08-21 03:48:00'),
(5, 2, 2, 1, 1, 2, 1, '2018-08-21 04:13:02', '2018-08-21 04:13:02'),
(6, 2, 2, 1, 2, 2, 1, '2018-08-21 04:16:19', '2018-08-21 04:16:19'),
(7, 2, 2, 1, 3, 2, 1, '2018-08-21 04:16:19', '2018-08-21 04:16:19'),
(8, 2, 2, 1, 6, 1, 1, '2018-08-22 02:59:03', '2018-08-22 02:59:03');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_07_13_073741_create_admins_table', 1),
(2, '2018_07_13_074017_create_schools_table', 1),
(3, '2018_07_16_071108_create_sessions_table', 1),
(4, '2018_07_17_131027_create_holidays_table', 1),
(5, '2018_07_18_054732_create_shifts_table', 1),
(6, '2018_07_18_105806_create_facilities_table', 1),
(7, '2018_07_18_110518_create_document_category_table', 1),
(8, '2018_07_18_111015_create_designation_table', 1),
(9, '2018_07_18_120941_create_titles_table', 1),
(10, '2018_07_18_121332_create_caste_table', 1),
(11, '2018_07_18_122048_create_religions_table', 1),
(12, '2018_07_18_123744_create_nationality_table', 1),
(13, '2018_07_18_124046_create_groups_table', 1),
(14, '2018_07_18_124417_create_classes_table', 1),
(15, '2018_07_18_124701_create_sections_table', 1),
(16, '2018_07_20_075703_create_co_scholastic_types_table', 1),
(17, '2018_07_20_133716_create_subjects_table', 1),
(18, '2018_07_20_135314_create_virtual_classes_table', 1),
(19, '2018_07_21_044454_create_competitions_table', 1),
(26, '2018_07_24_103008_add_feilds_students_table', 3),
(28, '2018_07_25_053735_create_students_table', 4),
(29, '2018_07_25_054105_create_student_parents_table', 5),
(30, '2018_07_25_055307_create_student_acdemic_info_table', 6),
(31, '2018_07_25_055518_create_student_academic_history_info_table', 6),
(32, '2018_07_25_055610_create_student_health_info_table', 6),
(33, '2018_07_25_061138_create_add_field_student_parents_table', 7),
(34, '2018_07_28_113000_create_room_no_table', 8),
(35, '2018_07_30_095105_create_student_parents_table', 9),
(36, '2018_07_30_095424_create_student_parents_table', 10),
(37, '2018_07_30_095542_create_students_table', 10),
(38, '2018_07_30_095700_create_student_academic_info_table', 10),
(39, '2018_07_30_095738_create_student_academic_history_info_table', 10),
(40, '2018_07_30_095843_create_student_health_info_table', 10),
(41, '2018_07_30_102303_create_student_virtual_class_mapping_table', 11),
(42, '2018_07_30_103810_create_student_competition_mapping_table', 12),
(43, '2018_07_30_104143_create_online_notes_table', 13),
(44, '2018_07_30_105707_create_term_exams_table', 14),
(45, '2018_07_30_110228_create_exams_table', 15),
(46, '2018_07_30_110747_create_question_papers_table', 16),
(47, '2018_08_02_064612_create_staff_roles_table', 17),
(48, '2018_08_02_065020_create_staff_table', 17),
(49, '2018_08_02_073410_create_question_papers_table', 17),
(50, '2018_08_02_074128_create_staff_documents_table', 17),
(51, '2018_08_02_075157_create_student_documents_table', 17),
(52, '2018_08_02_075844_create_staff_class_allocations_table', 17),
(53, '2018_08_02_091223_create_subject_class_mapping_table', 17),
(54, '2018_08_02_092920_create_class_subject_staff_mapping_table', 17),
(55, '2018_08_02_094038_create_staff_leaves_table', 17),
(56, '2018_08_02_094805_create_student_leaves_table', 17),
(57, '2018_08_11_042543_create_stream_table', 18),
(58, '2018_08_13_043408_create_book_category_table', 19),
(59, '2018_08_13_043437_create_book_allowance_table', 19),
(60, '2018_08_13_043453_create_book_vendor_table', 19),
(61, '2018_08_13_043518_create_book_cupboard_table', 19),
(62, '2018_08_13_043534_create_book_cupboardshelf_table', 19),
(64, '2018_08_14_043603_create_books_table', 20),
(67, '2018_08_14_043622_create_book_info_table', 21),
(69, '2018_08_21_041400_create_members_table', 22),
(70, '2018_08_21_151734_create_issued_books_table', 23),
(71, '2018_08_22_130233_create_issued_books_history_table', 24),
(73, '2018_08_23_075944_create_library_fine_table', 25);

-- --------------------------------------------------------

--
-- Table structure for table `nationality`
--

CREATE TABLE `nationality` (
  `nationality_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `nationality_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nationality`
--

INSERT INTO `nationality` (`nationality_id`, `admin_id`, `update_by`, `school_id`, `nationality_name`, `nationality_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Honduran', 1, '2018-07-23 17:08:06', '2018-07-23 17:08:06'),
(2, 2, 2, 1, 'Hungarian', 1, '2018-07-23 17:08:17', '2018-07-23 17:08:17'),
(3, 2, 2, 1, 'Icelander', 1, '2018-07-23 17:08:24', '2018-07-23 17:08:24'),
(4, 2, 2, 1, 'Indian', 1, '2018-07-23 17:08:31', '2018-07-23 17:08:31');

-- --------------------------------------------------------

--
-- Table structure for table `online_notes`
--

CREATE TABLE `online_notes` (
  `online_note_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `online_note_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `online_note_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `online_note_topic` text COLLATE utf8mb4_unicode_ci,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `online_note_url` text COLLATE utf8mb4_unicode_ci,
  `online_note_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `question_papers`
--

CREATE TABLE `question_papers` (
  `question_paper_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `question_paper_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `question_paper_file` text COLLATE utf8mb4_unicode_ci,
  `question_paper_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `religions`
--

CREATE TABLE `religions` (
  `religion_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `religion_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `religions`
--

INSERT INTO `religions` (`religion_id`, `admin_id`, `update_by`, `school_id`, `religion_name`, `religion_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Hinduism', 1, '2018-07-23 17:06:16', '2018-07-23 17:06:16'),
(2, 2, 2, 1, 'Christianity', 1, '2018-07-23 17:06:29', '2018-07-23 17:06:29'),
(3, 2, 2, 1, 'Sikhism', 1, '2018-07-23 17:06:37', '2018-07-23 17:06:37'),
(4, 2, 2, 1, 'Islam', 1, '2018-07-23 17:06:43', '2018-07-23 17:06:43'),
(5, 2, 2, 1, 'Buddhism', 1, '2018-07-23 17:07:36', '2018-07-23 17:07:36'),
(6, 2, 2, 1, 'Zoroastrianism', 1, '2018-07-23 17:07:43', '2018-07-23 17:07:43');

-- --------------------------------------------------------

--
-- Table structure for table `room_no`
--

CREATE TABLE `room_no` (
  `room_no_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `room_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_no_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_no`
--

INSERT INTO `room_no` (`room_no_id`, `admin_id`, `update_by`, `school_id`, `room_no`, `room_no_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, '101', 1, '2018-08-01 08:19:55', '2018-08-01 08:19:55');

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `school_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school_registration_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_sno_numbers` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'SNO numbers are stored with comma',
  `school_description` text COLLATE utf8mb4_unicode_ci,
  `school_address` text COLLATE utf8mb4_unicode_ci,
  `school_district` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_pincode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_board_of_exams` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_medium` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_class_from` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_class_to` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_total_students` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_total_staff` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_fee_range_from` double(18,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `school_fee_range_to` double(18,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `school_facilities` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Facilities are stored with comma',
  `school_url` text COLLATE utf8mb4_unicode_ci,
  `school_logo` text COLLATE utf8mb4_unicode_ci,
  `school_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school_mobile_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_telephone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_fax_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`school_id`, `admin_id`, `update_by`, `school_name`, `school_registration_no`, `school_sno_numbers`, `school_description`, `school_address`, `school_district`, `school_city`, `school_state`, `school_pincode`, `school_board_of_exams`, `school_medium`, `school_class_from`, `school_class_to`, `school_total_students`, `school_total_staff`, `school_fee_range_from`, `school_fee_range_to`, `school_facilities`, `school_url`, `school_logo`, `school_email`, `school_mobile_number`, `school_telephone`, `school_fax_number`, `school_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'Bal Niketan', 'P/1/15/08234/0544', 'P/1/15/08234/0544,P/1/15/08234/0545,P/1/15/08234/0546', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.', 'It is a long established fact', 'Jodhpur', 'Jodhpur', 'Rajasthan', '342006', 'CBSE', 'English', 1, 12, 1, 0, 100000.00, 1000000.00, 'Libraries,ICT,Auditorium,Innovation Studio,Sports Facilities,Art Room,Music Hall,Books and Uniforms Store', 'https://www.balniketan.com', 'profile_av775521531831662804571532345469.jpg', 'balniketan@gmail.com', '9876543210', '0291-2987584', '+91 161 999 8888', 1, '2018-07-23 16:31:09', '2018-07-23 16:31:09');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `section_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `class_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `section_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `section_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`section_id`, `admin_id`, `update_by`, `school_id`, `class_id`, `section_name`, `section_order`, `section_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 'A', 1, 1, '2018-07-23 17:12:03', '2018-07-23 17:33:13'),
(2, 2, 2, 1, 1, 'B', 2, 1, '2018-07-24 12:53:27', '2018-07-24 12:53:27'),
(3, 2, 2, 1, 2, 'A', 3, 1, '2018-07-24 12:53:53', '2018-07-24 12:55:42'),
(4, 2, 2, 1, 2, 'B', 4, 1, '2018-07-24 12:54:49', '2018-07-24 12:54:49'),
(5, 2, 2, 1, 3, 'A', 5, 1, '2018-07-24 12:55:06', '2018-07-24 12:55:06'),
(6, 2, 2, 1, 3, 'B', 6, 1, '2018-07-24 12:55:27', '2018-07-24 12:55:27'),
(7, 2, 2, 1, 3, 'C', 6, 1, '2018-07-27 23:46:03', '2018-07-27 23:46:03');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `session_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_start_date` date DEFAULT NULL,
  `session_end_date` date DEFAULT NULL,
  `session_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `admin_id`, `update_by`, `school_id`, `session_name`, `session_start_date`, `session_end_date`, `session_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, '2018-2019', '2018-04-01', '2019-03-31', 1, '2018-07-23 16:34:03', '2018-08-06 00:09:17'),
(2, 2, 2, 1, '2019-2020', '2018-04-01', '2019-03-31', 1, '2018-07-23 16:35:17', '2018-07-23 16:35:17');

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `shift_id` int(11) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `shift_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shift_start_time` time DEFAULT NULL,
  `shift_end_time` time DEFAULT NULL,
  `shift_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shifts`
--

INSERT INTO `shifts` (`shift_id`, `admin_id`, `update_by`, `school_id`, `shift_name`, `shift_start_time`, `shift_end_time`, `shift_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, '1st Shift', '08:00:00', '14:00:00', 1, '2018-07-23 16:54:42', '2018-07-23 16:54:42'),
(2, 2, 2, 1, '2nd Shift', '13:00:00', '18:00:00', 1, '2018-07-23 16:55:11', '2018-07-23 16:55:11'),
(3, 2, 2, 1, 'Test', '07:00:00', '17:19:00', 1, '2018-08-03 04:23:37', '2018-08-03 04:23:37');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `reference_admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `staff_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_profile_img` text COLLATE utf8mb4_unicode_ci,
  `designation_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_role_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_dob` date DEFAULT NULL,
  `staff_gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Male,1=Female',
  `staff_father_name_husband_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_mother_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_blood_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_marital_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Unmarried,1=Married',
  `caste_id` int(10) UNSIGNED DEFAULT NULL,
  `nationality_id` int(10) UNSIGNED DEFAULT NULL,
  `religion_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_attendance_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_qualification` text COLLATE utf8mb4_unicode_ci COMMENT 'Qualifications are store with comma',
  `staff_specialization` text COLLATE utf8mb4_unicode_ci COMMENT 'Specializations are store with comma',
  `staff_reference` text COLLATE utf8mb4_unicode_ci,
  `staff_temporary_address` text COLLATE utf8mb4_unicode_ci,
  `staff_temporary_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_temporary_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_temporary_county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_temporary_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_permanent_address` text COLLATE utf8mb4_unicode_ci,
  `staff_permanent_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_permanent_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_permanent_county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_permanent_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Leaved,1=Studying',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `admin_id`, `update_by`, `school_id`, `reference_admin_id`, `staff_name`, `staff_profile_img`, `designation_id`, `staff_role_id`, `staff_email`, `staff_mobile_number`, `staff_dob`, `staff_gender`, `staff_father_name_husband_name`, `staff_mother_name`, `staff_blood_group`, `staff_marital_status`, `caste_id`, `nationality_id`, `religion_id`, `staff_attendance_unique_id`, `staff_qualification`, `staff_specialization`, `staff_reference`, `staff_temporary_address`, `staff_temporary_city`, `staff_temporary_state`, `staff_temporary_county`, `staff_temporary_pincode`, `staff_permanent_address`, `staff_permanent_city`, `staff_permanent_state`, `staff_permanent_county`, `staff_permanent_pincode`, `staff_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 2, 'Ram', NULL, 1, 1, 'ppp@pp.com', '000000000', '1992-10-24', 1, 'shyam', 'Radha', 'B+', 1, 3, 4, 1, '45454575454421', '2121', '1212', '121', '1212', '212', '1212', '1212', '342001', '444', '444', '444', '444', '342001', 1, NULL, NULL),
(2, 2, 2, 1, 2, 'shyam', NULL, 1, 1, 'ppp@pp.com', '000000000', '1992-10-24', 1, 'shyam', 'Radha', 'B+', 1, 3, 4, 1, '45454575454421', '2121', '1212', '121', '1212', '212', '1212', '1212', '342001', '444', '444', '444', '444', '342001', 1, NULL, NULL),
(3, 2, 2, 1, 2, 'Sumit', NULL, 1, 1, 'ppp@pp.com', '000000000', '1992-10-24', 1, 'shyam', 'Radha', 'B+', 1, 3, 4, 1, '45454575454421', '2121', '1212', '121', '1212', '212', '1212', '1212', '342001', '444', '444', '444', '444', '342001', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff_class_allocations`
--

CREATE TABLE `staff_class_allocations` (
  `staff_class_allocation_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_class_allocations`
--

INSERT INTO `staff_class_allocations` (`staff_class_allocation_id`, `admin_id`, `update_by`, `school_id`, `class_id`, `section_id`, `staff_id`, `created_at`, `updated_at`) VALUES
(3, 2, 2, 1, 3, 7, 3, '2018-08-07 00:05:51', '2018-08-09 01:20:54'),
(6, 2, 2, 1, 3, 7, 2, '2018-08-08 04:39:06', '2018-08-16 01:05:39'),
(7, 2, 2, 1, 1, 1, 1, '2018-08-08 23:13:35', '2018-08-08 23:13:35');

-- --------------------------------------------------------

--
-- Table structure for table `staff_documents`
--

CREATE TABLE `staff_documents` (
  `staff_document_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `staff_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `document_category_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_document_details` text COLLATE utf8mb4_unicode_ci,
  `staff_document_file` text COLLATE utf8mb4_unicode_ci,
  `staff_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff_leaves`
--

CREATE TABLE `staff_leaves` (
  `staff_leave_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_leave_reason` text COLLATE utf8mb4_unicode_ci,
  `staff_leave_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_leave_from_date` date DEFAULT NULL,
  `staff_leave_to_date` date DEFAULT NULL,
  `staff_leave_attachment` text COLLATE utf8mb4_unicode_ci,
  `staff_leave_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff_roles`
--

CREATE TABLE `staff_roles` (
  `staff_role_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `staff_role_name` text COLLATE utf8mb4_unicode_ci,
  `staff_role_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_roles`
--

INSERT INTO `staff_roles` (`staff_role_id`, `admin_id`, `update_by`, `school_id`, `staff_role_name`, `staff_role_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Class Teacher', 1, '2018-08-05 21:34:20', '2018-08-05 21:51:09');

-- --------------------------------------------------------

--
-- Table structure for table `streams`
--

CREATE TABLE `streams` (
  `stream_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `stream_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stream_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `streams`
--

INSERT INTO `streams` (`stream_id`, `admin_id`, `update_by`, `school_id`, `stream_name`, `stream_status`, `created_at`, `updated_at`) VALUES
(3, 2, 2, 1, 'Commerce', 1, '2018-08-11 00:28:07', '2018-08-11 00:28:07');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `student_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `reference_admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `student_parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `student_enroll_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_roll_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_reg_date` date DEFAULT NULL,
  `student_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_password` text COLLATE utf8mb4_unicode_ci,
  `student_default_password` text COLLATE utf8mb4_unicode_ci,
  `student_image` text COLLATE utf8mb4_unicode_ci,
  `student_gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Boy,1=Girl',
  `student_dob` date DEFAULT NULL,
  `student_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caste_id` int(10) UNSIGNED DEFAULT NULL,
  `religion_id` int(10) UNSIGNED DEFAULT NULL,
  `nationality_id` int(10) UNSIGNED DEFAULT NULL,
  `student_sibling_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_sibling_class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_address` text COLLATE utf8mb4_unicode_ci,
  `student_temporary_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_temporary_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_temporary_county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_temporary_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_permanent_address` text COLLATE utf8mb4_unicode_ci,
  `student_permanent_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_permanent_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_permanent_county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_permanent_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_adhar_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Leaved,1=Studying',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`student_id`, `admin_id`, `update_by`, `school_id`, `reference_admin_id`, `student_parent_id`, `student_enroll_number`, `student_roll_no`, `student_reg_date`, `student_name`, `student_email`, `student_password`, `student_default_password`, `student_image`, `student_gender`, `student_dob`, `student_category`, `caste_id`, `religion_id`, `nationality_id`, `student_sibling_name`, `student_sibling_class_id`, `student_temporary_address`, `student_temporary_city`, `student_temporary_state`, `student_temporary_county`, `student_temporary_pincode`, `student_permanent_address`, `student_permanent_city`, `student_permanent_state`, `student_permanent_county`, `student_permanent_pincode`, `student_adhar_card_number`, `student_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 10, 1, '101', '101', '2018-07-30', 'suresh', 'pp@p.com', NULL, NULL, NULL, 0, '2018-07-30', 'OBC', 1, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-07-30 06:17:16', '2018-07-30 06:17:16'),
(2, 2, 2, 1, 12, 2, '102', '102', '2018-07-30', 'ramesh', 'prp@p.com', NULL, NULL, NULL, 0, '2018-07-30', 'OBC', 1, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-07-30 06:19:29', '2018-07-30 06:19:29'),
(3, 2, 2, 1, 14, 3, '103', '103', '2018-07-30', 'gajesh', 'prrp@p.com', NULL, NULL, 'avatar-business-man-people-person-3f86f80ad185a286-512x512730801532951474.png', 0, '2018-07-30', 'General', 1, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-07-30 06:21:14', '2018-07-30 06:21:14'),
(5, 2, 2, 1, 16, 3, '1111111111', '200020', '2018-08-09', 'Pratyush', 'test.pratyush@v2rsolution.co.in', NULL, NULL, 'avatar-business-man-people-person-3f86f80ad185a286-512x51257231533795230.png', 0, '2009-08-09', 'OBC', 1, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'aaa', 'Jodhpur', 'RAJASTHAN', 'India', '342001', NULL, 1, '2018-08-09 00:43:50', '2018-08-09 00:43:50'),
(6, 2, 2, 1, 17, 3, '1111111', '11111111111111', '2018-08-10', 'Rajat Dave', 'test.pratyush1@v2rsolution.co.in', NULL, NULL, 'avatar-business-man-people-person-3f86f80ad185a286-512x512360831533899268.png', 0, '2007-08-11', 'OBC', 1, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'aaa', 'Jodhpur', 'RAJASTHAN', 'India', '342001', NULL, 1, '2018-08-10 05:37:48', '2018-08-10 05:37:48'),
(7, 2, 2, 1, 19, 4, '5252', '52', '2018-08-13', '5252', '879897pp@p.com', NULL, NULL, 'avatar-business-man-people-person-3f86f80ad185a286-512x512710571534148740.png', 0, '2018-08-13', 'SC', 1, 2, 1, NULL, NULL, 'aaa', 'Jodhpur', 'RAJASTHAN', 'India', '342001', 'aaa', 'Jodhpur', 'RAJASTHAN', 'India', '342001', NULL, 1, '2018-08-13 02:55:40', '2018-08-13 02:55:40'),
(8, 2, 2, 1, 22, 5, '101112', '2323232', '2018-08-23', '23232', 'sdasdsadpp@p.com', NULL, NULL, NULL, 0, '2018-08-23', 'General', 1, 2, 1, NULL, NULL, 'aaa', 'Jodhpur', 'RAJASTHAN', 'India', '342001', 'aaa', 'Jodhpur', 'RAJASTHAN', 'India', '342001', NULL, 1, '2018-08-23 03:55:38', '2018-08-23 03:55:38');

-- --------------------------------------------------------

--
-- Table structure for table `student_academic_history_info`
--

CREATE TABLE `student_academic_history_info` (
  `student_academic_history_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `student_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `student_academic_info_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `session_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `class_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `section_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `percentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rank` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `activity_winner` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_academic_info`
--

CREATE TABLE `student_academic_info` (
  `student_academic_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `student_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `admission_session_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `admission_class_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `admission_section_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `current_session_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `current_class_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `current_section_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `student_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_academic_info`
--

INSERT INTO `student_academic_info` (`student_academic_info_id`, `admin_id`, `update_by`, `school_id`, `student_id`, `admission_session_id`, `admission_class_id`, `admission_section_id`, `current_session_id`, `current_class_id`, `current_section_id`, `student_unique_id`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, 1, 3, 5, 1, 3, 6, NULL, 2, '2018-07-30 06:17:16', '2018-07-30 06:17:16'),
(2, 2, 2, 1, 2, 1, 2, 3, 1, 2, 3, NULL, 1, '2018-07-30 06:19:29', '2018-07-30 06:19:29'),
(3, 2, 2, 1, 3, 1, 2, 4, 1, 2, 4, NULL, 3, '2018-07-30 06:21:14', '2018-07-30 06:21:14'),
(5, 2, 2, 1, 5, 1, 2, 4, 1, 1, 1, '2202020_5', NULL, '2018-08-09 00:43:50', '2018-08-09 00:43:50'),
(6, 2, 2, 1, 6, 1, 2, 3, 1, 3, 5, '1111111_6', NULL, '2018-08-10 05:37:48', '2018-08-10 05:37:48'),
(7, 2, 2, 1, 7, 1, 1, 1, 1, 1, 1, '5252_7', 1, '2018-08-13 02:55:40', '2018-08-13 02:55:40'),
(8, 2, 2, 1, 8, 1, 1, 1, 1, 3, 6, '101112_8', 1, '2018-08-23 03:55:38', '2018-08-23 03:55:38');

-- --------------------------------------------------------

--
-- Table structure for table `student_competition_mapping`
--

CREATE TABLE `student_competition_mapping` (
  `student_competition_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `competition_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `student_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `position_rank` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_competition_mapping`
--

INSERT INTO `student_competition_mapping` (`student_competition_map_id`, `admin_id`, `update_by`, `school_id`, `competition_id`, `student_id`, `position_rank`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 4, 1, '225', '2018-08-03 07:25:13', '2018-08-03 07:26:39'),
(2, 2, 2, 1, 4, 3, '5', '2018-08-03 07:25:23', '2018-08-03 07:25:23'),
(3, 2, 2, 1, 4, 2, '5', '2018-08-03 07:26:26', '2018-08-03 07:26:26'),
(4, 2, 2, 1, 3, 1, '5', '2018-08-08 04:59:57', '2018-08-08 04:59:57');

-- --------------------------------------------------------

--
-- Table structure for table `student_documents`
--

CREATE TABLE `student_documents` (
  `student_document_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `student_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `document_category_id` int(10) UNSIGNED DEFAULT NULL,
  `student_document_details` text COLLATE utf8mb4_unicode_ci,
  `student_document_file` text COLLATE utf8mb4_unicode_ci,
  `student_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_documents`
--

INSERT INTO `student_documents` (`student_document_id`, `admin_id`, `update_by`, `school_id`, `student_id`, `document_category_id`, `student_document_details`, `student_document_file`, `student_document_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 8, NULL, NULL, 'Screenshot_2018-08-22 Edit Question Paper Academic Management22831535016338.png', 0, '2018-08-23 03:55:38', '2018-08-23 03:55:38');

-- --------------------------------------------------------

--
-- Table structure for table `student_health_info`
--

CREATE TABLE `student_health_info` (
  `student_health_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `student_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `student_height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_blood_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_vision_left` text COLLATE utf8mb4_unicode_ci,
  `student_vision_right` text COLLATE utf8mb4_unicode_ci,
  `medical_issues` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_health_info`
--

INSERT INTO `student_health_info` (`student_health_info_id`, `admin_id`, `update_by`, `school_id`, `student_id`, `student_height`, `student_weight`, `student_blood_group`, `student_vision_left`, `student_vision_right`, `medical_issues`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 1, NULL, NULL, 'B+', NULL, NULL, 'none', '2018-07-30 06:17:16', '2018-07-30 06:17:16'),
(2, 2, 2, 1, 2, NULL, NULL, 'india', NULL, NULL, 'none', '2018-07-30 06:19:29', '2018-07-30 06:19:29'),
(3, 2, 2, 1, 3, NULL, NULL, 'in', NULL, NULL, 'none', '2018-07-30 06:21:14', '2018-07-30 06:21:14'),
(5, 2, 2, 1, 5, NULL, NULL, 'b+', NULL, NULL, 'none', '2018-08-09 00:43:50', '2018-08-09 00:43:50'),
(6, 2, 2, 1, 6, NULL, NULL, 'b+', NULL, NULL, 'none', '2018-08-10 05:37:48', '2018-08-10 05:37:48'),
(7, 2, 2, 1, 7, NULL, NULL, 'b+', NULL, NULL, 'none', '2018-08-13 02:55:40', '2018-08-13 02:55:40'),
(8, 2, 2, 1, 8, '43', '434', 'b+', NULL, NULL, 'none', '2018-08-23 03:55:38', '2018-08-23 03:55:38');

-- --------------------------------------------------------

--
-- Table structure for table `student_leaves`
--

CREATE TABLE `student_leaves` (
  `student_leave_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `student_leave_reason` text COLLATE utf8mb4_unicode_ci,
  `student_leave_from_date` date DEFAULT NULL,
  `student_leave_to_date` date DEFAULT NULL,
  `student_leave_attachment` text COLLATE utf8mb4_unicode_ci,
  `student_leave_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_leaves`
--

INSERT INTO `student_leaves` (`student_leave_id`, `admin_id`, `update_by`, `school_id`, `student_id`, `student_leave_reason`, `student_leave_from_date`, `student_leave_to_date`, `student_leave_attachment`, `student_leave_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 2, 'Sister Wedding', '2018-08-11', '2018-08-14', 'avatar-business-man-people-person-3f86f80ad185a286-512x512570381533901845.png', 0, '2018-08-10 06:20:45', '2018-08-10 06:20:45'),
(2, 2, 2, 1, 2, 'Going Goa', '2018-08-13', '2018-08-23', NULL, 0, '2018-08-13 01:29:37', '2018-08-13 01:29:37'),
(3, 2, 2, 1, 2, 'dfd', '2018-08-16', '2018-08-18', NULL, 0, '2018-08-16 00:33:46', '2018-08-16 00:33:46'),
(4, 2, 2, 1, 2, 'lkjljk', '2018-08-15', '2018-08-24', NULL, 0, '2018-08-16 00:35:27', '2018-08-16 00:35:27'),
(5, 2, 2, 1, 2, 'test', '2018-08-17', '2018-08-31', NULL, 0, '2018-08-17 01:32:27', '2018-08-17 01:32:27'),
(6, 2, 2, 1, 2, 'final test', '2018-08-01', '2018-09-27', NULL, 0, '2018-08-17 02:10:05', '2018-08-17 02:10:05'),
(7, 2, 2, 1, 2, 'vgdfgdfg', '2018-08-01', '2018-08-03', NULL, 0, '2018-08-17 04:15:39', '2018-08-17 04:15:39'),
(15, 16, 16, 1, 5, 'final test final', '2018-08-14', '2018-08-17', NULL, 0, '2018-08-17 04:57:27', '2018-08-17 04:57:27');

-- --------------------------------------------------------

--
-- Table structure for table `student_parents`
--

CREATE TABLE `student_parents` (
  `student_parent_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `reference_admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `student_father_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_annual_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_tongue` text COLLATE utf8mb4_unicode_ci,
  `student_guardian_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_annual_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_parent_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_parents`
--

INSERT INTO `student_parents` (`student_parent_id`, `admin_id`, `update_by`, `school_id`, `reference_admin_id`, `student_father_name`, `student_father_mobile_number`, `student_father_email`, `student_father_occupation`, `student_father_annual_salary`, `student_mother_name`, `student_mother_mobile_number`, `student_mother_email`, `student_mother_occupation`, `student_mother_tongue`, `student_guardian_name`, `student_guardian_email`, `student_guardian_mobile_number`, `student_mother_annual_salary`, `student_parent_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 9, 'a', 'a', 'p@p.com', 'test', NULL, 'b', 'b', 'p@p.com', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-07-30 06:17:16', '2018-07-30 06:17:16'),
(2, 2, 2, 1, 11, 'a', 'a', 'p@p.com', 'dd', NULL, 'dd', 'd', 'd', NULL, NULL, NULL, NULL, NULL, 'Below 1 Lac', 1, '2018-07-30 06:19:28', '2018-07-30 06:19:28'),
(3, 2, 2, 1, 13, 'q', 'q', 'q@gmail.com', 'y', 'Below 1 Lac', 'd', '7878787878', 'd@gmail.com', NULL, NULL, NULL, NULL, NULL, 'Below 1 Lac', 1, '2018-07-30 06:21:14', '2018-08-10 05:37:48'),
(4, 2, 2, 1, 18, 'q', '9982939801', 'q@gmail.com', '1', 'Below 1 Lac', 'd', '1', 'd@gmail.com', '1', '1', NULL, NULL, NULL, 'Below 1 Lac', 1, '2018-08-13 02:55:40', '2018-08-13 02:55:40'),
(5, 2, 2, 1, 21, 'e', 'e', 'e', 'e', 'Below 1 Lac', 'e', 'e', 'e', 'e', NULL, NULL, NULL, NULL, NULL, 1, '2018-08-23 03:55:38', '2018-08-23 03:55:38');

-- --------------------------------------------------------

--
-- Table structure for table `student_virtual_class_mapping`
--

CREATE TABLE `student_virtual_class_mapping` (
  `student_virtual_class_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `virtual_class_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `student_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_virtual_class_mapping`
--

INSERT INTO `student_virtual_class_mapping` (`student_virtual_class_map_id`, `admin_id`, `update_by`, `school_id`, `virtual_class_id`, `student_id`, `created_at`, `updated_at`) VALUES
(6, 2, 2, 1, 2, 3, '2018-08-01 23:28:53', '2018-08-01 23:28:53'),
(7, 2, 2, 1, 2, 1, '2018-08-03 04:34:01', '2018-08-03 04:34:01'),
(8, 2, 2, 1, 1, 2, '2018-08-03 04:34:13', '2018-08-03 04:34:13'),
(10, 2, 2, 1, 3, 2, '2018-08-03 05:01:04', '2018-08-03 05:01:04'),
(11, 2, 2, 1, 3, 1, '2018-08-06 02:08:59', '2018-08-06 02:08:59'),
(12, 2, 2, 1, 3, 3, '2018-08-06 02:09:01', '2018-08-06 02:09:01');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subject_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_is_coscholastic` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `class_ids` text COLLATE utf8mb4_unicode_ci,
  `co_scholastic_type_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subject_id`, `admin_id`, `update_by`, `school_id`, `subject_name`, `subject_code`, `subject_is_coscholastic`, `class_ids`, `co_scholastic_type_id`, `subject_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Hindi', 'h01', 0, '', NULL, 1, '2018-08-07 02:52:37', '2018-08-07 02:52:37'),
(2, 2, 2, 1, 'English', 'e01', 0, '', NULL, 1, '2018-08-07 02:53:05', '2018-08-07 02:53:05'),
(3, 2, 2, 1, 'Maths', 'm01', 0, '', NULL, 1, '2018-08-07 02:53:17', '2018-08-07 02:53:17'),
(4, 2, 2, 1, 'Science', 's01', 0, '', NULL, 1, '2018-08-07 02:53:50', '2018-08-07 02:53:50'),
(5, 2, 2, 1, 'Social Science', 'ss01', 0, '', NULL, 1, '2018-08-07 02:54:17', '2018-08-07 02:54:17');

-- --------------------------------------------------------

--
-- Table structure for table `subject_class_mapping`
--

CREATE TABLE `subject_class_mapping` (
  `subject_class_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subject_class_mapping`
--

INSERT INTO `subject_class_mapping` (`subject_class_map_id`, `admin_id`, `update_by`, `school_id`, `class_id`, `subject_id`, `created_at`, `updated_at`) VALUES
(18, 2, 2, 1, 2, 1, '2018-08-07 08:13:34', '2018-08-07 08:13:34'),
(19, 2, 2, 1, 2, 2, '2018-08-07 08:13:34', '2018-08-07 08:13:34'),
(20, 2, 2, 1, 2, 3, '2018-08-07 08:13:34', '2018-08-07 08:13:34'),
(21, 2, 2, 1, 2, 4, '2018-08-07 08:13:34', '2018-08-07 08:13:34'),
(22, 2, 2, 1, 2, 5, '2018-08-07 08:13:34', '2018-08-07 08:13:34'),
(31, 2, 2, 1, 3, 3, '2018-08-08 04:19:27', '2018-08-08 04:19:27'),
(32, 2, 2, 1, 3, 4, '2018-08-08 04:19:27', '2018-08-08 04:19:27'),
(88, 2, 2, 1, 1, 1, '2018-08-16 01:21:51', '2018-08-16 01:21:51'),
(89, 2, 2, 1, 1, 2, '2018-08-16 01:21:51', '2018-08-16 01:21:51'),
(90, 2, 2, 1, 1, 3, '2018-08-16 01:21:51', '2018-08-16 01:21:51'),
(91, 2, 2, 1, 1, 4, '2018-08-16 01:21:51', '2018-08-16 01:21:51'),
(92, 2, 2, 1, 1, 5, '2018-08-16 01:21:51', '2018-08-16 01:21:51');

-- --------------------------------------------------------

--
-- Table structure for table `term_exams`
--

CREATE TABLE `term_exams` (
  `term_exam_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `term_exam_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `term_exam_caption` text COLLATE utf8mb4_unicode_ci,
  `term_exam_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `term_exams`
--

INSERT INTO `term_exams` (`term_exam_id`, `admin_id`, `update_by`, `school_id`, `term_exam_name`, `term_exam_caption`, `term_exam_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Monthly', 'Monthly Terms', 1, '2018-08-11 01:38:09', '2018-08-13 01:07:43'),
(2, 2, 2, 1, 'name-123 final edit', 'Edit X Edit', 1, '2018-08-15 22:55:51', '2018-08-16 03:35:25'),
(3, 2, 2, 1, 'my term - 1 edited 2', 'Final Test edited 2', 1, '2018-08-16 01:28:38', '2018-08-16 02:55:38');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `name`) VALUES
(1, 'test'),
(2, '1'),
(3, '4');

--
-- Triggers `test`
--
DELIMITER $$
CREATE TRIGGER `DownCounter` AFTER DELETE ON `test` FOR EACH ROW UPDATE schools set school_total_students = (Select count(*) from test)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `updateCounter` AFTER INSERT ON `test` FOR EACH ROW UPDATE schools set school_total_students = (Select count(*) from test)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `title_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `titles`
--

INSERT INTO `titles` (`title_id`, `admin_id`, `update_by`, `school_id`, `title_name`, `title_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Mr.', 1, '2018-07-23 17:01:04', '2018-07-23 17:01:04'),
(2, 2, 2, 1, 'Mrs.', 1, '2018-07-23 17:01:14', '2018-07-23 17:01:14'),
(3, 2, 2, 1, 'Ms.', 1, '2018-07-23 17:01:55', '2018-07-23 17:01:55');

-- --------------------------------------------------------

--
-- Table structure for table `virtual_classes`
--

CREATE TABLE `virtual_classes` (
  `virtual_class_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `virtual_class_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `virtual_class_description` text COLLATE utf8mb4_unicode_ci,
  `virtual_class_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `virtual_classes`
--

INSERT INTO `virtual_classes` (`virtual_class_id`, `admin_id`, `update_by`, `school_id`, `virtual_class_name`, `virtual_class_description`, `virtual_class_status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, 'Activity Class', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 1, '2018-07-24 14:26:38', '2018-07-24 14:26:38'),
(2, 2, 2, 1, 'Music class', 'testw', 1, '2018-07-31 02:04:49', '2018-07-31 02:04:49'),
(3, 2, 2, 1, 'My Final class', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 1, '2018-08-03 05:00:05', '2018-08-03 05:00:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`),
  ADD KEY `books_admin_id_foreign` (`admin_id`),
  ADD KEY `books_update_by_foreign` (`update_by`),
  ADD KEY `books_school_id_foreign` (`school_id`),
  ADD KEY `books_book_category_id_foreign` (`book_category_id`),
  ADD KEY `books_book_vendor_id_foreign` (`book_vendor_id`),
  ADD KEY `books_book_cupboard_id_foreign` (`book_cupboard_id`),
  ADD KEY `books_book_cupboardshelf_id_foreign` (`book_cupboardshelf_id`);

--
-- Indexes for table `book_allowance`
--
ALTER TABLE `book_allowance`
  ADD PRIMARY KEY (`book_allowance_id`),
  ADD KEY `book_allowance_admin_id_foreign` (`admin_id`),
  ADD KEY `book_allowance_update_by_foreign` (`update_by`),
  ADD KEY `book_allowance_school_id_foreign` (`school_id`);

--
-- Indexes for table `book_categories`
--
ALTER TABLE `book_categories`
  ADD PRIMARY KEY (`book_category_id`),
  ADD KEY `book_categories_admin_id_foreign` (`admin_id`),
  ADD KEY `book_categories_update_by_foreign` (`update_by`),
  ADD KEY `book_categories_school_id_foreign` (`school_id`);

--
-- Indexes for table `book_copies_info`
--
ALTER TABLE `book_copies_info`
  ADD PRIMARY KEY (`book_info_id`),
  ADD KEY `book_copies_info_admin_id_foreign` (`admin_id`),
  ADD KEY `book_copies_info_update_by_foreign` (`update_by`),
  ADD KEY `book_copies_info_school_id_foreign` (`school_id`),
  ADD KEY `book_copies_info_book_id_foreign` (`book_id`);

--
-- Indexes for table `book_cupboards`
--
ALTER TABLE `book_cupboards`
  ADD PRIMARY KEY (`book_cupboard_id`),
  ADD KEY `book_cupboards_admin_id_foreign` (`admin_id`),
  ADD KEY `book_cupboards_update_by_foreign` (`update_by`),
  ADD KEY `book_cupboards_school_id_foreign` (`school_id`);

--
-- Indexes for table `book_cupboardshelfs`
--
ALTER TABLE `book_cupboardshelfs`
  ADD PRIMARY KEY (`book_cupboardshelf_id`),
  ADD KEY `book_cupboardshelfs_admin_id_foreign` (`admin_id`),
  ADD KEY `book_cupboardshelfs_update_by_foreign` (`update_by`),
  ADD KEY `book_cupboardshelfs_school_id_foreign` (`school_id`);

--
-- Indexes for table `book_vendors`
--
ALTER TABLE `book_vendors`
  ADD PRIMARY KEY (`book_vendor_id`),
  ADD KEY `book_vendors_admin_id_foreign` (`admin_id`),
  ADD KEY `book_vendors_update_by_foreign` (`update_by`),
  ADD KEY `book_vendors_school_id_foreign` (`school_id`);

--
-- Indexes for table `caste`
--
ALTER TABLE `caste`
  ADD PRIMARY KEY (`caste_id`),
  ADD KEY `caste_admin_id_foreign` (`admin_id`),
  ADD KEY `caste_update_by_foreign` (`update_by`),
  ADD KEY `caste_school_id_foreign` (`school_id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`class_id`),
  ADD KEY `classes_admin_id_foreign` (`admin_id`),
  ADD KEY `classes_update_by_foreign` (`update_by`),
  ADD KEY `classes_school_id_foreign` (`school_id`);

--
-- Indexes for table `class_subject_staff_mapping`
--
ALTER TABLE `class_subject_staff_mapping`
  ADD PRIMARY KEY (`class_subject_staff_map_id`),
  ADD KEY `class_subject_staff_mapping_admin_id_foreign` (`admin_id`),
  ADD KEY `class_subject_staff_mapping_update_by_foreign` (`update_by`),
  ADD KEY `class_subject_staff_mapping_school_id_foreign` (`school_id`),
  ADD KEY `class_subject_staff_mapping_class_id_foreign` (`class_id`),
  ADD KEY `class_subject_staff_mapping_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `competitions`
--
ALTER TABLE `competitions`
  ADD PRIMARY KEY (`competition_id`),
  ADD KEY `competitions_admin_id_foreign` (`admin_id`),
  ADD KEY `competitions_update_by_foreign` (`update_by`),
  ADD KEY `competitions_school_id_foreign` (`school_id`);

--
-- Indexes for table `co_scholastic_types`
--
ALTER TABLE `co_scholastic_types`
  ADD PRIMARY KEY (`co_scholastic_type_id`),
  ADD KEY `co_scholastic_types_admin_id_foreign` (`admin_id`),
  ADD KEY `co_scholastic_types_update_by_foreign` (`update_by`),
  ADD KEY `co_scholastic_types_school_id_foreign` (`school_id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`designation_id`),
  ADD KEY `designations_admin_id_foreign` (`admin_id`),
  ADD KEY `designations_update_by_foreign` (`update_by`),
  ADD KEY `designations_school_id_foreign` (`school_id`);

--
-- Indexes for table `document_category`
--
ALTER TABLE `document_category`
  ADD PRIMARY KEY (`document_category_id`),
  ADD KEY `document_category_admin_id_foreign` (`admin_id`),
  ADD KEY `document_category_update_by_foreign` (`update_by`),
  ADD KEY `document_category_school_id_foreign` (`school_id`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`exam_id`),
  ADD KEY `exams_admin_id_foreign` (`admin_id`),
  ADD KEY `exams_update_by_foreign` (`update_by`),
  ADD KEY `exams_school_id_foreign` (`school_id`),
  ADD KEY `exams_term_exam_id_foreign` (`term_exam_id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`facility_id`),
  ADD KEY `facilities_admin_id_foreign` (`admin_id`),
  ADD KEY `facilities_update_by_foreign` (`update_by`),
  ADD KEY `facilities_school_id_foreign` (`school_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `groups_admin_id_foreign` (`admin_id`),
  ADD KEY `groups_update_by_foreign` (`update_by`),
  ADD KEY `groups_school_id_foreign` (`school_id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`holiday_id`),
  ADD KEY `holidays_admin_id_foreign` (`admin_id`),
  ADD KEY `holidays_update_by_foreign` (`update_by`),
  ADD KEY `holidays_school_id_foreign` (`school_id`),
  ADD KEY `holidays_session_id_foreign` (`session_id`);

--
-- Indexes for table `issued_books`
--
ALTER TABLE `issued_books`
  ADD PRIMARY KEY (`issued_book_id`),
  ADD KEY `issued_books_admin_id_foreign` (`admin_id`),
  ADD KEY `issued_books_update_by_foreign` (`update_by`),
  ADD KEY `issued_books_school_id_foreign` (`school_id`),
  ADD KEY `issued_books_book_id_foreign` (`book_id`);

--
-- Indexes for table `issued_books_history`
--
ALTER TABLE `issued_books_history`
  ADD PRIMARY KEY (`history_id`),
  ADD KEY `issued_books_history_admin_id_foreign` (`admin_id`),
  ADD KEY `issued_books_history_update_by_foreign` (`update_by`),
  ADD KEY `issued_books_history_school_id_foreign` (`school_id`);

--
-- Indexes for table `library_fine`
--
ALTER TABLE `library_fine`
  ADD PRIMARY KEY (`library_fine_id`),
  ADD KEY `library_fine_admin_id_foreign` (`admin_id`),
  ADD KEY `library_fine_update_by_foreign` (`update_by`),
  ADD KEY `library_fine_school_id_foreign` (`school_id`),
  ADD KEY `library_fine_member_id_foreign` (`member_id`);

--
-- Indexes for table `library_members`
--
ALTER TABLE `library_members`
  ADD PRIMARY KEY (`library_member_id`),
  ADD KEY `library_members_admin_id_foreign` (`admin_id`),
  ADD KEY `library_members_update_by_foreign` (`update_by`),
  ADD KEY `library_members_school_id_foreign` (`school_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nationality`
--
ALTER TABLE `nationality`
  ADD PRIMARY KEY (`nationality_id`),
  ADD KEY `nationality_admin_id_foreign` (`admin_id`),
  ADD KEY `nationality_update_by_foreign` (`update_by`),
  ADD KEY `nationality_school_id_foreign` (`school_id`);

--
-- Indexes for table `online_notes`
--
ALTER TABLE `online_notes`
  ADD PRIMARY KEY (`online_note_id`),
  ADD KEY `online_notes_admin_id_foreign` (`admin_id`),
  ADD KEY `online_notes_update_by_foreign` (`update_by`),
  ADD KEY `online_notes_school_id_foreign` (`school_id`),
  ADD KEY `online_notes_class_id_foreign` (`class_id`),
  ADD KEY `online_notes_section_id_foreign` (`section_id`),
  ADD KEY `online_notes_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `question_papers`
--
ALTER TABLE `question_papers`
  ADD PRIMARY KEY (`question_paper_id`),
  ADD KEY `question_papers_admin_id_foreign` (`admin_id`),
  ADD KEY `question_papers_update_by_foreign` (`update_by`),
  ADD KEY `question_papers_school_id_foreign` (`school_id`),
  ADD KEY `question_papers_class_id_foreign` (`class_id`),
  ADD KEY `question_papers_section_id_foreign` (`section_id`),
  ADD KEY `question_papers_subject_id_foreign` (`subject_id`),
  ADD KEY `question_papers_exam_id_foreign` (`exam_id`);

--
-- Indexes for table `religions`
--
ALTER TABLE `religions`
  ADD PRIMARY KEY (`religion_id`),
  ADD KEY `religions_admin_id_foreign` (`admin_id`),
  ADD KEY `religions_update_by_foreign` (`update_by`),
  ADD KEY `religions_school_id_foreign` (`school_id`);

--
-- Indexes for table `room_no`
--
ALTER TABLE `room_no`
  ADD PRIMARY KEY (`room_no_id`),
  ADD KEY `room_no_admin_id_foreign` (`admin_id`),
  ADD KEY `room_no_update_by_foreign` (`update_by`),
  ADD KEY `room_no_school_id_foreign` (`school_id`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`school_id`),
  ADD KEY `schools_admin_id_foreign` (`admin_id`),
  ADD KEY `schools_update_by_foreign` (`update_by`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`section_id`),
  ADD KEY `sections_admin_id_foreign` (`admin_id`),
  ADD KEY `sections_update_by_foreign` (`update_by`),
  ADD KEY `sections_school_id_foreign` (`school_id`),
  ADD KEY `sections_class_id_foreign` (`class_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `sessions_admin_id_foreign` (`admin_id`),
  ADD KEY `sessions_update_by_foreign` (`update_by`),
  ADD KEY `sessions_school_id_foreign` (`school_id`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`shift_id`),
  ADD KEY `shifts_admin_id_foreign` (`admin_id`),
  ADD KEY `shifts_update_by_foreign` (`update_by`),
  ADD KEY `shifts_school_id_foreign` (`school_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`),
  ADD KEY `staff_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_update_by_foreign` (`update_by`),
  ADD KEY `staff_school_id_foreign` (`school_id`),
  ADD KEY `staff_reference_admin_id_foreign` (`reference_admin_id`),
  ADD KEY `staff_designation_id_foreign` (`designation_id`),
  ADD KEY `staff_staff_role_id_foreign` (`staff_role_id`),
  ADD KEY `staff_caste_id_foreign` (`caste_id`),
  ADD KEY `staff_religion_id_foreign` (`religion_id`),
  ADD KEY `staff_nationality_id_foreign` (`nationality_id`);

--
-- Indexes for table `staff_class_allocations`
--
ALTER TABLE `staff_class_allocations`
  ADD PRIMARY KEY (`staff_class_allocation_id`),
  ADD KEY `staff_class_allocations_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_class_allocations_update_by_foreign` (`update_by`),
  ADD KEY `staff_class_allocations_school_id_foreign` (`school_id`),
  ADD KEY `staff_class_allocations_class_id_foreign` (`class_id`),
  ADD KEY `staff_class_allocations_section_id_foreign` (`section_id`),
  ADD KEY `staff_class_allocations_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `staff_documents`
--
ALTER TABLE `staff_documents`
  ADD PRIMARY KEY (`staff_document_id`),
  ADD KEY `staff_documents_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_documents_update_by_foreign` (`update_by`),
  ADD KEY `staff_documents_school_id_foreign` (`school_id`),
  ADD KEY `staff_documents_staff_id_foreign` (`staff_id`),
  ADD KEY `staff_documents_document_category_id_foreign` (`document_category_id`);

--
-- Indexes for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  ADD PRIMARY KEY (`staff_leave_id`),
  ADD KEY `staff_leaves_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_leaves_update_by_foreign` (`update_by`),
  ADD KEY `staff_leaves_school_id_foreign` (`school_id`),
  ADD KEY `staff_leaves_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `staff_roles`
--
ALTER TABLE `staff_roles`
  ADD PRIMARY KEY (`staff_role_id`),
  ADD KEY `staff_roles_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_roles_update_by_foreign` (`update_by`),
  ADD KEY `staff_roles_school_id_foreign` (`school_id`);

--
-- Indexes for table `streams`
--
ALTER TABLE `streams`
  ADD PRIMARY KEY (`stream_id`),
  ADD KEY `streams_admin_id_foreign` (`admin_id`),
  ADD KEY `streams_update_by_foreign` (`update_by`),
  ADD KEY `streams_school_id_foreign` (`school_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`student_id`),
  ADD KEY `students_admin_id_foreign` (`admin_id`),
  ADD KEY `students_update_by_foreign` (`update_by`),
  ADD KEY `students_school_id_foreign` (`school_id`),
  ADD KEY `students_reference_admin_id_foreign` (`reference_admin_id`),
  ADD KEY `students_student_parent_id_foreign` (`student_parent_id`),
  ADD KEY `students_student_sibling_class_id_foreign` (`student_sibling_class_id`),
  ADD KEY `students_caste_id_foreign` (`caste_id`),
  ADD KEY `students_religion_id_foreign` (`religion_id`),
  ADD KEY `students_nationality_id_foreign` (`nationality_id`);

--
-- Indexes for table `student_academic_history_info`
--
ALTER TABLE `student_academic_history_info`
  ADD PRIMARY KEY (`student_academic_history_info_id`),
  ADD KEY `student_academic_history_info_admin_id_foreign` (`admin_id`),
  ADD KEY `student_academic_history_info_update_by_foreign` (`update_by`),
  ADD KEY `student_academic_history_info_school_id_foreign` (`school_id`),
  ADD KEY `student_academic_history_info_student_id_foreign` (`student_id`),
  ADD KEY `student_academic_history_info_student_academic_info_id_foreign` (`student_academic_info_id`),
  ADD KEY `student_academic_history_info_session_id_foreign` (`session_id`),
  ADD KEY `student_academic_history_info_class_id_foreign` (`class_id`),
  ADD KEY `student_academic_history_info_section_id_foreign` (`section_id`);

--
-- Indexes for table `student_academic_info`
--
ALTER TABLE `student_academic_info`
  ADD PRIMARY KEY (`student_academic_info_id`),
  ADD KEY `student_academic_info_admin_id_foreign` (`admin_id`),
  ADD KEY `student_academic_info_update_by_foreign` (`update_by`),
  ADD KEY `student_academic_info_school_id_foreign` (`school_id`),
  ADD KEY `student_academic_info_student_id_foreign` (`student_id`),
  ADD KEY `student_academic_info_admission_class_id_foreign` (`admission_class_id`),
  ADD KEY `student_academic_info_admission_section_id_foreign` (`admission_section_id`),
  ADD KEY `student_academic_info_admission_session_id_foreign` (`admission_session_id`),
  ADD KEY `student_academic_info_current_session_id_foreign` (`current_session_id`),
  ADD KEY `student_academic_info_current_class_id_foreign` (`current_class_id`),
  ADD KEY `student_academic_info_current_section_id_foreign` (`current_section_id`),
  ADD KEY `student_academic_info_group_id_foreign` (`group_id`);

--
-- Indexes for table `student_competition_mapping`
--
ALTER TABLE `student_competition_mapping`
  ADD PRIMARY KEY (`student_competition_map_id`),
  ADD KEY `student_competition_mapping_admin_id_foreign` (`admin_id`),
  ADD KEY `student_competition_mapping_update_by_foreign` (`update_by`),
  ADD KEY `student_competition_mapping_school_id_foreign` (`school_id`),
  ADD KEY `student_competition_mapping_competition_id_foreign` (`competition_id`),
  ADD KEY `student_competition_mapping_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_documents`
--
ALTER TABLE `student_documents`
  ADD PRIMARY KEY (`student_document_id`),
  ADD KEY `student_documents_admin_id_foreign` (`admin_id`),
  ADD KEY `student_documents_update_by_foreign` (`update_by`),
  ADD KEY `student_documents_school_id_foreign` (`school_id`),
  ADD KEY `student_documents_student_id_foreign` (`student_id`),
  ADD KEY `student_documents_document_category_id_foreign` (`document_category_id`);

--
-- Indexes for table `student_health_info`
--
ALTER TABLE `student_health_info`
  ADD PRIMARY KEY (`student_health_info_id`),
  ADD KEY `student_health_info_admin_id_foreign` (`admin_id`),
  ADD KEY `student_health_info_update_by_foreign` (`update_by`),
  ADD KEY `student_health_info_school_id_foreign` (`school_id`),
  ADD KEY `student_health_info_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_leaves`
--
ALTER TABLE `student_leaves`
  ADD PRIMARY KEY (`student_leave_id`),
  ADD KEY `student_leaves_admin_id_foreign` (`admin_id`),
  ADD KEY `student_leaves_update_by_foreign` (`update_by`),
  ADD KEY `student_leaves_school_id_foreign` (`school_id`),
  ADD KEY `student_leaves_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_parents`
--
ALTER TABLE `student_parents`
  ADD PRIMARY KEY (`student_parent_id`),
  ADD KEY `student_parents_admin_id_foreign` (`admin_id`),
  ADD KEY `student_parents_update_by_foreign` (`update_by`),
  ADD KEY `student_parents_school_id_foreign` (`school_id`),
  ADD KEY `student_parents_reference_admin_id_foreign` (`reference_admin_id`);

--
-- Indexes for table `student_virtual_class_mapping`
--
ALTER TABLE `student_virtual_class_mapping`
  ADD PRIMARY KEY (`student_virtual_class_map_id`),
  ADD KEY `student_virtual_class_mapping_admin_id_foreign` (`admin_id`),
  ADD KEY `student_virtual_class_mapping_update_by_foreign` (`update_by`),
  ADD KEY `student_virtual_class_mapping_school_id_foreign` (`school_id`),
  ADD KEY `student_virtual_class_mapping_virtual_class_id_foreign` (`virtual_class_id`),
  ADD KEY `student_virtual_class_mapping_student_id_foreign` (`student_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subject_id`),
  ADD KEY `subjects_admin_id_foreign` (`admin_id`),
  ADD KEY `subjects_update_by_foreign` (`update_by`),
  ADD KEY `subjects_school_id_foreign` (`school_id`),
  ADD KEY `subjects_co_scholastic_type_id_foreign` (`co_scholastic_type_id`);

--
-- Indexes for table `subject_class_mapping`
--
ALTER TABLE `subject_class_mapping`
  ADD PRIMARY KEY (`subject_class_map_id`),
  ADD KEY `subject_class_mapping_admin_id_foreign` (`admin_id`),
  ADD KEY `subject_class_mapping_update_by_foreign` (`update_by`),
  ADD KEY `subject_class_mapping_school_id_foreign` (`school_id`),
  ADD KEY `subject_class_mapping_class_id_foreign` (`class_id`),
  ADD KEY `subject_class_mapping_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `term_exams`
--
ALTER TABLE `term_exams`
  ADD PRIMARY KEY (`term_exam_id`),
  ADD KEY `term_exams_admin_id_foreign` (`admin_id`),
  ADD KEY `term_exams_update_by_foreign` (`update_by`),
  ADD KEY `term_exams_school_id_foreign` (`school_id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`title_id`),
  ADD KEY `titles_admin_id_foreign` (`admin_id`),
  ADD KEY `titles_update_by_foreign` (`update_by`),
  ADD KEY `titles_school_id_foreign` (`school_id`);

--
-- Indexes for table `virtual_classes`
--
ALTER TABLE `virtual_classes`
  ADD PRIMARY KEY (`virtual_class_id`),
  ADD KEY `virtual_classes_admin_id_foreign` (`admin_id`),
  ADD KEY `virtual_classes_update_by_foreign` (`update_by`),
  ADD KEY `virtual_classes_school_id_foreign` (`school_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `book_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `book_allowance`
--
ALTER TABLE `book_allowance`
  MODIFY `book_allowance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `book_categories`
--
ALTER TABLE `book_categories`
  MODIFY `book_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `book_copies_info`
--
ALTER TABLE `book_copies_info`
  MODIFY `book_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `book_cupboards`
--
ALTER TABLE `book_cupboards`
  MODIFY `book_cupboard_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `book_cupboardshelfs`
--
ALTER TABLE `book_cupboardshelfs`
  MODIFY `book_cupboardshelf_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `book_vendors`
--
ALTER TABLE `book_vendors`
  MODIFY `book_vendor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `caste`
--
ALTER TABLE `caste`
  MODIFY `caste_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `class_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `class_subject_staff_mapping`
--
ALTER TABLE `class_subject_staff_mapping`
  MODIFY `class_subject_staff_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `competitions`
--
ALTER TABLE `competitions`
  MODIFY `competition_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `co_scholastic_types`
--
ALTER TABLE `co_scholastic_types`
  MODIFY `co_scholastic_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `designation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `document_category`
--
ALTER TABLE `document_category`
  MODIFY `document_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `exam_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `facility_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `holiday_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `issued_books`
--
ALTER TABLE `issued_books`
  MODIFY `issued_book_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `issued_books_history`
--
ALTER TABLE `issued_books_history`
  MODIFY `history_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `library_fine`
--
ALTER TABLE `library_fine`
  MODIFY `library_fine_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `library_members`
--
ALTER TABLE `library_members`
  MODIFY `library_member_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `nationality`
--
ALTER TABLE `nationality`
  MODIFY `nationality_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `online_notes`
--
ALTER TABLE `online_notes`
  MODIFY `online_note_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `question_papers`
--
ALTER TABLE `question_papers`
  MODIFY `question_paper_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `religions`
--
ALTER TABLE `religions`
  MODIFY `religion_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `room_no`
--
ALTER TABLE `room_no`
  MODIFY `room_no_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `school_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `section_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `session_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `shift_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `staff_class_allocations`
--
ALTER TABLE `staff_class_allocations`
  MODIFY `staff_class_allocation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `staff_documents`
--
ALTER TABLE `staff_documents`
  MODIFY `staff_document_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  MODIFY `staff_leave_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff_roles`
--
ALTER TABLE `staff_roles`
  MODIFY `staff_role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `streams`
--
ALTER TABLE `streams`
  MODIFY `stream_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `student_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `student_academic_history_info`
--
ALTER TABLE `student_academic_history_info`
  MODIFY `student_academic_history_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_academic_info`
--
ALTER TABLE `student_academic_info`
  MODIFY `student_academic_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `student_competition_mapping`
--
ALTER TABLE `student_competition_mapping`
  MODIFY `student_competition_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `student_documents`
--
ALTER TABLE `student_documents`
  MODIFY `student_document_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `student_health_info`
--
ALTER TABLE `student_health_info`
  MODIFY `student_health_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `student_leaves`
--
ALTER TABLE `student_leaves`
  MODIFY `student_leave_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `student_parents`
--
ALTER TABLE `student_parents`
  MODIFY `student_parent_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `student_virtual_class_mapping`
--
ALTER TABLE `student_virtual_class_mapping`
  MODIFY `student_virtual_class_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `subject_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `subject_class_mapping`
--
ALTER TABLE `subject_class_mapping`
  MODIFY `subject_class_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `term_exams`
--
ALTER TABLE `term_exams`
  MODIFY `term_exam_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `title_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `virtual_classes`
--
ALTER TABLE `virtual_classes`
  MODIFY `virtual_class_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `books_book_category_id_foreign` FOREIGN KEY (`book_category_id`) REFERENCES `book_categories` (`book_category_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `books_book_cupboard_id_foreign` FOREIGN KEY (`book_cupboard_id`) REFERENCES `book_cupboards` (`book_cupboard_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `books_book_cupboardshelf_id_foreign` FOREIGN KEY (`book_cupboardshelf_id`) REFERENCES `book_cupboardshelfs` (`book_cupboardshelf_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `books_book_vendor_id_foreign` FOREIGN KEY (`book_vendor_id`) REFERENCES `book_vendors` (`book_vendor_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `books_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `books_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `book_allowance`
--
ALTER TABLE `book_allowance`
  ADD CONSTRAINT `book_allowance_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_allowance_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_allowance_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `book_categories`
--
ALTER TABLE `book_categories`
  ADD CONSTRAINT `book_categories_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_categories_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_categories_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `book_copies_info`
--
ALTER TABLE `book_copies_info`
  ADD CONSTRAINT `book_copies_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_copies_info_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_copies_info_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_copies_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `book_cupboards`
--
ALTER TABLE `book_cupboards`
  ADD CONSTRAINT `book_cupboards_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_cupboards_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_cupboards_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `book_cupboardshelfs`
--
ALTER TABLE `book_cupboardshelfs`
  ADD CONSTRAINT `book_cupboardshelfs_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_cupboardshelfs_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_cupboardshelfs_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `book_vendors`
--
ALTER TABLE `book_vendors`
  ADD CONSTRAINT `book_vendors_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_vendors_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_vendors_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `caste`
--
ALTER TABLE `caste`
  ADD CONSTRAINT `caste_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `caste_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `caste_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `classes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `classes_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `classes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `class_subject_staff_mapping`
--
ALTER TABLE `class_subject_staff_mapping`
  ADD CONSTRAINT `class_subject_staff_mapping_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `class_subject_staff_mapping_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `class_subject_staff_mapping_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `class_subject_staff_mapping_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `class_subject_staff_mapping_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `competitions`
--
ALTER TABLE `competitions`
  ADD CONSTRAINT `competitions_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `competitions_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `competitions_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `co_scholastic_types`
--
ALTER TABLE `co_scholastic_types`
  ADD CONSTRAINT `co_scholastic_types_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `co_scholastic_types_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `co_scholastic_types_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `designations`
--
ALTER TABLE `designations`
  ADD CONSTRAINT `designations_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designations_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designations_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `document_category`
--
ALTER TABLE `document_category`
  ADD CONSTRAINT `document_category_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `document_category_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `document_category_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `exams`
--
ALTER TABLE `exams`
  ADD CONSTRAINT `exams_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `exams_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `exams_term_exam_id_foreign` FOREIGN KEY (`term_exam_id`) REFERENCES `term_exams` (`term_exam_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `exams_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `facilities`
--
ALTER TABLE `facilities`
  ADD CONSTRAINT `facilities_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `facilities_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `facilities_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `groups_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `groups_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `holidays`
--
ALTER TABLE `holidays`
  ADD CONSTRAINT `holidays_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `holidays_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `holidays_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `holidays_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `issued_books`
--
ALTER TABLE `issued_books`
  ADD CONSTRAINT `issued_books_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `issued_books_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `issued_books_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `issued_books_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `issued_books_history`
--
ALTER TABLE `issued_books_history`
  ADD CONSTRAINT `issued_books_history_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `issued_books_history_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `issued_books_history_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `library_fine`
--
ALTER TABLE `library_fine`
  ADD CONSTRAINT `library_fine_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `library_fine_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `library_members` (`library_member_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `library_fine_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `library_fine_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `library_members`
--
ALTER TABLE `library_members`
  ADD CONSTRAINT `library_members_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `library_members_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `library_members_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `nationality`
--
ALTER TABLE `nationality`
  ADD CONSTRAINT `nationality_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `nationality_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `nationality_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `online_notes`
--
ALTER TABLE `online_notes`
  ADD CONSTRAINT `online_notes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `online_notes_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `online_notes_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `online_notes_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `online_notes_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `online_notes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `question_papers`
--
ALTER TABLE `question_papers`
  ADD CONSTRAINT `question_papers_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `question_papers_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `question_papers_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`exam_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `question_papers_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `question_papers_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `question_papers_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `question_papers_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `religions`
--
ALTER TABLE `religions`
  ADD CONSTRAINT `religions_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `religions_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `religions_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `room_no`
--
ALTER TABLE `room_no`
  ADD CONSTRAINT `room_no_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `room_no_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `room_no_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `schools`
--
ALTER TABLE `schools`
  ADD CONSTRAINT `schools_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `schools_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sections_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sections_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sections_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sessions_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sessions_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `shifts`
--
ALTER TABLE `shifts`
  ADD CONSTRAINT `shifts_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `shifts_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `shifts_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `staff_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_caste_id_foreign` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`caste_id`),
  ADD CONSTRAINT `staff_designation_id_foreign` FOREIGN KEY (`designation_id`) REFERENCES `designations` (`designation_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_nationality_id_foreign` FOREIGN KEY (`nationality_id`) REFERENCES `nationality` (`nationality_id`),
  ADD CONSTRAINT `staff_reference_admin_id_foreign` FOREIGN KEY (`reference_admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_religion_id_foreign` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`religion_id`),
  ADD CONSTRAINT `staff_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_staff_role_id_foreign` FOREIGN KEY (`staff_role_id`) REFERENCES `staff_roles` (`staff_role_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `staff_class_allocations`
--
ALTER TABLE `staff_class_allocations`
  ADD CONSTRAINT `staff_class_allocations_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_class_allocations_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_class_allocations_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_class_allocations_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_class_allocations_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_class_allocations_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `staff_documents`
--
ALTER TABLE `staff_documents`
  ADD CONSTRAINT `staff_documents_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `document_category` (`document_category_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_documents_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_documents_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_documents_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  ADD CONSTRAINT `staff_leaves_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_leaves_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_leaves_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_leaves_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `staff_roles`
--
ALTER TABLE `staff_roles`
  ADD CONSTRAINT `staff_roles_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_roles_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_roles_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `streams`
--
ALTER TABLE `streams`
  ADD CONSTRAINT `streams_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `streams_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `streams_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_caste_id_foreign` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`caste_id`),
  ADD CONSTRAINT `students_nationality_id_foreign` FOREIGN KEY (`nationality_id`) REFERENCES `nationality` (`nationality_id`),
  ADD CONSTRAINT `students_reference_admin_id_foreign` FOREIGN KEY (`reference_admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_religion_id_foreign` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`religion_id`),
  ADD CONSTRAINT `students_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_student_parent_id_foreign` FOREIGN KEY (`student_parent_id`) REFERENCES `student_parents` (`student_parent_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_student_sibling_class_id_foreign` FOREIGN KEY (`student_sibling_class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_academic_history_info`
--
ALTER TABLE `student_academic_history_info`
  ADD CONSTRAINT `student_academic_history_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_student_academic_info_id_foreign` FOREIGN KEY (`student_academic_info_id`) REFERENCES `student_academic_info` (`student_academic_info_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_academic_info`
--
ALTER TABLE `student_academic_info`
  ADD CONSTRAINT `student_academic_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_admission_class_id_foreign` FOREIGN KEY (`admission_class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_admission_section_id_foreign` FOREIGN KEY (`admission_section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_admission_session_id_foreign` FOREIGN KEY (`admission_session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_current_class_id_foreign` FOREIGN KEY (`current_class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_current_section_id_foreign` FOREIGN KEY (`current_section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_current_session_id_foreign` FOREIGN KEY (`current_session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`),
  ADD CONSTRAINT `student_academic_info_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_competition_mapping`
--
ALTER TABLE `student_competition_mapping`
  ADD CONSTRAINT `student_competition_mapping_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_competition_mapping_competition_id_foreign` FOREIGN KEY (`competition_id`) REFERENCES `competitions` (`competition_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_competition_mapping_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_competition_mapping_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_competition_mapping_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_documents`
--
ALTER TABLE `student_documents`
  ADD CONSTRAINT `student_documents_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `document_category` (`document_category_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_documents_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_documents_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_documents_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_health_info`
--
ALTER TABLE `student_health_info`
  ADD CONSTRAINT `student_health_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_health_info_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_health_info_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_health_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_leaves`
--
ALTER TABLE `student_leaves`
  ADD CONSTRAINT `student_leaves_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_leaves_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_leaves_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_leaves_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_parents`
--
ALTER TABLE `student_parents`
  ADD CONSTRAINT `student_parents_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_parents_reference_admin_id_foreign` FOREIGN KEY (`reference_admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_parents_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_parents_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_virtual_class_mapping`
--
ALTER TABLE `student_virtual_class_mapping`
  ADD CONSTRAINT `student_virtual_class_mapping_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_virtual_class_mapping_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_virtual_class_mapping_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_virtual_class_mapping_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_virtual_class_mapping_virtual_class_id_foreign` FOREIGN KEY (`virtual_class_id`) REFERENCES `virtual_classes` (`virtual_class_id`) ON DELETE CASCADE;

--
-- Constraints for table `subjects`
--
ALTER TABLE `subjects`
  ADD CONSTRAINT `subjects_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subjects_co_scholastic_type_id_foreign` FOREIGN KEY (`co_scholastic_type_id`) REFERENCES `co_scholastic_types` (`co_scholastic_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subjects_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subjects_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `subject_class_mapping`
--
ALTER TABLE `subject_class_mapping`
  ADD CONSTRAINT `subject_class_mapping_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_class_mapping_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_class_mapping_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_class_mapping_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_class_mapping_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `term_exams`
--
ALTER TABLE `term_exams`
  ADD CONSTRAINT `term_exams_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `term_exams_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `term_exams_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `titles`
--
ALTER TABLE `titles`
  ADD CONSTRAINT `titles_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `titles_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `titles_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `virtual_classes`
--
ALTER TABLE `virtual_classes`
  ADD CONSTRAINT `virtual_classes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `virtual_classes_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `virtual_classes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
