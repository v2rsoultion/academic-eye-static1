<?php if(isset($notes['online_note_id']) && !empty($notes['online_note_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?>

<?php if(!isset($notes['online_note_id']) && empty($notes['online_note_id'])): ?>
<?php $admit_section_readonly = false; $admit_section_disabled=''; 
?>
<?php else: ?>
<?php $admit_section_readonly = false; $admit_section_disabled=''; ?>
<?php endif; ?>



<style> 

.state-error{
    color: red;
    font-size: 13px;
    margin-bottom: 10px;
}

</style>

<?php echo Form::hidden('online_note_id',old('online_note_id',isset($notes['online_note_id']) ? $notes['online_note_id'] : ''),['class' => 'gui-input', 'id' => 'online_note_id', 'readonly' => 'true']); ?>


<!-- Basic Info notes -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.t_name'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('t_name', old('t_name',isset($notes['online_note_name']) ? $notes['online_note_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.t_name'), 'id' => 't_name']); ?>

        </div>
        <?php if($errors->has('t_name')): ?> <p class="help-block"><?php echo e($errors->first('t_name')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.notes_class'); ?> :</lable>
        <label class=" field select" style="width: 100%">
            <?php echo Form::select('notes_class_id', $notes['arr_class'],isset($notes['class_id']) ? $notes['class_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'notes_class_id','onChange' => 'getSection(this.value)']); ?>

            <i class="arrow double"></i>
        </label>
        <?php if($errors->has('notes_class_id')): ?> <p class="help-block"><?php echo e($errors->first('notes_class_id')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.notes_section'); ?>:</lable>
        <label class=" field select" style="width: 100%" >
            <?php echo Form::select('notes_section_id', $notes['arr_section'],isset($notes['section_id']) ? $notes['section_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'notes_section_id',$admit_section_disabled]); ?>

            <i class="arrow double"></i>
        </label>
        <?php if($errors->has('notes_section_id')): ?> <p class="help-block"><?php echo e($errors->first('notes_section_id')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.notes_subject'); ?>:</lable>
        <label class=" field select" style="width: 100%" >
            <?php echo Form::select('notes_subject_id', $notes['arr_subject'],isset($notes['subject_id']) ? $notes['subject_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'notes_subject_id']); ?>

            <i class="arrow double"></i>
        </label>
        <?php if($errors->has('notes_subject_id')): ?> <p class="help-block"><?php echo e($errors->first('notes_subject_id')); ?></p> <?php endif; ?>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.notes_unit'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('unit', old('unit',isset($notes['online_note_unit']) ? $notes['online_note_unit']: ''), ['class' => 'form-control','placeholder'=>trans('language.notes_unit'), 'id' => 'unit']); ?>

        </div>
        <?php if($errors->has('unit')): ?> <p class="help-block"><?php echo e($errors->first('unit')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-9 col-md-9">
        <lable class="from_one1"><?php echo trans('language.notes_topic'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::textarea('topic', old('unit',isset($notes['online_note_topic']) ? $notes['online_note_topic']: ''), ['class' => 'form-control','rows'=>'5','placeholder'=>trans('language.notes_topic'), 'id' => 'topic']); ?>

        </div>
        <?php if($errors->has('topic')): ?> <p class="help-block"><?php echo e($errors->first('topic')); ?></p> <?php endif; ?>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-primary','name'=>'save']); ?>

        <a href="<?php echo url('admin-panel/dashboard'); ?>" ><?php echo Form::button('Cancel', ['class' => 'btn btn-raised btn-primary']); ?></a>
    </div>
</div>

<script type="text/javascript">

// $("select[name='current_section_id'").removeAttr("disabled");
    jQuery(document).ready(function () {
        $("#notes-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation  rules 
             ------------------------------------------ */

            rules: {
                t_name: {
                    required: true
                },
                notes_class_id: {
                    required: true
                },
                notes_section_id: {
                    required: true
                },
                notes_subject_id: {
                    required: true
                },
                unit: {
                    required: true
                },
                topic: {
                    required: true
                }
            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>

<script type="text/javascript">


function getSection(class_id)
    {
        if(class_id != "") {
            $('.mycustloading').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/notes/get-section-data')); ?>",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                        $("select[name='notes_section_id'").html('');
                        $("select[name='notes_section_id'").html(data.options);
                        $("select[name='notes_section_id'").removeAttr("disabled");
                        $("select[name='notes_section_id'").selectpicker('refresh');
                        $('.mycustloading').hide();
                }
            });
        } else {
            $("select[name='notes_section_id'").html(''); 
            $("select[name='notes_section_id'").selectpicker('refresh');
        }
    }


</script>