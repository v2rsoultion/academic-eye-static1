@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.map_subject') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/class-subject-mapping/view-subject-class-mapping') !!}">{!! trans('language.map_subject_to_class') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.map_subject') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            
                            <div class="body">
                                <span class="classname_padding">Class Name : <strong>{{$class_name}}</strong></span>
                                {!! Form::open(['files'=>TRUE,'id' => 'class-teacher-allocation-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                                {!! Form::hidden('class_id',$class_id,array('readonly' => 'true')) !!}
                                @if(session()->has('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session()->get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="designation-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>
                                                <label class="option block mn" >
                                                <input type="checkbox" id="check_all"> 
                                                <span class="checkbox mn"></span>
                                                </label>
                                            </th>
                                            <th>{{trans('language.ms_subject_name')}}</th>
                                        </tr>
                                    </thead>
                                </table>
                                </div>
                                <div class="container-fluid">
                                    <div class="row row_seach_field1">
                                            {!! Form::submit('Apply', ['class' => 'btn btn-info float-right search_button3','name'=>'save']) !!}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#designation-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            bPaginate: false,
            bInfo : false,
            bFilter: false,
            ajax: {
                url: "{{url('admin-panel/class-subject-mapping/map-subject-data')}}",
                data: function (d) {
                    d.class_id = $('input[name=class_id]').val();
                }
            },
            // ajax: '{{url('admin-panel/class-subject-mapping/map-subject-data')}}',
            
            columns: [
                {data: 'checkbox', name: 'checkbox' },
                {data: 'subjects', name: 'subjects'},
            ],
             columnDefs: [
                {
                "targets": 0,
                "orderable": false
                },
                {
                    targets: [ 0, 1],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
    });

</script>
@endsection