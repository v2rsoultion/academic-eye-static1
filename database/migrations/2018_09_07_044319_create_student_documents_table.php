<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'student_documents';
    protected $primaryKey = 'student_document_id';
    public function up()
    {
        if (!Schema::hasTable('student_documents')) {
            Schema::create('student_documents', function (Blueprint $table) {
                $table->increments('student_document_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('student_id')->unsigned()->nullable();
                $table->integer('document_category_id')->unsigned()->nullable();
                $table->text('student_document_details')->nullable();
                $table->text('student_document_file')->nullable();
                $table->tinyInteger('student_document_status')->default(0)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('student_documents', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('student_documents', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('student_documents', function($table) {
                $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade');
            });
            Schema::table('student_documents', function($table) {
                $table->foreign('document_category_id')->references('document_category_id')->on('document_category')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_documents');
    }
}
