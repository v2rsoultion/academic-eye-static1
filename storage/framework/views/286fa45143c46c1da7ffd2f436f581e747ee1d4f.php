
<?php $__env->startSection('content'); ?>
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2><?php echo trans('language.menu_fee_collection'); ?></h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-4">
                    <div class="dashboard_div" style="height: auto;">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/FeesStructure.svg'); ?>" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Fees Heads</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="<?php echo e(url('/admin-panel/fees-collection/one-time/add-one-time')); ?>" class="float-left" title="One Time ">
                          <i class="fas fa-plus"></i>One Time 
                        </a>
                        <a href="<?php echo e(url('/admin-panel/fees-collection/recurring-heads/add-recurring-head')); ?>" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>Recurring Heads 
                        </a>
                        <div class="clearfix"></div>
                         <a href="" class="cusa" title="View ">
                          <i class="fas fa-eye"></i>Facilities Fees 
                        </a>
                        <div class="clearfix"></div>
                        <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="<?php echo e(url('/admin-panel/fees-collection/one-time/view-one-time')); ?>" title="">View One Time</a>
                              </li>
                              <li>
                                <a href="<?php echo e(url('/admin-panel/fees-collection/recurring-heads/view-recurring-heads')); ?>" title="">View Recurring Heads</a>
                              </li>
                              <li>
                                <a href="#" title="">View Facilities Fee</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-md-4">
                    <div class="dashboard_div" style="min-height: 207px;">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/MapStudent.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Map Free Student</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/view-rates')); ?>" class="cusa" title="Free By Rte " style="width: 48%; margin: 20px auto;">
                      <i class="fas fa-plus"></i> By Rte 
                      </a>
                      <!-- <a href="<?php echo e(url('/admin-panel/fees-collection/view-free-by-management')); ?>" class="float-right" title="Free By Management">
                      <i class="fas fa-eye" style="margin-right: 0px;"></i>Management 
                      </a> -->
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="dashboard_div" style="min-height: 207px;">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/Map Heads to Student.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Map Heads to Student</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/view-map-student')); ?>" class="cusa" title="Add" style="width: 68%; margin: 20px auto;">
                      <i class="fas fa-plus"></i> Manage Head Student 
                      </a>
                      <!-- <a href="" class="float-right" title="View ">
                      <i class="fas fa-eye"></i>View 
                      </a> -->
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
                <div class="row">
                   <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/FacilitiesFees.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Concession </div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <!--    <a href="" class="float-left" title="Add ">
                        <i class="fas fa-plus"></i> Add 
                        </a> -->
                      <a href="<?php echo e(url('/admin-panel/fees-collection/concession/view-concession-details')); ?>" class="cusa" title="View ">
                      <i class="fas fa-eye"></i>View 
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/Fee Counter.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Fees Counter</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/fee-counter/add-fee-counter')); ?>" class="cusa" title="Add ">
                      <i class="fas fa-plus"></i> Add 
                      </a>
                      <!-- <a href="" class="float-right" title="View ">
                      <i class="fas fa-eye"></i>View 
                      </a> -->
                      <div class="clearfix"></div>
                    </div>
                  </div>
                   <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/Cheque Details.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Cheques Details</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/cheque-details/view-cheque-details')); ?>" class="cusa" title="Add " style="width: 68%; margin: 10px auto;">
                      <i class="fas fa-plus"></i> List of Cheques  
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/RTE Collection.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">RTE Collection</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/rte-collection/rte-fee-head')); ?>" class="float-left" title="RTE Fees Head  ">
                      <i class="fas fa-plus"></i> RTE Fees Head 
                      </a>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/rte-collection/apply-fees-on-head')); ?>" class="float-right" title="Apply Fees on Head">
                      <i class="fas fa-eye"></i>Apply Fees on Head 
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
                <div class="row">
                   <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/Imprest Account.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Imprest Account</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/rte-received-cheque/add-rte-cheque-detail')); ?>" class="float-left" title="RTE Fees Head  ">
                      <i class="fas fa-plus"></i>Add
                      </a>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/rte-received-cheque/view-rte-cheque-detail')); ?>" class="float-right" title="Apply Fees on Head">
                      <i class="fas fa-eye"></i>View
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                   <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/wallet.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Wallet Account</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/rte-received-cheque/add-rte-cheque-detail')); ?>" class="float-left" title="RTE Fees Head  ">
                      <i class="fas fa-plus"></i>Add
                      </a>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/rte-received-cheque/view-rte-cheque-detail')); ?>" class="float-right" title="Apply Fees on Head">
                      <i class="fas fa-eye"></i>View
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                   <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/bank.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Bank</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/rte-received-cheque/add-rte-cheque-detail')); ?>" class="float-left" title="RTE Fees Head  ">
                      <i class="fas fa-plus"></i>Add
                      </a>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/rte-received-cheque/view-rte-cheque-detail')); ?>" class="float-right" title="Apply Fees on Head">
                      <i class="fas fa-eye"></i>View
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                   <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/Fine.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Fine</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/rte-received-cheque/add-rte-cheque-detail')); ?>" class="float-left" title="RTE Fees Head  ">
                      <i class="fas fa-plus"></i>Add
                      </a>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/rte-received-cheque/view-rte-cheque-detail')); ?>" class="float-right" title="Apply Fees on Head">
                      <i class="fas fa-eye"></i>View
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
                <div class="row">
                 
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/RTE Received Cheque.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">RTE Received Cheque</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/rte-received-cheque/add-rte-cheque-detail')); ?>" class="float-left" title="RTE Fees Head  ">
                      <i class="fas fa-plus"></i>Add
                      </a>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/rte-received-cheque/view-rte-cheque-detail')); ?>" class="float-right" title="Apply Fees on Head">
                      <i class="fas fa-eye"></i>View
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                   <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/Receipts.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Receipts</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/fees-receipt/view-fees-receipt')); ?>" class="cusa" title="Add" style="width: 48%; margin: 15px auto;">
                      <i class="fas fa-plus"></i>View
                      </a>
                     <!--  <a href="cancel_fee_receipt.php" class="float-right" title="Cancel"> -->
                       <!-- <a href="#" class="float-right" title="Cancel">
                      <i class="fas fa-eye"></i>Cancel
                      </a> -->
                      <div class="clearfix"></div>
                    </div>
                  </div>
                   <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/PrepaidAccount.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Prepaid Account</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/prepaid-account/manage-account')); ?>" class="cusa" title="View" style="width: 68%; margin: 15px auto;">
                      <i class="fas fa-eye"></i>Manage Accounts 
                      </a>
                     <!--  <a href="<?php echo e(url('/admin-panel/fees-collection/prepaid-account/manage-amount')); ?>" class="float-right" title="View">
                      <i class="fas fa-eye"></i> Manage Amounts 
                      </a> -->
                      <div class="clearfix"></div>
                        <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="<?php echo e(url('/admin-panel/fees-collection/prepaid-account/manage-amount')); ?>" title="">  Manage Amounts </a>
                              </li>
                              <li>
                                <a href="<?php echo e(url('/admin-panel/fees-collection/prepaid-account/common-entry')); ?>" title="">Add Common Entry</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                  </div>
                   <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/Account Report.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Account Report</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/report/view-report')); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                      <i class="fas fa-plus"></i>View
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/Report.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Reports</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/report/view-reports')); ?>" class="cusa" title="Add " style="width: 48%; margin: 15px auto;">
                      <i class="fas fa-eye"></i> View
                      </a>
                     
                        <div class="clearfix"></div>
                    </div>
                  </div>
                  <!-- <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/Report.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Reports</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/report/class-sheet-report')); ?>" class="cusa" title="Add " style="width: 64%; margin: 15px auto;">
                      <i class="fas fa-eye"></i> Class Wise Sheet 
                      </a>
                      <a href="<?php echo e(url('/admin-panel/fees-collection/report/student-sheet-report')); ?>" class="float-right" title="View ">
                      <i class="fas fa-eye"></i>Student Sheet 
                      </a>
                      <div class="clearfix"></div>
                      <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="<?php echo e(url('/admin-panel/fees-collection/report/student-sheet-report')); ?>" title="">  Student Wise Sheet </a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                  </div> -->
                </div> 
                <div class="row">
                 
                 <!--  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/Report.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Reports</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="" class="float-left" title="Add ">
                      <i class="fas fa-plus"></i> Add 
                      </a>
                      <a href="" class="float-right" title="View ">
                      <i class="fas fa-eye"></i>View 
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div> -->
                   <!-- <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/OneTimeFees.svg'); ?>" alt="Student" >
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">One Time Fees</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="" class="float-left" title="Add ">
                      <i class="fas fa-plus"></i> Add 
                      </a>
                      <a href="" class="float-right" title="View ">
                      <i class="fas fa-eye"></i>View 
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div> -->
                  <!-- <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/FeesCollection/FacilitiesFees.svg'); ?>" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Facilities Fees</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="" class="float-left" title="Add ">
                      <i class="fas fa-plus"></i> Add 
                      </a>
                      <a href="" class="float-right" title="View ">
                      <i class="fas fa-eye"></i>View 
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div> -->
                </div>         
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    <div class="clearfix"></div>
  </div>
  </div>
</section>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>