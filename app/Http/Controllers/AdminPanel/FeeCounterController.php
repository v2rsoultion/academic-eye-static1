<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeeCounterController extends Controller
{
    public function feeCounter() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.fee-counter.fee_counter')->with($data);
    }
}
