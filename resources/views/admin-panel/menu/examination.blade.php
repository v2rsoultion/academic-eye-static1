@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2>Examination And Report Cards</h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <!-- <li class="breadcrumb-item">{!! trans('language.menu_examination') !!}</li> -->
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/examination') !!}">Examination And Report Cards</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Examination/Terms.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Manage Terms</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{!! URL::to('admin-panel/examination/manage-term') !!}" class="cusa" title="Add " style="width: 60%; margin: 15px auto;">
                          <i class="fas fa-plus"></i> Manage Term 
                        </a>
                        <!-- <a href="" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a> -->
                        <div class="clearfix"></div>
                        <!-- <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="#" title="">Option 1</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 2</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 3</a>
                              </li>
                            </ul>
                          </li>
                        </ul> -->
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Examination/Exams.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Manage Exams</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{!! URL::to('admin-panel/examination/manage-exam') !!}" class="cusa" title="Add " style="width: 60%; margin: 15px auto;">
                          <i class="fas fa-plus"></i> Manage Exams
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="dashboard_div">
                        <div class="imgdash">
                          <img src="{!! URL::to('public/assets/images/Examination/GradeScheme.svg') !!}" alt="Student" >
                          </div>
                          <h4 class="">
                            <div class="tableCell" style="height: 64px;">
                              <div class="insidetable">Grade Scheme</div>
                            </div>
                          </h4>
                          <div class="clearfix"></div>
                          <a href="{{ url('/admin-panel/examination/grade-scheme/add-grade-scheme') }}" class="float-left" title="Add ">
                            <i class="fas fa-plus"></i> Add 
                          </a>
                          <a href="{{ url('/admin-panel/examination/grade-scheme/view-grade-scheme') }}" class="float-right" title="View ">
                            <i class="fas fa-eye"></i>View 
                          </a>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Examination/Markscriteria.svg') !!}" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Marks Criteria</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="{{ url('/admin-panel/examination/marks-criteria/add') }}" class="cusa" title="Add" style="width: 48%; margin: 15px auto;">
                              <i class="fas fa-plus"></i> Manage 
                            </a>
                            <!-- <a href="" class="float-right" title="View ">
                              <i class="fas fa-eye"></i>View 
                            </a> -->
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        
                        </div>
                        <div class="row">
                          <div class="col-md-4">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="{!! URL::to('public/assets/images/Examination/MapExamTClassSubjects.svg') !!}" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Map Exam To Class Subjects</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="{{ url('/admin-panel/examination/map-exam-to-class-subjects/add') }}" class="float-left" title="Add ">
                                <i class="fas fa-plus"></i> Manage 
                              </a>
                              <a href="{{ url('/admin-panel/examination/map-exam-to-class-subjects/view') }}" class="float-right" title="View ">
                                <i class="fas fa-eye"></i>Mapping Report
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="{!! URL::to('public/assets/images/Examination/MapMarkCriteriatoSubjects.svg') !!}" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Map Mark Criteria to Subjects</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="{{ url('/admin-panel/examination/map-mark-criteria-to-subjects/add') }}" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Manage 
                                </a>
                                <a href="{{ url('/admin-panel/examination/map-mark-criteria-to-subjects/view') }}" class="float-right" title="View ">
                                  <i class="fas fa-eye"></i>Mapping Report 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Examination/MapGradeSchemetoSubjects.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Map Grade Scheme to Subjects</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <a href="{{ url('/admin-panel/examination/map-grade-scheme-to-subjects/add') }}" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Manage 
                                  </a>
                                  <a href="{{ url('/admin-panel/examination/map-grade-scheme-to-subjects/view') }}" class="float-right" title="View ">
                                    <i class="fas fa-eye"></i>Mapping Report 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                             
                                </div>


                                 <div class="row">
                                   <div class="col-md-3">
                                <div class="dashboard_div">
                                  <div class="imgdash">
                                    <img src="{!! URL::to('public/assets/images/Examination/Marksheet.svg') !!}" alt="Student">
                                    </div>
                                    <h4 class="">
                                      <div class="tableCell" style="height: 64px;">
                                        <div class="insidetable">Marksheet</div>
                                      </div>
                                    </h4>
                                    <div class="clearfix"></div>
                                    <a href="" class="float-left" title="Add ">
                                      <i class="fas fa-plus"></i> Add 
                                    </a>
                                    <a href="" class="float-right" title="View ">
                                      <i class="fas fa-eye"></i>View 
                                    </a>
                                    <div class="clearfix"></div>
                                  </div>
                                </div>
                              
                                    <div class="col-md-3">
                                  <div class="dashboard_div">
                                    <div class="imgdash">
                                      <img src="{!! URL::to('public/assets/images/Examination/ExamSchedule.svg') !!}" alt="Student">
                                      </div>
                                      <h4 class="">
                                        <div class="tableCell" style="height: 64px;">
                                          <div class="insidetable">Exam Schedule</div>
                                        </div>
                                      </h4>
                                      <div class="clearfix"></div>
                                      <a href="{{ url('/admin-panel/examination/exam-schedule/add') }}" class="cusa" style="width: 68%; margin: 15px auto;" title="Add ">
                                        <i class="fas fa-plus"></i> Manage Schedule 
                                      </a>
                                     <!--  <a href="{{ url('/admin-panel/examination/exam-schedule/view') }}" class="float-right" title="View ">
                                        <i class="fas fa-eye"></i>View 
                                      </a> -->
                                      <div class="clearfix"></div>
                                    </div>
                                  </div>
                          <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="{!! URL::to('public/assets/images/Examination/Marks.svg') !!}" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Marks</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="{{ url('/admin-panel/examination/marks/add') }}" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a>
                                <a href="{{ url('/admin-panel/examination/marks/view') }}" class="float-right" title="View ">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Examination/GenerateReportCard.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Generate Report Card</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a>
                                  <a href="" class="float-right" title="View ">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                                </div>
                              <div class="row">
                              <div class="col-md-3">
                                <div class="dashboard_div">
                                  <div class="imgdash">
                                    <img src="{!! URL::to('public/assets/images/Examination/Report.svg') !!}" alt="Student">
                                    </div>
                                    <h4 class="">
                                      <div class="tableCell" style="height: 64px;">
                                        <div class="insidetable">Report</div>
                                      </div>
                                    </h4>
                                    <div class="clearfix"></div>
                                    <a href="" class="float-left" title="Add ">
                                      <i class="fas fa-plus"></i> Add 
                                    </a>
                                    <a href="" class="float-right" title="View ">
                                      <i class="fas fa-eye"></i>View 
                                    </a>
                                    <div class="clearfix"></div>
                                  </div>
                                </div>

                              </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
</section>
@endsection

