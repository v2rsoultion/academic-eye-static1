<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MapExamToClassSubjectsController extends Controller
{
     public function add() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.map-exam-to-class-subjects.map_exam_to_class_subjects')->with($data);
    }
     public function addClass() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.map-exam-to-class-subjects.add_class_in_map')->with($data);
    }

     public function mappedClass() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.map-exam-to-class-subjects.mapped_class')->with($data);
    }

    public function viewClass() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.map-exam-to-class-subjects.view_map_exam_class_subjects')->with($data);
    }
}
