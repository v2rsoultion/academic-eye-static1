@if(isset($designation['designation_id']) && !empty($designation['designation_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif
<style type="text/css">
    .theme-blush .btn-primary {
    margin-top: 2px !important;
}
</style>
{!! Form::hidden('designation_id',old('designation_id',isset($designation['designation_id']) ? $designation['designation_id'] : ''),['class' => 'gui-input', 'id' => 'designation_id', 'readonly' => 'true']) !!}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.designation_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('designation_name', old('designation_name',isset($designation['designation_name']) ? $designation['designation_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.designation_name'), 'id' => 'designation_name']) !!}
        </div>
        @if ($errors->has('designation_name')) <p class="help-block">{{ $errors->first('designation_name') }}</p> @endif
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

       jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
        }, "Please use only alphanumeric values");

        $("#designation-form").validate({

            /* @validation states + elements ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules ------------------------------------------ */
            rules: {
                designation_name: {
                    required: true,
                    lettersonly:true
                }
            },
            /* @validation highlighting + error placement  ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>