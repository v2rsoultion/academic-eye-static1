<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookAllowanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'book_allowance';
    protected $primaryKey = 'book_allowance_id';
    public function up()
    {
        if (!Schema::hasTable('book_allowance')){
            Schema::create('book_allowance', function (Blueprint $table) {
                $table->increments('book_allowance_id');
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0) ;
                $table->integer('allow_for_staff')->unsigned()->default(0) ;
                $table->integer('allow_for_student')->unsigned()->default(0) ;
                $table->tinyInteger('book_category_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('book_allowance', function($table) {
                    $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
                });
            Schema::table('book_allowance', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('book_allowance', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_allowance');
    }
}
