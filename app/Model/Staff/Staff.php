<?php

namespace App\Model\Staff;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table      = 'staff';
    protected $primaryKey = 'staff_id';


    public function getDesignation()
    {
        return $this->belongsTo('App\Model\Designation\Designation', 'designation_id');
    }
}
