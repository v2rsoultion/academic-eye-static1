<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AssignDriverConductorController extends Controller
{
	/**
     *  Manage Driver Conductor
     *  @Khushbu on 28 Sept 2018   
    **/
    public function manageDriverConductor() {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.assign-driver-conductor.assign-driver-conductor')->with($data);
    }
}
