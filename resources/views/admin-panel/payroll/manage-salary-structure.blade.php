@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Salary Structure</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/payroll') !!}">Payroll</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-salary-structure') !!}">Salary Structure</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card" style="margin-bottom: 20px;">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body" style="padding: 0px 20px 20px 20px;">
               
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Add Salary Structure :-</div> -->
                  <form class="" action="" id="manage_vendor" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Structure Name</lable>
                          <input type="text" name="structure_name" id="" class="form-control" placeholder="Structure Name">
                        </div>
                         @if ($errors->has('structure_name')) <p class="help-block">{{ $errors->first('structure_name') }}</p> @endif
                      </div>
                      <div class="col-lg-1 ">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
                        </button>
                      </div>
                      <div class="col-lg-1 ">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
                        </button>
                      </div>
                    </div>
                  
                  </form>
                </div>
              </div>
                  <div class="card">
                    <div class="body form-gap">
                  <div class="headingcommon  col-lg-12" style="margin-left: -13px">Search By :-</div>
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                       <div class="col-lg-3">
                        <div class="form-group">
                          <!-- <lable class="from_one1">Structure Name</lable> -->
                          <input type="text" name="structure_name" id="" class="form-control" placeholder="Structure Name">
                        </div>
                      </div>
                     
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary" title="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                
                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <!-- <hr> -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Structure Name</th>
                        <!-- <th>Cal Type</th>
                        <th>PF Vol (%)</th>
                        <th>ESI Vol (%)</th>
                        <th>Round</th> -->
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Structure - 1</td>
                        <!-- <td>Amount</td>
                        <td>2</td>
                        <td>10</td>
                        <td>Nearest Rupee</td> -->
                        <td>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                          <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                              <li> <a title="Manage Head" href="#" data-toggle="modal" data-target="#ManageHeadsModel">Manage Head</a></li>   
                              <li> <a title="Map Structure" href="{{url('admin-panel/payroll/manage-salary-structure/map-structure')}}">Map Structure</a></li> 
                            </ul> </div>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Structure - 2</td>
                        <!-- <td>Basic Pay</td>
                        <td>5</td>
                        <td>20</td>
                        <td>Highest Rupee</td> -->
                        <td>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                          <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li> <a title="Manage Head" href="#" data-toggle="modal" data-target="#ManageHeadsModel">Manage Head</a></li>   
                                <li> <a title="Map Structure" href="{{url('admin-panel/payroll/manage-salary-structure/map-structure')}}">Map Structure</a></li> 
                            </ul> </div>
                        </td>
                      </tr>
                    
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->

<script type="text/javascript">
  function checkForAmount(val) {
        var x = document.getElementById("AmountMode");
        if(val == 0) {
            if (x.style.display === "none") {
                x.style.display = "none";
            } else {
                x.style.display = "none";
            }
        } else {
            if (x.style.display === "block") {
                x.style.display = "block";
            } else {
                x.style.display = "block";
            }
        }

        var y = document.getElementById("BasicPayMode");
        if(val == 1) {
            if (y.style.display === "none") {
                y.style.display = "none";
            } else {
                y.style.display = "none";
            }
        } else {
            if (y.style.display === "block") {
                y.style.display = "block";
            } else {
                y.style.display = "block";
            }
        }
    }
</script>

<!-- Action Model  -->
<div class="modal fade" id="ManageHeadsModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Manage Head</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
     
      <div class="modal-body">
         <div style="border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;margin: 5px 0px;">
        <div><b>Structure Name:</b> Exam Fees</div>
      </div>
        <div class="row">
         <div class="col-lg-3">
            <div class="form-group">
              <lable class="from_one1">Heads</lable>
              <select class="form-control show-tick select_form1" name="select_head" id="select_head">
                <option value="">Select Heads</option>
                <option value="1">Basic Pay</option>
                <option value="2">Head-1</option>
                <option value="3">Head-2</option>
                <option value="4">Head-3</option>
                </select>
            </div>
          </div>
          <div class="col-lg-3" id="BasicPayMode" style="display: none;">
            <div class="form-group">
              <lable class="from_one1">Basic Pay (%)</lable>
              <input type="text" name="basic_pay" id="" class="form-control" placeholder="Basic Pay (%)">
            </div>
              @if ($errors->has('basic_pay')) <p class="help-block">{{ $errors->first('basic_pay') }}</p> @endif  
          </div> 
        </div>

        <div class="row" id="otherHeadsMode" style="display: none;" >
          <div class="col-lg-3">
            <div class="form-group">
              <lable class="from_one1">PF Vol (%)</lable>
              <input type="text" name="pf_vol" id="" class="form-control" placeholder="PF Vol(%)">
            </div>
          </div>
           <div class="col-lg-3">
            <div class="form-group">
              <lable class="from_one1">ESI Vol (%)</lable>
              <input type="text" name="pf_vol" id="" class="form-control" placeholder="PF Vol(%)">
            </div>
          </div>
           <div class="col-lg-6">
            <div class="form-group">
              <lable class="from_one1">Round Off</lable>
              <div class="radio" style="margin-top:6px !important;">
               <input id="radio33" name="calculation_basic" type="radio">
                <label for="radio33" class="document_staff"> Lower Rupee</label>
                <input id="radio34" name="calculation_basic" type="radio">
                <label for="radio34" class="document_staff"> Nearest Rupee</label>
              </div>
            </div>
          </div>
        </div>
         <hr>
        <div class="row">
        <div class="col-lg-1 ">
          <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
          </button>
        </div>
        <div class="col-lg-1 ">
          <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
          </button>
        </div>
      
      </div>
         
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#select_head').on('change', function() {
      var test = $("#select_head").val();

      if(test == 1)
      {
        $('#BasicPayMode').show();
        $("#otherHeadsMode").hide();
      }
      else
      {
        $("#otherHeadsMode").show();
         $('#BasicPayMode').hide();
      }
  });
</script>
@endsection