<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Caste\Caste; // Model
use Yajra\Datatables\Datatables;

class CasteController extends Controller
{
    /**
     *  View page for Caste
     *  @Pratyush on 20 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_caste'),
            'redirect_url'  => url('admin-panel/caste/view-caste'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.caste.index')->with($data);
    }

    /**
     *  Add page for Caste
     *  @Pratyush on 20 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $caste      	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_caste_id 	    = get_decrypted_value($id, true);
            $caste      			    = Caste::Find($decrypted_caste_id);
            if (!$caste)
            {
                return redirect('admin-panel/caste/add-caste')->withError('Caste not found!');
            }
            $page_title             	= trans('language.edit_caste');
            $encrypted_caste_id         = get_encrypted_value($caste->caste_id, true);
            $save_url               	= url('admin-panel/caste/save/' . $encrypted_caste_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_caste');
            $save_url      = url('admin-panel/caste/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'caste' 		    => $caste,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/caste/view-caste'),
        );
        return view('admin-panel.caste.add')->with($data);
    }

    /**
     *  Add and update Caste's data
     *  @Pratyush on 20 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_caste_id	        = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $caste = Caste::find($decrypted_caste_id);

            if (!$caste)
            {
                return redirect('/admin-panel/caste/add-caste/')->withError('Caste not found!');
            }
            $success_msg = 'Caste updated successfully!';
        }
        else
        {
            $caste     = New Caste;
            $success_msg = 'Caste saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
            'caste_name'   => 'required|unique:caste,caste_name,' . $decrypted_caste_id . ',caste_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $caste->admin_id            	= $loginInfo['admin_id'];
                $caste->update_by           	= $loginInfo['admin_id'];
                $caste->caste_name 		        = Input::get('caste_name');
                $caste->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/caste/view-caste')->withSuccess($success_msg);
    }

    /**
     *  Get Caste's Data for view page(Datatables)
     *  @Pratyush on 20 July 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $caste  		    = Caste::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('caste_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('caste_id', 'DESC')->get();

        return Datatables::of($caste)
        		->addColumn('action', function ($caste)
                {

                    $encrypted_caste_id = get_encrypted_value($caste->caste_id, true);
                    if($caste->caste_status == 0) {
                        $status = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="caste-status/'.$status.'/' . $encrypted_caste_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-caste/' . $encrypted_caste_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-caste/' . $encrypted_caste_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Destroy Caste's data
     *  @Pratyush on 20 July 2018.
    **/
    public function destroy($id)
    {
        $caste_id 		= get_decrypted_value($id, true);
        $caste 		  	= Caste::find($caste_id);
        if ($caste)
        {
            $caste->delete();
            $success_msg = "Caste deleted successfully!";
            return redirect('admin-panel/caste/view-caste')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Caste not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Caste's status
     *  @Pratyush on 20 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $caste_id       = get_decrypted_value($id, true);
        $caste          = Caste::find($caste_id);
        if ($caste)
        {
            $caste->caste_status  = $status;
            $caste->save();
            $success_msg = "Caste status updated!";
            return redirect('admin-panel/caste/view-caste')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Caste not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
