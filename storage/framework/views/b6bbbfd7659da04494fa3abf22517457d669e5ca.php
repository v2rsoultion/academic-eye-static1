<?php $__env->startSection('content'); ?>
<!-- Main Content -->
<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Add Fees Head</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Transport</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Manage Fees</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/transport/manage-fees/add-fees-head'); ?>">Add Fees Head</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div>
                    <div class="body form-gap">
                        <form class="" action="" method="post"  id="form_validation">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1"> Name :</lable>
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1"> Km Range :</lable>
                                    <div class="form-group">
                                        <!-- <label class="from_one1">Km(Min)</label> -->
                                        <input type="Number" name="km_min" class="form-control" placeholder="Km Range(Min)">
                                    </div>
                                </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group">
                                        <label class="">Km(Max)</label>
                                        <input type="Number" name="km_max" class="form-control" style="margin-top: -4px" placeholder="Km Range(Max)" >
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1">Amount:</lable>
                                    <div class="form-group">
                                        <input type="text" name="amount" class="form-control" placeholder="Amount">
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-raised  btn-primary">Save</button>
                                    <button type="submit" class="btn btn-raised btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<script type="text/javascript">

$('#form_validation').validate({
        rules: {
            'name': {
                required: true
            },
            'amount': {
                required: true
            }
        },

        /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        name: {
          required: 'Please fill your required name'
        },
        amount: {
          required: 'Please fill requerd amount '
        }         
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

</script>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>