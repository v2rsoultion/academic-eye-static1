<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarksController extends Controller
{
    public function addMarks() {
    	$loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.marks.add_marks')->with($data);
    }
     public function viewMarks() {
    	$loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.marks.view_marks')->with($data);
    }
}
