<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin-panel','middleware' => 'admin.guest'], function () {
  Route::get('/', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'admin-panel','middleware' => 'admin'], function () {

  Route::get('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  // For Dashboard
  Route::get('/dashboard', 'AdminPanel\DashboardController@index');

  // For Admin Profile
  // Route::get('/dashboard', 'AdminPanel\DashboardController@index');

  // For School
  Route::get('/school/add-school/{id?}', 'AdminPanel\SchoolController@add');
  Route::post('/school/add-school/save/{id?}', 'AdminPanel\SchoolController@save');
  
  // Route::get('/school/view-school-detail/{id?}', 'AdminPanel\SchoolController@profile');

  Route::get('/admin-profile/{id?}', 'AdminPanel\SchoolController@profile');

  // For Session
  Route::get('/session/add-session/{id?}', 'AdminPanel\SessionController@add');
  Route::post('/session/save/{id?}', 'AdminPanel\SessionController@save');
  Route::get('/session/view-sessions', 'AdminPanel\SessionController@index');
  Route::get('/session/data', 'AdminPanel\SessionController@anyData');
  Route::get('/session/delete-session/{id?}', 'AdminPanel\SessionController@destroy');
  Route::post('/session/set-session', 'AdminPanel\SessionController@setSession');
  Route::get('/session/session-status/{status?}/{id?}', 'AdminPanel\SessionController@changeStatus');

  // For Holidays
  Route::get('/holidays/add-holiday/{id?}', 'AdminPanel\HolidayController@add');
  Route::post('/holidays/save/{id?}', 'AdminPanel\HolidayController@save');
  Route::get('/holidays/view-holidays', 'AdminPanel\HolidayController@index');
  Route::get('/holidays/data', 'AdminPanel\HolidayController@anyData');
  Route::get('/holidays/delete-holiday/{id?}', 'AdminPanel\HolidayController@destroy');
  Route::get('/holidays/holiday-status/{status?}/{id?}', 'AdminPanel\HolidayController@changeStatus');

  // For shifts
  Route::get('/shift/add-shift/{id?}', 'AdminPanel\ShiftController@add');
  Route::post('/shift/save/{id?}', 'AdminPanel\ShiftController@save');
  Route::get('/shift/view-shifts', 'AdminPanel\ShiftController@index');
  Route::get('/shift/data', 'AdminPanel\ShiftController@anyData');
  Route::get('/shift/delete-shift/{id?}', 'AdminPanel\ShiftController@destroy');
  Route::get('/shift/shift-status/{status?}/{id?}', 'AdminPanel\ShiftController@changeStatus');

  // Academic section start here
  // For Class
  Route::get('/class/add-class/{id?}', 'AdminPanel\ClassesController@add');
  Route::post('/class/save/{id?}', 'AdminPanel\ClassesController@save');
  Route::get('/class/view-classes', 'AdminPanel\ClassesController@index');
  Route::get('/class/data', 'AdminPanel\ClassesController@anyData');
  Route::get('/class/delete-class/{id?}', 'AdminPanel\ClassesController@destroy');
  Route::get('/class/class-status/{status?}/{id?}', 'AdminPanel\ClassesController@changeStatus');

  // For Section
  Route::get('/section/add-section/{id?}', 'AdminPanel\SectionController@add');
  Route::post('/section/save/{id?}', 'AdminPanel\SectionController@save');
  Route::get('/section/view-sections', 'AdminPanel\SectionController@index');
  Route::get('/section/data', 'AdminPanel\SectionController@anyData');
  Route::get('/section/delete-section/{id?}', 'AdminPanel\SectionController@destroy');
  Route::get('/section/section-status/{status?}/{id?}', 'AdminPanel\SectionController@changeStatus');

  // For Ajax
  Route::get('/class/get-class-data/{medium?}', 'AdminPanel\ClassesController@getClassData');
  Route::get('/stream/get-stream-data/{medium?}', 'AdminPanel\StreamController@getStreamData');
  Route::get('/student/check-section-capacity/{id?}', 'AdminPanel\StudentController@checkSectionCapacity');
  Route::get('/state/show-state/{id?}', 'AdminPanel\StateController@showStates');
  Route::get('/city/show-city/{id?}', 'AdminPanel\CityController@showCity');
  Route::get('/brochure/get-brochure-data/{session_id?}', 'AdminPanel\BrochureController@getBrochureData');
  Route::get('/class/get-total-intake-data/{class_id?}', 'AdminPanel\ClassesController@getIntakeData');

  // Route::get('/student/remove-student-document-data/{id?}/{id?}', 'AdminPanel\StudentController@destroyDocument');
  
  // For subject 
  // For Type of Co-Scholastic 
  Route::get('/subject/add-type-of-co-scholastic/{id?}', 'AdminPanel\CoScholasticController@add');
  Route::post('/subject/save-type-of-co-scholastic/{id?}', 'AdminPanel\CoScholasticController@save');
  Route::get('/subject/view-type-of-co-scholastic', 'AdminPanel\CoScholasticController@index');
  Route::get('/subject/get-data-co-scholastic', 'AdminPanel\CoScholasticController@anyData');
  Route::get('/subject/delete-type-of-co-scholastic/{id?}', 'AdminPanel\CoScholasticController@destroy');
  Route::get('/subject/type-of-co-scholastic-status/{status?}/{id?}', 'AdminPanel\CoScholasticController@changeStatus');

  // For subjects
  Route::get('/subject/add-subject/{id?}', 'AdminPanel\SubjectController@add');
  Route::post('/subject/save/{id?}', 'AdminPanel\SubjectController@save');
  Route::get('/subject/view-subjects', 'AdminPanel\SubjectController@index');
  Route::get('/subject/data', 'AdminPanel\SubjectController@anyData');
  Route::get('/subject/delete-subject/{id?}', 'AdminPanel\SubjectController@destroy');
  Route::get('/subject/subject-status/{status?}/{id?}', 'AdminPanel\SubjectController@changeStatus');

  // Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  // Route::post('/register', 'AdminAuth\RegisterController@register');

  // For facilities @Pratyush on 19 July 2018
  Route::get('/facilities/add-facility/{id?}', 'AdminPanel\FacilityController@add');
  Route::post('/facilities/save/{id?}', 'AdminPanel\FacilityController@save');
  Route::get('/facilities/view-facilities', 'AdminPanel\FacilityController@index');
  Route::get('/facilities/data', 'AdminPanel\FacilityController@anyData');
  Route::get('/facilities/delete-facility/{id?}', 'AdminPanel\FacilityController@destroy');
  Route::get('/facilities/facility-status/{status?}/{id?}', 'AdminPanel\FacilityController@changeStatus');


  // For document category @Pratyush on 19 July 2018
  Route::get('/document-category/add-document-category/{id?}', 'AdminPanel\DocumentCategoryController@add');
  Route::post('/document-category/save/{id?}', 'AdminPanel\DocumentCategoryController@save');
  Route::get('/document-category/view-document-categories', 'AdminPanel\DocumentCategoryController@index');
  Route::get('/document-category/data', 'AdminPanel\DocumentCategoryController@anyData');
  Route::get('/document-category/delete-document-category/{id?}', 'AdminPanel\DocumentCategoryController@destroy');
  Route::get('/document-category/document-categories-status/{status?}/{id?}', 'AdminPanel\DocumentCategoryController@changeStatus');


  // For designation @Pratyush on 19 July 2018
  Route::get('/designation/add-designation/{id?}', 'AdminPanel\DesignationController@add');
  Route::post('/designation/save/{id?}', 'AdminPanel\DesignationController@save');
  Route::get('/designation/view-designations', 'AdminPanel\DesignationController@index');
  Route::get('/designation/data', 'AdminPanel\DesignationController@anyData');
  Route::get('/designation/delete-designation/{id?}', 'AdminPanel\DesignationController@destroy');
  Route::get('/designation/designation-status/{status?}/{id?}', 'AdminPanel\DesignationController@changeStatus');

  // For Title @Pratyush on 20 July 2018
  Route::get('/title/add-title/{id?}', 'AdminPanel\TitleController@add');
  Route::post('/title/save/{id?}', 'AdminPanel\TitleController@save');
  Route::get('/title/view-titles', 'AdminPanel\TitleController@index');
  Route::get('/title/data', 'AdminPanel\TitleController@anyData');
  Route::get('/title/delete-title/{id?}', 'AdminPanel\TitleController@destroy');
  Route::get('/title/title-status/{status?}/{id?}', 'AdminPanel\TitleController@changeStatus');


  // For Caste  @Pratyush on 20 July 2018
  Route::get('/caste/add-caste/{id?}', 'AdminPanel\CasteController@add');
  Route::post('/caste/save/{id?}', 'AdminPanel\CasteController@save');
  Route::get('/caste/view-caste', 'AdminPanel\CasteController@index');
  Route::get('/caste/data', 'AdminPanel\CasteController@anyData');
  Route::get('/caste/delete-caste/{id?}', 'AdminPanel\CasteController@destroy');
  Route::get('/caste/caste-status/{status?}/{id?}', 'AdminPanel\CasteController@changeStatus');

  // For Religions  @Pratyush on 20 July 2018
  Route::get('/religion/add-religion/{id?}', 'AdminPanel\ReligionController@add');
  Route::post('/religion/save/{id?}', 'AdminPanel\ReligionController@save');
  Route::get('/religion/view-religions', 'AdminPanel\ReligionController@index');
  Route::get('/religion/data', 'AdminPanel\ReligionController@anyData');
  Route::get('/religion/delete-religion/{id?}', 'AdminPanel\ReligionController@destroy');
  Route::get('/religion/religion-status/{status?}/{id?}', 'AdminPanel\ReligionController@changeStatus');

  // For Nationality @Pratyush on 20 July 2018
  Route::get('/nationality/add-nationality/{id?}', 'AdminPanel\NationalityController@add');
  Route::post('/nationality/save/{id?}', 'AdminPanel\NationalityController@save');
  Route::get('/nationality/view-nationality', 'AdminPanel\NationalityController@index');
  Route::get('/nationality/data', 'AdminPanel\NationalityController@anyData');
  Route::get('/nationality/delete-nationality/{id?}', 'AdminPanel\NationalityController@destroy');
  Route::get('/nationality/nationality-status/{status?}/{id?}', 'AdminPanel\NationalityController@changeStatus');

  // For School Group @Pratyush on 20 July 2018
  Route::get('/schoolgroup/add-group/{id?}', 'AdminPanel\SchoolGroupController@add');
  Route::post('/schoolgroup/save/{id?}', 'AdminPanel\SchoolGroupController@save');
  Route::get('/schoolgroup/view-groups', 'AdminPanel\SchoolGroupController@index');
  Route::get('/schoolgroup/data', 'AdminPanel\SchoolGroupController@anyData');
  Route::get('/schoolgroup/delete-group/{id?}', 'AdminPanel\SchoolGroupController@destroy');
  Route::get('/schoolgroup/group-status/{status?}/{id?}', 'AdminPanel\SchoolGroupController@changeStatus');


  // For Virtual Class @Pratyush on 21 July 2018 modify on 30 July 2018
  Route::get('/virtual-class/add-virtual-class/{id?}', 'AdminPanel\VirtualClassController@add');
  Route::post('/virtual-class/save/{id?}', 'AdminPanel\VirtualClassController@save');
  Route::get('/virtual-class/view-virtual-classes', 'AdminPanel\VirtualClassController@index');
  Route::get('/virtual-class/data', 'AdminPanel\VirtualClassController@anyData');
  Route::get('/virtual-class/delete-class/{id?}', 'AdminPanel\VirtualClassController@destroy');
  Route::get('/virtual-class/delete-student/{id?}', 'AdminPanel\VirtualClassController@destroyStudent');
  Route::get('/virtual-class/class-status/{status?}/{id?}', 'AdminPanel\VirtualClassController@changeStatus');
    // For Add student to virtual class
  Route::get('/virtual-class/add-student/{id?}', 'AdminPanel\VirtualClassController@studentList'); 
  Route::get('/virtual-class/student-list-data/', 'AdminPanel\VirtualClassController@studentListData');
  Route::get('/virtual-class/add-student-in-class/{classid}/{studentid}', 'AdminPanel\VirtualClassController@addStudentInClass');
    // For Delete student to virtual class
  Route::get('/virtual-class/student-list/{id?}', 'AdminPanel\VirtualClassController@studentDeleteList'); 
  Route::get('/virtual-class/student-delete-list-data/', 'AdminPanel\VirtualClassController@studentDeleteListData');
  Route::get('/virtual-class/delete-student-in-class/{classid}/{studentid}', 'AdminPanel\VirtualClassController@destroyStudent');

  // For Competitions @Pratyush on 21 July 2018
  Route::get('/competition/add-competition/{id?}', 'AdminPanel\CompetitionController@add');
  Route::post('/competition/save/{id?}', 'AdminPanel\CompetitionController@save');
  Route::get('/competition/view-competitions', 'AdminPanel\CompetitionController@index');
  Route::get('/competition/data', 'AdminPanel\CompetitionController@anyData');
  Route::get('/competition/delete-competition/{id?}', 'AdminPanel\CompetitionController@destroy');
  Route::get('/competition/competition-status/{status?}/{id?}', 'AdminPanel\CompetitionController@changeStatus');
  Route::get('/competition/competition-select-winner/{competitionid?}/{studentid?}', 'AdminPanel\CompetitionController@selectWinner');
  Route::get('/competition/competition-list-of-winner/{id?}', 'AdminPanel\CompetitionController@listWinner');
  Route::get('/competition/competition-select-winner-data', 'AdminPanel\CompetitionController@selectWinnerData');
  Route::get('/competition/competition-list-of-winner-data', 'AdminPanel\CompetitionController@listWinnerData');
  Route::get('/competition/competition-set-position', 'AdminPanel\CompetitionController@setPosition');
  Route::get('/competition/competition-delete-student/{id?}','AdminPanel\CompetitionController@destoryStudent');

  // For students
  Route::get('/student/add-student/{id?}', 'AdminPanel\StudentController@add');
  Route::post('/student/save/{id?}', 'AdminPanel\StudentController@save');
  Route::get('/student/view-list', 'AdminPanel\StudentController@index');
  Route::get('/student/list-data', 'AdminPanel\StudentController@listData');
  Route::get('/student/student-list-data', 'AdminPanel\StudentController@studentListData');
  Route::get('/student/view-students/{sectionid?}', 'AdminPanel\StudentController@view_students');
  Route::get('/student/assign-roll-no/{sectionid?}', 'AdminPanel\StudentController@assign_roll_numbers');
  Route::get('/student/student-profile/{id?}', 'AdminPanel\StudentController@student_profile');
  Route::get('/student/delete-student/{id?}', 'AdminPanel\StudentController@destroy');
  Route::get('/student/student-status/{status?}/{id?}', 'AdminPanel\StudentController@changeStatus');
  Route::get('/student/get-section-data/{id?}', 'AdminPanel\StudentController@getSectionData');
  Route::get('/student/parent-information', 'AdminPanel\StudentController@parent_information');
  Route::get('/student/parent-list', 'AdminPanel\StudentController@parentListData');
  Route::get('/student/view-parent-detail/{id?}', 'AdminPanel\StudentController@view_parent_detail');
  Route::get('/student/check-parent-existance/{mobile_no?}', 'AdminPanel\StudentController@check_parent_existance');
  Route::get('/student/edit-document/{id?}', 'AdminPanel\StudentController@edit_document'); // For document @Pratyush 10 Aug 2018
  Route::post('/student/save-document/{id?}', 'AdminPanel\StudentController@save_document'); // For document @Pratyush 10 Aug 2018
  Route::get('/student/student-document-status/{status?}/{id?}', 'AdminPanel\StudentController@changeDocumentStatus'); // For document @Pratyush 10 Aug 2018
  Route::get('/student/delete-student-document/{id?}/{type}', 'AdminPanel\StudentController@destroyDocument'); // For document @Pratyush 10 

  // For Room No @Pratyush on 28 July 2018
  Route::get('/room-no/add-room-no/{id?}', 'AdminPanel\RoomNoController@add');
  Route::post('/room-no/save/{id?}', 'AdminPanel\RoomNoController@save');
  Route::get('/room-no/view-room-no', 'AdminPanel\RoomNoController@index');
  Route::get('/room-no/data', 'AdminPanel\RoomNoController@anyData');
  Route::get('/room-no/delete-room-no/{id?}', 'AdminPanel\RoomNoController@destroy');
  Route::get('/room-no/room-no-status/{status?}/{id?}', 'AdminPanel\RoomNoController@changeStatus');

  
  // For Staff Roles
  Route::get('/staff/staff-role/add-staff-role/{id?}', 'AdminPanel\StaffRolesController@add');
  Route::post('/staff/staff-role/save/{id?}', 'AdminPanel\StaffRolesController@save');
  Route::get('/staff/staff-role/view-staff-roles', 'AdminPanel\StaffRolesController@index');
  Route::get('/staff/staff-role/data', 'AdminPanel\StaffRolesController@anyData');
  Route::get('/staff/staff-role/delete-staff-role/{id?}', 'AdminPanel\StaffRolesController@destroy');
  Route::get('/staff/staff-role/staff-role-status/{status?}/{id?}', 'AdminPanel\StaffRolesController@changeStatus');

  // For Staff
  Route::get('/staff/add-staff/{id?}', 'AdminPanel\StaffController@add');
  Route::post('/staff/save/{id?}', 'AdminPanel\StaffController@save');
  Route::get('/staff/view-staff', 'AdminPanel\StaffController@index');
  Route::get('/staff/data', 'AdminPanel\StaffController@anyData');
  Route::get('/staff/delete-staff/{id?}', 'AdminPanel\StaffController@destroy');
  Route::get('/staff/staff-status/{status?}/{id?}', 'AdminPanel\StaffController@changeStatus');
  Route::get('/staff/view-profile', 'AdminPanel\StaffController@viewProfile');

  // For Notes @Ashish on 31 July 2018
  Route::get('/notes/add-notes/{id?}', 'AdminPanel\NoteController@add');
  Route::post('/notes/save/{id?}', 'AdminPanel\NoteController@save');
  Route::get('/notes/view-notes', 'AdminPanel\NoteController@index');
  Route::get('/notes/data', 'AdminPanel\NoteController@anyData');
  Route::get('/notes/delete-notes/{id?}', 'AdminPanel\NoteController@destroy');
  Route::get('/notes/notes-status/{status?}/{id?}', 'AdminPanel\NoteController@changeStatus');
  Route::get('/notes/get-section-data/{id?}', 'AdminPanel\NoteController@getSectionData');

  // For Class Teacher Allocation @Pratyush on 06 Aug 2018
  Route::get('/class-teacher-allocation/allocate-class/{id?}', 'AdminPanel\ClassTeacherAllocationController@add');
  Route::post('/class-teacher-allocation/save/{id?}', 'AdminPanel\ClassTeacherAllocationController@save'); // Allot Teacher
  Route::get('/class-teacher-allocation/view-allocations', 'AdminPanel\ClassTeacherAllocationController@index');
  Route::get('/class-teacher-allocation/data', 'AdminPanel\ClassTeacherAllocationController@anyData');
  Route::get('/class-teacher-allocation/delete-teacher/{id?}', 'AdminPanel\ClassTeacherAllocationController@destroy');
  Route::get('/class-teacher-allocation/get-section-data/{id?}', 'AdminPanel\ClassTeacherAllocationController@getSectionData');

  // For Class Teacher Allocation @Pratyush on 06 Aug 2018
  Route::get('/class-subject-mapping/map-subject-class/{id?}', 'AdminPanel\SubjectClassmappingController@add');
  Route::post('/class-subject-mapping/save/{id?}', 'AdminPanel\SubjectClassmappingController@save'); // Allot Teacher
  Route::get('/class-subject-mapping/view-subject-class-mapping', 'AdminPanel\SubjectClassmappingController@index');
  Route::get('/class-subject-mapping/subject-class-mapping-data', 'AdminPanel\SubjectClassmappingController@anyData');
  Route::get('/class-subject-mapping/get-section-data/{id?}', 'AdminPanel\SubjectClassmappingController@getSectionData');
  Route::get('/class-subject-mapping/map-subject/{id?}', 'AdminPanel\SubjectClassmappingController@getMapSubject');
  Route::get('/class-subject-mapping/map-subject-data/{id?}', 'AdminPanel\SubjectClassmappingController@anyMapSubjectData');
  Route::get('/class-subject-mapping/view-report/{id?}', 'AdminPanel\SubjectClassmappingController@getViewReport');
  Route::get('/class-subject-mapping/view-report-data/{id?}', 'AdminPanel\SubjectClassmappingController@anyViewReportData');

  // For Class Teacher Allocation @Pratyush on 06 Aug 2018
  Route::get('/teacher-subject-mapping/map-subject-teacher/{id?}', 'AdminPanel\SubjectTeachermappingController@add');
  Route::get('/teacher-subject-mapping/save/{id?}', 'AdminPanel\SubjectTeachermappingController@save'); // Allot Teacher
  Route::get('/teacher-subject-mapping/view-subject-teacher-mapping', 'AdminPanel\SubjectTeachermappingController@index');
  Route::get('/teacher-subject-mapping/subject-teacher-mapping-data', 'AdminPanel\SubjectTeachermappingController@anyData');
  Route::get('/teacher-subject-mapping/get-section-data/{id?}', 'AdminPanel\SubjectTeachermappingController@getSectionData');
  Route::get('/teacher-subject-mapping/map-teacher/{id?}', 'AdminPanel\SubjectTeachermappingController@getMapSubject');
  Route::get('/teacher-subject-mapping/map-teacher-data/{id?}', 'AdminPanel\SubjectTeachermappingController@anyMapSubjectData');
  Route::get('/teacher-subject-mapping/view-report/{id?}', 'AdminPanel\SubjectTeachermappingController@getViewReport');
  Route::get('/teacher-subject-mapping/view-report-data/{id?}', 'AdminPanel\SubjectTeachermappingController@anyViewReportData');
  // Route::get('/teacher-subject-mapping/delete-all-teacher/{id?}', 'AdminPanel\SubjectTeachermappingController@destroy');


  // For Student Leave Application @Pratyush on 10 Aug 2018
  Route::get('/student-leave-application/add-leave-application/{id?}', 'AdminPanel\StudentLeaveApplicationController@add');
  Route::post('/student-leave-application/save/{id?}', 'AdminPanel\StudentLeaveApplicationController@save');
  Route::get('/student-leave-application/view-leave-application', 'AdminPanel\StudentLeaveApplicationController@index');
  Route::get('/student-leave-application/data', 'AdminPanel\StudentLeaveApplicationController@anyData');
  Route::get('/student-leave-application/delete/{id?}', 'AdminPanel\StudentLeaveApplicationController@destroy');
  Route::get('/student-leave-application/edit/{id?}', 'AdminPanel\StudentLeaveApplicationController@add');
  Route::get('/student-leave-application/student-leave-application-status/{status?}/{id?}', 'AdminPanel\StudentLeaveApplicationController@changeStatus');


  // For Stream @Pratyush on 11 Aug 2018
  Route::get('/stream/add-stream/{id?}', 'AdminPanel\StreamController@add');
  Route::post('/stream/save/{id?}', 'AdminPanel\StreamController@save');
  Route::get('/stream/view-streams', 'AdminPanel\StreamController@index');
  Route::get('/stream/data', 'AdminPanel\StreamController@anyData');
  Route::get('/stream/delete-stream/{id?}', 'AdminPanel\StreamController@destroy');
  Route::get('/stream/stream-status/{status?}/{id?}', 'AdminPanel\StreamController@changeStatus');

  // For Term @Pratyush on 11 Aug 2018
  Route::get('/examination/manage-term/{id?}', 'AdminPanel\TermController@add');
  Route::post('/examination/save-term/{id?}', 'AdminPanel\TermController@save');
  // Route::get('/term/view-terms', 'AdminPanel\TermController@index');
  Route::get('/examination/data-term', 'AdminPanel\TermController@anyData');
  Route::get('/examination/delete-term/{id?}', 'AdminPanel\TermController@destroy');
  Route::get('/examination/term-status/{status?}/{id?}', 'AdminPanel\TermController@changeStatus');

  // For Exam @Pratyush on 11 Auclass_nameg 2018
  Route::get('/examination/manage-exam/{id?}', 'AdminPanel\ExamController@add');
  Route::post('/examination/save-exam/{id?}', 'AdminPanel\ExamController@save');
  // Route::get('/exam/view-exams', 'AdminPanel\ExamController@index');
  Route::get('/examination/data-exam', 'AdminPanel\ExamController@anyData');
  Route::get('/examination/delete-exam/{id?}', 'AdminPanel\ExamController@destroy');
  Route::get('/examination/exam-status/{status?}/{id?}', 'AdminPanel\ExamController@changeStatus');

  // For Book Category @Pratyush on 13 Aug 2018
  Route::get('/book-category/add-book-category/{id?}', 'AdminPanel\BookCategoryController@add');
  Route::post('/book-category/save/{id?}', 'AdminPanel\BookCategoryController@save');
  Route::get('/book-category/view-book-categories', 'AdminPanel\BookCategoryController@index');
  Route::get('/book-category/data', 'AdminPanel\BookCategoryController@anyData');
  Route::get('/book-category/delete-book-category/{id?}', 'AdminPanel\BookCategoryController@destroy');
  Route::get('/book-category/book-category-status/{status?}/{id?}', 'AdminPanel\BookCategoryController@changeStatus');

  // For Book Allowance @Pratyush on 13 Aug 2018
  Route::get('/book-allowance/edit-book-category/{id?}', 'AdminPanel\BookAllowanceController@add');
  Route::post('/book-allowance/save/{id?}', 'AdminPanel\BookAllowanceController@save');

  // For Book Allowance @Pratyush on 13 Aug 2018
  Route::get('/book-vendor/add-book-vendor/{id?}', 'AdminPanel\BookVendorController@add');
  Route::post('/book-vendor/save/{id?}', 'AdminPanel\BookVendorController@save');
  Route::get('/book-vendor/view-book-vendors', 'AdminPanel\BookVendorController@index');
  Route::get('/book-vendor/data', 'AdminPanel\BookVendorController@anyData');
  Route::get('/book-vendor/delete-book-vendor/{id?}', 'AdminPanel\BookVendorController@destroy');
  Route::get('/book-vendor/book-vendor-status/{status?}/{id?}', 'AdminPanel\BookVendorController@changeStatus');

  // For Book CupBoard @Pratyush on 13 Aug 2018
  Route::get('/book-cupboard/add-book-cupboard/{id?}', 'AdminPanel\BookCupboardController@add');
  Route::post('/book-cupboard/save/{id?}', 'AdminPanel\BookCupboardController@save');
  Route::get('/book-cupboard/view-book-cupboard', 'AdminPanel\BookCupboardController@index');
  Route::get('/book-cupboard/data', 'AdminPanel\BookCupboardController@anyData');
  Route::get('/book-cupboard/delete-book-cupboard/{id?}', 'AdminPanel\BookCupboardController@destroy');
  Route::get('/book-cupboard/book-cupboard-status/{status?}/{id?}', 'AdminPanel\BookCupboardController@changeStatus');

  // For Book CupBoard @Pratyush on 13 Aug 2018
  Route::get('/cupboard-shelf/add-cupboard-shelf/{id?}', 'AdminPanel\BookCupboardshelfController@add');
  Route::post('/cupboard-shelf/save/{id?}', 'AdminPanel\BookCupboardshelfController@save');
  Route::get('/cupboard-shelf/view-cupboard-shelf', 'AdminPanel\BookCupboardshelfController@index');
  Route::get('/cupboard-shelf/data', 'AdminPanel\BookCupboardshelfController@anyData');
  Route::get('/cupboard-shelf/delete-cupboard-shelf/{id?}', 'AdminPanel\BookCupboardshelfController@destroy');
  Route::get('/cupboard-shelf/cupboard-shelf-status/{status?}/{id?}', 'AdminPanel\BookCupboardshelfController@changeStatus');

  // For Book CupBoard @Pratyush on 13 Aug 2018
  Route::get('/book/add-book/{id?}', 'AdminPanel\BookController@add');
  Route::post('/book/save/{id?}', 'AdminPanel\BookController@save');
  Route::get('/book/view-books', 'AdminPanel\BookController@index');
  Route::get('/book/view-book-details/{id?}', 'AdminPanel\BookController@displayBook');
  Route::get('/book/data', 'AdminPanel\BookController@anyData');
  Route::get('/book/single-book-data', 'AdminPanel\BookController@singleBookData');
  Route::get('/book/delete-book/{id?}', 'AdminPanel\BookController@destroy');
  Route::get('/book/delete-single-book/{id?}', 'AdminPanel\BookController@destroySingleBook');
  Route::get('/book/book-status/{status?}/{id?}', 'AdminPanel\BookController@changeStatus');
  Route::get('/book/get-cupboard-shelf-data/{id?}', 'AdminPanel\BookController@getCupboardShelfData');

  // For Library Mamber @Pratyush on 19 Aug 2018
  // -- Student
  Route::get('/member/register-student/{id?}', 'AdminPanel\LibraryMemberController@addStudent');
  Route::get('/member/register-student-data/{id?}', 'AdminPanel\LibraryMemberController@anyStudnetData');
  Route::any('/member/save-students/{id?}', 'AdminPanel\LibraryMemberController@saveStudent');
  Route::get('/member/view-students', 'AdminPanel\LibraryMemberController@viewStudent');
  Route::get('/member/view-students-data', 'AdminPanel\LibraryMemberController@viewStudentData');
  Route::get('/member/view-students/issued-books/{id?}', 'AdminPanel\LibraryMemberController@studentIssuedBooks');
  Route::get('/member/view-students/issued-books-data', 'AdminPanel\LibraryMemberController@studentIssuedBooksData');


  // -- Sfaff
  Route::get('/member/register-staff/{id?}', 'AdminPanel\LibraryMemberController@addStaff');
  Route::get('/member/register-staff-data/{id?}', 'AdminPanel\LibraryMemberController@anyStaffData');
  Route::any('/member/save-staff/{id?}', 'AdminPanel\LibraryMemberController@saveStaff');
  Route::get('/member/view-staff', 'AdminPanel\LibraryMemberController@viewStaff');
  Route::get('/member/view-staff-data', 'AdminPanel\LibraryMemberController@viewStaffData');
  Route::get('/member/view-staff/issued-books/{id?}', 'AdminPanel\LibraryMemberController@staffIssuedBooks');
  Route::get('/member/view-staff/issued-books-data', 'AdminPanel\LibraryMemberController@staffIssuedBooksData');

  // For Library Book Issue @Pratyush on 19 Aug 2018
  Route::get('/issue-book', 'AdminPanel\BookIssueController@index');  
  Route::get('/issue-book-data', 'AdminPanel\BookIssueController@anyData');
  Route::get('/issue-book/books/{id?}', 'AdminPanel\BookIssueController@displayBooks');  
  Route::get('/issue-book-data/books-data', 'AdminPanel\BookIssueController@displayBooksData');
  Route::post('/issue-book/issue-member-book/{id?}', 'AdminPanel\BookIssueController@issueBook');

  // For Question Paper @Ashish on 21 Aug 2018
  Route::get('/question-paper/add-question-paper/{id?}', 'AdminPanel\QuestionPaperController@add');
  Route::post('/question-paper/save/{id?}', 'AdminPanel\QuestionPaperController@save');
  Route::get('/question-paper/view-question-paper', 'AdminPanel\QuestionPaperController@index');
  Route::get('/question-paper/data', 'AdminPanel\QuestionPaperController@anyData');
  Route::get('/question-paper/delete-question-paper/{id?}', 'AdminPanel\QuestionPaperController@destroy');
  Route::get('/question-paper/question-paper-status/{status?}/{id?}', 'AdminPanel\QuestionPaperController@changeStatus');
  Route::get('/question-paper/get-section-data/{id?}', 'AdminPanel\QuestionPaperController@getSectionData');
  Route::get('/question-paper/get-subject-data/{id?}', 'AdminPanel\QuestionPaperController@getSubjectData');


  // For Library Book return / Renew / Update @Pratyush on 19 Aug 2018
  Route::get('/return-book', 'AdminPanel\BookIssueController@showReturnBookPage');  
  Route::get('/return-book-data', 'AdminPanel\BookIssueController@ReturnBookPageData');
  Route::get('/return-book/return-book/{id?}', 'AdminPanel\BookIssueController@returnBook');
  Route::post('/return-book/renew-book', 'AdminPanel\BookIssueController@renewBook');
  Route::post('/return-book/update-book', 'AdminPanel\BookIssueController@updateBook');

  // For Library Fine @Pratyush on 11 Aug 2018
  Route::get('library-fine/get-member/{id?}', 'AdminPanel\LibraryFineController@getMemberDetail');
  Route::get('library-fine', 'AdminPanel\LibraryFineController@add');
  Route::post('library-fine/save/{id?}', 'AdminPanel\LibraryFineController@save');
  Route::get('library-fine/data', 'AdminPanel\LibraryFineController@anyData');
  Route::get('library-fine/delete/{id?}', 'AdminPanel\LibraryFineController@destroy');
  Route::get('library-fine/status/{status?}/{id?}', 'AdminPanel\LibraryFineController@changeStatus');

  //For Time Table @Khushbu on 06 Sept 2018
  Route::get('/time-table/add-time-table/{id?}', 'AdminPanel\TimeTableController@add');
  Route::post('/time-table/save/{id?}', 'AdminPanel\TimeTableController@save');
  Route::get('/time-table/view-time-table', 'AdminPanel\TimeTableController@index');
  Route::get('/time-table/data', 'AdminPanel\TimeTableController@anyData');
  Route::get('/time-table/delete-time-table/{id?}', 'AdminPanel\TimeTableController@destroy');
  Route::get('/time-table/time-table-status/{status?}/{id?}', 'AdminPanel\TimeTableController@changeStatus');

  //For Ajax
  Route::get('/section/get-class-section-data/{class_id?}', 'AdminPanel\SectionController@getClassSectionData');
  // Set Time Table
  Route::get('/time-table/set-time-table', 'AdminPanel\TimeTableController@showTimeTable');
  Route::get('/time-table/view-time-table/data', 'AdminPanel\TimeTableController@setTimeTableData');

  //For Country @Khushbu on 08 Sept 2018
  Route::get('/country/add-country/{id?}', 'AdminPanel\CountryController@add');
  Route::post('/country/save/{id?}', 'AdminPanel\CountryController@save');
  Route::get('/country/view-country', 'AdminPanel\CountryController@index');
  Route::get('/country/data', 'AdminPanel\CountryController@anyData');
  Route::get('/country/country-status/{status?}/{id?}', 'AdminPanel\CountryController@changeStatus');
  Route::get('/country/delete-country/{id?}', 'AdminPanel\CountryController@destroy');

  //For State @Khushbu on 08 Sept 2018
  Route::get('/state/add-state/{id?}', 'AdminPanel\StateController@add');
  Route::post('/state/save/{id?}', 'AdminPanel\StateController@save');
  Route::get('/state/view-state', 'AdminPanel\StateController@index');
  Route::get('/state/data', 'AdminPanel\StateController@anyData');
  Route::get('/state/state-status/{status?}/{id?}', 'AdminPanel\StateController@changeStatus');
  Route::get('/state/delete-state/{id?}', 'AdminPanel\StateController@destroy');

  //For City @Khushbu on 08 Sept 2018
  Route::get('/city/add-city/{id?}', 'AdminPanel\CityController@add');
  Route::post('/city/save/{id?}', 'AdminPanel\CityController@save');
  Route::get('/city/view-city', 'AdminPanel\CityController@index');
  Route::get('/city/data', 'AdminPanel\CityController@anyData');
  Route::get('/city/city-status/{status?}/{id?}', 'AdminPanel\CityController@changeStatus');
  Route::get('/city/delete-city/{id?}', 'AdminPanel\CityController@destroy');
  
  //For Ajax
  Route::get('/state/get-country-state-data/{country_id?}', 'AdminPanel\StateController@showStates');

  // For brochure module on 10 Sept 2018
  Route::get('/brochure/add-brochure/{id?}', 'AdminPanel\BrochureController@add');
  Route::post('/brochure/save/{id?}', 'AdminPanel\BrochureController@save');
  Route::get('/brochure/view-brochures', 'AdminPanel\BrochureController@index');
  Route::get('/brochure/data', 'AdminPanel\BrochureController@anyData');
  Route::get('/brochure/delete-brochure/{id?}', 'AdminPanel\BrochureController@destroy');
  Route::get('/brochure/brochure-status/{status?}/{id?}', 'AdminPanel\BrochureController@changeStatus');

  // For Admission form module on 11 Sept 2018
  Route::get('/admission-form/add-admission-form/{id?}', 'AdminPanel\AdmissionController@add');
  Route::post('/admission-form/save/{id?}', 'AdminPanel\AdmissionController@save');
  Route::get('/admission-form/view-admission-forms', 'AdminPanel\AdmissionController@index');
  Route::get('/admission-form/data', 'AdminPanel\AdmissionController@anyData');
  Route::get('/admission-form/manage-fields/{id?}', 'AdminPanel\AdmissionController@manageFieldsView');
  Route::get('/admission-form/delete-admission-form/{id?}', 'AdminPanel\AdmissionController@destroy');
  Route::get('/admission-form/admission-form-status/{status?}/{id?}', 'AdminPanel\AdmissionController@changeStatus');

  //For job module @Khushbu on 15 Sept 2018
  Route::get('/recruitment/add-job/{id?}', 'AdminPanel\JobController@add');
  // Route::post('/recruitment/save/{id?}', 'AdminPanel\JobController@save');
  Route::get('/recruitment/view-job', 'AdminPanel\JobController@index');
  //Route::get('/recruitment/data', 'AdminPanel\JobController@anyData');
  Route::get('/recruitment/delete-job/{id?}', 'AdminPanel\JobController@destroy');
  Route::get('/recruitment/job-status/{status?}/{id?}', 'AdminPanel\JobController@changeStatus');

  //For View Applied Candidate
  Route::get('/recruitment/view-applied-candidate', 'AdminPanel\JobController@viewAppliedCandidate');

  //For View Interview Schedule @Khushbu on 15 Sept 2018
  Route::get('/recruitment/view-interview-schedule', 'AdminPanel\JobController@viewInterviewScheduleData');

  //For View Candidate List @Khushbu on 15 Sept 2018
  Route::get('/recruitment/view-job-list', 'AdminPanel\JobController@viewCandidateListData');
  Route::get('/recruitment/view-candidate', 'AdminPanel\JobController@viewCandidateData');

  //For job wise @Khushbu on 15 Sept 2018
  Route::get('/recruitment/view-report-job-wise', 'AdminPanel\JobController@jobWiseData');
  Route::get('/recruitment/view-candidate-record', 'AdminPanel\JobController@viewCandidateRecord');


  // For Menu url @Khushbu on 18 Sept 2018
  Route::get('/menu/configuration', 'AdminPanel\MenuController@configuration');
  Route::get('/menu/academic', 'AdminPanel\MenuController@academic');
  Route::get('/menu/admission', 'AdminPanel\MenuController@admission'); 
  Route::get('/menu/student', 'AdminPanel\MenuController@student');  
  // Route::get('/menu/subject', 'AdminPanel\MenuController@subject');  
  Route::get('/menu/recruitment', 'AdminPanel\MenuController@recruitment'); 

  Route::get('/menu/examination', 'AdminPanel\MenuController@examination');
  Route::get('/menu/fees-collection', 'AdminPanel\MenuController@feesCollection');
  Route::get('/menu/staff', 'AdminPanel\MenuController@staff');
  Route::get('/menu/hostel', 'AdminPanel\MenuController@hostel');
  Route::get('/menu/library', 'AdminPanel\MenuController@library');
  Route::get('/menu/transport', 'AdminPanel\MenuController@transport');
  Route::get('/menu/visitor', 'AdminPanel\MenuController@visitor');
  Route::get('/menu/notice-board', 'AdminPanel\MenuController@noticeBoard');
  Route::get('/menu/online-content', 'AdminPanel\MenuController@onlineContent');
  Route::get('/menu/task-manager', 'AdminPanel\MenuController@taskManager');
  Route::get('/menu/substitute-management', 'AdminPanel\MenuController@substituteManagement'); 
  Route::get('/menu/inventory', 'AdminPanel\MenuController@inventory'); 
  Route::get('/menu/account', 'AdminPanel\MenuController@account');
  Route::get('/menu/payroll', 'AdminPanel\MenuController@payroll'); 
  Route::get('/menu/certificate', 'AdminPanel\MenuController@certificate'); 


  // For Staff Attendance
  Route::get('/staff/staff-attendance/add-staff-attendance/{id?}', 'AdminPanel\StaffAttendanceController@add');
  Route::post('/staff/staff-attendance/save', 'AdminPanel\StaffAttendanceController@save');
  Route::get('/staff/staff-attendance/view-staff-attendance', 'AdminPanel\StaffAttendanceController@index');
  Route::get('/staff/staff-attendance/edit-staff-attendance', 'AdminPanel\StaffAttendanceController@editAttendance');
  Route::get('/staff/staff-attendance/view-profile', 'AdminPanel\StaffAttendanceController@viewProfile');

  // For Staff Leave management
  Route::get('/staff/staff-leave/add-staff-leave/{id?}', 'AdminPanel\StaffLeaveController@add');
  Route::post('/staff/staff-leave/save', 'AdminPanel\StaffLeaveController@save');
  Route::get('/staff/staff-leave/view-staff-leave', 'AdminPanel\StaffLeaveController@index');

  //For One Time
  Route::get('/fees-collection/one-time/add-one-time/{id?}', 'AdminPanel\OneTimeController@add');
  // Route::post('/fees-collection/one-time/save', 'AdminPanel\OneTimeController@save');
  Route::get('/fees-collection/one-time/view-one-time', 'AdminPanel\OneTimeController@index');

  //For recurring heads
  Route::get('/fees-collection/recurring-heads/add-recurring-head/{id?}', 'AdminPanel\RecurringHeadsController@add');
  // Route::post('/fees-collection/recurring-heads/save', 'AdminPanel\RecurringHeadsController@save');
  Route::get('/fees-collection/recurring-heads/view-recurring-heads', 'AdminPanel\RecurringHeadsController@index');

  //For Map Free Student
  Route::get('/fees-collection/view-rates', 'AdminPanel\MapFreeStudentController@viewFreeByRate');
  Route::get('/fees-collection/view-free-by-management', 'AdminPanel\MapFreeStudentController@viewFreeByManagement');
  // For Map Heads to Student
  Route::get('/fees-collection/view-map-student', 'AdminPanel\MapHeadsToStudentController@viewMapStudent');
  Route::get('/fees-collection/view-map-student-details', 'AdminPanel\MapHeadsToStudentController@viewMapStudentDetails');

  //For Concession
  Route::get('/fees-collection/concession/view-concession-details', 'AdminPanel\ConcessionController@viewConcessionDetails');

  //For Fee Counter
  Route::get('/fees-collection/fee-counter/add-fee-counter', 'AdminPanel\FeeCounterController@feeCounter');

   //For Cheque Details
  Route::get('/fees-collection/cheque-details/view-cheque-details', 'AdminPanel\ChequeDetailsController@viewChequesDetails');

  //For RTE Collection
  Route::get('/fees-collection/rte-collection/rte-fee-head', 'AdminPanel\RteCollectionController@rteFeesHead');
  Route::get('/fees-collection/rte-collection/apply-fees-on-head', 'AdminPanel\RteCollectionController@applyFeesOnHead');

  //For RTE Received Cheque
  Route::get('/fees-collection/rte-received-cheque/add-rte-cheque-detail', 'AdminPanel\RteReceivedChequeController@rteChequeDetail');
  Route::get('/fees-collection/rte-received-cheque/view-rte-cheque-detail', 'AdminPanel\RteReceivedChequeController@viewRteChequeDetail');

  //For Fees Receipt
  Route::get('/fees-collection/fees-receipt/view-fees-receipt', 'AdminPanel\FeesReceiptController@feesReceipt');

  //For Prepaid Account
  Route::get('/fees-collection/prepaid-account/add-prepaid-account/{id?}', 'AdminPanel\PrepaidAccountController@add');
  // Route::post('/fees-collection/prepaid-account/save', 'AdminPanel\PrepaidAccountController@save');
  Route::get('/fees-collection/prepaid-account/manage-account', 'AdminPanel\PrepaidAccountController@index');
  Route::get('/fees-collection/prepaid-account/manage-amount', 'AdminPanel\PrepaidAccountController@manageAmount');
  Route::get('/fees-collection/prepaid-account/common-entry', 'AdminPanel\PrepaidAccountController@commonEntry');

  // For Fee Collection Reports
  Route::get('/fees-collection/report/view-report', 'AdminPanel\FeesCollectionReportController@viewReport');

  // For Vehicle 
   Route::get('/transport/vehicle/add-vehicle/{id?}', 'AdminPanel\VehicleController@add');
  // Route::post('/transport/vehicle/save', 'AdminPanel\VehicleController@save');
  Route::get('/transport/vehicle/manage-vehicle', 'AdminPanel\VehicleController@index');

  // For Vehicle Documents
  Route::get('/transport/vehicle-documents/add-vehicle-document/{id?}', 'AdminPanel\VehicleDocumentController@add');
  Route::get('/transport/vehicle-documents/view-vehicle-documents', 'AdminPanel\VehicleDocumentController@index');

  // For Vehicle Routes
  Route::get('/transport/vehicle-route/add-vehicle-route/{id?}', 'AdminPanel\VehicleRoutesController@add');
  Route::get('/transport/vehicle-route/view-vehicle-routes', 'AdminPanel\VehicleRoutesController@index');

  // For Map Route Vehicle
  Route::get('/transport/vehicle-route/map-route-vehicle', 'AdminPanel\VehicleRoutesController@mapRouteVehicle');

  // For Driver
  Route::get('/transport/driver/add-driver/{id?}', 'AdminPanel\DriverController@add');
  Route::get('/transport/driver/view-driver', 'AdminPanel\DriverController@index');

  // For Conductor
  Route::get('/transport/conductor/add-conductor/{id?}', 'AdminPanel\ConductorController@add');
  Route::get('/transport/conductor/view-conductor', 'AdminPanel\ConductorController@index');

  //For Assign-driver-conductor
  Route::get('/transport/assign-driver-conductor/manage-driver-conductor', 'AdminPanel\AssignDriverConductorController@manageDriverConductor');  

  // For One Way Transport Controller
  Route::get('/transport/one-way-transport/request', 'AdminPanel\OneWayTransportController@request');
  Route::get('/transport/one-way-transport/response', 'AdminPanel\OneWayTransportController@response');

  //For Track Vehicle
  Route::get('/transport/track-vehicle/add-track-vehicle/{id?}', 'AdminPanel\TrackVehicleController@add');
  // Route::post('/transport/track-vehicle/save', 'AdminPanel\TrackVehicleController@save');
  Route::get('/transport/track-vehicle/view-track-vehicle', 'AdminPanel\TrackVehicleController@index');

  // For History Tracking
  Route::get('/transport/history-tracking/view-history-tracking', 'AdminPanel\HistoryTrackingController@viewHistoryTracking');

  //For Manage Fees
   Route::get('/transport/manage-fees/add-fees-head/{id?}', 'AdminPanel\ManageFeesController@add');
  Route::get('/transport/manage-fees/view-fees-head', 'AdminPanel\ManageFeesController@index');

  // For Check Student Attendance 
   Route::get('/transport/student-attendance/student-attendance/{id?}', 'AdminPanel\CheckStudentAttendanceController@add');
  Route::get('/transport/student-attendance/view-student-attendance', 'AdminPanel\CheckStudentAttendanceController@index');

  // For Transport Report Url
  Route::get('/transport/transport-report/check-vehicle-report', 'AdminPanel\TransportReportController@checkVehicleReport');  
  Route::get('/transport/transport-report/view-fees-report', 'AdminPanel\TransportReportController@viewFeesReport'); 
  Route::get('/transport/transport-report/view-staff-attendence-report', 'AdminPanel\TransportReportController@viewStaffAttendanceReport'); 

  // For Visitor List
  Route::get('/visitor/add-visitor/{id?}', 'AdminPanel\VisitorListController@add');
  Route::get('/visitor/view-visitor', 'AdminPanel\VisitorListController@index');

  // For Hostel-Configuration 
  Route::get('/hostel/configuration/hostel-details', 'AdminPanel\HostelDetailsController@hostelDetails');
  Route::get('/hostel/configuration/hostel-block', 'AdminPanel\HostelBlockController@hostelBlock');
  Route::get('/hostel/configuration/room-details', 'AdminPanel\RoomDetailsController@roomDetails');

  //For Register Student
  Route::get('/hostel/register-student/view', 'AdminPanel\RegisterStudentController@registerStudent');

  //For Allocate Room
  Route::get('/hostel/allocate-room/add', 'AdminPanel\AllocateRoomController@allocateRoom');

  //For Transferred Student
  Route::get('/hostel/transfer-student/add', 'AdminPanel\TransferStudentController@transferStudent');  

  //For Leave Room
  Route::get('/hostel/leave-room/view', 'AdminPanel\LeaveRoomController@leaveRoom'); 

  //For Collect Hostel Fees
  Route::get('/hostel/hostel-fees/view', 'AdminPanel\HostelFeesController@hostelFees');

  //For Hostel Reports
  Route::get('/hostel/reports/register-student-report', 'AdminPanel\HostelReportsController@registerStudentReport'); 
  Route::get('/hostel/reports/allocation-report', 'AdminPanel\HostelReportsController@allocationReport');  
  Route::get('/hostel/reports/leave-student-report', 'AdminPanel\HostelReportsController@leaveStudentReport');
  Route::get('/hostel/reports/free-space-room-report', 'AdminPanel\HostelReportsController@freeSpaceRoomReport');
  Route::get('/hostel/reports/fees-due-report', 'AdminPanel\HostelReportsController@feesDueReport');


  // For Grade Scheme
   Route::get('/examination/grade-scheme/add-grade-scheme', 'AdminPanel\GradeSchemeController@addGradeScheme'); 
    Route::get('/examination/grade-scheme/view-grade-scheme', 'AdminPanel\GradeSchemeController@viewGradeScheme'); 

    //For manage Terms
    // Route::get('/examination/Manage-terms/add', 'AdminPanel\TermController@add'); 

    //For Marks-Criteria
    Route::get('/examination/marks-criteria/add', 'AdminPanel\MarksCriteriaController@add'); 

    //For map exam to class subjects
    Route::get('/examination/map-exam-to-class-subjects/add', 'AdminPanel\MapExamToClassSubjectsController@add');

    Route::get('/examination/map-exam-to-class-subjects/add-class', 'AdminPanel\MapExamToClassSubjectsController@addClass');
    Route::get('/examination/map-exam-to-class-subjects/mapped-class', 'AdminPanel\MapExamToClassSubjectsController@mappedClass');
    Route::get('/examination/map-exam-to-class-subjects/view', 'AdminPanel\MapExamToClassSubjectsController@viewClass');

    //For map marks criteria to subjects
    Route::get('/examination/map-mark-criteria-to-subjects/add', 'AdminPanel\MapMarksCriteriaToSubjectsController@addMarksCriteria');
    Route::get('/examination/map-mark-criteria-to-subjects/view', 'AdminPanel\MapMarksCriteriaToSubjectsController@viewMarksCriteria');

    //For map Grade scheme to subjects
    Route::get('/examination/map-grade-scheme-to-subjects/add', 'AdminPanel\MapGradeSchemeToSubjectsController@addGradeScheme');
    Route::get('/examination/map-grade-scheme-to-subjects/view', 'AdminPanel\MapGradeSchemeToSubjectsController@viewGradeScheme');

    //For Exam Schedule
    Route::get('/examination/exam-schedule/add','AdminPanel\ExamScheduleController@examSchedule');
    Route::get('/examination/exam-schedule/view-schedule','AdminPanel\ExamScheduleController@viewSchedule');  
    Route::get('/examination/exam-schedule/add-schedule','AdminPanel\ExamScheduleController@addSchedule');
    Route::get('/examination/exam-schedule/manage-class','AdminPanel\ExamScheduleController@manageClass');

    // For Marks
    Route::get('/examination/marks/add','AdminPanel\MarksController@addMarks');
    Route::get('/examination/marks/view','AdminPanel\MarksController@viewMarks');

    // For Notice
    Route::get('/notice-board/add-notice','AdminPanel\NoticeController@addNotice');
    Route::get('/notice-board/view-notice','AdminPanel\NoticeController@viewNotice');

    // For Task Manager
    Route::get('/task-manager/add-task','AdminPanel\TaskManagerController@add');
    Route::get('/task-manager/view-task','AdminPanel\TaskManagerController@viewTask');
    Route::get('/task-manager/view-task/view-responses','AdminPanel\TaskManagerController@viewResponses');
    Route::get('/task-manager/view-task/mapping','AdminPanel\TaskManagerController@mapping');

    // For Substitute management
    Route::get('/substitute-management/manage-substitute','AdminPanel\SubstituteController@manageSubstitute');
    Route::get('/substitute-management/substitute-schedule','AdminPanel\SubstituteController@substituteSchedule');
    Route::get('/substitute-management/substitute-schedule/allocate-teacher','AdminPanel\SubstituteController@allocateTeacher');
    Route::get('/substitute-management/view','AdminPanel\SubstituteController@viewAllocateTeacher');


    // For Substitude Report
    Route::get('/substitute-management/absent-teacher-report','AdminPanel\SubstituteReportController@absentTeacherReport');
    Route::get('/substitute-management/absent-teacher/schedule-teacher-report','AdminPanel\SubstituteReportController@scheduleTeacherReport');

    Route::get('/substitute-management/substitute-teacher-report','AdminPanel\SubstituteReportController@substituteTeacherReport');
    Route::get('/substitute-management/substitute-teacher/schedule-teacher-report','AdminPanel\SubstituteReportController@scheduleTeacherReport');

    Route::get('/substitute-management/form-to-date-report','AdminPanel\SubstituteReportController@formToDateReport');
    Route::get('/substitute-management/form-to-date/schedule-teacher-report','AdminPanel\SubstituteReportController@scheduleTeacherReport');

    Route::get('/substitute-management/pay-extra-report','AdminPanel\SubstituteReportController@payExtraReport');


    // For Student Attendance
    Route::get('/student/student-attendance/add','AdminPanel\StudentAttendanceController@studentAttendance');  
    Route::get('/student/student-attendance/view','AdminPanel\StudentAttendanceController@viewStudentAttendance');
    Route::get('/student/student-attendance/view-profile','AdminPanel\StudentAttendanceController@viewProfile');  
    Route::get('/student/student-attendance/edit-student-attendance','AdminPanel\StudentAttendanceController@editStudentAttendance');  

    //For Group
    Route::get('/student/group/add-group','AdminPanel\GroupController@addGroup');

    // For Student Group
    Route::get('/student/group/student-group','AdminPanel\StudentGroupController@studentGroup');
    Route::get('/student/group/student-group/manage-student','AdminPanel\StudentGroupController@manageStudent');
    Route::get('/student/group/student-group/manage-student/add-student','AdminPanel\StudentGroupController@addStudent');
    Route::get('/student/group/student-group/student-list','AdminPanel\StudentGroupController@studentList');

     // For Parent Group
    Route::get('/student/group/parent-group','AdminPanel\ParentGroupController@parentGroup');
    Route::get('/student/group/parent-group/manage-parent','AdminPanel\ParentGroupController@manageParentStudent');
    Route::get('/student/group/parent-group/manage-parent/add-parent','AdminPanel\ParentGroupController@addParent');
    Route::get('/student/group/parent-group/parent-list','AdminPanel\ParentGroupController@parentList');


    // For Dairy Remarks
    Route::get('/student/dairy-remarks/view','AdminPanel\DairyRemarksController@dairyRemarks');

    // For HomeWork Group
    Route::get('/student/homework-group/view','AdminPanel\HomeWorkGroupController@homeWorkGroup');
    Route::get('/student/homework-group/home-work-details','AdminPanel\HomeWorkGroupController@homeWorkDetails');
    Route::get('/student/homework-group/manage-students','AdminPanel\HomeWorkGroupController@manageStudents');
    Route::get('/student/homework-group/manage-parents','AdminPanel\HomeWorkGroupController@manageParents');
    Route::get('/student/homework-group/manage-teachers','AdminPanel\HomeWorkGroupController@manageTeachers');

    // For Staff Remarks
    Route::get('/staff/remarks/add-remark','AdminPanel\RemarkController@addRemark');
    Route::get('/staff/remarks/view-remark','AdminPanel\RemarkController@viewRemark');  

    // For Student Remarks
    Route::get('/student/remarks/add-remark','AdminPanel\RemarkController@addRemark');
    Route::get('/student/remarks/view-remark','AdminPanel\RemarkController@viewRemark');

    // For Student Homework
    Route::get('/student/homework/add-homework','AdminPanel\HomeWorkController@addHomework');
    Route::get('/student/homework/view-homework','AdminPanel\HomeWorkController@viewHomework');    

    //For Subject Homework
    Route::get('/subject/subject-homework/add-homework','AdminPanel\SubjectHomeWorkController@addHomework');
    Route::get('/subject/subject-homework/view-homework','AdminPanel\SubjectHomeWorkController@viewHomework');

     // For Subject Remarks
    Route::get('/subject/subject-remarks/add-remark','AdminPanel\SubjectRemarkController@addRemark');
    Route::get('/subject/subject-remarks/view-remark','AdminPanel\SubjectRemarkController@viewRemark');



    //For Staff- Login Websitw Url
    Route::get('/staff-menu/profile','AdminPanel\StaffMenuController@myProfile');
    Route::get('/staff-menu/my-class','AdminPanel\StaffMenuController@myClass');
    Route::get('/staff-menu/my-subjects','AdminPanel\StaffMenuController@mySubject');
    Route::get('/staff-menu/my-schedule','AdminPanel\StaffMenuController@mySchedule');

    //Student Leave For Staff Menu 
    Route::get('/staff-menu/my-class/view-student-leaves','AdminPanel\MyClassController@studentLeaves');

    // For Student Attendance
    Route::get('/staff-menu/my-class/add-student-attendance','AdminPanel\MyClassController@addStudentAttendance');
    Route::get('/staff-menu/my-class/view-student-attendance','AdminPanel\MyClassController@viewStudentAttendance');
     Route::get('/staff-menu/my-class/edit-student-attendance','AdminPanel\MyClassController@editStudentAttendance');
    Route::get('/staff-menu/my-class/view-student-attendance-profile','AdminPanel\MyClassController@studentAttendanceProfile');


    // For Student Remarks
    Route::get('/staff-menu/my-class/remark/add-remark','AdminPanel\MyClassController@addRemark');
     Route::get('/staff-menu/my-class/remark/view-remark','AdminPanel\MyClassController@viewRemark');

    // For Student HomeWork
    Route::get('/staff-menu/my-class/homework/add-homework','AdminPanel\MyClassController@addHomework');
     Route::get('/staff-menu/my-class/homework/view-homework','AdminPanel\MyClassController@viewHomework');

     // For Staff Dashboard My Subjects
     Route::get('/staff-menu/my-subjects/subject-list/view-students','AdminPanel\MySubjectsController@viewStudents');
     Route::get('/staff-menu/my-subjects/view-homework','AdminPanel\MySubjectsController@viewHomework');
     Route::get('/staff-menu/my-subjects/view-students/view-remark','AdminPanel\MySubjectsController@viewRemark');

     // For Staff Dashboard My Schedule
      Route::get('/staff-menu/my-schedule/daily-schedule','AdminPanel\MyScheduleController@dailySchedule');
      Route::get('/staff-menu/my-schedule/exam-duty','AdminPanel\MyScheduleController@examDuty');


      // For Parent-Login Website
      Route::get('/parent/view-profile','AdminPanel\ParentMenuController@myProfile');
      Route::get('/parent/children-details','AdminPanel\ParentMenuController@studentDetails');
      Route::get('/parent/children-details/view-attendance','AdminPanel\ParentMenuController@viewAttendance');
      Route::get('/parent/children-details/add-leave-application','AdminPanel\ParentMenuController@addLeaveApplication');
      Route::get('/parent/children-details/view-leave-application','AdminPanel\ParentMenuController@viewLeaveApplication');
      Route::get('/parent/children-details/view-remarks','AdminPanel\ParentMenuController@viewRemarks');
      Route::get('/parent/children-details/view-homework','AdminPanel\ParentMenuController@viewHomeWork');
      Route::get('/parent/children-details/view-time-table','AdminPanel\ParentMenuController@viewTimeTable');
      Route::get('/parent/children-details/view-vehicle-tracks','AdminPanel\ParentMenuController@viewVehicleTracks');
      Route::get('/parent/children-details/online-content','AdminPanel\ParentMenuController@viewOnlineContent');



      // For Inventory 
       //dynamic vendor url 
       Route::get('/inventory/manage-vendor/{id?}','AdminPanel\InventoryVendorController@add');
       Route::post('/inventory/manage-vendor-save/{id?}','AdminPanel\InventoryVendorController@save');
       Route::get('/inventory/manage-vendor-view','AdminPanel\InventoryVendorController@anyData');
       Route::get('/inventory/manage-vendor-status/{status?}/{id?}','AdminPanel\InventoryVendorController@changeStatus');
       Route::get('/inventory/delete-manage-vendor/{id?}','AdminPanel\InventoryVendorController@destroy');
       // Route::get('/inventory/manage-vendor','AdminPanel\InventoryController@manageVendor');
       //dynamic items url
       Route::get('/inventory/manage-items/{id?}','AdminPanel\InventoryItemController@add');
       Route::post('/inventory/manage-items-save/{id?}','AdminPanel\InventoryItemController@save');
       Route::get('/inventory/manage-items-view','AdminPanel\InventoryItemController@anyData');
       Route::get('/inventory/manage-items-status/{status?}/{id?}','AdminPanel\InventoryItemController@changeStatus');
       Route::get('/inventory/delete-manage-items/{id?}','AdminPanel\InventoryItemController@destroy');

       //For Ajax
       Route::get('/inventory/get-subcategory-data/{category_id?}','AdminPanel\InventoryCategoryController@getSubCategoryData');

       // Route::get('/inventory/manage-items','AdminPanel\InventoryController@manageItems');
       //dynamic category url
       Route::get('/inventory/manage-category/{id?}','AdminPanel\InventoryCategoryController@add');
       Route::post('/inventory/manage-category-save/{id?}','AdminPanel\InventoryCategoryController@save');
       Route::get('/inventory/manage-category-view','AdminPanel\InventoryCategoryController@anyData');
       Route::get('/inventory/manage-category-status/{status?}/{id?}','AdminPanel\InventoryCategoryController@changeStatus');
       Route::get('/inventory/delete-manage-category/{id?}','AdminPanel\InventoryCategoryController@destroy');
       // Route::get('/inventory/manage-category','AdminPanel\InventoryController@manageCategory');
       //dynamic unit url
       Route::get('/inventory/manage-unit/{id?}','AdminPanel\InventoryUnitController@add');
       Route::post('/inventory/manage-unit-save/{id?}','AdminPanel\InventoryUnitController@save');
       Route::get('/inventory/manage-unit-view','AdminPanel\InventoryUnitController@anyData');
       Route::get('/inventory/manage-unit-status/{status?}/{id?}','AdminPanel\InventoryUnitController@changeStatus');
       Route::get('/inventory/delete-manage-unit/{id?}','AdminPanel\InventoryUnitController@destroy');
       // Route::get('/inventory/manage-unit','AdminPanel\InventoryController@manageUnit');
      

       Route::get('/inventory/manage-purchase','AdminPanel\InventoryController@managePurchase');
       Route::get('/inventory/view-purchase-entry','AdminPanel\InventoryController@viewPurchaseEntry');
       Route::get('/inventory/view-purchase-entry/item-details','AdminPanel\InventoryController@itemDetails');

       Route::get('/inventory/manage-sales','AdminPanel\InventoryController@manageSales');
       Route::get('/inventory/view-sales','AdminPanel\InventoryController@viewSales');
       Route::get('/inventory/view-sales/item-details','AdminPanel\InventoryController@salesItemDetails');

       Route::get('/inventory/manage-print-invoice','AdminPanel\InventoryController@managePrintInvoice');
       Route::get('/inventory/manage-purchase-return','AdminPanel\InventoryController@managePurchaseReturn');
       Route::get('/inventory/manage-sales-return','AdminPanel\InventoryController@manageSalesReturn');
       Route::get('/inventory/view-pdf-format','AdminPanel\InventoryController@viewPdfFormat');

       // invoice PDF
       Route::get('pdfview',array('as'=>'pdfview','uses'=>'AdminPanel\InventoryController@pdfview'));

       // For Inventory Module Invoice Configuration
       Route::get('/inventory/manage-invoice-configuration','AdminPanel\InventoryController@manageInvoiceConfiguration');

       // For Account module
       Route::get('/account/manage-accounts-group','AdminPanel\AccountsController@accountsGroup');
       Route::get('/account/manage-accounts-head','AdminPanel\AccountsController@accountsHeads');
       Route::get('/account/manage-opening-balance','AdminPanel\AccountsController@openingBalance');
       Route::get('/account/manage-journal-entries','AdminPanel\AccountsController@journalEntries');
       Route::get('/account/view-journal-entries','AdminPanel\AccountsController@viewJournalEntries');
       Route::get('/account/payment-receipt-voucher','AdminPanel\AccountsController@paymentReceiptVoucher');

       // For Account Sheets
       Route::get('/account/view-profit-loss-account','AdminPanel\AccountsController@viewProfitLoss');

       Route::get('/account/view-trial-balance','AdminPanel\AccountsController@viewTrialBalance');
       Route::get('/account/ledger-vouchers','AdminPanel\AccountsController@ledgerVouchers');
       Route::get('/account/ledger-vouchers/view-ledger','AdminPanel\AccountsController@viewLedgerVouchers');

       Route::get('/account/view-balance-sheet','AdminPanel\AccountsController@viewBalanceSheet');

      // For Student-Login Website
      Route::get('/student/view-profile','AdminPanel\StudentMenuController@myProfile');
      Route::get('/student/view-attendance','AdminPanel\StudentMenuController@myAttendance');
      Route::get('/student/view-time-table','AdminPanel\StudentMenuController@myTimeTable');
      Route::get('/student/view-homework','AdminPanel\StudentMenuController@myHomework');
      Route::get('/student/view-remarks','AdminPanel\StudentMenuController@myRemarks');
      Route::get('/student/view-vehicle-tracks','AdminPanel\StudentMenuController@vehicleTracks');
      Route::get('/student/online-content','AdminPanel\StudentMenuController@onlineContent');
      Route::get('/student/view-task','AdminPanel\StudentMenuController@viewTask');
      Route::get('/student/view-task/view-responses','AdminPanel\StudentMenuController@viewResponses');
      Route::get('/student/online-content/notes/view-notes','AdminPanel\StudentMenuController@viewNotes');
       

      // For Payroll Module
      Route::get('/payroll/manage-department','AdminPanel\PayrollController@mangeDepartment'); 
       Route::get('/payroll/manage-department/view-map-employees','AdminPanel\PayrollController@viewMapEmployees');

       Route::get('/payroll/manage-grades','AdminPanel\PayrollController@mangeGrades'); 
       Route::get('/payroll/manage-grades/view-map-employees','AdminPanel\PayrollController@viewGradeMapEmployees');

       Route::get('/payroll/manage-salary-heads','AdminPanel\PayrollController@manageSalaryHeads');  

        Route::get('/payroll/manage-pf-esi-setting','AdminPanel\PayrollController@managePfEsiSetting');
       Route::get('/payroll/manage-tds-setting','AdminPanel\PayrollController@manageTdsSetting');

       Route::get('/payroll/manage-pt-setting','AdminPanel\PayrollController@managePtSetting');

       Route::get('/payroll/manage-leave-scheme','AdminPanel\PayrollController@manageLeaveScheme');  
       Route::get('/payroll/manage-leave-scheme/view-map-employees','AdminPanel\PayrollController@leaveSchemeMapEmployees'); 

       Route::get('/payroll/manage-salary-structure','AdminPanel\PayrollController@salaryStructure');   
       Route::get('/payroll/manage-salary-structure/map-structure','AdminPanel\PayrollController@mapStructure');

       Route::get('/payroll/add-arrears','AdminPanel\PayrollController@addArrears');
       Route::get('/payroll/manage-arrears','AdminPanel\PayrollController@manageArrears');

       Route::get('/payroll/add-bonus','AdminPanel\PayrollController@addBonus');
       Route::get('/payroll/manage-bonus','AdminPanel\PayrollController@manageBonus');

       Route::get('/payroll/manage-loan','AdminPanel\PayrollController@manageLoan');
       Route::get('/payroll/manage-advance','AdminPanel\PayrollController@manageAdvance');

       Route::get('/payroll/manage-salary-generation','AdminPanel\PayrollController@manageSalaryGeneration'); 




       // Participation Certificate 
       Route::get('/certificate/participation-certificate','AdminPanel\CertificateController@viewParticipation');
       Route::get('pdfviewp',array('as'=>'pdfviewp','uses'=>'AdminPanel\CertificateController@pdfviewp'));

        // Winner Certificate 
       Route::get('/certificate/winner-certificate','AdminPanel\CertificateController@viewWinner');
       Route::get('pdfview1',array('as'=>'pdfview1','uses'=>'AdminPanel\CertificateController@pdfview1'));

        // Character Certificate 
       Route::get('/certificate/character-certificate','AdminPanel\CertificateController@viewCharacter');
       Route::get('pdfview2',array('as'=>'pdfview2','uses'=>'AdminPanel\CertificateController@pdfview2'));
       Route::get('pdfview3',array('as'=>'pdfview3','uses'=>'AdminPanel\CertificateController@pdfview3'));

       // TC Certificate
       Route::get('pdfview4',array('as'=>'pdfview4','uses'=>'AdminPanel\CertificateController@pdfview4'));
       Route::get('pdfview5',array('as'=>'pdfview5','uses'=>'AdminPanel\CertificateController@pdfview5'));
       Route::get('pdfview6',array('as'=>'pdfview6','uses'=>'AdminPanel\CertificateController@pdfview6'));

       //Fee receipt certificate
       Route::get('pdfview7',array('as'=>'pdfview7','uses'=>'AdminPanel\CertificateController@pdfview7'));
       Route::get('pdfview11',array('as'=>'pdfview11','uses'=>'AdminPanel\CertificateController@pdfview11'));

       // Report card certificate
       Route::get('pdfview8',array('as'=>'pdfview8','uses'=>'AdminPanel\CertificateController@pdfview8'));
       Route::get('pdfview9',array('as'=>'pdfview9','uses'=>'AdminPanel\CertificateController@pdfview9'));
       Route::get('pdfview10',array('as'=>'pdfview10','uses'=>'AdminPanel\CertificateController@pdfview10'));

       // Fees collection reports 
       Route::get('/fees-collection/report/view-reports', 'AdminPanel\FeesCollectionReportController@viewReports');

       Route::get('/fees-collection/report/view-reports/class-sheet-report', 'AdminPanel\FeesCollectionReportController@classSheet');
       Route::get('/fees-collection/report/view-reports/student-sheet-report', 'AdminPanel\FeesCollectionReportController@studentSheet');
       Route::get('/fees-collection/report/view-reports/classwise-admission-report', 'AdminPanel\FeesCollectionReportController@classwiseAdmissionReport');
       Route::get('/fees-collection/report/view-reports/formwise-fees-report', 'AdminPanel\FeesCollectionReportController@formwiseFeesReport');
       Route::get('/fees-collection/report/view-reports/attendance-report', 'AdminPanel\FeesCollectionReportController@attendanceReport');
       Route::get('/fees-collection/report/view-reports/fees-wise-report', 'AdminPanel\FeesCollectionReportController@feesWiseReport');
       Route::get('/fees-collection/report/view-reports/subject-wise-marks-report', 'AdminPanel\FeesCollectionReportController@subjectWiseMarksReport');
       Route::get('/fees-collection/report/view-reports/exam-wise-marks-report', 'AdminPanel\FeesCollectionReportController@examWiseMarksReport');

       Route::get('/fees-collection/report/view-reports/staff-attendance-report', 'AdminPanel\FeesCollectionReportController@staffAttendanceReport');


       // For Stock Register inventory module
       Route::get('/inventory/manage-stock-register','AdminPanel\InventoryController@manageStockRegister');

       // For Admission Form
       Route::get('/admission-form/view-offline-form',array('as'=>'admin-panel/admission-form/view-offline-form','uses'=>'AdminPanel\AdmissionFormController@viewOfflineForm'));
       Route::get('/admission-form/view-online-form','AdminPanel\AdmissionFormController@viewOnlineForm');


});


  Route::group(['middleware' => 'staff'], function () {

  // For Dashboard
  // Route::get('admin-panel/dashboard', 'AdminPanel\DashboardController@index');
    // Route::get('admin-panel/staff/view-profile', 'AdminPanel\StaffController@viewProfile');
});