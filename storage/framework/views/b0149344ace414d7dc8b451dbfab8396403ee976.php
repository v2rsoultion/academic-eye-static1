﻿
<?php $__env->startSection('content'); ?>
<style type="text/css">

    .headingcommon {
            font-size: 13px;
    color: #6572b8;
    font-weight: 800;
    text-transform: uppercase;
    padding: 10px 0px;
    }
</style>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Room Details</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/hostel'); ?>"><?php echo trans('language.menu_hostel'); ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/hostel'); ?>">Configuration</a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/hostel/configuration/room-details'); ?>">Room Details</a></li>
                </ul>                
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body form-gap">
                        <div class="headingcommon  col-lg-12">Add Room Details :-</div>
                        <form class="" action="" method=""  id="contact_form">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                <lable class="from_one1">Hostel Name :</lable>                 
                                    <select class="form-control show-tick select_form1" name="">
                                        <option value="">Hostel Name</option>
                                        <option value="VII">Hostel-1</option>
                                        <option value="X">Hostel-2</option>
                                    
                                    </select>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                <lable class="from_one1">Block Name :</lable>                 
                                    <select class="form-control show-tick select_form1" name="block_name">
                                        <option value="">Block Name</option>
                                        <option value="1">Block-1</option>
                                        <option value="0">Block-2</option>
                                        <option value="0">Block-3</option>
                                        <option value="0">Block-4</option>

                                    </select>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                <lable class="from_one1">Floor No :</lable>                 
                                    <select class="form-control show-tick select_form1" name="floor_no">
                                        <option value="">Floor No</option>
                                        <option value="VII">Floor-1</option>
                                        <option value="X">Floor-2</option>
                                        <option value="X">Floor-3</option>
                                        <option value="X">Floor-4</option>
                                    </select>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                <lable class="from_one1">Room No :</lable>                 
                                    <select class="form-control show-tick select_form1" name="room_no">
                                        <option value="">Room No</option>
                                        <option value="VII">Room-1</option>
                                        <option value="X">Room-2</option>
                                        <option value="X">Room-3</option>
                                        <option value="X">Room-4</option>
                                        <option value="X">Room-5</option>
                                        <option value="X">Room-6</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row clearfix" style="margin-top:10px;">
                                
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <lable class="from_one1">Room Alias :</lable>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Room Alias"  name="room_alias">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <lable class="from_one1">Capacity :</lable>
                                    <div class="form-group">
                                        <input class="form-control positive-numeric-only" id="id-blah1" min="0" name="capacity" type="number" value="" placeholder="Capacity">
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row clearfix" style="margin-top:10px;">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-raised btn-primary">Save</button>
                                    <button type="submit" class="btn btn-raised btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>

                        <div class="clearfix"></div>
                        <hr>
                        <div class="headingcommon  col-lg-12" style="padding: 10px 0px;">Search By :-</div>
                          <div class="row ">
                                  <div class="col-lg-3 col-md-3 col-sm-12">
                                <lable class="from_one1">Hostel Name :</lable>                 
                                    <select class="form-control show-tick select_form1" name="">
                                        <option value="">Hostel Name</option>
                                        <option value="VII">Hostel-1</option>
                                        <option value="X">Hostel-2</option>
                                    
                                    </select>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12">
                                <lable class="from_one1">Block Name :</lable>                 
                                    <select class="form-control show-tick select_form1" name="block_name">
                                        <option value="">Block Name</option>
                                        <option value="1">Block-1</option>
                                        <option value="0">Block-2</option>
                                        <option value="0">Block-3</option>
                                        <option value="0">Block-4</option>

                                    </select>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-12">
                                <lable class="from_one1">Floor No :</lable>                 
                                    <select class="form-control show-tick select_form1" name="floor_no">
                                        <option value="">Floor No</option>
                                        <option value="VII">Floor-1</option>
                                        <option value="X">Floor-2</option>
                                        <option value="X">Floor-3</option>
                                        <option value="X">Floor-4</option>
                                    </select>
                                </div>
                                 <div class="col-lg-2 col-md-2 col-sm-12">
                                <lable class="from_one1">Room No :</lable>                 
                                    <select class="form-control show-tick select_form1" name="room_no">
                                        <option value="">Room No</option>
                                        <option value="VII">Room-1</option>
                                        <option value="X">Room-2</option>
                                        <option value="X">Room-3</option>
                                        <option value="X">Room-4</option>
                                        <option value="X">Room-5</option>
                                        <option value="X">Room-6</option>
                                    </select>
                                </div>
                                 <div class="col-lg-1">
                                    <button type="submit" class="btn btn-raised btn-primary saveBtn">Search</button>
                                </div>
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-raised btn-primary cancelBtn">Clear</button>
                                </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">
                                <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Hostel Name</th>
                                                <th>Block Name</th>
                                                <th>Floor No</th>
                                                <th>Room No</th>
                                                <th>Room Alias</th>
                                                <th>Capacity</th>
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Hostel-1</td>
                                                <td>Block-2</td>
                                                 <td>Floor-1</td>
                                                <td>Room-20</td>
                                                <td>---</td>
                                                <td>5</td>
                                                <td>
                                                <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>    
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Hostel-1</td>
                                                <td>Block-2</td>
                                                 <td>Floor-2</td>
                                                <td>Room-10</td>
                                                <td>---</td>
                                                <td>3</td>
                                                 <td>
                                                 <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>    
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Hostel-2</td>
                                                <td>Block-1</td>
                                                 <td>Floor-5</td>
                                                <td>Room-40</td>
                                                <td>---</td>
                                                <td>5</td>
                                                 <td>
                                                 <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                               <td>4</td>
                                                <td>Hostel-2</td>
                                                <td>Block-2</td>
                                                 <td>Floor-7</td>
                                                <td>Room-50</td>
                                                <td>---</td>
                                                <td>4</td>
                                                 <td>
                                                 <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>    
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>


<style type="text/css">

.dataTables_filter{
    float: right !important;
}
#DataTables_Table_0_paginate{
    float: right;
}

</style>



<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
           
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>