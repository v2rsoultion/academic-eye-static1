<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoScholasticTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'co_scholastic_types';
    protected $primaryKey = 'co_scholastic_type_id';
    public function up()
    {
        if (!Schema::hasTable('co_scholastic_types')) { 
            Schema::create('co_scholastic_types', function (Blueprint $table) {
                $table->increments('co_scholastic_type_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable(0);
                $table->integer('update_by')->unsigned()->nullable(0);
                $table->string('co_scholastic_type_name', 255)->nullable();
                $table->text('co_scholastic_type_description')->nullable();
                $table->tinyInteger('co_scholastic_type_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('co_scholastic_types', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('co_scholastic_types', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('co_scholastic_types');
    }
}
