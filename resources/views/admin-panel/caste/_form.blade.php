@if(isset($caste['caste_id']) && !empty($caste['caste_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
    /*.theme-blush .btn-primary {
    margin-top: 4px !important;
}*/
</style>

<p class="red">
@if ($errors->any())
    {{$errors->first()}}
@endif
</p>
{!! Form::hidden('caste_id',old('caste_id',isset($caste['caste_id']) ? $caste['caste_id'] : ''),['class' => 'gui-input', 'id' => 'caste_id', 'readonly' => 'true']) !!}

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.caste_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('caste_name', old('caste_name',isset($caste['caste_name']) ? $caste['caste_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.caste_name'), 'id' => 'caste_name']) !!}
        </div>
        @if ($errors->has('caste_name')) <p class="help-block">{{ $errors->first('caste_name') }}</p> @endif
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#caste-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                caste_name: {
                    required: true,
                    lettersonly:true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>