@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<style type="text/css">
  th, td{
    text-align: center;
  }
  .tabing {
    /*width: 100%;*/
    padding: 0px !important;
    border: 1px solid #ccc;
    border-radius: 5px;
    margin-bottom: 10px;
    /*background: #959ecf;*/
   }
   .theme-blush .nav-tabs .nav-link.active{
      color: #fff !important;
    border-radius: 0px !important;
    background: #1f2f60 !important;
    width: 190px;
   }
</style>
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Stock Register</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">Inventory</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/inventory/manage-item') !!}">Stock Register</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                <ul class="nav nav-tabs tabing">
                  <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#stockInwardReport">Stock Inward Report</a></li>
                  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#stockOutwardReport">
                  Stock Outward Report</a></li>
                  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#netStockReport">
                  Net Stock Report</a></li>
                </ul>

                <div class="tab-content">

                  <div class="tab-pane active" id="stockInwardReport">
                    <div class="row feecounterclass">  
                      <div style="width: 60%;margin-left: 15px;"><h6 class="text-center heading">Stock Inward Report</h6>
                      <table class="table border" id="" style="color: #000;">
                      {{ csrf_field() }}
                        <thead>
                          <tr>
                            <th style="width: 80px;">Sr. No</th>
                            <th style="width: 125px;">Date</th>
                            <th>Product</th>
                            <th style="width: 160px;">Quantity</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>18-06-2018</td>
                            <td>apple</td>
                            <td>15</td>
                          </tr>
                          <tr>
                            <td>2</td>
                            <td>18-06-2018</td>
                            <td>apple</td>
                            <td>5</td>
                          </tr>
                          <tr>
                            <td>3</td>
                            <td>18-06-2018</td>
                            <td>Banana</td>
                            <td>20</td>
                          </tr>
                          <tr>
                            <td>4</td>
                            <td>18-06-2018</td>
                            <td>Banana</td>
                            <td>25</td>
                          </tr>
                          <tr>
                            <td>5</td>
                            <td>18-06-2018</td>
                            <td> </td>
                            <td> </td>
                          </tr>
                          <tr>
                            <td>6</td>
                            <td>18-06-2018</td>
                            <td> </td>
                            <td> </td>
                          </tr>
                          <tr>
                            <td>7</td>
                            <td>18-06-2018</td>
                            <td> </td>
                            <td> </td>
                          </tr>
                          <tr>
                            <td>8</td>
                            <td>18-06-2018</td>
                            <td> </td>
                            <td> </td>
                          </tr>
                          <tr>
                            <td>9</td>
                            <td>18-06-2018</td>
                            <td> </td>
                            <td> </td>
                          </tr>
                        </tbody>
                      </table>
                      </div>
                    </div>
                  </div>

                  <div class="tab-pane fade" id="stockOutwardReport">
                    <div class="row feecounterclass"> 
                      <div style="width: 60%;margin-left: 15px;"><h6 class="heading text-center">Stock Outward Report</h6>
                      <table class="table border" id="" style="color: #000;">
                      {{ csrf_field() }}
                        <thead>
                          <tr>
                            <th style="width: 80px;">Sr. No</th>
                            <th style="width:125px;">Date</th>
                            <th>Product</th>
                            <th style="width: 160px;">Quantity</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>18-06-2018</td>
                            <td>apple</td>
                            <td>10</td>
                          </tr>
                          <tr>
                            <td>2</td>
                            <td>18-06-2018</td>
                            <td>apple</td>
                            <td>2</td>
                          </tr>
                          <tr>
                            <td>3</td>
                            <td>18-06-2018</td>
                            <td>Banana</td>
                            <td>15</td>
                          </tr>
                          <tr>
                            <td>4</td>
                            <td>18-06-2018</td>
                            <td>Banana</td>
                            <td>21</td>
                          </tr>
                          <tr>
                            <td>5</td>
                            <td>18-06-2018</td>
                            <td> </td>
                            <td> </td>
                          </tr>
                          <tr>
                            <td>6</td>
                            <td>18-06-2018</td>
                            <td> </td>
                            <td> </td>
                          </tr>
                          <tr>
                            <td>7</td>
                            <td>18-06-2018</td>
                            <td> </td>
                            <td> </td>
                          </tr>
                          <tr>
                            <td>8</td>
                            <td>18-06-2018</td>
                            <td> </td>
                            <td> </td>
                          </tr>
                          <tr>
                            <td>9</td>
                            <td>18-06-2018</td>
                            <td> </td>
                            <td> </td>
                          </tr>
                        </tbody>
                        </table>
                        </div>
                      </div>
                    </div>
                    
                    <div class="tab-pane fade" id="netStockReport">
                      <div class="row feecounterclass"> 
                        <div style="width: 50%;margin-left: 15px;"><h6 class="heading text-center">Net Stock Report</h6>
                        <table class="table border" id="" style="color: #000;">
                        {{ csrf_field() }}
                        <thead>
                          <tr>
                            <th style="width: 80px;">Sr. No</th>
                            <th>Product</th>
                            <th style="width: 160px;">Quantity</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>apple</td>
                            <td>8</td>
                          </tr>
                          <tr>
                            <td>2</td>
                            <td>Banana</td>
                            <td>9</td>
                          </tr>
                          <tr>
                            <td>3</td>
                            <td>Orange</td>
                            <td>0</td>
                          </tr>
                          <tr>
                            <td>4</td>
                            <td>Pine Apple</td>
                            <td>0</td>
                          </tr>
                          <tr>
                            <td>5</td>
                            <td>Guava</td>
                            <td>0</td>
                          </tr>
                          <tr>
                            <td>6</td>
                            <td>Cherry</td>
                            <td>0</td>
                          </tr>
                          <tr>
                            <td>7</td>
                            <td>Leechi</td>
                            <td>0</td>
                          </tr>
                          <tr>
                            <td>8</td>
                            <td>Mango</td>
                            <td>0</td>
                          </tr>
                          <tr>
                            <td>9</td>
                            <td>Grapes</td>
                            <td>0</td>
                          </tr>
                        </tbody>
                        </table>
                    </div>
                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
@endsection