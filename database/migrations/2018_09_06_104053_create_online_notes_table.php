<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlineNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'online_notes';
    protected $primaryKey = 'online_note_id';
    public function up()
    {
        if (!Schema::hasTable('online_notes')) {
            Schema::create('online_notes', function (Blueprint $table) {
                $table->increments('online_note_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->string('online_note_name', 255)->nullable();
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('subject_id')->unsigned()->nullable();
                $table->string('online_note_unit', 255)->nullable();
                $table->text('online_note_topic')->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->text('online_note_url')->nullable();
                $table->tinyInteger('online_note_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('online_notes', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('online_notes', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('online_notes', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');
            });
            Schema::table('online_notes', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');
            });
            Schema::table('online_notes', function($table) {
                $table->foreign('subject_id')->references('subject_id')->on('subjects')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_notes');
    }
}
