@if(isset($subject['subject_id']) && !empty($subject['subject_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
    /*.theme-blush .btn-primary {
    margin-top: 4px !important;
}*/
</style>

{!! Form::hidden('subject_id',old('subject_id',isset($subject['subject_id']) ? $subject['subject_id'] : ''),['class' => 'gui-input', 'id' => 'subject_id', 'readonly' => 'true']) !!}

<p class="red">
@if ($errors->any())
    {{$errors->first()}}
@endif
</p>
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.medium_type') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('medium_type', $subject['arr_medium'],isset($subject['medium_type']) ? $subject['medium_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type', 'onChange'=> 'getClass(this.value)'])!!}
                <i class="arrow double"></i>
            </label>
            @if($errors->has('medium_type')) <p class="help-block">{{ $errors->first('medium_type') }}</p> @endif
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.subject_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('subject_name', old('subject_name',isset($subject['subject_name']) ? $subject['subject_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.subject_name'), 'id' => 'subject_name']) !!}
        </div>
        @if ($errors->has('subject_name')) <p class="help-block">{{ $errors->first('subject_name') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.subject_code') !!} :</lable>
        <div class="form-group">
            {!! Form::text('subject_code', old('subject_code',isset($subject['subject_code']) ? $subject['subject_code']: ''), ['class' => 'form-control','placeholder'=>trans('language.subject_code'), 'id' => 'subject_code']) !!}
        </div>
        @if ($errors->has('subject_code')) <p class="help-block">{{ $errors->first('subject_code') }}</p> @endif
    </div>
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">Is_Coscholastic :</lable>
        <div class="form-group">
            <div class="radio" style="margin-top:6px !important;">
                @if(isset($subject['subject_is_coscholastic'])) 
                    {{ Form::radio('subject_is_coscholastic', '1',  $subject['subject_is_coscholastic'] == '1', array('id'=>'radio12')) }}
                    {{ Form::label('radio12', 'Yes', array('onclick'=>'checkForScholastic("1")', 'class' => 'document_staff')) }}

                    {{ Form::radio('subject_is_coscholastic', '0',  $subject['subject_is_coscholastic'] == '0', array('id'=>'radio2')) }}
                {{ Form::label('radio2', 'No', array('onclick'=>'checkForScholastic("0")', 'class' => 'document_staff')) }}
                @else
                {{ Form::radio('subject_is_coscholastic', '1',  false, array('id'=>'radio12')) }}
                {{ Form::label('radio12', 'Yes', array('onclick'=>'checkForScholastic("1")', 'class' => 'document_staff')) }}
                {{ Form::radio('subject_is_coscholastic', '0',  true, array('id'=>'radio2')) }}
                {{ Form::label('radio2', 'No', array('onclick'=>'checkForScholastic("0")', 'class' => 'document_staff')) }}
                @endif
                
            </div>
        </div>
    </div>
</div>

<div style="@if(isset($subject['subject_is_coscholastic']) && $subject['subject_is_coscholastic'] == '1') display:block; @else  display:none; @endif" id="scholasticBlock">
    <div class="row clearfix"> 
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.subject_class_ids') !!} :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    {!!Form::select('class_ids[]', $subject['arr_class'],  isset( $subject['class_ids']) ?  $subject['class_ids'] : "", ['class' => 'form-control show-tick select_form1 select2','id'=>'class_ids', 'multiple'=>'multiple', 'required'])!!}
                    <i class="arrow double"></i>
                </label>
            </div>
            @if($errors->has('class_ids')) <p class="help-block">{{ $errors->first('class_ids') }}</p> @endif
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
            <lable class="from_one1">{!! trans('language.co_scholastic_type_id') !!} :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    {!!Form::select('co_scholastic_type_id', $subject['arr_co_scholastic'],isset($subject['co_scholastic_type_id']) ? $subject['co_scholastic_type_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'co_scholastic_type_id', 'required'])!!}
                    <i class="arrow double"></i>
                </label>
            </div>
            @if($errors->has('co_scholastic_type_id')) <p class="help-block">{{ $errors->first('co_scholastic_type_id') }}</p> @endif
        </div>    
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        jQuery.validator.addMethod("specialChars", function( value, element ) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var key = value;

        if (!regex.test(key)) {
           return false;
        }
        return true;
        }, "please use only alphanumeric or alphabetic characters");


        $("#subject-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                subject_name: {
                    required: true,
                    lettersonly:true
                },
                subject_code: {
                    required: true,
                    specialChars:true
                },
                medium_type: {
                    required: true,
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    //error.insertAfter(element.parent());
                    element.closest('.form-group').after(error);
                }
            }
        });
    });

    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/class/get-class-data')}}",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("#class_ids").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#class_ids").html('<option value="">Select Classes</option>');
            $(".mycustloading").hide();
        }
    }

    function checkForScholastic(val) {
        var x = document.getElementById("scholasticBlock");
        if(val == 0) {
            if (x.style.display === "none") {
                x.style.display = "none";
            } else {
                x.style.display = "none";
            }
        } else {
            if (x.style.display === "block") {
                x.style.display = "block";
            } else {
                x.style.display = "block";
            }
        }
    }

</script>