<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\Datatables\Datatables;

class RecurringHeadsController extends Controller
{
     /**
     *  View page for recurring heads
     *  @Khushbu on 22 Sept 2018
    **/
    public function index()
    {
        $loginInfo 		    = get_loggedin_user_data();
        
        $data 				= array(
            'page_title'    => trans('language.view_one_time'),
            'redirect_url'  => url('admin-panel/fees-collection/recurring-heads/view-recurring-heads'),
            'login_info'    => $loginInfo,
            // 'job'           => $job,
        );
        return view('admin-panel.recurring-heads.view_recurring_heads')->with($data);
    }

    /**
     *  Add page for recurring heads
     *  @Khushbu on 22 Sept 2018   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        // $job        = [];
        if (!empty($id))
        {
            // $decrypted/////////_job_id = get_decrypted_value($id, true);
            $page_title             = trans('language.edit_one_time');
            $save_url               = url('admin-panel/fees-collection/recurring-heads/save/' . $decrypted_job_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_job');
            $save_url      = url('admin-panel/fees-collection/recurring-heads/save');
            $submit_button = 'Save';
        }

        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/fees-collection/recurring-heads/view-recurring-heads'),
        );
        return view('admin-panel.recurring-heads.recurring_heads')->with($data);
    }
}

