<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RteCollectionController extends Controller
{
    
    public function rteFeesHead() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.rte-collection.rte_fees_head')->with($data);
    }

    public function applyFeesOnHead() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.rte-collection.apply_fees_on_rte_head')->with($data);
    }
}
