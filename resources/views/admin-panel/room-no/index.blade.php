@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_room_no') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <a href="{!! url('admin-panel/room-no/add-room-no') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.menu_configuration') !!}</li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.room_no') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="">{!! trans('language.view_room_no') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <p class="green">
                                        {{ session()->get('success') }}
                                    </p>
                                @endif
                                @if($errors->any())
                                    <p class="red">{{$errors->first()}}</p>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('room_no', old('room_no', ''), ['class' => 'form-control ','placeholder'=>trans('language.room_no_add_page'), 'id' => 'room_no']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="table-responsive">
                                <table class="table m-b-0 c_list" id="room-no-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.room_no_add_page')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#room-no-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/room-no/data')}}',
                data: function (d) {
                    d.room_no = $('input[name=room_no]').val();
                }
            },
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index'},
                {data: 'room_no', name: 'room_no'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "15%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });

        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })
    });


</script>
@endsection