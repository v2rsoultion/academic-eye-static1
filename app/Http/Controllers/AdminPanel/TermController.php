<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Term\Term; // Model
use Yajra\Datatables\Datatables;

class TermController extends Controller
{
	/**
     *  View page for Term
     *  @Pratyush on 11 Aug 2018
    **/
    public function index()
    {
        $term                   = [];
        $loginInfo              = get_loggedin_user_data();
        $arr_medium             = \Config::get('custom.medium_type');
        $term['arr_medium']     = add_blank_option($arr_medium, 'Select Medium');
        $data = array(
            'page_title'    => trans('language.view_terms'),
            'redirect_url'  => url('admin-panel/examination/manage-term'),
            'login_info'    => $loginInfo,
            'term'          => $term
        );
        return view('admin-panel.term.index')->with($data);
    } 	   

    /**
     *  Add page for Term
     *  @Pratyush on 11 Aug 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $term 			= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_term_id 	= get_decrypted_value($id, true);
            $term      			= Term::Find($decrypted_term_id);
            if (!$term)
            {
                return redirect('admin-panel/examination/manage-term')->withError('Term not found!');
            }
            $page_title             	= trans('language.add_terms');
            $encrypted_term_id   		= get_encrypted_value($term->term_exam_id, true);
            $save_url               	= url('admin-panel/examination/save-term/' . $encrypted_term_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_terms');
            $save_url      = url('admin-panel/examination/save-term');
            $submit_button = 'Save';
        }
        $arr_medium             = \Config::get('custom.medium_type');
        $term['arr_medium']  = add_blank_option($arr_medium, 'Select Medium');
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'term' 				=> $term,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/examination/manage-term'),
        );
        return view('admin-panel.term.add')->with($data);
    }

    /**
     *  Add and update Term's data
     *  @Pratyush on 11 Aug 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_term_id			= get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        $medium_type = null;
        if (!empty($id))
        {
            $term = Term::find($decrypted_term_id);
            $admin_id = $term['admin_id'];
            if (!$term)
            {
                return redirect('/admin-panel/examination/manage-term/')->withError('Term not found!');
            }
            $success_msg = 'Term updated successfully!';
        }
        else
        {
            $term     		= New Term;
            $success_msg 	= 'Term saved successfully!';
        }
        if ($request->has('medium_type'))
        {
            $medium_type = Input::get('medium_type');
        }
        $validatior = Validator::make($request->all(), [
                'term_name'   => 'required|unique:term_exams,term_exam_name,' . $decrypted_term_id . ',term_exam_id,medium_type,' . $medium_type,
                'medium_type'      => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $term->admin_id       		= $admin_id;
                $term->update_by      		= $loginInfo['admin_id'];
                $term->term_exam_name 		= Input::get('term_name');
                $term->medium_type 		    = Input::get('medium_type');
                $term->term_exam_caption 	= Input::get('term_exam_caption');
                $term->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/examination/manage-term')->withSuccess($success_msg);
    }

    /**
     *  Get Term's Data for view page(Datatables)
     *  @Pratyush on 11 Aug 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 		= get_loggedin_user_data();
        $term  			= Term::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('term_exam_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null)
            {
                $query->where('medium_type', "=", $request->get('medium_type'));
            }
        })->orderBy('term_exam_id', 'ASC')->get();
        return Datatables::of($term,$request)
        ->addColumn('medium_type', function ($term)
        {
            $arr_medium   = \Config::get('custom.medium_type');
            $medium_type  = $arr_medium[$term->medium_type];
            return $medium_type;
        })
        ->addColumn('action', function ($term) use($request)
        {
            if($request->get('tempid') != '' && $request->get('tempid') != null){
                $edit_manage_path = '../manage-term';                        
            }else{
                $edit_manage_path = 'manage-term';
            }
            $encrypted_term_id = get_encrypted_value($term->term_exam_id, true);
            if($term->term_exam_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="term-status/'.$status.'/' . $encrypted_term_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="'.$edit_manage_path.'/'.$encrypted_term_id.')"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-term/' . $encrypted_term_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy Term's data
     *  @Pratyush on 11 Aug 2018.
    **/
    public function destroy($id)
    {
        $term_id 		= get_decrypted_value($id, true);
        $term 		  	= Term::find($term_id);
        if ($term)
        {
            $term->delete();
            $success_msg = "Term deleted successfully!";
            return redirect('admin-panel/examination/manage-term')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Term not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Term's status
     *  @Pratyush on 11 Aug 2018.
    **/
    public function changeStatus($status,$id)
    {
        $term_id 		= get_decrypted_value($id, true);
        $term 		  	= Term::find($term_id);
        if ($term)
        {
            $term->term_exam_status  = $status;
            $term->save();
            $success_msg = "Term status update successfully!";
            return redirect('admin-panel/examination/manage-term')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Term not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    /**
     *  Get term data according medium
     *  @Shree on 17 Sept 2018
    **/
    public function getTermData()
    {
        $medium_type = Input::get('medium_type');
        $terms = get_all_terms($medium_type);
        $data = view('admin-panel.term.ajax-term-select',compact('terms'))->render();
        return response()->json(['options'=>$data]);
    }

}