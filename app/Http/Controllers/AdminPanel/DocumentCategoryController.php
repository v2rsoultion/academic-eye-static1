<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\DocumentCategory\DocumentCategory; // Model
use Yajra\Datatables\Datatables;

class DocumentCategoryController extends Controller
{
    /**
     *  View page for Document Category
     *  @Pratyush on 19 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_document_category'),
            'redirect_url'  => url('admin-panel/document-category/view-document-categories'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.document_category.index')->with($data);
    }

    /**
     *  Add page for Document Category
     *  @Pratyush on 19 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    			= [];
        $document_category 	= [];
        $loginInfo = get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_docu_cate_id = get_decrypted_value($id, true); //Document Category ID
            $document_category      = DocumentCategory::Find($decrypted_docu_cate_id);
            if (!$document_category)
            {
                return redirect('admin-panel/document-category/add-document-category')->withError('Document Category not found!');
            }
            $page_title             = trans('language.edit_document_category');
            $encrypted_doc_cate_id  = get_encrypted_value($document_category->document_category_id, true);
            $save_url               = url('admin-panel/document-category/save/' . $encrypted_doc_cate_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_document_category');
            $save_url      = url('admin-panel/document-category/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'document_category' => $document_category,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/document-category/view-document-categories'),
        );
        return view('admin-panel.document_category.add')->with($data);
    }

    /**
     *  Add and update Document category's data
     *  @Pratyush on 19 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo             = get_loggedin_user_data();
        $decrypted_doc_cate_id = get_decrypted_value($id, true);
        $admin_id               = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $document_category = DocumentCategory::find($decrypted_doc_cate_id);
            $admin_id = $document_category['admin_id'];
            if (!$document_category)
            {
                return redirect('/admin-panel/document-category/add-document-category/')->withError('Document Category not found!');
            }
            $success_msg = 'Document Category updated successfully!';
        }
        else
        {
            $document_category     = New DocumentCategory;
            $success_msg = 'Document Category saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
                'document_category_name'   => 'required|unique:document_category,document_category_name,' . $decrypted_doc_cate_id . ',document_category_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $document_category->admin_id            	= $admin_id;
                $document_category->update_by           	= $loginInfo['admin_id'];
                $document_category->document_category_name 	= Input::get('document_category_name');
                $document_category->document_category_for  	= Input::get('doc_cate_for');  
                // p($shift);
                $document_category->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/document-category/view-document-categories')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Pratyush on 19 July 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 			= get_loggedin_user_data();
        $temp_document_category  = DocumentCategory::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('document_category_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('document_category_id', 'DESC')->get();
        $document_category_arr 	= array();
        $document_category 		= array();

        foreach ($temp_document_category as $temp_key1 => $temp_value1){
        	if($temp_value1['document_category_for'] == 0){
        		$doc_for = 'Student';
        	}elseif($temp_value1['document_category_for'] == 1){
        		$doc_for = 'Staff';
        	}elseif($temp_value1['document_category_for'] == 2){
        		$doc_for = 'Student and Staff';
        	}
        	$document_category_arr[$temp_key1]['document_category_id'] 		= $temp_value1['document_category_id'];
        	$document_category_arr[$temp_key1]['document_category_name'] 	= $temp_value1['document_category_name'];
        	$document_category_arr[$temp_key1]['document_category_status'] 	= $temp_value1['document_category_status'];
        	$document_category_arr[$temp_key1]['document_category_for'] 	= $doc_for;
        }

        foreach ($document_category_arr as $temp_key2 => $temp_value2) {
			$document_category[$temp_key2] = (object)$temp_value2;
        }

        return Datatables::of($document_category)
        ->addColumn('action', function ($document_category)
        {
            $encrypted_doc_cate_id = get_encrypted_value($document_category->document_category_id, true);
            if($document_category->document_category_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="pull-left"><a href="document-categories-status/'.$status.'/' . $encrypted_doc_cate_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-document-category/' . $encrypted_doc_cate_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-document-category/' . $encrypted_doc_cate_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
                
    }

    /**
     *  Destroy Document Category's data
     *  @Pratyush on 19 July 2018.
    **/
    public function destroy($id)
    {
        $doc_cate_id 		= get_decrypted_value($id, true);
        $document_category  = DocumentCategory::find($doc_cate_id);
        if ($document_category)
        {
            $document_category->delete();
            $success_msg    = "Document Category deleted successfully!";
            return redirect('admin-panel/document-category/view-document-categories')->withSuccess($success_msg);
        }
        else
        {
            $error_message  = "Document Category not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Document Category's status
     *  @Pratyush on 19 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $doc_cate_id 		= get_decrypted_value($id, true);
        $document_category  = DocumentCategory::find($doc_cate_id);
        if ($document_category)
        {
            $document_category->document_category_status  = $status;
            $document_category->save();
            $success_msg    = "Document Category status updated!";
            return redirect('admin-panel/document-category/view-document-categories')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Document Category not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
