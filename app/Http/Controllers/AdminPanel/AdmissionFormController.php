<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class AdmissionFormController extends Controller
{
    public function viewOfflineForm(Request $request) {
        if($request->has('download')) {
            $pdf = PDF::loadView('admin-panel/admission-form-templates/admission-form-offline');
            return $pdf->stream('offline-form.pdf');
        }
        
        return view('admin-panel.admission-form-templates.admission-form-offline');
    }

    public function viewOnlineForm() {
        $loginInfo   		  = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.admission-form-templates.admission-form-online')->with($data);
    }
}
// <tr style="font-size: 12px;">
//                 <td colspan="4">
//                     <div class="head-gap">Attachment Req:</div>
//                     <div class="box-size"> </div>
//                     <div class="box-content less"> T.C. (Original) </div>
//                     <div class="box-size"> </div>
//                     <div class="box-content less"> Mark Sheet (Photocopy) </div>
//                     <div class="box-size"> </div>
//                     <div class="box-content less"> Birth Certificate (Photocopy) </div>
//                 </td>
//             </tr>