<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuedBooksHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'issued_books_history';
    protected $primaryKey = 'history_id';
    public function up()
    {
        if (!Schema::hasTable('issued_books_history')){ 
            Schema::create('issued_books_history', function (Blueprint $table) {
                $table->increments('history_id');
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->integer('book_id')->unsigned()->default(0);
                $table->integer('member_id')->unsigned()->default(0);
                $table->integer('member_type')->unsigned()->default(0);
                $table->integer('book_copies_id')->unsigned()->default(0);
                $table->date('issued_from_date')->nullable();
                $table->date('issued_to_date')->nullable();
                $table->timestamps();
            });

            Schema::table('issued_books_history', function($table) {
                    $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('issued_books_history', function($table) {
                    $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('issued_books_history', function($table) {
                    $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issued_books_history');
    }
}
