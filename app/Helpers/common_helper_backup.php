<?php

//use File;
    
    use App\Model\Address\Country;
    use App\Model\Address\State;
    use App\Model\Address\City;
    use App\Model\School\School;
    use App\Model\School\SchoolBoard;
    use App\Model\Session\Session;
    use App\Model\Classes\Classes;
    use App\Model\CoScholastic\CoScholastic;
    use App\Model\Section\Section;
    use App\Model\Caste\Caste;
    use App\Model\Religion\Religion;
    use App\Model\Nationality\Nationality;
    use App\Model\SchoolGroup\SchoolGroup; 
    use App\Model\Student\Student;
    use App\Model\Student\StudentParent;
    use App\Model\Student\StudentAcademic;
    use App\Model\Student\StudentHealth;
    use App\Model\Designation\Designation;
    use App\Model\Staff\Staff;
    use App\Model\StaffRoles\StaffRoles;
    use App\Model\Subject\Subject;
    use App\Model\DocumentCategory\DocumentCategory; 
    use App\Model\Term\Term; // Model @Pratyush on 11  Aug 2018
    use App\Model\BookCupboard\BookCupboard; // Model @Pratyush on 13 Aug 2018
    use App\Model\BookCategory\BookCategory; // Model @Pratyush on 14 Aug 2018
    use App\Model\BookVendor\BookVendor;     // Model @Pratyush on 14 Aug 2018
    use App\Model\BookCupboardshelf\BookCupboardshelf;     // Model @Pratyush on 16 Aug 2018
    use App\Model\SubjectClassMapping\SubjectClassmapping;     // Model @Ashish on 22 Aug 2018
    use App\Model\Exam\Exam;     // Model @Ashish on 22 Aug 2018
    use App\Model\Stream\Stream; // Model
    use App\Model\Competition\Competition; // Model 
    use App\Model\Competition\CompetitionMapping; // Model 
    use App\Model\Brochure\Brochure; // Model 
    use Illuminate\Support\Facades\Crypt;
    use Session as LSession;
    
    use Carbon\Carbon;

    function p($p, $exit = 1)
    {
        echo '<pre>';
        print_r($p);
        echo '</pre>';
        if ($exit == 1)
        {
            exit;
        }
    }

    function set_session($session_id, $session_year)
    {
        $session_data = [];
        $session_data = array(
            'session_id'   => $session_id,
            'session_year' => $session_year,
        );
        LSession::put('set_session_year', $session_data);
        return LSession::get('set_session_year');
    }

    function get_loggedin_user_data()
    {
        $user_data  = array();
        $admin_user = Auth::guard('admin')->user();
        $staff_id   = "";
        if (!empty($admin_user['admin_id']))
        {
            $encrypted_school_id = "";
            $school_id = "";
            $student_id = ""; // @Pratyush on 17 Aug 2018 for leave application
            if($admin_user['admin_type'] == 0){
                $adminType = "Super Admin";
                if(COUNT(School::get())) {
                    $school = School::first()->toArray();
                    $encrypted_school_id = get_encrypted_value($school['school_id'], true);
                }
            } else if($admin_user['admin_type'] == 1){
                $adminType = "School Admin";
                if(COUNT(School::where('admin_id','=' ,$admin_user['admin_id'])->get()->toArray())) {
                    $school = School::where('admin_id','=' ,$admin_user["admin_id"])->get()->toArray();
                    $school              = isset($school[0]) ? $school[0] : [];
                    $encrypted_school_id = get_encrypted_value($school['school_id'], true);
                    $school_id           = $school['school_id'];
                }
            } else if($admin_user['admin_type'] == 2){
                $adminType = "Staff Admin";
                $school    = Staff::where('reference_admin_id',$admin_user['admin_id'])->first();
                $school_id = $school['school_id'];
                $staff_id = $school['staff_id'];

            }  else if($admin_user['admin_type'] == 3){
                $adminType = "Parent Admin";
            } else if($admin_user['admin_type'] == 4){
                $adminType = "Student Admin";
                // @Pratyush on 17 Aug 2018
                if(COUNT(Student::where('reference_admin_id',$admin_user['admin_id'])->get())) {
                    $student    = Student::where('reference_admin_id',$admin_user['admin_id'])->first();
                    $school_id  = $student->school_id; 
                    $student_id = $student->student_id;  
                    $encrypted_school_id = get_encrypted_value($student->school_id, true);
                }

            }
            $user_data = array(
                'admin_id'              => $admin_user['admin_id'],
                'admin_name'            => $admin_user['admin_name'],
                'email'                 => $admin_user['email'],
                'admin_type'            => $admin_user['admin_type'],
                'admin_role'            => $adminType,
                'encrypted_school_id'   => $encrypted_school_id,
                'school_id'             => $school_id,
                'student_id'            => $student_id,
                'staff_id'              => $staff_id
            );
        }
        return $user_data;
    }

    function last_query($raw = false, $last = false)
    {
        DB::enableQueryLog();
        $log = DB::getQueryLog();

        if ($raw)
        {
            foreach ($log as $key => $query)
            {
                $log[$key] = vsprintf(str_replace('?', "'%s'", $query['query']), $query['bindings']);
            }
        }

        // return ($last) ? end($log) : $log; // return single table query
        return $log;  // return join tables queries 
    }

    function check_string_empty($value)
    {
        if (!empty($value))
        {
            return $value;
        }
        else
        {
            return '';
        }
    }

    function get_class_order()
    {

        $arr_classes          = [];
        $arr_existing_classes = Classes::where(array('class_status' => 1))->select('class_name', 'class_order', 'class_id')->orderBy('class_order', 'ASC')->get();
        foreach ($arr_existing_classes as $existing_class)
        {
            $arr_classes[$existing_class['class_id'] . '_' . $existing_class['class_order']] = $existing_class['class_name'];
        }
        return $arr_classes;
    }

    /**
     * Get all classes 
     */
    function get_all_classes($medium_type)
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_classes = [];
        $arr_classes_list = Classes::where(array('class_status' => 1,'medium_type'=> $medium_type))->select('class_name', 'class_order', 'class_id')->orderBy('class_order', 'ASC')->get();
        if (!empty($arr_classes_list))
        {
            foreach ($arr_classes_list as $arr_class)
            {
                $arr_classes[$arr_class['class_id']] = $arr_class['class_name'];
            }
        }
        return $arr_classes;
    }

    /**
     * Get all types of Co-Scholastic  
     */
    function get_all_co_scholastic()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_scholastic = [];
        $arr_scholastic_list = CoScholastic::where(array('co_scholastic_type_status' => 1))->select('co_scholastic_type_name', 'co_scholastic_type_id')->orderBy('co_scholastic_type_name', 'ASC')->get();
        if (!empty($arr_scholastic_list))
        {
            foreach ($arr_scholastic_list as $arr_scholastics)
            {
                $arr_scholastic[$arr_scholastics['co_scholastic_type_id']] = $arr_scholastics['co_scholastic_type_name'];
            }
        }
        return $arr_scholastic;
    }

    function add_blank_option($arr, $option)
    {
        $arr_option = array();
        if (!empty($option))
        {
            $arr_option[''] = $option;
        }
        else
        {
            $arr_option[''] = '';
        }
        // operator on array
        $result = $arr_option + $arr;

        return $result;
    }

    function get_formatted_date($date)
    {
        $return      = array();
        $date_format = Config::get('custom.date_format');
        if (!empty($date))
        {
            $return = date($date_format, strtotime($date));
        }
        else
        {
            $return = '';
        }
        return $return;
    }

    /**
     * Get all caste 
     */
    function get_caste()
    {
        $loginInfo 			= get_loggedin_user_data();
        $arr_caste          = [];
        $arr_caste_data = Caste::where([['caste_status', '=', 1]])->select('caste_id', 'caste_name')->get();
        if (!empty($arr_caste_data))
        {
            foreach ($arr_caste_data as $arr_caste_data)
            {
                $arr_caste[$arr_caste_data['caste_id']] = $arr_caste_data['caste_name'];
            }
        }
        return $arr_caste;
    }

     /**
     * Get all religions 
     */
    function get_religion()
    {
        $loginInfo 		= get_loggedin_user_data();
        $arr_religion   = [];
        $arr_religion_data = Religion::where([['religion_status', '=', 1]])->select('religion_id', 'religion_name')->get();
        if (!empty($arr_religion_data))
        {
            foreach ($arr_religion_data as $arr_religion_data)
            {
                $arr_religion[$arr_religion_data['religion_id']] = $arr_religion_data['religion_name'];
            }
        }
        return $arr_religion;
    }

    /**
     * Get all nationality 
     */
    function get_nationality()
    {
        $loginInfo 			= get_loggedin_user_data();
        $arr_nationality       = [];
        $arr_nationality_data = Nationality::where([['nationality_status', '=', 1]])->select('nationality_id', 'nationality_name')->get();
        if (!empty($arr_nationality_data))
        {
            foreach ($arr_nationality_data as $arr_nationality_data)
            {
                $arr_nationality[$arr_nationality_data['nationality_id']] = $arr_nationality_data['nationality_name'];
            }
        }
        return $arr_nationality;
    }

    /**
     * Get all house/group
     */
    function get_student_groups()
    {
        $loginInfo 			= get_loggedin_user_data();
        $arr_group       = [];
        $arr_group_data = SchoolGroup::where([['group_status', '=', 1]])->select('group_id', 'group_name')->get();
        if (!empty($arr_group_data))
        {
            foreach ($arr_group_data as $arr_group_data)
            {
                $arr_group[$arr_group_data['group_id']] = $arr_group_data['group_name'];
            }
        }
        return $arr_group;
    }

    function get_student_type()
    {
        $student_type     = [];
        $arr_student_type = StudentType::where('student_type_status', 1)->select('student_type_id', 'student_type')->get();
        if (!empty($arr_student_type))
        {
            foreach ($arr_student_type as $arr_type)
            {
                $student_type[$arr_type['student_type_id']] = $arr_type['student_type'];
            }
        }
        return $student_type;
    }

    function get_class_section($class_id=null)
    {
        $loginInfo 			= get_loggedin_user_data();
        $section     = [];
        if(isset($class_id) && $class_id != null){
            $arr_section = Section::where([['section_status', '=', 1],['class_id', '=', $class_id]])->select('section_id', 'section_name')->get();
        } else {
            $arr_section = Section::where([['section_status', '=', 1]])->select('section_id', 'section_name')->get();
        }
        
        if (!empty($arr_section))
        {
            foreach ($arr_section as $value)
            {
                $section[$value['section_id']] = $value['section_name'];
            }
        }
        return $section;
    }
    /**
     * Get class - section info using section ID
     */
    function get_class_section_info($section_id=null)
    {
        $loginInfo 			= get_loggedin_user_data();
        $sectionInfo     = [];
        if(isset($section_id) && $section_id != null){
            $arr_section = Section::where([['section_status', '=', 1],['section_id', '=', $section_id]])->with('sectionClass')->get();
        } else {
            $arr_section = Section::where([['section_status', '=', 1]])->with('sectionClass')->get();
        }
        if (!empty($arr_section))
        {   
            foreach ($arr_section as $value)
            {
                $totalStudents = 0;
                $totalStudents = StudentAcademic::where(function($query) use ($loginInfo,$value) 
                {
                    $query->where('current_class_id', "=", $value['class_id']);
                    $query->where('current_section_id', "=", $value['section_id']);
                   
                })->get()->count();
                $list_arr = array(
                    'section_name'      => $value['section_name'],
                    'section_order'     => $value['section_order'],
                    'total_students'    => $totalStudents,
                );
                if (isset($value['sectionClass']['class_name']))
                {
                    $list_arr['class_name'] = $value['sectionClass']['class_name'];
                }
            }
        }
        return $list_arr;
    }

    function get_session()
    {
        $set_session     = LSession::get('set_session_year');
        $session_id      = null;
        $updated_session = [];
        if (!empty($set_session))
        {
            $session_id = $set_session['session_id'];
        }
        $session = Session::where(function($query) use ($session_id)
            {
                if (!empty($session_id))
                {
                    $query->where('session_id', $session_id);
                }
                else
                {
                    $query->where('session_status', 1);
                }
            })->select('session_id', 'session_name')->first();
        if (!empty($session->session_id))
        {
            $updated_session[$session->session_id] = $session['session_name'];
        }
        return $updated_session;
    }

    function get_arr_session()
    {
        $session     = [];
        $arr_session = Session::select('session_id', 'session_name')->get();
        if (!empty($arr_session))
        {
            foreach ($arr_session as $value)
            {
                $session[$value['session_id']] = $value['session_name'];
            }
        }
        return add_blank_option($session, 'Select session');
    }

    function get_encrypted_value($key, $encrypt = false)
    {
        $encrypted_key = null;
        if (!empty($key))
        {
            if ($encrypt == true)
            {
                $key = Crypt::encrypt($key);
            }
            $encrypted_key = $key;
        }
        return $encrypted_key;
    }

    function get_decrypted_value($key, $decrypt = false)
    {
        $decrypted_key = null;
        if (!empty($key))
        {
            if ($decrypt == true)
            {
                $key = Crypt::decrypt($key);
            }
            $decrypted_key = $key;
        }
        return $decrypted_key;
    }

    // Check for siblings 
    function check_sibling($student_parent_id,$student_id)
    {
        
        $loginInfo          = get_loggedin_user_data();
        $info      = Student::where(function($query) use ($student_parent_id,$student_id) 
        {
            // For student parent id
            if (!empty($student_parent_id) && $student_parent_id != null)
            {
                $query->where('student_parent_id', "=", $student_parent_id);
            }
            // For student student id
            if (!empty($student_parent_id) && $student_parent_id != null)
            {
                $query->where('student_id', "!=", $student_id);
            }
        })->get();
        if(!empty($info[0]->student_id)){
            return 'true';
        } else {
            return 'false';
        }
    }
    // Check parents already exist or not?
    function check_parents_exist($email, $mobile_no)
    {
        $parent_info = [];
        $loginInfo          = get_loggedin_user_data();
        $arr_parent_info      = StudentParent::where(function($query) use ($email,$mobile_no) 
        {
            // For Email
            if (!empty($email) && $email != null)
            {
                $query->where('student_login_email', "=", $email);
            }
             // For Mobile
             if (!empty($mobile_no) && $mobile_no != null)
             {
                 $query->where('student_login_contact_no', "=", $mobile_no);
             }
            
        })->join('admins', function($join) {
            $join->on('admins.admin_id', '=', 'student_parents.reference_admin_id');
           
        })->get();
        if(!empty($arr_parent_info[0]->student_login_email)){
            $parent_info = (object) $arr_parent_info[0];
        }
        return $parent_info;
    }

    // Get student list using section ID
    function get_student_list_by_section_id($request)
    {
       // p($request->get('section_id'));
        $arr_student_list = [];
        $arr_medium         = \Config::get('custom.medium_type');
        $arr_student_type   = \Config::get('custom.student_type');
        $arr_student      = Student::where(function($query) use ($request) 
        {
            // For Roll No
            if (!empty($request) && !empty($request->get('roll_no')))
            {
                $query->where('student_roll_no', "like", "%{$request->get('roll_no')}%");
            }
            // For Enroll No
            if (!empty($request) && !empty($request->get('enroll_no')))
            {
                $query->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
            }
            // For Student Name
            if (!empty($request) && !empty($request->get('student_name')))
            {
                $query->where('student_name', "like", "%{$request->get('student_name')}%");
            }
            
        })->join('student_academic_info', function($join) use ($request){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            $join->where('current_section_id', '=',$request->get('section_id'));
        })->with('getStudentAcademic')
        ->join('student_parents', function($join) use ($request){
            $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
            if (!empty($request) && !empty($request->get('father_name')))
            {
                $join->where('student_father_name', "like", "%{$request->get('father_name')}%");
            }
        })
        ->with('getParent')->get();
        
        if (!empty($arr_student[0]->student_id))
        {
            foreach ($arr_student as $student)
            {
                if (!empty($student->student_image))
                {
                    $profile = check_file_exist($student->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                } else {
                    $student_image = "";
                }
                if(!empty($student->student_roll_no)){
                    $rollNo = $student->student_roll_no;
                } else {
                    $rollNo = "----";
                }
                $arr_student_list[] = array(
                    'student_id'            => $student->student_id,
                    'profile'               => $student_image,
                    'student_enroll_number' => $student->student_enroll_number,
                    'student_roll_no'       => $rollNo,
                    'student_name'          => $student->student_name,
                    'student_status'        => $student->student_status,
                    'medium_type'           => $arr_medium[$student->medium_type],
                    'section_id'            => $request->get('section_id'),
                    'student_father_name'   => $student['getParent']->student_father_name,
                );
            }
        }
        return $arr_student_list;
    }

    // Get Student signle data
    function get_student_signle_data($student_id = null) {
        $gender             = \Config::get('custom.student_gender');
        $status             = \Config::get('custom.student_status');
        $arr_student_type   = \Config::get('custom.student_type');
        $arr_medium         = \Config::get('custom.medium_type');
        $student       = [];
        $student_array = [];
        $session_id    = null;
        $session       = get_current_session();
        if (!empty($session))
        {
            $session_id = $session['session_id'];
        }
        $arr_student = Student::where(function($query) use ($student_id)
            {
                if (!empty($student_id))
                {
                    $query->where('student_id', $student_id);
                }
            })->with('getParent')->with('getStudentCaste')->with('getStudentReligion')->with('getStudentNationality')->with('getStudentAcademic')->with('getStudentAcademic.getAdmitSession')->with('getStudentAcademic.getAdmitClass')->with('getStudentAcademic.getAdmitSection')->with('getStudentAcademic.getCurrentSession')->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')->with('getStudentAcademic.getStudentGroup')->with('getStudentAcademic.getStudentStream')->with('getStudentHealth')->with('documents')->with('documents.getDocumentCategory')->get();
    //  p($arr_student);
    
        foreach ($arr_student as $key => $student)
        {
            //p($student['getStudentAcademic']);
            if (!empty($student['student_id']))
            {
                if (!empty($student['student_image']))
                {
                    $profile = check_file_exist($student['student_image'], 'student_image');
                    if (!empty($profile))
                    {
                        $student['profile'] = $profile;
                    }
                }
                $document_list = [];
                foreach ($student['documents'] as $documents)
                {
                    $document_data['student_document_id']       = $documents['student_document_id'];
                    $document_data['document_category_id']      = $documents['document_category_id'];
                    $document_data['document_category_name']    = $documents['getDocumentCategory']['document_category_name'];
                    $document_data['student_document_details']  = $documents['student_document_details'];
                    $document_data['student_document_status']  = $documents['student_document_status']; //@pratyush 10 Aug 2018
                    $document_data['document_submit_date']      = date("d F, Y ", strtotime($documents['created_at']->toDateString()));
                    if (!empty($documents['student_document_file']))
                    {
                        $document_file = check_file_exist($documents['student_document_file'], 'student_document_file');
                        if (!empty($document_file))
                        {
                            $document_data['student_document_file'] = $document_file;
                        } 
                    }else {
                        $document_data['student_document_file'] = "";
                    }
                    $document_list[] = $document_data;
                }
                $student_array[] = array(
                    'student_enroll_number'         => $student['student_enroll_number'],
                    'student_roll_no'               => $student['student_roll_no'],
                    'student_reg_date'              => $student['student_reg_date'],
                    'student_id'                    => $student['student_id'],
                    'student_name'                  => $student['student_name'],
                    'student_email'                 => $student['student_email'],
                    'student_image'                 => $student['profile'],
                    'student_gender'                => $gender[$student['student_gender']],
                    'status'                        => $status[$student['student_status']],
                    'student_gender1'               => $student['student_gender'],
                    'student_dob'                   => $student['student_dob'],
                    'student_category'              => $student['student_category'],
                    'caste_id'                      => $student['caste_id'],
                    'caste_name'                    => $student['getStudentCaste']['caste_name'],
                    'religion_id'                   => $student['religion_id'],
                    'religion_name'                 => $student['getStudentReligion']['religion_name'],
                    'nationality_id'                => $student['nationality_id'],
                    'nationality_name'              => $student['getStudentNationality']['nationality_name'],
                    'student_sibling_name'          => $student['student_sibling_name'],
                    'student_sibling_class_id'      => $student['student_sibling_class_id'],
                    'student_sibling_class_name'    => $student['getSiblingClass']['class_name'],
                    'student_temporary_address'     => $student['student_temporary_address'],
                    'student_temporary_city'        => $student['student_temporary_city'],
                    'student_temporary_state'       => $student['student_temporary_state'],
                    'student_temporary_county'      => $student['student_temporary_county'],
                    'student_temporary_pincode'     => $student['student_temporary_pincode'],
                    'student_permanent_address'     => $student['student_permanent_address'],
                    'student_permanent_city'        => $student['student_permanent_city'],
                    'student_permanent_state'       => $student['student_permanent_state'],
                    'student_permanent_county'      => $student['student_permanent_county'],
                    'student_permanent_pincode'     => $student['student_permanent_pincode'],
                    'student_adhar_card_number'     => $student['student_adhar_card_number'],
                    'medium_type'                   => $student['medium_type'],
                    'medium_type1'                  => $arr_medium[$student['medium_type']],
                    'student_type'                  => $student['student_type'],
                    'student_type1'                 => $arr_student_type[$student['student_type']],
                    'student_privious_school'       => $student['student_privious_school'],
                    'student_privious_class'        => $student['student_privious_class'],
                    'student_privious_tc_no'        => $student['student_privious_tc_no'],
                    'student_privious_tc_date'      => $student['student_privious_tc_date'],
                    'student_privious_result'       => $student['student_privious_result'],

                    
                    'student_father_name'               => $student['getParent']['student_father_name'],
                    'student_father_mobile_number'      => $student['getParent']['student_father_mobile_number'],
                    'student_father_email'              => $student['getParent']['student_father_email'],
                    'student_father_occupation'         => $student['getParent']['student_father_occupation'],
                    'student_father_annual_salary'      => $student['getParent']['student_father_annual_salary'],
                    'student_mother_name'               => $student['getParent']['student_mother_name'],
                    'student_mother_mobile_number'      => $student['getParent']['student_mother_mobile_number'],
                    'student_mother_email'              => $student['getParent']['student_mother_email'],
                    'student_mother_occupation'         => $student['getParent']['student_mother_occupation'],
                    'student_mother_tongue'             => $student['getParent']['student_mother_tongue'],
                    'student_mother_annual_salary'      => $student['getParent']['student_mother_annual_salary'],
                    'student_guardian_name'             => $student['getParent']['student_guardian_name'],
                    'student_guardian_email'            => $student['getParent']['student_guardian_email'],
                    'student_guardian_mobile_number'    => $student['getParent']['student_guardian_mobile_number'],
                    'student_guardian_relation'         => $student['getParent']['student_guardian_relation'],
                    'student_login_name'                => $student['getParent']['student_login_name'],
                    'student_login_email'               => $student['getParent']['student_login_email'],
                    'student_login_contact_no'          => $student['getParent']['student_login_contact_no'],
                    'student_parent_status'             => $student['getParent']['student_parent_status'],

                    'student_unique_id'                 => $student['getStudentAcademic']['student_unique_id'],
                    'admission_session_id'              => $student['getStudentAcademic']['admission_session_id'],
                    'admission_class_id'                => $student['getStudentAcademic']['admission_class_id'],
                    'admission_section_id'              => $student['getStudentAcademic']['admission_section_id'],
                    'current_session_id'                => $student['getStudentAcademic']['current_session_id'],
                    'current_class_id'                  => $student['getStudentAcademic']['current_class_id'],
                    'current_section_id'                => $student['getStudentAcademic']['current_section_id'],
                    'group_id'                          => $student['getStudentAcademic']['group_id'],
                    'stream_id'                         => $student['getStudentAcademic']['stream_id'],

                    'current_session'                   => $student['getStudentAcademic']['getCurrentSession']['session_name'],
                    'current_class'                     => $student['getStudentAcademic']['getCurrentClass']['class_name'],
                    'current_section'                   => $student['getStudentAcademic']['getCurrentSection']['section_name'],
                    'admission_session'                 => $student['getStudentAcademic']['getAdmitSession']['session_name'],
                    'admission_class'                   => $student['getStudentAcademic']['getAdmitClass']['class_name'],
                    'admission_section'                 => $student['getStudentAcademic']['getAdmitSection']['section_name'],
                    'student_group_name'                => $student['getStudentAcademic']['getStudentGroup']['group_name'],
                    'stream_name'                       => $student['getStudentAcademic']['getStudentStream']['stream_name'],


                    'student_height'                    => $student['getStudentHealth']['student_height'],
                    'student_weight'                    => $student['getStudentHealth']['student_weight'],
                    'student_blood_group'               => $student['getStudentHealth']['student_blood_group'],
                    'student_vision_left'               => $student['getStudentHealth']['student_vision_left'],
                    'student_vision_right'              => $student['getStudentHealth']['student_vision_right'],
                    'medical_issues'                    => $student['getStudentHealth']['medical_issues'],

                    'documents'                         => $document_list,
                );
            }
        }
        //p($student_array);
        return $student_array;
    }

    function get_current_session()
    {
        $session_data = [];
        $session      = get_session();
        if (!empty($session))
        {
            foreach ($session as $key => $value)
            {
                $session_data = array(
                    'session_id'   => $key,
                    'session_session' => $value,
                );
            }
        }
        return $session_data;
    }


    function check_file_exist($file_name, $custome_key)
    {
        $return_file        = '';
        $config_upload_path = \Config::get('custom.' . $custome_key);
        if (!empty($file_name))
        {
            if (is_file($config_upload_path['display_path'] . $file_name))
            {
                $return_file = $config_upload_path['display_path'] . $file_name;
            }
        }
        return $return_file;
    }

    function get_academic_year()
    {
        $year_list      = [];
        $current        = new \DateTime();
        $end            = new \DateTime('+ 5 years'); // end year 
        $interval_year  = new DateInterval('P1Y');
        $arr_year_range = new DatePeriod($current, $interval_year, $end);
        foreach ($arr_year_range as $key => $year)
        {
            $year_list[$year->format('Y')] = $year->format('Y');
        }
        return $year_list;
    }

     // Get parent list
     function get_parent_list($student_parent_id=null,$request)
     {
         $loginInfo = get_loggedin_user_data();
         $arr_parent_list = [];
         $arr_parent      = StudentParent::where(function($query) use ($loginInfo,$request) 
         {
             if (!empty($student_parent_id))
             {
                 $query->where('student_parents.student_parent_id', "=", $student_parent_id);
             }
             // For Father Name
             if (!empty($request) && !empty($request->has('father_name')))
             {
                 $query->where('student_father_name', "like", "%{$request->get('father_name')}%");
             }
             
         })
         ->join('students', function($join) use ($request){
             $join->on('students.student_parent_id', '=', 'student_parents.student_parent_id');
             // For Roll No
             if (!empty($request) && !empty($request->has('roll_no')))
             {
                 $join->where('student_roll_no', "like", "%{$request->get('roll_no')}%");
             }
             // For Enroll No
             if (!empty($request) && !empty($request->has('enroll_no')))
             {
                 $join->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
             }
             if (!empty($request) && !empty($request->has('student_name')))
             {
                 $join->where('student_name', "like", "%{$request->get('student_name')}%");
             }
         })
         ->groupBy('students.student_parent_id')->get();
         if (!empty($arr_parent[0]->student_parent_id))
         {
             foreach ($arr_parent as $parent)
             {
                $profile = "";
                if (!empty($parent->student_image))
                {
                    $profile = check_file_exist($parent->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $profile = URL::to($profile);
                    }
                } else {
                    $profile = "";
                }
                $arr_parent_list[] = array(
                    'student_parent_id'                => $parent->student_parent_id,
                    'student_enroll_number'            => $parent->student_enroll_number,
                    'student_roll_no'                  => $parent->student_roll_no,
                    'student_name'                     => $parent->student_name,
                    'profile'                          => $profile,
                    'student_status'                   => $parent->student_status,
                    'student_father_name'              => $parent->student_father_name,
                    'student_father_mobile_number'     => $parent->student_father_mobile_number,
                    'student_father_email'             => $parent->student_father_email,
                    'student_father_occupation'        => $parent->student_father_occupation,
                    'student_father_annual_salary'     => $parent->student_father_annual_salary,
                    'student_mother_name'              => $parent->student_mother_name,
                    'student_mother_mobile_number'     => $parent->student_mother_mobile_number,
                    'student_mother_email'             => $parent->student_mother_email,
                    'student_mother_occupation'        => $parent->student_mother_occupation,
                    'student_mother_annual_salary'     => $parent->student_mother_annual_salary,
                    'student_mother_tongue'            => $parent->student_mother_tongue,
                    'student_guardian_name'            => $parent->student_guardian_name,
                    'student_guardian_email'           => $parent->student_guardian_email,
                    'student_guardian_mobile_number'   => $parent->student_guardian_mobile_number,
                );
                
             }
         }
         return $arr_parent_list;
     }
    /*
    * Funtion Name get_all_student_list_for_virtual_class.
    * To get all student details.
    * @pratyush 30 July 2018.  
    */

    function get_all_student_list_for_virtual_class($request)
    {  

        $loginInfo        = get_loggedin_user_data();
        $arr_student_list = [];
        if($request->get('operation_type') == 1){
            $arr_student      = Student::where(function($query) use ($loginInfo,$request) 
            {
                // For Roll No
                if (!empty($request) && !empty($request->has('roll_no')))
                {
                    $query->where('student_roll_no', "like", "%{$request->get('roll_no')}%");
                }
                // For Enroll No
                if (!empty($request) && !empty($request->has('enroll_no')))
                {
                    $query->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
                }
                // For Student Name
                if (!empty($request) && !empty($request->has('student_name')))
                {
                    $query->where('student_name', "like", "%{$request->get('student_name')}%");
                }
                
            })->join('student_academic_info', function($join) use ($request){
                $join->on('student_academic_info.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null)
                {
                    $join->where('current_section_id', '=',$request->get('section_id'));
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
                {
                    $join->where('current_class_id', '=',$request->get('class_id'));
                }
            })->with('getStudentAcademic')->join('student_parents', function($join) use ($request){
                $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
                if (!empty($request) && !empty($request->has('father_name')))
                {
                    $join->where('student_father_name', "like", "%{$request->get('father_name')}%");
                }
            })->with('getParent')->whereNotExists( function ($query) use ($request) {
                    
                        $query->select(DB::raw(1))
                        ->from('virtual_class_map')
                        ->whereRaw('students.student_id = virtual_class_map.student_id')
                        ->where('virtual_class_map.virtual_class_id', '=', $request->get('virtual_class_id'))
                        ->where('students.student_id', '!=', 'virtual_class_map.student_id');
                    
                })->get();
        //p($arr_student);
        }elseif($request->get('operation_type') == 2){
            $arr_student      = Student::where(function($query) use ($loginInfo,$request) 
            {
                // For Roll No
                if (!empty($request) && !empty($request->has('roll_no')))
                {
                    $query->where('student_roll_no', "like", "%{$request->get('roll_no')}%");
                }
                // For Enroll No
                if (!empty($request) && !empty($request->has('enroll_no')))
                {
                    $query->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
                }
                // For Student Name
                if (!empty($request) && !empty($request->has('student_name')))
                {
                    $query->where('student_name', "like", "%{$request->get('student_name')}%");
                }
                
            })->join('student_academic_info', function($join) use ($request){
                $join->on('student_academic_info.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null)
                {
                    $join->where('current_section_id', '=',$request->get('section_id'));
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
                {   
                    $join->where('current_class_id', '=',$request->get('class_id'));
                }
            })->with('getStudentAcademic')->join('student_parents', function($join) use ($request){
                $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
                if (!empty($request) && !empty($request->has('father_name')))
                {
                    $join->where('student_father_name', "like", "%{$request->get('father_name')}%");
                }
            })->with('getParent')->whereExists( function ($query) use ($request) {
                    
                        $query->select(DB::raw(1))
                        ->from('virtual_class_map')
                        ->whereRaw('students.student_id = virtual_class_map.student_id')
                        ->where('virtual_class_map.virtual_class_id', '=', $request->get('virtual_class_id'));
                })
            ->get();
        }
        //p($arr_student);
        
        if (!empty($arr_student[0]->student_id))
        {
            foreach ($arr_student as $student)
            {
                $student_image = "";
                if (!empty($student->student_image))
                {
                    $profile = check_file_exist($student->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                } else {
                    $student_image = "";
                }

                if($request->has('virtual_class_id')){
                    $arr_student_list[] = array(
                        'student_id'            => $student->student_id,
                        'profile'               => $student_image,
                        'student_enroll_number' => $student->student_enroll_number,
                        'student_roll_no'       => $student->student_roll_no,
                        'student_name'          => $student->student_name,
                        'student_status'        => $student->student_status,
                        'student_father_name'   => $student['getParent']->student_father_name,
                        'student_session'       => $student['getStudentAcademic']['getCurrentSection']->section_name,
                        'student_class'         => $student['getStudentAcademic']['getCurrentClass']->class_name,
                        'virtual_class_id'      => $request->get('virtual_class_id')
                    );
                }else{
                    $arr_student_list[] = array(
                        'student_id'            => $student->student_id,
                        'profile'               => $student_image,
                        'student_enroll_number' => $student->student_enroll_number,
                        'student_roll_no'       => $student->student_roll_no,
                        'student_name'          => $student->student_name,
                        'student_status'        => $student->student_status,
                        'student_father_name'   => $student['getParent']->student_father_name,
                        'student_session'       => $student['getStudentAcademic']['getCurrentSection']->section_name,
                        'student_class'         => $student['getStudentAcademic']['getCurrentClass']->class_name,
                    );
                }
            }
        }
        return $arr_student_list;
    }


    /*
    * Funtion Name get_all_student_list.
    * To get all student details.
    * @pratyush 30 July 2018.  
    */

    function get_all_student_list_competition($request)
    {  
        $competition_id   = get_decrypted_value($request->get('competition_id'), true);
        $loginInfo        = get_loggedin_user_data();
        $arr_student_list = [];
        $competition      = Competition::Find($competition_id);
        // if($request->get('operation_type') == 1){
            $arr_student      = Student::where(function($query) use ($loginInfo,$competition_id,$competition,$request) 
            {
                $query->where('medium_type', '=',$competition['medium_type']);
                // For Roll No
                if (!empty($request) && !empty($request->has('roll_no')))
                {
                    $query->where('student_roll_no', "like", "%{$request->get('roll_no')}%");
                }
                // For Enroll No
                if (!empty($request) && !empty($request->has('enroll_no')))
                {
                    $query->where('student_enroll_number', "like", "%{$request->get('enroll_no')}%");
                }
                // For Student Name
                if (!empty($request) && !empty($request->has('student_name')))
                {
                    $query->where('student_name', "like", "%{$request->get('student_name')}%");
                }
                
            })->join('student_academic_info', function($join) use ($request){
                $join->on('student_academic_info.student_id', '=', 'students.student_id');
                if (!empty($request) && !empty($request->has('section_id')) && $request->get('section_id') != null)
                {
                    $join->where('current_section_id', '=',$request->get('section_id'));
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
                {
                    $join->where('current_class_id', '=',$request->get('class_id'));
                }
            })->with('getStudentAcademic')->join('student_parents', function($join) use ($request){
                $join->on('student_parents.student_parent_id', '=', 'students.student_parent_id');
                if (!empty($request) && !empty($request->has('father_name')))
                {
                    $join->where('student_father_name', "like", "%{$request->get('father_name')}%");
                }
            })->with('getParent')
            ->whereNotExists( function ($query) use ($competition_id) {
              $query->select(DB::raw(1))
                    ->from('competition_map')
                    ->whereRaw('students.student_id = competition_map.student_id')
                    ->where('competition_map.competition_id', '=', $competition_id)
                    ->where('competition_map.position_rank', '!=','');
                    
            })->get();
       
        if (!empty($arr_student[0]->student_id))
        {
            foreach ($arr_student as $student)
            {
                $student_image = "";
                if (!empty($student->student_image))
                {
                    $profile = check_file_exist($student->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $student_image = URL::to($profile);
                    }
                } else {
                    $student_image = "";
                }
                
                    $arr_student_list[] = array(
                        'student_id'            => $student->student_id,
                        'profile'               => $student_image,
                        'student_enroll_number' => $student->student_enroll_number,
                        'student_roll_no'       => $student->student_roll_no,
                        'student_name'          => $student->student_name,
                        'student_status'        => $student->student_status,
                        'student_father_name'   => $student['getParent']->student_father_name,
                        'student_session'       => $student['getStudentAcademic']['getCurrentSection']->section_name,
                        'student_class'         => $student['getStudentAcademic']['getCurrentClass']->class_name,
                        'competition_id'        => $request->get('competition_id')
                    );
            }
        }
        ///p($arr_student_list);
        return $arr_student_list;
    }


    /**
     * Get all staff roles 
     */
    function get_all_staff_roles()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_staff_roles = [];
        $arr_staff_role_list = StaffRoles::where(array('staff_role_status' => 1))->select('staff_role_name', 'staff_role_id')->orderBy('staff_role_name', 'ASC')->get();
        if (!empty($arr_staff_role_list))
        {
            foreach ($arr_staff_role_list as $arr_staff_role)
            {
                $arr_staff_roles[$arr_staff_role['staff_role_id']] = $arr_staff_role['staff_role_name'];
            }
        }
        return $arr_staff_roles;
    }

    /**
     * Get all designations
     */
    function get_all_designations()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_designations = [];
        $arr_designation_list = Designation::where(array('designation_status' => 1))->select('designation_name', 'designation_id')->orderBy('designation_name', 'ASC')->get();
        if (!empty($arr_designation_list))
        {
            foreach ($arr_designation_list as $arr_designation)
            {
                $arr_designations[$arr_designation['designation_id']] = $arr_designation['designation_name'];
            }
        }
        return $arr_designations;
    }

    
    /**
     * Get all subject here 1/8/2018 
     */
    function get_all_subject()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_subjects = [];
        $arr_subject_list = Subject::where(array('subject_status' => 1))->select('subject_name', 'subject_code', 'subject_id')->orderBy('subject_name', 'ASC')->get();
       
        if (!empty($arr_subject_list))
        {
            foreach ($arr_subject_list as $arr_subject)
            {
                $arr_subjects[$arr_subject['subject_id']] = $arr_subject['subject_name'];
            }
        }
        return $arr_subjects;
    }
    /**
     * Get all Staff
     * @Pratyush 06 Aug 2018
     */
    function get_all_staffs()
    {
        $loginInfo  = get_loggedin_user_data();
        $arr_staffs = [];
        $arr_staff_list = Staff::where(array('staff_status' => 1))->select('staff_name', 'staff_id')->orderBy('staff_name', 'ASC')->get();
        if (!empty($arr_staff_list))
        {
            foreach ($arr_staff_list as $arr_staff)
            {
                $arr_staffs[$arr_staff['staff_id']] = $arr_staff['staff_name'];
            }
        }
        return $arr_staffs;
    }

     // Get child list according to parent list
     function get_child_list($student_parent_id)
     {
        $loginInfo = get_loggedin_user_data();
        $arr_child_list = [];
        $arr_child      = Student::where(function($query) use ($loginInfo,$student_parent_id) 
        {
            if (!empty($student_parent_id))
            {
                $query->where('students.student_parent_id', "=", $student_parent_id);
            }
            
        })->with('getStudentAcademic.getCurrentClass')->with('getStudentAcademic.getCurrentSection')->get();
        
         if (!empty($arr_child[0]->student_parent_id))
         {
             foreach ($arr_child as $child)
             {
                $profile = "";
                if (!empty($child->student_image))
                {
                    $profile = check_file_exist($child->student_image, 'student_image');
                    if (!empty($profile))
                    {
                        $profile = URL::to($profile);
                    }
                } else {
                    $profile = "";
                }
                $arr_child_list[] = array(
                    'student_parent_id'                => $child->student_parent_id,
                    'student_id'                       => $child->student_id,
                    'encrypted_student_id'             => get_encrypted_value($child->student_id, true),
                    'student_enroll_number'            => $child->student_enroll_number,
                    'student_roll_no'                  => $child->student_roll_no,
                    'student_name'                     => $child->student_name,
                    'profile'                          => $profile,
                    'student_status'                   => $child->student_status,
                    'student_class_section'            => $child['getStudentAcademic']['getCurrentClass']['class_name'].' - '.$child['getStudentAcademic']['getCurrentSection']['section_name'],

                );
                
             }
         }
         return $arr_child_list;
     }
     /**
     * Get all document category
     */
    function get_all_document_category($type)
    {
        if($type == 0){
            $type1 = 2;
        }
        if($type == 1){
            $type1 = 2;
        }
        $loginInfo              = get_loggedin_user_data();
        $arr_document_categories = [];
        $arr_document_category_list = DocumentCategory::where(['document_category_for' => $type])->orWhere(['document_category_for' => $type1])->where(array('document_category_status' => 1))->select('document_category_name', 'document_category_for', 'document_category_id')->orderBy('document_category_name', 'ASC')->get();
        if (!empty($arr_document_category_list))
        {
            foreach ($arr_document_category_list as $arr_document_category)
            {
                $arr_document_categories[$arr_document_category['document_category_id']] = $arr_document_category['document_category_name'];
            }
        }
        return $arr_document_categories;
    }

    /*  Get class name by class id
    *   @Pratyush 08 Aug 2018.
    */ 

    function get_class_name_by_id($id)
    {
        $loginInfo  = get_loggedin_user_data();
        $decrypted_class_id = get_decrypted_value($id, true);
        $class      = Classes::select('class_name')->where('class_id',$decrypted_class_id)->first();
        $class_name = '';
        if(!empty($class)){
            $class_name = $class->class_name;
        }
        return $class_name;
    }

    /*  Get All Terms 
    *   @Pratyush 11 Aug 2018.
    */ 

    function get_all_terms()
    {
        $loginInfo  = get_loggedin_user_data();
        $arr_term   = [];
        $arr_term_list   = Term::where(array('term_exam_status' => 1))->select('term_exam_name','term_exam_id')->orderBy('term_exam_name', 'ASC')->get();
        
        if (!empty($arr_term_list))
        {
            foreach ($arr_term_list as $arr_term_value)
            {
                $arr_term[$arr_term_value['term_exam_id']] = $arr_term_value['term_exam_name'];
            }
        }
        return $arr_term;
    }

    /*  Get All Cupboard data
    *   @Pratyush 13 Aug 2018.
    */ 

    function get_all_cupbpard_data()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_capboard           = [];

        $arr_cupboard_list  = BookCupboard::where(array('book_cupboard_status' => 1))->select('book_cupboard_name','book_cupboard_id')->orderBy('book_cupboard_name', 'ASC')->get();
        
        if (!empty($arr_cupboard_list))
        {
            foreach ($arr_cupboard_list as $arr_cupboard_value)
            {
                $arr_capboard[$arr_cupboard_value['book_cupboard_id']] = $arr_cupboard_value['book_cupboard_name'];
            }
        }
        return $arr_capboard;
    }

    /*  Get All Book Category
    *   @Pratyush 14 Aug 2018.
    */ 

    function get_all_book_categoory_data()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_book_category      = [];

        $arr_cupboard_list  = BookCategory::where(array('book_category_status' => 1))->select('book_category_name','book_category_id')->orderBy('book_category_name', 'ASC')->get();
        
        if (!empty($arr_cupboard_list))
        {
            foreach ($arr_cupboard_list as $arr_cupboard_value)
            {
                $arr_book_category[$arr_cupboard_value['book_category_id']] = $arr_cupboard_value['book_category_name'];
            }
        }
        return $arr_book_category;
    }

    /*  Get All Book Vendors
    *   @Pratyush 14 Aug 2018.
    */ 

    function get_all_book_vendor_data()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_book_vendor        = [];

        $arr_vendor_list  = BookVendor::where(array('book_vendor_status' => 1))->select('book_vendor_name','book_vendor_id')->orderBy('book_vendor_name', 'ASC')->get();
        
        if (!empty($arr_vendor_list))
        {
            foreach ($arr_vendor_list as $arr_vendor_value)
            {
                $arr_book_vendor[$arr_vendor_value['book_vendor_id']] = $arr_vendor_value['book_vendor_name'];
            }
        }
        return $arr_book_vendor;
    }

    /*  Get CupBoard Shelf
    *   @Pratyush 16 Aug 2018.
    */ 
    function get_cupboard_shelf($cupboard_id=null)
    {
        $loginInfo          = get_loggedin_user_data();
        $cupboard_shelf     = [];
        if(isset($cupboard_id) && $cupboard_id != null){
            $arr_section = BookCupboardshelf::where([['book_cupboardshelf_status', '=', 1],['book_cupboard_id', '=', $cupboard_id]])->select('book_cupboardshelf_id', 'book_cupboardshelf_name')->get();
        } else {
            $arr_section = BookCupboardshelf::where(['book_cupboardshelf_status', '=', 1])->select('book_cupboardshelf_id', 'book_cupboardshelf_name')->get();
        }
        
        if (!empty($arr_section))
        {
            foreach ($arr_section as $value)
            {
                $cupboard_shelf[$value['book_cupboardshelf_id']] = $value['book_cupboardshelf_name'];
            }
        }
        return $cupboard_shelf;
    }

    /*  Get Book Type
    *   @Pratyush 16 Aug 2018.
    */ 
    function get_book_type($cupboard_id=null)
    {
        $book_type       = [];
        $book_type       = array(
                '0' => 'General',
                '1' => 'Barcoded'
            ); 
        return $book_type;
    }

     /**
     * Get all subject here @Ashish 21/8/2018 
     */
    function get_all_exam_type()
    {
        $loginInfo              = get_loggedin_user_data();
        $arr_exam_types = [];
        $arr_exam_type_list = Exam::where(array('exam_status' => 1))->select('exam_name', 'exam_id')->orderBy('exam_name', 'ASC')->get();
       
        if (!empty($arr_exam_type_list))
        {
            foreach ($arr_exam_type_list as $arr_exam_type)
            {
                $arr_exam_types[$arr_exam_type['exam_id']] = $arr_exam_type['exam_name'];
            }
        }
        return $arr_exam_types;
    }

    /**
     * Get all subject here @Ashish 22/8/2018 
     */
    function get_all_subject_mapping($class_id = false)
    {
        
        $loginInfo              = get_loggedin_user_data();
        $arr_subject_mapping = [];
        $arr_subject_mapping_list = SubjectClassMapping::where('class_id','=',$class_id)->with('getSubjects')->get();

       
        if (!empty($arr_subject_mapping_list))
        {
            foreach ($arr_subject_mapping_list as $arr_subject_mappings)
            {
                $arr_subject_mapping[$arr_subject_mappings['subject_id']] = $arr_subject_mappings['getSubjects']['subject_name'];
            }
        }
        return $arr_subject_mapping;
    }

    /**
     * Get all school boards 
     */
    function get_all_school_boards()
    {
        $arr_boards = [];
        $arr_boards_list = SchoolBoard::where(array('board_status' => 1))->select('board_name', 'board_id')->orderBy('board_name', 'ASC')->get();
        if (!empty($arr_boards_list))
        {
            foreach ($arr_boards_list as $arr_board)
            {
                $arr_boards[$arr_board['board_name']] = $arr_board['board_name'];
            }
        }
        return $arr_boards;
    }

    /**
    * Get all country
    */
    function get_all_country()
    {
        $arr_country_data = [];
        $arr_country_list = Country::where(array('country_status' => 1))->select('country_name', 'country_id')->orderBy('country_name', 'ASC')->get();
        if (!empty($arr_country_list))
        {
            foreach ($arr_country_list as $arr_country)
            {
                $arr_country_data[$arr_country['country_id']] = $arr_country['country_name'];
            }
        }
        return $arr_country_data;
    }

    /**
    * Get all states without country id
    */
    function getting_all_states()
    {
        $arr_state_data = [];
        $arr_state_list = State::where(array('state_status' => 1))->select('state_name', 'state_id')->orderBy('state_name', 'DESC')->get();
        if (!empty($arr_state_list))
        {
            foreach ($arr_state_list as $arr_state)
            {
                $arr_state_data[$arr_state['state_id']] = $arr_state['state_name'];
            }
        }
        return $arr_state_data;
    }
    /**
    * Get all states
    */
    function get_all_states($country_id)
    {
        $arr_state_data = [];
        $arr_state_list = State::where(array('state_status' => 1, 'country_id' => $country_id))->select('state_name', 'state_id')->orderBy('state_name', 'ASC')->get();
        if (!empty($arr_state_list))
        {
            foreach ($arr_state_list as $arr_state)
            {
                $arr_state_data[$arr_state['state_id']] = $arr_state['state_name'];
            }
        }
        return $arr_state_data;
    }

    /**
    * Get all city
    */
    function get_all_city($state_id)
    {
        $arr_city_data = [];
        $arr_city_list = City::where(array('city_status' => 1, 'state_id' => $state_id))->select('city_name', 'city_id')->orderBy('city_name', 'ASC')->get();
        if (!empty($arr_city_list))
        {
            foreach ($arr_city_list as $arr_city)
            {
                $arr_city_data[$arr_city['city_id']] = $arr_city['city_name'];
            }
        }
        return $arr_city_data;
    }

    /**
     * Get all classes 
     */
    function get_all_streams($medium_type)
    {
        $arr_streams = [];
        $arr_stream_list = Stream::where(array('stream_status' => 1,'medium_type'=> $medium_type))->select('stream_name', 'stream_id')->orderBy('stream_name', 'ASC')->get();
        if (!empty($arr_stream_list))
        {
            foreach ($arr_stream_list as $arr_stream)
            {
                $arr_streams[$arr_stream['stream_id']] = $arr_stream['stream_name'];
            }
        }
        return $arr_streams;
    }

    /** 
     * Check section capacity
     */

    function check_section_capacity($section_id)
    {
        $sectionInfo = Section::where(array('section_status' => 1,'section_id'=> $section_id))->select('section_intake', 'section_id')->first();

        $studentCount = StudentAcademic::where(array('current_section_id'=> $section_id))->count();
        if($sectionInfo->section_intake > $studentCount){
            return "True";
        } else {
            return "False";
        }
    }

    /** 
     * Get student for roll number assign feature
     */

    function assign_roll_no_by_section($section_id){
        $arr_student_list = [];
        $arr_student      = Student::where(array('students.student_status'=> 1))->join('student_academic_info', function($join) use ($section_id){
            $join->on('student_academic_info.student_id', '=', 'students.student_id');
            $join->where('current_section_id', '=',$section_id);
        })->select('students.student_name', 'students.student_id','students.student_roll_no','student_academic_info.current_section_id')->orderBy('student_name', 'ASC')->get()->toArray();

        $key = 1;
        foreach($arr_student as $student){
            $studentData = Student::find($student['student_id']);
            $studentData->student_roll_no = $key;
            $studentData->save();
            $key++;
        }
        return "Success";
    }

    /**
     * Get all brochures
     */
    function get_all_brochure($session_id)
    {
        $arr_brochures = [];
        $arr_brochure_list = Brochure::where(array('brochure_status' => 1, 'session_id'=> $session_id))->select('brochure_name', 'brochure_id')->orderBy('brochure_id', 'ASC')->get();
        if (!empty($arr_brochure_list))
        {
            foreach ($arr_brochure_list as $arr_brochure)
            {
                $arr_brochures[$arr_brochure['brochure_id']] = $arr_brochure['brochure_name'];
            }
        }
        return $arr_brochures;
    }

    /** 
     * Check class capacity
     */

    function get_total_intake($class_id)
    {
        $totalCount = DB::table('sections')
        ->join('classes', 'sections.class_id', '=', 'classes.class_id')
        ->where('sections.section_status', '=', 1)
        ->where('sections.class_id', '=', $class_id)
        ->sum('sections.section_intake');
       
        $studentCount = StudentAcademic::where(array('current_class_id'=> $class_id))->count();
        
        if($totalCount > $studentCount){
            return $totalCount - $studentCount;
        } else {
            return 0;
        }
    }


?>