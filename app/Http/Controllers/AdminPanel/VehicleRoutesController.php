<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VehicleRoutesController extends Controller
{
     /**
     *  View page for Vehicle Routes
     *  @Khushbu on 28 Sept 2018   
    **/
    public function index()
    {
        $loginInfo 		    = get_loggedin_user_data();
        
        $data 				= array(
            'page_title'    => trans('language.view_vehicle_routes'),
            'redirect_url'  => url('admin-panel/transport/vehicle-route/view-vehicle-routes'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.vehicle-route.view-vehicle-route')->with($data);
    }

    /**
     *  Add page for Vehicle Routes
     *  @Khushbu on 28 Sept 2018   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        // $job        = [];
        if (!empty($id))
        {
            $page_title             = trans('language.edit_vehicle_route');
            $save_url               = url('admin-panel/transport/vehicle-route/save/' . $decrypted_job_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_vehicle_routes');
            $save_url      = url('admin-panel/transport/vehicle-route/save');
            $submit_button = 'Save';
        }

        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/vehicle-route/view-vehicle-routes'),
        );
        return view('admin-panel.vehicle-route.add-vehicle-route')->with($data);
    }

    public function mapRouteVehicle() {
         $loginInfo             = get_loggedin_user_data();
        
        $data               = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.vehicle-route.map-route-vehicle')->with($data);
    }
}
