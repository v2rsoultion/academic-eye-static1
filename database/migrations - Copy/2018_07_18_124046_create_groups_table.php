<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'groups';
    protected $primaryKey = 'group_id';
    public function up()
    {
        if (!Schema::hasTable('groups')) { 
            Schema::create('groups', function (Blueprint $table) {
                $table->increments('group_id')->unsigned();
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->string('group_name', 255)->nullable();
                $table->tinyInteger('group_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('groups', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('groups', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('groups', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
