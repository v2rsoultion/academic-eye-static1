<?php $__env->startSection('content'); ?>
<style type="">

.theme-blush .btn-primary {
    margin-top: 5px !important;
}

.table-responsive{
        overflow-x: visible !important;
    }
</style>

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2><?php echo trans('language.view_interview_schedule'); ?></h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <a href="<?php echo url('admin-panel/recruitment/add-job'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#"><?php echo trans('language.recruitment'); ?></a></li>
                    <li class="breadcrumb-item active"><?php echo trans('language.view_interview_schedule'); ?></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <div class="body form-gap">
                                <?php if(session()->has('success')): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?php echo e(session()->get('success')); ?>

                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif; ?>
                                <?php if($errors->any()): ?>
                                   <div class="alert alert-danger" role="alert">
                                    <?php echo e($errors->first()); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                   </div>      
                                <?php endif; ?>
                                
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th><?php echo e(trans('language.s_no')); ?></th>
                                                <th>Form No</th>
                                                <th><?php echo e(trans('language.job_name')); ?></th>
                                                <th><?php echo e(trans('language.applied_for_job')); ?></th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            <td>1</td>
                                            <td>1001</td>
                                            <td>Class Teacher</td>
                                            <td>447</td>
                                            <td> <div class="dropdown">
                                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="zmdi zmdi-label"></i>
                                            <span class="zmdi zmdi-caret-down"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#largeModal">Schedule</a></li> 
                                            </ul> </div></td>
                                           <!--  <td><a href="#" data-toggle="modal" data-target="#largeModal"><i class="fas fa-clock"></i></a></td> -->
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>1002</td>
                                            <td>Class Teacher</td>
                                            <td>447</td>
                                            <td> <div class="dropdown">
                                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="zmdi zmdi-label"></i>
                                            <span class="zmdi zmdi-caret-down"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#largeModal">Schedule</a></li> 
                                            </ul> </div></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>1003</td>
                                            <td>Accountant</td>
                                            <td>4</td>
                                            <td> <div class="dropdown">
                                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="zmdi zmdi-label"></i>
                                            <span class="zmdi zmdi-caret-down"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#largeModal">Schedule</a></li> 
                                            </ul> </div></td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>1004</td>
                                            <td>Accountant</td>
                                            <td>4</td>
                                            <td> <div class="dropdown">
                                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="zmdi zmdi-label"></i>
                                            <span class="zmdi zmdi-caret-down"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#largeModal">Schedule</a></li> 
                                            </ul> </div></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#recruitment-job-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '<?php echo e(url('admin-panel/recruitment/data')); ?>'
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'job_name', name: 'job_name'},
                {data: 'no_of_vacancy', name: 'no_of_vacancy'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }  

</script>

<!-- model -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" style="padding: 40px;">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content send_messsage_button1">
            <div class="header header_model_2">
                <div class="header_model_1">
                    <h6 style="padding-left:20px;"><strong>Send Message</strong></h6>
                </div>
                <ul class="header-dropdown" style="padding-right: 20px; ">
                    <li class="remove remove_cross_x_2 float-right" style="position: relative; top: 20px;">
                        <a role="button" class="button_1" data-dismiss="modal"><i class="zmdi zmdi-close zmdi_close1"></i></a>
                    </li>
                </ul>
            
            </div>
          <div class="tab-pane active" id="classlist">
              <div class="card form-gap">
                  <div class="body form-gap">
                        <form class="" action="" method="post"  id="">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <!-- <div class="col-lg-3 col-md-3"> -->
                                    <lable class="from_one1"><?php echo trans('language.date_time'); ?> :</lable>
                                    <div class="form-group">
                                        <?php echo Form::text('date_time','', ['class' => 'form-control','placeholder'=>trans('language.date_time'), 'id' => 'schedule']); ?>

                                    </div>
                                    <?php if($errors->has('date_time')): ?> <p class="help-block"><?php echo e($errors->first('date_time')); ?></p> <?php endif; ?>
                                <!-- </div> -->
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 text_area_desc">
                                    <lable class="from_one1">Message :</lable>
                                    <div class="form-group">
                                        <textarea rows="4" name="message" value="" class="form-control no-resize" placeholder="">Dear #CandidateName, you applied for #jobname in #schoolname, your interview is scheduled on #datetime, we request you to be on time with all the Education and work experience related documents.</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <lable class="from_one1">Send Via :</lable>
                                    <div class="form-group">
                                        <div class="radio" style="margin:10px 4px !important;">
                                            <input type="radio" name="radio_option" id="radio1" value="option1">
                                            <label for="radio1" class="document_staff">Email</label>
                                            <input type="radio" name="radio_option" id="radio2" value="option2">
                                            <label for="radio2" class="document_staff">SMS</label>
                                            <input type="radio" name="radio_option" id="radio4" value="option4" checked="">
                                            <label for="radio4" class="document_staff">Both</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button style="margin-top: 5px !important;" type="submit" class="btn btn-danger btn-round waves-effect float-right" data-dismiss="modal">Cancel</button>    
                                    <button type="submit" class="btn btn-raised btn-round btn-primary float-right">Send</button>
                                </div>
                            </div>
                        </form>
                  </div>
              </div>
          </div>


          <!-- <div class="modal-footer">
              <button type="button" class="btn btn-default btn-round waves-effect">SAVE CHANGES</button>
              <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
          </div> -->
      </div>
  </div>
</div>
<style type="text/css">
    .dtp{
        z-index: 999999999999 !important;
    }
</style>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="http://v2rsolution.co.in/school/school-management/admin-panel/assets/plugins/momentjs/moment.js"></script>
<script type="text/javascript" src="http://v2rsolution.co.in/school/school-management/admin-panel/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script>


//For Date & Time
$('#schedule').bootstrapMaterialDatePicker({ 
        
        date: true,shortTime: true,format: 'DD-MM-YYYY - hh:mm a' }).on('change', function(e, date){ $(this).valid(); });
</script>

<?php $__env->stopSection(); ?>





<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>