﻿
<?php $__env->startSection('content'); ?>
<style type="text/css">
    .card .body .table td, .cshelf1 {
        width: 50px !important;
    }
    .pp{
        width: 800px !important;
    }
    .table-responsive {
        overflow-x: visible !important;
    }
    .tabless table {
        margin: 10px 15px;
    }
</style>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Transfer Student</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/hostel'); ?>"><?php echo trans('language.menu_hostel'); ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/hostel'); ?>">Transfer Student</a></li>
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/hostel/transfer-student/add'); ?>">Add</a></li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">

                                <!-- <div class="container-fluid"> -->
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-12">                 
                                            <select class="form-control show-tick select_form1" name="class_name">
                                                <option value="">Hostel Name</option>
                                                <option value="VII">Hostel-1</option>
                                                <option value="VII">Hostel-2</option>
                                            </select>
                                        </div>
                                         <div class="col-lg-2 col-md-2 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="class_name">
                                                <option value="">Class</option>
                                                <option value="VII">XII</option>
                                                <option value="X">VIII</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 ">                 
                                            <select class="form-control show-tick select_form1" name="Class_section">
                                                <option value="">Section</option>
                                                <option value="VII">A</option>
                                                <option value="X">B</option>
                                            </select>
                                        </div>
                                         <div class="col-lg-2 col-md-2 col-sm-12">
                                            <div class="input-group ">
                                                <input type="text" class="form-control" value="" placeholder="Student Name">
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <button class="btn btn-raised btn-primary">Search</button>
                                        </div>
                                        <div class="col-md-1">
                                            <button class="btn btn-raised btn-primary">Clear</button>
                                        </div>
                                    </div>
                                <!-- </div> -->
                                <hr>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Student Name</th>
                                                <th>Father Name</th>
                                                <th>Class - section</th>
                                                <th>Hostel-Block</th>
                                                <th>Floor-Room</th>
                                                <th>Leave Date</th>
                                                
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%"> <a href="<?php echo e(url('/admin-panel/student/student-profile')); ?>" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>Mahesh Kumar</td>
                                                <td>XII-A</td>
                                                <td>Hostel-1 - Block-1</td>
                                                <td>Floor-1 - Room-12</td>
                                                <td>2011/8/16</td>
                                                <td>
                                                <div class="dropdown">
                                                  <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                  <i class="zmdi zmdi-label"></i>
                                                    <span class="zmdi zmdi-caret-down"></span>
                                                  </button>
                                                  <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a href="#" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#transferStudentModel" style="background: transparent !important;">Transfer Student</a>
                                                    </li>
                                                  </ul>
                                                </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%"> <a href="<?php echo e(url('/admin-panel/student/student-profile')); ?>" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>Mahesh Kumar</td>
                                                <td>XII-A</td>
                                                <td>Hostel-2 - Block-1</td>
                                                <td>Floor-2 - Room-10</td>
                                                <td>2011/8/16</td>
                                                <td>
                                                <div class="dropdown">
                                                  <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                  <i class="zmdi zmdi-label"></i>
                                                    <span class="zmdi zmdi-caret-down"></span>
                                                  </button>
                                                  <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a href="#" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#transferStudentModel" style="background: transparent !important;">Transfer Student</a>
                                                    </li>
                                                  </ul>
                                                </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%"> <a href="<?php echo e(url('/admin-panel/student/student-profile')); ?>" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>Mahesh Kumar</td>
                                                <td>XII-A</td>
                                                <td>Hostel-1 - Block-1</td>
                                                <td>Floor-3 - Room-23</td>
                                                <td>2011/8/16</td>
                                                <td>
                                                <div class="dropdown">
                                                  <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                  <i class="zmdi zmdi-label"></i>
                                                    <span class="zmdi zmdi-caret-down"></span>
                                                  </button>
                                                  <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a href="#" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#transferStudentModel" style="background: transparent !important;">Transfer Student</a>
                                                    </li>
                                                  </ul>
                                                </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%"> <a href="<?php echo e(url('/admin-panel/student/student-profile')); ?>" title="Link_to_profile">Ankit Dave</a></td>
                                                <td>Mahesh Kumar</td>
                                                <td>XII-A</td>
                                                <td>Hostel-1 - Block-2</td>
                                                <td>Floor-1 - Romm-2</td>
                                                <td>2011/8/16</td>
                                               <td>
                                                <div class="dropdown">
                                                  <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                  <i class="zmdi zmdi-label"></i>
                                                    <span class="zmdi zmdi-caret-down"></span>
                                                  </button>
                                                  <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a href="#" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#transferStudentModel" style="background: transparent !important;">Transfer Student</a>
                                                    </li>
                                                  </ul>
                                                </div>
                                                </td>
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>




<!-- Action Model  -->
<div class="modal fade" id="transferStudentModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Transfer Student</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-12 ">   
            <lable class="from_one1" for="name">Block Name</lable>               
            <select class="form-control show-tick select_form1" name="class_name" onchange="showTable()">
                <option value="">Block Name</option>
                <option value="1">Block-1</option>
                <option value="0">Block-2</option>
                <option value="0">Block-3</option>
                <option value="0">Block-4</option>
            </select>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 "> 
            <lable class="from_one1" for="name">Room No</lable>                
            <select class="form-control show-tick select_form1" name="Class_section">
                <option value="">Room No</option>
                <option value="VII">Room-1</option>
                <option value="X">Room-2</option>
                <option value="X">Room-3</option>
                <option value="X">Room-4</option>
            </select>
            </div>
        <div class="col-lg-3">
          <div class="form-group">
            <lable class="from_one1" for="name">Joining Date</lable>
            <input type="text" name="name" id="joiningDate" class="form-control" placeholder="Joining Date">
          </div>
        </div>
         <div class="col-lg-3">
          <div class="form-group">
            <lable class="from_one1" for="name">Leaving Date</lable>
            <input type="text" name="name" id="leavingDate" class="form-control" placeholder="Leaving Date">
          </div>
        </div>
    </div>
        <div class="row tabless">
             <div style="width:752px;border: 1px solid #ccc;font-weight: bold; border-radius: 5px; margin: 5px 15px; padding: 5px 10px;">
             <div>Student Name: Ankit Dave</div>
             <div>Block-Room: Block-1-Room10</div>
             <div>Room Capacity: 4</div>
             </div>
            
                <!-- <div class="col-lg-12">
                <div class="row">
                <div class="col-lg-3"><b>Room Capacity: </b></div> <div class="col-lg-1"><b>4</b></div>
                </div>
                <div class="clearfix"></div>
                
                </div> -->
                <table class="table  m-b-0 c_list pp">
            <thead>
              <tr>
                <th style="width: 60px;">S No</th>
                <th style="width: 140px;">Enroll No</th>
                <th>Name</th>
                <th style="width: 260px;">Class-Section</th>
              </tr>
            </thead>
            <tbody>
              <?php $count= 1; for ($i=0; $i < 5; $i++) {  ?>
              <tr>
                <td><?php echo $count; ?></td>
                <td>100A1</td>
                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%"> <a href="<?php echo e(url('/admin-panel/student/student-profile')); ?>" title="Link_to_profile">Ankit Dave</a></td>
                <td>XII - A</td>
              </tr>
              <?php $count++; } ?>
            </tbody>
          </table>
          
        </div>

        <div class="col-lg-12" style="margin-top: 10px;">
             <button type="submit" class="btn btn-raised  btn-primary waves-effect float-right" data-dismiss="modal">Cancel</button>  
            <button class="btn btn-raised btn-primary float-right" style="margin-right: 10px;">Transfer</button>
          
        </div>

      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
    function showTable() {
        <table class="table  m-b-0 c_list pp">
            <thead>
              <tr>
                <th style="width: 60px;">S No</th>
                <th style="width: 140px;">Enroll No</th>
                <th>Name</th>
                <th style="width: 260px;">Class-Section</th>
              </tr>
            </thead>
            <tbody>
              <?php $count= 1; for ($i=0; $i < 5; $i++) {  ?>
              <tr>
                <td><?php echo $count; ?></td>
                <td>100A1</td>
                <td><img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%"> <a href="<?php echo e(url('/admin-panel/student/student-profile')); ?>" title="Link_to_profile">Ankit Dave</a></td>
                <td>XII - A</td>
              </tr>
              <?php $count++; } ?>
            </tbody>
          </table>
    }
</script>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>