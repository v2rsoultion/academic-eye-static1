@if(isset($school['school_id']) && !empty($school['school_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
</style>

{!! Form::hidden('school_id',old('school_id',isset($school['school_id']) ? $school['school_id'] : ''),['class' => 'gui-input', 'id' => 'school_id', 'readonly' => 'true']) !!}

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_name', old('school_name',isset($school['school_name']) ? $school['school_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_name'), 'id' => 'school_name']) !!}
        </div>
        @if ($errors->has('school_name')) <p class="help-block">{{ $errors->first('school_name') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_registration_no') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_registration_no', old('school_registration_no',isset($school['school_registration_no']) ? $school['school_registration_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_registration_no'), 'id' => 'school_registration_no']) !!}
        </div>
        @if ($errors->has('school_registration_no')) <p class="help-block">{{ $errors->first('school_registration_no') }}</p> @endif
    </div>

    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.school_sno_numbers') !!} :</lable>
        <div class="form-group bs-example">
            {!! Form::text('school_sno_numbers', old('school_sno_numbers',isset($school['school_sno_numbers']) ? $school['school_sno_numbers']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_sno_numbers'), 'id' => 'school_sno_numbers','data-role'=>'tagsinput']) !!}
        </div>
        @if ($errors->has('school_sno_numbers')) <p class="help-block">{{ $errors->first('school_sno_numbers') }}</p> @endif
    </div>

</div>

<!-- Staff and board section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_board_of_exams') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_board_of_exams', old('school_board_of_exams',isset($school['school_board_of_exams']) ? $school['school_board_of_exams']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_board_of_exams'), 'id' => 'school_board_of_exams']) !!}
        </div>
        @if ($errors->has('school_board_of_exams')) <p class="help-block">{{ $errors->first('school_board_of_exams') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_medium') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_medium', old('school_medium',isset($school['school_medium']) ? $school['school_medium']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_medium'), 'id' => 'school_medium']) !!}
        </div>
        @if ($errors->has('school_medium')) <p class="help-block">{{ $errors->first('school_medium') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_total_students') !!} :</lable>
        <div class="form-group">
            {!! Form::number('school_total_students', old('school_total_students',isset($school['school_total_students']) ? $school['school_total_students']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_total_students'), 'id' => 'school_total_students', 'min'=> 0]) !!}
        </div>
        @if ($errors->has('school_total_students')) <p class="help-block">{{ $errors->first('school_total_students') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_total_staff') !!} :</lable>
        <div class="form-group">
            {!! Form::number('school_total_staff', old('school_total_staff',isset($school['school_total_staff']) ? $school['school_total_staff']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_total_staff'), 'id' => 'school_total_staff', 'min'=> 0]) !!}
        </div>
        @if ($errors->has('school_total_staff')) <p class="help-block">{{ $errors->first('school_total_staff') }}</p> @endif
    </div>

</div>

<div class="header">
    <h2><strong>Branch</strong> Information</h2>
</div>

<!-- Class & Fees section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_class_from') !!} :</lable>
        <div class="form-group">
            {!! Form::number('school_class_from', old('school_class_from',isset($school['school_class_from']) ? $school['school_class_from']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_class_from'), 'id' => 'school_class_from', 'min'=> 0, 'max' => 12]) !!}
        </div>
        @if ($errors->has('school_class_from')) <p class="help-block">{{ $errors->first('school_class_from') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_class_to') !!} :</lable>
        <div class="form-group">
            {!! Form::number('school_class_to', old('school_class_to',isset($school['school_class_to']) ? $school['school_class_to']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_class_to'), 'id' => 'school_class_to', 'min'=> 0, 'max' => 12]) !!}
        </div>
        @if ($errors->has('school_class_to')) <p class="help-block">{{ $errors->first('school_class_to') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_fee_range_from') !!} :</lable>
        <div class="form-group">
            {!! Form::number('school_fee_range_from', old('school_fee_range_from',isset($school['school_fee_range_from']) ? $school['school_fee_range_from']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_fee_range_from'), 'id' => 'school_fee_range_from' , 'min'=> 0]) !!}
        </div>
        @if ($errors->has('school_fee_range_from')) <p class="help-block">{{ $errors->first('school_fee_range_from') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_fee_range_to') !!} :</lable>
        <div class="form-group">
            {!! Form::number('school_fee_range_to', old('school_fee_range_to',isset($school['school_fee_range_to']) ? $school['school_fee_range_to']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_fee_range_to'), 'id' => 'school_fee_range_to', 'min'=> 0]) !!}
        </div>
        @if ($errors->has('school_fee_range_to')) <p class="help-block">{{ $errors->first('school_fee_range_to') }}</p> @endif
    </div>
</div>

<!-- Facilities section -->
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.school_facilities') !!} :</lable>
        <div class="form-group bs-example">
            {!! Form::text('school_facilities', old('school_facilities',isset($school['school_facilities']) ? $school['school_facilities']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_facilities'), 'id' => 'school_facilities','data-role'=>'tagsinput']) !!}
        </div>
        @if ($errors->has('school_facilities')) <p class="help-block">{{ $errors->first('school_facilities') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_url') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_url', old('school_url',isset($school['school_url']) ? $school['school_url']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_url'), 'id' => 'school_url']) !!}
        </div>
        @if ($errors->has('school_url')) <p class="help-block">{{ $errors->first('school_url') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
    <span class="radioBtnpan">{{trans('language.school_logo')}}</span>
        <label for="file1" class="field file">
            
            <input type="file" class="gui-file" name="school_logo" id="file1" onChange="document.getElementById('school_logo').value = this.value;">
            <input type="hidden" class="gui-input" id="school_logo" placeholder="Upload Photo" readonly>
        </label>
        @if ($errors->has('school_logo')) <p class="help-block">{{ $errors->first('school_logo') }}</p> @endif
        @if(isset($school['logo']))<img src="{{url($school['logo'])}}"  height="100px" width="100px">@endif
    </div>
</div>

<div class="header">
    <h2><strong>Other</strong> Information</h2>
</div>

<!-- Email section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_email', old('school_email',isset($school['school_email']) ? $school['school_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_email'), 'id' => 'school_email']) !!}
        </div>
        @if ($errors->has('school_email')) <p class="help-block">{{ $errors->first('school_email') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_mobile_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_mobile_number', old('school_mobile_number',isset($school['school_mobile_number']) ? $school['school_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_mobile_number'), 'id' => 'school_mobile_number']) !!}
        </div>
        @if ($errors->has('school_mobile_number')) <p class="help-block">{{ $errors->first('school_mobile_number') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_telephone') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_telephone', old('school_telephone',isset($school['school_telephone']) ? $school['school_telephone']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_telephone'), 'id' => 'school_telephone']) !!}
        </div>
        @if ($errors->has('school_telephone')) <p class="help-block">{{ $errors->first('school_telephone') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_fax_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_fax_number', old('school_fax_number',isset($school['school_fax_number']) ? $school['school_fax_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_fax_number'), 'id' => 'school_fax_number']) !!}
        </div>
        @if ($errors->has('school_fax_number')) <p class="help-block">{{ $errors->first('school_fax_number') }}</p> @endif
    </div>
</div>

<!-- Description section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.school_description') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('school_description', old('school_description',isset($school['school_description']) ? $school['school_description']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_description'), 'id' => 'school_description', 'rows'=> 3]) !!}
        </div>
        @if ($errors->has('school_description')) <p class="help-block">{{ $errors->first('school_description') }}</p> @endif
    </div>
</div>

<div class="header">
    <h2><strong>Address</strong> Information</h2>
</div>

<!-- Address section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.school_address') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('school_address', old('school_address',isset($school['school_address']) ? $school['school_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_address'), 'id' => 'school_address', 'rows'=> 2]) !!}
        </div>
        @if ($errors->has('school_address')) <p class="help-block">{{ $errors->first('school_address') }}</p> @endif
    </div>
</div>

<!-- City section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_district') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_district', old('school_district',isset($school['school_district']) ? $school['school_district']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_district'), 'id' => 'school_district']) !!}
        </div>
        @if ($errors->has('school_district')) <p class="help-block">{{ $errors->first('school_district') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_city') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_city', old('school_city',isset($school['school_city']) ? $school['school_city']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_city'), 'id' => 'school_city']) !!}
        </div>
        @if ($errors->has('school_city')) <p class="help-block">{{ $errors->first('school_city') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_state') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_state', old('school_state',isset($school['school_state']) ? $school['school_state']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_state'), 'id' => 'school_state']) !!}
        </div>
        @if ($errors->has('school_state')) <p class="help-block">{{ $errors->first('school_state') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.school_pincode') !!} :</lable>
        <div class="form-group">
            {!! Form::text('school_pincode', old('school_pincode',isset($school['school_pincode']) ? $school['school_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_pincode'), 'id' => 'school_pincode']) !!}
        </div>
        @if ($errors->has('school_pincode')) <p class="help-block">{{ $errors->first('school_pincode') }}</p> @endif
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
        <!-- <button type="submit" class="btn btn-raised btn-round">Cancel</button> -->
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#school-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                school_name: {
                    required: true
                },
                school_registration_no: {
                    required: true
                },
                school_board_of_exams: {
                    required: true
                },
                school_medium: {
                    required: true
                },
                school_class_from: {
                    required: true,
                    digits: true
                },
                school_class_to: {
                    required: true,
                    digits: true
                },
                school_fee_range_from: {
                    required: true,
                    digits: true
                },
                school_fee_range_to: {
                    required: true,
                    digits: true
                },
                school_email: {
                    required: true
                },
                school_mobile_number: {
                    required: true
                },
                school_address: {
                    required: true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        
            var student_id = $("#student_id").val(); 
            if(student_id == '')
            {
                $('#password').valid();
            }
        
        // reset form data
        $('#reset_data').on('click', function (e) {
            $("#student-form")[0].reset()
        });

        
        
    });

</script>