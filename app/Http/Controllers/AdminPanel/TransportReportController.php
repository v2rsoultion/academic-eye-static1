<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransportReportController extends Controller
{
    /**
     *  View Page of Check Vehicle Report
     *  @Khushbu on 02 Oct 2018   
    **/
    public function checkVehicleReport()
    {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'page_title'    => trans('language.check_vehicle_report'),
            'redirect_url'  => url('admin-panel/transport/transport-report/check-vehicle-report'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.transport-report.check-vehicle-report')->with($data);
    }

    /**
     *  View Page of Fees Report
     *  @Khushbu on 02 Oct 2018   
    **/
    public function viewFeesReport()
    {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'page_title'    => trans('language.fees_report'),
            'redirect_url'  => url('admin-panel/transport/transport-report/view-fees-report'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.transport-report.view-fees-report')->with($data);
    }

    /**
     *  View Page of Staff Attendance Report
     *  @Khushbu on 02 Oct 2018   
    **/
    public function viewStaffAttendanceReport()
    {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'page_title'    => trans('language.staff_attendance_report'),
            'redirect_url'  => url('admin-panel/transport/transport-report/view-staff-attendence-report'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.transport-report.view-staff-attendence-report')->with($data);
    }
}
