<!DOCTYPE html>
<html>
<head>
	<title>Print Invoice</title>
 <link rel="stylesheet" href="<?php echo e(url("/public/assets/plugins/bootstrap/css/bootstrap.min.css")); ?>">
	<style type="text/css">
		table
		{
			width: 720px;
			/*border: 1px solid red;*/
		}
		/*tbody {
			background-color: #fff;
		}
		thead {
			background-color: #1f2f60;
			color: #fff;
		}*/
		/*float-right
		{
			float: right;
		}*/
		/*hr{
			width: 700px;
		}*/
		/*body
		{
			width: 1100px;
			margin: auto;
		}*/
		.align {
			text-align: right;
		}
		.footer {
			width: 720px;
			padding: 8px 0px ;
			/*height: 28px;*/
			background-color: #1f2f60;
			text-align: center;
			color: #fff;
			font-size: 10px;

		}
		.clear {
			clear: both;
		} 
    th{
      padding-bottom: 7px;
    }
    td{
      padding-bottom: 7px;
    }
    .border_bottom {
    border-bottom:1px solid #000;
  }
  .border_top {
    border-top:1px solid #000;
  }
  .border_right {
    border-right: 1px solid #000;
  }
  .border_left {
    border-left:1px solid #000;
  }
  .gap {
    margin-right: 10px !important;
  }
	</style>
</head>
<body>
<table>
  <tr>
    <td><img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="80px"></td>
    <td style="width: 640px;text-align: right;"><h2 style="color: #1f2f60;margin: 5px 0px;width: 640px;text-align: right; background-color: #fff;">INVOICE</h2><p class="align_right" style="font-size: 13px;margin: 0px;"> Apex Senior Secondary School<br> E-mail : demo@demomail.com<br> Ph. +91 999 999 9999</p></td>
  </tr>
</table>
<div style="width: 720px; border-bottom: solid #e6e6e6; margin-top:10px!important;height: 1px; margin-bottom: 5px;"></div>
<div class="clear"></div>
<table>
  <tr>
    <td>System Invoice No</td>
    <td> : &nbsp; P131220182 </td>
    <td style="text-align: right;">Invoice No : &nbsp;</td>
    <td style="text-align: right;">  SI-1091 </td>
  </tr>
  <tr>
    <td>Name  </td>
    <td> : &nbsp; Amit</td>
    <td style="text-align: right;">Date  : &nbsp;</td>
    <td style="text-align: right;">  12 Nov 2018</td>
  </tr>
  <!-- <tr>
    <td>Account  </td>
    <td> : &nbsp; HEAD OFFICE </td>
    <td style="text-align: right;">Invoice  : &nbsp;</td>
    <td style="text-align: right;">  SI-1091 </td>
  </tr> -->
</table>
<!-- <hr> -->
<div style="width: 720px; border-bottom: solid #e6e6e6; margin-top:10px!important;height: 1px; margin-bottom: 10px;"></div>
<!-- <div style="width: 720px; border: 0.5px solid #e6e6e6; margin: 10px 0px;"></div> -->
<table style="width: 720px; margin: 10px 0px;">
   <tr>
    <th style="text-align: center;" class="border_top border_right border_left border_bottom">S No</th>
    <th style="text-align: center;" class="border_top border_right border_bottom">Item Name</th>
    <th style="text-align: center;" class="border_top border_right border_bottom">Amount</th>
  </tr>
    <tr>
      <td style="text-align: center;" class="border_right border_left">1</td>
      <td style="text-align: center;" class="border_right">Pen Black</td>
      <td style="text-align: center;" class="border_right"> 180.00</td>
    </tr>
    <tr>
      <td style="text-align: center;" class="border_bottom border_right border_left">2</td>
      <td style="text-align: center;" class="border_bottom border_right">Pen Blue</td>
      <td style="text-align: center;" class="border_bottom border_right"> &nbsp; 25.00</td>
    </tr>
    <tr>
      <td> </td>
      <td style="text-align: right;" class="gap"> Total</td>
      <td style="text-align: center;" class="border_left border_right">205.00</td>
    </tr>
    <tr>
      <td> </td>
      <td style="text-align: right;" class="gap"> Tax(0.00%)</td>
      <td style="text-align: center;" class="border_left border_right">&nbsp;&nbsp;&nbsp; 0.00</td>
    </tr>
    <tr>
      <td> </td>
      <td style="text-align: right;" class="gap">Discount(0.00%)</td>
      <td style="text-align: center;" class="border_left border_right"> &nbsp;&nbsp;&nbsp; 0.00</td>
    </tr>
    <tr>
      <td> </td>
      <td style="text-align: right;" class="gap">Advance</td>
      <td style="text-align: center;" class="border_left border_right"> &nbsp;&nbsp;&nbsp; 0.00</td>
    </tr>
    <tr>
      <td> </td>
      <td style="text-align: right;" class="gap">Net Total</td>
      <td style="text-align: center;" class="border_left border_right border_bottom border_top">205.00</td>
    </tr>
 <!--  <tr style="background-color: #1f2f60; color: #fff;">
    <th style="text-align: center;">S No</th>
    <th style="text-align: center;">Item Name</th>
    <th style="text-align: center;">Amount</th>
  </tr>
    <tr style="background-color: #e6e6e6;">
      <td style="text-align: center;">1</td>
      <td style="text-align: center;">Pen Black</td>
      <td style="text-align: center;"> 180.00</td>
    </tr>
    <tr style="background-color: #e6e6e6;">
      <td style="text-align: center;">2</td>
      <td style="text-align: center;">Pen Blue</td>
      <td style="text-align: center;"> &nbsp; 25.00</td>
    </tr> -->
</table>
<br>

<!-- <table>
  <tr>
    <td colspan="1" style="width: 435px"></td>
    <td>Total</td>
    <td style="text-align: right;">205.00</td>
  </tr>
  <tr>
    <td colspan="1" style="width: 125px"></td>
    <td>Tax(0.00%)</td>
    <td style="text-align: right;"> &nbsp;&nbsp;&nbsp; 0.00</td>
  </tr>
  <tr>
    <td colspan="1" style="width: 125px"></td>
    <td>Discount(0.00%)</td>
    <td style="text-align: right;"> &nbsp;&nbsp;&nbsp; 0.00</td>
  </tr>
  <tr>
    <td colspan="1" style="width: 125px"></td>
    <td>Advance</td>
    <td style="text-align: right;"> &nbsp;&nbsp;&nbsp; 0.00</td>
  </tr>
  <tr>
    <td colspan="1" style="width: 125px"></td>
    <td>Net Total</td>
    <td style="text-align: right;">205.00</td>
  </tr>
</table>
 -->
<br><br>
<div class="footer">Academic Management 2018 © Copyright All Rights Reserved</div>
</body>
</html>