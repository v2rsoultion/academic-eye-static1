<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use App\Model\Country\Country; // Model 
use App\Model\State\State; // Model 
use App\Model\City\City; // Model
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Datatables;

class CityController extends Controller
{
    /**
     *  Get all city according state id
     *  @Khushbu on 08 Sept 2018
    **/
    public function showCity(){
        $state_id  = Input::get('state_id');
        $city      = get_all_city($state_id);
        $data      = view('admin-panel.city.ajax-city-select',compact('city'))->render();
        return response()->json(['options'=>$data]);
    }
    /**
     *  View page for city
     *  @Khushbu on 08 Sept 2018
    **/
    public function index()
    {
        $city               	= [];
        $loginInfo           	= get_loggedin_user_data();
        $arr_country 			= get_all_country();
        $city['arr_country']    = add_blank_option($arr_country, 'Select Country');
        $arr_state 				= [];
        $city['arr_state']      = add_blank_option($arr_state, 'Select State');  
        $data 			        = array(
            'page_title'        => trans('language.view_city'),
            'redirect_url'      => url('admin-panel/city/view-city'),
            'login_info'        => $loginInfo,
            'city'              => $city
        );
        return view('admin-panel.city.index')->with($data);
    }
	/**
     *  Add page for city
     *  @Khushbu on 08 Sept 2018
    **/
    public function add(Request $request, $id = null) 
    {
    	$data    		= [];
        $city	    	= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_city_id    = get_decrypted_value($id, true);
            $city                 = City::join('state', 'state.state_id', '=', 'city.state_id')->join('country', 'country.country_id', '=', 'state.country_id')->select('city.*','state.*','country.*')->where('city_id', $decrypted_city_id)->get();
            $city                 = isset($city[0]) ? $city[0] : [];
            if (!$city)
            {
                return redirect('admin-panel/city/add-city')->withError('City not found!');
            }
            $page_title               = trans('language.edit_city');
            $encrypted_city_id        = get_encrypted_value($city['city_id'], true);
            $save_url                 = url('admin-panel/city/save/' . $encrypted_city_id);
            $submit_button            = 'Update';
            $arr_country 			  = get_all_country();
        	$city['arr_country']      = add_blank_option($arr_country, 'Select Country');
        	$arr_state 				  = get_all_states($city['country_id']);
        	$city['arr_state']    	  = $arr_state;
        }
        else
        {
            $page_title               = trans('language.add_city');
            $save_url                 = url('admin-panel/city/save');
            $submit_button            = 'Save';
            $arr_country 			  = get_all_country();
            $city['arr_country']      = add_blank_option($arr_country, 'Select Country');
            $arr_state                = [];
            $city['arr_state']    	  = add_blank_option($arr_state, 'Select State');
        }
        $data                   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'city'        		=> $city,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/city/view-city'),
        );
        return view('admin-panel.city.add')->with($data);
    }
    /**
     *  Add and update city data
     *  @Khushbu on 08 Sept 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo            = get_loggedin_user_data();
        $decrypted_city_id    = null;
        $decrypted_city_id    = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $city = City::find($decrypted_city_id);
            if (!$city)
            {
                return redirect('admin-panel/city/view-city/')->withError('City not found!');
            }
            $success_msg = 'City updated successfully!';
        }
        else
        { 
            $city      	     = New City;
            $success_msg     = 'City saved successfully!';
        }
       	$state_id   = null;
       	if ($request->has('state_id'))
        {
            $state_id   = Input::get('state_id');
        }
        $validatior            = Validator::make($request->all(), [
                'country_id'   => 'required',
                'state_id'     => 'required',
                'city_name'    => 'required|unique:city,city_name,'. $decrypted_city_id . ',city_id,state_id,' .$state_id,
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $city->state_id       = Input::get('state_id');
                $city->city_name      = Input::get('city_name');
                $city->save();
            }
            catch (\Exception $e)
            {  
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/city/view-city')->withSuccess($success_msg);
    }
    /**
     *  Get Data for view page(Datatables)
     *  @Khushbu on 10 Sept 2018
    **/
    public function anyData(Request $request, $city_id = array())
    {
        $city                = [];
        $loginInfo           = get_loggedin_user_data();
        $city                = City::where(function($query) use ($city_id,$loginInfo,$request) 
            {
                if (!empty($city_id))
                {
                    $query->where('city_id', $city_id);
                }
                if (!empty($request) && !empty($request->get('city_name')))
                {
                    $query->where('city_name', "like", "%{$request->get('city_name')}%");
                }
             })
        ->join('state', function($join) use ($request){
            $join->on('state.state_id', '=', 'city.state_id');
             if (!empty($request) && !empty($request->get('state_id')) && $request->get('state_id') != null) {
                $join->where('state.state_id', "=", $request->get('state_id'));
             } 
        })
        ->join('country', function($join) use ($request){
            $join->on('country.country_id', '=', 'state.country_id');
            if (!empty($request) && !empty($request->get('country_id')) && $request->get('country_id') != null) {
                $join->where('country.country_id', '=', $request->get('country_id'));
            }
        })->get();
        return Datatables::of($city)
            ->addColumn('action', function ($city)
            {
                $encrypted_city_id = get_encrypted_value($city->city_id, true);
                if($city->city_status == 0) {
                    $status    = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';

                } else {
                    $status    = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="city-status/'.$status.'/' . $encrypted_city_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-city/' . $encrypted_city_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-city/' . $encrypted_city_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }
    /**
     *  Destroy City data
     *  @Khushbu on 08 Sept 2018
    **/
    public function destroy($id)
    {
        $city_id = get_decrypted_value($id, true);
        $city    = City::find($city_id);
        if ($city)
        {
            $city->delete();
            $success_msg = "City deleted successfully!";
            return redirect('admin-panel/city/view-city')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "City not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    /**
     *  Change City's status
     *  @Khushbu on 08 Sept 2018
    **/
    public function changeStatus($status,$id)
    {
        $city_id = get_decrypted_value($id, true);
        $city    = City::find($city_id);
        if ($city)
        {
            $city->city_status  = $status;
            $city->save();
            $success_msg = "City status updated successfully!";
            return redirect('admin-panel/city/view-city')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "City not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
