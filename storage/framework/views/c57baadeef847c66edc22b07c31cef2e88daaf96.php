<?php $__env->startSection('content'); ?>
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>View Student Attendance</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <a href="<?php echo url('admin-panel/student/student-attendance/add'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/student'); ?>"><?php echo trans('language.menu_student'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/student'); ?>">Attendance</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/student/student-attendance/view'); ?>">View Student Attendance</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           <div class="body form-gap ">
                               <form>
                                    <div class="row clearfix">
                                      <div class="col-lg-2">
                                        <div class="form-group">
                                          <input type="text" name="roll_no" placeholder="Roll No" class="form-control">
                                        </div>
                                      </div>
                                      <div class="col-lg-2">
                                        <div class="form-group">
                                          <input type="text" name="name" placeholder="Name" class="form-control">
                                        </div>
                                      </div>
                                     <div class="col-lg-2">
                                       <div class="form-group">
                                        <!-- <lable class="from_one1">Class</lable> -->
                                          <select class="form-control show-tick select_form1" name="" id="">
                                            <option value="">Select Class</option>
                                            <option value="1">Class-1</option>
                                            <option value="2">Class-2</option>
                                            <option value="3">Class-3</option>
                                            <option value="4">Class-4</option>
                                          </select>
                                       </div>
                                    </div>
                                     <div class="col-lg-2">
                                       <div class="form-group">
                                        <!-- <lable class="from_one1">Section</lable> -->
                                          <select class="form-control show-tick select_form1" name="" id="">
                                            <option value="">Select Section</option>
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                            <option value="D">D</option>
                                          </select>
                                       </div>
                                    </div> 
                                        <div class="col-lg-1 col-md-1">
                                          <button type="submit" class="btn btn-raised btn-primary" title="Search">Search
                                          </button>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <button type="submit" class="btn btn-raised btn-primary" title="Clear">Clear
                                          </button>
                                        </div>
                                    </div>
                                </form>
                                <hr>
                                <div class="table-responsive">
                                   
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                            <?php echo e(csrf_field()); ?>

                                <thead>
                                    <tr>
                                        <th><?php echo e(trans('language.s_no')); ?></th>
                                        <th>Roll No</th>
                                        <th>Student Name</th>
                                        <th>Attendance</th>
                                        <th>Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                          <td>1</td>
                                          <td>101</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                          <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                                <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/student/student-attendance/view-profile'); ?>">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                          <tr>
                                          <td>2</td>
                                          <td>102</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                            <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff/staff-attendance/view-profile'); ?>">View More</a></li> 
                                                </ul> </div></td>
                                            
                                        </tr>
                                        <tr>
                                          <td>3</td>
                                          <td>103</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                              <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff/staff-attendance/view-profile'); ?>">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                        <tr>
                                          <td>4</td>
                                          <td>104</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                              <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff/staff-attendance/view-profile'); ?>">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                       <tr>
                                          <td>5</td>
                                          <td>105</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                             <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff/staff-attendance/view-profile'); ?>">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                          <tr>
                                          <td>6</td>
                                          <td>106</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                              <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff/staff-attendance/view-profile'); ?>">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                          <tr>
                                          <td>7</td>
                                          <td>107</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                              <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff/staff-attendance/view-profile'); ?>">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                          <tr>
                                          <td>8</td>
                                          <td>108</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                              <td class="">
                                              <button type="button"  data-backdrop="static" data-keyboard="false" class="btn btn-raised btn-primary " data-toggle="modal" data-target="#attendanceModel">
                                              View Attendance
                                              </button></td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff/staff-attendance/view-profile'); ?>">View More</a></li> 
                                                </ul> </div></td>
                                        </tr>
                                </tbody>
                            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#staff-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '<?php echo e(url('admin-panel/staff/data')); ?>',
                data: function (d) {
                    d.class_id = $('input[name="name"]').val();
                    d.section_id = $('select[name="designation_id"]').val();
                }
            },
            //ajax: '<?php echo e(url('admin-panel/class/data')); ?>',
           
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'staff_name', name: 'staff_name'},
                {data: 'staff_designation_name', name: 'staff_designation_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

</script>
<?php $__env->stopSection(); ?>

<div class="modal fade" id="attendanceModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ankit Dave </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <table class="table m-b-0 c_list">
                    <thead>
                      <tr>
                        <th class="text-center">S no</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Attendance</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">20 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                      </tr>
                     <tr>
                        <td class="text-center">2</td>
                        <td class="text-center">21 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: red;"></i> (Absent) </td>
                      </tr>
                      <tr>
                        <td class="text-center">3</td>
                        <td class="text-center">22 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                      </tr>
                      <tr>
                        <td class="text-center">4</td>
                        <td class="text-center">23 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                      </tr>
                      <tr>
                        <td class="text-center">5</td>
                        <td class="text-center">24 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: green;"></i> (Present) </td>
                      </tr>
                      <tr>
                        <td class="text-center">6</td>
                        <td class="text-center">25 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: red;"></i> (Absent) </td>
                      </tr>
                      <tr>
                        <td class="text-center">7</td>
                        <td class="text-center">26 July 2018</td>
                        <td class="text-center"><i class="zmdi zmdi-circle" style="color: red;"></i> (Absent) </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>



<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>