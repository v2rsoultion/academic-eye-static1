<!DOCTYPE html>
<html>
<head>
	<title>School Name Certificate</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">
	<style type="text/css">
	body {
		/*width: 1020px;*/
		width: 720px;
		margin: auto;
		/*margin-top: 20px;*/
	}
	.content_size {
		font-size: 17px;
		/*font-style: italic;*/
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		/*border-radius: 10px;*/
		border: 1px solid #1f2f60;
		padding:10px 40px;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 20px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 0px;
	}
	.rl {
		padding: 0px;
	}
	.bold {
		font-weight: bold;
	}
	.position{
		position: absolute;
		left: 62%;
		top: 23%;

		/*position: relative;*/
		/*left: -115px;*/
		/*left: -100%;*/
	}
	/*table{
		height: 40px;

	}*/
	#border11 tr {
		height: 30px;
	}
	.border_bottom {
		border-bottom:1px solid #000;
	}
	.border_top {
		border-top:1px solid #000;
	}
	.border {
		border-right: 1px solid #000;
	}
	</style>

</head>
<body>
	<div class="header">
	
  		<h1 class="align">APEX SENIOR SECONDARY SCHOOL</h1>
  		
		<hr>

		<table>
			<!-- <tr>
				<td style="width: 20%" class="bold content_size">Receipt No. </td>
				<td style="width: 30%" class="content_size bold">1</td>
				<td style="width: 15%"> </td>
				<td style="width: 48%;position: relative;" class="bold content_size">Dated :</td>
				<td class="content_size position">28/11/2011</td>
			</tr> -->
			<tr>
				<td style="width: 25%" class="bold content_size">Receipt No. </td>
				<td style="width: 20%" class="content_size bold">1</td>
				<td style="width: 10%"> </td>
				<td style="width: 47%" class="bold content_size">Dated :</td>
				<td class="content_size">28/11/2011</td>
			</tr>
			
			<tr>
				<td class="bold content_size">Name</td>
				<td class="content_size">Sarthak Pundir</td>
			</tr>
			<tr>
				<td class="bold content_size">Adm No </td>
				<td class="content_size">2381</td>
				<td> </td>
				<td class="bold content_size">For the Month/Year of</td>
			</tr>
			<tr>
				<td class="bold content_size">Class</td>
				<td class="content_size">First</td>
				<td> </td>
				<td class=" content_size">April, May, June, July, August, September, October, November, December2011</td>
			</tr>
		</table>
		<table style="width: 100%" id="border11" cellspacing="0px">
			<tr class="align">
				<th class="border border_top border_bottom">S No.</th>
				<th class="border_top border_bottom">Feehead</th>
				<th class="border_top border_bottom">Paid</th>
			</tr>
			
			<tbody class="align">
				<tr style="height: 23px;">
					<td class="border">1</td>
					<td>admission</td>
					<td>&nbsp;&nbsp;&nbsp;400.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="border">2</td>
					<td>building</td>
					<td>&nbsp;&nbsp;&nbsp;150.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="border">3</td>
					<td>games & sports</td>
					<td>&nbsp;&nbsp;&nbsp;200.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="border">4</td>
					<td>admission</td>
					<td>&nbsp;&nbsp;&nbsp;400.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="border">5</td>
					<td>library</td>
					<td>&nbsp;&nbsp;&nbsp;150.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="border">6</td>
					<td>play way / sciet equip</td>
					<td>&nbsp;&nbsp;&nbsp;150.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="border">7</td>
					<td>student fund</td>
					<td>&nbsp;&nbsp;&nbsp;450.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="border">8</td>
					<td>school journal</td>
					<td>&nbsp;&nbsp;&nbsp;125.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="border">9</td>
					<td>security</td>
					<td>1000.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="border">10</td>
					<td>Computer Fees</td>
					<td>&nbsp;&nbsp;&nbsp;675.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="border border_bottom">11</td>
					<td class="border_bottom">Tuition Fee</td>
					<td class="border_bottom">6,750.00</td>
				</tr>
			</tbody>
		</table>
		<table cellspacing="0px" id="border11" style="width: 100%">
		<tr>
			<td style="width: 62%;" class="align_right border_bottom">Fine/Other:</td>
			<td style="width: 24.9%;" class="align border_bottom">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.00</td>
		</tr>
		<tr>
			<td class="align_right border_bottom">Total Charges:</td>
			<td class="align border_bottom">10,250.00</td>
		</tr>
		<tr>
			<td class="align_right border_bottom">Total:</td>
			<td class="align border_bottom">10,250.00</td>
		</tr>
		<tr>
			<td class="align_right border_bottom">Balance:</td>
			<td class="align border_bottom">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.00</td>  
		</tr>
		</table>
		<!-- <table cellspacing="0px" id="border11">
		<tr>
			<td style="width: 700px;" class="align_right border_bottom">Fine/Other:</td>
			<td style="width: 232px;" class="align border_bottom">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.00</td>
		</tr>
		<tr>
			<td class="align_right border_bottom">Total Charges:</td>
			<td class="align border_bottom">10,250.00</td>
		</tr>
		<tr>
			<td class="align_right border_bottom">Total:</td>
			<td class="align border_bottom">10,250.00</td>
		</tr>
		<tr>
			<td class="align_right border_bottom">Balance:</td>
			<td class="align border_bottom">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.00</td>  
		</tr>
		</table> -->
		<p style="padding-top: 24px;" class="bold align_right">Account Clerk</p>
		
		
	</div>
</body>
</html>

<script type="text/javascript">
	window.print();
</script>