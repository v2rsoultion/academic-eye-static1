@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2>{!! trans('language.menu_task_manager') !!}</h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('admin-panel/menu/task-manager') }}">{!! trans('language.menu_task_manager') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/Task Manager/Task.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Task Manager</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('admin-panel/task-manager/add-task') }}" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="{{ url('admin-panel/task-manager/view-task') }}" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                        <!-- <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="#" title="">Option 1</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 2</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 3</a>
                              </li>
                            </ul>
                          </li>
                        </ul> -->
                        <div class="clearfix"></div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </section>
@endsection

