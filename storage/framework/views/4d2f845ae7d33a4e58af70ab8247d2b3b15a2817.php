﻿
<?php $__env->startSection('content'); ?>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h2>Add Vehicle Document</h2>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-12 line"> 
            <a href="<?php echo url('admin-panel/transport/vehicle-documents/view-vehicle-documents'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-eye"></i> </a>               
                <ul class="breadcrumb float-md-right">
                  <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Transport</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Vehicle Document</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/transport/vehicle-documents/add-vehicle-document'); ?>">Add Vehicle Document</a></li>
                </ul>                
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="headingcommon  col-lg-12" style="padding-bottom: 0px !important">Add Vehicle Document :-</div>
                    <div class="body form-gap">
                        <form class="" action="" method=""  id="contact_form">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-4 col-sm-12">
                                    <lable class="from_one1">Name :</lable>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Name"  name="name">
                                    </div>
                                </div>
                                                
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-raised saveBtn btn-primary ">Save</button>
                                </div>
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-raised btn-primary cancelBtn">Cancel</button>
                                </div>
                            </div>
                        </form>
                        <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div>
                       
                      <div class="headingcommon  col-lg-12" style="margin-left: -20px !important;">Search By :-</div>
                      <form class="" action="" method="" id="" style="width: 100%;">
                        <div class="row clearfix" >
                        <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Name</lable>
                          <input type="text" name="name" id="" class="form-control" placeholder="Name">
                        </div>
                      </div>
                          <div class="col-lg-1 ">
                          <button type="submit" class="btn btn-raised  btn-primary saveBtn" title="Submit">Search
                          </button>
                        </div>
                        <div class="col-lg-1">
                            <button type="reset" class="btn btn-raised btn-primary cancelBtn">Clear</button>
                        </div>
                        </div>
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                        <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div>
                            <div class="body form-gap">

                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Name</th>
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Action & Adventure</td>
                                                <td>  
                                                 <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>  
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>  
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Action & Adventure</td>
                                                <td>    
                                                  <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>  
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Action & Adventure</td>
                                                <td>
                                                 <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Action & Adventure</td>
                                                <td>
                                                 <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>    
                                                <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>



<!-- status fuction -->

<script>

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                        stringLength: {
                        min: 4,
                    },
                        notEmpty: {
                        message: 'Please fill required name'
                    }
                }
            },

            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>