<?php if(isset($school['school_id']) && !empty($school['school_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?>


<?php echo Form::hidden('school_id',old('school_id',isset($school['school_id']) ? $school['school_id'] : ''),['class' => 'gui-input', 'id' => 'school_id', 'readonly' => 'true']); ?>

<style type="">

.theme-blush .btn-primary {
    margin-top: 2px !important;
}
.btn-round.btn-simple {
    padding: 5px 22px !important;
}
</style>

<?php if($errors->any()): ?>
<div class="alert alert-danger" role="alert">
    <?php echo e($errors->first()); ?>

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_name'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('school_name', old('school_name',isset($school['school_name']) ? $school['school_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_name'), 'id' => 'school_name']); ?>

        </div>
        <?php if($errors->has('school_name')): ?> <p class="help-block"><?php echo e($errors->first('school_name')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_registration_no'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('school_registration_no', old('school_registration_no',isset($school['school_registration_no']) ? $school['school_registration_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_registration_no'), 'id' => 'school_registration_no']); ?>

        </div>
        <?php if($errors->has('school_registration_no')): ?> <p class="help-block"><?php echo e($errors->first('school_registration_no')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-6 col-md-6">
        <lable class="from_one1"><?php echo trans('language.school_sno_numbers'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('school_sno_numbers', old('school_sno_numbers',isset($school['school_sno_numbers']) ? $school['school_sno_numbers']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_sno_numbers'), 'id' => 'school_sno_numbers']); ?>

        </div>
        <?php if($errors->has('school_sno_numbers')): ?> <p class="help-block"><?php echo e($errors->first('school_sno_numbers')); ?></p> <?php endif; ?>
    </div>
<!-- ,'data-role'=>'tagsinput' -->
</div>

<!-- Staff and board section -->
<div class="row clearfix">

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_board_of_exams'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('school_board_of_exams[]', $school['arr_board'],isset($school['school_board_of_exams']) ? $school['school_board_of_exams'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'school_board_of_exams','multiple']); ?>

                <i class="arrow double"></i>
            </label>
            <?php if($errors->has('school_board_of_exams')): ?> <p class="help-block"><?php echo e($errors->first('school_board_of_exams')); ?></p> <?php endif; ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_medium'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('school_medium', $school['arr_medium'],isset($school['school_medium']) ? $school['school_medium'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'school_medium']); ?>

                <i class="arrow double"></i>
            </label>
            <?php if($errors->has('school_medium')): ?> <p class="help-block"><?php echo e($errors->first('school_medium')); ?></p> <?php endif; ?>
        </div>
    </div>
    

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_total_students'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::number('school_total_students', old('school_total_students',isset($school['school_total_students']) ? $school['school_total_students']: 0), ['class' => 'form-control','placeholder'=>trans('language.school_total_students'), 'id' => 'school_total_students', 'min'=> 0]); ?>

        </div>
        <?php if($errors->has('school_total_students')): ?> <p class="help-block"><?php echo e($errors->first('school_total_students')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_total_staff'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::number('school_total_staff', old('school_total_staff',isset($school['school_total_staff']) ? $school['school_total_staff']: 0), ['class' => 'form-control','placeholder'=>trans('language.school_total_staff'), 'id' => 'school_total_staff', 'min'=> 0]); ?>

        </div>
        <?php if($errors->has('school_total_staff')): ?> <p class="help-block"><?php echo e($errors->first('school_total_staff')); ?></p> <?php endif; ?>
    </div>

</div>

<div class="header">
    <h2><strong>Branch</strong> Information</h2>
</div>

<!-- Class & Fees section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_class_from'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::number('school_class_from', old('school_class_from',isset($school['school_class_from']) ? $school['school_class_from']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_class_from'), 'id' => 'school_class_from', 'min'=> 0, 'max' => 12]); ?>

        </div>
        <?php if($errors->has('school_class_from')): ?> <p class="help-block"><?php echo e($errors->first('school_class_from')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_class_to'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::number('school_class_to', old('school_class_to',isset($school['school_class_to']) ? $school['school_class_to']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_class_to'), 'id' => 'school_class_to', 'min'=> 0, 'max' => 12]); ?>

        </div>
        <?php if($errors->has('school_class_to')): ?> <p class="help-block"><?php echo e($errors->first('school_class_to')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_fee_range_from'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::number('school_fee_range_from', old('school_fee_range_from',isset($school['school_fee_range_from']) ? $school['school_fee_range_from']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_fee_range_from'), 'id' => 'school_fee_range_from' , 'min'=> 0]); ?>

        </div>
        <?php if($errors->has('school_fee_range_from')): ?> <p class="help-block"><?php echo e($errors->first('school_fee_range_from')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_fee_range_to'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::number('school_fee_range_to', old('school_fee_range_to',isset($school['school_fee_range_to']) ? $school['school_fee_range_to']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_fee_range_to'), 'id' => 'school_fee_range_to', 'min'=> 0]); ?>

        </div>
        <?php if($errors->has('school_fee_range_to')): ?> <p class="help-block"><?php echo e($errors->first('school_fee_range_to')); ?></p> <?php endif; ?>
    </div>
</div>

<!-- Facilities section -->
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1"><?php echo trans('language.school_facilities'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('school_facilities', old('school_facilities',isset($school['school_facilities']) ? $school['school_facilities']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_facilities'), 'id' => 'school_facilities']); ?>

        </div>
        <?php if($errors->has('school_facilities')): ?> <p class="help-block"><?php echo e($errors->first('school_facilities')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_url'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('school_url', old('school_url',isset($school['school_url']) ? $school['school_url']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_url'), 'id' => 'school_url']); ?>

        </div>
        <?php if($errors->has('school_url')): ?> <p class="help-block"><?php echo e($errors->first('school_url')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-3 col-md-3">
    <span class="radioBtnpan"><?php echo e(trans('language.school_logo')); ?></span>
        <label for="file1" class="field file">
            
            <input type="file" class="gui-file" name="school_logo" id="file1" onChange="document.getElementById('school_logo').value = this.value;">
            <input type="hidden" class="gui-input" id="school_logo" placeholder="Upload Photo" readonly>
        </label>
        <?php if($errors->has('school_logo')): ?> <p class="help-block"><?php echo e($errors->first('school_logo')); ?></p> <?php endif; ?>
        <?php if(isset($school['logo'])): ?><img src="<?php echo e(url($school['logo'])); ?>"  height="100px" width="100px"><?php endif; ?>
    </div>
</div>

<div class="header">
    <h2><strong>Other</strong> Information</h2>
</div>

<!-- Email section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_email'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('school_email', old('school_email',isset($school['school_email']) ? $school['school_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_email'), 'id' => 'school_email']); ?>

        </div>
        <?php if($errors->has('school_email')): ?> <p class="help-block"><?php echo e($errors->first('school_email')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_mobile_number'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('school_mobile_number', old('school_mobile_number',isset($school['school_mobile_number']) ? $school['school_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_mobile_number'), 'id' => 'school_mobile_number']); ?>

        </div>
        <?php if($errors->has('school_mobile_number')): ?> <p class="help-block"><?php echo e($errors->first('school_mobile_number')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_telephone'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('school_telephone', old('school_telephone',isset($school['school_telephone']) ? $school['school_telephone']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_telephone'), 'id' => 'school_telephone']); ?>

        </div>
        <?php if($errors->has('school_telephone')): ?> <p class="help-block"><?php echo e($errors->first('school_telephone')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_fax_number'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('school_fax_number', old('school_fax_number',isset($school['school_fax_number']) ? $school['school_fax_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_fax_number'), 'id' => 'school_fax_number']); ?>

        </div>
        <?php if($errors->has('school_fax_number')): ?> <p class="help-block"><?php echo e($errors->first('school_fax_number')); ?></p> <?php endif; ?>
    </div>
</div>

<!-- Description section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1"><?php echo trans('language.school_description'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::textarea('school_description', old('school_description',isset($school['school_description']) ? $school['school_description']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_description'), 'id' => 'school_description', 'rows'=> 3]); ?>

        </div>
        <?php if($errors->has('school_description')): ?> <p class="help-block"><?php echo e($errors->first('school_description')); ?></p> <?php endif; ?>
    </div>
</div>

<div class="header">
    <h2><strong>Address</strong> Information</h2>
</div>

<!-- Address section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1"><?php echo trans('language.school_address'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::textarea('school_address', old('school_address',isset($school['school_address']) ? $school['school_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_address'), 'id' => 'school_address', 'rows'=> 2]); ?>

        </div>
        <?php if($errors->has('school_address')): ?> <p class="help-block"><?php echo e($errors->first('school_address')); ?></p> <?php endif; ?>
    </div>
</div>

<!-- City section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.country'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('country_id', $school['arr_country'],isset($school['country_id']) ? $school['country_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'country_id', 'onChange'=> 'showStates(this.value,"state_id")']); ?>

                <i class="arrow double"></i>
            </label>
        </div>
        <?php if($errors->has('country_id')): ?> <p class="help-block"><?php echo e($errors->first('country_id')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.state'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('state_id', $school['arr_state'],isset($school['state_id']) ? $school['state_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'state_id' , 'onChange'=> 'showCity(this.value,"city_id")']); ?>

                <i class="arrow double"></i>
            </label>
        </div>
        <?php if($errors->has('state_id')): ?> <p class="help-block"><?php echo e($errors->first('state_id')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.city'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('city_id', $school['arr_city'],isset($school['city_id']) ? $school['city_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'city_id']); ?>

                <i class="arrow double"></i>
            </label>
        </div>
        <?php if($errors->has('city_id')): ?> <p class="help-block"><?php echo e($errors->first('city_id')); ?></p> <?php endif; ?>
    </div>
    

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.school_pincode'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('school_pincode', old('school_pincode',isset($school['school_pincode']) ? $school['school_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.school_pincode'), 'id' => 'school_pincode']); ?>

        </div>
        <?php if($errors->has('school_pincode')): ?> <p class="help-block"><?php echo e($errors->first('school_pincode')); ?></p> <?php endif; ?>
    </div>

</div>


<div class="header">
    <h2><strong>Images</strong> </h2>
</div>

<div class="row clearfix">
    <?php 
        for($i = 1; $i <= 6; $i++) {
    ?>
    <div class="col-lg-3 col-md-3">
        <span class="radioBtnpan"><?php echo e(trans('language.school_image')); ?></span>
        <label for="file1" class="field file">
            
            <input type="file" class="gui-file" name="school_img<?php echo e($i); ?>" id="school_file<?php echo e($i); ?>" >
            <input type="hidden" class="gui-input" id="school_img<?php echo e($i); ?>" placeholder="Upload Photo" readonly>
        </label>
        <?php if($errors->has('school_img'.$i)): ?> <p class="help-block"><?php echo e($errors->first('school_img1')); ?></p> <?php endif; ?>
        <?php if(isset($school['school_img'.$i])): ?><img src="<?php echo e(url($school['school_img'.$i])); ?>"  height="100px" width="100px"><?php endif; ?>
    </div>
    <?php if($i == 4): ?>

    </div><br /><br /><div class="row clearfix">
    <?php endif; ?>
    <?php 
        }
    ?>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']); ?>

        <a href="<?php echo url('admin-panel/dashboard'); ?>" class="btn btn-raised" >Cancel</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2().on('change', function(e, data){ $(this).valid(); });
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
        }, "Please use only alphanumeric values");
        $("#school-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation  rules 
             ------------------------------------------ */

            rules: {
                school_name: {
                    required: true
                },
                school_registration_no: {
                    required: true
                },
                school_board_of_exams: {
                    required: true
                },
                school_medium: {
                    required: true
                },
                school_class_from: {
                    required: true,
                    digits: true
                },
                school_class_to: {
                    required: true,
                    digits: true
                },
                school_fee_range_from: {
                    required: true,
                    digits: true
                },
                school_fee_range_to: {
                    required: true,
                    digits: true
                },
                school_email: {
                    required: true
                },
                school_mobile_number: {
                    required: true
                },
                school_address: {
                    required: true
                }
            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                }
            }
        });
        
    });
    function showStates(country_id,name){
        if(country_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/state/show-state')); ?>",
                type: 'GET',
                data: {
                    'country_id': country_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select State</option>');
            $(".mycustloading").hide();
        }
    }

    function showCity(state_id,name){
        if(state_id != "" && name != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/city/show-city')); ?>",
                type: 'GET',
                data: {
                    'state_id': state_id
                },
                success: function (data) {
                    $("#"+name).html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#"+name).html('<option value="">Select City</option>');
            $(".mycustloading").hide();
        }
    }

</script>