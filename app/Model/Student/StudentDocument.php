<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class StudentDocument extends Model
{
    protected $table      = 'student_documents';
    protected $primaryKey = 'student_document_id';
    protected $dates = ['created_at'];

    public function getDocumentCategory()
    {
        return $this->belongsTo('App\Model\DocumentCategory\DocumentCategory','document_category_id');
    }

}
