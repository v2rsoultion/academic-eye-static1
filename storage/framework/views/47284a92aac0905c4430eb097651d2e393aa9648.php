
<?php $__env->startSection('content'); ?>
<style type="text/css">
  td{
    padding: 12px !important;
  }
  .modal-dialog {
    max-width: 550px !important;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Purchase Entry</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/inventory'); ?>">Inventory</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/inventory/view-purchase-entry'); ?>">View Purchase Entry</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                  
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">View Purchase Entry :-</div> -->
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-2">
                        <div class="form-group">
                          <input type="text" name="system_invoice_no" id="" class="form-control" placeholder="System Invoice No">
                        </div>
                      </div>
                       <div class="col-lg-2">
                        <div class="form-group">
                          <input type="text" name="invoice_no" id="" class="form-control" placeholder="Invoice No">
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                          <input type="text" name="" id="date" class="form-control" placeholder="Date">
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                            <select class="form-control show-tick select_form1" name="vendor" id="">
                              <option value="">Select Vendor</option>
                              <?php $count = 1; for ($i=0; $i < 4 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Vendor <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div>
                      </div>
                       <div class="col-lg-2">
                        <div class="form-group">
                            <select class="form-control show-tick select_form1" name="category" id="">
                              <option value="">Select Category</option>
                              <option value="0">Root Category</option>
                              <?php $count = 1; for ($i=0; $i < 4 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Category <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div>
                      </div>
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary" title="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                
                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <!-- <hr> -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>System Invoice</th>
                        <th>Invoice No</th>
                        <th>Date</th>
                        <th>Vendor</th>
                        <th>Category</th>
                        <!-- <th style="width: 108px">Payment Mode</th> -->
                        <!-- <th>Tax</th>
                        <th>Discount</th>
                        <th>Amount</th> -->
                        <!-- <th>Item Details</th> -->
                        <th style="width: 150px;" class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $count = 1; for ($i=0; $i < 15; $i++) {  ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td>P13122018<?php echo $count; ?></td>
                        <td>PI-102<?php echo $count; ?></td>
                        <td>23-05-2018</td>
                        <td>Vendor- <?php echo $count; ?></td>
                        <td>Pen- Black pen</td>
                        <!-- <td>Credit</td>
                        <td>15%</td>
                        <td>10%</td>
                        <td>10000</td> -->
                        <!-- <td style="text-align: center;"><a href="<?php echo e(url('admin-panel/inventory/view-purchase-entry/item-details')); ?>" class="btn btn-raised btn-primary">Item Details</a></td> -->
                        <td class="text-center">
                          <div class="dropdown">
                            <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                            <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li> <a title="Item Details" href="<?php echo e(url('admin-panel/inventory/view-purchase-entry/item-details')); ?>">Item Details</a></li> 
                                <li> <a title="More Details" href="#" data-toggle="modal" data-target="#MoreDetailsModel">More Details</a></li> 
                            </ul> 
                          </div>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                          
                        </td>
                      </tr>
                      <?php $count++; } ?>
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<div class="modal fade" id="MoreDetailsModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">More Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
           <div style="width:502px;border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;margin-left: 0px;">
                <div><b>System Invoice No:</b> P131220181</div>
                <div><b>Invoice No:</b> PI-1029</div>
                <div><b>Payment Mode:</b> Cheque</div>
                <div style="font-size: 14px;margin-left: 20px;">Bank Name: SBI <br> Cheque No: <b>"12354"</b><br> Cheque Date: 13-12-2018 <br> Cheque Amount: 20000</div>
                <div><b>Payable Amount:</b> 12000</div>
                <div><b>Tax:</b> 10%</div>
                <div><b>Discount:</b> 15%</div>
                <div><b>Total Amount:</b> 11220</div>
             </div>
      
      </div>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>