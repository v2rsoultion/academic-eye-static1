<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'holidays';
    protected $primaryKey = 'holiday_id';
    public function up()
    {
        if (!Schema::hasTable('holidays')) {
           
            Schema::create('holidays', function (Blueprint $table) {
                $table->increments('holiday_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('session_id')->unsigned()->nullable();
                $table->string('holiday_name', 255)->nullable();
                $table->date('holiday_start_date')->nullable();
                $table->date('holiday_end_date')->nullable();
                $table->tinyInteger('holiday_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('holidays', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('holidays', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('holidays', function($table) {
                $table->foreign('session_id')->references('session_id')->on('sessions')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('holidays');
    }
}
