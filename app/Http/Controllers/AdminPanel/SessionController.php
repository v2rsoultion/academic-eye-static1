<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Session\Session;

use Yajra\Datatables\Datatables;

class SessionController extends Controller
{
    public function __construct()
    {
        
    }

    /**
     *  View page for session
     *  @Shree on 17 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'   => trans('language.view_session'),
            'redirect_url' => url('admin-panel/session/view-sessions'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.session.index')->with($data);
    }

    /**
     *  Add page for session
     *  @Shree on 16 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    = [];
        $session = [];
        $loginInfo = get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_session_id = get_decrypted_value($id, true);
            $session              = Session::Find($decrypted_session_id);
            if (!$session)
            {
                return redirect('admin-panel/session/add-session')->withError('Session not found!');
            }
            $page_title           = trans('language.edit_session');
            $encrypted_session_id = get_encrypted_value($session->session_id, true);
            $save_url             = url('admin-panel/session/save/' . $encrypted_session_id);
            $submit_button        = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_session');
            $save_url      = url('admin-panel/session/save');
            $submit_button = 'Save';
        }
        $arr_current_session            = \Config::get('custom.current_session');
        $session['arr_current_session'] = $arr_current_session;
        $data                           = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'session'       => $session,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/session/view-sessions'),
        );
        return view('admin-panel.session.add')->with($data);
    }

    /**
     *  Add and update session's data
     *  @Shree on 17 July 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo = get_loggedin_user_data();
        $admin_id = $loginInfo['admin_id'];
        $decrypted_session_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $session = Session::find($decrypted_session_id);
            $admin_id = $session['admin_id'];
            if (!$session)
            {
                return redirect('/admin-panel/session/add-session/')->withError('Session not found!');
            }
            $success_msg = 'Session updated successfully!';
        }
        else
        {
            $session     = New Session;
            $success_msg = 'Session saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'session_name'   => 'required|unique:sessions,session_name,' . $decrypted_session_id . ',session_id',
                'session_start_date'     => 'required|date|date_format:Y-m-d',
                'session_end_date'       => 'required|date|date_format:Y-m-d'
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $session->admin_id              = $admin_id;
                $session->update_by             = $loginInfo['admin_id'];
                $session->session_name          = Input::get('session_name');
                $session->session_start_date    = date('Y-m-d', strtotime(Input::get('session_start_date')));
                $session->session_end_date      = date('Y-m-d', strtotime(Input::get('session_end_date')));
                $session->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }

        return redirect('admin-panel/session/view-sessions')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 17 July 2018
    **/
    public function anyData(Request $request)
    {
        $session = Session::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('name')))
            {
                $query->where('session_name', "like", "%{$request->get('name')}%");
            }
        })->orderBy('session_start_date', 'ASC')->get();
        return Datatables::of($session)
            ->addColumn('action', function ($session)
            {
                $encrypted_session_id = get_encrypted_value($session->session_id, true);
                if($session->session_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                        <div class="pull-left" ><a href="session-status/'.$status.'/' . $encrypted_session_id . '">'.$statusVal.'</a></div>
                        <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-session/' . $encrypted_session_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                        <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-session/' . $encrypted_session_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Destroy session's data
     *  @Shree on 17 July 2018
    **/
    public function destroy($id)
    {
        $session_id = get_decrypted_value($id, true);
        $session    = Session::find($session_id);
        if ($session)
        {
            $session->delete();
            $success_msg = "Session deleted successfully!";
            return redirect('admin-panel/session/view-sessions')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Session not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Set Session Data
     *  @Shree on 17 July 2018
    **/
    public function setSession(Request $request)
    {
        $session_id = Input::get('session_id');
        $session    = Session::where('session_id', $session_id)->first();

        if (isset($session->session_id))
        {
            $session_id   = $session->session_id;
            $session_year = $session->session_name;
            if (!empty($session_id) && !empty($session_year))
            {
                $set_data   = set_session($session_id, $session_year);
                //p($set_data);
                $return_arr = array(
                    'status'  => 'success',
                    'message' => 'Session set successfully!'
                );
            }
        }
        else
        {
            $return_arr = array(
                'status'  => 'success',
                'message' => 'Set session as current!'
            );
        }
        return response()->json($return_arr);
    }

    /**
     *  Change sessions's status
     *  @Shree on 18 July 2018
    **/
    public function changeStatus($status,$id)
    {
        $session_id = get_decrypted_value($id, true);
        $session    = Session::find($session_id);
        if ($session)
        {
            $session->session_status  = $status;
            $session->save();
            $success_msg = "Session deleted successfully!";
            return redirect('admin-panel/session/view-sessions')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Session not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
