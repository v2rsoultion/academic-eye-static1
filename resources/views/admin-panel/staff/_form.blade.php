@if(isset($staff['staff_id']) && !empty($staff['staff_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
    #sectiontable-body123 .btn-primary {
        width: 125px !important;
    }
   /* .theme-blush .btn-primary {
    margin-top: 3px !important;
}*/
.btn-round.btn-simple {
    padding: 5px 22px !important;
}
/*.select2-container--default .select2-selection--single {
    border: 1px solid #e3e3e3 !important;
}*/
</style>

{!! Form::hidden('staff_id',old('staff_id',isset($staff['staff_id']) ? $staff['staff_id'] : ''),['class' => 'gui-input', 'id' => 'staff_id', 'readonly' => 'true']) !!}

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_name', old('staff_name',isset($staff['staff_name']) ? $staff['staff_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_name'), 'id' => 'staff_name']) !!}
        </div>
        @if ($errors->has('staff_name')) <p class="help-block">{{ $errors->first('staff_name') }}</p> @endif
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_designation_id') !!} :</lable>
        <div class="form-group m-bottom-0">
        <label class=" field select" style="width: 100%">
            {!!Form::select('designation_id', $staff['arr_designations'],isset($staff['designation_id']) ? $staff['designation_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'designation_id'])!!}
            <i class="arrow double"></i>
        </label>
        @if ($errors->has('designation_id')) <p class="help-block">{{ $errors->first('designation_id') }}</p> @endif
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_role_id') !!} :</lable>
        <div class="form-group m-bottom-0">
        <label class=" field select" style="width: 100%">
            {!!Form::select('staff_role_id', $staff['arr_staff_roles'],isset($staff['staff_role_id']) ? $staff['staff_role_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_role_id'])!!}
            <i class="arrow double"></i>
        </label>
        @if ($errors->has('staff_role_id')) <p class="help-block">{{ $errors->first('staff_role_id') }}</p> @endif
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
    <!-- <span class="radioBtnpan"> -->
        <lable class="from_one1">{{trans('language.staff_profile_img')}}</lable>
        <label for="file1" class="field file">
            <input type="file" class="gui-file" name="staff_profile_img" id="file1" onChange="document.getElementById('staff_profile_img').value = this.value;">
            <input type="hidden" class="gui-input" id="staff_profile_img" placeholder="Upload Photo" readonly>
        </label>
        @if ($errors->has('staff_profile_img')) <p class="help-block">{{ $errors->first('staff_profile_img') }}</p> @endif
        @if(isset($staff['staff_profile_img']))<a href="{{url($staff['staff_profile_img'])}}" target="_blank" >View Image</a>@endif
    </div>

</div>


<div class="row clearfix">
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_email') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_email', old('staff_email',isset($staff['staff_email']) ? $staff['staff_email']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_email'), 'id' => 'staff_email']) !!}
        </div>
        @if ($errors->has('staff_email')) <p class="help-block">{{ $errors->first('staff_email') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_mobile_number') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_mobile_number', old('staff_mobile_number',isset($staff['staff_mobile_number']) ? $staff['staff_mobile_number']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_mobile_number'), 'id' => 'staff_mobile_number', 'maxlength'=> '10']) !!}
        </div>
        @if ($errors->has('staff_mobile_number')) <p class="help-block">{{ $errors->first('staff_mobile_number') }}</p> @endif
    </div>
    
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_dob') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_dob', old('staff_dob',isset($staff['staff_dob']) ? $staff['staff_dob']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_dob'), 'id' => 'staff_dob']) !!}
        </div>
        @if ($errors->has('staff_dob')) <p class="help-block">{{ $errors->first('staff_dob') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">Gender :</lable>
        <div class="form-group m-bottom-0">
        <label class=" field select" style="width: 100%">
            {!!Form::select('staff_gender', $staff['arr_gender'],isset($staff['staff_gender']) ? $staff['staff_gender'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'staff_gender'])!!}
            <i class="arrow double"></i>
        </label>
        @if ($errors->has('staff_gender')) <p class="help-block">{{ $errors->first('staff_gender') }}</p> @endif
        </div>
    </div>

</div>


<!-- Other Information -->
<div class="header">
    <h2><strong>Other</strong> Information</h2>
</div>

<div class="row clearfix">

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_father_name_husband_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_father_name_husband_name', old('staff_father_name_husband_name',isset($staff['staff_father_name_husband_name']) ? $staff['staff_father_name_husband_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_father_name_husband_name'), 'id' => 'staff_father_name_husband_name']) !!}
        </div>
        @if ($errors->has('staff_father_name_husband_name')) <p class="help-block">{{ $errors->first('staff_father_name_husband_name') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_father_husband_mobile_no') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_father_husband_mobile_no', old('staff_father_husband_mobile_no',isset($staff['staff_father_husband_mobile_no']) ? $staff['staff_father_husband_mobile_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_father_husband_mobile_no'), 'id' => 'staff_father_husband_mobile_no', 'maxlength' => '10']) !!}
        </div>
        @if ($errors->has('staff_father_husband_mobile_no')) <p class="help-block">{{ $errors->first('staff_father_husband_mobile_no') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_mother_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_mother_name', old('staff_mother_name',isset($staff['staff_mother_name']) ? $staff['staff_mother_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_mother_name'), 'id' => 'staff_mother_name']) !!}
        </div>
        @if ($errors->has('staff_mother_name')) <p class="help-block">{{ $errors->first('staff_mother_name') }}</p> @endif
    </div>

     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_blood_group') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_blood_group', old('staff_blood_group',isset($staff['staff_blood_group']) ? $staff['staff_blood_group']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_blood_group'), 'id' => 'staff_blood_group']) !!}
        </div>
        @if ($errors->has('staff_blood_group')) <p class="help-block">{{ $errors->first('staff_blood_group') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.caste_id') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('caste_id', $staff['arr_caste'],isset($staff['caste_id']) ? $staff['caste_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'caste_id'])!!}
                <i class="arrow double"></i>
            </label>
            @if ($errors->has('caste_id')) <p class="help-block">{{ $errors->first('caste_id') }}</p> @endif
        </div>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.religion_id') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('religion_id', $staff['arr_religion'],isset($staff['religion_id']) ? $staff['religion_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'religion_id'])!!}
                <i class="arrow double"></i>
            </label>
            @if ($errors->has('religion_id')) <p class="help-block">{{ $errors->first('religion_id') }}</p> @endif
        </div>    
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.nationality_id') !!} :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                {!!Form::select('nationality_id', $staff['arr_natioanlity'],isset($staff['nationality_id']) ? $staff['nationality_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'nationality_id'])!!}
                <i class="arrow double"></i>
            </label>
            @if ($errors->has('nationality_id')) <p class="help-block">{{ $errors->first('nationality_id') }}</p> @endif
        </div>    
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_marital_status') !!} :</lable>
        <div class="form-group">
            <div class="radio" style="margin-top:6px !important;">
                @if(isset($student['staff_marital_status'])) 
                    {{ Form::radio('staff_marital_status', '1',  $student['staff_marital_status'] == '1', array('id'=>'radio31')) }}
                    {{ Form::label('radio31', $staff['arr_marital'][1], array( 'class' => 'document_staff')) }}

                    {{ Form::radio('staff_marital_status', '0',  $student['staff_marital_status'] == '0', array('id'=>'radio32')) }}
                    {{ Form::label('radio32', $staff['arr_marital'][0], array('class' => 'document_staff')) }}
                @else
                {{ Form::radio('staff_marital_status', '1',  false, array('id'=>'radio31')) }}
                {{ Form::label('radio31', $staff['arr_marital'][1], array( 'class' => 'document_staff')) }}
                {{ Form::radio('staff_marital_status', '0',  true, array('id'=>'radio32')) }}
                {{ Form::label('radio32', $staff['arr_marital'][0], array( 'class' => 'document_staff')) }}
                @endif
            </div>
        </div>
    </div>

</div>


<!-- Temporary Address Information -->
<div class="header">
    <h2><strong>Temporary Address</strong> Information</h2>
</div>

<!-- Address section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.staff_temporary_address') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('staff_temporary_address', old('staff_temporary_address',isset($staff['staff_temporary_address']) ? $staff['staff_temporary_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_temporary_address'), 'id' => 'staff_temporary_address', 'rows'=> 2]) !!}
        </div>
        @if ($errors->has('staff_temporary_address')) <p class="help-block">{{ $errors->first('staff_temporary_address') }}</p> @endif
    </div>
</div>


<div class="row clearfix">
<!-- Country section -->
     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_temporary_county') !!} :</lable>
         <label class=" field select" style="width: 100%">
            <div class="form-group">
             {!!Form::select('country_id', $staff['arr_country'],isset($staff['country_id']) ? $staff['country_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'country_id', 'onChange'=>'getCountryState(this.value)'])!!}
            <i class="arrow double"></i>
            </div></label>
            @if ($errors->has('staff_temporary_county')) <p class="help-block">{{ $errors->first('staff_temporary_county') }}</p> @endif
    </div>
<!-- State section  -->
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_temporary_state') !!} :</lable>
        <label class=" field select" style="width: 100%">
        <div class="form-group">
            {!!Form::select('state_id', $staff['arr_state'],isset($staff['state_id']) ? $staff['state_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'state_id', 'onChange'=>'getStateCity(this.value)'])!!}
        </div></label>
        @if ($errors->has('staff_temporary_state')) <p class="help-block">{{ $errors->first('staff_temporary_state') }}</p> @endif
    </div>
<!-- City section -->
     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_temporary_city') !!} :</lable>
        <label class=" field select" style="width: 100%">
        <div class="form-group">
            {!!Form::select('city_id', $staff['arr_city'],isset($staff['city_id']) ? $staff['city_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'city_id'])!!}
        </div></label>
        @if ($errors->has('staff_temporary_city')) <p class="help-block">{{ $errors->first('staff_temporary_city') }}</p> @endif
    </div>
   

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_temporary_pincode') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_temporary_pincode', old('staff_temporary_pincode',isset($staff['staff_temporary_pincode']) ? $staff['staff_temporary_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_temporary_pincode'), 'id' => 'staff_temporary_pincode', 'maxlength'=> '6']) !!}
        </div>
        @if ($errors->has('staff_temporary_pincode')) <p class="help-block">{{ $errors->first('staff_temporary_pincode') }}</p> @endif
    </div>

</div>

<!-- Permanent Address Information -->
<div class="header">
    <h2><strong>Permanent Address</strong> Information</h2>
</div>

<!-- Address section -->
<div class="row clearfix">
    <div class="col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.staff_permanent_address') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('staff_permanent_address', old('staff_permanent_address',isset($staff['staff_permanent_address']) ? $staff['staff_permanent_address']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_permanent_address'), 'id' => 'staff_permanent_address', 'rows'=> 2]) !!}
        </div>
        @if ($errors->has('staff_permanent_address')) <p class="help-block">{{ $errors->first('staff_permanent_address') }}</p> @endif
    </div>
</div>

<div class="row clearfix">
    <!-- Country section -->
     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_temporary_county') !!} :</lable>
         <label class=" field select" style="width: 100%">
            <div class="form-group">
             {!!Form::select('country_id', $staff['arr_country'],isset($staff['country_id']) ? $staff['country_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'country_id', 'onChange'=>'getCountryState(this.value)'])!!}
            <i class="arrow double"></i>
            </div></label>
            @if ($errors->has('staff_temporary_county')) <p class="help-block">{{ $errors->first('staff_temporary_county') }}</p> @endif
    </div>
<!-- State section  -->
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_temporary_state') !!} :</lable>
        <label class=" field select" style="width: 100%">
        <div class="form-group">
            {!!Form::select('state_id', $staff['arr_state'],isset($staff['state_id']) ? $staff['state_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'state_id', 'onChange'=>'getStateCity(this.value)'])!!}
        </div></label>
        @if ($errors->has('staff_temporary_state')) <p class="help-block">{{ $errors->first('staff_temporary_state') }}</p> @endif
    </div>
<!-- City section -->
     <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_temporary_city') !!} :</lable>
        <label class=" field select" style="width: 100%">
        <div class="form-group">
            {!!Form::select('city_id', $staff['arr_city'],isset($staff['city_id']) ? $staff['city_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'city_id'])!!}
        </div></label>
        @if ($errors->has('staff_temporary_city')) <p class="help-block">{{ $errors->first('staff_temporary_city') }}</p> @endif
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1">{!! trans('language.staff_permanent_pincode') !!} :</lable>
        <div class="form-group">
            {!! Form::text('staff_permanent_pincode', old('staff_permanent_pincode',isset($staff['staff_permanent_pincode']) ? $staff['staff_permanent_pincode']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_permanent_pincode'), 'id' => 'staff_permanent_pincode', 'maxlength'=> '6']) !!}
        </div>
        @if ($errors->has('staff_permanent_pincode')) <p class="help-block">{{ $errors->first('staff_permanent_pincode') }}</p> @endif
    </div>

</div>

<!-- Qualification and Experience Information -->
<div class="header">
    <h2><strong> Qualification and Experience </strong> Information</h2>
</div>

<div class="row clearfix">
    <!-- Specialization section -->
    <div class="col-lg-4 col-md-4 col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.staff_specialization') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('staff_specialization', old('staff_specialization',isset($staff['staff_specialization']) ? $staff['staff_specialization']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_specialization'), 'id' => 'staff_specialization', 'rows'=> 2]) !!}
        </div>
        @if ($errors->has('staff_specialization')) <p class="help-block">{{ $errors->first('staff_specialization') }}</p> @endif
    </div>
    <!-- Qualification section -->
     <div class="col-lg-4 col-md-4 col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.staff_qualification') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('staff_qualification', old('staff_qualification',isset($staff['staff_qualification']) ? $staff['staff_qualification']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_qualification'), 'id' => 'staff_qualification', 'rows'=> 2]) !!}
        </div>
        @if ($errors->has('staff_qualification')) <p class="help-block">{{ $errors->first('staff_qualification') }}</p> @endif
    </div>
    <!-- Reference section -->
    <div class="col-lg-4 col-md-4 col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.staff_reference') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('staff_reference', old('staff_reference',isset($staff['staff_reference']) ? $staff['staff_reference']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_reference'), 'id' => 'staff_reference', 'rows'=> 2]) !!}
        </div>
        @if ($errors->has('staff_reference')) <p class="help-block">{{ $errors->first('staff_reference') }}</p> @endif
    </div>
</div>

<!-- Experience section -->
<div class="row rowAddmore">
    <div class="col-lg-10 col-md-6 col-sm-12 text_area_desc">
        <lable class="from_one1">{!! trans('language.staff_experience') !!} :</lable>
        <div class="form-group">
            {!! Form::textarea('staff_experience', old('staff_experience',isset($staff['staff_experience']) ? $staff['staff_experience']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_experience'), 'id' => 'staff_experience', 'rows'=> 1]) !!}
        </div>
        @if ($errors->has('staff_experience')) <p class="help-block">{{ $errors->first('staff_experience') }}</p> @endif
    </div>
    <!-- <div class="col-lg-4"></div> -->
     <div class="col-lg-2 col-md-2"><button class="btn btn-primary add_experience_block" style="margin-top: 36px; margin-left: 34px !important" type="button"><i class="fas fa-plus-circle"></i> Add More</button></div>


<div id="sectiontable-body" class="row seprateclass"  >
<input type="hidden" id="hide" value="0" />
  <div class="input_experience_wrap">
  </div>
</div>
</div>
<!-- Document Information -->
<div class="header">
    <h2><strong>Document</strong> Information</h2>
</div>

{!! Form::hidden('hide',isset($staff['documents']) ? COUNT($staff['documents']) : '1',['class' => 'gui-input', 'id' => 'hide']) !!}
<div id="sectiontable-body123">
    @if(isset($staff['documents']))
    @php $key = 0; @endphp
    @foreach($staff['documents'] as $documentKey => $documents)
    @php 
        $staff_document_category = "documents[".$key."][document_category_id]";  
        $staff_document_id = "documents[".$key."][staff_document_id]"; 
    @endphp
    <div id="document_block{{$documentKey}}">
        {!! Form::hidden($staff_document_id,$documents['staff_document_id'],['class' => 'gui-input']) !!}
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3">
                <lable class="from_one1">{!! trans('language.staff_document_category') !!} :</lable>
                <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    {!!Form::select($staff_document_category, $staff['arr_document_category'],$documents['document_category_id'], ['class' => 'form-control show-tick select_form1 select2','id'=>'document_category_id'])!!}
                    <i class="arrow double"></i>
                </label>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
            <span class="radioBtnpan">{{trans('language.staff_document_file')}}</span>
                <label for="file1" class="field file">
                    
                    <input type="file" class="gui-file" name="documents[{{$key}}][staff_document_file]" id="staff_document_file{{$key}}" >
                    <input type="hidden" class="gui-input" id="staff_document_file{{$key}}" placeholder="Upload Photo" readonly>
                    
                    @if(isset($documents['staff_document_file']))<a href="{{url($documents['staff_document_file'])}}" target="_blank" >View Image</a>@endif
                </label>
            
            </div>
            <div class="col-lg-3 col-md-3">&nbsp;</div>
            <div class="col-lg-3 col-md-3 text-right">
                @if($documentKey == 0)
                <button class="btn btn-raised btn-primary" type="button" onclick="addDocumentBlock()" ><i class="fas fa-plus-circle"></i> Add More</button>
                @else 
                <button class="btn btn-raised btn-danger" type="button" onclick="remove_block({{$documentKey}})" ><i class="fas fa-minus-circle"></i> Remove</button>
                @endif
            </div>
        </div>

        <!-- Document detail section -->
        <div class="row clearfix">
            <div class="col-sm-12 text_area_desc">
                <lable class="from_one1">{!! trans('language.staff_document_details') !!} :</lable>
                <div class="form-group">
                    <textarea name="documents[{{$key}}][staff_document_details]" id="" class="form-control" placeholder="{{trans('language.staff_document_details')}}" rows="2">{!! $documents['staff_document_details'] !!}</textarea>
                   
                </div>
                
            </div>
        </div>
    </div>
    @php $key++ @endphp
    @endforeach
    @else
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1">{!! trans('language.staff_document_category') !!} :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%;">
                    {!!Form::select('documents[0][document_category_id]', $staff['arr_document_category'],isset($staff['document_category_id']) ? $staff['document_category_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'document_category_id'])!!}
                    <i class="arrow double"></i>
                </label>
                @if ($errors->has('document_category_id')) <p class="help-block">{{ $errors->first('document_category_id') }}</p> @endif
            </div>    
        </div>
        <div class="col-lg-3 col-md-3">
        <!-- <span class="radioBtnpan"> -->
            <lable class="from_one1">{{trans('language.staff_document_file')}}</lable>
            <label for="file1" class="field file">
                
                <input type="file" class="gui-file" name="documents[0][staff_document_file]" id="staff_document_file" onChange="document.getElementById('staff_document_file').value = this.value;">
                <input type="hidden" class="gui-input" id="staff_document_file" placeholder="Upload Photo" readonly>
               
            </label>
           
        </div>
        <div class="col-lg-3 col-md-3">&nbsp;</div>
        <div class="col-lg-3 col-md-3 text-right"><button class="btn btn-raised btn-primary" type="button" onclick="addDocumentBlock()" ><i class="fas fa-plus-circle"></i> Add More</button></div>
    </div>

    <!-- Document detail section -->
    <div class="row clearfix">
        <div class="col-sm-12 text_area_desc">
            <lable class="from_one1">{!! trans('language.staff_document_details') !!} :</lable>
            <div class="form-group">
                {!! Form::textarea('documents[0][staff_document_details]', old('staff_document_details',isset($staff['staff_document_details']) ? $staff['staff_document_details']: ''), ['class' => 'form-control','placeholder'=>trans('language.staff_document_details'), 'id' => 'staff_document_details', 'rows'=> 2]) !!}
            </div>
            @if ($errors->has('staff_document_details')) <p class="help-block">{{ $errors->first('staff_document_details') }}</p> @endif
        </div>
    </div>
    @endif
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-lg-1 ">
      <button type="button" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Save">Save
      </button>
    </div>
     <div class="col-lg-1 ">
      <button type="reset" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Clear">Cancel
      </button>
    </div>
    <!-- <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-primary']) !!}</a>
        
    </div> -->
</div>

<script type="text/javascript">

    jQuery(document).ready(function () {
         $("#staff_gender").select2();
        $("#staff-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                student_enroll_number: {
                    required: true
                },
                student_roll_no: {
                    required: true
                },
                student_name: {
                    required: true
                },
                student_email: {
                    required: true,
                    email: true
                },
                student_reg_date: {
                    required: true
                },
                student_dob: {
                    required: true
                },
                student_category: {
                    required: true
                },
                caste_id: {
                    required: true
                },
                religion_id: {
                    required: true
                },
                nationality_id: {
                    required: true
                },
                student_permanent_address: {
                    required: true
                },
                student_permanent_city: {
                    required: true
                },
                student_permanent_state: {
                    required: true
                },
                student_permanent_county: {
                    required: true
                },
                student_permanent_pincode: {
                    required: true
                },
                admission_session_id: {
                    required: true
                },
                admission_class_id: {
                    required: true
                },
                admission_section_id: {
                    required: true
                },
                current_session_id: {
                    required: true
                },
                current_class_id: {
                    required: true
                },
                current_section_id: {
                    required: true
                },
                student_blood_group: {
                    required: true
                },
                medical_issues: {
                    required: true
                },
                student_father_name: {
                    required: true
                },
                student_father_mobile_number: {
                    required: true,
                    digits: true
                },
                student_father_email: {
                    required: true,
                    email: true
                },
                student_father_occupation: {
                    required: true
                },
                student_mother_name: {
                    required: true
                },
                student_mother_mobile_number: {
                    required: true,
                    digits: true
                },
                student_guardian_mobile_number: {
                    digits: true
                },
                student_guardian_email: {
                    email: true
                },
                student_mother_email: {
                    email: true
                },
                student_temporary_pincode: {
                    digits: true
                },
                student_permanent_pincode: {
                    digits: true,
                    required: true
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                     element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
        $('#staff_dob').bootstrapMaterialDatePicker({ weekStart : 0,time: false });
        
    });


    function addDocumentBlock() {
        var a = parseInt(document.getElementById('hide').value);
        document.getElementById('hide').value = a+1;
        var b = document.getElementById('hide').value;
        var arr_key = parseInt(b) - 1;
        
        $("#sectiontable-body123").append('<div id="document_block'+arr_key+'" ><div class="row clearfix"><div class="col-lg-3 col-md-3"><lable class="from_one1">{!! trans("language.student_document_category") !!} :</lable><label class=" field select" style="width: 100%">  <select class="form-control show-tick select_form1" id="document_category_id'+arr_key+'" name="documents['+arr_key+'][document_category_id]">@foreach ($staff["arr_document_category"] as $document_key => $document_category) <option value="{{ $document_key}}">{!! $document_category !!}</option>@endforeach</select><i class="arrow double"></i></label></div><div class="col-lg-3 col-md-3"><span class="radioBtnpan">{{trans("language.staff_document_file")}}</span><label for="file1" class="field file"><input type="file" class="gui-file" name="documents['+arr_key+'][staff_document_file]" id="file'+arr_key+'" ><input type="hidden" class="gui-input" id="staff_document_file'+arr_key+'" placeholder="Upload Photo" readonly></label></div><div class="col-lg-3 col-md-3">&nbsp;</div><div class="col-lg-3 col-md-3 text-right"><button class="btn btn-raised btn-danger" type="button" onclick="remove_block('+arr_key+')" ><i class="fas fa-minus-circle"></i> Remove</button></div></div><div class="row clearfix"><div class="col-sm-12 text_area_desc"><lable class="from_one1">{!! trans("language.staff_document_details") !!} :</lable><div class="form-group"><textarea name="documents['+arr_key+'][staff_document_details]" class="form-control" placeholder="{{trans("language.staff_document_details")}}" id="staff_document_details'+arr_key+'" rows="2" ></textarea></div> </div></div></div>');
        $("select[name='documents["+arr_key+"][document_category_id]'").selectpicker('refresh');
    }
    function remove_block(id){
        $("#document_block"+id).remove();
        // var c = parseInt(document.getElementById('hide').value);
        // document.getElementById('hide').value = c-1;
    }

    function getCountryState(country_id)
    {
        if(country_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/state/get-country-state-data')}}",
                type: 'GET',
                data: {
                    'country_id': country_id
                },
                success: function (data) {
                    $("select[name='state_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='state_id'").html('<option value="">Select State</option>');
            $(".mycustloading").hide();
        }
    }
</script>

