@extends('admin-panel.layout.header')
@section('content')
<!-- Main Content -->
<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Add Group</h2>
            </div> 
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right ">
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/student') !!}">Student</a></li>
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/student') !!}">Groups</a></li>
                <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student/group/add-group') !!}">Add Group</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div>
                    <div class="body form-gap">
                        <form class="" action="" method=""  id="form_validation">
                            <div class="row clearfix"> 
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1">Name :</lable>
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" placeholder="Name">
                                    </div>
                                </div>
                               
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1">Group Type :</lable>
                                    <div class="form-group">
                                        <div class="radio" style="margin-top:6px !important;">
                                            <input type="radio" name="type" id="radio1" value="option1" checked="">
                                            <label for="radio1" class="document_staff">Student</label>
                                            <input type="radio" name="type" id="radio2" value="option2">
                                            <label for="radio2" class="document_staff">Parents</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <lable class="from_one1">Description :</lable>
                                    <div class="form-group">
                                       <textarea class="form-control" placeholder="Description"></textarea>
                                    </div>
                                </div>
                                  <!--  <div class="col-lg-4 col-md-3 col-sm-12">
                                    <lable class="from_one1">No of Students/Parents :</lable>
                                    <div class="form-group">
                                        <input type="text" name="no_of_student_parents" class="form-control" placeholder="No of Student/ Parents">
                                    </div>
                                </div> -->
                                
                            </div>
                        
                            <div class="row clearfix">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-raised  btn-primary">Save</button>
                                    <button type="submit" class="btn btn-raised  btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <script src="assets/plugins/jquery-validation/jquery.validate.js"></script> 
<script src="assets/js/pages/forms/form-validation.js"></script> -->

<script>

function myFunction1() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "none";
    } else {
        x.style.display = "none";
    }
}
</script>

<style>
#myDIV {
    width: 100%;
}
</style>
<script type="text/javascript">

$('#form_validation').validate({
        rules: {
            'vehcile_name': {
                required: true
            },
            'vehcile_reg_number': {
                required: true
            },
            'type': {
                required: true
            },
            'capacity': {
                required: true
            },
            'contact_person': {
                required: true
            },
            'contact_number': {
                required: true
            }
        },

        /* @validation error messages 
      ---------------------------------------------- */
      messages: {
        vehcile_name: {
          required: 'Please fill your required vehcile name'
        },
        vehcile_reg_number: {
          required: 'Please fill requerd vehcile reg number'
        },
        type: {
          required: 'Please fill requerd type'
        },
        capacity: {
          required: 'Please fill requerd capacity'
        },
        contact_person: {
          required: 'Please fill requerd contact person'
        },
        contact_number: {
          required: 'Please fill requerd contact number'
        }         
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

</script>
@endsection