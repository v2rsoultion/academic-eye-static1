@extends('admin-panel.layout.header')
@section('content')

<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>{!! trans('language.manage_competitions') !!}</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <a href="{!! url('admin-panel/competition/add-competition') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item">{!! trans('language.menu_academic') !!}</li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.competitions') !!}</a></li>
                    <li class="breadcrumb-item active"><a href="">{!! trans('language.manage_competitions') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           
                            <div class="body form-gap">
                                @if(session()->has('success'))
                                    <p class="green">
                                        {{ session()->get('success') }}
                                    </p>
                                @endif
                                @if($errors->any())
                                    <p class="red">{{$errors->first()}}</p>
                                @endif
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('medium_type', $competition['arr_medium'],'', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                            @if($errors->has('medium_type')) <p class="help-block">{{ $errors->first('medium_type') }}</p> @endif
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>trans('language.competitions_name'), 'id' => 'name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised btn-round btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="">
                                <table class="table m-b-0 c_list" id="competition-table" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.competitions_name')}}</th>
                                            <th>{{trans('language.competitions_date')}}</th>
                                            <th>{{trans('language.competitions_level')}}</th>
                                            <th>{{trans('language.medium_type')}}</th>
                                            <th>{{trans('language.competitions_no_winner')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
     $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#competition-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/competition/data')}}',
                data: function (d) {
                    d.medium_type = $('select[name="medium_type"]').val();
                    d.name = $('input[name=name]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'competition_name', name: 'competition_name'},
                {data: 'competition_date', name: 'competition_date'},
                {data: 'competition_level', name: 'competition_level'},
                {data: 'medium_type', name: 'medium_type'},
                {data: 'no_of_winner', name: 'no_of_winner'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "8%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1,2,3,4],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });


</script>
@endsection




