@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
 <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>{!! trans('language.manage_item') !!}</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">{!! trans('language.inventory') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/inventory/manage-item') !!}">{!! trans('language.manage_item') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="card gap-m-bottom">
          <div class="header">
            <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
          </div>
          <div class="body form-gap1">
            {!! Form::open(['files'=>TRUE,'id' => 'manage_item' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
              @include('admin-panel.inventory-item._form',['submit_button' => $submit_button])
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid">
  <div class="card">
    <div class="body form-gap1">
      <div class="headingcommon col-lg-12">Search By :-</div>
      @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
          {{ session()->get('success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif
      
        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
        <div class="row" >
          <div class="col-lg-3 col-md-3">
            <div class="form-group m-bottom-0">
              <label class=" field select size">
                {!!Form::select('search_category', $items['arr_category'], '', ['class' => 'form-control show-tick select_form1 select2','id'=>'search_category'])!!}
                <i class="arrow double"></i>
              </label>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="form-group">
              {!! Form::text('s_item_name','',['class' => 'form-control','placeholder'=> trans('language.s_item_name'), 'id' => 's_item_name']) !!}
            </div>
          </div>
          
          <div class="col-lg-1">
            {!! Form::submit('Search', ['class' => 'btn btn-raised btn-primary ','name'=>'Search']) !!}
          </div>
            <div class="col-lg-1">
              {!! Form::button('Clear', ['class' => 'btn btn-raised btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
          </div>
        </div>
        {!! Form::close() !!}
      <div class="clearfix"></div>
      <!--  DataTable for view Records  -->
      <div class="table-responsive">
        <table class="table m-b-0 c_list" id="item-table" style="width:100%">
        {{ csrf_field() }}
          <thead>
            <tr>
              <th>{!! trans('language.item_s_no') !!}</th>
              <th>{!! trans('language.item_name') !!}</th>
              <th>{!! trans('language.item_category_name') !!}</th>
              <th>{!! trans('language.item_sub_category_name') !!}</th>
              <th>{!! trans('language.item_measuring_unit') !!}</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
<script type="text/javascript">
  $(document).ready(function () {
        var table = $('#item-table').DataTable({
          serverSide: true,
          searching: false, 
          paging: false, 
          info: false,
          ajax: {
              url:"{{ url('admin-panel/inventory/manage-items-view')}}",
              data: function (f) {
                  f.search_category = $('#search_category').val();
                  f.s_item_name     = $('#s_item_name').val();
              }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex' },
            {data: 'item_name', name: 'item_name'},
            {data: 'category_id', name: 'category_id'},
            {data: 'sub_category_id', name: 'sub_category_id'},
            {data: 'unit_id', name: 'unit_id'},
            {data: 'action', name: 'action'}
        ] 
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        });
    });
</script>
@endsection