<?php

    return [
        "authorization_error_msg"       => "You don\"t have authorization !",
        "email_sender"                  => "Academic Management Team",
        "project_title"                 => "Academic Management",
        "reset_password"                => "Reset Password",
        "dashboard"                     => "Dashboard",
        "profile"                       => "Profile",
        "medium_type"                   => "Medium Type", 

        "school"                        => "School",
        "add_school"                    => "Add School", // School segment start here
        "view_school"                   => "View School",
        "school_detail"                 => "School Detail",
        "school_name"                   => "School Name",
        "school_registration_no"        => "School Reg/Affiliation No",
        "school_sno_numbers"            => "Sr No(Multiple)",
        "school_description"            => "Description",
        "school_address"                => "Address",
        "school_district"               => "District",
        "school_city"                   => "City",
        "school_state"                  => "State",
        "school_pincode"                => "Pincode",
        "school_board_of_exams"         => "Board of Education",
        "school_medium"                 => "Medium (Lang)",
        "school_class_from"             => "Class From",
        "school_class_to"               => "Class To",
        "school_total_students"         => "No of Students",
        "school_total_staff"            => "No of Staff",
        "school_fee_range_from"         => "Fees Range(From)",
        "school_fee_range_to"           => "Fees Range(To)",
        "school_facilities"             => "Facilities",
        "school_url"                    => "Branch URL",
        "school_logo"                   => "Branch Logo",
        "school_email"                  => "Email",
        "school_mobile_number"          => "Mobile",
        "school_telephone"              => "Telephone",
        "school_fax_number"             => "Fax",
        "school_image"                  => "Image",


        "session"                       => "Sessions", // For Session
        "add_session"                   => "Add Session",
        "view_session"                  => "View Sessions",
        "edit_session"                  => "Edit Session",
        "session_name"                  => "Session Name",
        "session_start_date"            => "Start Date",
        "session_end_date"              => "End Date",

        "holidays"                      => "Holidays", // For Holiday
        "add_holiday"                   => "Add Holiday",
        "view_holiday"                  => "View Holidays",
        "edit_holiday"                  => "Edit Holiday",
        "holiday_name"                  => "Holiday Name",
        "holiday_start_date"            => "Start Date",
        "holiday_end_date"              => "End Date",

        "shifts"                        => "Shifts", // For Shifts
        "add_shift"                     => "Add Shift",
        "view_shift"                    => "View Shifts",
        "edit_shift"                    => "Edit Shift",
        "shift_name"                    => "Shift Name",
        "shift_start_time"              => "Start Time",
        "shift_end_time"                => "End Time",


        "class"                         => "Class", // For Classes
        "add_class"                     => "Add Class",
        "view_class"                    => "View Classes",
        "edit_class"                    => "Edit Class",
        "class_name"                    => "Class Name",
        "class_order"                   => "Class Order",
        "class_students"                => "No of students",
        
        "section"                       => "Section", // For Section
        "add_section"                   => "Add Section",
        "view_section"                  => "View Sections",
        "edit_section"                  => "Edit Section",
        "section_class_id"              => "Classes",
        "section_name"                  => "Section Name",
        "section_order"                 => "Section Order",
        "section_intake"                => "No of Intake",
        
        "co_scholastic"                     => "Type of Co-Scholastic", // For Type of Co-Scholastic
        "add_co_scholastic"                 => "Add Type of Co-Scholastic",
        "view_co_scholastic"                => "View Type of Co-Scholastic",
        "edit_co_scholastic"                => "Edit Type of Co-Scholastic",
        "co_scholastic_type_name"           => "Co Scholastic",
        "co_scholastic_type_description"    => "Description",
   

        "facilities"                    => "Facilities", // For Facilities by Pratyush on 19 July 2018
        "add_facility"                  => "Add Facility",
        "view_facility"                 => "View Facilities",
        "edit_facility"                 => "Edit Facility",
        "facility_name"                 => "Facility Name",
        "is_fees_applied"               => "Is Fees Applied",
        "fees_applied_yes"              => "Yes",
        "fees_applied_no"               => "No",
        "facility_fees_amount"          => "Fees Amount",

        "document_category"             => "Category", // For Document categories by Pratyush on 19 July 2018
        "add_document_category"         => "Add Document Category",
        "view_document_category"        => "View Document Categories",
        "edit_document_category"        => "Edit Document Category",
        "name_of_doc"                   => "Name",
        "document_category_for"         => "Documents For",
        "document_category_student"     => "Student",
        "document_category_staff"       => "Staff",
        "document_category_both"        => "Both",

        "designation"                   => "Designation", // For Document categories by Pratyush on 19 July 2018
        "add_designation"               => "Add Designation",
        "view_designation"              => "View Designation",
        "edit_designation"              => "Edit Designation",
        "designation_name"              => "Designation Name",

        "title"                         => "Title", // For Title by Pratyush on 20 July 2018
        "add_title"                     => "Add Title",
        "view_title"                    => "View Titles",
        "edit_title"                    => "Edit Title",
        "title_name"                    => "Title",

        "caste"                         => "Caste", // For Caste by Pratyush on 20 July 2018
        "add_caste"                     => "Add Caste",
        "view_caste"                    => "View Caste",
        "edit_caste"                    => "Edit Caste",
        "caste_name"                    => "Caste Name",

        "religion"                      => "Religion", // For Religion by Pratyush on 20 July 2018
        "add_religion"                  => "Add Religion",
        "view_religion"                 => "View Religions",
        "edit_religion"                 => "Edit Religion",
        "religion_name"                 => "Religion Name",

        "nationality"                   => "Nationality", // For Nationality by Pratyush on 20 July 2018
        "add_nationality"               => "Add Nationality",
        "view_nationality"              => "View Nationality",
        "edit_nationality"              => "Edit Nationality",
        "nationality_name"              => "Nationality",

        "school_group"                  => "School Group", // For School House by Pratyush on 20 July 2018
        "add_school_group"              => "Add School Group",
        "view_school_group"             => "View School Groups",
        "edit_school_group"             => "Edit School Group",
        "school_group_name"             => "School Group Name",

        "virtual_class"                 => "Virtual Class", // For Virtual Class by Pratyush on 21 July 2018
        "add_virtual_class"             => "Add Virtual Class",
        "manage_virtual_classes"        => "View Virtual Classes",
        "edit_virtual_class"            => "Edit Virtual Class",
        "virtual_class_name"            => "Name",
        "virtual_class_description"     => "Description",
        "virtual_class_sr_no"           => "Sr. No.",
        "virtual_class_stu_list"        => "View Student List", // For Student list (Virtual class add student)
        "virtual_class_stu_name"        => "Name",
        "virtual_class_stu_roll_no"     => "Roll No",
        "virtual_class_stu_profile"     => "Student Profile",
        "virtual_class_stu_enroll_no"   => "Enroll No",
        "virtual_class_stu_f_name"      => "Father Name",
        "virtual_class_stu_class"       => "Class",
        "virtual_class_stu_session"     => "Session",
        "virtual_class_no_of_student"   => "No Of Students",

        "competitions"                  => "Competition", // For Competitions by Pratyush on 21 July 2018
        "add_competitions"              => "Add Competition",
        "manage_competitions"           => "View Competitions",
        "edit_competitions"             => "Edit Competition",
        "competitions_name"             => "Name",
        "competitions_description"      => "Description",
        "competitions_date"             => "Date",
        "competitions_level"            => "Level",
        "competitions_level_school"     => "school",
        "competitions_level_class"      => "Class",
        "Issue_certificate"             => "Issue Participation Certificate",
        "Issue_certificate_yes"         => "Yes",
        "Issue_certificate_no"          => "No",
        "competitions_no_winner"        => "No Of Winner",
        "competitions_level_classes"    => "Classes", //for add page dropdown menu

        "comp_select_winner"            => "Select Winner", //for Select Winner page (Competition)
        "comp_list_of_winner"           => "List of winners",
        "comp_list_of_winner_rank"      => "Rank",
        "comp_list_of_winner_name"      => "Name",
        "comp_list_of_winner_class"     => "Class",
        "comp_list_of_winner_section"   => "Section",

        "subject"                        => "Subject", // For Type of Co-Scholastic
        "add_subject"                    => "Add Subject",
        "view_subject"                   => "View Subjects",
        "edit_subject"                   => "Edit Subject",
        "subject_name"                   => "Subject Name",
        "subject_code"                   => "Subject Code",
        "subject_class_ids"              => "Classes",
        "co_scholastic_type_id"          => "Type of Co scholastic",
        "subject_is_coscholastic"        => "Co-Scholastic",

        "student"                        => "Student", // Student
        "add_student"                    => "Add Student",
        "view_student_list"              => "View List",
        "view_students"                  => "View students",
        "student_profile"                => "Student Profile",
        "edit_student"                   => "Edit Student",
        "student_enroll_number"          => "Enroll No",
        "student_roll_no"                => "Roll No",
        "student_email"                  => "Email Address",
        "student_reg_date"               => "Date of Reg",
        "student_name"                   => "Name",
        "student_type"                   => "Student Type",
        "student_class_section"          => "Class Section",
        "student_image"                  => "Photo of student",
        "student_gender"                 => "Gender",
        "student_dob"                    => "DOB",
        "student_have"                   => "Student Have",
        "student_adhar_card_number"      => "Adhaar Card Number",
        "student_category"               => "Category",
        "caste_id"                       => "Caste",
        "religion_id"                    => "Religion",
        "nationality_id"                 => "Nationality",
        "student_sibling_name"           => "Sibling Name",
        "student_sibling_class_id"       => "Sibling Class",
        "student_privious_school"        => "School Name",
        "student_privious_class"         => "Class",
        "student_privious_tc_no"         => "TC No.",
        "student_privious_tc_date"       => "TC Date",
        "student_privious_result"        => "Result",
        "student_status"                 => "Status",

        "student_temporary_address"      => "Address", // Student temporary address information
        "student_temporary_city"         => "City",
        "student_temporary_state"        => "State",
        "student_temporary_county"       => "Country",
        "student_temporary_pincode"      => "Pincode",

        "student_permanent_address"      => "Address", // Student permanent address information
        "student_permanent_city"         => "City",
        "student_permanent_state"        => "State",
        "student_permanent_county"       => "Country",
        "student_permanent_pincode"      => "Pincode",

        "student_login_name"             => "Name",
        "student_login_email"            => "Email",
        "student_login_contact_no"       => "Contact No",
        "student_father_name"            => "Father Name",  // Student parent information
        "student_father_mobile_number"   => "Father’s Mobile number",
        "student_father_email"           => "Father’s Email Address",
        "student_father_occupation"      => "Father’s Occupation",
        "student_father_annual_salary"   => "Father’s Annual Salary",
        "student_mother_name"            => "Mother Name",
        "student_mother_mobile_number"   => "Mother’s Mobile number",
        "student_mother_email"           => "Mother’s Email Address",
        "student_mother_occupation"      => "Mother’s Occupation",
        "student_mother_annual_salary"   => "Mother’s Annual Salary",
        "student_mother_tongue"          => "Mother Tongue",
        "student_guardian_name"          => "Guardian Name",
        "student_guardian_email"         => "Email Address",
        "student_guardian_mobile_number" => "Mobile Number",
        "student_guardian_relation"      => "Relation with guardian",

        "admission_session_id"           => "Admit Session",  // Student academic information
        "admission_class_id"             => "Admit Class",
        "admission_section_id"           => "Admit Section",
        "current_session_id"             => "Current Session",
        "current_class_id"               => "Current Class",
        "current_section_id"             => "Current Section",
        "student_unique_id"              => "Attendance Unique ID",
        "group_id"                       => "House/Group",

        "ac_session_id"                  => "Session",  // Student academic history information
        "ac_class_id"                    => "Class",
        "ac_section_id"                  => "Section",
        "ac_percentage"                  => "Percentage",
        "ac_rank"                        => "Rank",
        "ac_activity_winner"             => "Any CC activity winner",

        "student_height"                  => "Height",  // Student health information
        "student_weight"                  => "Weight",
        "student_blood_group"             => "Blood Group",
        "student_vision_left"             => "Vision Right",
        "student_vision_right"            => "Vision Left",
        "medical_issues"                  => "Medical Issue(You can add multiple issue by press Enter)",
        "medical_issues_p"                => "Medical Issue",
        "total_students"                  => "No of students",
        "search_by_roll_no"               => "Roll No",
        "search_by_enroll_no"             => "Enroll No",
        "search_by_student_name"          => "Student Name",
        "search_by_father_name"           => "Father Name",

        "student_document_category"       => "Document category", // Student document information
        "student_document_file"           => "Document File",
        "student_document_details"        => "Document Details",
        "student_edit_document"           => "Edit Document", // @Pratyush 10 Aug 2018

        "room_no"                         => "Room No", // For Room No by Pratyush on 28 July 2018
        "add_room_no"                     => "Add Room No",
        "view_room_no"                    => "View Room No",
        "edit_room_no"                    => "Edit Room No",
        "room_no_add_page"                => "Room No",

        "s_no"                            => "S. No.",
        "parent_information"              => "Parent Information",
        "student_contact"                 => "Contact No",
        "view_parent_detail"              => "View Parent Detail",

        "staff_role"                      => "Staff Roles", // For Holiday
        "add_staff_role"                  => "Add Staff Role",
        "view_staff_role"                 => "View Staff Roles",
        "edit_staff_role"                 => "Edit Staff Role",
        "staff_role_name"                 => "Staff Role Name",

        "staff"                           => "Staff", // For Staff
        "add_staff"                       => "Add Staff",
        "view_staff"                      => "View Staff",
        "edit_staff"                      => "Edit Staff",
        "staff_name"                      => "Name",
        "staff_designation_id"            => "Designation",
        "staff_designation_name"          => "Designation",
        "staff_role_id"                   => "Roles",
        "staff_email"                     => "Email",
        "staff_mobile_number"             => "Mobile Number",
        "staff_dob"                       => "DOB",
        "staff_gender"                    => "Gender",
        "staff_profile_img"               => "Profile Image",

        "staff_specialization"            => "Specialization",
        "staff_qualification"             => "Qualification",
        "staff_reference"                 => "Reference",
        "staff_experience"                => "Experience ",

        "staff_father_name_husband_name"  => "Father Name/Husband Name",
        "staff_father_husband_mobile_no"  => "Contact Number",
        "staff_mother_name"               => "Mother Name",
        "staff_marital_status"            => "Marital Status",
        "staff_blood_group"               => "Blood Group",
        "staff_document_category"       => "Document category", // Staff document information
        "staff_document_file"           => "Document File",
        "staff_document_details"        => "Document Details",

        "staff_temporary_address"         => "Address", // staff temporary address information
        "staff_temporary_city"            => "City",
        "staff_temporary_state"           => "State",
        "staff_temporary_county"          => "Country",
        "staff_temporary_pincode"         => "Pincode",

        "staff_permanent_address"         => "Address", // staff permanent address information
        "staff_permanent_city"            => "City",
        "staff_permanent_state"           => "State",
        "staff_permanent_county"          => "Country",
        "staff_permanent_pincode"         => "Pincode",

        "notes"                           => "Notes", // For Notes by Ashish on 31 July 2018
        "add_notes"                       => "Add Notes",
        "view_notes"                      => "View Notes",
        "edit_notes"                      => "Edit Notes",
        "online_content"                  => "Online Content",
        "t_name"                          => "Name",
        "class_section"                   => "Class-section",
        "notes_class"                     => "Class",
        "notes_section"                   => "Section",
        "notes_subject"                   => "Subject",
        "notes_unit"                      => "Unit",
        "notes_topic"                     => "Topic",
        "teacher_name"                    => "Teacher Name",
        "upload_date"                     => "Upload Date",
        "file_url"                        => "File Url",

        
        "class_teacher_allocation"        => "Class Teacher Allocation", // For Class Teacher Allocation by Pratyush on 06 Aug 2018
        "allot_teacher"                   => "Allocate Class",
        "view_allocation"                 => "View Allocations",
        "edit_allocation"                 => "Edit Allocation",
        "allot_class_name"                => "Class Name",
        "allot_section_name"              => "Section Name",
        "allot_teacher_name"              => "Teacher Name",

        "map_subject_to_class"            => "Map Subject To Class", // For Map Subject to class by Pratyush on 07 Aug 2018
        "map_subject"                     => "Map Subject",
        "view_map_subject"                => "View Subject Class Mapping",
        "ms_class_name"                   => "Class Name",
        "ms_section_name"                 => "Sections",
        "ms_no_subject_applied"           => "No Of Subject Applied",
        "ms_subject_code"                 => "Subject Code",
        "ms_subject_name"                 => "Subject Name",
        "ms_subject"                      => "Subject",

        "map_subject_to_teacher"          => "Map Subject To Teacher", // For Map Subject to teacher by Pratyush on 07 Aug 2018
        "map_subject_teacher"             => "Map Subject",
        "view_map_subject_teacher"        => "View Subject Teacher Mapping",
        "ms_class_section"                => "Class Name - Section",
        "mst_no_subjects"                 => "No Of Subjects",
        "mst_no_of_map_subject"           => "No of Mapped Subject", 
        "mst_remaining_subjects"          => "Remaining Subject",
        "ms_subject_code"                 => "Subject Code - Subject Name",
        "ms_teacher_code"                 => "Teacher Id/Code",
        "ms_teacher_name"                 => "Teacher Name",
        "mst_view_report"                 => "View Report",
        "mst_subject_code_name"           => "Subject Code - Subject Name",

        "student_leave_application"       => "Student", // For Student Leave Application by Pratyush on 10 Aug 2018
        "add_student_leave_application"   => "Add Student Leave Application",
        "view_student_leave_application"  => "View Student Leave Application",
        "edit_student_leave_application"  => "Edit Student Leave Application",
        "stud_leave_app_reason"           => "Reason",
        "stud_leave_app_date_to"          => "Date To",
        "stud_leave_app_date_from"        => "Date From",
        "stud_leave_app_attachment"       => "Attachment Link",
        "stud_leave_app_status"           => "Status",

        "stream"                          => "Stream", // For Stream by Pratyush on 11 Aug 2018
        "add_stream"                      => "Add Stream",
        "view_stream"                     => "View Streams",
        "edit_stream"                     => "Edit Stream",
        "stream_name"                     => "Stream",

        "examination"                    => "Examination",
        "terms"                          => "Manage Term", // For terms by Pratyush on 11 Aug 2018
        "add_terms"                      => "Manage Term",
        // "view_terms"                     => "View Terms",
        "edit_terms"                     => "Edit Term",
        "terms_name"                     => "Term Name",
        "terms_caption"                  => "Caption",

        "exam"                           => "Manage Exam", // For Exam by Pratyush on 11 Aug 2018
        "add_exam"                       => "Manage Exam",
        "view_exam"                      => "View Exam",
        "edit_exam"                      => "Edit Exam",
        "exam_name"                      => "Exam",
        "term_select"                    => "Select Term",
        "exam_term"                      => "Term",

        "book_category"                  => "Book Category", // For Book Category by Pratyush on 13 Aug 2018
        "add_book_category"              => "Add Book Category",
        "view_book_category"             => "View Book Categories",
        "edit_book_category"             => "Edit Book Category",
        "book_category_name"             => "Name",

        "book_allowance"                 => "Book Allowance", // For Book Allowance by Pratyush on 13 Aug 2018
        "edit_book_allowance"            => "Edit Book Allowance",
        "edit_book_staff"                => "For staff",
        "edit_book_student"              => "For student",

        "book_vendor"                    => "Book Vendor", // For Book Allowance by Pratyush on 13 Aug 2018
        "add_book_vendor"                => "Add Book Vendor",
        "view_book_vendor"               => "View Book Vendors",
        "edit_book_vendor"               => "Edit Book Vendor",
        "book_vendor_name"               => "Name",
        "book_vendor_no"                 => "Contact No",
        "book_vendor_email"              => "Email Address",
        "book_vendor_address"            => "Address",

        "cupboard"                       => "CupBoard", // For CupBoard by Pratyush on 13 Aug 2018
        "add_cupboard"                   => "Add CupBoard",
        "view_cupboard"                  => "View CupBoard",
        "edit_cupboard"                  => "Edit CupBoard",
        "cupboard_name"                  => "Name",

        "cupboard_shelf"                 => "CupBoard Shelf", // For CupBoard Shelf by Pratyush on 13 Aug 2018
        "add_cupboard_shelf"             => "Add CupBoard Shelf",
        "view_cupboard_shelf"            => "View CupBoard Shelf",
        "edit_cupboard_shelf"            => "Edit CupBoard Shelf",
        "cupboard_shelf_name"            => "Name",
        "cupboard_shelf_cup"             => "CupBoard",
        "cupboard_shelf_capacity"        => "Capacity",
        "cupboard_shelf_detail"          => "Details",

        "bbook"                          => "Book", // For Book by Pratyush on 14 Aug 2018
        "add_bbook"                      => "Add Book",
        "view_bbook"                     => "View Books",
        "edit_bbook"                     => "Edit Book",
        "bbook_category"                 => "Book Category",
        "bbook_name"                     => "Book Name",
        "bbook_type"                     => "Book Type",
        "bbook_subtitle"                 => "Subtitle",
        "bbook_isbn_no"                  => "ISBN No",
        "bbook_author"                   => "Author",
        "bbook_publisher"                => "Publisher",
        "bbook_edition"                  => "Edition",
        "bbook_vendor"                   => "Book Vendor",
        "bbook_price"                    => "Price",
        "bbook_copy"                     => "Copy",
        "bbook_unique_no"                => "Book Unique No",
        "bbook_exclusive_staff"          => "Exclusive for staff",
        "bbook_remark"                   => "Remark",
        "bbook_cupboard"                 => "CupBoard",
        "bbook_cupboard_shelf"           => "CupBoard Shelf",
        "bbook_total_copies"             => "Total Copies",
        "bbook_issue_copies"             => "Copies Issued",
        "bbook_cupboard_plus_shelf"      => "cupboard + cupboard shelf",
        "bbook_issue_available"          => "Issued/Available",


        "library_member"                 => "Member",
        "member_student"                 => "Register Student",
        "member_student_view"            => "View Students",
        'member_staff'                   => "Register Staff",
        'member_staff_view'              => "View Staff",
        'member_staff_emp_id'            => "Emp ID",
        'member_staff_photo'             => "Photo",
        'member_staff_name'              => "Name",
        'member_staff_designation'       => "Designation",
        'member_student_roll_no'         => "Roll No",
        'member_student_name'            => "Name",
        'member_student_class_section'   => "Class",
        'member_no_of_book_issued'       => "No of Issued Book",
        'member_issued_books'            => "Issued Books",
        'member_issued_from'             => "Issue From",
        'member_issued_to'               => "Issue To",
        'member_issued_book'             => "Book",

        'book_issued'                    => "Issue Book",
        'book_issued_name'               => "Name",
        'book_issued_father_name'        => "Father Name",
        'book_issued_profile'            => "Profile",
        'book_issued_book_left'          => "No of Book Left",
        'book_issued_available_copies'   => "Available Copies",
        'book_issued_from'               => 'From',   
        'book_issued_to'                 => 'To',
        'book_issued_start'              => 'Start Date',   
        'book_issued_end'                => 'End Date',
        'book_issued_unique_no'          => 'Book Unique No',
        'book_issued_book_name'          => 'Book Name',
        'book_issued_duration'           => 'Duration',
        'book_issued_member_name'        => 'Member Name',
        'book_issued_isbn_no'            => 'ISBN No',


        "question_paper"                  => "Question Paper", // For Question Paper by Ashish on 21 July 2018
        "add_question_paper"              => "Add Question Paper",
        "view_question_paper"             => "View Question Paper",
        "edit_question_paper"             => "Edit Question Paper",
        "online_content"                  => "Online Content",
        "question_paper_name"             => "Name",
        "question_paper_file_upload"      => "File Upload",
        "question_paper_exam_type_id"     => "Exam Type",
        "question_paper_class_section"    => "Class-section",
        "question_paper_class_id"         => "Class",
        "question_paper_section_id"       => "Section",
        "question_paper_subject_id"       => "Subject",

        'return_renew_book'             => 'Return Book',

        "library_fine"                  => "Library Fine",
        "library_fine_member_type"      => "Member Type",
        "library_fine_member"           => "Member",
        "library_fine_amount"           => "Amount",
        "library_fine_status"           => "Status",
        "library_fine_due"              => "Due",
        "library_fine_paid"             => "Paid",
        "library_fine_remark"           => "Remark",
        "library_fine_date"             => "Date",
        "library_fine_name"             => "Name",

 
        "time_table"                    => "Time Table",  //For time table by Khushbu on 06 Sept 2018
        "add_time_table"                => "Add Time Table",
        "edit_time_table"               => "Edit Time Table",
        "view_time_table"               => "View Time Table",   
        "time_table_class_name"         => "Name",   
        "time_table_section"            => "Section",    
        "time_table_week_day"           => "Week Days",
        "set_time_table"                => "Set Time Table",
        "academic"                      => "Academic",
        "monday"                        => "Monday",
        "tuesday"                       => "Tuesday",
        "wednesday"                     => "Wednesday",
        "thursday"                      => "Thursday",
        "friday"                        => "Friday",
        "saturday"                      => "Saturday",

        "country"                       => "Country",  //For Country by Khushbu on 08 Sept 2018
        "add_country"                   => "Add Country",
        "view_country"                  => "View Country",
        "edit_country"                  => "Edit Country",
        "country_name"                  => "Country Name",    

        "state"                         => "State",   //For State by Khushbu on 08 Sept 2018
        "add_state"                     => "Add State",
        "view_state"                    => "View State",
        "edit_state"                    => "Edit State",
        "state_name"                    => "State Name",    

        "city"                          => "City",   //For City by Khushbu on 08 Sept 2018
        "add_city"                      => "Add City",
        "view_city"                     => "View City",
        "edit_city"                     => "Edit City",
        "city_name"                     => "City Name",


        "brochure"                      => "Brochure", // For Brochure module
        "add_brochure"                  => "Add Brochure",
        "view_brochure"                 => "View Brochures",
        "edit_brochure"                 => "Edit Brochure",
        "brochure_name"                 => "Brochure Name", 
        "brochure_upload_date"          => "Upload Date",
        "brochure_file"                 => "Brochure File",

        "admission_form"                => "Admission Form", // For Brochure module
        "add_admission_form"            => "Add Admission Form",
        "view_admission_form"           => "View Admission Forms",
        "edit_admission_form"           => "Edit Admission Form",
        "form_name"                     => "Form Name", 
        "no_of_intake"                  => "No of Intake",
        "form_prefix"                   => "Form Prefix",
        "form_instruction"              => "Instruction",
        "form_information"              => "Information",
        "form_type"                     => "Form Type",
        "form_last_date"                => "Application Last Date",
        "form_fee_amount"               => "Fees Amount",
        "form_pay_mode"                 => "Payment Mode",
        "form_email_receipt"            => "Email Receipt", 
        "form_success_message"          => "Success Message",
        "form_failed_message"           => "Failure Message",
        "form_message_via"              => "Message Via",

        "recruitment"                   => "Recruitment", //For Recruitment by Khushbu on 08 Sept 2018
        "job"                           => "Job",
        "add_job"                       => "Add Job",
        "view_job"                      => "View Job",
        "edit_job"                      => "Edit Job",  
        "job_name"                      => "Name",  
        "job_type"                      => "Type",
        "job_no_of_vacancy"             => "No of Vacancy",
        "job_recruitment"               => "Recruitment",
        "job_description"               => "Description",
        "job_durations"                 => "Durations",
        "view_interview_schedule"       => "View Interview Schedule",
        "view_candidate_list"           => "View Candidate List",
        "job_wise"                      => "Job Wise",
        "reports"                       => "Reports",
        "view_candidate"                => "View Candidate",
        "candidate_details"             => "Candidate Details",
        "applied_for_job"               => "Applied For Job",
        "job_names"                     => "Job Name",
        "no_of_remaining_vacancy"       => "No of Remaining Vacancy", 
        "email"                         => "Email",
        "contact"                       => "Contact",   
        "date_time"                     => "Date & Time",    

        "staff_attendance"              => "Staff Attendance", // For Staff Attendance
        "add_staff_attendance"          => "Add Staff Attendance", 
        "view_staff_attendance"         => "View Staff Attendance",
        "edit_staff_attendance"         => "Edit Staff Attendance",  
        "attendance"                    => "Attendance",

        "leave_application"              => "Leave Application", // For Staff Leave
        "add_leave_application"          => "Add Leave Application", 
        "view_leave_application"         => "View Leave Application",
        "edit_leave_application"         => "Edit Leave Application",  
        "leave_reason"                   => "Reason",   
        "leave_form_date"                => "Form Date",
        "leave_to_date"                  => "To Date",
        "leave_total_days"               => "Leave Total Days",


        "menu_configuration"             => "Configuration", // For menu
        "menu_academic"                  => "Academic",
        "menu_admission"                 => "Admission",
        "menu_student"                   => "Student",
        "menu_recruitment"               => "Recruitment",
        "menu_fee_collection"            => "Fee Collection",
        "menu_staff"                     => "Staff",
        "menu_examination"               => "Examination",
        "menu_hostel"                    => "Hostel",
        "menu_library"                   => "Library",
        "menu_transport"                 => "Transport",
        "menu_visitor"                   => "Visitor",
        "menu_online_content"            => "Online Content",
        "menu_notice_board"              => "Notice Board",
        "menu_task_manager"              => "Task Manager", 
        "menu_theme_options"             => "Theme Option's", 
        "menu_university"                => "University", 
        "menu_components"                => "Components", 
        "menu_extra"                     => "Extra",    

        "one_time"                       => "One Time", //For Fees Collection   
        "add_one_time"                   => "Add One Time",
        "edit_one_time"                  => "Edit One Time",
        "view_one_time"                  => "View One Time",
        "fees_particular_name"            => "Fees Particular Name",   



        "recurring_heads"               => "Recurring Heads", // For Recurring Heads 
        "add_recurring_heads"           => "Add Recurring Heads",
        "view_recurring_heads"          => "View Recurring Heads",
        "edit_recurring_heads"          => "Edit Recurring Heads",

        "prepaid_account"               => "Prepaid Account", // For Prepaid Account 
        "add_prepaid_account"           => "Add Prepaid Account",
        "view_prepaid_account"          => "View Prepaid Account",
        "edit_prepaid_account"          => "Edit Prepaid Account",        
               
        "vehicle"                       => "Vehicle", // For Vehicle
        "add_vehicle"                   => "Add Vehicle",
        "view_vehicle"                  => "View Vehicle",
        "edit_vehicle"                  => "Edit Vehicle",      

        "vehicle_document"              => "Vehicle Document", //For vehicle document
        "add_vehicle_document"          => "Add Vehicle Document",
        "view_vehicle_document"         => "View Vehicle Document",
        "edit_vehicle_document"         => "Edit Vehicle Document", 

        "vehicle_routes"                => "Vehicle Routes", // For vehicle routes
        "add_vehicle_routes"            => "Add Vehicle Route",
        "view_vehicle_routes"           => "View Vehicle Routes",
        "edit_vehicle_route"            => "Edit Vehicle Route",

        "driver"                        => "Driver",  // For Vehicle Driver
        "add_driver"                    => "Add Driver",
        "view_driver"                   => "View Driver",
        "edit_driver"                   => "Edit Driver",

        "conductor"                     => "Conductor", // For Conductor
        "add_conductor"                 => "Add Conductor",
        "view_conductor"                => "View Conductor",
        "edit_conductor"                => "Edit Conductor",


        "track_vehicle"                 => "Track Vehicle",   // For Track Vehicle
        "add_track_vehicle"             => "Add Track Vehicle",
        "view_track_vehicle"            => "View Track Vehicle",
        "edit_track_vehicle"            => "Edit Track Vehicle",

        "history_tracking"              => "History Tracking",


        "manage_fees"                   => "Manage Fees", // For Manage Fees
        "add_fees_head"                 => "Add Fees Head",
        "view_fees_head"                => "View Fees Head",
        "edit_fees_head"                => "Edit Fees Head",

        "check_vehicle_report"          => "Check Vehicle Report",  // For vehicle report
        "fees_report"                   => "Fees Report",
        "staff_attendance_report"       => "Staff Attendance Report",    

        "visitor_list"                  => "Visitor List",   // For Visitor List
        "add_visitor"                   => "Add Visitor",
        "view_visitor"                  => "View Visitor",
        "edit_visitor"                  => "Edit Visitor",    


        "inventory"                     => "Inventory",     // For Vendor of Inventory Module
        "manage_vendor"                 => "Manage Vendor",
        "inventory_s_no"                => "S No",
        "inventory_name"                => "Name",
        "inventory_gstin"               => "GSTIN",
        "inventory_contact_no"          => "Contact No",
        "inventory_email"               => "Email",
        "inventory_address"             => "Address",  
        "inventory_vendor_name"         => "Vendor Name",
        "inventory_form_gstin"          => "GSTIN",
        "inventory_form_contact_no"     => "Contact No",
        "inventory_form_email"          => "Email",
        "inventory_form_address"        => "Address", 

       
        "manage_unit"                   => "Manage Unit",   // For Unit of Inventory Module
        "unit_s_no"                     => "S No",
        "unit_name"                     => "Unit Name",    


        "manage_category"                   => "Manage Category", // For Category of Inventory Module
        "category_s_no"                     => "S No",
        "category_level"                    => "Category Level",
        "category_name"                     => "Category Name",   
        "s_category_name"                   => "Category Name",


        "manage_item"                       => "Manage Item", // For Items of Inventory Module
        "item_s_no"                         => "S No",
        "item_name"                         => "Item Name", 
        "item_category_name"                => "Category", 
        "item_sub_category_name"            => "Sub Category",
        "item_measuring_unit"               => "Measuring Unit", 
        "s_item_name"                       => "Item Name", 
         
    ];
