@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
/*.teacher_ids{
    color: red;
}*/
.dataTables_wrapper select {
    width: 196px !important;
}
.option{
    font-size: 14px !important;
    color: #ccc;
}
</style>
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.map_subject') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/teacher-subject-mapping/view-subject-teacher-mapping') !!}">{!! trans('language.map_subject_to_teacher') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.map_subject') !!}</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                                
                            <div class="body">
                                <span class="classname_padding">Class Name : <strong>{{$class_name}}</strong></span><br/>
                                <span class="text-success" id="display_cust_msg"></span>
                                <span class="text-danger" id="display_cust_msg2"></span>
                                {!! Form::hidden('class_id',$class_id,array('readonly' => 'true','id'=>'class_id')) !!}
                                    <div class="table-responsive" id="map-data">
                                        {{ csrf_field() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var class_id = $("#class_id").val();
        $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('admin-panel/teacher-subject-mapping/map-teacher-data')}}",
                    type: 'GET',
                    data: {class_id:class_id},
                    success: function (data) {

                        $('#map-data').html(data.data);
                        $("select[name='teacher'").selectpicker('refresh');
                    }
        });
           
        
    });

    $(function() {
        $("#check_all").on("click", function() {
            $(".check").prop("checked",$(this).prop("checked"));
        });

        $(".check").on("click", function() {
            var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
            $("#check_all").prop("checked", flag);
        });
    });

    function assignteacher(id){
        var teacher_ids = [];
        var class_id    = $('#class_id').val();
        $('#teacher_ids_'+id+' :selected').each(function(i, selectedElement) {
         teacher_ids[i]   = $(selectedElement).val();
        });
        
            if(confirm("Are you sure?")){
                $('.mycustloading').css('display','block');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('admin-panel/teacher-subject-mapping/save')}}",
                    type: 'GET',
                    data: {
                        'class_id': class_id,
                        'subject_id': id,
                        'teacher_ids': teacher_ids
                    },
                    success: function (data) {
                        $('.mycustloading').css('display','none');
                        if(data.status = 1001){
                            $('#display_cust_msg').html(data.message);
                        }else if(data.status = 1002){
                            $('#display_cust_msg2').html(data.message);
                        }   //window.location.reload(true);     
                    }
                });
            }
    }

</script>
@endsection




