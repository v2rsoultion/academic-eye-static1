<?php $__env->startSection('content'); ?>
<!-- Main Content -->
<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Student Attendance</h2>
            </div> 
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right ">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Transport</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Student Attendance</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/transport/vehicle/add-vehicle'); ?>">Edit</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div>
                    <div class="body form-gap">
                        <form class="" action="" method=""  id="form_validation">
                            <div class="row clearfix"> 
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1">Enroll No :</lable>
                                    <div class="form-group">
                                        <input type="text" name="enroll_no" class="form-control" placeholder="Enroll No">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1">Name:</lable>
                                    <div class="form-group">
                                        <input type="text" name="Name" class="form-control" placeholder="Name">
                                    </div>
                                </div>
                                 <div class="col-lg-3 col-md-3 col-sm-12">
                                    <lable class="from_one1">Father Name :</lable>
                                    <div class="form-group">
                                        <input  class="form-control positive-numeric-only" id="id-blah1" min="0" name="father_name" type="text" value="" placeholder="Father Name">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-12">
                                    <span class="from_one1">Profile Image:</span>
                                    <label for="file1" class="field file">
                                        <div class="form-group"> 
                                            <input type="file" class="gui-file" name="document_file" id="student_profile">
                                            <input type="hidden" class="gui-input" id="student_profile" placeholder="Upload Photo" readonly="">
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-3">
                              <div class="form-group">
                                <lable class="from_one1" for="name">Class</lable>
                                <select class="form-control show-tick select_form1" name="classes" id="">
                                  <option value="">Class</option>
                                  <option value="1">1st</option>
                                  <option value="2">2nd</option>
                                  <option value="3">3rd</option>
                                  <option value="4">4th</option>
                                  <option value="10th"> 10th</option>
                                  <option value="11th">11th</option>
                                  <option value="12th">12th</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="form-group">
                                <lable class="from_one1" for="name">Section</lable>
                                <select class="form-control show-tick select_form1" name="classes" id="">
                                  <option value="">Section</option>
                                  <option value="A">A</option>
                                  <option value="B">B</option>
                                  <option value="C">C</option>
                                  <option value="D">D</option>
                                </select>
                              </div>
                            </div>
                           <div class="col-lg-6 col-md-6 col-sm-12">
                                <lable class="from_one1">Attendance :</lable>
                                <div class="form-group">
                                    <div class="radio" style="margin-top:6px !important;">
                                        <input type="radio" name="type" id="radio1" value="option1">
                                        <label for="radio1" class="document_staff">In Attendance</label>
                                        <input type="radio" name="type" id="radio2" value="option2" checked="">
                                        <label for="radio2" class="document_staff">Out Attendance</label>
                                    </div>
                                </div>
                            </div>  
                            </div>
                            <div class="row clearfix">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-raised  btn-primary">Update</button>
                                    <button type="submit" class="btn btn-raised  btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<!-- <script src="assets/plugins/jquery-validation/jquery.validate.js"></script> 
<script src="assets/js/pages/forms/form-validation.js"></script> -->


<script>

function myFunction1() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "none";
    } else {
        x.style.display = "none";
    }
}
</script>

<style>
#myDIV {
    width: 100%;
}
</style>
<script type="text/javascript">

$('#form_validation').validate({
        rules: {
            'vehcile_name': {
                required: true
            },
            'vehcile_reg_number': {
                required: true
            },
            'type': {
                required: true
            },
            'capacity': {
                required: true
            },
            'contact_person': {
                required: true
            },
            'contact_number': {
                required: true
            }
        },

        /* @validation  error messages 
      ---------------------------------------------- */
      messages: {
        vehcile_name: {
          required: 'Please fill your required vehcile name'
        },
        vehcile_reg_number: {
          required: 'Please fill requerd vehcile reg number'
        },
        type: {
          required: 'Please fill requerd type'
        },
        capacity: {
          required: 'Please fill requerd capacity'
        },
        contact_person: {
          required: 'Please fill requerd contact person'
        },
        contact_number: {
          required: 'Please fill requerd contact number'
        }         
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

</script>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>