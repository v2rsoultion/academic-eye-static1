<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'staff_documents';
    protected $primaryKey = 'staff_document_id';
    public function up()
    {
        if (!Schema::hasTable('staff_documents')) {
            Schema::create('staff_documents', function (Blueprint $table) {
                $table->increments('staff_document_id');
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->integer('staff_id')->unsigned()->default(0);
                $table->integer('document_category_id')->unsigned()->nullable();
                $table->text('staff_document_details')->nullable();
                $table->text('staff_document_file')->nullable();
                $table->tinyInteger('staff_document_status')->default(0)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
            Schema::table('staff_documents', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('staff_documents', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('staff_documents', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
            Schema::table('staff_documents', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff')->onDelete('cascade');
            });
            Schema::table('staff_documents', function($table) {
                $table->foreign('document_category_id')->references('document_category_id')->on('document_category')->onDelete('cascade');
            });
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_documents');
    }
}
