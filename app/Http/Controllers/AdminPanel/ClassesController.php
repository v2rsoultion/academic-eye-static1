<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Classes\Classes;

use Yajra\Datatables\Datatables;

class ClassesController extends Controller
{
    /**
     *  View page for class
     *  @Shree on 18 July 2018
    **/
    public function index()
    {
        $class = [];
        $loginInfo              = get_loggedin_user_data();
        $arr_medium             = \Config::get('custom.medium_type');
        $class['arr_medium']    = add_blank_option($arr_medium, 'Select Medium');
        $data = array(
            'page_title'    => trans('language.view_class'),
            'redirect_url'  => url('admin-panel/class/view-classes'),
            'login_info'    => $loginInfo,
            'class'         => $class
        );
        return view('admin-panel.class.index')->with($data);
    }

    /**
     *  Add page for class
     *  @Shree on 18 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data   = [];
        $class  = [];
        $loginInfo = get_loggedin_user_data();
        $arr_medium     = \Config::get('custom.medium_type');
        if (!empty($id))
        {
            $decrypted_class_id = get_decrypted_value($id, true);
            $class              = Classes::Find($decrypted_class_id);
            if (!$class)
            {
                return redirect('admin-panel/class/add-class')->withError('Class not found!');
            }
            $page_title             = trans('language.edit_class');
            $decrypted_class_id     = get_encrypted_value($class->class_id, true);
            $save_url               = url('admin-panel/class/save/' . $decrypted_class_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_class');
            $save_url      = url('admin-panel/class/save');
            $submit_button = 'Save';
        }
        $class['arr_medium']   = add_blank_option($arr_medium, 'Select Medium');
        $data                           = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'class'         => $class,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/class/view-classes'),
        );
        return view('admin-panel.class.add')->with($data);
    }

    /**
     *  Add and update class's data
     *  @Shree on 18 July 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo  = get_loggedin_user_data();
        $decrypted_class_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $class = Classes::find($decrypted_class_id);

            if (!$class)
            {
                return redirect('/admin-panel/class/add-class/')->withError('Class not found!');
            }
            $success_msg = 'Class updated successfully!';
        }
        else
        {
            $class     = New Classes;
            $success_msg = 'Class saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'class_name'        => 'required|unique:classes,class_name,' . $decrypted_class_id . ',class_id',
                'medium_type'      => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $class->admin_id            = $loginInfo['admin_id'];
                $class->update_by           = $loginInfo['admin_id'];
                $class->class_name          = Input::get('class_name');
                $class->medium_type         = Input::get('medium_type');
                $class->class_order         = Input::get('class_order');
                $class->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/class/view-classes')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 18 July 2018
    **/
    public function anyData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $class = Classes::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->get('name')))
            {
                $query->where('class_name', "like", "%{$request->get('name')}%");
            }
            if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null)
            {
                $query->where('medium_type', "=", $request->get('medium_type'));
            }
        })->orderBy('class_order', 'ASC')->get();
        return Datatables::of($class)
        ->addColumn('medium_type', function ($class)
        {
            $arr_medium   = \Config::get('custom.medium_type');
            $medium_type  = $arr_medium[$class->medium_type];
            return $medium_type;
        })
        ->addColumn('class_students', function($class) {
            $encrypted_class_id = get_encrypted_value($class->class_id, true);
            return '0';
        })
        ->addColumn('action', function ($class)
        {
            $encrypted_class_id = get_encrypted_value($class->class_id, true);
            if($class->class_status == 0) {
                $status = 1;
                $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
            } else {
                $status = 0;
                $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
            }
            return '
                    <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="class-status/'.$status.'/' . $encrypted_class_id . '">'.$statusVal.'</a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-class/' . $encrypted_class_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                    <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-class/' . $encrypted_class_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
            
        })->rawColumns(['action' => 'action'])->addIndexColumn()
        ->make(true);
    }

    /**
     *  Destroy class's data
     *  @Shree on 18 July 2018
    **/
    public function destroy($id)
    {
        $class_id = get_decrypted_value($id, true);
        $class    = Classes::find($class_id);
        if ($class)
        {
            $class->delete();
            $success_msg = "Class deleted successfully!";
            return redirect('admin-panel/class/view-classes')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "class not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change class's status
     *  @Shree on 18 July 2018
    **/
    public function changeStatus($status,$id)
    {
        $class_id = get_decrypted_value($id, true);
        $class    = Classes::find($class_id);
        if ($class)
        {
            $class->class_status  = $status;
            $class->save();
            $success_msg = "Class deleted successfully!";
            return redirect('admin-panel/class/view-classes')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "class not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get class data according medium
     *  @Shree on 5 Sept 2018
    **/
    public function getClassData()
    {
        $medium_type = Input::get('medium_type');
        $classes = get_all_classes($medium_type);
        $data = view('admin-panel.class.ajax-class-select',compact('classes'))->render();
        return response()->json(['options'=>$data]);
    }

    /**
     *  Get total intake data according class
     *  @Shree on 12 Sept 2018
    **/
    public function getIntakeData()
    {
        $totalIntake = 0;
        $class_id = Input::get('class_id');
        $totalIntake = get_total_intake($class_id);
        return $totalIntake;
    }
}
