<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Notes\Notes; // Model
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\Datatables\Datatables;

class NoteController extends Controller
{
    /**
     *  View page for Notes
     *  @Ashish on 31 July 2018
    **/
    public function index()
    {
        $listData  = $arr_class = $arr_subject  = [];
        $listData['arr_class']      = add_blank_option($arr_class, 'Select Class');
        $listData['arr_subject']    = add_blank_option($arr_subject, 'Select Subject');
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_notes'),
            'redirect_url'  => url('admin-panel/notes/view-notes'),
            'login_info'    => $loginInfo,
            'listData'      => $listData

        );
        return view('admin-panel.notes.index')->with($data);
    }

    /**
     *  Add page for Notes
     *  @Ashish on 31 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data  = $notes = 	$arr_subject = $arr_class = $arr_section = [];
        $notes 			= [];
        $class_id       = null;
        $loginInfo 		= get_loggedin_user_data();
        $arr_medium         = \Config::get('custom.medium_type');
        if (!empty($id))
        {
            $decrypted_notes_id 	= get_decrypted_value($id, true);
            $notes      			= Notes::Find($decrypted_notes_id);
            if (!$notes)
            {
                return redirect('admin-panel/notes/add-notes')->withError('Notes not found!');
            }
            $page_title             	= trans('language.edit_notes');
            $encrypted_notes_id   		= get_encrypted_value($notes->online_note_id, true);
            $save_url               	= url('admin-panel/notes/save/' . $encrypted_notes_id);
            $submit_button          	= 'Update';
            $arr_class                   = get_all_classes();
            $arr_section                 = get_class_section($notes->class_id);

        }
        else
        {
            $page_title    = trans('language.add_notes');
            $save_url      = url('admin-panel/notes/save');
            $submit_button = 'Save';
        }
        $notes['arr_medium']         = add_blank_option($arr_medium, 'Select Medium');
        $notes['arr_class']          = add_blank_option($arr_class, 'Select class');
        $notes['arr_section']        = add_blank_option($arr_section, 'Select section');
        $notes['arr_subject']        = add_blank_option($arr_subject, 'Select subject');

        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'notes' 			=> $notes,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/notes/view-notes'),
        );
        return view('admin-panel.notes.add')->with($data);
    }

    /**
     *  Get sections data according class
     *  @Ashish on 30 July 2018
    **/
    public function getSectionData()
    {
        $class_id = Input::get('class_id');
        $section = get_class_section($class_id);
        $data = view('admin-panel.notes.ajax-select',compact('section'))->render();
        return response()->json(['options'=>$data]);
    }


    /**
     *  Add and update Notes's data
     *  @Ashish on 31 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo      			= get_loggedin_user_data();
        $decrypted_notes_id			= get_decrypted_value($id, true);
        $admin_id = $loginInfo['admin_id'];
        if (!empty($id))
        {
            $notes = Notes::find($decrypted_notes_id);
            $admin_id = $notes['admin_id'];
            if (!$notes)
            {
                return redirect('/admin-panel/notes/add-notes/')->withError('notes not found!');
            }
            $success_msg = 'notes updated successfully!';
        }
        else
        {
            $notes     		= New Notes;
            $success_msg 	= 'notes saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                't_name'   => 'required|unique:online_notes,online_note_name,' . $decrypted_notes_id . ',online_note_id',
                'notes_class_id'           => 'required',
                'notes_section_id'         => 'required',
                'notes_subject_id'         => 'required',
                'unit'                     => 'required',
                'topic'                    => 'required',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $notes->admin_id             = $admin_id;
                $notes->update_by            = $loginInfo['admin_id'];
                $notes->staff_id             = $loginInfo['admin_id'];
                $notes->online_note_name 	 = Input::get('t_name');
                $notes->online_note_unit 	 = Input::get('unit');
                $notes->online_note_topic 	 = Input::get('topic');
                $notes->class_id 	         = Input::get('notes_class_id');
                $notes->section_id 	         = Input::get('notes_section_id');
                $notes->subject_id 	         = Input::get('notes_subject_id');
                $notes->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/notes/view-notes')->withSuccess($success_msg);
    }

    /**
     *  Get Notes's Data for view page(Datatables)
     *  @Ashish on 31 July 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo	= get_loggedin_user_data();
        $class_id   = null;
        $subject_id = null;
        $class_id   = $request->get('class_id');
        $subject_id = $request->get('subject_id');
        $notes      = Notes::where('school_id',$loginInfo['school_id'])
                     ->where(function($query) use ($class_id,$subject_id) 
                        { 
                            if (!empty($class_id)) 
                            {
                             $query->where('class_id', $class_id); 
                            }
                            if (!empty($subject_id)) 
                            {
                             $query->where('subject_id', $subject_id); 
                            }
                        })
                    ->orderBy('online_note_id', 'DESC')
                    ->with('getClass')
                    ->with('getSection')
                    ->with('getSubject')
                    ->get();

        return Datatables::of($notes)
                ->addColumn('online_note_topic', function ($notes)
                {
                     return substr($notes->online_note_topic,0,40)."...";
                    
                })
                ->addColumn('class_subject', function ($notes)
                {
                     return $notes['getSubject']['subject_name'];
                    
                })
                ->addColumn('class_section', function ($notes)
                {
                     return $notes['getClass']['class_name'].' - '.$notes['getSection']['section_name'];
                    
                })
        		->addColumn('action', function ($notes)
                {
                    $encrypted_notes_id = get_encrypted_value($notes->online_note_id, true);
                    if($notes->online_note_status == 0) {
                        $status = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="notes-status/'.$status.'/' . $encrypted_notes_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-notes/' . $encrypted_notes_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-notes/' . $encrypted_notes_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['online_note_topic' => 'online_note_topic', 'class_subject' => 'class_subject','class_section' => 'class_section','action' => 'action'])->addIndexColumn()
                ->make(true);
    }

    /**
     *  Destroy Notes's data
     *  @Ashish on 31 July 2018.
    **/
    public function destroy($id)
    {
        $notes_id 		= get_decrypted_value($id, true);
        $notes 		  	= Notes::find($notes_id);
        if ($notes)
        {
            $notes->delete();
            $success_msg = "Notes deleted successfully!";
            return redirect('admin-panel/notes/view-notes')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Notes not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Notes's status
     *  @Ashish on 31 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $notes_id 		= get_decrypted_value($id, true);
        $notes 		  	= Notes::find($notes_id);
        if ($notes)
        {
            $notes->online_note_status  = $status;
            $notes->save();
            $success_msg = "Notes status updated!";
            return redirect('admin-panel/notes/view-notes')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Notes not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

}
