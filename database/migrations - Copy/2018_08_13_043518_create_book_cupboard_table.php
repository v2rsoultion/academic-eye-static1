<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookCupboardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'book_cupboards';
    protected $primaryKey = 'book_cupboard_id';
    public function up()
    {
        if (!Schema::hasTable('book_cupboards')){ 
            Schema::create('book_cupboards', function (Blueprint $table) {
                $table->increments('book_cupboard_id');
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->string('book_cupboard_name', 255)->nullable();
                $table->tinyInteger('book_cupboard_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('book_cupboards', function($table) {
                    $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
                });
            Schema::table('book_cupboards', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('book_cupboards', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_cupboards');
    }
}
