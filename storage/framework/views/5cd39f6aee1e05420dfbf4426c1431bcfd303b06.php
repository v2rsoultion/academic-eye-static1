
<?php $__env->startSection('content'); ?>
<style type="text/css">
  td{
    padding: 14px 10px !important;
  }
  .table-responsive {
    overflow-x: visible !impportant;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Grade Scheme</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
         <a href="<?php echo url('admin-panel/examination/grade-scheme/add-grade-scheme'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/examination'); ?>">Examination And Report Cards</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/examination'); ?>">Grade Scheme</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/examination/grade-scheme/view-grade-scheme'); ?>">View</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 20px !important;">
                     <form class="" action="" method=""  id="" style="width: 100%;">
                      <div class="row">
                       <div class="col-lg-3">
                         <div class="form-group">
                            <lable class="from_one1" for="name">Scheme Name</lable>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Scheme Name">

                         </div>
                      </div>
                 <div class="col-lg-3">
                         <div class="form-group">
                            <lable class="from_one1" for="name">Grade Point</lable>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Grade Point">
                         </div>
                      </div>
                     <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1 ">
                      <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear">Clear
                      </button>
                    </div>
                    </div>
                   </form>
                   <hr>
                    <!--  DataTable for view Records  -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                      <thead>
                        <tr>
                          <th>S No</th>
                          <th>Scheme Name </th>
                          <th>Grade Type</th>
                          <th>Range (Max - Min) </th>
                          <th>Grade Point</th>
                          <th>Action</th>
                         
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 15; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td>Norm-Referenced Grading Systems </td>
                          <td>Percent</td>
                          <td>
                           90% - 95%
                          </td>
                         
                          <td>10</td>
                           <td>
                            <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div>
            
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<?php $__env->stopSection(); ?>
<!-- Content end here  -->

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>