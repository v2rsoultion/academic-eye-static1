<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DriverController extends Controller
{
    /**
     *  View page for Vehicle Driver
     *  @Khushbu on 28 Sept 2018   
    **/
    public function index()
    {
        $loginInfo 		    = get_loggedin_user_data();
        
        $data 				= array(
            'page_title'    => trans('language.view_driver'),
            'redirect_url'  => url('admin-panel/transport/driver/view-driver'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.driver.view-driver')->with($data);
    }

    /**
     *  Add page for Vehicle Driver
     *  @Khushbu on 28 Sept 2018   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        // $job        = [];
        if (!empty($id))
        {
            $page_title             = trans('language.edit_driver');
            $save_url               = url('admin-panel/transport/driver/save/' . $decrypted_job_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_driver');
            $save_url      = url('admin-panel/transport/driver/save');
            $submit_button = 'Save';
        }

        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/driver/view-driver'),
        );
        return view('admin-panel.driver.add-driver')->with($data);
    }
}
