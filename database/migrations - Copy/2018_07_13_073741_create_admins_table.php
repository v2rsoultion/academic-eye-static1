<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'admins';
    protected $primaryKey = 'admin_id';
    public function up()
    {
        if (!Schema::hasTable('admins'))
        {
            Schema::create('admins', function (Blueprint $table)
            {
                $table->increments('admin_id')->unsigned();
                $table->integer('admin_type')->unsigned()->default('0')->comment = '0: super admin,1: school admin,2: staff admin,3: parent admin,4: student admin';
                $table->string('admin_name')->nullable();
                $table->string('email');
                $table->string('password');
                $table->text('admin_profile_img')->nullable();
                $table->rememberToken();
                $table->timestamps();
            });
        }
        // Insert some stuff
        DB::table('admins')->insert(
            array(
                'admin_id'          => 1,
                'admin_name'        => 'Super Admin',
                'email'             => 'admin@gmail.com',
                'password'          => '$2y$10$ivvyJkg.XjVg1IhMrQlezuNIFBgMLh3M0UnpRHetyVuMABHhN.CwK',
                'default_password'  => '$2y$10$ivvyJkg.XjVg1IhMrQlezuNIFBgMLh3M0UnpRHetyVuMABHhN.CwK',
                'remember_token'    => 'oJJmPSevfhpZdKdRf2iSfnKLM8boFxrkWkYP1GjANsSkmcU6CMBNHkdMIfcx',
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
