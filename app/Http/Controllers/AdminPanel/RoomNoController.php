<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\RoomNo\RoomNo; // Model
use Yajra\Datatables\Datatables;

class RoomNoController extends Controller
{
    /**
     *  View page for Room No
     *  @Pratyush on 28 July 2018
    **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_room_no'),
            'redirect_url'  => url('admin-panel/room-no/view-room-no'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.room-no.index')->with($data);
    }

    /**
     *  Add page for Room No
     *  @Pratyush on 28 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data    		= [];
        $room_no 		= [];
        $loginInfo 		= get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_room_no_id 	= get_decrypted_value($id, true);
            $room_no      			= RoomNo::Find($decrypted_room_no_id);
            if (!$room_no)
            {
                return redirect('admin-panel/room-no/add-room-no')->withError('Room No not found!');
            }
            $page_title             	= trans('language.edit_room_no');
            $encrypted_room_no_id   		= get_encrypted_value($room_no->room_no_id, true);
            $save_url               	= url('admin-panel/room-no/save/' . $encrypted_room_no_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $page_title    = trans('language.add_room_no');
            $save_url      = url('admin-panel/room-no/save');
            $submit_button = 'Save';
        }
        $data                           = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'room_no' 			=> $room_no,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/room-no/view-room-no'),
        );
        return view('admin-panel.room-no.add')->with($data);
    }

    /**
     *  Add and update Room No's data
     *  @Pratyush on 28 July 2018.
    **/
    public function save(Request $request, $id = NULL)
    {

        $loginInfo      			= get_loggedin_user_data();
        $decrypted_room_no_id		= get_decrypted_value($id, true);
        if (!empty($id))
        {
            $room_no = RoomNo::find($decrypted_room_no_id);

            if (!$room_no)
            {
                return redirect('/admin-panel/room-no/add-room-no/')->withError('Room No not found!');
            }
            $success_msg = 'Room No updated successfully!';
        }
        else
        {
            $room_no     	= New RoomNo;
            $success_msg 	= 'Room No saved successfully!';
        }

        $validatior = Validator::make($request->all(), [
                'room_no'   => 'required|unique:room_no,room_no,' . $decrypted_room_no_id . ',room_no_id',
        ]);

        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $room_no->admin_id       = $loginInfo['admin_id'];
                $room_no->update_by      = $loginInfo['admin_id'];
                $room_no->room_no 	   	 = Input::get('room_no');
                $room_no->save();
            }
            catch (\Exception $e)
            {
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }
        return redirect('admin-panel/room-no/view-room-no')->withSuccess($success_msg);
    }

    /**
     *  Get Room No's Data for view page(Datatables)
     *  @Pratyush on 28 July 2018.
    **/
    public function anyData(Request $request)
    {
        $loginInfo 	   = get_loggedin_user_data();
        $room_no   = RoomNo::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('room_no')))
            {
                $query->where('room_no', "like", "%{$request->get('room_no')}%");
            }
        })->orderBy('room_no_id', 'ASC')->get();
       
        return Datatables::of($room_no)
        		->addColumn('action', function ($room_no)
                {
                    $encrypted_room_no_id = get_encrypted_value($room_no->room_no_id, true);
                    if($room_no->room_no_status == 0) {
                        $status = 1;
                        $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                    } else {
                        $status = 0;
                        $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                    }
                    return '
                            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="room-no-status/'.$status.'/' . $encrypted_room_no_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-room-no/' . $encrypted_room_no_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-room-no/' . $encrypted_room_no_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
                })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Destroy Room No's data
     *  @Pratyush on 28 July 2018.
    **/
    public function destroy($id)
    {
        $room_no_id		= get_decrypted_value($id, true);
        $room_no	  	= RoomNo::find($room_no_id);
        if ($room_no)
        {
            $room_no->delete();
            $success_msg = "Room No deleted successfully!";
            return redirect('admin-panel/room-no/view-room-no')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Room No not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Room No's status
     *  @Pratyush on 28 July 2018.
    **/
    public function changeStatus($status,$id)
    {
        $room_no_id		= get_decrypted_value($id, true);
        $room_no	  	= RoomNo::find($room_no_id);
        if ($room_no)
        {
            $room_no->room_no_status  = $status;
            $room_no->save();
            $success_msg = "Room No status updated!";
            return redirect('admin-panel/room-no/view-room-no')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Room No not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
