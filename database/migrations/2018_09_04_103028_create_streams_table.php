<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStreamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'streams';
    protected $primaryKey = 'stream_id';
    public function up()
    {
        if (!Schema::hasTable('streams')){ 
            Schema::create('streams', function (Blueprint $table) {
                $table->increments('stream_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->tinyInteger('medium_type')->default(1)->comment = '0=Hindi,1=English';
                $table->string('stream_name', 255)->nullable();
                $table->tinyInteger('stream_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('streams', function($table) {
                    $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
                });
            Schema::table('streams', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('streams');
    }
}
