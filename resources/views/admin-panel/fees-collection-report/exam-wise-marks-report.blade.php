@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
   .table-responsive {
    overflow-x: auto !important;
   }
   /*.card .body .table td, .cshelf1 {
    width: 100px !important;
   }*/
  
   td{
    padding: -1px !important;
   }
   
</style>
<!--  Main content here -->
<section class="content">
   <div class="block-header">
      <div class="row">
         <div class="col-lg-5 col-md-6 col-sm-12">
            <h2>Exam Wise Marks Report</h2>
         </div>
         <div class="col-lg-7 col-md-6 col-sm-12 line">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">{!! trans('language.menu_fee_collection') !!}</a></li>
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">Reports</a></li>
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection/report/view-reports') !!}">View Reports</a></li>
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection/report/view-reports/exam-wise-marks-report') !!}">Exam Wise Marks Report</a></li>
            </ul>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row clearfix">
         <div class="col-lg-12" id="bodypadd">
            <div class="tab-content">
               <div class="tab-pane active" id="classlist">
                  <div class="card">
                  <div class="body form-gap">
                   <!-- <div class="row"> -->
                  
                  <div class="table-responsive position">
                  <!--  DataTable for view Records  -->
                  <table class="table  m-b-0 c_list table-bordered text-center" id="exam-wise-marks-report" style="width: 100%;">
                    <thead class="bold">
                    <tr>
                      <td rowspan="2">Roll No</td>
                      <td rowspan="2">Student Photo + Name</td>
                      <td colspan="10">Mid Term - 2018</td>
                    </tr>
                    <!-- <tr>
                      <td colspan="2">Sub 1</td>
                      <td colspan="2">Sub 2</td>
                      <td colspan="2">Sub 3</td>
                      <td colspan="2">Sub 4</td>
                      <td colspan="2">Sub 5</td>
                    </tr> -->
                    <tr>
                      <td>Sub-1 Total Marks</td>
                      <td>Sub-1 Obtain Marks</td>
                      <td>Sub-2 Total Marks</td>
                      <td>Sub-2 Obtain Marks</td>
                      <td>Sub-3 Total Marks</td>
                      <td>Sub-3 Obtain Marks</td>
                      <td>Sub-4 Total Marks</td>
                      <td>Sub-4 Obtain Marks</td>
                      <td>Sub-5 Total Marks</td>
                      <td>Sub-5 Obtain Marks</td>
                    </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Rajesh Sharma</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Mahesh Verma</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Hitesh Sharma</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>Brijesh Sharma</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>Gitesh Sharma</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                        <td>100</td>
                        <td>85</td>
                      </tr>
                    </tbody>
                  </table>
              </div>          
             </div>
            </div>
           </div>
          </div>
        </div>
       </div>
      </div>
  
</section>
<!-- Content end here  -->

<script type="text/javascript">
  $(document).ready(function () {
        var table = $('#exam-wise-marks-report').DataTable({
           dom: 'Blfrtip',
            buttons: [
                'csvHtml5'
            ]
        });
    });

</script>
@endsection
