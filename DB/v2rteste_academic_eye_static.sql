-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2020 at 12:41 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `v2rteste_academic_eye_static`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `admin_type` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0: super admin,1: school admin,2: staff admin,3: parent admin,4: student admin',
  `admin_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_profile_img` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_type`, `admin_name`, `email`, `password`, `admin_profile_img`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 0, 'Super Admin', 'admin@gmail.com', '$2y$10$9vjZrRPGbokSeNYP.sQ8wOo1MeJYdiSMvH2zEHCn//VrbxGZPxtWG', NULL, 'wNeMaRo6akaKICCLPsFr79l62jI741x7Vj9syRzLBY4MCwBWJ5MQSwSJN0ir', '2018-10-21 23:33:44', '2018-10-21 23:33:44'),
(2, 1, 'School Admin', 'school@gmail.com', '$2y$10$9vjZrRPGbokSeNYP.sQ8wOo1MeJYdiSMvH2zEHCn//VrbxGZPxtWG', NULL, 'FXrgAgfRih6zrEJ48KHCJo5qqYKscojs68tCNyijCFDprANuMAuVNO4oHRWL', '2018-10-21 23:33:44', '2018-10-21 23:33:44'),
(3, 2, 'Ravi', 'ravi@gmail.com', '$2y$10$dNnpqgsolLdj6IN6IObbmOarH8RQmfT0b1Aau6hKdfliThy/UZFtW', NULL, 'gpFccPk6EB5Oo7PIZc8v7Fnmx72IR1GDqBEswvMCzN3LMQe9JHoVrmuYyTIs', '2018-10-22 00:26:59', '2018-10-22 00:26:59'),
(4, 3, 'Shree', 'Bhagyashree@gmail.com', '$2y$10$pprJeRr7c/f.A7Fi4UozVuFkHerPlUtY21MkOEhinaZMQxlwjgFQe', NULL, 'cdyB8OPr8NfNWsX6hatjC1flkeFJJxngHeP1MCQfIAafIQOPuCE4xmTYuqnt', '2018-10-23 04:29:14', '2018-10-23 04:29:14'),
(5, 4, 'Shree', 'student@gmail.com', '$2y$10$mWRGQAiljso1fNKDj/E5c.n3JgGXW69GHWy0Km8UY7fkKnHyRXuX.', NULL, 'Aj3fguCCwScapscr2mtmDWtvyUt5PQqqLchwBCoy7NYy8n2j6AUiUaMlpON7', '2018-10-23 04:29:22', '2018-10-23 04:29:22'),
(6, 2, 'subject_teacher', 'subject_teacher@gmail.com', '$2y$10$dNnpqgsolLdj6IN6IObbmOarH8RQmfT0b1Aau6hKdfliThy/UZFtW', NULL, 'QnPdFtzzpU6cLWWY4h47zczW9BWd1jNflk45oUfcgAeI7p7N0UqaYTuTalWn', '2018-10-22 00:26:59', '2018-10-22 00:26:59'),
(7, 2, 'exam_manager', 'exam_manager@gmail.com', '$2y$10$dNnpqgsolLdj6IN6IObbmOarH8RQmfT0b1Aau6hKdfliThy/UZFtW', NULL, 'vrpgudJbXiYkWXBRZxs8nWTtV65KiQFZAE6mBKDuDlchbEnxZpARtJvvDF0c', '2018-10-22 00:26:59', '2018-10-22 00:26:59'),
(8, 2, 'hostel', 'hostel@gmail.com', '$2y$10$dNnpqgsolLdj6IN6IObbmOarH8RQmfT0b1Aau6hKdfliThy/UZFtW', NULL, 'nna1olcB9biFWO7WeS6yhlvXVypyNOmBqnu43onAVULY9fqgyCSLvUgjvDld', '2018-10-22 00:26:59', '2018-10-22 00:26:59'),
(9, 2, 'library', 'library@gmail.com', '$2y$10$dNnpqgsolLdj6IN6IObbmOarH8RQmfT0b1Aau6hKdfliThy/UZFtW', NULL, 'xMyGDsQtQkXWZAGXTe6Ppycb3Z5Q8cef8M0OEqEsXaCnv9zEDbbu0DY3dUbA', '2018-10-22 00:26:59', '2018-10-22 00:26:59'),
(10, 2, 'finance_head', 'finance_head@gmail.com', '$2y$10$dNnpqgsolLdj6IN6IObbmOarH8RQmfT0b1Aau6hKdfliThy/UZFtW', NULL, 'G4ORUNJHqjLQCcre1SFddLgfhD19OJq5KZbuBKMdQ1Wi1IxuEmdwQ1QHEHl8', '2018-10-22 00:26:59', '2018-10-22 00:26:59'),
(11, 2, 'guard', 'guard@gmail.com', '$2y$10$dNnpqgsolLdj6IN6IObbmOarH8RQmfT0b1Aau6hKdfliThy/UZFtW', NULL, 'leyAFuAIyIIdbrRa7wSZJ5QQOWwGYtWGXGfmhhGT1mjAGPbwoIbtX7SGTQF3', '2018-10-22 00:26:59', '2018-10-22 00:26:59');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `bank_id` int(10) UNSIGNED NOT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `book_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_category_id` int(10) UNSIGNED DEFAULT NULL,
  `book_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=General ,1=Barcoded',
  `book_subtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_isbn_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publisher_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_id` int(10) UNSIGNED DEFAULT NULL,
  `book_copies_no` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_cupboard_id` int(10) UNSIGNED DEFAULT NULL,
  `book_cupboardshelf_id` int(10) UNSIGNED DEFAULT NULL,
  `book_price` double(18,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `book_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `book_allowance`
--

CREATE TABLE `book_allowance` (
  `book_allowance_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `allow_for_staff` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `allow_for_student` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_allowance`
--

INSERT INTO `book_allowance` (`book_allowance_id`, `admin_id`, `update_by`, `allow_for_staff`, `allow_for_student`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, 0, '2018-10-21 23:39:40', '2018-10-21 23:39:40');

-- --------------------------------------------------------

--
-- Table structure for table `book_categories`
--

CREATE TABLE `book_categories` (
  `book_category_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_category_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `book_copies_info`
--

CREATE TABLE `book_copies_info` (
  `book_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_id` int(10) UNSIGNED DEFAULT NULL,
  `book_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exclusive_for_staff` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=No,1=Yes',
  `book_copy_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Available,1=Issued',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `book_cupboards`
--

CREATE TABLE `book_cupboards` (
  `book_cupboard_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_cupboard_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_cupboard_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `book_cupboardshelfs`
--

CREATE TABLE `book_cupboardshelfs` (
  `book_cupboardshelf_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_cupboard_id` int(10) UNSIGNED DEFAULT NULL,
  `book_cupboardshelf_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_cupboard_capacity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `book_cupboard_shelf_detail` text COLLATE utf8mb4_unicode_ci,
  `book_cupboardshelf_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `book_vendors`
--

CREATE TABLE `book_vendors` (
  `book_vendor_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_vendor_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `book_vendor_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brochures`
--

CREATE TABLE `brochures` (
  `brochure_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `brochure_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brochure_upload_date` date DEFAULT NULL,
  `brochure_file` text COLLATE utf8mb4_unicode_ci,
  `brochure_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `caste`
--

CREATE TABLE `caste` (
  `caste_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `caste_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caste_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `caste`
--

INSERT INTO `caste` (`caste_id`, `admin_id`, `update_by`, `caste_name`, `caste_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Rajput', 1, '2018-10-22 00:20:14', '2018-10-22 00:20:14'),
(2, 1, 1, 'Sain', 1, '2018-10-22 00:20:21', '2018-10-22 00:20:21'),
(3, 1, 1, 'Jain', 1, '2018-10-22 00:20:29', '2018-10-22 00:20:29');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(10) UNSIGNED NOT NULL,
  `state_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `city_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `state_id`, `city_name`, `city_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Jodhpur', 1, '2018-10-22 00:23:55', '2018-10-22 00:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `class_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `board_id` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `class_stream` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `class_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`class_id`, `admin_id`, `update_by`, `class_name`, `class_order`, `board_id`, `medium_type`, `class_stream`, `class_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Class I', 0, 1, 1, 0, 1, '2018-10-22 00:09:02', '2018-10-22 00:09:02'),
(2, 1, 1, 'Class II', 0, 1, 1, 0, 1, '2018-10-22 00:10:05', '2018-10-22 00:10:05');

-- --------------------------------------------------------

--
-- Table structure for table `class_subject_staff_map`
--

CREATE TABLE `class_subject_staff_map` (
  `class_subject_staff_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_ids` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `competitions`
--

CREATE TABLE `competitions` (
  `competition_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `competition_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `competition_description` text COLLATE utf8mb4_unicode_ci,
  `competition_date` date DEFAULT NULL,
  `competition_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=School,1=Class',
  `competition_class_ids` text COLLATE utf8mb4_unicode_ci,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `competition_issue_participation_certificate` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `competition_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `competition_map`
--

CREATE TABLE `competition_map` (
  `student_competition_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `competition_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `position_rank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `concession_map`
--

CREATE TABLE `concession_map` (
  `concession_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `head_id` int(10) UNSIGNED DEFAULT NULL,
  `reason_id` int(10) UNSIGNED DEFAULT NULL,
  `head_type` text COLLATE utf8mb4_unicode_ci,
  `concession_amt` int(10) UNSIGNED DEFAULT NULL,
  `concession_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Unpaid,1=Paid',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `concession_map`
--

INSERT INTO `concession_map` (`concession_map_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `student_id`, `head_id`, `reason_id`, `head_type`, `concession_amt`, `concession_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 'ONETIME', 100, 0, '2018-10-23 04:40:51', '2018-10-23 04:40:51');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` int(10) UNSIGNED NOT NULL,
  `country_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`, `country_status`, `created_at`, `updated_at`) VALUES
(1, 'India', 1, '2018-10-22 00:23:15', '2018-10-22 00:23:15');

-- --------------------------------------------------------

--
-- Table structure for table `co_scholastic_types`
--

CREATE TABLE `co_scholastic_types` (
  `co_scholastic_type_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `update_by` int(10) UNSIGNED NOT NULL,
  `co_scholastic_type_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `co_scholastic_type_description` text COLLATE utf8mb4_unicode_ci,
  `co_scholastic_type_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `designation_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `designation_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation_description` text COLLATE utf8mb4_unicode_ci,
  `designation_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`designation_id`, `admin_id`, `update_by`, `designation_name`, `designation_description`, `designation_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Class Teacher', NULL, 1, '2018-10-22 00:18:28', '2018-10-22 00:18:28');

-- --------------------------------------------------------

--
-- Table structure for table `document_category`
--

CREATE TABLE `document_category` (
  `document_category_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `document_category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_category_for` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=student,1=staff,2=both',
  `document_category_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `exam_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `term_exam_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `exam_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`exam_id`, `admin_id`, `update_by`, `term_exam_id`, `exam_name`, `medium_type`, `exam_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, 'Exam 1', 1, 1, '2018-10-22 00:43:58', '2018-10-22 00:44:16'),
(2, 1, 1, NULL, 'Exam 2', 1, 1, '2018-10-22 00:44:08', '2018-10-22 00:44:08');

-- --------------------------------------------------------

--
-- Table structure for table `exam_map`
--

CREATE TABLE `exam_map` (
  `exam_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `marks_criteria_id` int(10) UNSIGNED DEFAULT NULL,
  `grade_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exam_map`
--

INSERT INTO `exam_map` (`exam_map_id`, `admin_id`, `update_by`, `exam_id`, `class_id`, `subject_id`, `marks_criteria_id`, `grade_id`, `created_at`, `updated_at`) VALUES
(21, 1, 1, 2, 1, 1, 2, NULL, '2018-10-23 02:01:12', '2018-10-23 04:00:30'),
(22, 1, 1, 2, 1, 2, 1, NULL, '2018-10-23 02:01:12', '2018-10-23 04:00:30'),
(23, 1, 1, 2, 1, 3, NULL, NULL, '2018-10-23 02:01:12', '2018-10-23 02:01:12'),
(24, 1, 1, 2, 1, 4, NULL, NULL, '2018-10-23 02:01:12', '2018-10-23 02:01:12'),
(25, 1, 1, 2, 1, 5, NULL, NULL, '2018-10-23 02:01:12', '2018-10-23 02:01:12');

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `facility_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `facility_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facility_fees_applied` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=not apply,1=apply',
  `facility_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fees_st_map`
--

CREATE TABLE `fees_st_map` (
  `fees_st_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `head_id` int(10) UNSIGNED DEFAULT NULL,
  `head_type` text COLLATE utf8mb4_unicode_ci,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_ids` text COLLATE utf8mb4_unicode_ci COMMENT 'student ids are stored',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fee_receipt`
--

CREATE TABLE `fee_receipt` (
  `receipt_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `receipt_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt_date` date DEFAULT NULL,
  `total_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total receipt amount',
  `wallet_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Wallet amount',
  `total_concession_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total Concession amount',
  `total_fine_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total Fine amount',
  `total_calculate_fine_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total Calculate Fine amount',
  `net_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total net amount',
  `pay_later_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Pay later amount',
  `imprest_amount` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Imprest amount',
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `pay_mode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque_date` date DEFAULT NULL COMMENT 'If cheque mode',
  `cheque_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If cheque mode',
  `cheque_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'If cheque mode',
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If online mode',
  `transaction_date` date DEFAULT NULL COMMENT 'If online mode',
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fee_receipt_details`
--

CREATE TABLE `fee_receipt_details` (
  `fee_receipt_detail_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `receipt_id` int(10) UNSIGNED DEFAULT NULL,
  `head_id` int(10) UNSIGNED DEFAULT NULL,
  `head_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `head_inst_id` int(10) UNSIGNED DEFAULT NULL,
  `inst_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total installment amount',
  `inst_paid_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Total paid installment amount',
  `inst_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Less,1=complete',
  `r_concession_map_id` int(10) UNSIGNED DEFAULT NULL,
  `r_concession_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Concession amount',
  `r_fine_id` int(10) UNSIGNED DEFAULT NULL,
  `r_fine_detail_id` int(10) UNSIGNED DEFAULT NULL,
  `r_fine_detail_days` int(10) UNSIGNED DEFAULT NULL,
  `r_fine_amt` decimal(10,2) UNSIGNED DEFAULT NULL COMMENT 'Fine amount',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fine`
--

CREATE TABLE `fine` (
  `fine_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `fine_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fine_for` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=One Time Fees,1=Recurring Fees',
  `head_id` int(10) UNSIGNED DEFAULT NULL,
  `fine_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fine_details`
--

CREATE TABLE `fine_details` (
  `fine_detail_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `fine_id` int(10) UNSIGNED DEFAULT NULL,
  `fine_detail_days` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fine_detail_amt` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `grade_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `grade_scheme_id` int(10) UNSIGNED DEFAULT NULL,
  `grade_min` int(10) UNSIGNED DEFAULT NULL,
  `grade_max` int(10) UNSIGNED DEFAULT NULL,
  `grade_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade_point` double DEFAULT NULL,
  `grade_remark` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`grade_id`, `admin_id`, `update_by`, `grade_scheme_id`, `grade_min`, `grade_max`, `grade_name`, `grade_point`, `grade_remark`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 90, 100, 'A', 100, NULL, '2018-10-22 00:45:24', '2018-10-22 00:45:24'),
(2, 1, 1, 1, 80, 90, 'B', 90, NULL, '2018-10-22 00:45:24', '2018-10-22 00:45:24');

-- --------------------------------------------------------

--
-- Table structure for table `grade_schemes`
--

CREATE TABLE `grade_schemes` (
  `grade_scheme_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `scheme_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Percent,1=Marks',
  `scheme_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grade_schemes`
--

INSERT INTO `grade_schemes` (`grade_scheme_id`, `admin_id`, `update_by`, `scheme_name`, `grade_type`, `scheme_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Scheme 1', 0, 1, '2018-10-22 00:45:24', '2018-10-22 00:45:24');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `group_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `holiday_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `holiday_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `holiday_start_date` date DEFAULT NULL,
  `holiday_end_date` date DEFAULT NULL,
  `holiday_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `homework_groups`
--

CREATE TABLE `homework_groups` (
  `homework_group_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `homework_group_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homework_groups`
--

INSERT INTO `homework_groups` (`homework_group_id`, `admin_id`, `update_by`, `class_id`, `section_id`, `homework_group_name`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 'Class I - A Group', '2018-10-22 00:12:34', '2018-10-22 00:12:34'),
(2, 1, 1, 1, 2, 'Class I - B Group', '2018-10-22 00:12:48', '2018-10-22 00:12:48');

-- --------------------------------------------------------

--
-- Table structure for table `imprest_ac`
--

CREATE TABLE `imprest_ac` (
  `imprest_ac_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `imprest_ac_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imprest_ac_amt` double DEFAULT NULL,
  `imprest_ac_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `imprest_ac`
--

INSERT INTO `imprest_ac` (`imprest_ac_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `student_id`, `imprest_ac_name`, `imprest_ac_amt`, `imprest_ac_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 'Shree', 0, 1, '2018-10-23 04:29:28', '2018-10-23 04:29:28');

-- --------------------------------------------------------

--
-- Table structure for table `imprest_ac_confi`
--

CREATE TABLE `imprest_ac_confi` (
  `imprest_ac_confi_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `imprest_ac_confi_amt` double UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `imprest_ac_confi`
--

INSERT INTO `imprest_ac_confi` (`imprest_ac_confi_id`, `admin_id`, `update_by`, `imprest_ac_confi_amt`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, '2018-10-21 23:33:59', '2018-10-21 23:33:59');

-- --------------------------------------------------------

--
-- Table structure for table `issued_books`
--

CREATE TABLE `issued_books` (
  `issued_book_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `book_id` int(10) UNSIGNED DEFAULT NULL,
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `member_type` int(10) UNSIGNED DEFAULT NULL,
  `book_copies_id` int(10) UNSIGNED DEFAULT NULL,
  `issued_from_date` date DEFAULT NULL,
  `issued_to_date` date DEFAULT NULL,
  `issued_book_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=issued,2=returned',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `library_members`
--

CREATE TABLE `library_members` (
  `library_member_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `library_member_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=student,2=staff',
  `member_member_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marks_criteria`
--

CREATE TABLE `marks_criteria` (
  `marks_criteria_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `criteria_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_marks` double UNSIGNED DEFAULT NULL,
  `passing_marks` double UNSIGNED DEFAULT NULL,
  `criteria_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marks_criteria`
--

INSERT INTO `marks_criteria` (`marks_criteria_id`, `admin_id`, `update_by`, `criteria_name`, `max_marks`, `passing_marks`, `criteria_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Creteria 1', 100, 33, 1, '2018-10-22 00:46:24', '2018-10-22 00:46:24'),
(2, 1, 1, 'Criteria 2', 100, 50, 1, '2018-10-22 00:47:37', '2018-10-22 00:47:37');

-- --------------------------------------------------------

--
-- Table structure for table `medium_type`
--

CREATE TABLE `medium_type` (
  `medium_type_id` int(10) UNSIGNED NOT NULL,
  `medium_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `medium_type_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_09_04_081951_create_admins_table', 1),
(2, '2018_09_04_082724_create_country_table', 1),
(3, '2018_09_04_082725_create_medium_type_table', 1),
(4, '2018_09_04_085450_create_state_table', 1),
(5, '2018_09_04_085724_create_city_table', 1),
(6, '2018_09_04_090000_create_school_boards_table', 1),
(7, '2018_09_04_090037_create_school_table', 1),
(8, '2018_09_04_090038_create_imprest_ac_confi_table', 1),
(9, '2018_09_04_091709_create_sessions_table', 1),
(10, '2018_09_04_092243_create_holidays_table', 1),
(11, '2018_09_04_092628_create_shifts_table', 1),
(12, '2018_09_04_092824_create_facilities_table', 1),
(13, '2018_09_04_093153_create_document_category_table', 1),
(14, '2018_09_04_093346_create_designation_table', 1),
(15, '2018_09_04_093413_create_titles_table', 1),
(16, '2018_09_04_093453_create_caste_table', 1),
(17, '2018_09_04_093537_create_religions_table', 1),
(18, '2018_09_04_093957_create_nationality_table', 1),
(19, '2018_09_04_094116_create_groups_table', 1),
(20, '2018_09_04_094117_create_streams_table', 1),
(21, '2018_09_04_094221_create_classes_table', 1),
(22, '2018_09_04_094401_create_sections_table', 1),
(23, '2018_09_04_094530_create_co_scholastic_types_table', 1),
(24, '2018_09_04_100509_create_subjects_table', 1),
(25, '2018_09_04_100807_create_virtual_classes_table', 1),
(26, '2018_09_04_100943_create_competitions_table', 1),
(27, '2018_09_04_101149_create_room_no_table', 1),
(28, '2018_09_04_101335_create_student_parents_table', 1),
(29, '2018_09_06_065837_create_term_exams_table', 1),
(30, '2018_09_06_065937_create_exams_table', 1),
(31, '2018_09_06_083000_create_student_table', 1),
(32, '2018_09_06_100023_create_student_academic_info_table', 1),
(33, '2018_09_06_100515_create_student_academic_history_info_table', 1),
(34, '2018_09_06_101111_create_student_health_info_table', 1),
(35, '2018_09_06_101112_create_imprest_ac_table', 1),
(36, '2018_09_06_101113_create_wallet_ac_table', 1),
(37, '2018_09_06_101240_create_virtual_class_map_table', 1),
(38, '2018_09_06_101310_create_competition_map_table', 1),
(39, '2018_09_06_104053_create_online_notes_table', 1),
(40, '2018_09_06_104234_create_staff_roles_table', 1),
(41, '2018_09_06_104354_create_staff_table', 1),
(42, '2018_09_06_132553_create_time_tables_table', 1),
(43, '2018_09_07_043215_create_question_papers_table', 1),
(44, '2018_09_07_043425_create_staff_documents_table', 1),
(45, '2018_09_07_044319_create_student_documents_table', 1),
(46, '2018_09_08_082046_create_staff_class_allocations_table', 1),
(47, '2018_09_08_082829_create_subject_class_map_table', 1),
(48, '2018_09_08_083054_create_class_subject_staff_map_table', 1),
(49, '2018_09_08_083343_create_staff_leaves_table', 1),
(50, '2018_09_08_091546_create_student_leaves_table', 1),
(51, '2018_09_10_061556_create_brochures_table', 1),
(52, '2018_09_30_094828_create_class_subject_staff_maping_table', 1),
(53, '2018_10_01_043537_create_one_time_heads_table', 1),
(54, '2018_10_01_044251_create_recurring_heads_table', 1),
(55, '2018_10_01_044530_create_recurring_inst_table', 1),
(56, '2018_10_02_005202_create_book_category_table', 1),
(57, '2018_10_02_005800_create_book_allowance_table', 1),
(58, '2018_10_02_005938_create_book_vendor_table', 1),
(59, '2018_10_02_010120_create_book_cupboard_table', 1),
(60, '2018_10_02_010258_create_book_cupboardshelf_table', 1),
(61, '2018_10_02_044518_create_banks_table', 1),
(62, '2018_10_02_044836_create_student_cheques_table', 1),
(63, '2018_10_02_045309_create_rte_cheques_table', 1),
(64, '2018_10_02_165720_create_books_table', 1),
(65, '2018_10_02_170222_create_book_copies_info_table', 1),
(66, '2018_10_02_170530_create_library_members_table', 1),
(67, '2018_10_02_170949_create_issued_books_table', 1),
(68, '2018_10_03_045935_create_rte_heads_table', 1),
(69, '2018_10_03_140228_create_visitors_table', 1),
(70, '2018_10_06_075741_create_grade_schemes_table', 1),
(71, '2018_10_06_080011_create_grades_table', 1),
(72, '2018_10_08_072229_create_fine_table', 1),
(73, '2018_10_08_072737_create_fine_details_table', 1),
(74, '2018_10_09_055058_create_rte_head_map_table', 1),
(75, '2018_10_09_125120_create_marks_criteria_table', 1),
(76, '2018_10_09_141430_create_subject_section_map_table', 1),
(77, '2018_10_09_141750_create_subject_student_map_table', 1),
(78, '2018_10_12_091353_create_reasons_table', 1),
(79, '2018_10_12_091354_create_concession_map_table', 1),
(80, '2018_10_13_054337_create_fees_st_map_table', 1),
(81, '2018_10_18_073910_create_fee_receipt_table', 1),
(82, '2018_10_18_074452_create_fee_receipt_details_table', 1),
(83, '2018_10_21_160700_create_homework_groups_table', 1),
(84, '2018_10_22_063731_create_exam_map_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `nationality`
--

CREATE TABLE `nationality` (
  `nationality_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `nationality_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nationality`
--

INSERT INTO `nationality` (`nationality_id`, `admin_id`, `update_by`, `nationality_name`, `nationality_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Indian', 1, '2018-10-22 00:21:31', '2018-10-22 00:21:31');

-- --------------------------------------------------------

--
-- Table structure for table `one_time_heads`
--

CREATE TABLE `one_time_heads` (
  `ot_head_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `ot_particular_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ot_alias` text COLLATE utf8mb4_unicode_ci,
  `ot_classes` text COLLATE utf8mb4_unicode_ci COMMENT 'Classes ids are comma seperated store here',
  `ot_amount` double(18,2) UNSIGNED DEFAULT NULL,
  `ot_date` date DEFAULT NULL,
  `ot_refundable` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `for_students` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=New,1=Old,2=Both',
  `deduct_imprest_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `receipt_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `ot_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `one_time_heads`
--

INSERT INTO `one_time_heads` (`ot_head_id`, `admin_id`, `update_by`, `ot_particular_name`, `ot_alias`, `ot_classes`, `ot_amount`, `ot_date`, `ot_refundable`, `for_students`, `deduct_imprest_status`, `receipt_status`, `ot_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Admission', 'Admission', '1,2', 1000.00, '2018-10-31', 0, 0, 0, 0, 1, '2018-10-22 02:06:20', '2018-10-22 02:06:20');

-- --------------------------------------------------------

--
-- Table structure for table `online_notes`
--

CREATE TABLE `online_notes` (
  `online_note_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `online_note_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `online_note_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `online_note_topic` text COLLATE utf8mb4_unicode_ci,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `online_note_url` text COLLATE utf8mb4_unicode_ci,
  `online_note_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `question_papers`
--

CREATE TABLE `question_papers` (
  `question_paper_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `question_paper_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `exam_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `question_paper_file` text COLLATE utf8mb4_unicode_ci,
  `question_paper_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reasons`
--

CREATE TABLE `reasons` (
  `reason_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `reason_text` text COLLATE utf8mb4_unicode_ci,
  `reason_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reasons`
--

INSERT INTO `reasons` (`reason_id`, `admin_id`, `update_by`, `reason_text`, `reason_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Free by management', 1, '2018-10-23 04:31:00', '2018-10-23 04:31:00');

-- --------------------------------------------------------

--
-- Table structure for table `recurring_heads`
--

CREATE TABLE `recurring_heads` (
  `rc_head_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `rc_particular_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rc_alias` text COLLATE utf8mb4_unicode_ci,
  `rc_classes` text COLLATE utf8mb4_unicode_ci COMMENT 'Classes ids are comma seperated store here',
  `rc_amount` double(18,2) UNSIGNED DEFAULT NULL,
  `rc_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recurring_inst`
--

CREATE TABLE `recurring_inst` (
  `rc_inst_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `rc_head_id` int(10) UNSIGNED DEFAULT NULL,
  `rc_inst_particular_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rc_inst_amount` double(18,2) UNSIGNED DEFAULT NULL,
  `rc_inst_date` date DEFAULT NULL,
  `rc_inst_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `religions`
--

CREATE TABLE `religions` (
  `religion_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `religion_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `religions`
--

INSERT INTO `religions` (`religion_id`, `admin_id`, `update_by`, `religion_name`, `religion_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Hindu', 1, '2018-10-22 00:20:54', '2018-10-22 00:20:54'),
(2, 1, 1, 'Shikh', 1, '2018-10-22 00:21:02', '2018-10-22 00:21:12');

-- --------------------------------------------------------

--
-- Table structure for table `room_no`
--

CREATE TABLE `room_no` (
  `room_no_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `room_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_no_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rte_cheques`
--

CREATE TABLE `rte_cheques` (
  `rte_cheque_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `rte_cheque_no` text COLLATE utf8mb4_unicode_ci,
  `rte_cheque_date` date DEFAULT NULL,
  `rte_cheque_amt` text COLLATE utf8mb4_unicode_ci,
  `rte_cheque_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Pending,1=Cancel,2=Cleared,3=Bounce',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rte_heads`
--

CREATE TABLE `rte_heads` (
  `rte_head_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `rte_head` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rte_amount` double(18,2) UNSIGNED DEFAULT NULL,
  `rte_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rte_head_map`
--

CREATE TABLE `rte_head_map` (
  `rte_head_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `rte_head_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE `school` (
  `school_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `school_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school_registration_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_sno_numbers` text COLLATE utf8mb4_unicode_ci COMMENT 'SNO numbers are stored with comma',
  `school_description` text COLLATE utf8mb4_unicode_ci,
  `school_address` text COLLATE utf8mb4_unicode_ci,
  `school_district` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `state_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `school_pincode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_medium` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Hindi,1=English,2=Both',
  `school_board_of_exams` text COLLATE utf8mb4_unicode_ci COMMENT 'Board names are stored with comma',
  `school_class_from` int(10) UNSIGNED DEFAULT NULL,
  `school_class_to` int(10) UNSIGNED DEFAULT NULL,
  `school_total_students` int(10) UNSIGNED DEFAULT NULL,
  `school_total_staff` int(10) UNSIGNED DEFAULT NULL,
  `school_fee_range_from` double(18,2) UNSIGNED DEFAULT NULL,
  `school_fee_range_to` double(18,2) UNSIGNED DEFAULT NULL,
  `school_facilities` text COLLATE utf8mb4_unicode_ci COMMENT 'Facilities are stored with comma',
  `school_url` text COLLATE utf8mb4_unicode_ci,
  `school_logo` text COLLATE utf8mb4_unicode_ci,
  `school_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_mobile_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_telephone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_fax_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_img1` text COLLATE utf8mb4_unicode_ci,
  `school_img2` text COLLATE utf8mb4_unicode_ci,
  `school_img3` text COLLATE utf8mb4_unicode_ci,
  `school_img4` text COLLATE utf8mb4_unicode_ci,
  `school_img5` text COLLATE utf8mb4_unicode_ci,
  `school_img6` text COLLATE utf8mb4_unicode_ci,
  `imprest_ac_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `school_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`school_id`, `admin_id`, `update_by`, `school_name`, `school_registration_no`, `school_sno_numbers`, `school_description`, `school_address`, `school_district`, `city_id`, `state_id`, `country_id`, `school_pincode`, `school_medium`, `school_board_of_exams`, `school_class_from`, `school_class_to`, `school_total_students`, `school_total_staff`, `school_fee_range_from`, `school_fee_range_to`, `school_facilities`, `school_url`, `school_logo`, `school_email`, `school_mobile_number`, `school_telephone`, `school_fax_number`, `school_img1`, `school_img2`, `school_img3`, `school_img4`, `school_img5`, `school_img6`, `imprest_ac_status`, `school_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2018-10-21 23:33:56', '2018-10-21 23:33:56');

-- --------------------------------------------------------

--
-- Table structure for table `school_boards`
--

CREATE TABLE `school_boards` (
  `board_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `board_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `board_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `school_boards`
--

INSERT INTO `school_boards` (`board_id`, `admin_id`, `update_by`, `board_name`, `board_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'CBSE Board', 1, '2018-10-21 23:33:50', '2018-10-21 23:33:50'),
(2, 1, 1, 'RBSE Board', 1, '2018-10-21 23:33:50', '2018-10-21 23:33:50'),
(3, 1, 1, 'IB Board', 1, '2018-10-21 23:33:50', '2018-10-21 23:33:50'),
(4, 1, 1, 'IGCSE Board', 1, '2018-10-21 23:33:50', '2018-10-21 23:33:50'),
(5, 1, 1, 'State Board', 1, '2018-10-21 23:33:50', '2018-10-21 23:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `section_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `shift_id` int(10) UNSIGNED DEFAULT NULL,
  `stream_id` int(10) UNSIGNED DEFAULT NULL,
  `section_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section_intake` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'capacity/seats',
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `section_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `section_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`section_id`, `admin_id`, `update_by`, `class_id`, `shift_id`, `stream_id`, `section_name`, `section_intake`, `medium_type`, `section_order`, `section_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, NULL, 'A', 5, 1, 0, 1, '2018-10-22 00:12:34', '2018-10-22 23:37:48'),
(2, 1, 1, 1, 1, NULL, 'B', 5, 1, 0, 1, '2018-10-22 00:12:48', '2018-10-22 00:12:48');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_start_date` date DEFAULT NULL,
  `session_end_date` date DEFAULT NULL,
  `session_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `admin_id`, `update_by`, `session_name`, `session_start_date`, `session_end_date`, `session_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-2019', '2018-06-01', '2019-05-31', 1, '2018-10-22 00:13:57', '2018-10-22 00:13:57');

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `shift_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `shift_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shift_start_time` time DEFAULT NULL,
  `shift_end_time` time DEFAULT NULL,
  `shift_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shifts`
--

INSERT INTO `shifts` (`shift_id`, `admin_id`, `update_by`, `shift_name`, `shift_start_time`, `shift_end_time`, `shift_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '1st shift', '07:00:00', '00:00:00', 1, '2018-10-22 00:11:24', '2018-10-22 00:11:24'),
(2, 1, 1, '2nd shift', '00:00:00', '05:00:00', 1, '2018-10-22 00:12:19', '2018-10-22 00:12:19');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `reference_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_profile_img` text COLLATE utf8mb4_unicode_ci,
  `designation_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_role_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_dob` date DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `staff_gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Male,1=Female',
  `staff_father_name_husband_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_father_husband_mobile_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_mother_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_blood_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_marital_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Unmarried,1=Married',
  `title_id` int(10) UNSIGNED DEFAULT NULL,
  `caste_id` int(10) UNSIGNED DEFAULT NULL,
  `nationality_id` int(10) UNSIGNED DEFAULT NULL,
  `religion_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_attendance_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_qualification` text COLLATE utf8mb4_unicode_ci COMMENT 'Qualifications are store with comma',
  `staff_specialization` text COLLATE utf8mb4_unicode_ci COMMENT 'Specializations are store with comma',
  `staff_reference` text COLLATE utf8mb4_unicode_ci,
  `staff_experience` text COLLATE utf8mb4_unicode_ci,
  `staff_temporary_address` text COLLATE utf8mb4_unicode_ci,
  `staff_temporary_city` int(10) UNSIGNED DEFAULT NULL,
  `staff_temporary_state` int(10) UNSIGNED DEFAULT NULL,
  `staff_temporary_county` int(10) UNSIGNED DEFAULT NULL,
  `staff_temporary_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_permanent_address` text COLLATE utf8mb4_unicode_ci,
  `staff_permanent_city` int(10) UNSIGNED DEFAULT NULL,
  `staff_permanent_state` int(10) UNSIGNED DEFAULT NULL,
  `staff_permanent_county` int(10) UNSIGNED DEFAULT NULL,
  `staff_permanent_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Leaved,1=Studying',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `admin_id`, `update_by`, `reference_admin_id`, `staff_name`, `staff_profile_img`, `designation_id`, `staff_role_id`, `staff_email`, `staff_mobile_number`, `staff_dob`, `medium_type`, `staff_gender`, `staff_father_name_husband_name`, `staff_father_husband_mobile_no`, `staff_mother_name`, `staff_blood_group`, `staff_marital_status`, `title_id`, `caste_id`, `nationality_id`, `religion_id`, `staff_attendance_unique_id`, `staff_qualification`, `staff_specialization`, `staff_reference`, `staff_experience`, `staff_temporary_address`, `staff_temporary_city`, `staff_temporary_state`, `staff_temporary_county`, `staff_temporary_pincode`, `staff_permanent_address`, `staff_permanent_city`, `staff_permanent_state`, `staff_permanent_county`, `staff_permanent_pincode`, `staff_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3, 'Ravi', NULL, 1, 2, 'ravi@gmail.com', '8890149916', '2008-10-01', 1, 0, 'Ramesh', '8890149916', 'Sarla', NULL, 0, 1, 1, 1, 1, 'Staff_1', 'Specialization', 'Specialization', 'Reference', '[\"Experience 1\"]ravi', NULL, NULL, NULL, NULL, NULL, 'Address', 1, 1, 1, '342006', 1, '2018-10-22 00:27:08', '2018-10-22 00:27:08'),
(2, 1, 1, 6, 'subject_teacher', NULL, 1, 3, 'subject_teacher@gmail.com', '8890149916', '2008-10-01', 1, 0, 'Ramesh', '8890149916', 'Sarla', NULL, 0, 1, 1, 1, 1, 'Staff_1', 'Specialization', 'Specialization', 'Reference', '[\"Experience 1\"]', NULL, NULL, NULL, NULL, NULL, 'Address', 1, 1, 1, '342006', 1, '2018-10-22 00:27:08', '2018-10-22 00:27:08'),
(3, 1, 1, 7, 'exam_manager', NULL, 1, 5, 'exam_manager@gmail.com', '8890149916', '2008-10-01', 1, 0, 'Ramesh', '8890149916', 'Sarla', NULL, 0, 1, 1, 1, 1, 'Staff_1', 'Specialization', 'Specialization', 'Reference', '[\"Experience 1\"]', NULL, NULL, NULL, NULL, NULL, 'Address', 1, 1, 1, '342006', 1, '2018-10-22 00:27:08', '2018-10-22 00:27:08'),
(4, 1, 1, 8, 'hostel', NULL, 1, 7, 'hostel@gmail.com', '8890149916', '2008-10-01', 1, 0, 'Ramesh', '8890149916', 'Sarla', NULL, 0, 1, 1, 1, 1, 'Staff_1', 'Specialization', 'Specialization', 'Reference', '[\"Experience 1\"]', NULL, NULL, NULL, NULL, NULL, 'Address', 1, 1, 1, '342006', 1, '2018-10-22 00:27:08', '2018-10-22 00:27:08'),
(5, 1, 1, 9, 'library', NULL, 1, 12, 'library@gmail.com', '8890149916', '2008-10-01', 1, 0, 'Ramesh', '8890149916', 'Sarla', NULL, 0, 1, 1, 1, 1, 'Staff_1', 'Specialization', 'Specialization', 'Reference', '[\"Experience 1\"]', NULL, NULL, NULL, NULL, NULL, 'Address', 1, 1, 1, '342006', 1, '2018-10-22 00:27:08', '2018-10-22 00:27:08'),
(6, 1, 1, 10, 'finance_head', NULL, 1, 9, 'finance_head@gmail.com', '8890149916', '2008-10-01', 1, 0, 'Ramesh', '8890149916', 'Sarla', NULL, 0, 1, 1, 1, 1, 'Staff_1', 'Specialization', 'Specialization', 'Reference', '[\"Experience 1\"]', NULL, NULL, NULL, NULL, NULL, 'Address', 1, 1, 1, '342006', 1, '2018-10-22 00:27:08', '2018-10-22 00:27:08'),
(7, 1, 1, 11, 'guard', NULL, 1, 11, 'guard@gmail.com', '8890149916', '2008-10-01', 1, 0, 'Ramesh', '8890149916', 'Sarla', NULL, 0, 1, 1, 1, 1, 'Staff_1', 'Specialization', 'Specialization', 'Reference', '[\"Experience 1\"]', NULL, NULL, NULL, NULL, NULL, 'Address', 1, 1, 1, '342006', 1, '2018-10-22 00:27:08', '2018-10-22 00:27:08');

-- --------------------------------------------------------

--
-- Table structure for table `staff_class_allocations`
--

CREATE TABLE `staff_class_allocations` (
  `staff_class_allocation_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_class_allocations`
--

INSERT INTO `staff_class_allocations` (`staff_class_allocation_id`, `admin_id`, `update_by`, `class_id`, `section_id`, `staff_id`, `medium_type`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2018-10-22 00:30:32', '2018-10-22 00:30:32');

-- --------------------------------------------------------

--
-- Table structure for table `staff_documents`
--

CREATE TABLE `staff_documents` (
  `staff_document_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `document_category_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_document_details` text COLLATE utf8mb4_unicode_ci,
  `staff_document_file` text COLLATE utf8mb4_unicode_ci,
  `staff_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff_leaves`
--

CREATE TABLE `staff_leaves` (
  `staff_leave_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `staff_leave_reason` text COLLATE utf8mb4_unicode_ci,
  `staff_leave_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_leave_from_date` date DEFAULT NULL,
  `staff_leave_to_date` date DEFAULT NULL,
  `staff_leave_attachment` text COLLATE utf8mb4_unicode_ci,
  `staff_leave_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff_roles`
--

CREATE TABLE `staff_roles` (
  `staff_role_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `staff_role_name` text COLLATE utf8mb4_unicode_ci,
  `staff_role_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_roles`
--

INSERT INTO `staff_roles` (`staff_role_id`, `admin_id`, `update_by`, `staff_role_name`, `staff_role_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'School Principal', 1, '2018-10-21 23:37:10', '2018-10-21 23:37:10'),
(2, 1, 1, 'Class Teacher', 1, '2018-10-21 23:37:10', '2018-10-21 23:37:10'),
(3, 1, 1, 'Subject Teacher', 1, '2018-10-21 23:37:10', '2018-10-21 23:37:10'),
(4, 1, 1, 'Marks Manager', 1, '2018-10-21 23:37:10', '2018-10-21 23:37:10'),
(5, 1, 1, 'Exam Manager', 1, '2018-10-21 23:37:10', '2018-10-21 23:37:10'),
(6, 1, 1, 'Accountant', 1, '2018-10-21 23:37:10', '2018-10-21 23:37:10'),
(7, 1, 1, 'Hostel Warden', 1, '2018-10-21 23:37:10', '2018-10-21 23:37:10'),
(8, 1, 1, 'HR Manager', 1, '2018-10-21 23:37:10', '2018-10-21 23:37:10'),
(9, 1, 1, 'Finance Head', 1, '2018-10-21 23:37:10', '2018-10-21 23:37:10'),
(10, 1, 1, 'Admission Officer / Public Relation Officer', 1, '2018-10-21 23:37:10', '2018-10-21 23:37:10'),
(11, 1, 1, 'Guard for Visitor Management', 1, '2018-10-21 23:37:10', '2018-10-21 23:37:10'),
(12, 1, 1, 'Librarian', 1, '2018-10-21 23:37:10', '2018-10-21 23:37:10'),
(13, 1, 1, 'Website Manager', 1, '2018-10-21 23:37:10', '2018-10-21 23:37:10');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `state_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `country_id`, `state_name`, `state_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Rajasthan', 1, '2018-10-22 00:23:40', '2018-10-22 00:23:40');

-- --------------------------------------------------------

--
-- Table structure for table `streams`
--

CREATE TABLE `streams` (
  `stream_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `stream_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stream_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `student_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `reference_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `student_parent_id` int(10) UNSIGNED DEFAULT NULL,
  `student_enroll_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_roll_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_image` text COLLATE utf8mb4_unicode_ci,
  `student_reg_date` date DEFAULT NULL,
  `student_dob` date DEFAULT NULL,
  `student_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Boy,1=Girl',
  `student_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=PaidBoy,1=RTE,2=FREE',
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `student_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caste_id` int(10) UNSIGNED DEFAULT NULL,
  `title_id` int(10) UNSIGNED DEFAULT NULL,
  `religion_id` int(10) UNSIGNED DEFAULT NULL,
  `nationality_id` int(10) UNSIGNED DEFAULT NULL,
  `student_sibling_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_sibling_class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_adhar_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_tc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_privious_tc_date` date DEFAULT NULL,
  `student_privious_result` text COLLATE utf8mb4_unicode_ci,
  `student_temporary_address` text COLLATE utf8mb4_unicode_ci,
  `student_temporary_city` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_state` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_county` int(10) UNSIGNED DEFAULT NULL,
  `student_temporary_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_permanent_address` text COLLATE utf8mb4_unicode_ci,
  `student_permanent_city` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_state` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_county` int(10) UNSIGNED DEFAULT NULL,
  `student_permanent_pincode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rte_apply_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `student_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Leaved,1=Studying',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`student_id`, `admin_id`, `update_by`, `reference_admin_id`, `student_parent_id`, `student_enroll_number`, `student_roll_no`, `student_name`, `student_image`, `student_reg_date`, `student_dob`, `student_email`, `student_gender`, `student_type`, `medium_type`, `student_category`, `caste_id`, `title_id`, `religion_id`, `nationality_id`, `student_sibling_name`, `student_sibling_class_id`, `student_adhar_card_number`, `student_privious_school`, `student_privious_class`, `student_privious_tc_no`, `student_privious_tc_date`, `student_privious_result`, `student_temporary_address`, `student_temporary_city`, `student_temporary_state`, `student_temporary_county`, `student_temporary_pincode`, `student_permanent_address`, `student_permanent_city`, `student_permanent_state`, `student_permanent_county`, `student_permanent_pincode`, `rte_apply_status`, `student_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 5, 1, 'N0001', NULL, 'Shree', NULL, '2018-10-01', '2017-10-01', 'bhagyashree@gmail.com', 1, 0, 1, 'General', 1, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Address', 1, 1, 1, '342006', 0, 1, '2018-10-23 04:29:27', '2018-10-23 04:29:27');

-- --------------------------------------------------------

--
-- Table structure for table `student_academic_history_info`
--

CREATE TABLE `student_academic_history_info` (
  `student_academic_history_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `student_academic_info_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `stream_id` int(10) UNSIGNED DEFAULT NULL,
  `percentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rank` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `activity_winner` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_academic_info`
--

CREATE TABLE `student_academic_info` (
  `student_academic_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_session_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_class_id` int(10) UNSIGNED DEFAULT NULL,
  `admission_section_id` int(10) UNSIGNED DEFAULT NULL,
  `current_session_id` int(10) UNSIGNED DEFAULT NULL,
  `current_class_id` int(10) UNSIGNED DEFAULT NULL,
  `current_section_id` int(10) UNSIGNED DEFAULT NULL,
  `student_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `stream_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_academic_info`
--

INSERT INTO `student_academic_info` (`student_academic_info_id`, `admin_id`, `update_by`, `student_id`, `admission_session_id`, `admission_class_id`, `admission_section_id`, `current_session_id`, `current_class_id`, `current_section_id`, `student_unique_id`, `group_id`, `stream_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'N0001_1', NULL, NULL, '2018-10-23 04:29:27', '2018-10-23 04:29:27');

-- --------------------------------------------------------

--
-- Table structure for table `student_cheques`
--

CREATE TABLE `student_cheques` (
  `student_cheque_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `student_cheque_no` text COLLATE utf8mb4_unicode_ci,
  `student_cheque_date` date DEFAULT NULL,
  `student_cheque_amt` text COLLATE utf8mb4_unicode_ci,
  `student_cheque_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Pending,1=Cancel,2=Cleared,3=Bounce',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_documents`
--

CREATE TABLE `student_documents` (
  `student_document_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `document_category_id` int(10) UNSIGNED DEFAULT NULL,
  `student_document_details` text COLLATE utf8mb4_unicode_ci,
  `student_document_file` text COLLATE utf8mb4_unicode_ci,
  `student_document_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_health_info`
--

CREATE TABLE `student_health_info` (
  `student_health_info_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `student_height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_blood_group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_vision_left` text COLLATE utf8mb4_unicode_ci,
  `student_vision_right` text COLLATE utf8mb4_unicode_ci,
  `medical_issues` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_health_info`
--

INSERT INTO `student_health_info` (`student_health_info_id`, `admin_id`, `update_by`, `student_id`, `student_height`, `student_weight`, `student_blood_group`, `student_vision_left`, `student_vision_right`, `medical_issues`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, NULL, 'A', NULL, NULL, 'No', '2018-10-23 04:29:28', '2018-10-23 04:29:28');

-- --------------------------------------------------------

--
-- Table structure for table `student_leaves`
--

CREATE TABLE `student_leaves` (
  `student_leave_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `student_leave_reason` text COLLATE utf8mb4_unicode_ci,
  `student_leave_from_date` date DEFAULT NULL,
  `student_leave_to_date` date DEFAULT NULL,
  `student_leave_attachment` text COLLATE utf8mb4_unicode_ci,
  `student_leave_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_parents`
--

CREATE TABLE `student_parents` (
  `student_parent_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `reference_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `student_login_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_login_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_login_contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_father_annual_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_annual_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_mother_tongue` text COLLATE utf8mb4_unicode_ci,
  `student_guardian_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_guardian_relation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_parent_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_parents`
--

INSERT INTO `student_parents` (`student_parent_id`, `admin_id`, `update_by`, `reference_admin_id`, `student_login_name`, `student_login_email`, `student_login_contact_no`, `student_father_name`, `student_father_mobile_number`, `student_father_email`, `student_father_occupation`, `student_father_annual_salary`, `student_mother_name`, `student_mother_mobile_number`, `student_mother_email`, `student_mother_occupation`, `student_mother_annual_salary`, `student_mother_tongue`, `student_guardian_name`, `student_guardian_email`, `student_guardian_mobile_number`, `student_guardian_relation`, `student_parent_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 4, 'Shree', 'Bhagyashree@gmail.com', '8890149916', 'Ramesh', '8890149916', 'ramesh@gmail.com', 'Govt', NULL, 'Sarla', '8890149916', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-10-23 04:29:27', '2018-10-23 04:29:27');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subject_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `subject_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_is_coscholastic` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `class_ids` text COLLATE utf8mb4_unicode_ci,
  `co_scholastic_type_id` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `subject_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subject_id`, `admin_id`, `update_by`, `subject_name`, `subject_code`, `subject_is_coscholastic`, `class_ids`, `co_scholastic_type_id`, `medium_type`, `subject_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Subject 1', 'sub1', 0, '', NULL, 1, 1, '2018-10-22 00:31:22', '2018-10-22 00:31:22'),
(2, 1, 1, 'Subject 2', 'sub2', 0, '', NULL, 1, 1, '2018-10-22 00:31:34', '2018-10-22 00:31:34'),
(3, 1, 1, 'Subject 3', 'sub3', 0, '', NULL, 1, 1, '2018-10-22 00:31:47', '2018-10-22 00:31:47'),
(4, 1, 1, 'Subject 4', 'sub4', 0, '', NULL, 1, 1, '2018-10-22 00:32:38', '2018-10-22 00:32:38'),
(5, 1, 1, 'Subject 5', 'sub5', 0, '', NULL, 1, 1, '2018-10-22 00:32:57', '2018-10-22 00:32:57');

-- --------------------------------------------------------

--
-- Table structure for table `subject_class_map`
--

CREATE TABLE `subject_class_map` (
  `subject_class_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subject_class_map`
--

INSERT INTO `subject_class_map` (`subject_class_map_id`, `admin_id`, `update_by`, `class_id`, `section_id`, `subject_id`, `medium_type`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, 1, 1, '2018-10-22 00:33:19', '2018-10-22 00:33:19'),
(2, 1, 1, 1, NULL, 2, 1, '2018-10-22 00:33:19', '2018-10-22 00:33:19'),
(3, 1, 1, 1, NULL, 3, 1, '2018-10-22 00:33:19', '2018-10-22 00:33:19'),
(4, 1, 1, 1, NULL, 4, 1, '2018-10-22 00:33:19', '2018-10-22 00:33:19'),
(5, 1, 1, 1, NULL, 5, 1, '2018-10-22 00:33:19', '2018-10-22 00:33:19'),
(6, 1, 1, 2, NULL, 1, 1, '2018-10-22 04:26:45', '2018-10-22 04:26:45'),
(7, 1, 1, 2, NULL, 2, 1, '2018-10-22 04:26:45', '2018-10-22 04:26:45'),
(8, 1, 1, 2, NULL, 3, 1, '2018-10-22 04:26:45', '2018-10-22 04:26:45'),
(9, 1, 1, 2, NULL, 4, 1, '2018-10-22 04:26:45', '2018-10-22 04:26:45');

-- --------------------------------------------------------

--
-- Table structure for table `subject_section_map`
--

CREATE TABLE `subject_section_map` (
  `subject_section_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subject_student_map`
--

CREATE TABLE `subject_student_map` (
  `subject_student_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `term_exams`
--

CREATE TABLE `term_exams` (
  `term_exam_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `term_exam_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `term_exam_caption` text COLLATE utf8mb4_unicode_ci,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `term_exam_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `term_exams`
--

INSERT INTO `term_exams` (`term_exam_id`, `admin_id`, `update_by`, `term_exam_name`, `term_exam_caption`, `medium_type`, `term_exam_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Term1', NULL, 1, 1, '2018-10-22 00:43:29', '2018-10-22 00:43:29'),
(2, 1, 1, 'Term 2', NULL, 1, 1, '2018-10-22 00:43:41', '2018-10-22 00:43:41');

-- --------------------------------------------------------

--
-- Table structure for table `time_tables`
--

CREATE TABLE `time_tables` (
  `time_table_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `time_table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medium_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Hindi,1=English',
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `time_table_week_days` text COLLATE utf8mb4_unicode_ci,
  `time_table_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `title_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `title_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `titles`
--

INSERT INTO `titles` (`title_id`, `admin_id`, `update_by`, `title_name`, `title_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Mr', 1, '2018-10-22 00:19:45', '2018-10-22 00:19:45'),
(2, 1, 1, 'Ms', 1, '2018-10-22 00:19:56', '2018-10-22 00:19:56');

-- --------------------------------------------------------

--
-- Table structure for table `virtual_classes`
--

CREATE TABLE `virtual_classes` (
  `virtual_class_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `virtual_class_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `virtual_class_description` text COLLATE utf8mb4_unicode_ci,
  `virtual_class_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `virtual_class_map`
--

CREATE TABLE `virtual_class_map` (
  `virtual_class_map_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `virtual_class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `visitor_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purpose` text COLLATE utf8mb4_unicode_ci,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visit_date` date DEFAULT NULL,
  `check_in_time` time DEFAULT NULL,
  `check_out_time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wallet_ac`
--

CREATE TABLE `wallet_ac` (
  `wallet_ac_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `wallet_ac_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wallet_ac_amt` double DEFAULT NULL,
  `wallet_ac_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Deactive,1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wallet_ac`
--

INSERT INTO `wallet_ac` (`wallet_ac_id`, `admin_id`, `update_by`, `session_id`, `class_id`, `student_id`, `wallet_ac_name`, `wallet_ac_amt`, `wallet_ac_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 'Shree', 0, 1, '2018-10-23 04:29:28', '2018-10-23 04:29:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`),
  ADD KEY `books_admin_id_foreign` (`admin_id`),
  ADD KEY `books_update_by_foreign` (`update_by`),
  ADD KEY `books_book_category_id_foreign` (`book_category_id`),
  ADD KEY `books_book_vendor_id_foreign` (`book_vendor_id`),
  ADD KEY `books_book_cupboard_id_foreign` (`book_cupboard_id`),
  ADD KEY `books_book_cupboardshelf_id_foreign` (`book_cupboardshelf_id`);

--
-- Indexes for table `book_allowance`
--
ALTER TABLE `book_allowance`
  ADD PRIMARY KEY (`book_allowance_id`),
  ADD KEY `book_allowance_admin_id_foreign` (`admin_id`),
  ADD KEY `book_allowance_update_by_foreign` (`update_by`);

--
-- Indexes for table `book_categories`
--
ALTER TABLE `book_categories`
  ADD PRIMARY KEY (`book_category_id`),
  ADD KEY `book_categories_admin_id_foreign` (`admin_id`),
  ADD KEY `book_categories_update_by_foreign` (`update_by`);

--
-- Indexes for table `book_copies_info`
--
ALTER TABLE `book_copies_info`
  ADD PRIMARY KEY (`book_info_id`),
  ADD KEY `book_copies_info_admin_id_foreign` (`admin_id`),
  ADD KEY `book_copies_info_update_by_foreign` (`update_by`),
  ADD KEY `book_copies_info_book_id_foreign` (`book_id`);

--
-- Indexes for table `book_cupboards`
--
ALTER TABLE `book_cupboards`
  ADD PRIMARY KEY (`book_cupboard_id`),
  ADD KEY `book_cupboards_admin_id_foreign` (`admin_id`),
  ADD KEY `book_cupboards_update_by_foreign` (`update_by`);

--
-- Indexes for table `book_cupboardshelfs`
--
ALTER TABLE `book_cupboardshelfs`
  ADD PRIMARY KEY (`book_cupboardshelf_id`),
  ADD KEY `book_cupboardshelfs_admin_id_foreign` (`admin_id`),
  ADD KEY `book_cupboardshelfs_update_by_foreign` (`update_by`);

--
-- Indexes for table `book_vendors`
--
ALTER TABLE `book_vendors`
  ADD PRIMARY KEY (`book_vendor_id`),
  ADD KEY `book_vendors_admin_id_foreign` (`admin_id`),
  ADD KEY `book_vendors_update_by_foreign` (`update_by`);

--
-- Indexes for table `brochures`
--
ALTER TABLE `brochures`
  ADD PRIMARY KEY (`brochure_id`),
  ADD KEY `brochures_admin_id_foreign` (`admin_id`),
  ADD KEY `brochures_update_by_foreign` (`update_by`),
  ADD KEY `brochures_session_id_foreign` (`session_id`);

--
-- Indexes for table `caste`
--
ALTER TABLE `caste`
  ADD PRIMARY KEY (`caste_id`),
  ADD KEY `caste_admin_id_foreign` (`admin_id`),
  ADD KEY `caste_update_by_foreign` (`update_by`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`),
  ADD KEY `city_state_id_foreign` (`state_id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`class_id`),
  ADD KEY `classes_admin_id_foreign` (`admin_id`),
  ADD KEY `classes_update_by_foreign` (`update_by`),
  ADD KEY `classes_board_id_foreign` (`board_id`);

--
-- Indexes for table `class_subject_staff_map`
--
ALTER TABLE `class_subject_staff_map`
  ADD PRIMARY KEY (`class_subject_staff_map_id`),
  ADD KEY `class_subject_staff_map_admin_id_foreign` (`admin_id`),
  ADD KEY `class_subject_staff_map_update_by_foreign` (`update_by`),
  ADD KEY `class_subject_staff_map_class_id_foreign` (`class_id`),
  ADD KEY `class_subject_staff_map_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `competitions`
--
ALTER TABLE `competitions`
  ADD PRIMARY KEY (`competition_id`),
  ADD KEY `competitions_admin_id_foreign` (`admin_id`),
  ADD KEY `competitions_update_by_foreign` (`update_by`);

--
-- Indexes for table `competition_map`
--
ALTER TABLE `competition_map`
  ADD PRIMARY KEY (`student_competition_map_id`),
  ADD KEY `competition_map_admin_id_foreign` (`admin_id`),
  ADD KEY `competition_map_update_by_foreign` (`update_by`),
  ADD KEY `competition_map_competition_id_foreign` (`competition_id`),
  ADD KEY `competition_map_student_id_foreign` (`student_id`);

--
-- Indexes for table `concession_map`
--
ALTER TABLE `concession_map`
  ADD PRIMARY KEY (`concession_map_id`),
  ADD KEY `concession_map_admin_id_foreign` (`admin_id`),
  ADD KEY `concession_map_update_by_foreign` (`update_by`),
  ADD KEY `concession_map_class_id_foreign` (`class_id`),
  ADD KEY `concession_map_session_id_foreign` (`session_id`),
  ADD KEY `concession_map_student_id_foreign` (`student_id`),
  ADD KEY `concession_map_reason_id_foreign` (`reason_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `co_scholastic_types`
--
ALTER TABLE `co_scholastic_types`
  ADD PRIMARY KEY (`co_scholastic_type_id`),
  ADD KEY `co_scholastic_types_admin_id_foreign` (`admin_id`),
  ADD KEY `co_scholastic_types_update_by_foreign` (`update_by`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`designation_id`),
  ADD KEY `designations_admin_id_foreign` (`admin_id`),
  ADD KEY `designations_update_by_foreign` (`update_by`);

--
-- Indexes for table `document_category`
--
ALTER TABLE `document_category`
  ADD PRIMARY KEY (`document_category_id`),
  ADD KEY `document_category_admin_id_foreign` (`admin_id`),
  ADD KEY `document_category_update_by_foreign` (`update_by`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`exam_id`),
  ADD KEY `exams_admin_id_foreign` (`admin_id`),
  ADD KEY `exams_update_by_foreign` (`update_by`),
  ADD KEY `exams_term_exam_id_foreign` (`term_exam_id`);

--
-- Indexes for table `exam_map`
--
ALTER TABLE `exam_map`
  ADD PRIMARY KEY (`exam_map_id`),
  ADD KEY `exam_map_admin_id_foreign` (`admin_id`),
  ADD KEY `exam_map_update_by_foreign` (`update_by`),
  ADD KEY `exam_map_class_id_foreign` (`class_id`),
  ADD KEY `exam_map_subject_id_foreign` (`subject_id`),
  ADD KEY `exam_map_marks_criteria_id_foreign` (`marks_criteria_id`),
  ADD KEY `exam_map_grade_scheme_id_foreign` (`grade_id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`facility_id`),
  ADD KEY `facilities_admin_id_foreign` (`admin_id`),
  ADD KEY `facilities_update_by_foreign` (`update_by`);

--
-- Indexes for table `fees_st_map`
--
ALTER TABLE `fees_st_map`
  ADD PRIMARY KEY (`fees_st_map_id`),
  ADD KEY `fees_st_map_admin_id_foreign` (`admin_id`),
  ADD KEY `fees_st_map_update_by_foreign` (`update_by`),
  ADD KEY `fees_st_map_class_id_foreign` (`class_id`),
  ADD KEY `fees_st_map_session_id_foreign` (`session_id`),
  ADD KEY `fees_st_map_section_id_foreign` (`section_id`);

--
-- Indexes for table `fee_receipt`
--
ALTER TABLE `fee_receipt`
  ADD PRIMARY KEY (`receipt_id`),
  ADD KEY `fee_receipt_admin_id_foreign` (`admin_id`),
  ADD KEY `fee_receipt_update_by_foreign` (`update_by`),
  ADD KEY `fee_receipt_session_id_foreign` (`session_id`),
  ADD KEY `fee_receipt_class_id_foreign` (`class_id`),
  ADD KEY `fee_receipt_student_id_foreign` (`student_id`),
  ADD KEY `fee_receipt_bank_id_foreign` (`bank_id`);

--
-- Indexes for table `fee_receipt_details`
--
ALTER TABLE `fee_receipt_details`
  ADD PRIMARY KEY (`fee_receipt_detail_id`),
  ADD KEY `fee_receipt_details_admin_id_foreign` (`admin_id`),
  ADD KEY `fee_receipt_details_update_by_foreign` (`update_by`),
  ADD KEY `fee_receipt_details_receipt_id_foreign` (`receipt_id`),
  ADD KEY `fee_receipt_details_head_inst_id_foreign` (`head_inst_id`),
  ADD KEY `fee_receipt_details_r_concession_map_id_foreign` (`r_concession_map_id`),
  ADD KEY `fee_receipt_details_r_fine_id_foreign` (`r_fine_id`),
  ADD KEY `fee_receipt_details_r_fine_detail_id_foreign` (`r_fine_detail_id`);

--
-- Indexes for table `fine`
--
ALTER TABLE `fine`
  ADD PRIMARY KEY (`fine_id`),
  ADD KEY `fine_admin_id_foreign` (`admin_id`),
  ADD KEY `fine_update_by_foreign` (`update_by`);

--
-- Indexes for table `fine_details`
--
ALTER TABLE `fine_details`
  ADD PRIMARY KEY (`fine_detail_id`),
  ADD KEY `fine_details_admin_id_foreign` (`admin_id`),
  ADD KEY `fine_details_update_by_foreign` (`update_by`),
  ADD KEY `fine_details_fine_id_foreign` (`fine_id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`grade_id`),
  ADD KEY `grades_admin_id_foreign` (`admin_id`),
  ADD KEY `grades_update_by_foreign` (`update_by`),
  ADD KEY `grades_grade_scheme_id_foreign` (`grade_scheme_id`);

--
-- Indexes for table `grade_schemes`
--
ALTER TABLE `grade_schemes`
  ADD PRIMARY KEY (`grade_scheme_id`),
  ADD KEY `grade_schemes_admin_id_foreign` (`admin_id`),
  ADD KEY `grade_schemes_update_by_foreign` (`update_by`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `groups_admin_id_foreign` (`admin_id`),
  ADD KEY `groups_update_by_foreign` (`update_by`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`holiday_id`),
  ADD KEY `holidays_admin_id_foreign` (`admin_id`),
  ADD KEY `holidays_update_by_foreign` (`update_by`),
  ADD KEY `holidays_session_id_foreign` (`session_id`);

--
-- Indexes for table `homework_groups`
--
ALTER TABLE `homework_groups`
  ADD PRIMARY KEY (`homework_group_id`),
  ADD KEY `homework_groups_admin_id_foreign` (`admin_id`),
  ADD KEY `homework_groups_update_by_foreign` (`update_by`),
  ADD KEY `homework_groups_class_id_foreign` (`class_id`),
  ADD KEY `homework_groups_section_id_foreign` (`section_id`);

--
-- Indexes for table `imprest_ac`
--
ALTER TABLE `imprest_ac`
  ADD PRIMARY KEY (`imprest_ac_id`),
  ADD KEY `imprest_ac_admin_id_foreign` (`admin_id`),
  ADD KEY `imprest_ac_update_by_foreign` (`update_by`),
  ADD KEY `imprest_ac_session_id_foreign` (`session_id`),
  ADD KEY `imprest_ac_class_id_foreign` (`class_id`),
  ADD KEY `imprest_ac_student_id_foreign` (`student_id`);

--
-- Indexes for table `imprest_ac_confi`
--
ALTER TABLE `imprest_ac_confi`
  ADD PRIMARY KEY (`imprest_ac_confi_id`),
  ADD KEY `imprest_ac_confi_admin_id_foreign` (`admin_id`),
  ADD KEY `imprest_ac_confi_update_by_foreign` (`update_by`);

--
-- Indexes for table `issued_books`
--
ALTER TABLE `issued_books`
  ADD PRIMARY KEY (`issued_book_id`),
  ADD KEY `issued_books_admin_id_foreign` (`admin_id`),
  ADD KEY `issued_books_update_by_foreign` (`update_by`),
  ADD KEY `issued_books_book_id_foreign` (`book_id`);

--
-- Indexes for table `library_members`
--
ALTER TABLE `library_members`
  ADD PRIMARY KEY (`library_member_id`),
  ADD KEY `library_members_admin_id_foreign` (`admin_id`),
  ADD KEY `library_members_update_by_foreign` (`update_by`);

--
-- Indexes for table `marks_criteria`
--
ALTER TABLE `marks_criteria`
  ADD PRIMARY KEY (`marks_criteria_id`),
  ADD KEY `marks_criteria_admin_id_foreign` (`admin_id`),
  ADD KEY `marks_criteria_update_by_foreign` (`update_by`);

--
-- Indexes for table `medium_type`
--
ALTER TABLE `medium_type`
  ADD PRIMARY KEY (`medium_type_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nationality`
--
ALTER TABLE `nationality`
  ADD PRIMARY KEY (`nationality_id`),
  ADD KEY `nationality_admin_id_foreign` (`admin_id`),
  ADD KEY `nationality_update_by_foreign` (`update_by`);

--
-- Indexes for table `one_time_heads`
--
ALTER TABLE `one_time_heads`
  ADD PRIMARY KEY (`ot_head_id`),
  ADD KEY `one_time_heads_admin_id_foreign` (`admin_id`),
  ADD KEY `one_time_heads_update_by_foreign` (`update_by`);

--
-- Indexes for table `online_notes`
--
ALTER TABLE `online_notes`
  ADD PRIMARY KEY (`online_note_id`),
  ADD KEY `online_notes_admin_id_foreign` (`admin_id`),
  ADD KEY `online_notes_update_by_foreign` (`update_by`),
  ADD KEY `online_notes_class_id_foreign` (`class_id`),
  ADD KEY `online_notes_section_id_foreign` (`section_id`),
  ADD KEY `online_notes_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `question_papers`
--
ALTER TABLE `question_papers`
  ADD PRIMARY KEY (`question_paper_id`),
  ADD KEY `question_papers_admin_id_foreign` (`admin_id`),
  ADD KEY `question_papers_update_by_foreign` (`update_by`),
  ADD KEY `question_papers_class_id_foreign` (`class_id`),
  ADD KEY `question_papers_section_id_foreign` (`section_id`),
  ADD KEY `question_papers_subject_id_foreign` (`subject_id`),
  ADD KEY `question_papers_exam_id_foreign` (`exam_id`),
  ADD KEY `question_papers_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `reasons`
--
ALTER TABLE `reasons`
  ADD PRIMARY KEY (`reason_id`),
  ADD KEY `reasons_admin_id_foreign` (`admin_id`),
  ADD KEY `reasons_update_by_foreign` (`update_by`);

--
-- Indexes for table `recurring_heads`
--
ALTER TABLE `recurring_heads`
  ADD PRIMARY KEY (`rc_head_id`),
  ADD KEY `recurring_heads_admin_id_foreign` (`admin_id`),
  ADD KEY `recurring_heads_update_by_foreign` (`update_by`);

--
-- Indexes for table `recurring_inst`
--
ALTER TABLE `recurring_inst`
  ADD PRIMARY KEY (`rc_inst_id`),
  ADD KEY `recurring_inst_admin_id_foreign` (`admin_id`),
  ADD KEY `recurring_inst_update_by_foreign` (`update_by`),
  ADD KEY `recurring_inst_rc_head_id_foreign` (`rc_head_id`);

--
-- Indexes for table `religions`
--
ALTER TABLE `religions`
  ADD PRIMARY KEY (`religion_id`),
  ADD KEY `religions_admin_id_foreign` (`admin_id`),
  ADD KEY `religions_update_by_foreign` (`update_by`);

--
-- Indexes for table `room_no`
--
ALTER TABLE `room_no`
  ADD PRIMARY KEY (`room_no_id`),
  ADD KEY `room_no_admin_id_foreign` (`admin_id`),
  ADD KEY `room_no_update_by_foreign` (`update_by`);

--
-- Indexes for table `rte_cheques`
--
ALTER TABLE `rte_cheques`
  ADD PRIMARY KEY (`rte_cheque_id`),
  ADD KEY `rte_cheques_admin_id_foreign` (`admin_id`),
  ADD KEY `rte_cheques_update_by_foreign` (`update_by`),
  ADD KEY `rte_cheques_bank_id_foreign` (`bank_id`);

--
-- Indexes for table `rte_heads`
--
ALTER TABLE `rte_heads`
  ADD PRIMARY KEY (`rte_head_id`),
  ADD KEY `rte_heads_admin_id_foreign` (`admin_id`),
  ADD KEY `rte_heads_update_by_foreign` (`update_by`);

--
-- Indexes for table `rte_head_map`
--
ALTER TABLE `rte_head_map`
  ADD PRIMARY KEY (`rte_head_map_id`),
  ADD KEY `rte_head_map_admin_id_foreign` (`admin_id`),
  ADD KEY `rte_head_map_update_by_foreign` (`update_by`),
  ADD KEY `rte_head_map_rte_head_id_foreign` (`rte_head_id`),
  ADD KEY `rte_head_map_session_id_foreign` (`session_id`),
  ADD KEY `rte_head_map_class_id_foreign` (`class_id`),
  ADD KEY `rte_head_map_section_id_foreign` (`section_id`),
  ADD KEY `rte_head_map_student_id_foreign` (`student_id`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`school_id`),
  ADD KEY `school_admin_id_foreign` (`admin_id`),
  ADD KEY `school_update_by_foreign` (`update_by`),
  ADD KEY `school_country_id_foreign` (`country_id`),
  ADD KEY `school_state_id_foreign` (`state_id`),
  ADD KEY `school_city_id_foreign` (`city_id`);

--
-- Indexes for table `school_boards`
--
ALTER TABLE `school_boards`
  ADD PRIMARY KEY (`board_id`),
  ADD KEY `school_boards_admin_id_foreign` (`admin_id`),
  ADD KEY `school_boards_update_by_foreign` (`update_by`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`section_id`),
  ADD KEY `sections_admin_id_foreign` (`admin_id`),
  ADD KEY `sections_update_by_foreign` (`update_by`),
  ADD KEY `sections_class_id_foreign` (`class_id`),
  ADD KEY `sections_shift_id_foreign` (`shift_id`),
  ADD KEY `sections_stream_id_foreign` (`stream_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `sessions_admin_id_foreign` (`admin_id`),
  ADD KEY `sessions_update_by_foreign` (`update_by`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`shift_id`),
  ADD KEY `shifts_admin_id_foreign` (`admin_id`),
  ADD KEY `shifts_update_by_foreign` (`update_by`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`),
  ADD KEY `staff_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_update_by_foreign` (`update_by`),
  ADD KEY `staff_reference_admin_id_foreign` (`reference_admin_id`),
  ADD KEY `staff_designation_id_foreign` (`designation_id`),
  ADD KEY `staff_staff_role_id_foreign` (`staff_role_id`),
  ADD KEY `staff_title_id_foreign` (`title_id`),
  ADD KEY `staff_caste_id_foreign` (`caste_id`),
  ADD KEY `staff_religion_id_foreign` (`religion_id`),
  ADD KEY `staff_nationality_id_foreign` (`nationality_id`),
  ADD KEY `staff_staff_temporary_county_foreign` (`staff_temporary_county`),
  ADD KEY `staff_staff_temporary_state_foreign` (`staff_temporary_state`),
  ADD KEY `staff_staff_temporary_city_foreign` (`staff_temporary_city`),
  ADD KEY `staff_staff_permanent_county_foreign` (`staff_permanent_county`),
  ADD KEY `staff_staff_permanent_state_foreign` (`staff_permanent_state`),
  ADD KEY `staff_staff_permanent_city_foreign` (`staff_permanent_city`);

--
-- Indexes for table `staff_class_allocations`
--
ALTER TABLE `staff_class_allocations`
  ADD PRIMARY KEY (`staff_class_allocation_id`),
  ADD KEY `staff_class_allocations_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_class_allocations_update_by_foreign` (`update_by`),
  ADD KEY `staff_class_allocations_class_id_foreign` (`class_id`),
  ADD KEY `staff_class_allocations_section_id_foreign` (`section_id`),
  ADD KEY `staff_class_allocations_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `staff_documents`
--
ALTER TABLE `staff_documents`
  ADD PRIMARY KEY (`staff_document_id`),
  ADD KEY `staff_documents_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_documents_update_by_foreign` (`update_by`),
  ADD KEY `staff_documents_staff_id_foreign` (`staff_id`),
  ADD KEY `staff_documents_document_category_id_foreign` (`document_category_id`);

--
-- Indexes for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  ADD PRIMARY KEY (`staff_leave_id`),
  ADD KEY `staff_leaves_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_leaves_update_by_foreign` (`update_by`),
  ADD KEY `staff_leaves_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `staff_roles`
--
ALTER TABLE `staff_roles`
  ADD PRIMARY KEY (`staff_role_id`),
  ADD KEY `staff_roles_admin_id_foreign` (`admin_id`),
  ADD KEY `staff_roles_update_by_foreign` (`update_by`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`),
  ADD KEY `state_country_id_foreign` (`country_id`);

--
-- Indexes for table `streams`
--
ALTER TABLE `streams`
  ADD PRIMARY KEY (`stream_id`),
  ADD KEY `streams_admin_id_foreign` (`admin_id`),
  ADD KEY `streams_update_by_foreign` (`update_by`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`student_id`),
  ADD KEY `students_admin_id_foreign` (`admin_id`),
  ADD KEY `students_update_by_foreign` (`update_by`),
  ADD KEY `students_reference_admin_id_foreign` (`reference_admin_id`),
  ADD KEY `students_student_parent_id_foreign` (`student_parent_id`),
  ADD KEY `students_student_sibling_class_id_foreign` (`student_sibling_class_id`),
  ADD KEY `students_caste_id_foreign` (`caste_id`),
  ADD KEY `students_title_id_foreign` (`title_id`),
  ADD KEY `students_religion_id_foreign` (`religion_id`),
  ADD KEY `students_nationality_id_foreign` (`nationality_id`),
  ADD KEY `students_student_permanent_county_foreign` (`student_permanent_county`),
  ADD KEY `students_student_permanent_state_foreign` (`student_permanent_state`),
  ADD KEY `students_student_permanent_city_foreign` (`student_permanent_city`),
  ADD KEY `students_student_temporary_county_foreign` (`student_temporary_county`),
  ADD KEY `students_student_temporary_state_foreign` (`student_temporary_state`),
  ADD KEY `students_student_temporary_city_foreign` (`student_temporary_city`);

--
-- Indexes for table `student_academic_history_info`
--
ALTER TABLE `student_academic_history_info`
  ADD PRIMARY KEY (`student_academic_history_info_id`),
  ADD KEY `student_academic_history_info_admin_id_foreign` (`admin_id`),
  ADD KEY `student_academic_history_info_update_by_foreign` (`update_by`),
  ADD KEY `student_academic_history_info_student_id_foreign` (`student_id`),
  ADD KEY `student_academic_history_info_student_academic_info_id_foreign` (`student_academic_info_id`),
  ADD KEY `student_academic_history_info_session_id_foreign` (`session_id`),
  ADD KEY `student_academic_history_info_class_id_foreign` (`class_id`),
  ADD KEY `student_academic_history_info_stream_id_foreign` (`stream_id`),
  ADD KEY `student_academic_history_info_section_id_foreign` (`section_id`);

--
-- Indexes for table `student_academic_info`
--
ALTER TABLE `student_academic_info`
  ADD PRIMARY KEY (`student_academic_info_id`),
  ADD KEY `student_academic_info_admin_id_foreign` (`admin_id`),
  ADD KEY `student_academic_info_update_by_foreign` (`update_by`),
  ADD KEY `student_academic_info_student_id_foreign` (`student_id`),
  ADD KEY `student_academic_info_admission_class_id_foreign` (`admission_class_id`),
  ADD KEY `student_academic_info_admission_section_id_foreign` (`admission_section_id`),
  ADD KEY `student_academic_info_admission_session_id_foreign` (`admission_session_id`),
  ADD KEY `student_academic_info_current_session_id_foreign` (`current_session_id`),
  ADD KEY `student_academic_info_current_class_id_foreign` (`current_class_id`),
  ADD KEY `student_academic_info_current_section_id_foreign` (`current_section_id`),
  ADD KEY `student_academic_info_group_id_foreign` (`group_id`),
  ADD KEY `student_academic_info_stream_id_foreign` (`stream_id`);

--
-- Indexes for table `student_cheques`
--
ALTER TABLE `student_cheques`
  ADD PRIMARY KEY (`student_cheque_id`),
  ADD KEY `student_cheques_admin_id_foreign` (`admin_id`),
  ADD KEY `student_cheques_update_by_foreign` (`update_by`),
  ADD KEY `student_cheques_student_id_foreign` (`student_id`),
  ADD KEY `student_cheques_bank_id_foreign` (`bank_id`);

--
-- Indexes for table `student_documents`
--
ALTER TABLE `student_documents`
  ADD PRIMARY KEY (`student_document_id`),
  ADD KEY `student_documents_admin_id_foreign` (`admin_id`),
  ADD KEY `student_documents_update_by_foreign` (`update_by`),
  ADD KEY `student_documents_student_id_foreign` (`student_id`),
  ADD KEY `student_documents_document_category_id_foreign` (`document_category_id`);

--
-- Indexes for table `student_health_info`
--
ALTER TABLE `student_health_info`
  ADD PRIMARY KEY (`student_health_info_id`),
  ADD KEY `student_health_info_admin_id_foreign` (`admin_id`),
  ADD KEY `student_health_info_update_by_foreign` (`update_by`),
  ADD KEY `student_health_info_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_leaves`
--
ALTER TABLE `student_leaves`
  ADD PRIMARY KEY (`student_leave_id`),
  ADD KEY `student_leaves_admin_id_foreign` (`admin_id`),
  ADD KEY `student_leaves_update_by_foreign` (`update_by`),
  ADD KEY `student_leaves_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_parents`
--
ALTER TABLE `student_parents`
  ADD PRIMARY KEY (`student_parent_id`),
  ADD KEY `student_parents_admin_id_foreign` (`admin_id`),
  ADD KEY `student_parents_update_by_foreign` (`update_by`),
  ADD KEY `student_parents_reference_admin_id_foreign` (`reference_admin_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subject_id`),
  ADD KEY `subjects_admin_id_foreign` (`admin_id`),
  ADD KEY `subjects_update_by_foreign` (`update_by`),
  ADD KEY `subjects_co_scholastic_type_id_foreign` (`co_scholastic_type_id`);

--
-- Indexes for table `subject_class_map`
--
ALTER TABLE `subject_class_map`
  ADD PRIMARY KEY (`subject_class_map_id`),
  ADD KEY `subject_class_map_admin_id_foreign` (`admin_id`),
  ADD KEY `subject_class_map_update_by_foreign` (`update_by`),
  ADD KEY `subject_class_map_class_id_foreign` (`class_id`),
  ADD KEY `subject_class_map_section_id_foreign` (`section_id`),
  ADD KEY `subject_class_map_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `subject_section_map`
--
ALTER TABLE `subject_section_map`
  ADD PRIMARY KEY (`subject_section_map_id`),
  ADD KEY `subject_section_map_admin_id_foreign` (`admin_id`),
  ADD KEY `subject_section_map_update_by_foreign` (`update_by`),
  ADD KEY `subject_section_map_class_id_foreign` (`class_id`),
  ADD KEY `subject_section_map_section_id_foreign` (`section_id`),
  ADD KEY `subject_section_map_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `subject_student_map`
--
ALTER TABLE `subject_student_map`
  ADD PRIMARY KEY (`subject_student_map_id`),
  ADD KEY `subject_student_map_admin_id_foreign` (`admin_id`),
  ADD KEY `subject_student_map_update_by_foreign` (`update_by`),
  ADD KEY `subject_student_map_class_id_foreign` (`class_id`),
  ADD KEY `subject_student_map_section_id_foreign` (`section_id`),
  ADD KEY `subject_student_map_subject_id_foreign` (`subject_id`),
  ADD KEY `subject_student_map_student_id_foreign` (`student_id`);

--
-- Indexes for table `term_exams`
--
ALTER TABLE `term_exams`
  ADD PRIMARY KEY (`term_exam_id`),
  ADD KEY `term_exams_admin_id_foreign` (`admin_id`),
  ADD KEY `term_exams_update_by_foreign` (`update_by`);

--
-- Indexes for table `time_tables`
--
ALTER TABLE `time_tables`
  ADD PRIMARY KEY (`time_table_id`),
  ADD KEY `time_tables_admin_id_foreign` (`admin_id`),
  ADD KEY `time_tables_update_by_foreign` (`update_by`),
  ADD KEY `time_tables_class_id_foreign` (`class_id`),
  ADD KEY `time_tables_section_id_foreign` (`section_id`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`title_id`),
  ADD KEY `titles_admin_id_foreign` (`admin_id`),
  ADD KEY `titles_update_by_foreign` (`update_by`);

--
-- Indexes for table `virtual_classes`
--
ALTER TABLE `virtual_classes`
  ADD PRIMARY KEY (`virtual_class_id`),
  ADD KEY `virtual_classes_admin_id_foreign` (`admin_id`),
  ADD KEY `virtual_classes_update_by_foreign` (`update_by`);

--
-- Indexes for table `virtual_class_map`
--
ALTER TABLE `virtual_class_map`
  ADD PRIMARY KEY (`virtual_class_map_id`),
  ADD KEY `virtual_class_map_admin_id_foreign` (`admin_id`),
  ADD KEY `virtual_class_map_update_by_foreign` (`update_by`),
  ADD KEY `virtual_class_map_virtual_class_id_foreign` (`virtual_class_id`),
  ADD KEY `virtual_class_map_student_id_foreign` (`student_id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`visitor_id`),
  ADD KEY `visitors_admin_id_foreign` (`admin_id`),
  ADD KEY `visitors_update_by_foreign` (`update_by`);

--
-- Indexes for table `wallet_ac`
--
ALTER TABLE `wallet_ac`
  ADD PRIMARY KEY (`wallet_ac_id`),
  ADD KEY `wallet_ac_admin_id_foreign` (`admin_id`),
  ADD KEY `wallet_ac_update_by_foreign` (`update_by`),
  ADD KEY `wallet_ac_session_id_foreign` (`session_id`),
  ADD KEY `wallet_ac_class_id_foreign` (`class_id`),
  ADD KEY `wallet_ac_student_id_foreign` (`student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `bank_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `book_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `book_allowance`
--
ALTER TABLE `book_allowance`
  MODIFY `book_allowance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `book_categories`
--
ALTER TABLE `book_categories`
  MODIFY `book_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `book_copies_info`
--
ALTER TABLE `book_copies_info`
  MODIFY `book_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `book_cupboards`
--
ALTER TABLE `book_cupboards`
  MODIFY `book_cupboard_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `book_cupboardshelfs`
--
ALTER TABLE `book_cupboardshelfs`
  MODIFY `book_cupboardshelf_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `book_vendors`
--
ALTER TABLE `book_vendors`
  MODIFY `book_vendor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brochures`
--
ALTER TABLE `brochures`
  MODIFY `brochure_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `caste`
--
ALTER TABLE `caste`
  MODIFY `caste_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `class_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `class_subject_staff_map`
--
ALTER TABLE `class_subject_staff_map`
  MODIFY `class_subject_staff_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `competitions`
--
ALTER TABLE `competitions`
  MODIFY `competition_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `competition_map`
--
ALTER TABLE `competition_map`
  MODIFY `student_competition_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `concession_map`
--
ALTER TABLE `concession_map`
  MODIFY `concession_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `co_scholastic_types`
--
ALTER TABLE `co_scholastic_types`
  MODIFY `co_scholastic_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `designation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `document_category`
--
ALTER TABLE `document_category`
  MODIFY `document_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `exam_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `exam_map`
--
ALTER TABLE `exam_map`
  MODIFY `exam_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `facility_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fees_st_map`
--
ALTER TABLE `fees_st_map`
  MODIFY `fees_st_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fee_receipt`
--
ALTER TABLE `fee_receipt`
  MODIFY `receipt_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fee_receipt_details`
--
ALTER TABLE `fee_receipt_details`
  MODIFY `fee_receipt_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fine`
--
ALTER TABLE `fine`
  MODIFY `fine_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fine_details`
--
ALTER TABLE `fine_details`
  MODIFY `fine_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `grade_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `grade_schemes`
--
ALTER TABLE `grade_schemes`
  MODIFY `grade_scheme_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `holiday_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `homework_groups`
--
ALTER TABLE `homework_groups`
  MODIFY `homework_group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `imprest_ac`
--
ALTER TABLE `imprest_ac`
  MODIFY `imprest_ac_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `imprest_ac_confi`
--
ALTER TABLE `imprest_ac_confi`
  MODIFY `imprest_ac_confi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `issued_books`
--
ALTER TABLE `issued_books`
  MODIFY `issued_book_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `library_members`
--
ALTER TABLE `library_members`
  MODIFY `library_member_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marks_criteria`
--
ALTER TABLE `marks_criteria`
  MODIFY `marks_criteria_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `medium_type`
--
ALTER TABLE `medium_type`
  MODIFY `medium_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `nationality`
--
ALTER TABLE `nationality`
  MODIFY `nationality_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `one_time_heads`
--
ALTER TABLE `one_time_heads`
  MODIFY `ot_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `online_notes`
--
ALTER TABLE `online_notes`
  MODIFY `online_note_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `question_papers`
--
ALTER TABLE `question_papers`
  MODIFY `question_paper_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reasons`
--
ALTER TABLE `reasons`
  MODIFY `reason_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `recurring_heads`
--
ALTER TABLE `recurring_heads`
  MODIFY `rc_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recurring_inst`
--
ALTER TABLE `recurring_inst`
  MODIFY `rc_inst_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `religions`
--
ALTER TABLE `religions`
  MODIFY `religion_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `room_no`
--
ALTER TABLE `room_no`
  MODIFY `room_no_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rte_cheques`
--
ALTER TABLE `rte_cheques`
  MODIFY `rte_cheque_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rte_heads`
--
ALTER TABLE `rte_heads`
  MODIFY `rte_head_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rte_head_map`
--
ALTER TABLE `rte_head_map`
  MODIFY `rte_head_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
  MODIFY `school_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `school_boards`
--
ALTER TABLE `school_boards`
  MODIFY `board_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `section_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `session_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `shift_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `staff_class_allocations`
--
ALTER TABLE `staff_class_allocations`
  MODIFY `staff_class_allocation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `staff_documents`
--
ALTER TABLE `staff_documents`
  MODIFY `staff_document_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  MODIFY `staff_leave_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff_roles`
--
ALTER TABLE `staff_roles`
  MODIFY `staff_role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `streams`
--
ALTER TABLE `streams`
  MODIFY `stream_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `student_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `student_academic_history_info`
--
ALTER TABLE `student_academic_history_info`
  MODIFY `student_academic_history_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_academic_info`
--
ALTER TABLE `student_academic_info`
  MODIFY `student_academic_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `student_cheques`
--
ALTER TABLE `student_cheques`
  MODIFY `student_cheque_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_documents`
--
ALTER TABLE `student_documents`
  MODIFY `student_document_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_health_info`
--
ALTER TABLE `student_health_info`
  MODIFY `student_health_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `student_leaves`
--
ALTER TABLE `student_leaves`
  MODIFY `student_leave_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_parents`
--
ALTER TABLE `student_parents`
  MODIFY `student_parent_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `subject_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `subject_class_map`
--
ALTER TABLE `subject_class_map`
  MODIFY `subject_class_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `subject_section_map`
--
ALTER TABLE `subject_section_map`
  MODIFY `subject_section_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subject_student_map`
--
ALTER TABLE `subject_student_map`
  MODIFY `subject_student_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `term_exams`
--
ALTER TABLE `term_exams`
  MODIFY `term_exam_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `time_tables`
--
ALTER TABLE `time_tables`
  MODIFY `time_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `title_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `virtual_classes`
--
ALTER TABLE `virtual_classes`
  MODIFY `virtual_class_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `virtual_class_map`
--
ALTER TABLE `virtual_class_map`
  MODIFY `virtual_class_map_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `visitor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wallet_ac`
--
ALTER TABLE `wallet_ac`
  MODIFY `wallet_ac_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `books_book_category_id_foreign` FOREIGN KEY (`book_category_id`) REFERENCES `book_categories` (`book_category_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `books_book_cupboard_id_foreign` FOREIGN KEY (`book_cupboard_id`) REFERENCES `book_cupboards` (`book_cupboard_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `books_book_cupboardshelf_id_foreign` FOREIGN KEY (`book_cupboardshelf_id`) REFERENCES `book_cupboardshelfs` (`book_cupboardshelf_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `books_book_vendor_id_foreign` FOREIGN KEY (`book_vendor_id`) REFERENCES `book_vendors` (`book_vendor_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `books_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `book_allowance`
--
ALTER TABLE `book_allowance`
  ADD CONSTRAINT `book_allowance_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_allowance_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `book_categories`
--
ALTER TABLE `book_categories`
  ADD CONSTRAINT `book_categories_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_categories_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `book_copies_info`
--
ALTER TABLE `book_copies_info`
  ADD CONSTRAINT `book_copies_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_copies_info_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_copies_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `book_cupboards`
--
ALTER TABLE `book_cupboards`
  ADD CONSTRAINT `book_cupboards_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_cupboards_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `book_cupboardshelfs`
--
ALTER TABLE `book_cupboardshelfs`
  ADD CONSTRAINT `book_cupboardshelfs_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_cupboardshelfs_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `book_vendors`
--
ALTER TABLE `book_vendors`
  ADD CONSTRAINT `book_vendors_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `book_vendors_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `brochures`
--
ALTER TABLE `brochures`
  ADD CONSTRAINT `brochures_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `brochures_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `brochures_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `caste`
--
ALTER TABLE `caste`
  ADD CONSTRAINT `caste_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `caste_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `state` (`state_id`) ON DELETE CASCADE;

--
-- Constraints for table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `classes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `classes_board_id_foreign` FOREIGN KEY (`board_id`) REFERENCES `school_boards` (`board_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `classes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `class_subject_staff_map`
--
ALTER TABLE `class_subject_staff_map`
  ADD CONSTRAINT `class_subject_staff_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `class_subject_staff_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `class_subject_staff_map_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `class_subject_staff_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `competitions`
--
ALTER TABLE `competitions`
  ADD CONSTRAINT `competitions_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `competitions_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `competition_map`
--
ALTER TABLE `competition_map`
  ADD CONSTRAINT `competition_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `competition_map_competition_id_foreign` FOREIGN KEY (`competition_id`) REFERENCES `competitions` (`competition_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `competition_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `competition_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `concession_map`
--
ALTER TABLE `concession_map`
  ADD CONSTRAINT `concession_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `concession_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `concession_map_reason_id_foreign` FOREIGN KEY (`reason_id`) REFERENCES `reasons` (`reason_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `concession_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `concession_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `concession_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `co_scholastic_types`
--
ALTER TABLE `co_scholastic_types`
  ADD CONSTRAINT `co_scholastic_types_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `co_scholastic_types_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `designations`
--
ALTER TABLE `designations`
  ADD CONSTRAINT `designations_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `designations_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `document_category`
--
ALTER TABLE `document_category`
  ADD CONSTRAINT `document_category_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `document_category_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `exams`
--
ALTER TABLE `exams`
  ADD CONSTRAINT `exams_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `exams_term_exam_id_foreign` FOREIGN KEY (`term_exam_id`) REFERENCES `term_exams` (`term_exam_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `exams_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `exam_map`
--
ALTER TABLE `exam_map`
  ADD CONSTRAINT `exam_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `exam_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `exam_map_grade_scheme_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`grade_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `exam_map_marks_criteria_id_foreign` FOREIGN KEY (`marks_criteria_id`) REFERENCES `marks_criteria` (`marks_criteria_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `exam_map_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `exam_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `facilities`
--
ALTER TABLE `facilities`
  ADD CONSTRAINT `facilities_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `facilities_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `fees_st_map`
--
ALTER TABLE `fees_st_map`
  ADD CONSTRAINT `fees_st_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fees_st_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fees_st_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fees_st_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fees_st_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `fee_receipt`
--
ALTER TABLE `fee_receipt`
  ADD CONSTRAINT `fee_receipt_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fee_receipt_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`bank_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fee_receipt_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fee_receipt_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fee_receipt_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fee_receipt_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `fee_receipt_details`
--
ALTER TABLE `fee_receipt_details`
  ADD CONSTRAINT `fee_receipt_details_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fee_receipt_details_head_inst_id_foreign` FOREIGN KEY (`head_inst_id`) REFERENCES `recurring_inst` (`rc_inst_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fee_receipt_details_r_concession_map_id_foreign` FOREIGN KEY (`r_concession_map_id`) REFERENCES `concession_map` (`concession_map_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fee_receipt_details_r_fine_detail_id_foreign` FOREIGN KEY (`r_fine_detail_id`) REFERENCES `fine_details` (`fine_detail_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fee_receipt_details_r_fine_id_foreign` FOREIGN KEY (`r_fine_id`) REFERENCES `fine` (`fine_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fee_receipt_details_receipt_id_foreign` FOREIGN KEY (`receipt_id`) REFERENCES `fee_receipt` (`receipt_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fee_receipt_details_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `fine`
--
ALTER TABLE `fine`
  ADD CONSTRAINT `fine_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fine_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `fine_details`
--
ALTER TABLE `fine_details`
  ADD CONSTRAINT `fine_details_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fine_details_fine_id_foreign` FOREIGN KEY (`fine_id`) REFERENCES `fine` (`fine_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fine_details_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `grades_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `grades_grade_scheme_id_foreign` FOREIGN KEY (`grade_scheme_id`) REFERENCES `grade_schemes` (`grade_scheme_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `grades_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `grade_schemes`
--
ALTER TABLE `grade_schemes`
  ADD CONSTRAINT `grade_schemes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `grade_schemes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `groups_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `holidays`
--
ALTER TABLE `holidays`
  ADD CONSTRAINT `holidays_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `holidays_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `holidays_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `homework_groups`
--
ALTER TABLE `homework_groups`
  ADD CONSTRAINT `homework_groups_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `homework_groups_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `homework_groups_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `homework_groups_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `imprest_ac`
--
ALTER TABLE `imprest_ac`
  ADD CONSTRAINT `imprest_ac_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `imprest_ac_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `imprest_ac_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `imprest_ac_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `imprest_ac_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `imprest_ac_confi`
--
ALTER TABLE `imprest_ac_confi`
  ADD CONSTRAINT `imprest_ac_confi_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `imprest_ac_confi_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `issued_books`
--
ALTER TABLE `issued_books`
  ADD CONSTRAINT `issued_books_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `issued_books_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `issued_books_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `library_members`
--
ALTER TABLE `library_members`
  ADD CONSTRAINT `library_members_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `library_members_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `marks_criteria`
--
ALTER TABLE `marks_criteria`
  ADD CONSTRAINT `marks_criteria_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `marks_criteria_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `nationality`
--
ALTER TABLE `nationality`
  ADD CONSTRAINT `nationality_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `nationality_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `one_time_heads`
--
ALTER TABLE `one_time_heads`
  ADD CONSTRAINT `one_time_heads_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `one_time_heads_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `online_notes`
--
ALTER TABLE `online_notes`
  ADD CONSTRAINT `online_notes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `online_notes_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `online_notes_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `online_notes_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `online_notes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `question_papers`
--
ALTER TABLE `question_papers`
  ADD CONSTRAINT `question_papers_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `question_papers_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `question_papers_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`exam_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `question_papers_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `question_papers_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `question_papers_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `question_papers_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `reasons`
--
ALTER TABLE `reasons`
  ADD CONSTRAINT `reasons_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reasons_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `recurring_heads`
--
ALTER TABLE `recurring_heads`
  ADD CONSTRAINT `recurring_heads_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recurring_heads_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `recurring_inst`
--
ALTER TABLE `recurring_inst`
  ADD CONSTRAINT `recurring_inst_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recurring_inst_rc_head_id_foreign` FOREIGN KEY (`rc_head_id`) REFERENCES `recurring_heads` (`rc_head_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recurring_inst_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `religions`
--
ALTER TABLE `religions`
  ADD CONSTRAINT `religions_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `religions_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `room_no`
--
ALTER TABLE `room_no`
  ADD CONSTRAINT `room_no_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `room_no_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `rte_cheques`
--
ALTER TABLE `rte_cheques`
  ADD CONSTRAINT `rte_cheques_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rte_cheques_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`bank_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rte_cheques_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `rte_heads`
--
ALTER TABLE `rte_heads`
  ADD CONSTRAINT `rte_heads_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rte_heads_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `rte_head_map`
--
ALTER TABLE `rte_head_map`
  ADD CONSTRAINT `rte_head_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rte_head_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rte_head_map_rte_head_id_foreign` FOREIGN KEY (`rte_head_id`) REFERENCES `rte_heads` (`rte_head_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rte_head_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rte_head_map_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rte_head_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rte_head_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `school`
--
ALTER TABLE `school`
  ADD CONSTRAINT `school_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `school_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `school_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `school_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `state` (`state_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `school_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `school_boards`
--
ALTER TABLE `school_boards`
  ADD CONSTRAINT `school_boards_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `school_boards_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sections_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sections_shift_id_foreign` FOREIGN KEY (`shift_id`) REFERENCES `shifts` (`shift_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sections_stream_id_foreign` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`stream_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sections_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sessions_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `shifts`
--
ALTER TABLE `shifts`
  ADD CONSTRAINT `shifts_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `shifts_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `staff_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_caste_id_foreign` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`caste_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_designation_id_foreign` FOREIGN KEY (`designation_id`) REFERENCES `designations` (`designation_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_nationality_id_foreign` FOREIGN KEY (`nationality_id`) REFERENCES `nationality` (`nationality_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_reference_admin_id_foreign` FOREIGN KEY (`reference_admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_religion_id_foreign` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`religion_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_staff_permanent_city_foreign` FOREIGN KEY (`staff_permanent_city`) REFERENCES `city` (`city_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_staff_permanent_county_foreign` FOREIGN KEY (`staff_permanent_county`) REFERENCES `country` (`country_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_staff_permanent_state_foreign` FOREIGN KEY (`staff_permanent_state`) REFERENCES `state` (`state_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_staff_role_id_foreign` FOREIGN KEY (`staff_role_id`) REFERENCES `staff_roles` (`staff_role_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_staff_temporary_city_foreign` FOREIGN KEY (`staff_temporary_city`) REFERENCES `city` (`city_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_staff_temporary_county_foreign` FOREIGN KEY (`staff_temporary_county`) REFERENCES `country` (`country_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_staff_temporary_state_foreign` FOREIGN KEY (`staff_temporary_state`) REFERENCES `state` (`state_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_title_id_foreign` FOREIGN KEY (`title_id`) REFERENCES `titles` (`title_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `staff_class_allocations`
--
ALTER TABLE `staff_class_allocations`
  ADD CONSTRAINT `staff_class_allocations_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_class_allocations_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_class_allocations_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_class_allocations_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_class_allocations_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `staff_documents`
--
ALTER TABLE `staff_documents`
  ADD CONSTRAINT `staff_documents_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `document_category` (`document_category_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_documents_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_documents_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  ADD CONSTRAINT `staff_leaves_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_leaves_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_leaves_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `staff_roles`
--
ALTER TABLE `staff_roles`
  ADD CONSTRAINT `staff_roles_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `staff_roles_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `state`
--
ALTER TABLE `state`
  ADD CONSTRAINT `state_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`) ON DELETE CASCADE;

--
-- Constraints for table `streams`
--
ALTER TABLE `streams`
  ADD CONSTRAINT `streams_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `streams_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_caste_id_foreign` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`caste_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_nationality_id_foreign` FOREIGN KEY (`nationality_id`) REFERENCES `nationality` (`nationality_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_reference_admin_id_foreign` FOREIGN KEY (`reference_admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_religion_id_foreign` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`religion_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_student_parent_id_foreign` FOREIGN KEY (`student_parent_id`) REFERENCES `student_parents` (`student_parent_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_student_permanent_city_foreign` FOREIGN KEY (`student_permanent_city`) REFERENCES `city` (`city_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_student_permanent_county_foreign` FOREIGN KEY (`student_permanent_county`) REFERENCES `country` (`country_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_student_permanent_state_foreign` FOREIGN KEY (`student_permanent_state`) REFERENCES `state` (`state_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_student_sibling_class_id_foreign` FOREIGN KEY (`student_sibling_class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_student_temporary_city_foreign` FOREIGN KEY (`student_temporary_city`) REFERENCES `city` (`city_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_student_temporary_county_foreign` FOREIGN KEY (`student_temporary_county`) REFERENCES `country` (`country_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_student_temporary_state_foreign` FOREIGN KEY (`student_temporary_state`) REFERENCES `state` (`state_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_title_id_foreign` FOREIGN KEY (`title_id`) REFERENCES `titles` (`title_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `students_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_academic_history_info`
--
ALTER TABLE `student_academic_history_info`
  ADD CONSTRAINT `student_academic_history_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_stream_id_foreign` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`stream_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_student_academic_info_id_foreign` FOREIGN KEY (`student_academic_info_id`) REFERENCES `student_academic_info` (`student_academic_info_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_history_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_academic_info`
--
ALTER TABLE `student_academic_info`
  ADD CONSTRAINT `student_academic_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_admission_class_id_foreign` FOREIGN KEY (`admission_class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_admission_section_id_foreign` FOREIGN KEY (`admission_section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_admission_session_id_foreign` FOREIGN KEY (`admission_session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_current_class_id_foreign` FOREIGN KEY (`current_class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_current_section_id_foreign` FOREIGN KEY (`current_section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_current_session_id_foreign` FOREIGN KEY (`current_session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_stream_id_foreign` FOREIGN KEY (`stream_id`) REFERENCES `streams` (`stream_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_academic_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_cheques`
--
ALTER TABLE `student_cheques`
  ADD CONSTRAINT `student_cheques_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_cheques_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`bank_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_cheques_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_cheques_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_documents`
--
ALTER TABLE `student_documents`
  ADD CONSTRAINT `student_documents_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `document_category` (`document_category_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_documents_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_documents_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_health_info`
--
ALTER TABLE `student_health_info`
  ADD CONSTRAINT `student_health_info_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_health_info_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_health_info_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_leaves`
--
ALTER TABLE `student_leaves`
  ADD CONSTRAINT `student_leaves_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_leaves_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_leaves_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `student_parents`
--
ALTER TABLE `student_parents`
  ADD CONSTRAINT `student_parents_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_parents_reference_admin_id_foreign` FOREIGN KEY (`reference_admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `student_parents_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `subjects`
--
ALTER TABLE `subjects`
  ADD CONSTRAINT `subjects_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subjects_co_scholastic_type_id_foreign` FOREIGN KEY (`co_scholastic_type_id`) REFERENCES `co_scholastic_types` (`co_scholastic_type_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subjects_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `subject_class_map`
--
ALTER TABLE `subject_class_map`
  ADD CONSTRAINT `subject_class_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_class_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_class_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_class_map_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_class_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `subject_section_map`
--
ALTER TABLE `subject_section_map`
  ADD CONSTRAINT `subject_section_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_section_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_section_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_section_map_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_section_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `subject_student_map`
--
ALTER TABLE `subject_student_map`
  ADD CONSTRAINT `subject_student_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_student_map_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_student_map_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_student_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_student_map_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subject_student_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `term_exams`
--
ALTER TABLE `term_exams`
  ADD CONSTRAINT `term_exams_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `term_exams_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `time_tables`
--
ALTER TABLE `time_tables`
  ADD CONSTRAINT `time_tables_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `time_tables_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `time_tables_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `time_tables_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `titles`
--
ALTER TABLE `titles`
  ADD CONSTRAINT `titles_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `titles_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `virtual_classes`
--
ALTER TABLE `virtual_classes`
  ADD CONSTRAINT `virtual_classes_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `virtual_classes_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `virtual_class_map`
--
ALTER TABLE `virtual_class_map`
  ADD CONSTRAINT `virtual_class_map_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `virtual_class_map_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `virtual_class_map_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `virtual_class_map_virtual_class_id_foreign` FOREIGN KEY (`virtual_class_id`) REFERENCES `virtual_classes` (`virtual_class_id`) ON DELETE CASCADE;

--
-- Constraints for table `visitors`
--
ALTER TABLE `visitors`
  ADD CONSTRAINT `visitors_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `visitors_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `wallet_ac`
--
ALTER TABLE `wallet_ac`
  ADD CONSTRAINT `wallet_ac_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wallet_ac_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wallet_ac_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wallet_ac_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wallet_ac_update_by_foreign` FOREIGN KEY (`update_by`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
