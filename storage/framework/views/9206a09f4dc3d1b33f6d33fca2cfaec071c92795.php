<?php $__env->startSection('content'); ?>
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }
      .modal-dialog {
  max-width: 550px !important;
}
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Daily Schedule</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/staff-menu/my-schedule'); ?>">My Schedule</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/staff-menu/my-schedule/daily-schedule'); ?>">Daily Schedule</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           <div class="body form-gap ">
                              <div class="headingcommon col-lg-12 show" style="padding: 15px 0px;">Daily Schedule:-</div>
                <div class="tab-pane body" id="schedule"">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;">
                          <div class="tab-pane active" id="classlist">
                            <div class="card border_info_tabs_1 card_top_border1">
                              <div class="body">
                                <div class="">
                               <!--  <div class="nav_item1">
                                  <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  Schedule Info </a>
                                </div> -->
                                <table class="table table-bordered tb1">
                                        <thead>
                                            <tr>
                                                <th width="100">Period no. / Date</th>
                                                <th>09-10-2018</th>
                                                <th>10-10-2018</th>
                                                <th>11-10-2018</th>
                                                <th>12-10-2018</th>
                                                <th>13-10-2018</th>
                                                <th>14-10-2018</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Period-1</td>
                                                <td class="data_table1">
                                                    07:30 AM - 08:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> I-A
                                                </td>
                                                <td class="data_table1">
                                                    07:30 AM - 08:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> I-A
                                                </td>
                                                
                                                <td>----</td>
                                                <td class="data_table1">
                                                    07:30 AM - 08:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> I-A
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                     07:30 AM - 08:00 AM <br>
                                                    <strong>Sub. :</strong> Economic <br>
                                                    <strong>Class :</strong> XII-A
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Period-2</td>
                                                <td class="data_table1">
                                                    08:00 AM - 08:30 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> II-A
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    08:00 AM - 08:30 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> II-A 
                                                </td>
                                                <td class="data_table1">
                                                    08:00 AM - 08:30 AM<br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> II-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    08:00 AM - 08:30 AM <br>
                                                    <strong>Sub. :</strong> Business <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Period-3</td>
                                                <td class="data_table1">
                                                    08:30 AM - 09:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> III-A
                                                     
                                                </td>
                                                 <td class="data_table1">
                                                    08:30 AM - 09:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> III-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    08:30 AM - 09:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                     08:30 AM - 09:00 AM  <br>
                                                    <strong>Sub. :</strong> Computer <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    08:30 AM - 09:00 AM <br>
                                                    <strong>Sub. :</strong> English <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>                                                <td>----</td>
                                            </tr>
                                            <tr>
                                                <td>Period-4</td>
                                                <td class="data_table1">
                                                    09:00 AM - 09:30 AM <br>
                                                    <strong>Sub. :</strong> Economic <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                   09:00 AM - 09:30 AM  <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    09:00 AM - 09:30 AM <br>
                                                    <strong>Sub. :</strong> Economic <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    09:00 AM - 09:30 AM <br>
                                                    <strong>Sub. :</strong> Account <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    09:00 AM - 09:30 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td>----</td>
                                            </tr>
                                            <tr>
                                                <td>Period-5</td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    10:00 AM - 10:30 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> X-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    10:00 AM - 10:30 AM <br>
                                                    <strong>Sub. :</strong> Account <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    10:00 AM - 10:30 AM <br>
                                                    <strong>Sub. :</strong> Business <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    10:00 AM - 10:30 AM <br>
                                                    <strong>Sub. :</strong> Account <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Period-6</td>
                                                 <td class="data_table1">
                                                    10:30 AM - 11:00 AM <br>
                                                    <strong>Sub. :</strong> Business <br>
                                                    <strong>Class :</strong> XI-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    10:30 AM - 11:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> X-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    10:30 AM - 11:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td>----</td>
                                            </tr>
                                            <tr>
                                                <td>Period-7</td>
                                                <td>----</td>
                                                 <td class="data_table1">
                                                    11:00 AM - 11:30 AM <br>
                                                    <strong>Sub. :</strong> Business <br>
                                                    <strong>Class :</strong> XI-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    11:00 AM - 11:30 AM <br>
                                                    <strong>Sub. :</strong> Economic <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td>----</td>
                                            </tr> 
                                            <tr>
                                                <td>Period-8</td>
                                                <td class="data_table1">
                                                    11:30 AM - 12:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    11:30 AM - 12:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                </td>
                                                
                                                <td class="data_table1">
                                                    11:30 AM - 12:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td>----</td>
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div> 
</section>
<div class="modal fade" id="RemarksModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Remark </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
           <div style="width:502px;border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;margin-left: 0px;">
            <!-- <div class="row">
             <div class="col-lg-5"><b>Class:</b> Class-1 - A</div>
             <div class="col-lg-7"><b>Subject:</b> Sub-1 - Sub-101</div></div>
             <div class="row">
             <div class="col-lg-5"><b>Enroll No:</b> 1001A1</div> 
             <div class="col-lg-7"><b>Student Name:</b> Ankit Dave</div></div> -->
             <div><b>Class:</b> Class-1 - A</div>
             <div><b>Subject:</b> Sub-1 - Sub-101</div>
             <div><b>Student Name:</b> Ankit Dave</div>
             </div>
              <div class="col-lg-12 padding-0">
                  <div class="form-group">
                    <lable class="from_one1" for="name"><b>Remark</b></lable>
                    <textarea class="form-control" name="remark" placeholder="Remark"></textarea>
                    
                  </div>
                </div>
                <hr>
                <div class="row">
                 <div class="col-lg-2">
                      <button type="submit" class="btn btn-raised btn-primary " title="Search">Save
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Cancel
                      </button>
                    </div>
                    </div>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>