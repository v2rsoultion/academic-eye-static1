<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MapHeadsToStudentController extends Controller
{
    public function viewMapStudent() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.map-heads-to-student.map_student_main')->with($data);
    }

    public function viewMapStudentDetails() {
    	$loginInfo   = get_loggedin_user_data();
    	$data                 = array(
            'login_info'      => $loginInfo
        );
    	
    	return view('admin-panel.map-heads-to-student.map_student')->with($data);
    }
}
