<?php $__env->startSection('content'); ?>
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>View Fees Head</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Transport</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/transport'); ?>">Manage Fees</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/transport/manage-fees/view-fees-head'); ?>">View Fees Head</a></li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            
                            <div class="body form-gap">
                                 <?php echo Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']); ?>

                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                <?php echo Form::text('name', old('name', ''), ['class' => 'form-control ','placeholder'=>'Name', 'id' => 'name']); ?>

                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                       
                                        <div class="col-lg-1 ">
                                          <button type="submit" class="btn btn-raised btn-primary"  title="Search">Search
                                          </button>
                                        </div>
                                         <div class="col-lg-1 ">
                                          <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                                          </button>
                                        </div>
                                    </div>
                                <?php echo Form::close(); ?>

                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Name</th>
                                                <th>KM Range</th>
                                                <th>Amount</th>
                                                <!-- <th>Action</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Transport Fees</td>
                                                <td>120-230</td>
                                                <td>500</td>
                                            </tr>
                                             <tr>
                                                <td>2</td>
                                                <td>Transport Fees</td>
                                                <td>120-230</td>
                                                <td>500</td>
                                             </tr> 
                                             <tr>
                                                <td>3</td>
                                                <td>Transport Fees</td>
                                                <td>120-230</td>
                                                <td>500</td>
                                            </tr>                                     
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>


<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            block_name: {
                validators: {
                        stringLength: {
                        min: 4,
                    },
                        notEmpty: {
                        message: 'Please fill required block name'
                    }
                }
            },
            hostel_name: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required hostel name'
                    }
                }
            },
            floor_no: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required floor no'
                    }
                }
            },
            room_no: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required room no'
                    }
                }
            },
            room_alias: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required room alias'
                    }
                }
            },
            capacity: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required capacity'
                    }
                }
            },
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>