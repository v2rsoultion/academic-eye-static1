<?php $__env->startSection('content'); ?>
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }
      .modal-dialog {
  max-width: 550px !important;
}
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>View Students</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/staff-menu/my-subjects'); ?>">My Subjects</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/staff-menu/my-subjects/subject-list/view-students'); ?>">View Students</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           <div class="body form-gap ">
                               <form>
                                    <div class="row clearfix">
                                      <div class="col-lg-3">
                                        <div class="form-group">
                                          <input type="text" name="roll_no" placeholder="Roll No" class="form-control">
                                        </div>
                                      </div>
                                       <div class="col-lg-3">
                                        <div class="form-group">
                                          <input type="text" name="enroll_no" placeholder="Enroll No" class="form-control">
                                        </div>
                                      </div>
                                      <div class="col-lg-3">
                                        <div class="form-group">
                                          <input type="text" name="name" placeholder="Student Name" class="form-control">
                                        </div>
                                      </div>
                                  
                                        <div class="col-lg-1 col-md-1">
                                          <button type="submit" class="btn btn-raised btn-primary" title="Search">Search
                                          </button>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <button type="submit" class="btn btn-raised btn-primary" title="Clear">Clear
                                          </button>
                                        </div>
                                    </div>
                                </form>
                                <hr>
                                 <div style="width:100%;border: 1px solid #ccc;border-radius: 5px;font-size:13px;padding: 5px 10px;margin-left: 0px;">
                                 <!--  <div class="col-lg-6"><b>Subject Name:</b> Sub-1 - Sub-101</div>
                                  <div class="col-lg-6"><b>Class:</b> Class-1 - A</div> -->
                                  <div><b>Subject Name:</b> Sub-1 - Sub-101</div>
                                  <div><b>Class:</b> Class-1 - A</div>
                                  </div>
                                  <hr>
                                <div class="table-responsive">
                                   
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                            <?php echo e(csrf_field()); ?>

                                <thead>
                                    <tr>
                                        <th><?php echo e(trans('language.s_no')); ?></th>
                                        <th>Roll No</th>
                                        <th>Enroll No</th>
                                        <th style="width: 250px">Student Name</th>
                                        <th>Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                          <td>1</td>
                                          <td>101</td>
                                          <td>100A01</td>
                                          <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">
                                           Ankit Dave
                                          </td>
                                          <td>
                                            <div class="dropdown">
                                              <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i class="zmdi zmdi-label"></i>
                                              <span class="zmdi zmdi-caret-down"></span>
                                              </button>
                                              <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                  <li> <a title="Add Remark" href="#" data-toggle="modal" data-target="#RemarksModel">Add Remark</a></li> 
                                                  <li> <a title="View Remark" href="<?php echo url('admin-panel/staff-menu/my-subjects/view-students/view-remark'); ?>">View Remark</a></li> 
                                              </ul> </div>
                                          </td>
                                        </tr>
                                          <tr>
                                          <td>2</td>
                                          <td>102</td>
                                          <td>100A02</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">
                                           Ankit Dave
                                            </td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="Add Remark" href="#" data-toggle="modal" data-target="#RemarksModel">Add Remark</a></li>
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff-menu/my-subjects/view-students/view-remark'); ?>">View Remark</a></li> 
                                                </ul> </div>
                                               </td>
                                            
                                        </tr>
                                        <tr>
                                          <td>3</td>
                                          <td>103</td>
                                          <td>100A03</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">
                                           Ankit Dave
                                            </td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="Add Remark" href="#" data-toggle="modal" data-target="#RemarksModel">Add Remark</a></li>
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff-menu/my-subjects/view-students/view-remark'); ?>">View Remark</a></li> 
                                                </ul> </div>
                                              </td>
                                        </tr>
                                        <tr>
                                          <td>4</td>
                                          <td>104</td>
                                          <td>100A04</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">
                                           Ankit Dave
                                            </td>
                                             
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="Add Remark" href="#" data-toggle="modal" data-target="#RemarksModel">Add Remark</a></li>
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff-menu/my-subjects/view-students/view-remark'); ?>">View Remark</a></li> 
                                                </ul> </div>
                                               </td>
                                        </tr>
                                       <tr>
                                          <td>5</td>
                                          <td>105</td>
                                          <td>100A05</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">
                                           Ankit Dave
                                            </td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="Add Remark" href="#" data-toggle="modal" data-target="#RemarksModel">Add Remark</a></li>
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff-menu/my-subjects/view-students/view-remark'); ?>">View Remark</a></li>  
                                                </ul> </div>
                                              </td>
                                        </tr>
                                          <tr>
                                          <td>6</td>
                                          <td>106</td>
                                          <td>100A06</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">
                                           Ankit Dave
                                            </td>
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                   <li> <a title="Add Remark" href="#" data-toggle="modal" data-target="#RemarksModel">Add Remark</a></li>
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff-menu/my-subjects/view-students/view-remark'); ?>">View Remark</a></li> 
                                                </ul> </div>
                                              </td>
                                        </tr>
                                          <tr>
                                          <td>7</td>
                                          <td>107</td>
                                          <td>100A07</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">
                                           Ankit Dave
                                            </td>
                                          
                                              <td><div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="Add Remark" href="#" data-toggle="modal" data-target="#RemarksModel">Add Remark</a></li>
                                                    <li> <a title="View Candidates" href="<?php echo url('admin-panel/staff-menu/my-subjects/view-students/view-remark'); ?>">View Remark</a></li> 
                                                </ul> </div>
                                              </td>
                                        </tr>
                                         
                                </tbody>
                            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>
<div class="modal fade" id="RemarksModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Remark </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
           <div style="width:502px;border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;margin-left: 0px;">
            <!-- <div class="row">
             <div class="col-lg-5"><b>Class:</b> Class-1 - A</div>
             <div class="col-lg-7"><b>Subject:</b> Sub-1 - Sub-101</div></div>
             <div class="row">
             <div class="col-lg-5"><b>Enroll No:</b> 1001A1</div> 
             <div class="col-lg-7"><b>Student Name:</b> Ankit Dave</div></div> -->
             <div><b>Class:</b> Class-1 - A</div>
             <div><b>Subject:</b> Sub-1 - Sub-101</div>
             <div><b>Student Name:</b> Ankit Dave</div>
             </div>
              <div class="col-lg-12 padding-0">
                  <div class="form-group">
                    <lable class="from_one1" for="name"><b>Remark</b></lable>
                    <textarea class="form-control" name="remark" placeholder="Remark"></textarea>
                    
                  </div>
                </div>
                <hr>
                <div class="row">
                 <div class="col-lg-2">
                      <button type="submit" class="btn btn-raised btn-primary " title="Search">Save
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Cancel
                      </button>
                    </div>
                    </div>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>