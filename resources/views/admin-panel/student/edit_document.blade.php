@extends('admin-panel.layout.header')

@section('content')
<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{!! trans('language.student_edit_document') !!}</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">{!! trans('language.student') !!}</a></li>
                    <li class="breadcrumb-item active">{!! trans('language.student_edit_document') !!}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div>
                    <div class="body">
                    {!! Form::open(['files'=>TRUE,'id' => 'edit-document-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                    @include('admin-panel.student._form_edit_document',['submit_button' => $submit_button])
                    {!! Form::close() !!}
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

