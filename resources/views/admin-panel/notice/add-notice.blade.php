@extends('admin-panel.layout.header')
@section('content')
<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Add Notice</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/notice-board') !!}">{!! trans('language.menu_notice_board') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/notice-board') !!}">Notice</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/notice-board/add-notice') !!}">Add Notice</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                    </div>
                    <div class="body form-gap">
                        <form class="" action="" method="" id="" style="width: 100%;">
                        <div class="row">
                            <div class="col-lg-4">
                              <div class="form-group">
                                <lable class="from_one1" for="name">Title</lable>
                                <input type="text" name="title_name" id="title" class="form-control" placeholder="Title">
                              </div>
                            </div>
                            <div class="col-lg-8">
                              <lable class="from_one1">Description</lable>
                              <div class="form-group">
                                <textarea class="form-control" placeholder="Description"></textarea>
                              </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <lable class="from_one1">Class :</lable>
                                <div class="form-group">
                                    <div class="radio" style="margin-top:6px !important;">
                                        <label name= "radio12" onclick="classForSelected('0')" class="document_staff">All</label>
                                        <input type="radio" name="all" value='0' id="radio12">

                                        <label name= "radio2" onclick="classForSelected('1')" class="document_staff">Selected</label>
                                        <input type="radio" name="selected" value='1' id="radio2">
                                    </div>
                                </div>
                            </div> 
                            <div class="col-lg-3" id="classBlock" style="display: none;">
                               <div class="form-group">
                               <lable class="from_one1">Class :</lable>
                               <select class="form-control show-tick select_form1" name="" id="" multiple="multiple">
                                   <option value="">Select Class</option>
                                   <option value="1">Class-1</option>
                                   <option value="2">Class-2</option>
                                   <option value="3">Class-3</option>
                                   <option value="4">Class-4</option>
                               </select>
                               </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <lable class="from_one1">Student :</lable>
                                <div class="form-group">
                                    <div class="radio" style="margin-top:6px !important;">
                                        <label name= "radio12" onclick="studentForSelected('0')" class="document_staff">All</label>
                                        <input type="radio" name="all" value='0' id="radio12">

                                        <label name= "radio2" onclick="studentForSelected('1')" class="document_staff">Groups</label>
                                        <input type="radio" name="groups" value='1' id="radio2">
                                    </div>
                                </div>
                            </div> 
                            <div class="col-lg-3" id="studentBlock" style="display: none;">
                               <div class="form-group">
                               <lable class="from_one1">Student Group :</lable>
                               <select class="form-control show-tick select_form1" name="" id="" multiple="multiple">
                                   <option value="">Select Student Group</option>
                                   <option value="1">SGroup-1</option>
                                   <option value="2">SGroup-2</option>
                                   <option value="3">SGroup-3</option>
                                   <option value="4">SGroup-4</option>
                               </select>
                               </div>
                            </div>
                        </div>

                         <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <lable class="from_one1">Parent :</lable>
                                <div class="form-group">
                                    <div class="radio" style="margin-top:6px !important;">
                                        <label name= "radio12" onclick="parentForSelected('0')" class="document_staff">All</label>
                                        <input type="radio" name="all" value='0' id="radio12">

                                        <label name= "radio2" onclick="parentForSelected('1')" class="document_staff">Groups</label>
                                        <input type="radio" name="groups" value='1' id="radio2">
                                    </div>
                                </div>
                            </div> 
                            <div class="col-lg-3" id="parentBlock" style="display: none;">
                               <div class="form-group">
                               <lable class="from_one1">Parent Group :</lable>
                               <select class="form-control show-tick select_form1" name="" id="" multiple="multiple">
                                   <option value="">Select Parent Group</option>
                                   <option value="1">PGroup-1</option>
                                   <option value="2">PGroup-2</option>
                                   <option value="3">PGroup-3</option>
                                   <option value="4">PGroup-4</option>
                               </select>
                               </div>
                            </div>
                        </div>
                          <hr>
                          <div class="row">
                              <div class="col-lg-1">
                                  <button type="submit" class="btn btn-raised btn-primary" title="Save">Save</button>
                              </div>
                              <div class="col-lg-1">
                                  <button type="reset" class="btn btn-raised btn-primary" title="Cancel">Cancel</button>
                              </div>
                          </div>
                        </form>

                   </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection     

<script type="text/javascript">
  function classForSelected(val) {
        var x = document.getElementById("classBlock");
        if(val == 0) {
            if (x.style.display === "none") {
                x.style.display = "none";
            } else {
                x.style.display = "none";
            }
        } else {
            if (x.style.display === "block") {
                x.style.display = "block";
            } else {
                x.style.display = "block";
            }
        }
    }

    function studentForSelected(val) {
        var x = document.getElementById("studentBlock");
        if(val == 0) {
            if (x.style.display === "none") {
                x.style.display = "none";
            } else {
                x.style.display = "none";
            }
        } else {
            if (x.style.display === "block") {
                x.style.display = "block";
            } else {
                x.style.display = "block";
            }
        }
    }

     function parentForSelected(val) {
        var x = document.getElementById("parentBlock");
        if(val == 0) {
            if (x.style.display === "none") {
                x.style.display = "none";
            } else {
                x.style.display = "none";
            }
        } else {
            if (x.style.display === "block") {
                x.style.display = "block";
            } else {
                x.style.display = "block";
            }
        }
    }

</script>