<option value="">Select Brochure</option>
@if(!empty($brochures))
  @foreach($brochures as $key => $value)
    <option value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif