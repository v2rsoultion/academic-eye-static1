<?php

namespace App\Model\InventoryVendor;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'inv_vendor';
    protected $primaryKey = 'vendor_id';
}
