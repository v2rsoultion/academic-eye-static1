@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Accounts Heads</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/account') !!}">Accounts</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/account/manage-accounts-head') !!}">Accounts Heads</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card" style="margin-bottom: 20px;">
              <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body form-gap1">
               
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Add Accounts Heads :-</div> -->
                  <form class="" action="" id="manage_vendor" style="width: 100%;">
                    <div class="row" >
                      <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Main Heads</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Head</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Head- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div>
                      </div>
                         <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Sub Heads</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Subhead</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Subhead- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Group</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Group</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Group- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div>
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Head Name</lable>
                          <input type="text" name="head_name" id="" class="form-control" placeholder="Head Name">
                        </div>
                         @if ($errors->has('group_name')) <p class="help-block">{{ $errors->first('group_name') }}</p> @endif
                      </div>
                    </div>
                      <!-- <hr> -->
                      <div class="row">
                      <div class="col-lg-1 ">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
                        </button>
                      </div>
                      <div class="col-lg-1 ">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn  " title="Cancel">Cancel
                        </button>
                      </div>
                    
                    </div>
                  </form>
                </div>
              </div>
                  <!-- <hr> -->
                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
              <div class="card">
                <div class="body form-gap"> 
                  <div class="headingcommon  col-lg-12" style="margin-left: -13px">Search By :-</div>
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-2">
                        <div class="form-group">
                            <lable class="from_one1">Main Heads</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Head</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Head- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div>
                      </div>
                         <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Sub Heads</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Subhead</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Subhead- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                            <lable class="from_one1">Group</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Group</option>
                              <?php $count = 1; for ($i=0; $i < 3 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Group- <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div>
                      </div>
                       <div class="col-lg-2">
                        <div class="form-group">
                          <lable class="from_one1">Head Name</lable>
                          <input type="text" name="head_name" id="" class="form-control" placeholder="Head Name">
                        </div>
                      </div>
                     <!-- </div> -->
                     <!-- <hr> -->
                     <!-- <div class="row"> -->
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Clear">Clear
                        </button>
                      </div>
                    <!-- </div> -->
                  </form>
                
                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <hr>
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Main Head</th>
                        <th>Sub Head</th>
                        <th>Groups</th>
                        <th>Head Name</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                       
                      <tr>
                        <td>1</td>
                        <td>Head- 1</td>
                        <td>Subhead- 1</td>
                        <td>Group-1</td>
                        <td>H1</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Head- 2</td>
                        <td>Subhead- 2</td>
                        <td>Group-2</td>
                        <td>H2</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Head- 1</td>
                        <td>Subhead- 3</td>
                        <td>Group-3</td>
                        <td>H3</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>Head- 4</td>
                        <td>Subhead- 4</td>
                        <td>Group-4</td>
                        <td>H4</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>Head- 2</td>
                        <td>Subhead- 3</td>
                        <td>Group-5</td>
                        <td>H5</td>
                        <td class="text-center">
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->


@endsection