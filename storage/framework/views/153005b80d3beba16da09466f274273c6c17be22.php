
<?php $__env->startSection('content'); ?>
<style type="text/css">
  /*td{
  padding: 14px 10px !important;
  }*/
  
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-12">
        <h2>Add Schedule</h2>
      </div>
      <div class="col-lg-8 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/examination'); ?>">Examination And Report Cards</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/examination/exam-schedule/add'); ?>">Exam Schedule</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/examination/exam-schedule/add-schedule'); ?>">Add Schedule</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
                <div class="header">
                    <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
                </div>
              <div class="body form-gap" style="margin-bottom: 40px;">
                
                  <form class="searchpanel11" action="" method=""  id="" style="width: 100%;">
                    <div class="row">
                       <div class="col-lg-3">
                         <div class="form-group">
                          <lable class="from_one1">Schedule Name</lable>
                            <input type="text" name="schedule_name" placeholder="Schedule Name" class="form-control">
                         </div>
                      </div>
                       <div class="col-lg-3">
                         <div class="form-group">
                          <lable class="from_one1">Exam Name</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Exam</option>
                              <option value="1">Exam-1</option>
                              <option value="2">Exam-2</option>
                              <option value="3">Exam-3</option>
                              <option value="4">Exam-4</option>
                            </select>
                         </div>
                      </div>
                    </div>
                     <!--   <div class="col-lg-3">
                         <div class="form-group">
                          <lable class="from_one1">Class Name</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Class</option>
                              <option value="1">Class-1</option>
                              <option value="2">Class-2</option>
                              <option value="3">Class-3</option>
                              <option value="4">Class-4</option>
                            </select>
                         </div>
                      </div>
                       <div class="col-lg-3">
                         <div class="form-group">
                          <lable class="from_one1">Section</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Section</option>
                              <option value="A">A</option>
                              <option value="B">B</option>
                              <option value="C">C</option>
                              <option value="D">D</option>
                            </select>
                         </div>
                      </div>
                    </div> -->
                   
                  
                    <!-- <div class="row">
                         <div class="col-lg-2">
                        <lable class="from_one1">Start Date :</lable>
                        <div class="form-group">
                          <input type="text" name="start_schedule_date" class="form-control" id="start_schedule_date" placeholder="Start Date">
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <lable class="from_one1">End Date :</lable>
                        <div class="form-group">
                          <input type="text" name="end_schedule_date" class="form-control" id="end_schedule_date" placeholder="End Date">
                        </div>
                      </div>
                    </div> -->
                   <!--  <hr>
                    <div id="table-wrapper">
                      <div class="table-scroll">
                  
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    <?php echo e(csrf_field()); ?>

                      <thead>
                        <tr>
                          <th style="width: 60px">S No</th>
                          <th style="width: 230px">Subject</th>
                          <th  class="text-center">Teacher</th>
                          <th  class="text-center">Date</th>
                          <th class="text-center">Time Range</th>
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 10; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td>Subject-<?php echo $counter; ?> - Sub-10<?php echo $counter; ?></td>
                          <td>
                          <div class="form-group">
                            <select class="form-control show-tick select_form1 select2" name="" id="teacher-"+$counter>
                              <option value="">Select Teacher</option>
                              <option value="1">Teacher-1</option>
                              <option value="2">Teacher-2</option>
                              <option value="3">Teacher-3</option>
                              <option value="4">Teacher-4</option>
                            </select>
                          </div></td>
                          <td> <div class="form-group"> 
                          <input type="text" name="schedule_date" class="form-control schedule_date" id="schedule_date<?php echo $counter; ?>" placeholder="Date"> </div></td>
                          <td> 
                          <div class="col-lg-5 float-left">
                            <div class="form-group">
                            <input type="text" name="start_schedule_time" placeholder="Start Time" class="form-control">
                            </div>
                          </div>
                          <div class="col-lg-5 float-left">
                            <div class="form-group">
                            <input type="text" name="end_schedule_time" placeholder="End Time" class="form-control">
                            </div>
                          </div>
                          </td> -->
                          <!-- <td><div class="col-lg-6 float-left">
                            <div class="form-group">
                          <input type="text" name="start_schedule_time" class="form-control" id="start_schedule_time" placeholder="Start Time"></div></div>
                          <div class="col-lg-6 float-left">
                            <div class="form-group">
                          <input type="text" name="end_schedule_time" class="form-control" id="end_schedule_time" placeholder="End Time"></div></div>
                          </td> -->
                       <!--  </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                </div>
              </div> -->
                    
                   
                          <hr>
                         <div class="row">
                     <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary" title="Save">Save</button>
                      </div>
                      <div class="col-lg-1">
                        <button class="btn btn-raised btn-primary" title="Cancel">Cancel
                        </button>
                      </div>
                    </div>
                        </form>
                
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    
<!-- <input type="text" id="timepicker-12-hr" name="timepicker-12-hr" class="timepicker-12-hr hasWickedpicker" onkeypress="return false;" aria-showingpicker="false" tabindex="0">
         -->
</section>
<!-- Content end here  -->


<link rel="stylesheet" type="text/css" href="http://ericjgagnon.github.io/wickedpicker/wickedpicker/wickedpicker.min.css">
<script type="text/javascript" src="http://ericjgagnon.github.io/wickedpicker/javascript/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="http://ericjgagnon.github.io/wickedpicker/wickedpicker/wickedpicker.min.js"></script>
<script type="text/javascript">
            var twelveHour = $('.timepicker-12-hr').wickedpicker();
            $('.time').text('//JS Console: ' + twelveHour.wickedpicker('time'));
            $('.timepicker-24-hr').wickedpicker({twentyFour: true});
            $('.timepicker-12-hr-clearable').wickedpicker({clearable: true});
        </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>