@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 14px 10px !important;
  }
  .nav-tabs>.nav-item>.nav-link {
    width: 100px;
    margin-right: 5px;
        border: 1px solid #ccc !important;
  }
  #tabingclass .nav-tabs>.nav-item>.nav-link {
    border-radius: 4px !important;
    padding: 8px 25px;
  }
   #tabingclass .nav-link:hover {
        background: #6572b8 !important;
    color: #fff !important;
}
#tabingclass .nav-link:active {
        background: #6572b8 !important;
    color: #fff !important;
}
.idi .nav-tabs>.nav-item>.nav-link.active {
  background: #6572b8 !important;
    color: #fff !important;
}
.modal-dialog {
  max-width: 550px !important;
}
.checkbox {
  float: left;
  margin-right: 20px;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>My Homework</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student/view-homework') !!}">My Homework</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 20px !important;">
                 
                     <form class="" action="" method=""  id="" style="width: 100%;">
                      <div class="row">
                   <div class="col-lg-3">
                    <div class="form-group">
                      <!-- <lable class="from_one1" for="name">Subject</lable> -->
                      <select class="form-control show-tick select_form1" name="classes" id="">
                        <option value="">Subject</option>
                        <option value="A">Subject-1</option>
                        <option value="B">Subject-2</option>
                        <option value="C">Subject-3</option>
                        <option value="D">Subject-4</option>
                      </select>
                    </div>
                  </div>
                     <div class="col-lg-3">
                    <div class="form-group">
                      <!-- <lable class="from_one1" for="name">Teacher</lable> -->
                      <select class="form-control show-tick select_form1" name="classes" id="">
                        <option value="">Teacher</option>
                        <option value="A">Teacher-1</option>
                        <option value="B">Teacher-2</option>
                        <option value="C">Teacher-3</option>
                        <option value="D">Teacher-4</option>
                      </select>
                    </div>
                  </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                         <input type="text" name="date" id="taskDate" placeholder="Date" class="form-control">
                        </div>
                      </div>
                     <div class="col-lg-1">
                      <button type="submit" class="btn btn-raised btn-primary " title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Clear
                      </button>
                    </div>
                  
                    </div>
                   </form>
                    <!--  DataTable for view Records  -->
                     <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                      <thead>
                        <tr>
                          <th>S No</th>
                          <!-- <th>Class-Section</th> -->
                          <th>Subject</th>
                          <th>Teacher</th>
                          <th>Date</th>
                          <th>HomeWork Description</th>
                          
                          <!-- <th>Action</th> -->
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 10; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <!-- <td>Class-1 - A</td> -->
                          <td>Subject-<?php echo $counter; ?></td>
                          <td>Teacher-<?php echo $counter; ?></td>
                          <td>09-10-2018</td>
                          <td style="width: 250px;">Lesson-1 Question Answer in written</td>
                           
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->
@endsection

<!-- model -->
<div class="modal fade" id="sendMessageModel" tabindex="-1" role="dialog" style="padding: 40px;">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content send_messsage_button1">
            <div class="header header_model_2">
                <div class="header_model_1">
                    <h6 style="padding-left:20px;"><strong>Send Message</strong></h6>
                </div>
                <ul class="header-dropdown" style="padding-right: 20px; ">
                    <li class="remove remove_cross_x_2 float-right" style="position: relative; top: 20px;">
                        <a role="button" class="button_1" data-dismiss="modal"><i class="zmdi zmdi-close zmdi_close1"></i></a>
                    </li>
                </ul>
            
            </div>
          <div class="tab-pane active" id="classlist">
              <div class="card form-gap" style="padding: 20px 40px;">
                  <div class="body">
                        <form class="" action="" method=""  id="">
                            <div class="row clearfix">
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                    <lable class="from_one1">Subject :</lable>
                                    <div class="form-group">
                                       <input type="text" name="subject" class="form-control" placeholder="Subject">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <lable class="from_one1">Message :</lable>
                                    <div class="form-group">
                                       <textarea class="form-control" class="form-control" placeholder="Message"></textarea>
                                    </div>
                                </div>
                            </div>
                          
                            <div class="row clearfix">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-raised  btn-primary waves-effect float-right" data-dismiss="modal">Cancel</button>    
                                    <button type="submit" class="btn btn-raised  btn-primary float-right" id="send_msg" onclick="showMessage()">Send</button>
                                </div>
                            </div>
                        </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>