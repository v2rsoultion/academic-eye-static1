@extends('admin-panel.layout.header')
@section('content')
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Free By Management</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12">
        <ul class="breadcrumb float-md-right">
           <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item">{!! trans('language.menu_fee_collection') !!}</li>
          <li class="breadcrumb-item"><a href="fees-collection1.php"> Free by management</a>
          </li>
          <li class="breadcrumb-item"><a href="javascript:void(0);">View  free by management</a>
          </li>
          
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                <div class="row tabless">
                  <form class="" action="" method="post" id="searchpanel" style="width: 100%;">
                    <div class="row">
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-2">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Class</lable>
                          <select class="form-control show-tick select_form1" name="classes" id="">
                            <option value="">Class</option>
                            <option value="1">1st</option>
                            <option value="2">2nd</option>
                            <option value="3">3rd</option>
                            <option value="4">4th</option>
                            <option value="10th">10th</option>
                            <option value="11th">11th</option>
                            <option value="12th">12th</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                          <lable class="from_one1" for="name">Section</lable>
                          <select class="form-control show-tick select_form1" name="classes" id="">
                            <option value="">Section</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-1 ">
                      <button type="submit" class="btn btn-raised btn-round btn-primary" style="margin-top: 20px !important;" title="Submit">Search
                      </button>
                    </div>
                     <div class="col-lg-1 ">
                      <button type="reset" class="btn btn-raised btn-round btn-primary" style="margin-top: 20px !important;" title="Clear">Clear
                      </button>
                    </div>
                    
                    </div>
                  </form>
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <table class="table table-striped m-b-0 c_list">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Roll No</th>
                        <th>Enroll No</th>
                        <th>Student Name</th>
                        <th>Father Name</th>
                        <th>Fee Particular</th>
                        <th>Payable Fees</th>
                        <th>Free Fees</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $count= 1; for ($i=0; $i < 15; $i++) { ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td>156895</td>
                        <td>ABC20156</td>
                        <td>
                          <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">Ankit Dave
                        </td>
                        <td>Akash Dave</td>
                        <td>Tution Fees</td>
                        <td>22451</td>
                        <td>2589</td>
                        <td class="text-center">
                          <ul class="opendiv">
                            <button type="button" data-backdrop="static" data-keyboard="false" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter" style="background: #6572b8 !important;">
                            Map heads
                            </button>
                          </ul>
                        </td>
                      </tr>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td>156895</td>
                        <td>ABC20156</td>
                        <td>
                          <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="15%">Ankit Dave
                        </td>
                        <td>Akash Dave</td>
                        <td>Exam Fees</td>
                        <td>22451</td>
                        <td>2589</td>
                        <td class="text-center">
                          <ul class="opendiv">
                            <button type="button" data-backdrop="static" data-keyboard="false" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter" style="background: #6572b8 !important;">
                            Map heads
                            </button>
                          </ul>
                        </td>
                      </tr>
                      <?php $count++;} ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Action Model  -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Recurring Heads</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row tabless">
          <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
          <div class="col-lg-12">
            <div class="form-group">
              <select class="form-control show-tick select_form1" name="classes" id="" multiple="multiple">
                <option value="">Select</option>
                <option value="1">Tution Fees(July-Sept)</option>
                <option value="1">Tution Fees(Oct-Dec)</option>
                <option value="1">Exam Fees</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>
@endsection
<script type="text/javascript">
  $('#dropbtn').on('click', 'dropbtnAction', function() {
      $(this).toggleClass('active');
  });
</script>