
<?php $__env->startSection('content'); ?>
<style type="text/css">
   .table-responsive {
    overflow-x: auto !important;
   }
   /*.card .body .table td, .cshelf1 {
    width: 100px !important;
   }*/
   td{
    padding: -1px !important;
   }
</style>
<!--  Main content here -->
<section class="content">
   <div class="block-header">
      <div class="row">
         <div class="col-lg-5 col-md-6 col-sm-12">
            <h2>Classwise Admission Report</h2>
         </div>
         <div class="col-lg-7 col-md-6 col-sm-12 line">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>">Reports</a></li>
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/report/view-reports'); ?>">View Reports</a></li>
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/report/view-reports/student-sheet-report'); ?>">Classwise Admission Report</a></li>
            </ul>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row clearfix">
         <div class="col-lg-12" id="bodypadd">
            <div class="tab-content">
               <div class="tab-pane active" id="classlist">
                  <div class="card">
                  <div class="body form-gap">
                   <!-- <div class="row"> -->
                  
                  <div class="table-responsive position">
                  <!--  DataTable for view Records  -->
                  <table class="table  m-b-0 c_list table-bordered text-center" id="classwise-admission-report" style="width: 100%;">
                    <thead class="bold">
                    <tr>
                      <td>Class Section</td>
                      <td>Board of Education</td>
                      <td>Medium</td>
                      <td>No of Intake</td>
                      <td>No of Applicant</td>
                      
                    </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1 A</td>
                        <td>RBSE</td>
                        <td>HINDI</td>
                        <td>25</td>
                        <td>12</td>
                      </tr>
                      <tr>
                        <td>1 B</td>
                        <td>RBSE</td>
                        <td>HINDI</td>
                        <td>25</td>
                        <td>12</td>
                      </tr>
                      <tr>
                        <td>1 C</td>
                        <td>RBSE</td>
                        <td>HINDI</td>
                        <td>25</td>
                        <td>12</td>
                      </tr>
                      <tr>
                        <td>1 A</td>
                        <td>CBSE</td>
                        <td>ENGLISH</td>
                        <td>25</td>
                        <td>18</td>
                      </tr> 
                      <tr>
                        <td>2 A</td>
                        <td>RBSE</td>
                        <td>HINDI</td>
                        <td>25</td>
                        <td>22</td>
                      </tr> 
                      <tr>
                        <td>2 A</td>
                        <td>CBSE</td>
                        <td>ENGLISH</td>
                        <td>25</td>
                        <td>25</td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td>150</td>
                        <td>101 (67.34%) </td>
                      </tr>
                    </tfoot>
                  </table>
              </div>          
             </div>
            </div>
           </div>
          </div>
        </div>
       </div>
      </div>
  
</section>
<!-- Content end here  -->

<script type="text/javascript">
  $(document).ready(function () {
        var table = $('#classwise-admission-report').DataTable({
          fixed: {
            header: true,
            footer: true
        },
           dom: 'Blfrtip',
             buttons: [{ 
              extend: 'csvHtml5',
              footer: true,
              exportOptions: {
              columns: ':visible' }
            }],
        });
    });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>