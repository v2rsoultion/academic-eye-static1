<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'students';
    protected $primaryKey = 'student_id';
    public function up()
    {
        if (!Schema::hasTable('students')) { 
            Schema::create('students', function (Blueprint $table) {
                $table->increments('student_id')->unsigned();
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('update_by')->unsigned()->nullable();
                $table->integer('reference_admin_id')->unsigned()->nullable();
                $table->integer('student_parent_id')->unsigned()->nullable();

                $table->string('student_enroll_number', 255)->nullable();
                $table->string('student_roll_no', 255)->nullable();
                $table->string('student_name', 255)->nullable();
                $table->text('student_image')->nullable();
                $table->date('student_reg_date')->nullable();
                $table->date('student_dob')->nullable();
                $table->string('student_email', 255)->nullable();
                $table->tinyInteger('student_gender')->default(0)->comment = '0=Boy,1=Girl';
                $table->tinyInteger('student_type')->default(0)->comment = '0=PaidBoy,1=RTE,2=FREE';
                $table->tinyInteger('medium_type')->default(1)->comment = '0=Hindi,1=English';
                
                $table->string('student_category', 255)->nullable();
                $table->integer('caste_id')->unsigned()->nullable();
                $table->integer('religion_id')->unsigned()->nullable();
                $table->integer('nationality_id')->unsigned()->nullable();
                $table->string('student_sibling_name', 255)->nullable();
                $table->integer('student_sibling_class_id')->unsigned()->nullable();
                $table->string('student_adhar_card_number', 255)->nullable();

                $table->string('student_privious_school',255)->nullable();
                $table->string('student_privious_class',255)->nullable();
                $table->string('student_privious_tc_no',255)->nullable();
                $table->date('student_privious_tc_date')->nullable();
                $table->text('student_privious_result')->nullable();

                $table->text('student_temporary_address')->nullable();
                $table->integer('student_temporary_city')->unsigned()->nullable();
                $table->integer('student_temporary_state')->unsigned()->nullable();
                $table->integer('student_temporary_county')->unsigned()->nullable();
                $table->string('student_temporary_pincode', 20)->nullable();

                $table->text('student_permanent_address')->nullable();
                $table->integer('student_permanent_city')->unsigned()->nullable();
                $table->integer('student_permanent_state')->unsigned()->nullable();
                $table->integer('student_permanent_county')->unsigned()->nullable();
                $table->string('student_permanent_pincode', 20)->nullable();
                $table->tinyInteger('student_status')->default(1)->comment = '0=Leaved,1=Studying';
                $table->timestamps();
            });

            Schema::table('students', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('students', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('students', function($table) {
                $table->foreign('reference_admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('students', function($table) {
                $table->foreign('student_parent_id')->references('student_parent_id')->on('student_parents')->onDelete('cascade');
            });
            Schema::table('students', function($table) {
                $table->foreign('student_sibling_class_id')->references('class_id')->on('classes')->onDelete('cascade');
            });
            Schema::table('students', function($table) {
                $table->foreign('caste_id')->references('caste_id')->on('caste');
            });
            Schema::table('students', function($table) {
                $table->foreign('religion_id')->references('religion_id')->on('religions');
            });
            Schema::table('students', function($table) {
                $table->foreign('nationality_id')->references('nationality_id')->on('nationality');
            });

            Schema::table('students', function($table) {
                $table->foreign('student_permanent_county')->references('country_id')->on('country');
            });
            Schema::table('students', function($table) {
                $table->foreign('student_permanent_state')->references('state_id')->on('state');
            });
            Schema::table('students', function($table) {
                $table->foreign('student_permanent_city')->references('city_id')->on('city');
            });

            Schema::table('students', function($table) {
                $table->foreign('student_temporary_county')->references('country_id')->on('country');
            });
            Schema::table('students', function($table) {
                $table->foreign('student_temporary_state')->references('state_id')->on('state');
            });
            Schema::table('students', function($table) {
                $table->foreign('student_temporary_city')->references('city_id')->on('city');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
