@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2>{!! trans('language.menu_online_content') !!}</h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/student/online-content') !!}">{!! trans('language.menu_online_content') !!}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/OnlineContent/Notes.svg') !!}" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Notes</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="{{ url('admin-panel/student/online-content/notes/view-notes') }}" style="width: 48%; margin: 15px auto;" class="cusa" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                       
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <!--  <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="{!! URL::to('public/assets/images/OnlineContent/QuestionPaper.svg') !!}" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Question Paper</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                               
                                <a href="{{ url('/admin-panel/question-paper/view-question-paper') }}" class="cusa" style="width: 48%; margin: 15px auto;" title="View ">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </section>
@endsection