<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffClassAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table      = 'staff_class_allocations';
    protected $primaryKey = 'staff_class_allocation_id';
    public function up()
    {
        if (!Schema::hasTable('staff_class_allocations')) {
            Schema::create('staff_class_allocations', function (Blueprint $table) {
                $table->increments('staff_class_allocation_id');
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->integer('class_id')->unsigned()->nullable();
                $table->integer('section_id')->unsigned()->nullable();
                $table->integer('staff_id')->unsigned()->nullable();
                $table->timestamps();
            });
            Schema::table('staff_class_allocations', function($table) {
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('staff_class_allocations', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('staff_class_allocations', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
            Schema::table('staff_class_allocations', function($table) {
                $table->foreign('class_id')->references('class_id')->on('classes')->onDelete('cascade');
            });
            Schema::table('staff_class_allocations', function($table) {
                $table->foreign('section_id')->references('section_id')->on('sections')->onDelete('cascade');
            });
            Schema::table('staff_class_allocations', function($table) {
                $table->foreign('staff_id')->references('staff_id')->on('staff')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_class_allocations');
    }
}
