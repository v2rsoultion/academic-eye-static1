<?php if(isset($competition['competition_id']) && !empty($competition['competition_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?>

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
    .list_dropdown1{
    z-index: 999999 !important;
    }
   
</style>

<?php echo Form::hidden('competition_id',old('competition_id',isset($competition['competition_id']) ? $competition['competition_id'] : ''),['class' => 'gui-input', 'id' => 'competition_id', 'readonly' => 'true']); ?>


<p class="red">
<?php if($errors->any()): ?>
    <?php echo e($errors->first()); ?>

<?php endif; ?>
</p>
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <!-- Name -->
        <lable class="from_one1"><?php echo trans('language.competitions_name'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('competition_name', old('competition_name',isset($competition['competition_name']) ? $competition['competition_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.competitions_name'), 'id' => 'competition_name']); ?>

        </div>
        <?php if($errors->has('competition_name')): ?> <p class="help-block"><?php echo e($errors->first('competition_name')); ?></p> <?php endif; ?>
    </div>        
    <div class="col-lg-4 col-md-4">
        <!-- Date -->
        <lable class="from_one1"><?php echo trans('language.competitions_date'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('competition_date', old('competition_date',isset($competition['competition_date']) ? $competition['competition_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.competitions_date'), 'id' => 'competition_date']); ?>

        </div>
        <?php if($errors->has('competition_date')): ?> <p class="help-block"><?php echo e($errors->first('competition_date')); ?></p> <?php endif; ?>
    </div>    
    <div class="col-lg-4 col-md-4">
        <!-- Issue Participation Certificate -->
        <label class="from_one1"><?php echo trans('language.Issue_certificate'); ?> :</label>
        <div class="form-group">
            <div class="radio">

                <?php $certificate_yes = ''; ?>
                    <?php if(isset($competition['competition_issue_participation_certificate']) && $competition['competition_issue_participation_certificate'] == 1): ?>
                       <?php $certificate_yes = 'checked'; ?>
                    <?php endif; ?>

                    <?php $certificate_no = ''; ?>
                    
                    <?php if(isset($competition['competition_issue_participation_certificate']) && $competition['competition_issue_participation_certificate'] == 0): ?>
                        <?php $certificate_no = 'checked'; ?>
                    <?php endif; ?>

                <?php echo Form::radio('competition_issue_participation_certificate','1',$certificate_yes,['class' => 'form-control','id'=>'Issue_certificate_yes']); ?>

                <label for="Issue_certificate_yes"><?php echo trans('language.Issue_certificate_yes'); ?></label>
                <?php echo Form::radio('competition_issue_participation_certificate','0',$certificate_no,['class' => 'form-control','id'=>'Issue_certificate_no']); ?>

                <label for="Issue_certificate_no"><?php echo trans('language.Issue_certificate_no'); ?></label>
            </div>
        </div>
        <?php if($errors->has('competition_issue_participation_certificate')): ?> <p class="help-block"><?php echo e($errors->first('competition_issue_participation_certificate')); ?></p> <?php endif; ?> 
    </div>
    
    <div class="col-lg-4 col-md-4">
        <!-- Level -->
        <label class="from_one1"><?php echo trans('language.competitions_level'); ?> :</label>
        <div class="form-group">
            <div class="radio">
                <?php $select_class = ''; ?>
                    <?php if(isset($competition['competition_level']) && $competition['competition_level'] == 1): ?>
                       <?php $select_class = 'checked'; ?>
                    <?php endif; ?>
                    <?php $select_school = ''; ?>
                    <?php if(isset($competition['competition_level']) && $competition['competition_level'] == 0): ?>
                        <?php $select_school = 'checked'; ?>
                    <?php endif; ?>

                <?php echo Form::radio('competition_level','0',$select_school,['class' => 'form-control','id'=>'competitions_level_school','onClick'=>'display_div()']); ?>

                <label for="competitions_level_school"><?php echo trans('language.competitions_level_school'); ?></label>
                <?php echo Form::radio('competition_level','1',$select_class,['class' => 'form-control','id'=>'competitions_level_class','onClick'=>'hide_div()']); ?>

                <label for="competitions_level_class"><?php echo trans('language.competitions_level_class'); ?></label>
            </div>
        </div>
        <?php if($errors->has('competition_level')): ?> <p class="help-block"><?php echo e($errors->first('competition_level')); ?></p> <?php endif; ?> 
        
    </div>
    <div class="col-lg-8 col-md-8" id="comp_class">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <lable class="from_one1"><?php echo trans('language.medium_type'); ?>  :</lable>
                <div class="form-group m-bottom-0">
                    <label class=" field select" style="width: 100%">
                        <?php echo Form::select('medium_type', $competition['arr_medium'],isset($competition['medium_type']) ? $competition['medium_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type', 'onChange'=>'getClass(this.value)']); ?>

                        <i class="arrow double"></i>
                    </label>
                </div>
                <?php if($errors->has('medium_type')): ?> <p class="help-block"><?php echo e($errors->first('medium_type')); ?></p> <?php endif; ?>
            </div>
            <?php 
                $temp_class_id_arr = array();
                if(isset($competition['competition_class_ids'])){
                    $temp_class_id_arr = explode(',',$competition['competition_class_ids']);
                }
            ?>
            <div class="col-lg-6 col-md-6">
                <lable class="from_one1"><?php echo trans('language.competitions_level_classes'); ?>  :</lable>
                <div class="form-group m-bottom-0">
                    <label class=" field select" style="width: 100%">
                        <?php echo Form::select('competition_class_ids[]', $competition['arr_class'],$temp_class_id_arr, ['class' => 'form-control show-tick select_form1 select2','id'=>'competition_class_ids','multiple'=>'multiple']); ?>

                        <i class="arrow double"></i>
                    </label>
                </div>
                <?php if($errors->has('competition_class_ids')): ?> <p class="help-block"><?php echo e($errors->first('competition_class_ids')); ?></p> <?php endif; ?>
            </div>
        </div>
    </div>    
    <div class="col-lg-12 col-md-12">
        <!-- Description -->
        <lable class="from_one1"><?php echo trans('language.competitions_description'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::textarea('competition_description',old('competition_description',isset($competition['competition_description']) ? $competition['competition_description']: ''),array('class'=>'form-control no-resize','placeholder'=>trans('language.competitions_description'),'rows' => 3, 'cols' => 50)); ?>

        </div>
        <?php if($errors->has('competition_description')): ?> <p class="help-block"><?php echo e($errors->first('competition_description')); ?></p> <?php endif; ?>
    </div>

    

    
</div>
<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']); ?>

        <a href="<?php echo url('admin-panel/dashboard'); ?>" ><?php echo Form::button('Cancel', ['class' => 'btn btn-raised btn-round']); ?></a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    // For show and hide class dropdown
    function hide_div() {
        var x = document.getElementById("comp_class");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    function display_div() {
        var x = document.getElementById("comp_class");
        if (x.style.display === "none") {
            x.style.display = "none";

        } else {
            x.style.display = "none";
            $('#competition_fees').val("");
        }
    }

    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/class/get-class-data')); ?>",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("#competition_class_ids").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("#competition_class_ids").html('<option value="">Select Classes</option>');
            $(".mycustloading").hide();
        }
    }
    jQuery(document).ready(function () {

        var temp_level_val       = $('#competitions_level_class:checked').val()?true:false;
        var temp_certificate_det =  $('#Issue_certificate_yes:checked').val()?true:false;
        if(temp_certificate_det == false){
           $('#Issue_certificate_no').prop('checked', true); 
        }
        if(temp_level_val == true){
            var x = document.getElementById("comp_class");
            x.style.display = "block";
        }else{
            var x = document.getElementById("comp_class");
            x.style.display = "none";    
            $('#competitions_level_school').prop('checked', true);
        }    
            
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");            

        $("#competition-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation  rules 
             ------------------------------------------ */

            rules: {
                competition_name: {
                    required: true,
                    lettersonly:true
                },
                competition_date: {
                    required: true
                },
                competition_issue_participation_certificate: {
                    required: true
                },
                competition_level: {
                    required: true
                },
                competition_description: {
                    required: true
                }
            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        

    });

    

</script>