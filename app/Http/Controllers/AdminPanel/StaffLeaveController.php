<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use Symfony\Component\HttpFoundation\File\File;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;

class StaffLeaveController extends Controller
{
    /**
     *  View page for staff leave application
     *  @Khushbu on 20 Sept 2018
    **/
   
    public function index()
    {
        $loginInfo      	= get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/staff/staff-leave/view-staff-leave'),
            'page_title'    => trans('language.view_leave_application'),
        );
        
        return view('admin-panel.staff-leave.index')->with($data);
    }

     /**
     *  Get Data for view staff page(Datatables)
     *  @Khushbu on 20 Sept 2018
    **/
    public function anyData(Request $request)
    {
        $loginInfo  = get_loggedin_user_data();
        $staff      = [];
        return Datatables::of($staff)
            ->addColumn('action', function ($staff)
            {
                $encrypted_staff_id = get_encrypted_value($staff->staff_id, true);
                if($staff->staff_status == 0) {
                    $status = 1;
                    $statusVal = "Deactive";
                } else {
                    $status = 0;
                    $statusVal = "Active";
                }
                return ' <div class="dropdown" >
                    <button class="btn btn-primary dropdown-toggle custom_btn" type="button" data-toggle="dropdown">Action
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="view-staff-profile/' . $encrypted_staff_id . '" ">View Profile</a></li>
                        <li><a href="add-staff/' . $encrypted_staff_id . '" ">Edit</a></li>
                        <li><a href="delete-staff/' . $encrypted_staff_id . '" onclick="return confirm('."'Are you sure?'".')" >Delete</a></li>
                        <li><a href="staff-status/'.$status.'/' . $encrypted_staff_id . '">'.$statusVal.'</a></li>
                    </ul>
                </div>';
               
                
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

     /**
     *  Add page for staff leave application
     *  @Khushbu on 20 Sept 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data     = [];
        $staff 	  = [];
        $loginInfo  = get_loggedin_user_data();
        
        if (!empty($id))
        {
            if (!$staff)
            {
                return redirect('admin-panel/staff/staff-leave/add-staff-leave')->withError('Staff Attendance not found!');
            }
            $encrypted_staff_id = get_encrypted_value($staff['staff_id'], true);
            $page_title         = trans('language.edit_leave_application');
            $save_url           = url('admin-panel/staff/staff-leave/save/' . $encrypted_student_id);
            $submit_button      = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_leave_application');
            $save_url      = url('admin-panel/staff/staff-leave/save');
            $submit_button = 'Save';
        }

        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'staff'       	=> $staff,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/staff/staff-leave/view-staff-leave'),
        );
        return view('admin-panel.staff-leave.add')->with($data);
    }
}
