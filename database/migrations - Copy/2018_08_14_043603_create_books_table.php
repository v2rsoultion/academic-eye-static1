<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table      = 'books';
    protected $primaryKey = 'book_id';
    public function up()
    {
        if (!Schema::hasTable('books')){ 
            Schema::create('books', function (Blueprint $table) {
                $table->increments('book_id');
                $table->integer('admin_id')->unsigned()->default(0);
                $table->integer('update_by')->unsigned()->default(0);
                $table->integer('school_id')->unsigned()->default(0);
                $table->integer('book_category_id')->unsigned()->default(0);
                $table->string('book_name', 255)->nullable();
                $table->tinyInteger('book_type')->default(1)->comment = '0=General ,1=Barcoded';
                $table->string('book_subtitle', 255)->nullable();
                $table->string('book_isbn_no', 50)->nullable();
                $table->string('author_name', 255)->nullable();
                $table->string('publisher_name', 255)->nullable();
                $table->string('edition', 255)->nullable();
                $table->integer('book_vendor_id')->unsigned()->default(0);
                $table->integer('book_copies_no')->unsigned()->default(0);
                $table->integer('book_cupboard_id')->unsigned()->default(0);
                $table->integer('book_cupboardshelf_id')->unsigned()->default(0);
                $table->integer('book_price')->unsigned()->default(0);
                $table->string('book_remark', 255)->nullable();
                $table->tinyInteger('book_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('books', function($table) {
                    $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');
                });
            Schema::table('books', function($table) {
                $table->foreign('update_by')->references('admin_id')->on('admins')->onDelete('cascade');
            });
            Schema::table('books', function($table) {
                $table->foreign('school_id')->references('school_id')->on('schools')->onDelete('cascade');
            });
            Schema::table('books', function($table) {
                $table->foreign('book_category_id')->references('book_category_id')->on('book_categories')->onDelete('cascade');
            });
            Schema::table('books', function($table) {
                $table->foreign('book_vendor_id')->references('book_vendor_id')->on('book_vendors')->onDelete('cascade');
            });
            Schema::table('books', function($table) {
                $table->foreign('book_cupboard_id')->references('book_cupboard_id')->on('book_cupboards')->onDelete('cascade');
            });
            Schema::table('books', function($table) {
                $table->foreign('book_cupboardshelf_id')->references('book_cupboardshelf_id')->on('book_cupboardshelfs')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
