<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExamScheduleController extends Controller
{
     public function examSchedule() {
    	$loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.exam-schedule.exam_schedule')->with($data);
    }

    public function viewSchedule() {
    	$loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.exam-schedule.view_schedule')->with($data);
    }

    public function addSchedule() {
    	$loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.exam-schedule.add_schedule')->with($data);
    }

    public function manageClass() {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.exam-schedule.manage-class')->with($data);
    }
}
