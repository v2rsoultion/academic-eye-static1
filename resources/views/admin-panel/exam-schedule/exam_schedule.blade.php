@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 14px 10px !important;
  }
  .nav-tabs>.nav-item>.nav-link {
    width: 100px;
    margin-right: 5px;
        border: 1px solid #ccc !important;
  }
  #tabingclass .nav-tabs>.nav-item>.nav-link {
    border-radius: 4px !important;
    padding: 8px 25px;
  }
   #tabingclass .nav-link:hover {
        background: #6572b8 !important;
    color: #fff !important;
}
#tabingclass .nav-link:active {
        background: #6572b8 !important;
    color: #fff !important;
}
.idi .nav-tabs>.nav-item>.nav-link.active {
  background: #6572b8 !important;
    color: #fff !important;
}
.modal-dialog {
  max-width: 650px !important;
}
.checkbox {
  float: left;
  margin-right: 20px;
}
.table-responsive {
  overflow-x: visible !important;
}
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Exam Schedule</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
         <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/examination') !!}">Examination And Report Cards</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/examination') !!}">Exam Schedule </a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/examination/map-exam-to-class-subjects/add') !!}">Add</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap" style="padding-top: 20px !important;">
                 
                     <form class="" action="" method=""  id="" style="width: 100%;">
                      <div class="row">
                       <div class="col-lg-3">
                         <div class="form-group">
                          <!-- <lable class="from_one1">Schedule Name</lable> -->
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Schedule</option>
                              <option value="1">Schedule-1</option>
                              <option value="2">Schedule-2</option>
                              <option value="3">Schedule-3</option>
                              <option value="4">Schedule-4</option>
                            </select>
                         </div>
                      </div>
                     <div class="col-lg-1">
                      <button type="submit" class="btn btn-raised btn-primary " title="Search">Search
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Clear
                      </button>
                    </div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-2" style="margin-left:83px">
                      <a href="{{url('admin-panel/examination/exam-schedule/add-schedule')}}" class="btn btn-raised btn-primary"><i class="fas fa-plus-circle"></i> Add Schedule</a>                
                    </div>
                    </div>
                   </form>
                <hr style="width: 100%">
                    <!--  DataTable for view Records  -->
                     <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                      <thead>
                        <tr>
                          <th>S No</th>
                          <th>Schedule Name</th>
                          <th>Classes</th>
                          <th>Action</th>
                       </tr>
                      </thead>
                      <tbody>
                        <?php $counter = 1; for ($i=0; $i < 10; $i++) {  ?>
                        <tr>
                          <td><?php echo $counter; ?></td>
                          <td>Half Yearly</td>
                          <td>class-1, class-2, class-3, class-4</td>
                          <td>
                            <div class="dropdown">
                              <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="zmdi zmdi-label"></i>
                                <span class="zmdi zmdi-caret-down"></span>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                <li> <a href="{{url('admin-panel/examination/exam-schedule/manage-class')}}">Manage Class</a>
                                </li>
                                <li> <a href="{{url('admin-panel/examination/exam-schedule/view-schedule')}}">View Schedule</a>
                                </li>
                                <li><a href="#" data-toggle="modal" data-target="#AssignRoomNoModel">Assign Room No</a> 
                                </li>
                              </ul>
                            </div>
                           <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button></td>
                           
                        </tr>
                       <?php $counter++; } ?>
                      </tbody>
                    </table>
                  </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->


<!-- Action Model  -->
<div class="modal fade" id="AssignRoomNoModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Assign Room No</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row clearfix">
           <div class="col-lg-4">
               <div class="form-group">
                <lable class="from_one1">Exam Name</lable>
                  <select class="form-control show-tick select_form1" name="" id="">
                    <option value="">Select Exam</option>
                    <option value="1">Exam-1</option>
                    <option value="2">Exam-2</option>
                    <option value="3">Exam-3</option>
                    <option value="4">Exam-4</option>
                  </select>
               </div>
            </div>
                       <div class="col-lg-4">
                         <div class="form-group">
                          <lable class="from_one1">Class Name</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Class</option>
                              <option value="1">Class-1</option>
                              <option value="2">Class-2</option>
                              <option value="3">Class-3</option>
                              <option value="4">Class-4</option>
                            </select>
                         </div>
                      </div>
                       <div class="col-lg-4">
                         <div class="form-group">
                          <lable class="from_one1">Section</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Section</option>
                              <option value="A">A</option>
                              <option value="B">B</option>
                              <option value="C">C</option>
                              <option value="D">D</option>
                            </select>
                         </div>
                      </div>
                    </div>
                  
                    <div class="row">
                      <div class="col-lg-4">
                         <div class="form-group">
                          <lable class="from_one1">Start Roll No</lable>
                            <input type="text" name="start_roll_no" class="form-control" placeholder="Start Roll No">
                         </div>
                      </div>
                      <div class="col-lg-4">
                         <div class="form-group">
                          <lable class="from_one1">End Roll No</lable>
                            <input type="text" name="end_roll_no" class="form-control" placeholder="End Roll No">
                         </div>
                      </div>
                      <div class="col-lg-4">
                         <div class="form-group">
                          <lable class="from_one1">Room No</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Room No</option>
                              <option value="1">Room-1</option>
                              <option value="2">Room-2</option>
                              <option value="3">Room-3</option>
                              <option value="4">Room-4</option>
                            </select>
                         </div>
                      </div>
                    </div>
                    <hr><!-- #1f2f60 -->
                  <div class="row">
                       <div class="col-lg-2">
                      <button type="submit" class="btn btn-raised btn-primary">Save</button></div>
                      <div class="col-lg-1" style="margin-left: -30px;">
                    <button type="submit" class="btn btn-raised  btn-primary " data-dismiss="modal">Cancel</button> </div>
                  </div>

      </div>
    </div>
  </div>
</div>
@endsection