@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Manage Vendor</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/inventory') !!}">Inventory</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/inventory/manage-vendor') !!}">Manage Vendor</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card" style="margin-bottom: 20px;">
               <div class="header">
                <h2><strong>Basic</strong> Information <small>Enter New Detail To Create New Records...</small> </h2>
              </div>
              <div class="body form-gap1 ">
               
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Add Vendor :-</div> -->
                  <form class="" action="" id="manage_vendor" style="width: 100%;">
                    <div class="row" >
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Name</lable>
                          <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                        </div>
                         @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">GSTIN</lable>
                          <input type="text" name="gstin" id="gstin" class="form-control" placeholder="GSTIN">
                        </div>
                         @if ($errors->has('gstin')) <p class="help-block">{{ $errors->first('gstin') }}</p> @endif
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Contact No</lable>
                          <input type="text" name="contact_no" id="" class="form-control" placeholder="Contact No">
                        </div>
                         @if ($errors->has('contact_no')) <p class="help-block">{{ $errors->first('contact_no') }}</p> @endif
                      </div>
                       <div class="col-lg-3">
                        <div class="form-group">
                          <lable class="from_one1">Email</lable>
                          <input type="text" name="email" id="" class="form-control" placeholder="Email">
                        </div>
                         @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                      </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                       <div class="col-lg-12">
                        <div class="form-group">
                          <lable class="from_one1">Address</lable>
                          <textarea class="form-control" placeholder="Address" name="address"></textarea>
                        </div>
                         @if ($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif
                      </div>
                      <!-- <div class="col-lg-3">
                        <div class="form-group">
                            <lable class="from_one1">Select Term</lable>
                            <select class="form-control show-tick select_form1" name="" id="">
                              <option value="">Select Term</option>
                              <?php $count = 1; for ($i=0; $i < 5 ; $i++) {  ?>
                               <option value="<?php echo $count; ?>">Terms <?php echo $count; ?></option>
                             <?php $count++; } ?>
                            </select>
                         </div>
                      </div> -->
                    </div>
                      <!-- <hr> -->
                      <div class="row">
                      <div class="col-lg-1 ">
                        <button type="submit" class="btn btn-raised btn-primary saveBtn" title="Save">Save
                        </button>
                      </div>
                      <div class="col-lg-1 ">
                        <button type="reset" class="btn btn-raised btn-primary cancelBtn" title="Cancel">Cancel
                        </button>
                      </div>
                    
                    </div>
                  </form>
                </div>
              </div>
                  <!-- <hr> -->
                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <div class="card">
                    <div class="body form-gap1">
                  <div class="headingcommon  col-lg-12" style="margin-left: -13px">Search By :-</div>
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                      <!--    <div class="headingcommon  col-lg-12">Free By Rte :-</div> -->
                      <div class="col-lg-3">
                        <div class="form-group">
                          <!-- <lable class="from_one1" for="name">Name</lable> -->
                          <input type="text" name="name" id="" class="form-control" placeholder="Name">
                        </div>
                          @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <!-- <lable class="from_one1">Email</lable> -->
                          <input type="text" name="" id="" class="form-control" placeholder="Email">
                        </div>
                      </div>
                     <div class="col-lg-3">
                        <div class="form-group">
                          <!-- <lable class="from_one1">Contact No</lable> -->
                          <input type="text" name="" id="" class="form-control" placeholder="Contact No">
                        </div>
                      </div>
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary" title="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                
                  <!-- <div class="col-lg-12" style="border:1px solid #f1f1f1; margin-top: 20px;" > </div> -->
                  <div class="clearfix"></div>
                  <!--  DataTable for view Records  -->
                  <!-- <hr> -->
                    <div class="table-responsive">
                    <table class="table m-b-0 c_list" id="" style="width:100%">
                    {{ csrf_field() }}
                    <thead>
                      <tr>
                        <th>S No</th>
                        <th>Name</th>
                        <th>GSTIN</th>
                        <th>Contact No</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                       
                      <tr>
                        <td>1</td>
                        <td>Mahesh Kumar </td>
                        <td>GST-1</td>
                        <td>9009890883</td>
                        <td>maheshkumar@gmail.com</td>
                        <td>Jaipur</td>
                        <td class="text-center">
                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Raj Singh</td>
                        <td>GST-2</td>
                        <td>8012345433</td>
                        <td>rajsingh@gmail.com</td>
                        <td>Pali</td>
                        <td class="text-center">
                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Yogesh Mishra</td>
                        <td>GST-3</td>
                        <td>8760943667</td>
                        <td>yogeshmishra@gmail.com</td>
                        <td> Jodhpur</td>
                        <td class="text-center">
                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>Mayur Meena</td>
                        <td>GST-4</td>
                        <td>8760943669</td>
                        <td>mayurmeena@gmail.com</td>
                        <td> Bikaner</td>
                        <td class="text-center">
                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>Ashok</td>
                        <td>GST-5</td>
                        <td>9964268329</td>
                        <td>ashok011@gmail.com</td>
                        <td> Alwar</td>
                        <td class="text-center">
                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->


@endsection