
<?php $__env->startSection('content'); ?>
<style type="text/css">
   
</style>
<!--  Main content here -->
<section class="content">
   <div class="block-header">
      <div class="row">
         <div class="col-lg-5 col-md-6 col-sm-12">
            <h2><?php echo trans('language.recurring_heads'); ?></h2>
         </div>
         <div class="col-lg-7 col-md-6 col-sm-12 line">
            <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.menu_fee_collection'); ?></a></li>
               <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/fees-collection'); ?>"><?php echo trans('language.recurring_heads'); ?></a></li>
               <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/fees-collection/recurring-heads/add-recurring-head'); ?>"><?php echo trans('language.add_recurring_heads'); ?></a></li>
            </ul>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row clearfix">
         <div class="col-lg-12" id="bodypadd">
            <div class="tab-content">
               <div class="tab-pane active" id="classlist">
                  <div class="card">
                     <div class="header">
                        <h2><strong>Basic</strong> Information <small>Enter Records Will Show Here...</small> </h2>
                     </div>
                     <div class="body form-gap">
                             <form class="" action="" method="post" id="" style="width: 100%;">
                           <div class="row">
                              <div class="headingcommon  col-lg-12">Fees Heads :-</div>
                              <div class="col-lg-3">
                                 <div class="form-group">
                                    <lable class="from_one1" for="name">Fees Particular Name</lable>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Fees particular name">
                                 </div>
                              </div>
                             
                              <div class="col-lg-3">
                                 <div class="form-group">
                                    <lable class="from_one1" for="name">Class</lable>
                                    <select class="form-control show-tick select_form1" name="classes" id="" multiple="multiple">
                                       <option value="">Applied on Class</option>
                                       <option value="1"> 1st</option>
                                       <option value="2">2nd</option>
                                       <option value="3">3rd</option>
                                       <option value="4">4th</option>
                                       <option value="10th"> 10th</option>
                                       <option value="11th">11th</option>
                                       <option value="12th">12th</option>
                                    </select>
                                 </div>
                              </div>
                           
                              <div class="clearfix"></div>
                              <div class="col-lg-3">
                                 <div class="form-group">
                                    <lable class="from_one1" for="name">Total Fees</lable>
                                    <input type="text" name="name" id="" class="form-control" placeholder="Total fees">
                                 </div>
                              </div>
                               <div class="col-lg-12 commclass">
                                 <h4>No. of installment :- </h4>
                              </div>
                              <div id="sectiontable-body" class="row seprateclass"  >
                                 <input type="hidden" id="hide" value="0" />
                              <div class="col-lg-3 ">
                                 <div class="form-group">
                                    <lable class="from_one1" for="name">Name of installment</lable>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Name of installment">
                                 </div>
                              </div>
                              <div class="col-lg-3 ">
                                 <div class="form-group">
                                    <lable class="from_one1" for="name">Effective date of installment</lable>
                                    <input type="text" name="name" id="date_installation" class="form-control" placeholder="Effective date of installment">
                                 </div>
                              </div>
                              <div class="col-lg-3 ">
                                 <div class="form-group">
                                    <lable class="from_one1" for="name">Amount of installment</lable>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Amount of installment">
                                 </div>
                              </div>
                               <div class="col-lg-2" style="padding:0px 0px;">
                                 <button type="button" class="btn btn-primary add_field_button"><i class="fas fa-plus-circle"></i> Add</button>
                              </div>
                               <div class="input_fields_wrap">
                              </div>
                             
                           </div>
                               <!--  Submit button -->
                               <div class="clearfix"></div>
                               <div class="col-lg-12">
                               <button type="submit" class="btn btn-primary float-right" title="Submit">Submit
                              </button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   </div>
  
</section>
<!-- Content end here  -->
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>