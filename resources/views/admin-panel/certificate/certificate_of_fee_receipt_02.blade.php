<!DOCTYPE html>
<html>
<head>
	<title>Fee Receipt-02</title>
<link rel="stylesheet" href="../../../../public/assets/plugins/bootstrap/css/bootstrap.min.css">
	<style type="text/css">
	body {
		/*width: 1020px;*/
		width: 720px;
		margin: auto;
		/*margin-top: 20px;*/
	}
	.content_size {
		font-size: 17px;
		/*font-style: italic;*/
	}
	.align {
		text-align: center;
	}
	.header_gap{
		color: #f2a31c;
		margin-top: 40px;
		margin-bottom: 0px;
	}
	.header {
		/*border-radius: 10px;*/
		border: 1px solid #1f2f60;
		padding:10px 40px;
	}
	.left_gap {
		margin-left: 30px;
	}
	.align_right {
		text-align: right !important;
	}
	.sign {
		margin-right: 30px;
		font-weight: bold;
	}
	.text {
		border-bottom: 2px solid #000;
		font-size: 20px;
		font-style: italic;
	}
	.rr {
		padding-right: 0px;
	}
	.ll {
		padding-left: 10px;
	}
	.rl {
		padding: 0px;
	}
	.bold {
		font-weight: bold;
	}
	.position1{
		position: relative;
		left: -115px;
	}
	/*table{
		height: 40px;

	}*/
	tr {
		height: 23px;
	}
	#border11 tr {
		height: 30px;
	}
	.border_bottom {
		border-bottom:1px solid #000;
	}
	.border_top {
		border-top:1px solid #000;
	}
	.border {
		border-right: 1px solid #000;
	}
	</style>

</head>
<body>
	<div class="header">
		<table style="width: 100%;">
			<tr>
				<td colspan="3" style="width: 15%;" class="align">
					<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="80px" style="margin-top: 15px;">
				</td>
				<td style="width: 55%;"> </td>
				<td><h2 style="margin-bottom:10px;">Fee Receipt</h2>	
					<p class="align_left" style="font-size: 13px;margin-top: 0px;margin-bottom: 2px;"> Hanuman Gali, Pushkar, Rajasthan <br> Ph. +91 999 999 9999, &nbsp;&nbsp; <br>
					E-mail : demo@demomail.com</p>	
				</td>
			</tr>
		</table>
		<!-- <table>
			<tr>
				<td colspan="3" style="width: 150px;" class="align">
					<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="80px" style="margin-top: 15px;">
				</td>
				<td style="width: 600px;"> </td>
				<td><h2 style="margin-bottom:10px;">Fee Receipt</h2>	
					<p class="align_left" style="font-size: 13px;margin-top: 0px;margin-bottom: 2px;"> Hanuman Gali, Pushkar, Rajasthan <br> Ph. +91 999 999 9999, &nbsp;&nbsp; <br>
					E-mail : demo@demomail.com</p>	
				</td>
			</tr>
		</table> -->
		<hr style="margin:4px 0px;">
		<table style="width: 100%">
			<tr>
				<td style="width: 20%" class="bold content_size">Receipt No. </td>
				<td style="width: 20%" class="content_size bold">FC-108</td>
				<td style="width: 25%"> </td>
				<td style="width: 19%" class="bold content_size">Date</td>
				<td style="width: 13%" class="content_size position">28/11/2011</td>
			</tr>
			<tr>
				<td class="bold content_size">Name</td>
				<td class="content_size">Sarthak Pundir</td>
				<td> </td>
				<td class="bold content_size">Class</td>
				<td class="content_size position">XI A</td>
			</tr>
			<tr>
				<td class="bold content_size">Adm No </td>
				<td class="content_size">2381</td>
				<td> </td>
				<td class="bold content_size">Payment Mode</td>
				<td class="content_size position">Cash</td>
			</tr>
			
		</table>
		<!-- <table>
			<tr>
				<td style="width: 160px" class="bold content_size">Receipt No. </td>
				<td style="width: 150px" class="content_size bold">FC-108</td>
				<td style="width: 300px"> </td>
				<td style="width: 170px" class="bold content_size">Date</td>
				<td style="width: 140px" class="content_size position">28/11/2011</td>
			</tr>
			<tr>
				<td class="bold content_size">Name</td>
				<td class="content_size">Sarthak Pundir</td>
				<td> </td>
				<td class="bold content_size">Class</td>
				<td class="content_size position">XI A</td>
			</tr>
			<tr>
				<td class="bold content_size">Adm No </td>
				<td class="content_size">2381</td>
				<td> </td>
				<td class="bold content_size">Payment Mode</td>
				<td class="content_size position">Cash</td>
			</tr>
			
		</table> -->
		<table style="width: 100%;border: 1px solid #000;" id="border11" cellspacing="0px">
			<tr class="align">
				<th class="border border_bottom">S No.</th>
				<th class="border border_bottom">Particulars</th>
				<th class=" border_bottom">Amount</th>
			</tr>
			<tbody>
				<tr style="height: 23px;">
					<td class="align border">1</td>
					<td class="ll border">Admission Fees</td>
					<td class="align ">&nbsp;&nbsp;&nbsp;400.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="align border">2</td>
					<td class="ll border">Installment(Tuition) Fees</td>
					<td class="align ">&nbsp;&nbsp;&nbsp;150.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="align border">3</td>
					<td class="ll border">Exam Fees</td>
					<td class="align ">&nbsp;&nbsp;&nbsp;200.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="align border">4</td>
					<td class="ll border">Concession</td>
					<td class="align ">&nbsp;&nbsp;&nbsp;400.00</td>
				</tr>
				<tr style="height: 23px;">
					<td class="align border ">5</td>
					<td class="ll border">Fine</td>
					<td class="align ">&nbsp;&nbsp;&nbsp;150.00</td>
				</tr>
				
			</tbody>
		</table>
		<table cellspacing="0px" id="border11" style="width: 100%">
		<tr>
			<td style="width: 78%;" class="align_right">Total Amount:</td>
			<td style="width: 22%;" class="align">1,300.00</td>
		</tr>
		<tr>
			<td class="align_right">Paid:</td>
			<td class="align">1,300.00</td>
		</tr>
		
		</table>
		<p>Rupees :	<b>Ten thousands two hundred fifty only</b></p>
		
		
	</div>
</body>
</html>

<script type="text/javascript">
	window.print();
</script>