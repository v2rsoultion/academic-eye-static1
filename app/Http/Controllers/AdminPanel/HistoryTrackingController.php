<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HistoryTrackingController extends Controller
{
	/**
     *  Show for History Tracking
     *  @Khushbu on 01 Oct 2018   
    **/
    public function viewHistoryTracking() {

        $loginInfo 		    = get_loggedin_user_data();
        
        $data 				= array(
            'page_title'    => trans('language.history_tracking'),
            'redirect_url'  => url('admin-panel/transport/history-tracking/view-history-tracking'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.history-tracking.view-history-tracking')->with($data);
    }
}
