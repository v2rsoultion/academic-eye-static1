Hello {!! $receiver !!},
<br />
<br />
Please find below login credentials of your account.
<br />
Email: {!! $email !!} <br />
Password: {!! $password !!}
<br />
<br />
Thank You, <br />
{!! $sender !!}