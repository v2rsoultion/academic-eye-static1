@if(isset($country['country_id']) && !empty($country['country_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('country_id',old('country_id',isset($country['country_id']) ? $country['country_id'] : ''),['class' => 'gui-input', 'id' => 'country_id', 'readonly' => 'true']) !!}


@if ($errors->any())
<div class="alert alert-danger" role="alert">
    {{$errors->first()}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1">{!! trans('language.country_name') !!} :</lable>
        <div class="form-group">
            {!! Form::text('country_name', old('country_name',isset($country['country_name']) ? $country['country_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.country_name'), 'id' => 'country_name']) !!}
        </div>
        @if ($errors->has('country_name')) <p class="help-block">{{ $errors->first('country_name') }}</p> @endif
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#class-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            // errorLabelContainer: '.errorTxt',

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                country_name: {
                    required: true,
                    lettersonly:true
                },

            },

            // message: {
            //     required: 'This field is required';
            // }

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
    });


</script>