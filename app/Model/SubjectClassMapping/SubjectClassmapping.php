<?php

namespace App\Model\SubjectClassMapping;

use Illuminate\Database\Eloquent\Model;

class SubjectClassmapping extends Model
{
    protected $table      = 'subject_class_mapping';
    protected $primaryKey = 'subject_class_map_id';

    public function getSubjects()
    {
        return $this->belongsTo('App\Model\Subject\Subject', 'subject_id');
    }
}
