<?php $__env->startSection('content'); ?>
<style type="text/css">
  .nav_item7
  {
    min-height: 230px !important;
  }
  .imgclass:hover .active123 .tab_images123{
    left: 50px !important;
  }
  .imgclass:hover .tab_images123{
    left: 50px !important;
  }
  .nav_item3 {
    min-height: 255px !important;
  }
  .side-gap{
    width: 75% !important;
  }
   td{
    padding: 5px 10px !important;
  }
</style>
<!--  Main content here -->

<section class="content">
      <div class="block-header">
        <div class="row">
          <div class="col-lg-5 col-md-6 col-sm-12">
            <h2>My Profile</h2>
          </div>
          <div class="col-lg-7 col-md-6 col-sm-12 line">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/parent/view-profile'); ?>">My Profile</a></li>
            </ul>
          </div>
        </div>
      </div>

<div class="container-fluid">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card profile_card1">
              <div class="body text-center pro_body_center1 form-gap ">
                <div class="row clearfix">
                  <div class="col-lg-2 col-md-2 col-sm-12 text-left">
                    <div class="profile-image"> <img src="<?php echo URL::to('public/Wallpapers/1.jpg'); ?>" alt="" class="profile_image"> </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="std1">
                      <div class="profile_detail_3">
                          <i class="far fa-hand-point-right"></i>
                        <label> <span>Name: </span> Mahesh Kumar</label>
                      </div>
                      <!--  <div class="profile_detail_3">
                          <i class="far fa-hand-point-right"></i>
                        <label> <span>Mother Name: </span> Indira Devi</label>
                      </div> -->
                      <div class="profile_detail_3">
                          <i class="far fa-hand-point-right"></i>
                        <label> <span>Occupation : </span> Teacher</label>
                      </div>   
                        <div class="profile_detail_3">
                          <i class="far fa-hand-point-right"></i>
                        <label> <span> Annual Salary : </span> 3,00,000</label>
                      </div>
                      <div class="profile_detail_3">
                          <i class="far fa-hand-point-right"></i>
                        <label> <span>Mobile Number : </span> +91 9008090080</label>
                      </div>
                       <div class="profile_detail_3">
                          <i class="far fa-hand-point-right"></i>
                        <label> <span> Email : </span> Maheshkumar44@gmail.com</label>
                      </div>
                       
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12" style="margin-top: 15px;">
                    <div class="form-group">
                      <!-- <lable class="from_one1" for="name">Childrens</lable> -->
                      <select class="form-control show-tick select_form1" name="classes" id="children_list" onchange ="window.location='<?php echo e(url("admin-panel/parent/children-details")); ?>'">
                        <option value="">Select Children</option>
                        <option value="A">Amit</option>
                        <option value="B">Aman</option>
                        <option value="C">Aarav</option>
                        <option value="D">Anand</option>
                      </select>
                    </div>
                  </div>

                </div>


              </div>
            </div>
          </div>
        </div>
      </div>


<div class="container-fluid">
        <div class="row clearfix">
          <div class="col-lg-2 col-md-2 col-sm-12">
            <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 tab1">
                <div class="card side-gap">
                  <ul class="ul_underline_1">
                    
                     <li class="nav-item imgclass">
                      <a class="nav-link active active123" data-toggle="tab" href="#parent">
                        <div class="pro_image_tabs1">
                          <img src="<?php echo URL::to('public/Wallpapers/Parent-Information.png'); ?>" class="tab_images123">
                          <img src="<?php echo URL::to('public/Wallpapers/Parent-Information1.png'); ?>" class="tab_images2123">
                        </div>
                        Personal Information
                      </a>
                    </li>
                    <li class="nav-item imgclass">
                      <a class="nav-link active123" data-toggle="tab" href="#address_info">
                        <div class="pro_image_tabs1">
                          <img src="<?php echo URL::to('public/Wallpapers/Personal-informastion.png'); ?>" class="tab_images123">
                          <img src="<?php echo URL::to('public/Wallpapers/Personal-informastion1.png'); ?>" class="tab_images2123">
                        </div>
                        Address
                      </a>
                    </li>

                    <li class="nav-item imgclass">
                      <a class="nav-link active123" data-toggle="tab" href="#login_details">
                        <div class="pro_image_tabs1">
                          <img src="<?php echo URL::to('public/Wallpapers/Marks.png'); ?>" class="tab_images123">
                          <img src="<?php echo URL::to('public/Wallpapers/Marks1.png'); ?>" class="tab_images2123">
                        </div>
                        Login Details
                      </a>
                    </li>
                  
                  </ul>
                  <!--  <div class="ui_marks1"></div> -->
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-10 col-md-10 col-sm-12" style="margin-left:-73px;">
            <div class="">
              <div class="tab-content">
               
                <div class="tab-pane body active" id="parent" style="margin-left: 20px !important;">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;
    width: 989px !important;">
                          <div class="tab-pane active" id="classlist">
                            <div class="">
                              <div class="card border_info_tabs_1 card_top_border1">
                                <div class="row clearfix">
                                  <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="card">
                                      <div class="body">
                                        <div class="nav_item7">
                                        <div class="nav_item1">
                                          <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> My Info </a>
                                        </div>
                                        <div class="nav_item2">
                                              <div class="profile_detail_2">
                                                 <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2"> Name :</b> <span class="profile_detail_span_6">Rajesh Kumar</span>
                                                 </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                 <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2"> Mobile Number :</b> <span class="profile_detail_span_6">91+ 124512154</span>
                                                 </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                 <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2"> Email Address :</b> <span class="profile_detail_span_6">rajeshkumar@gmail.com</span>
                                                 </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                 <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2"> Annual Salary :</b> <span class="profile_detail_span_6">2.5 Lac</span>
                                                 </div>
                                              </div>
                                              <div  class="profile_detail_2">
                                                 <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">Occupation :</b> <span class="profile_detail_span_6">Army</span>
                                                 </div>
                                          </div>
                                        </div>
                                      </div>
                                        <div class="nav_item7">
                                        <div class="nav_item1">
                                          <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Spouse's Info </a>
                                        </div>
                                        <div class="nav_item2">
                                               <div  class="profile_detail_2">
                                               <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2"> Name :</b> <span class="profile_detail_span_6">Kiran Kumari</span>
                                               </div>
                                            </div>
                                            <div  class="profile_detail_2">
                                               <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2"> Mobile Number :</b> <span class="profile_detail_span_6">91+ 124512154</span>
                                               </div>
                                            </div>
                                            <div  class="profile_detail_2">
                                               <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2"> Email Address :</b> <span class="profile_detail_span_6">kirankumari@gmail.com</span>
                                               </div>
                                            </div>
                                            <div  class="profile_detail_2">
                                               <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2"> Annual Salary :</b> <span class="profile_detail_span_6">2.5 Lac</span>
                                               </div>
                                            </div>
                                            <div  class="profile_detail_2">
                                               <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                  <b class="profile_detail_bold_2">Occupation :</b> <span class="profile_detail_span_6">Doctor</span>
                                               </div>
                                            </div>
                                        </div>
                                      </div>
                                      <div class="nav_item7">
                                        <div class="nav_item1">
                                          <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Other Info </a>
                                        </div>
                                        <div class="nav_item2">
                                        <div class="profile_detail_2">
                                           <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                              <b class="profile_detail_bold_2">Guardian Name :</b> <span  class="profile_detail_span_6">Radha Kumari</span>
                                           </div>
                                        </div>
                                        <div  class="profile_detail_2">
                                           <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                              <b class="profile_detail_bold_2">Guardian Mobile Number :</b> <span class="profile_detail_span_6">91+ 124512154</span>
                                           </div>
                                        </div>
                                        <div  class="profile_detail_2">
                                           <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                              <b class="profile_detail_bold_2">Guardian Email Address :</b> <span class="profile_detail_span_6">radhakumari@gmail.com</span>
                                           </div>
                                        </div>  
                                        </div>
                                      </div>

                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                    
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
               
                <div class="tab-pane body" id="address_info" style="margin-left: 20px !important;">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;
    width: 989px !important;">
                          <div class="tab-pane active" id="classlist">
                            <div class="">
                              <div class="card border_info_tabs_1 card_top_border1">
                                <div class="row clearfix">
                                  <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="card">
                                      <div class="body">
                                        <div class="nav_item4">
                                        <div class="nav_item1">
                                          <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Address Info </a>
                                        </div>
                                        <div class="nav_item2">
                                          <div role="tabpanel" class="tab-pane" id="address_with_icon_title">
                                            <div class="parent_address_info_1">
                                              <div class="parent_address_temparay_info_1">
                                                <h6>Temporary Address :</h6>
                                                <div class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">Address :</b> <span class="profile_detail_span_6">Jodhpur,Rajasthan</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">City :</b> <span class="profile_detail_span_6">Jaipur</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">State :</b> <span class="profile_detail_span_6">Rajasthan</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">Country :</b> <span class="profile_detail_span_6">India</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">Pincode :</b> <span class="profile_detail_span_6">220123</span>
                                                  </div>
                                                </div>
                                                <hr>
                                              </div>
                                              <div style="" class="pi_1">
                                                <h6>Permanent Address :</h6>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">Address :</b> <span class="profile_detail_span_6">Jodhpur,Rajasthan</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">City :</b> <span class="profile_detail_span_6">Jaipur</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">State :</b> <span class="profile_detail_span_6">Rajasthan</span>
                                                  </div>
                                                </div>
                                                <div  class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">Country :</b> <span class="profile_detail_span_6">India</span>
                                                  </div>
                                                </div>
                                                <div class="profile_detail_2">
                                                  <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                    <b class="profile_detail_bold_2">Pincode :</b> <span class="profile_detail_span_6">220123</span>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="tab-pane body" id="documents" style="margin-left: 20px !important;">
                  <!-- <div class=""> -->
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;
    width: 989px !important;">
                          <div class="tab-pane active" id="classlist">
                            <div class="">
                              <div class="card border_info_tabs_1 card_top_border1">
                                <div class="body">
                                  <div class="nav_item4">
                                   <div class="nav_item1">
                                          <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Document Info </a>
                                        </div>
                                  <table class="table table-hover m-b-0 c_list">
                                    <thead>
                                      <tr>
                                        <th>S No</th>
                                        <th>Category</th>
                                        <th>Document Details</th>
                                        <th>Submit Date</th>
                                        <th>Status</th>
                                        <th>Download</th>
                                        <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>aadhaar card</td>
                                        <td><button  class="btn btn-info search_button28"  data-toggle="modal" data-target="#largeModal3">View Details</button></td>
                                        <td>2201/20/1</td>
                                        <td>Pending</td>
                                        <td>
                                          <a  href="http://v2rsolution.co.in/school/school-management/student-dairy-admin/assets/images/profile_av.jpg" class="acher_1">download file</a>
                                        </td>
                                        <td>
                                          <div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Disapproved"> <i class="fas fa-minus-circle"></i> </div>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>birth certificate</td>
                                        <td><button  class="btn btn-info search_button28"  data-toggle="modal" data-target="#largeModal3">View Details</button></td>
                                        <td>2201/20/1</td>
                                        <td>Pending</td>
                                        <td>
                                          <a  href="http://v2rsolution.co.in/school/school-management/student-dairy-admin/assets/images/profile_av.jpg" class="acher_1">download file</a>
                                        </td>
                                        <td>
                                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>3</td>
                                        <td>birth certificate</td>
                                        <td><button  class="btn btn-info search_button28"  data-toggle="modal" data-target="#largeModal3">View Details</button></td>
                                        <td>2201/20/1</td>
                                        <td>Pending</td>
                                        <td>
                                          <a  href="http://v2rsolution.co.in/school/school-management/student-dairy-admin/assets/images/profile_av.jpg" class="acher_1">download file</a>
                                        </td>
                                        <td>
                                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>4</td>
                                        <td>birth certificate</td>
                                        <td><button  class="btn btn-info search_button28"  data-toggle="modal" data-target="#largeModal3">View Details</button></td>
                                        <td>2201/20/1</td>
                                        <td>Pending</td>
                                        <td>
                                          <a  href="http://v2rsolution.co.in/school/school-management/student-dairy-admin/assets/images/profile_av.jpg" class="acher_1">download file</a>
                                        </td>
                                        <td>
                                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                          <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane body" id="shift" style="margin-left: 20px !important;">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;
    width: 989px !important;">
                          <div class="tab-pane active" id="classlist">
                            <div class="card border_info_tabs_1 card_top_border1">
                              <div class="body">
                                <div class="nav_item5">
                                <div class="nav_item1">
                                  <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Shift Info </a>
                                </div>
                                <table class="table table-hover m-b-0 c_list">
                                    <thead>
                                      <tr>
                                        <th>S No</th>
                                        <th>Shifts</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <!-- <th>Status</th>
                                        <th>Action</th> -->
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Morning</td>
                                        <td>7:00 AM</td>
                                        <td>12:00 PM</td>
                                        
                                      </tr>
                                     
                                      <tr>
                                        <td>2</td>
                                        <td>Evening</td>
                                        <td>12:00 PM</td>
                                        <td>05:00 PM</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                 <div class="tab-pane body" id="schedule" style="margin-left: 20px !important;">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;
    width: 989px !important;">
                          <div class="tab-pane active" id="classlist">
                            <div class="card border_info_tabs_1 card_top_border1">
                              <div class="body">
                                <div class="nav_item8">
                                <div class="nav_item1">
                                  <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  Schedule Info </a>
                                </div>
                                <table class="table table-bordered tb1">
                                        <thead>
                                            <tr>
                                                <th width="30">Period no.</th>
                                                <th>Monday</th>
                                                <th>Tuesday</th>
                                                <th>Wenesday</th>
                                                <th>Thursday</th>
                                                <th>Friday</th>
                                                <th>Saturday</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Period-1</td>
                                                <td class="data_table1">
                                                    07:30 AM - 08:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> I-A
                                                </td>
                                                <td class="data_table1">
                                                    07:30 AM - 08:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> I-A
                                                </td>
                                                
                                                <td>----</td>
                                                <td class="data_table1">
                                                    07:30 AM - 08:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> I-A
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                     07:30 AM - 08:00 AM <br>
                                                    <strong>Sub. :</strong> Economic <br>
                                                    <strong>Class :</strong> XII-A
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Period-2</td>
                                                <td class="data_table1">
                                                    08:00 AM - 08:30 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> II-A
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    08:00 AM - 08:30 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> II-A 
                                                </td>
                                                <td class="data_table1">
                                                    08:00 AM - 08:30 AM<br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> II-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    08:00 AM - 08:30 AM <br>
                                                    <strong>Sub. :</strong> Business <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Period-3</td>
                                                <td class="data_table1">
                                                    08:30 AM - 09:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> III-A
                                                     
                                                </td>
                                                 <td class="data_table1">
                                                    08:30 AM - 09:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> III-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    08:30 AM - 09:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                     08:30 AM - 09:00 AM  <br>
                                                    <strong>Sub. :</strong> Computer <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    08:30 AM - 09:00 AM <br>
                                                    <strong>Sub. :</strong> English <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>                                                <td>----</td>
                                            </tr>
                                            <tr>
                                                <td>Period-4</td>
                                                <td class="data_table1">
                                                    09:00 AM - 09:30 AM <br>
                                                    <strong>Sub. :</strong> Economic <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                   09:00 AM - 09:30 AM  <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    09:00 AM - 09:30 AM <br>
                                                    <strong>Sub. :</strong> Economic <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    09:00 AM - 09:30 AM <br>
                                                    <strong>Sub. :</strong> Account <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    09:00 AM - 09:30 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td>----</td>
                                            </tr>
                                            <tr>
                                                <td>Period-5</td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    10:00 AM - 10:30 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> X-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    10:00 AM - 10:30 AM <br>
                                                    <strong>Sub. :</strong> Account <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td class="data_table1">
                                                    10:00 AM - 10:30 AM <br>
                                                    <strong>Sub. :</strong> Business <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    10:00 AM - 10:30 AM <br>
                                                    <strong>Sub. :</strong> Account <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Period-6</td>
                                                 <td class="data_table1">
                                                    10:30 AM - 11:00 AM <br>
                                                    <strong>Sub. :</strong> Business <br>
                                                    <strong>Class :</strong> XI-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    10:30 AM - 11:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> X-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    10:30 AM - 11:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td>----</td>
                                            </tr>
                                            <tr>
                                                <td>Period-7</td>
                                                <td>----</td>
                                                 <td class="data_table1">
                                                    11:00 AM - 11:30 AM <br>
                                                    <strong>Sub. :</strong> Business <br>
                                                    <strong>Class :</strong> XI-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    11:00 AM - 11:30 AM <br>
                                                    <strong>Sub. :</strong> Economic <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td>----</td>
                                            </tr> 
                                            <tr>
                                                <td>Period-8</td>
                                                <td class="data_table1">
                                                    11:30 AM - 12:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                </td>
                                                <td>----</td>
                                                <td class="data_table1">
                                                    11:30 AM - 12:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                </td>
                                                
                                                <td class="data_table1">
                                                    11:30 AM - 12:00 AM <br>
                                                    <strong>Sub. :</strong> Hindi <br>
                                                    <strong>Class :</strong> XII-A
                                                     
                                                </td>
                                                <td>----</td>
                                                <td>----</td>
                                            </tr>                                            
                                        </tbody>
                                    </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane body" id="attendance" style="margin-left: 20px !important;">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;
    width: 989px !important;">
                          <div class="tab-pane active" id="classlist">
                            <div class="card border_info_tabs_1 card_top_border1">
                              <div class="body">
                                <div class="nav_item5">
                                <div class="nav_item1">
                                  <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  Attendance Info </a>
                                </div>
                                <button class="btn btn-primary btn-sm btn-round waves-effect" id="change-view-today">today</button>
                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-day" >Day</button>
                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-week">Week</button>
                            <button class="btn btn-default btn-sm btn-simple btn-round waves-effect" id="change-view-month">Month</button>
                            <div id="calendar" class="m-t-15"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                  <div class="tab-pane body" id="salary" style="margin-left: 20px !important;">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;
    width: 989px !important;">
                          <div class="tab-pane active" id="classlist">
                            <div class="card border_info_tabs_1 card_top_border1">
                                <div class="body">
                                  <div class="nav_item5">
                                  <div class="nav_item1">
                                  <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  Salary Info </a>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                 <div class="tab-pane body" id="edithistory" style="margin-left: 20px !important;">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;
    width: 989px !important;">
                          <div class="tab-pane active" id="classlist">
                            <div class="card border_info_tabs_1 card_top_border1">
                                <div class="body">
                                  <div class="nav_item5">
                                  <div class="nav_item1">
                                  <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  History Info </a>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="tab-pane body" id="leave" style="margin-left: 20px !important;">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;
    width: 989px !important;">
                          <div class="tab-pane active" id="classlist">
                            <div class="card border_info_tabs_1 card_top_border1">
                                <div class="body">
                                  <div class="nav_item5">
                                  <div class="nav_item1">
                                  <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i>  Attendance Info </a>
                                </div>
                                  <table class="table table-hover m-b-0 c_list">
                                    <thead>
                                      <tr>
                                        <th>S No</th>
                                        <th>Name</th>
                                        <th>Reason</th>
                                        <th>Date range</th>
                                        <th>Attachment</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Rakesh</td>
                                        <td>Lorem Ipsum is simply ..</td>
                                        <td>1 july 2018 - 5 July 2018</td>
                                        <td>
                                          <a  href="http://v2rsolution.co.in/school/school-management/student-dairy-admin/assets/images/profile_av.jpg" class="acher_1">view attachment</a>
                                        </td>
                                        <td>Disapproved</td>
                                        <td>
                                          <div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Disapproved"> <i class="fas fa-minus-circle"></i> </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Rakesh</td>
                                        <td>Lorem Ipsum is simply ..</td>
                                        <td>1 july 2018 - 5 July 2018</td>
                                        <td>
                                          <a  href="http://v2rsolution.co.in/school/school-management/student-dairy-admin/assets/images/profile_av.jpg" class="acher_1">view attachment</a>
                                        </td>
                                        <td>Approved</td>
                                        <td>
                                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>3</td>
                                        <td>Rakesh</td>
                                        <td>Lorem Ipsum is simply ..</td>
                                        <td>1 july 2018 - 5 July 2018</td>
                                        <td>
                                          <a  href="http://v2rsolution.co.in/school/school-management/student-dairy-admin/assets/images/profile_av.jpg" class="acher_1">view attachment</a>
                                        </td>
                                        <td>Approved</td>
                                        <td>
                                          <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>4</td>
                                        <td>Rakesh</td>
                                        <td>Lorem Ipsum is simply ..</td>
                                        <td>1 july 2018 - 5 July 2018</td>
                                        <td>
                                          <a  href="http://v2rsolution.co.in/school/school-management/student-dairy-admin/assets/images/profile_av.jpg" class="acher_1">view attachment</a>
                                        </td>
                                        <td>Disapproved</td>
                                        <td>
                                          <div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Disapproved"> <i class="fas fa-minus-circle"></i> </div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                              
                <div class="tab-pane body" id="login_details" style="margin-left: 20px !important;">
                  <div class="">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" style="background-color: #fff;
    width: 989px !important;">
                          <div class="tab-pane active" id="classlist">
                            <div class="">
                              <div class="card border_info_tabs_1 card_top_border1">
                                <div class="row clearfix">
                                  <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="card">
                                      <div class="body">
                                        <div class="nav_item7">
                                        <div class="nav_item1">
                                          <a class="nav-link"> <i class="zmdi zmdi-home zmdi_zmdi1"></i> Login Info </a>
                                        </div>
                                        <div class="nav_item2">
                                          <div role="tabpanel" class="tab-pane" id="address_with_icon_title">
                                             <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">Name :</b> <span class="profile_detail_span_6">Mahesh Kumar</span>
                                              </div>
                                            </div>

                                             <div class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span><b class="profile_detail_bold_2">Email :</b> <span class="profile_detail_span_6">maheshkumar@gmail.com</span>
                                              </div>
                                            </div>
                                          
                                            <div  class="profile_detail_2">
                                              <div class="demo-google-material-icon"> <i class="material-icons profile_detail_icon_2">radio_button_checked</i> <span class="icon-name"></span>
                                                <b class="profile_detail_bold_2">Contact No :</b> <span class="profile_detail_span_6">+91 9008090080</span>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>


                
                                 
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>



<!-- Content end here  -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>