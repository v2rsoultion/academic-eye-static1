
<?php $__env->startSection('content'); ?>
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2><?php echo trans('language.menu_hostel'); ?></h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/hostel'); ?>"><?php echo trans('language.menu_hostel'); ?></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-4">
                    <div class="dashboard_div" style="height: auto;">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/Hostel/Configuration.svg'); ?>" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Configuration</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="<?php echo e(url('/admin-panel/hostel/configuration/hostel-details')); ?>" class="float-left" title="Hostel-Details ">
                          <i class="fas fa-plus"></i> Hostel Details 
                        </a>
                        <a href="<?php echo e(url('/admin-panel/hostel/configuration/hostel-block')); ?>" class="float-right" title="Hostel-Block ">
                          <i class="fas fa-plus"></i>Hostel Block 
                        </a>
                        <div class="clearfix"></div>
                        <a href="<?php echo e(url('/admin-panel/hostel/configuration/room-details')); ?>" class="cusa" title="Room-Details">
                          <i class="fas fa-plus"></i>Room Details
                        </a>
                        <div class="clearfix"></div>
                        <!-- <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="#" title="">Option 1</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 2</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 3</a>
                              </li>
                            </ul>
                          </li>
                        </ul> -->
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="dashboard_div" style="min-height: 207px;">
                        <div class="imgdash">
                          <img src="<?php echo URL::to('public/assets/images/Hostel/Fees Structure.svg'); ?>" alt="Student" >
                          </div>
                          <h4 class="">
                            <div class="tableCell" style="height: 64px;">
                              <div class="insidetable">Fees Structure</div>
                            </div>
                          </h4>
                          <div class="clearfix"></div>
                          <a href="" class="float-left" title="Add ">
                            <i class="fas fa-plus"></i> Add 
                          </a>
                          <a href="" class="float-right" title="View ">
                            <i class="fas fa-eye"></i>View 
                          </a>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="dashboard_div" style="min-height: 207px;">
                          <div class="imgdash">
                            <img src="<?php echo URL::to('public/assets/images/Hostel/Register Students.svg'); ?>" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Register Students</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <!-- <a href="" class="float-left" title="Add ">
                              <i class="fas fa-plus"></i> Add 
                            </a> -->
                            <a href="<?php echo e(url('/admin-panel/hostel/register-student/view')); ?>" class="cusa" title="View " style="width: 48%; margin: 15px auto;">
                              <i class="fas fa-eye"></i>Manage Student 
                            </a>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="<?php echo URL::to('public/assets/images/Hostel/Allocate Room.svg'); ?>" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Allocate Room</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <!-- <a href="" class="float-left" title="Add ">
                                <i class="fas fa-plus"></i> Add 
                              </a> -->
                              <a href="<?php echo e(url('/admin-panel/hostel/allocate-room/add')); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                <i class="fas fa-plus"></i>Allocate 
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/Hostel/Transferred Student.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Transferred Student</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <!-- <a href="" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a> -->
                                <a href="<?php echo e(url('/admin-panel/hostel/transfer-student/add')); ?>" class="cusa" title="View" style="width: 68%; margin: 15px auto;">
                                  <i class="fas fa-plus"></i>Transfer Student 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Hostel/Leave Room.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Leave Room</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                 <!--  <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a> -->
                                  <a href="<?php echo e(url('/admin-panel/hostel/leave-room/view')); ?>" class="cusa" title="View" style="width: 58%; margin: 15px auto;">
                                        <i class="fas fa-eye"></i>Leave Room
                                      </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                               <div class="col-md-3">
                                  <div class="dashboard_div">
                                    <div class="imgdash">
                                      <img src="<?php echo URL::to('public/assets/images/Hostel/Collect hostal fees.svg'); ?>" alt="Student">
                                      </div>
                                      <h4 class="">
                                        <div class="tableCell" style="height: 64px;">
                                          <div class="insidetable">Collect hostal fees</div>
                                        </div>
                                      </h4>
                                      <div class="clearfix"></div>
                                      <!-- <a href="" class="float-left" title="Add ">
                                        <i class="fas fa-plus"></i> Add 
                                      </a> -->
                                      <a href="<?php echo e(url('/admin-panel/hostel/hostel-fees/view')); ?>" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                        <i class="fas fa-eye"></i>Hostel Fees 
                                      </a>
                                      <div class="clearfix"></div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-4">
                                    <div class="dashboard_div" style="height: auto;">
                                      <div class="imgdash">
                                        <img src="<?php echo URL::to('public/assets/images/Hostel/Report.svg'); ?>" alt="Student">
                                        </div>
                                        <h4 class="">
                                          <div class="tableCell" style="height: 64px;">
                                            <div class="insidetable">Reports</div>
                                          </div>
                                        </h4>
                                        <div class="clearfix"></div>
                                        <a href="<?php echo e(url('/admin-panel/hostel/reports/register-student-report')); ?>" class="float-left" title="" style="width: 48%; margin: 15px auto;"><i class="fas fa-eye"></i>Registered<br> Student Report</a>
                                        <a href="<?php echo e(url('/admin-panel/hostel/reports/free-space-room-report')); ?>" class="float-right" title="" style="width: 48%; margin: 15px auto;"><i class="fas fa-eye"></i>Free Space Room Report</a>
                                       
                                        <div class="clearfix"></div>
                                        <ul class="header-dropdown opendiv">
                                        <li class="dropdown">
                                          <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                          </button>
                                          <ul class="dropdown-menu dropdown-menu-right slideUp">
                                          <li><a href="<?php echo e(url('/admin-panel/hostel/reports/leave-student-report')); ?>" title="">Leave Student Report
                                          </a> </li>
                                          <li> <a href="<?php echo e(url('/admin-panel/hostel/reports/allocation-report')); ?>" title="">Allocation Report</a></li>
                                          <li><a href="<?php echo e(url('/admin-panel/hostel/reports/fees-due-report')); ?>" title="">Fees Due Report</a> </li>
                                        </ul>
                                      </li>
                                    </ul> 
                                      </div>
                                    </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>