@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2>View Reports</h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">{!! trans('language.menu_fee_collection') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">Reports</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection/report/view-reports') !!}">View Reports</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                   <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/FeesCollection/Class Wise Report.svg') !!}" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Class Wise</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/fees-collection/report/view-reports/class-sheet-report') }}" class="cusa" title="Add" style="width: 48%; margin: 15px auto;">
                      <i class="fas fa-eye"></i>View
                      </a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
              
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/FeesCollection/Student Wise Report.svg') !!}" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Student Wise</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/fees-collection/report/view-reports/student-sheet-report') }}" class="cusa" title="Add " style="width: 48%; margin: 15px auto;">
                      <i class="fas fa-eye"></i> View
                      </a>
                     
                        <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/FeesCollection/ClassWise Admission Report.svg') !!}" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Classwise Admission Report</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/fees-collection/report/view-reports/classwise-admission-report') }}" class="cusa" title="Add " style="width: 48%; margin: 15px auto;">
                      <i class="fas fa-eye"></i> View
                      </a>
                     
                        <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/FeesCollection/FormWise Fees Report.svg') !!}" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Formwise Fees Report</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/fees-collection/report/view-reports/formwise-fees-report') }}" class="cusa" title="Add " style="width: 48%; margin: 15px auto;">
                      <i class="fas fa-eye"></i> View
                      </a>
                     
                        <div class="clearfix"></div>
                    </div>
                  </div>  
                </div> 
                <div class="row">
                 <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/FeesCollection/Attendance Report.svg') !!}" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Attendance Report</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/fees-collection/report/view-reports/attendance-report') }}" class="cusa" title="Add " style="width: 48%; margin: 15px auto;">
                      <i class="fas fa-eye"></i> View
                      </a>
                     
                        <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/FeesCollection/Fees Wise Report.svg') !!}" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Fees Wise Report</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/fees-collection/report/view-reports/fees-wise-report') }}" class="cusa" title="Add " style="width: 48%; margin: 15px auto;">
                      <i class="fas fa-eye"></i> View
                      </a>
                     
                        <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/FeesCollection/Subjects wise marks Report.svg') !!}" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Subjects Wise Marks Report</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/fees-collection/report/view-reports/subject-wise-marks-report') }}" class="cusa" title="Add " style="width: 48%; margin: 15px auto;">
                      <i class="fas fa-eye"></i> View
                      </a>
                     
                        <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/FeesCollection/Exam wise marks Report.svg') !!}" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Exam Wise Marks Report</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/fees-collection/report/view-reports/exam-wise-marks-report') }}" class="cusa" title="Add " style="width: 48%; margin: 15px auto;">
                      <i class="fas fa-eye"></i> View
                      </a>
                     
                        <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="{!! URL::to('public/assets/images/FeesCollection/Staff Attendance Report.svg') !!}" alt="Student">
                      </div>
                      <h4 class="">
                        <div class="tableCell" style="height: 64px;">
                          <div class="insidetable">Staff Attendance Report</div>
                        </div>
                      </h4>
                      <div class="clearfix"></div>
                      <a href="{{ url('/admin-panel/fees-collection/report/view-reports/staff-attendance-report') }}" class="cusa" title="Add " style="width: 48%; margin: 15px auto;">
                      <i class="fas fa-eye"></i> View
                      </a>
                     
                        <div class="clearfix"></div>
                    </div>
                  </div>
                </div>         
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    <div class="clearfix"></div>
  </div>
  </div>
</section>
@endsection

