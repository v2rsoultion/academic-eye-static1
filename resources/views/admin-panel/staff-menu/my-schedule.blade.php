@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
  .table-responsive {
    overflow-x: visible !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>My Schedule</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff-menu/my-schedule') !!}">My Schedule</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                 
                   
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Staff-Login/MY Schedule/Daily Schedule.svg') !!}" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Daily Schedule</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                           
                            <a href="{{ url('admin-panel/staff-menu/my-schedule/daily-schedule') }}" class="cusa" title="View" style="width: 50%; margin: 15px auto;">
                              <i class="fas fa-eye"></i>Schedule 
                            </a>
                            <div class="clearfix"></div>
                            
                        <div class="row clearfix"></div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="{!! URL::to('public/assets/images/Staff-Login/MY Schedule/Duty.svg') !!}" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Exam Duty</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                             <!--  <a href="{{ url('/admin-panel/student-leave-application/add-leave-application') }}" class="float-left" title="Add ">
                                <i class="fas fa-plus"></i> Add 
                              </a> -->
                              <a href="{{ url('/admin-panel/staff-menu/my-schedule/exam-duty') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                <i class="fas fa-eye"></i>View 
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                            <div class="col-md-3">
                          
                        </div>
                      
                                
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </section>
@endsection
