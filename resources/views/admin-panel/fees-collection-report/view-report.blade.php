@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
   .table-responsive {
    overflow-x: visible !important;
   }
   .card .body .table td, .cshelf1 {
    width: 100px !important;
   }
</style>
<!--  Main content here -->
<section class="content">
   <div class="block-header">
      <div class="row">
         <div class="col-lg-5 col-md-6 col-sm-12">
            <h2>Report</h2>
         </div>
         <div class="col-lg-7 col-md-6 col-sm-12 line">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/fees-collection') !!}">{!! trans('language.menu_fee_collection') !!}</a></li>
              <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/fees-collection/prepaid-account/manage-account') !!}">Report</a></li>
            </ul>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row clearfix">
         <div class="col-lg-12" id="bodypadd">
            <div class="tab-content">
               <div class="tab-pane active" id="classlist">
                  <div class="card">
                  <div class="body form-gap">
                   <!-- <div class="row"> -->
                  <form class="" action="" method="" id="searchpanel" style="width: 100%;">
                    <div class="row tabless">
                       <div class="col-lg-3">
                        <div class="form-group">
                           <lable class="from_one1" for="name">Account</lable>
                           <select class="form-control show-tick select_form1" name="classes" id="">
                              <option value="">Select Account</option>
                              <option value="1">Rajesh Kumar</option>
                              <option value="2">Mahesh Kumar</option>
                              <option value="3">Raj Kumar</option>
                              <option value="4">Raju Kumar</option>
                           </select>
                        </div>
                     </div>
              
                     <div class="col-lg-3">
                        <div class="form-group">
                           <lable class="from_one1" for="name">Class</lable>
                           <select class="form-control show-tick select_form1" name="classes" id="">
                              <option value="">Select Class</option>
                              <option value="1"> 1st</option>
                              <option value="2">2nd</option>
                              <option value="3">3rd</option>
                              <option value="4">4th</option>
                              <option value="10th"> 10th</option>
                              <option value="11th">11th</option>
                              <option value="12th">12th</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="form-group">
                           <lable class="from_one1" for="name">Section</lable>
                           <select class="form-control show-tick select_form1" name="classes" id="">
                              <option value="">Select Section</option>
                              <option value="A">A</option>
                              <option value="B">B</option>
                              <option value="C">C</option>
                              <option value="D">D</option>
                              
                           </select>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-12">
                            <button type="submit" class="btn btn-raised  btn-primary saveBtn">Search</button>
                            <button type="submit" class="btn btn-raised  btn-primary cancelBtn">Clear</button>
                        </div>
                     <div class="clearfix"></div>
                     </div> 
                     <hr>
                  </form>
                  <div class="table-responsive">
                  <!--  DataTable for view Records  -->
                  <table class="table  m-b-0 c_list">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th style="width: 130px;">Name</th>
                        <th>Father Name</th>
                        <th>Class-Section</th>
                        <th>Total Balance</th>
                        <th>Total Deduction</th>
                        <th>Remaining Balance </th>
                        <th>View Details</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $count = 1; for ($i=0; $i < 15; $i++) {  ?>
                      <tr>
                        <td><?php echo $count; ?> </td>
                       <td>
                          <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="20%"> Ankit Dave
                        </td>
                        <td>Mahesh Kumar</td>
                        <td>XII-A</td>
                        <td>Rs: 20000</td>
                        <td>Rs: 500</td>
                        <td>Rs: 15000</td>
                        <td class="text-center">
                          <div class="dropdown">
                            <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                              <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                              <li>
                                <a href="#" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#EntriesModel" style="background: transparent !important;">Entries</a>
                              </li>
                              <li>
                                <a href="#" data-backdrop="static" data-keyboard="false" class="btn btn-primary actinvtnn" data-toggle="modal" data-target="#deductionModel" style="background: transparent !important;">Deduction</a>
                              </li>
                            </ul>
                          </div>
                        </td>
                        <td class="text-center">
                          <div class="dropdown">
                            <button class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="zmdi zmdi-label"></i>
                              <span class="zmdi zmdi-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                              <li>
                                <a href="#" >Generate Receipt</a>
                              </li>
                            </ul>
                          </div>
                        </td>
                      </tr>
                      <?php $count++; } ?>
                    </tbody>
                  </table>
                
              </div>          
             </div>
            </div>
           </div>
          </div>
        </div>
       </div>
      </div>
  
</section>
<!-- Content end here  -->
@endsection
<!-- Model Data -->
<div class="modal fade" id="EntriesModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Entries</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row tabless">
           <table class="table  m-b-0 c_list">
            <thead>
              <tr>
                <th>#</th>
                <th>Amount</th>
                <th>Date</th>
                <!-- <th class="text-center">Action</th> -->
              </tr>
            </thead>
            <tbody>
              <?php $count= 1; for ($i=0; $i < 5; $i++) {  ?>
              <tr>
                <td><?php echo $count; ?></td>
                <td>Rs: 1588</td>
                <td>02-10-2017</td>
                <!-- <td class="text-center">
                  <button type="button" class="btn btn-primary actinvtnn"  style="background: transparent !important;     padding: 1px 10px;">
                  <i class="zmdi zmdi-edit"></i>
                  </button>
                  <button type="button" class="btn btn-primary actinvtnn" style="background: transparent !important ;     padding: 1px 10px;color: red !important;">
                  <i class="zmdi zmdi-delete"></i>
                  </button>
                </td> -->
              </tr>
              <?php $count++; } ?>
            </tbody>
          </table>
       
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Model Data -->
<div class="modal fade" id="deductionModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Deduction</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row tabless">
           <table class="table  m-b-0 c_list">
            <thead>
              <tr>
                <th>#</th>
                <th>Head Name</th>
                <th>Amount</th>
                <!-- <th class="text-center">Action</th> -->
              </tr>
            </thead>
            <tbody>
              <?php $count= 1; for ($i=0; $i < 5; $i++) {  ?>
              <tr>
                <td><?php echo $count; ?></td>
                <td>Exam Fees</td>
                <td>Rs: 1588</td>
                <!-- <td class="text-center">
                  <button type="button" class="btn btn-primary actinvtnn"  style="background: transparent !important; padding: 1px 10px;">
                  <i class="zmdi zmdi-edit"></i>
                  </button>
                  <button type="button" class="btn btn-primary actinvtnn" style="background: transparent !important ;     padding: 1px 10px;color: red !important;">
                  <i class="zmdi zmdi-delete"></i>
                  </button>
                </td> -->
              </tr>
              <?php $count++; } ?>
            </tbody>
          </table>
       
        </div>
      </div>
    </div>
  </div>
</div>
