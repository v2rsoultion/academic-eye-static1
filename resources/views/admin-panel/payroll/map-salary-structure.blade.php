@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
  .checkbox label, .radio label{
        line-height: 19px;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Map Structure</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/payroll') !!}">Payroll</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-salary-structure') !!}">Salary Structure</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/payroll/manage-salary-structure/map-structure') !!}">Map Structure</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body form-gap">
                 <div class="headingcommon  col-lg-12" style="margin-left: -13px">Map Structure :-</div>
                <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">Search By :-</div> -->
                  <form class="" action="" method="" id="" style="width: 100%;">
                    <div class="row" >
                          <div class="col-lg-3">
                            <div class="form-group">
                              <!-- <lable class="from_one1">Structure</lable> -->
                              <select class="form-control show-tick select_form1" name="" id="">
                                <option value="">Select Structure</option>
                                <option value="1">Structure-1</option>
                                <option value="2">Structure-2</option>
                                <option value="3">Structure-3</option>
                                <option value="4">Structure-4</option>
                              </select>
                            </div>
                          </div>
                     
                      <div class="col-lg-1">
                        <button type="submit" class="btn btn-raised btn-primary " title="Search">Search
                        </button>
                      </div>
                        <div class="col-lg-1">
                        <button type="reset" class="btn btn-raised btn-primary " title="Clear">Clear
                        </button>
                      </div>
                    </div>
                  </form>
                   <!-- <hr> -->
                    <!--  DataTable for view Records  -->
                    <div class="">
                    <table class="table m-b-0 c_list" id="tablche" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th style="width: 400px">Employee Name</th>
                          <th>Basic Pay</th>
                       </tr>
                      </thead>
                      <tbody>
                        
                        <tr>
                          <td >
                            <div class="checkbox">
                               <input id="checkbox1" type="checkbox">
                               <label class="from_one1" for="checkbox1" style= "margin-bottom: 4px !important;"></label>
                            </div>
                          </td>
                          <td>
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="12%">
                           Ankit Dave
                        </td>
                         <td>12000</td> 
                        </tr>
                        <tr>
                          <td >
                            <div class="checkbox">
                               <input id="checkbox2" type="checkbox">
                               <label class="from_one1" for="checkbox2" style= "margin-bottom: 4px !important;"></label>
                            </div>
                          </td>
                          <td>
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="12%">
                           Ankit Dave
                        </td>
                         <td>12000</td> 
                        </tr>
                        <tr>
                          <td >
                            <div class="checkbox">
                               <input id="checkbox3" type="checkbox">
                               <label class="from_one1" for="checkbox3" style= "margin-bottom: 4px !important;"></label>
                            </div>
                          </td>
                          <td>
                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="12%">
                           Ankit Dave
                        </td>
                         <td>---</td> 
                        </tr>
                      </tbody>
                    </table>
                  </div>
                    <div class="col-lg-12">
                       <button type="button" class="btn btn-primary float-right" title="Save">Save</button>
                    </div>
                  </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
<!-- Content end here  -->
@endsection