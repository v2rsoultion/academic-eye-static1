<?php if(isset($room_no['room_no_id']) && !empty($room_no['room_no_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?>

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
    /*.theme-blush .btn-primary {
    margin-top: 4px !important;
}*/
</style>

<?php echo Form::hidden('room_no_id',old('room_no_id',isset($room_no['room_no_id']) ? $room_no['room_no_id'] : ''),['class' => 'gui-input', 'id' => 'room_no_id', 'readonly' => 'true']); ?>

<p class="red">
<?php if($errors->any()): ?>
    <?php echo e($errors->first()); ?>

<?php endif; ?>
</p>
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <lable class="from_one1"><?php echo trans('language.room_no_add_page'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('room_no', old('room_no',isset($room_no['room_no']) ? $room_no['room_no']: ''), ['class' => 'form-control','placeholder'=>trans('language.room_no_add_page'), 'id' => 'room_no']); ?>

        </div>
        <?php if($errors->has('room_no')): ?> <p class="help-block"><?php echo e($errors->first('room_no')); ?></p> <?php endif; ?>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']); ?>

        <a href="<?php echo url('admin-panel/dashboard'); ?>" ><?php echo Form::button('Cancel', ['class' => 'btn btn-raised btn-round']); ?></a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("specialChars", function( value, element ) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var key = value;

        if (!regex.test(key)) {
           return false;
        }
        return true;
        }, "please use only alphanumeric or alphabetic characters");

        $("#room-no-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation  rules 
             ------------------------------------------ */

            rules: {
                room_no: {
                    required: true,
                    specialChars:true
                }
            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>