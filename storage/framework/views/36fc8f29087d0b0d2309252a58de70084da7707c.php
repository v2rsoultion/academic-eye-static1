
<?php $__env->startSection('content'); ?>
<style type="text/css">
  td{
    padding: 12px !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>View Profit & Loss Account</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/account'); ?>">Accounts</a></li>
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/account/view-profit-loss-account'); ?>">View Profit & Loss Account</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12" id="bodypadd">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card gap_bottom">
              <div class="body form-gap">
                  <!-- <div class="headingcommon  col-lg-12" style="margin-left: -13px">View Journal Entries :-</div> -->
                  <div class="table-responsive">
                    <table class="bold" style="width:100%;float: left;">
                    <?php echo e(csrf_field()); ?>

                    <thead style="font-size: 14px;">
                      <tr style="border: 1px solid #424242;">
                        <th style="width: 310px;" class="gap_left">Particulars</th>
                        <th style="width: 230px;" class="text-center border_right">Chirag & Co<br> 1-Apr-2018 to 31-Mar-2019</th>
                        <th style="width: 310px;" class="gap_left">Particulars</th>
                        <th style="width: 230px;" class="text-center border_right">Chirag & Co<br> 1-Apr-2018 to 31-Mar-2019</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="border_left">Gross Profit c/o</td>
                        <td class="text-right border_right">9,99,99,600.00</td>
                        <td class="border_left">Direct Incomes</td>
                        <td class="text-right border_right">9,99,98,000.00</td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                        <td class="border_left"> Direct Expenses</td>
                        <td class="text-right border_right"> 1,600.00</td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td class="text-right border_right border_top border_bottom">9,99,99,600.00</td>
                        <td class="border_left"> </td>
                        <td class="text-right border_right border_top border_bottom">9,99,99,600.00</td>
                      </tr>
                      <tr>
                        <td class="border_left">Net profit</td>
                        <td class="text-right border_right">9,99,99,600.00</td>
                        <td class="border_left">Gross Profit b/f</td>
                        <td class="text-right border_right">9,99,99,600.00</td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                      </tr>
                      <tr>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                        <td class="border_left"> </td>
                        <td class="border_right"> </td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr class="border">
                        <td>Total</td>
                        <td class="text-right border_right">9,99,99,600.00</td>
                        <td>Total</td>
                        <td class="text-right">9,99,99,600.00</td>
                      </tr> 
                    </tfoot>
                  </table>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!-- Content end here  -->


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>