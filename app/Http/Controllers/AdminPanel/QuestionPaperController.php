<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\QuestionPaper\QuestionPaper; // Model
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\Datatables\Datatables;
use URL;

class QuestionPaperController extends Controller
{
     public function index() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.question-paper.index')->with($data);
     }

     public function add() {
        $loginInfo   = get_loggedin_user_data();
        $page_title  = 'Add Question Paper';
        $save_url    = url('admin-panel/virtual-class/save/');
        $submit_button = 'Save';
        $question_paper =[];
        $data                 = array(
            'login_info'      => $loginInfo,
            'page_title'	  => $page_title,
            'save_url'		  => $save_url,	
            'submit_button'	  => $submit_button,
            'question_paper'  => $question_paper,
        );
        
        return view('admin-panel.question-paper.add')->with($data);
     }
}