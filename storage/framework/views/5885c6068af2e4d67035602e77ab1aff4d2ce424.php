<?php if(isset($section['section_id']) && !empty($section['section_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?>

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
    /*.theme-blush .btn-primary {
    margin-top: 4px !important;
}*/
</style>

<?php echo Form::hidden('section_id',old('section_id',isset($section['section_id']) ? $section['section_id'] : ''),['class' => 'gui-input', 'id' => 'section_id', 'readonly' => 'true']); ?>


<p class="red">
<?php if($errors->any()): ?>
    <?php echo e($errors->first()); ?>

<?php endif; ?>
</p>
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.medium_type'); ?>  :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('medium_type', $section['arr_medium'],isset($section['medium_type']) ? $section['medium_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type', 'onChange'=>'getClass(this.value)']); ?>

                <i class="arrow double"></i>
            </label>
        </div>
        <?php if($errors->has('medium_type')): ?> <p class="help-block"><?php echo e($errors->first('medium_type')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.section_class_id'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class="form-group field select" style="width: 100%">
                <?php echo Form::select('class_id', $section['arr_class'],isset($section['class_id']) ? $section['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id']); ?>

                <i class="arrow double"></i>
            </label>
        </div>
        <?php if($errors->has('class_id')): ?> <p class="help-block"><?php echo e($errors->first('class_id')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.section_name'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('section_name', old('section_name',isset($section['section_name']) ? $section['section_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.section_name'), 'id' => 'section_name']); ?>

        </div>
        <?php if($errors->has('section_name')): ?> <p class="help-block"><?php echo e($errors->first('section_name')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.section_intake'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::number('section_intake', old('section_intake',isset($section['section_intake']) ? $section['section_intake']: 20), ['class' => 'form-control ','placeholder'=>trans('language.section_intake'), 'id' => 'section_order', 'min' => 0]); ?>

        </div>
        <?php if($errors->has('section_intake')): ?> <p class="help-block"><?php echo e($errors->first('section_intake')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.section_order'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::number('section_order', old('section_order',isset($section['section_order']) ? $section['section_order']: 0), ['class' => 'form-control ','placeholder'=>trans('language.section_order'), 'id' => 'section_order', 'min' => 0]); ?>

        </div>
        <?php if($errors->has('section_order')): ?> <p class="help-block"><?php echo e($errors->first('section_order')); ?></p> <?php endif; ?>
    </div>

</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']); ?>

        <a href="<?php echo url('admin-panel/dashboard'); ?>" ><?php echo Form::button('Cancel', ['class' => 'btn btn-raised btn-round']); ?></a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#section-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation  rules 
             ------------------------------------------ */

            rules: {
                medium_type: {
                    required: true,
                },
                class_id: {
                    required: true,
                },
                section_name: {
                    required: true,
                    lettersonly:true
                },
                section_intake: {
                    required: true,
                    digits: true
                },
            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
    });

    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/class/get-class-data')); ?>",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='class_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='class_id'").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }

    

</script>