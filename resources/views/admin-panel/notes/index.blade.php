@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    td {
        padding: 10px !important;
    }
</style>
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h2>{!! trans('language.view_notes') !!}</h2>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('admin-panel/menu/online-content') }}">{!! trans('language.online_content') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('admin-panel/menu/online-content') }}">{!! trans('language.notes') !!}</a></li>
                    <li class="breadcrumb-item "><a href="{{ url('admin-panel/notes/view-notes') }}">{!! trans('language.view_notes') !!}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">{!! trans('language.title') !!}</a></li>
                                    <li class="nav-item"><a class="nav-link"  href="{{ url('admin-panel/title/add-title') }}">{!! trans('language.add_title') !!}</a></li>
                                </ul>                        
                            </div> -->
                            <div class="body form-gap">
                                {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                                    <div class="row clearfix">                                     
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('class_id', $listData['arr_class'],isset($listData['class_id']) ? $listData['class_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'class_id','onChange' => 'getSection(this.value)'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                            @if($errors->has('class_id')) <p class="help-block">{{ $errors->first('class_id') }}</p> @endif
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <label class=" field select" style="width: 100%">
                                                {!!Form::select('subject_id', $listData['arr_subject'],isset($listData['subject_id']) ? $listData['subject_id'] : '', ['class' => 'form-control show-tick select_form1','id'=>'subject_id'])!!}
                                                <i class="arrow double"></i>
                                            </label>
                                            @if($errors->has('subject_id')) <p class="help-block">{{ $errors->first('subject_id') }}</p> @endif
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::submit('Search', ['class' => 'btn btn-raised  btn-primary ','name'=>'Search']) !!}
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            {!! Form::button('Clear', ['class' => 'btn btn-raised  btn-primary ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <div class="">
                                <table class="table m-b-0 c_list" id="" style="width:100%">
                                {{ csrf_field() }}
                                    <thead>
                                        <tr>
                                            <th>{{trans('language.s_no')}}</th>
                                            <th>{{trans('language.t_name')}}</th>
                                            <th>{{trans('language.class_section')}}</th>
                                            <th>{{trans('language.subject')}}</th>
                                            <th>{{trans('language.notes_unit')}}</th>
                                            <th>{{trans('language.notes_topic')}}</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Mahesh kumar</td>
                                            <td>I-A</td>
                                            <td>Hindi</td>
                                            <td>Unit-1</td>
                                            <td>chapter</td>
                                            <td>  <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>  
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Ravi Mishra</td>
                                            <td>X-A</td>
                                            <td>Computer</td>
                                            <td>Unit-5</td>
                                            <td>Hardware</td>
                                            <td>  <div class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>  
                                        </tr>
                                    </tbody>
                                </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#notes-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
        
            ajax: {
                url: '{{url('admin-panel/notes/data')}}',
                data: function (d) {
                    d.class_id   = $('select[name=class_id]').find(':selected').val();
                    d.subject_id = $('select[name=subject_id]').find(':selected').val();
                    // d.class_id = $('select[name="class_id"]').val();
                }
            },
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'online_note_name', name: 'online_note_name'},
                {data: 'class_section', name: 'class_section'},
                {data: 'class_subject', name: 'class_subject'},
                {data: 'online_note_unit', name: 'online_note_unit'},
                {data: 'online_note_topic', name: 'online_note_topic'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    targets: [ 0, 1, 2, 3, 4, 5, 6],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            $("select[name='class_id'").selectpicker('refresh');
            $("select[name='subject_id'").selectpicker('refresh');
            table.draw();
            e.preventDefault();
        })
    });


</script>
@endsection




