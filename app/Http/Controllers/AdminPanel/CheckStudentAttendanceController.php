<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckStudentAttendanceController extends Controller
{
    /**
     *  View page for Check Student Attendance
     *  @Khushbu on 01 Oct 2018   
    **/
    public function index()
    {
        $loginInfo 		    = get_loggedin_user_data();
        
        $data 				= array(
            'page_title'    => trans('language.view_student_attendance'),
            'redirect_url'  => url('admin-panel/transport/student-attendance/view-student-attendance'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.check-student-attendance.view-student-attendance')->with($data);
    }
    /**
     *  Add page for Student Attendance
     *  @Khushbu on 01 Oct 2018   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        if (!empty($id))
        {
            $page_title             = trans('language.edit_student_attendance');
            $save_url               = url('admin-panel/transport/student-attendance/save/' . $decrypted_job_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_student_attendance');
            $save_url      = url('admin-panel/transport/student-attendance/save');
            $submit_button = 'Save';
        }

        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/student-attendance/view-student-attendance'),
        );
        return view('admin-panel.check-student-attendance.student-attendance')->with($data);
    }
}
