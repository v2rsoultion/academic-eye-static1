<?php

namespace App\Model\Holiday;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    protected $table      = 'holidays';
    protected $primaryKey = 'holiday_id';
}
