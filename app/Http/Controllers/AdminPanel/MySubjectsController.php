<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MySubjectsController extends Controller
{
    public function viewStudents() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.my-subjects.view-students')->with($data);
    }
    public function viewHomework() {
         $loginInfo   = get_loggedin_user_data();
         $time = time();
         $messageOutput = "hello mam";
        $data                 =  array(
            'login_info'      => $loginInfo,
            'time'			  => $time,	
            'messageOutput'   => $messageOutput,
        );
        
        return view('admin-panel.my-subjects.view-homework')->with($data);
    }

     public function viewRemark() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.my-subjects.view-remark')->with($data);
    }
}
