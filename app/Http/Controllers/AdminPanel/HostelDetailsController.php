<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HostelDetailsController extends Controller
{
    public function hostelDetails() {
    	 $loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.hostel-configuration.view-hostel-details')->with($data);
    }
}
