<?php $__env->startSection('content'); ?>
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2><?php echo trans('language.edit_staff_attendance'); ?></h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <a href="<?php echo url('admin-panel/staff/staff-attendance/add-staff-attendance'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/staff'); ?>"><?php echo trans('language.menu_staff'); ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/staff'); ?>"><?php echo trans('language.staff_attendance'); ?></a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/staff/staff-attendance/edit-staff-attendance'); ?>"><?php echo trans('language.edit_staff_attendance'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#"><?php echo trans('language.student'); ?></a></li>
                                    <li class="nav-item"><a class="nav-link"  href="<?php echo e(url('/admin-panel/student/add-student')); ?>"><?php echo trans('language.add_student'); ?></a></li>
                                </ul>                        
                            </div> -->
                            <div class="body form-gap ">
                                <?php echo Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']); ?>

                                    <div class="row clearfix">
                                       <div class="col-lg-3">
                                        <div class="form-group">
                                          <lable class="from_one1" for="name">Date</lable>
                                          <input type="text" name="name" id="staff_date" class="form-control" placeholder="Date">
                                        </div>
                                      </div> 
                                
                                        <div class="col-lg-1 col-md-1">
                                             <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Serach">Serach
                                         </button>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Cancel">Cancel
                                        </button>
                                        </div>
                                    </div>
                                <?php echo Form::close(); ?>

                                <div class="float-right">
                          <button type="submit" class="btn btn-raised btn-primary" style="margin-top: -68px !important;" title="Save">Save
                          </button>
                        </div>
                                <div class="table-responsive">
                            <table class="table m-b-0 c_list" id="#" style="width:100%">
                            <?php echo e(csrf_field()); ?>

                                <thead>
                                    <tr>
                                        <th><?php echo e(trans('language.s_no')); ?></th>
                                        <th>Staff Id</th>
                                        <th>Staff Name</th>
                                        <th>Date</th>
                                        <th>Attendance</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                          <td>1</td>
                                          <td>Staff101</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td><div class="radio" style="margin-top:6px !important;"><input id="radio31" name="staff_marital_status" type="radio" value="1" multiple="multiple">
                                            <label for="radio31" class="document_staff">Present</label>
                                            <input id="radio32" checked="checked" name="staff_marital_status" type="radio" value="0">
                                            <label for="radio32" class="document_staff">Absent</label> </div></td>
                                        </tr>
                                          <tr>
                                          <td>2</td>
                                          <td>Staff102</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td><div class="radio" style="margin-top:6px !important;"><input id="radio33" name="staff_marital_status" type="radio" value="1" multiple="multiple">
                                            <label for="radio33" class="document_staff">Present</label>
                                            <input id="radio34" checked="checked" name="staff_marital_status" type="radio" value="0">
                                            <label for="radio34" class="document_staff">Absent</label> </div></td>
                                        </tr>
                                        <tr>
                                          <td>3</td>
                                          <td>Staff103</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td><div class="radio" style="margin-top:6px !important;"><input id="radio35" name="staff_marital_status" type="radio" value="1" multiple="multiple">
                                            <label for="radio35" class="document_staff">Present</label>
                                            <input id="radio36" checked="checked" name="staff_marital_status" type="radio" value="0">
                                            <label for="radio36" class="document_staff">Absent</label> </div></td>
                                        </tr>
                                        <tr>
                                          <td>4</td>
                                          <td>Staff104</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td><div class="radio" style="margin-top:6px !important;"><input id="radio37" name="staff_marital_status" type="radio" value="1" multiple="multiple">
                                            <label for="radio37" class="document_staff">Present</label>
                                            <input id="radio38" checked="checked" name="staff_marital_status" type="radio" value="0">
                                            <label for="radio38" class="document_staff">Absent</label> </div></td>
                                        </tr>
                                       <tr>
                                          <td>5</td>
                                          <td>Staff105</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td><div class="radio" style="margin-top:6px !important;"><input id="radio39" name="staff_marital_status" type="radio" value="1" multiple="multiple">
                                            <label for="radio39" class="document_staff">Present</label>
                                            <input id="radio40" checked="checked" name="staff_marital_status" type="radio" value="0">
                                            <label for="radio40" class="document_staff">Absent</label> </div></td>
                                        </tr>
                                          <tr>
                                          <td>6</td>
                                          <td>Staff106</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td><div class="radio" style="margin-top:6px !important;"><input id="radio41" name="staff_marital_status" type="radio" value="1" multiple="multiple">
                                            <label for="radio41" class="document_staff">Present</label>
                                            <input id="radio42" checked="checked" name="staff_marital_status" type="radio" value="0">
                                            <label for="radio42" class="document_staff">Absent</label> </div></td>
                                        </tr>
                                          <tr>
                                          <td>7</td>
                                          <td>Staff107</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td><div class="radio" style="margin-top:6px !important;"><input id="radio43" name="staff_marital_status" type="radio" value="1" multiple="multiple">
                                            <label for="radio43" class="document_staff">Present</label>
                                            <input id="radio44" checked="checked" name="staff_marital_status" type="radio" value="0">
                                            <label for="radio44" class="document_staff">Absent</label> </div></td>
                                        </tr>
                                          <tr>
                                          <td>8</td>
                                          <td>Staff108</td>
                                           <td width="20px" >
                                           <img src="http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg" class="rounded-circle" width="10%">
                                           Ankit Dave
                                            </td>
                                            <td>20-09-2018</td>
                                            <td><div class="radio" style="margin-top:6px !important;"><input id="radio45" name="staff_marital_status" type="radio" value="1" multiple="multiple">
                                            <label for="radio45" class="document_staff">Present</label>
                                            <input id="radio46" checked="checked" name="staff_marital_status" type="radio" value="0">
                                            <label for="radio46" class="document_staff">Absent</label> </div></td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <div class="float-right">
                          <button type="submit" class="btn btn-raised btn-primary" style="margin-top: 23px !important;" title="Save">Save
                          </button>
                        </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#staff-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '<?php echo e(url('admin-panel/staff/data')); ?>',
                data: function (d) {
                    d.class_id = $('input[name="name"]').val();
                    d.section_id = $('select[name="designation_id"]').val();
                }
            },
            //ajax: '<?php echo e(url('admin-panel/class/data')); ?>',
           
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'staff_name', name: 'staff_name'},
                {data: 'staff_designation_name', name: 'staff_designation_name'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 3, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2, 3 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });
        $('#clearBtn').click(function(){
            document.getElementById('search-form').reset();
            table.draw();
            e.preventDefault();
        })


    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

</script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>