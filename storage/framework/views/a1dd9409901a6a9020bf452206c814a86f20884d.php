
<?php $__env->startSection('content'); ?>

<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
</style>
<!--  Main content here -->

<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-7 col-md-6 col-sm-12">
        <h2><?php echo trans('language.menu_academic'); ?></h2>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
          <li class="breadcrumb-item"><?php echo trans('language.menu_academic'); ?></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                  <div class="col-md-3">
                    <div class="dashboard_div">
                      <div class="imgdash">
                        <img src="<?php echo URL::to('public/assets/images/Academic/Class.svg'); ?>" alt="Student">
                        </div>
                        <h4 class="">
                          <div class="tableCell" style="height: 64px;">
                            <div class="insidetable">Class</div>
                          </div>
                        </h4>
                        <div class="clearfix"></div>
                        <a href="<?php echo e(url('/admin-panel/class/add-class')); ?>" class="float-left" title="Add ">
                          <i class="fas fa-plus"></i> Add 
                        </a>
                        <a href="<?php echo e(url('/admin-panel/class/view-classes')); ?>" class="float-right" title="View ">
                          <i class="fas fa-eye"></i>View 
                        </a>
                        <div class="clearfix"></div>
                        <!-- <ul class="header-dropdown opendiv">
                          <li class="dropdown">
                            <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right slideUp">
                              <li>
                                <a href="#" title="">Option 1</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 2</a>
                              </li>
                              <li>
                                <a href="#" title="">Option 3</a>
                              </li>
                            </ul>
                          </li>
                        </ul> -->
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="dashboard_div">
                        <div class="imgdash">
                          <img src="<?php echo URL::to('public/assets/images/Academic/Section.svg'); ?>" alt="Student" >
                          </div>
                          <h4 class="">
                            <div class="tableCell" style="height: 64px;">
                              <div class="insidetable">Section</div>
                            </div>
                          </h4>
                          <div class="clearfix"></div>
                          <a href="<?php echo e(url('/admin-panel/section/add-section')); ?>" class="float-left" title="Add ">
                            <i class="fas fa-plus"></i> Add 
                          </a>
                          <a href="<?php echo e(url('/admin-panel/section/view-sections')); ?>" class="float-right" title="View ">
                            <i class="fas fa-eye"></i>View 
                          </a>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="<?php echo URL::to('public/assets/images/Academic/ClassTeacherAllocation.svg'); ?>" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Class Teacher Allocation</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="<?php echo e(url('/admin-panel/class-teacher-allocation/allocate-class')); ?>" class="float-left" title="Add ">
                              <i class="fas fa-plus"></i> Add 
                            </a>
                            <a href="<?php echo e(url('/admin-panel/class-teacher-allocation/view-allocations')); ?>" class="float-right" title="View ">
                              <i class="fas fa-eye"></i>View 
                            </a>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="<?php echo URL::to('public/assets/images/Academic/Co-Scholastics.svg'); ?>" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Co-Scholastics</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="<?php echo e(url('/admin-panel/subject/add-type-of-co-scholastic')); ?>" class="float-left" title="Add ">
                                <i class="fas fa-plus"></i> Add 
                              </a>
                              <a href="<?php echo e(url('/admin-panel/subject/view-type-of-co-scholastic')); ?>" class="float-right" title="View ">
                                <i class="fas fa-eye"></i>View 
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="<?php echo URL::to('public/assets/images/Academic/Subject.svg'); ?>" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Subject</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="<?php echo e(url('/admin-panel/subject/add-subject')); ?>" class="float-left" title="Add ">
                                  <i class="fas fa-plus"></i> Add 
                                </a>
                                <a href="<?php echo e(url('/admin-panel/subject/view-subjects')); ?>" class="float-right" title="View ">
                                  <i class="fas fa-eye"></i>View 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="<?php echo URL::to('public/assets/images/Academic/MapSubjectToClass.svg'); ?>" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Map Subject To Class</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <a href="" class="float-left" title="Add ">
                                    <i class="fas fa-plus"></i> Add 
                                  </a>
                                  <a href="" class="float-right" title="View ">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <div class="dashboard_div">
                                  <div class="imgdash">
                                    <img src="<?php echo URL::to('public/assets/images/Academic/MapSubjectToteacher.svg'); ?>" alt="Student">
                                    </div>
                                    <h4 class="">
                                      <div class="tableCell" style="height: 64px;">
                                        <div class="insidetable">Map Subject To Teacher</div>
                                      </div>
                                    </h4>
                                    <div class="clearfix"></div>
                                    <a href="" class="float-left" title="Add ">
                                      <i class="fas fa-plus"></i> Add 
                                    </a>
                                    <a href="" class="float-right" title="View ">
                                      <i class="fas fa-eye"></i>View 
                                    </a>
                                    <div class="clearfix"></div>
                                  </div>
                                </div>
                                 <div class="col-md-3">
                                <div class="dashboard_div">
                                  <div class="imgdash">
                                    <img src="<?php echo URL::to('public/assets/images/Academic/Map Subject To Section.svg'); ?>" alt="Student">
                                    </div>
                                    <h4 class="">
                                      <div class="tableCell" style="height: 64px;">
                                        <div class="insidetable">Map Subject To Section</div>
                                      </div>
                                    </h4>
                                    <div class="clearfix"></div>
                                    <a href="" class="float-left" title="Add ">
                                      <i class="fas fa-plus"></i> Add 
                                    </a>
                                    <a href="" class="float-right" title="View ">
                                      <i class="fas fa-eye"></i>View 
                                    </a>
                                    <div class="clearfix"></div>
                                  </div>
                                </div>
                               
                                </div>
                                <div class="row">
                                   <div class="col-md-3">
                                  <div class="dashboard_div">
                                    <div class="imgdash">
                                      <img src="<?php echo URL::to('public/assets/images/Academic/VirtualClass.svg'); ?>" alt="Student">
                                      </div>
                                      <h4 class="">
                                        <div class="tableCell" style="height: 64px;">
                                          <div class="insidetable">Virtual Class</div>
                                        </div>
                                      </h4>
                                      <div class="clearfix"></div>
                                      <a href="<?php echo e(url('/admin-panel/virtual-class/add-virtual-class')); ?>" class="float-left" title="Add ">
                                        <i class="fas fa-plus"></i> Add 
                                      </a>
                                      <a href="<?php echo e(url('/admin-panel/virtual-class/view-virtual-classes')); ?>" class="float-right" title="View ">
                                        <i class="fas fa-eye"></i>View 
                                      </a>
                                      <div class="clearfix"></div>
                                    </div>
                                  </div>
                                  <div class="col-md-3">
                                    <div class="dashboard_div">
                                      <div class="imgdash">
                                        <img src="<?php echo URL::to('public/assets/images/Academic/Competition.svg'); ?>" alt="Student">
                                        </div>
                                        <h4 class="">
                                          <div class="tableCell" style="height: 64px;">
                                            <div class="insidetable">Competition</div>
                                          </div>
                                        </h4>
                                        <div class="clearfix"></div>
                                        <a href="<?php echo e(url('/admin-panel/competition/add-competition')); ?>" class="float-left" title="Add ">
                                          <i class="fas fa-plus"></i> Add 
                                        </a>
                                        <a href="<?php echo e(url('/admin-panel/competition/view-competitions')); ?>" class="float-right" title="View ">
                                          <i class="fas fa-eye"></i>View 
                                        </a>
                                        <div class="clearfix"></div>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="dashboard_div">
                                        <div class="imgdash">
                                          <img src="<?php echo URL::to('public/assets/images/Academic/TimeTable.svg'); ?>" alt="Student">
                                          </div>
                                          <h4 class="">
                                            <div class="tableCell" style="height: 64px;">
                                              <div class="insidetable">Time Table</div>
                                            </div>
                                          </h4>
                                          <div class="clearfix"></div>
                                          <a href="<?php echo e(url('/admin-panel/time-table/add-time-table')); ?>" class="float-left" title="Add ">
                                            <i class="fas fa-plus"></i> Add 
                                          </a>
                                          <a href="<?php echo e(url('/admin-panel/time-table/view-time-table')); ?>" class="float-right" title="View ">
                                            <i class="fas fa-eye"></i>View 
                                          </a>
                                          <div class="clearfix"></div>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="dashboard_div">
                                          <div class="imgdash">
                                            <img src="<?php echo URL::to('public/assets/images/Academic/Certificates.svg'); ?>" alt="Student">
                                            </div>
                                            <h4 class="">
                                              <div class="tableCell" style="height: 64px;">
                                                <div class="insidetable">Certificates</div>
                                              </div>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="" class="float-left" title="Add ">
                                              <i class="fas fa-plus"></i> Add 
                                            </a>
                                            <a href="" class="float-right" title="View ">
                                              <i class="fas fa-eye"></i>View 
                                            </a>
                                            <div class="clearfix"></div>
                                          </div>
                                        </div>
                                       
                                        </div>
                                      
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>