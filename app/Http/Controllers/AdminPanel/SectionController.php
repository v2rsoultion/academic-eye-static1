<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Section\Section;

use Yajra\Datatables\Datatables;

class SectionController extends Controller
{
    /**
     *  View page for section
     *  @Shree on 19 July 2018
    **/
    public function index()
    {
        $section = [];
        $loginInfo              = get_loggedin_user_data();
        $arr_class              = [];
        $arr_medium             = \Config::get('custom.medium_type');
        $section['arr_medium']  = add_blank_option($arr_medium, 'Select Medium');
        $section['arr_class']   = add_blank_option($arr_class, 'Select Class');
        $data = array(
            'page_title'   => trans('language.view_section'),
            'redirect_url' => url('admin-panel/section/view-sections'),
            'login_info'    => $loginInfo,
            'section'       => $section
        );
        return view('admin-panel.section.index')->with($data);
    }

    /**
     *  Add page for section
     *  @Shree on 19 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $section    = [];
        $loginInfo = get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_section_id = get_decrypted_value($id, true);
            $section              = $this->getSectionData($decrypted_section_id,"");
            $section              = isset($section[0]) ? $section[0] : [];
            if (!$section)
            {
                return redirect('admin-panel/section/add-section')->withError('Section not found!');
            }
            $page_title             = trans('language.edit_section');
            $decrypted_section_id   = get_encrypted_value($section['section_id'], true);
            $save_url               = url('admin-panel/section/save/' . $decrypted_section_id);
            $submit_button          = 'Update';
            $arr_class              = get_all_classes($section['medium_type']);
            $section['arr_class']   = add_blank_option($arr_class, 'Select Class');
        }
        else
        {
            $arr_class              = [];
            $section['arr_class']   = add_blank_option($arr_class, 'Select Class');
            $page_title    = trans('language.add_section');
            $save_url      = url('admin-panel/section/save');
            $submit_button = 'Save';
        }
        $arr_medium             = \Config::get('custom.medium_type');
        $section['arr_medium']  = add_blank_option($arr_medium, 'Select Medium');
        $data = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'section'       => $section,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/section/view-sections'),
        );
        return view('admin-panel.section.add')->with($data);
    }

    /**
     *  Destroy section data
     *  @Shree on 19 July 2018
    **/
    public function destroy($id)
    {
        $section_id = get_decrypted_value($id, true);
        $section    = Section::find($section_id);
        if ($section)
        {
            $section->delete();
            $success_msg = "Section deleted successfully!";
            return redirect('admin-panel/section/view-sections')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Section not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Save section data
     *  @Shree on 19 July 2018
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo = get_loggedin_user_data();
        $decrypted_section_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $section = Section::find($decrypted_section_id);
            if (!$section)
            {
                return redirect('admin-panel/section/view-sections/')->withError('Section not found!');
            }
            $success_msg = 'Section updated successfully!';
        }
        else
        {
            $section     = New Section;
            $success_msg = 'Section saved successfully!';
        }
        
        $class_id = null;
        if ($request->has('class_id'))
        {
            $class_id = Input::get('class_id');
        }
        
        $validatior = Validator::make($request->all(), [
                'section_name' => 'required|unique:sections,section_name,' . $decrypted_section_id . ',section_id,class_id,' . $class_id,
                'class_id'     => 'required',
        ]);
        
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            
            DB::beginTransaction();
            try
            {
                $section->admin_id          = $loginInfo['admin_id'];
                $section->update_by         = $loginInfo['admin_id'];
                $section->class_id          = Input::get('class_id');
                $section->medium_type       = Input::get('medium_type');
                $section->section_name      = Input::get('section_name');
                $section->section_intake    = Input::get('section_intake');
                $section->section_order     = Input::get('section_order');
                $section->save();
            }
            catch (\Exception $e)
            {
                
                //failed logic here
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }

            DB::commit();
        }

        return redirect('admin-panel/section/view-sections')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 19 July 2018
    **/
    public function anyData(Request $request)
    {
        $section     = [];
        $arr_section = $this->getSectionData("",$request);
        foreach ($arr_section as $key => $section_data)
        {
            $section[$key] = (object) $section_data;
        }
        return Datatables::of($section)
            
            ->addColumn('action', function ($section)
            {
                $encrypted_section_id = get_encrypted_value($section->section_id, true);
                if($section->section_status == 0) {
                    $status = 1;
                    $statusVal = '<div class="btn btn-danger btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Deactive"> <i class="fas fa-minus-circle"></i> </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><i class="fas fa-plus-circle"></i></div>';
                }
                return '
                            <div class="btn btn-success btn-icon btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Active"><a href="section-status/'.$status.'/' . $encrypted_section_id . '">'.$statusVal.'</a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><a href="add-section/' . $encrypted_section_id . '"><i class="zmdi zmdi-edit"></i></a></div>
                            <div class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><a href="delete-section/' . $encrypted_section_id . '" onclick="return confirm('."'Are you sure?'".')"><i class="zmdi zmdi-delete"></i></a></div>';
               
                
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Change Section's status
     *  @Shree on 18 July 2018
    **/
    public function changeStatus($status,$id)
    {
        $section_id = get_decrypted_value($id, true);
        $section    = Section::find($section_id);
        if ($section)
        {
            $section->section_status  = $status;
            $section->save();
            $success_msg = "Section deleted successfully!";
            return redirect('admin-panel/section/view-sections')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Section not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Get section data
     *  @Shree on 19 July 2018
    **/
    public function getSectionData($section_id = array(),$request)
    {
        $loginInfo = get_loggedin_user_data();
        $section_return   = [];
        $section          = [];
        $arr_medium             = \Config::get('custom.medium_type');
        $arr_section_data = Section::where(function($query) use ($section_id,$loginInfo,$request) 
            {
                if (!empty($section_id))
                {
                    $query->where('section_id', $section_id);
                }
                if (!empty($request) && !empty($request->get('name')))
                {
                    $query->where('section_name', "like", "%{$request->get('name')}%");
                }
                if (!empty($request) && !empty($request->has('class_id')) && $request->get('class_id') != null)
                {
                    $query->where('class_id', "=", $request->get('class_id'));
                }
                if (!empty($request) && !empty($request->has('medium_type')) && $request->get('medium_type') != null)
                {
                    $query->where('medium_type', "=", $request->get('medium_type'));
                }
            })->with('sectionClass')->get();
          //  p($arr_section_data);
        if (!empty($arr_section_data))
        {
            foreach ($arr_section_data as $key => $section_data)
            {
                $section = array(
                    'section_id'        => $section_data['section_id'],
                    'section_name'      => $section_data['section_name'],
                    'medium_type'       => $section_data['medium_type'],
                    'medium_type1'      => $arr_medium[$section_data['medium_type']],
                    'section_intake'    => $section_data['section_intake'],
                    'section_order'     => $section_data['section_order'],
                    'section_status'    => $section_data['section_status'],
                    'class_id'          => $section_data['class_id'],
                );
                if (isset($section_data['sectionClass']['class_name']))
                {
                    $section['class_name'] = $section_data['sectionClass']['class_name'];
                }
                $section_return[] = $section;
            }
        }
        return $section_return;
    }

    /**
     *  Get class section data according class
    **/
    public function getClassSectionData()
    {

        $class_id = Input::get('class_id');
        $section = get_class_section($class_id);
        $data = view('admin-panel.section.ajax-class-section-select',compact('section'))->render();
        return response()->json(['options'=>$data]);
    }

}
