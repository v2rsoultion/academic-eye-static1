<?php if(isset($time_table['class_id']) && !empty($time_table['class_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?>
<?php echo Form::hidden('time_table_id',old('time_table_id',isset($shift['time_table_id']) ? $shift['time_table_id'] : ''),['class' => 'gui-input', 'id' => 'time_table_id', 'readonly' => 'true']); ?>

<!-- <?php echo Form::hidden('class_id',old('class_id',isset($time_table['class_id']) ? $time_table['class_id'] : ''),['class' => 'gui-input', 'id' => 'class_id', 'readonly' => 'true']); ?> -->
<style type="">

/*.theme-blush .btn-primary {
    margin-top: 4px !important;
}*/
/*.form-group .form-control {
    padding: 0px !important;
}*/
.btn-round.btn-simple {
    padding: 5px 22px !important;
}
</style>
<?php if($errors->any()): ?>
<div class="alert alert-danger" role="alert">
    <?php echo e($errors->first()); ?>

     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.time_table_class_name'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::text('name', old('name',isset($time_table['time_table_name']) ? $time_table['time_table_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.time_table_class_name'), 'id' => 'name']); ?>

        </div>
        <?php if($errors->has('name')): ?> <p class="help-block"><?php echo e($errors->first('name')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.medium_type'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('medium_type', $time_table['arr_medium'], isset($time_table['medium_type']) ? $time_table['medium_type'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type', 'onChange'=>'getClass(this.value)']); ?>

                <i class="arrow double"></i>
            </label>
            <?php if($errors->has('medium_type')): ?> <p class="help-block"><?php echo e($errors->first('medium_type')); ?></p> <?php endif; ?>
        </div> 
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.class'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('class_id', $time_table['arr_class'],isset($time_table['class_id']) ? $time_table['class_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id', 'onChange'=>'getClassSection(this.value)']); ?>

                <i class="arrow double"></i>
            </label>
            <?php if($errors->has('class_id')): ?> <p class="help-block"><?php echo e($errors->first('class_id')); ?></p> <?php endif; ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.time_table_section'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('section_id', $time_table['arr_section'],isset($time_table['section_id']) ? $time_table['section_id'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'section_id']); ?>

                <i class="arrow double"></i>
            </label>
            <?php if($errors->has('section_id')): ?> <p class="help-block"><?php echo e($errors->first('section_id')); ?></p> <?php endif; ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.time_table_week_day'); ?> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('week_days[]', $time_table['arr_week_days'],isset($time_table['week_days']) ? $time_table['week'] : '', ['class' => 'form-control show-tick select_form1 select2','id'=>'week_days','multiple' => 'multiple']); ?>

                <i class="arrow double"></i>
            </label>
            <?php if($errors->has('week_days')): ?> <p class="help-block"><?php echo e($errors->first('week_days')); ?></p> <?php endif; ?>
        </div>
    </div>


</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']); ?>

        <a href="<?php echo url('admin-panel/dashboard'); ?>" ><?php echo Form::button('Cancel', ['class' => 'btn btn-raised btn-round']); ?></a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#class-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            // errorLabelContainer: '.errorTxt',

            /* @validation  rules 
             ------------------------------------------ */

            rules: {
                name: {
                    required: true,
                    lettersonly:true
                },
                medium_type: {
                    required: true
                },
                class_id: {
                    required: true 
                },
                section_id: {
                    required: true
                },
                week_days: {
                    required: true
                },
            },

            // message: {
            //     required: 'This field is required';
            // }

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                   // error.insertAfter(element.parent());
                }
            }
        });
        
    });

    
    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/class/get-class-data')); ?>",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='class_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='class_id'").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }

    function getClassSection(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/section/get-class-section-data')); ?>",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("select[name='section_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='section_id'").html('<option value="">Select Section</option>');
            $(".mycustloading").hide();
        }
    }

</script>