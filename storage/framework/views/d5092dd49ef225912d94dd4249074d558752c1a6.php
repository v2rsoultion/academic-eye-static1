<?php $__env->startSection('content'); ?>
<style type="text/css">
    .table-responsive{
        overflow-x: visible !important;
    }
    .info {
    padding: 7px 0px;
    border: 1px solid #6471b8;
    height: 37px;
    color: #fff;
    background: #6471b8;
    }
</style>
<section class="content contact">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <h2>View Candidates</h2>
            </div>
            <div class="col-lg-9 col-md-6 col-sm-12 line">
                <!-- <a href="<?php echo url('admin-panel/recruitment/add-job'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a> -->
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/recruitment'); ?>"><?php echo trans('language.menu_recruitment'); ?></a></li>
                     <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/menu/recruitment'); ?>">Select Candidates</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/recruitment/view-job-list'); ?>">View Job List</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo URL::to('admin-panel/recruitment/view-candidate'); ?>">View Candidates</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="headingcommon  col-lg-12">View Candidates  :-</div> -->
                            <div class="body form-gap">
                           
                           <form>
                               <div class="row">
                                 <!-- <div class="headingcommon  col-lg-12">Search By  :-</div> -->
                                  <div class="col-lg-3 col-md-3 col-sm-12">
                            
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Name"  name="name">
                                    </div>
                                </div>
                                 <div class="col-lg-3 col-md-3 col-sm-12">

                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Email"  name="name">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                 
                                    <div class="form-group">
                                        <input class="form-control positive-numeric-only" id="" min="0" name="capacity" type="number" value="" placeholder="Contact No">
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                    <button type="submit" class="btn btn-raised btn-primary">Search</button>
                                </div>
                                <div class="col-lg-1">
                                    <button  class="btn btn-raised btn-primary ">Clear</button>
                                </div>
                               </div> 
                           </form>
                           <hr>
                            <div class="info">
                               <div class="col-lg-2 float-left"><b>Medium: Hindi</b></div>
                               <div class="col-lg-4 float-left"><b>Job Name: Class Teacher</b></div>
                           </div>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    <?php echo e(csrf_field()); ?>

                                        <thead>
                                            <tr>
                                                <th><?php echo e(trans('language.s_no')); ?></th>
                                                <th>Form No</th>
                                                <th><?php echo e(trans('language.job_name')); ?></th>
                                                <th><?php echo e(trans('language.email')); ?></th>
                                                <th><?php echo e(trans('language.contact')); ?></th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>1001</td>
                                                <td>Rajesh Kumar</td>
                                                <td>rajeshkumar@gmail.com</td>
                                                <td>91+ 654646654</td>
                                                <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#approvedModel">Approved</a></li> 
                                                    <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#disapprovedModal">Disapproved</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>1002</td>
                                                <td>Rajesh Kumar</td>
                                                <td>rajeshkumar@gmail.com</td>
                                                <td>91+ 654646654</td>
                                                <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#approvedModel">Approved</a></li> 
                                                    <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#disapprovedModal">Disapproved</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>1003</td>
                                                <td>Mahesh Kumar</td>
                                                <td>maheshkumar@gmail.com</td>
                                                <td>91+ 445454554</td>
                                                <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#approvedModel">Approved</a></li> 
                                                    <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#disapprovedModal">Disapproved</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>1004</td>
                                                <td>Mahesh Kumar</td>
                                                <td>maheshkumar@gmail.com</td>
                                                <td>91+ 445454554</td>
                                               <td> <div class="dropdown">
                                                <button type="button" class="btn btn-icon  btn-neutral dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="zmdi zmdi-label"></i>
                                                <span class="zmdi zmdi-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right pullDown selectlist11">
                                                    <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#approvedModel">Approved</a></li> 
                                                    <li> <a title="View Schedule" href="#" data-toggle="modal" data-target="#disapprovedModal">Disapproved</a></li> 
                                                </ul> </div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>
<?php $__env->stopSection(); ?>

<!-- model -->
<div class="modal fade" id="approvedModel" tabindex="-1" role="dialog" style="padding: 40px;">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content send_messsage_button1">
            <div class="header header_model_2">
                <div class="header_model_1">
                    <h6 style="padding-left:20px;"><strong>Send Message</strong></h6>
                </div>
                <ul class="header-dropdown" style="padding-right: 20px; ">
                    <li class="remove remove_cross_x_2 float-right" style="position: relative; top: 20px;">
                        <a role="button" class="button_1" data-dismiss="modal"><i class="zmdi zmdi-close zmdi_close1"></i></a>
                    </li>
                </ul>
            
            </div>
          <div class="tab-pane active" id="classlist">
              <div class="card form-gap" style="padding: 20px 40px;">
                  <div class="body">
                        <form class="" action="" method=""  id="">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 text_area_desc">
                                    <lable class="from_one1">Message :</lable>
                                    <div class="form-group">
                                        <textarea rows="4" name="message" value="" class="form-control no-resize" placeholder="">Dear #CandidateName, you are selected for #jobname in #schoolname, Please contact the Name in school before Date for joining the school.
</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <lable class="from_one1">Send Via :</lable>
                                    <div class="form-group">
                                        <div class="radio" style="margin:10px 4px !important;">
                                            <input type="radio" name="radio_option" id="radio1" value="option1">
                                            <label for="radio1" class="document_staff">Email</label>
                                            <input type="radio" name="radio_option" id="radio2" value="option2">
                                            <label for="radio2" class="document_staff">SMS</label>
                                            <input type="radio" name="radio_option" id="radio4" value="option4" checked="">
                                            <label for="radio4" class="document_staff">Both</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-raised  btn-primary waves-effect float-right" data-dismiss="modal">Cancel</button>    
                                    <button type="submit" class="btn btn-raised  btn-primary float-right" id="send_msg" onclick="showMessage()">Send</button>
                                </div>
                            </div>
                        </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<!-- model -->
<div class="modal fade" id="disapprovedModal" tabindex="-1" role="dialog" style="padding: 40px;">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content send_messsage_button1">
            <div class="header header_model_2">
                <div class="header_model_1">
                    <h6 style="padding-left:20px;"><strong>Send Message</strong></h6>
                </div>
                <ul class="header-dropdown" style="padding-right: 20px; ">
                    <li class="remove remove_cross_x_2 float-right" style="position: relative; top: 20px;">
                        <a role="button" class="button_1" data-dismiss="modal"><i class="zmdi zmdi-close zmdi_close1"></i></a>
                    </li>
                </ul>
            
            </div>
          <div class="tab-pane active" id="classlist">
              <div class="card form-gap" style="padding: 20px 40px;">
                  <div class="body">
                        <form class="" action="" method=""  id="">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 text_area_desc">
                                    <lable class="from_one1">Message :</lable>
                                    <div class="form-group">
                                        <textarea rows="4" name="message" value="" class="form-control no-resize" placeholder="">Dear #CandidateName, we are sorry your application for the #jobname is not approved, we will contact you for any other openings. From #schoolName
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <lable class="from_one1">Send Via :</lable>
                                    <div class="form-group">
                                        <div class="radio" style="margin:10px 4px !important;">
                                            <input type="radio" name="radio_option" id="radio1" value="option1">
                                            <label for="radio1" class="document_staff">Email</label>
                                            <input type="radio" name="radio_option" id="radio2" value="option2">
                                            <label for="radio2" class="document_staff">SMS</label>
                                            <input type="radio" name="radio_option" id="radio4" value="option4" checked="">
                                            <label for="radio4" class="document_staff">Both</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">                            
                                <div class="col-sm-12">
                                    <hr />
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-raised  btn-primary waves-effect float-right" data-dismiss="modal">Cancel</button>    
                                    <button type="submit" class="btn btn-raised  btn-primary float-right" id="send_msg1" onclick="showMessage()">Send</button>
                                </div>
                            </div>
                        </form>
                  </div>
              </div>
          </div>


          <!-- <div class="modal-footer">
              <button type="button" class="btn btn-default btn-round waves-effect">SAVE CHANGES</button>
              <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
          </div> -->
      </div>
  </div>
</div>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $(document).ready(function () {
        var table = $('#recruitment-job-table').DataTable({
            //dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            ajax: {
                url: '<?php echo e(url('admin-panel/recruitment/data')); ?>',
                data: function (d) {
                    d.medium_type = $('select[name="medium_type"]').val();
                    d.job_type    = $('select[name="job_type"]').val();
                    d.job_name    = $('input[name="job_name"]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'job_name', name: 'job_name'},
                {data: 'medium_type1', name: 'medium_type1'},
                {data: 'type', name: 'type'},
                {data: 'no_of_vacancy', name: 'no_of_vacancy'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 5, // your case first column
                    "width": "15%"
                },
                {
                    targets: [ 0, 1, 2, 3, 4 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }

</script>

<script type="text/javascript">
    function showMessage() {
        alert('Your message has been sent successfully.');
    }
</script>




<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>