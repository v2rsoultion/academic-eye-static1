@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
 .card{
      background: transparent !important;
  }
  section.content{
    background: #f0f2f5 !important;
  }
  .table-responsive {
    overflow-x: visible !important;
  }
</style>
<!--  Main content here -->
<section class="content">
  <div class="block-header">
    <div class="row">
      <div class="col-lg-5 col-md-6 col-sm-12">
        <h2>Accounts</h2>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 line">
        <ul class="breadcrumb float-md-right">
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
          <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/account') !!}">Accounts</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12">
        <div class="tab-content">
          <div class="tab-pane active" id="classlist">
            <div class="card">
              <div class="body">
                <!--  All Content here for any pages -->
                <div class="row">
                      <div class="col-md-3">
                        <div class="dashboard_div">
                          <div class="imgdash">
                            <img src="{!! URL::to('public/assets/images/Account/Accounts Group.svg') !!}" alt="Student">
                            </div>
                            <h4 class="">
                              <div class="tableCell" style="height: 64px;">
                                <div class="insidetable">Accounts Group</div>
                              </div>
                            </h4>
                            <div class="clearfix"></div>
                            <a href="{{ url('admin-panel/account/manage-accounts-group') }}" class="cusa" title="View " style="width: 48%; margin: 15px auto;">
                              <i class="fas fa-eye"></i>Manage 
                            </a>
                            <div class="clearfix"></div>
                            
                        <div class="row clearfix"></div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="dashboard_div">
                            <div class="imgdash">
                              <img src="{!! URL::to('public/assets/images/Account/Accounts Heads.svg') !!}" alt="Student">
                              </div>
                              <h4 class="">
                                <div class="tableCell" style="height: 64px;">
                                  <div class="insidetable">Accounts Heads</div>
                                </div>
                              </h4>
                              <div class="clearfix"></div>
                              <a href="{{ url('admin-panel/account/manage-accounts-head') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                <i class="fas fa-eye"></i>Manage 
                              </a>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                            <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="{!! URL::to('public/assets/images/Account/Opening Balance.svg') !!}" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Opening Balance</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="{{ url('admin-panel/account/manage-opening-balance') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                  <i class="fas fa-eye"></i>Manage 
                                </a>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                              <div class="col-md-3">
                            <div class="dashboard_div">
                              <div class="imgdash">
                                <img src="{!! URL::to('public/assets/images/Account/Journal Entries.svg') !!}" alt="Student">
                                </div>
                                <h4 class="">
                                  <div class="tableCell" style="height: 64px;">
                                    <div class="insidetable">Journal Entries</div>
                                  </div>
                                </h4>
                                <div class="clearfix"></div>
                                <a href="{{ url('admin-panel/account/manage-journal-entries') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                  <i class="fas fa-eye"></i>Manage 
                                </a>
                                <div class="clearfix"></div>
                                <ul class="header-dropdown opendiv">
                                      <li class="dropdown">
                                        <button class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right slideUp">
                                          <li>
                                            <a href="{{ url('admin-panel/account/view-journal-entries') }}" title="">View Journal Entries</a>
                                          </li>
                                         
                                        </ul>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                              </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Account/Payment & Receipt Voucher.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Payment & Receipt Voucher</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <a href="{{ url('admin-panel/account/payment-receipt-voucher') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>Manage 
                                  </a>
                                  <div class="clearfix"></div>
                                 
                                </div>
                              </div>
                              <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Account/profilt & loss Ac.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Profit & Loss A/c</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <a href="{{ url('admin-panel/account/view-profit-loss-account') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                 
                                </div>
                              </div>
                              <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Account/ledger vouchers.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Ledger Vouchers</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <a href="{{ url('admin-panel/account/ledger-vouchers') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                 
                                </div>
                              </div>
                              <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Account/trial balance.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Trial Balance</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <a href="{{ url('admin-panel/account/view-trial-balance') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>

                            </div>

                            <div class="row">
                              <div class="col-md-3">
                              <div class="dashboard_div">
                                <div class="imgdash">
                                  <img src="{!! URL::to('public/assets/images/Account/balance sheet.svg') !!}" alt="Student">
                                  </div>
                                  <h4 class="">
                                    <div class="tableCell" style="height: 64px;">
                                      <div class="insidetable">Balance Sheet</div>
                                    </div>
                                  </h4>
                                  <div class="clearfix"></div>
                                  <a href="{{ url('admin-panel/account/view-balance-sheet') }}" class="cusa" title="View" style="width: 48%; margin: 15px auto;">
                                    <i class="fas fa-eye"></i>View 
                                  </a>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                            </div>
                            
                                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
@endsection
