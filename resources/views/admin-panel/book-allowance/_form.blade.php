@if(isset($book_allowance['book_allowance_id']) && !empty($book_allowance['book_allowance_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

<style>
    .state-error{
        color: red;
        font-size: 13px;
        margin-bottom: 10px;
    }
</style>

{!! Form::hidden('book_allowance_id',old('book_allowance_id',isset($book_allowance['book_allowance_id']) ? $book_allowance['book_allowance_id'] : ''),['class' => 'gui-input', 'id' => 'book_allowance_id', 'readonly' => 'true']) !!}

<!-- Basic Info section -->
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.edit_book_staff') !!} :</lable>
        <div class="form-group">
            {!! Form::number('allow_for_staff', old('allow_for_staff',isset($book_allowance['allow_for_staff']) ? $book_allowance['allow_for_staff']: ''), ['class' => 'form-control','placeholder'=>trans('language.edit_book_staff'), 'id' => 'allow_for_staff']) !!}
        </div>
        @if ($errors->has('allow_for_staff')) <p class="help-block">{{ $errors->first('allow_for_staff') }}</p> @endif
    </div>

    <div class="col-lg-6 col-md-6">
        <lable class="from_one1">{!! trans('language.edit_book_student') !!} :</lable>
        <div class="form-group">
            {!! Form::number('allow_for_student', old('allow_for_student',isset($book_allowance['allow_for_student']) ? $book_allowance['allow_for_student']: ''), ['class' => 'form-control','placeholder'=>trans('language.edit_book_student'), 'id' => 'allow_for_student']) !!}
        </div>
        @if ($errors->has('allow_for_student')) <p class="help-block">{{ $errors->first('allow_for_student') }}</p> @endif
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']) !!}
        <a href="{!! url('admin-panel/dashboard') !!}" >{!! Form::button('Cancel', ['class' => 'btn btn-raised btn-round']) !!}</a>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#title-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                allow_for_staff: {
                    required: true,
                    min:0,
                },
                allow_for_student: {
                    required: true,
                    min:0,
                }
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

    });

    

</script>