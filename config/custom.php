<?php

    return [

        'school_medium_type'           => array(
            ''  => 'Select Medium',
            '0' => 'Hindi',
            '1' => 'English',
            '2' => 'Both',
        ),
        'medium_type'           => array(
            ''  => 'Select Medium',
            '0' => 'Hindi',
            '1' => 'English',
        ),
        'student_type'           => array(
            ''  => 'Student Type',
            '0' => 'Paid',
            '1' => 'Free',
            '2' => 'RTE',
        ),
        'month'                    => array(
            '1'  => 'January',
            '2'  => 'February',
            '3'  => 'March',
            '4'  => 'April',
            '5'  => 'May',
            '6'  => 'June',
            '7'  => 'July',
            '8'  => 'August',
            '9'  => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ),
        'school_logo'           => array(
            'upload_path' => '/uploads/school-logo/',
            'display_path' => 'public/uploads/school-logo/',
        ),
        'school_img'           => array(
            'upload_path' => '/uploads/school-images/',
            'display_path' => 'public/uploads/school-images/',
        ),
        'student_gender'           => array(
            '0' => 'Boy',
            '1' => 'Girl',
        ),
        'annual_salary'   => array(
            ''  => 'Select Annual Salary',
            'Below 1 Lac' => 'Below 1 Lac',
            'Below 2.5 Lac' => 'Below 2.5 Lac',
            'Below 5 Lac' => 'Below 5 Lac',
            'Below 7.5 Lac' => 'Below 7.5 Lac',
            'Below 10 Lac' => 'Below 10 Lac',
            'Above 10 Lac' => 'Above 10 Lac'
        ),
        'student_category'   => array(
            ''  => 'Select Category',
            'General' => 'General',
            'OBC' => 'OBC',
            'SC' => 'SC',
            'ST' => 'ST'
        ),
        'student_image'           => array(
            'upload_path' => '/uploads/student-profile/',
            'display_path' => 'public/uploads/student-profile/',
        ),
        'student_status'           => array(
            '0' => 'Leave',
            '1' => 'Studying',
        ),
        'staff_profile'           => array(
            'upload_path' => '/uploads/staff-profile/',
            'display_path' => 'public/uploads/staff-profile/',
        ),
        'staff_gender'           => array(
            ''  => 'Select gender',
            '0' => 'Male',
            '1' => 'Female',
        ),
        'staff_marital'           => array(
            ''  => 'Select gender',
            '0' => 'Unmarried',
            '1' => 'Married',
        ),
        'student_document_file'           => array(
            'upload_path' => '/uploads/student-document/',
            'display_path' => 'public/uploads/student-document/',
        ),
        'student_leave_document_file'     => array(
            'upload_path' => '/uploads/student-leave-document/',
            'display_path' => 'public/uploads/student-leave-document/',
        ),
        //@Ashish 21 Aug 2018
        'question_paper_document'           => array(
            'upload_path' => '/uploads/question-paper-document/',
            'display_path' => 'public/uploads/question-paper-document/',
        ),
    
        'week_days'      => array(
            // ''   => 'Nothing selected',
            '1'  => 'Monday',
            '2'  => 'Tuesday',
            '3'  => 'Wednesday',
            '4'  => 'Thursday',
            '5'  => 'Friday',
            '6'  => 'Saturday',
        ),     

        'brochure_file'           => array(
            'upload_path' => '/uploads/brochure-file/',
            'display_path' => 'public/uploads/brochure-file/',
        ),
        'form_type'           => array(
            '0' => 'Admission',
            '1' => 'Enquiry',
        ),
        'email_receipt'           => array(
            '0' => 'No',
            '1' => 'Yes',
        ),
        'message_via'           => array(
            '0' => 'Email',
            '1' => 'Phone',
            '2' => 'Both'
        ),

        'payment_mode'           => array(
            'Cash' => 'Cash',
            'Cheque' => 'Cheque',
            'Online' => 'Online',
            'Swipe Machine' => 'Swipe Machine'
        ),

        'student_fields'           => array(
            'medium_type' =>  'Medium Type',
            'admission_session_id' => 'Admission Session',
            'admission_class_id' => 'Admission Class',
            'admission_section_id' => 'Admission Section',
            'current_session_id' => 'Current Session',
            'current_class_id' => 'Current Class',
            'current_section_id' => 'Current Section',
            'stream_id' => 'Stream',
            'student_enroll_number' => 'Enroll No',
            'student_name' => 'Name ',
            'student_type' => 'Student Type',
            'student_image' => 'Photo of student',
            'student_reg_date' => 'Date of Reg',
            'student_dob' => 'DOB ',
            'student_email' => 'Email Address',
            'student_gender' => 'Gender',
            'student_privious_school' => 'School Name',
            'student_privious_class' => 'Class',
            'student_privious_tc_no' => 'TC No.',
            'student_privious_tc_date' => 'TC Date',
            'student_privious_result' => 'Result',
            'student_category' => 'Category',
            'caste_id' => 'Caste',
            'religion_id' => 'Religion',
            'nationality_id' => 'Nationality ',
            'student_sibling_name' => 'Sibling Name',
            'student_sibling_class_id' => 'Sibling Class',
            'student_adhar_card_number' => 'Adhaar Card Number',
            'group_id' => 'House/Group ',
            'student_login_name' => 'Name',
            'student_login_email' => 'Email Address',
            'student_login_contact_no' => 'Contact No',
            'student_father_name' => 'Father Name',
            'student_father_mobile_number' => 'Father’s Mobile number',
            'student_father_email' => 'Father’s Email Address',
            'student_father_occupation' => 'Father’s Occupation',
            'student_father_annual_salary' => 'Father’s Annual Salary',
            'student_mother_name' => 'Mother Name',
            'student_mother_mobile_number' => 'Mother’s Mobile number',
            'student_mother_email' => 'Mother’s Email Address',
            'student_mother_occupation' => 'Mother’s Occupation',
            'student_mother_annual_salary' => 'Mother’s Annual Salary',
            'student_mother_tongue' => 'Mother Tongue',
            'student_guardian_name' => 'Guardian Name',
            'student_guardian_email' => 'Email Address',
            'student_guardian_mobile_number' => 'Mobile Number',
            'student_guardian_relation' => 'Relation with guardian',
            'student_temporary_address' => 'Address',
            'student_temporary_city' => 'Country',
            'student_temporary_state' => 'State',
            'student_temporary_county' => 'City',
            'student_temporary_pincode' => 'Pincode',
            'student_permanent_address' => 'Address',
            'student_permanent_city' => 'Country',
            'student_permanent_state' => 'State',
            'student_permanent_county' => 'City',
            'student_permanent_pincode' => 'Pincode',
            'student_height' => 'Height',
            'student_weight' => 'Weight',
            'student_blood_group' => 'Blood Group',
            'student_vision_left' => 'Vision Right',
            'student_vision_right' => 'Vision Left',
            'medical_issues' => 'Medical Issue',
            'document_category_id' => 'Document Category',
            'student_document_details' => 'Document Details',
            'student_document_file' => 'Document File'
        ),
    ];


    
