<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VehicleDocumentController extends Controller
{
     /**
     *  View page for Vehicle Document
     *  @Khushbu on 28 Sept 2018   
    **/
    public function index()
    {
        $loginInfo 		    = get_loggedin_user_data();
        
        $data 				= array(
            'page_title'    => trans('language.view_vehicle_document'),
            'redirect_url'  => url('admin-panel/transport/vehicle-documents/view-vehicle-document'),
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.vehicle-documents.view-vehicle-documents')->with($data);
    }

    /**
     *  Add page for Vehicle Document
     *  @Khushbu on 28 Sept 2018   
    **/
    public function add(Request $request, $id = NULL)
    {
        $data       = [];
        $loginInfo  = get_loggedin_user_data();
        // $job        = [];
        if (!empty($id))
        {
            $page_title             = trans('language.edit_vehicle_document');
            $save_url               = url('admin-panel/transport/vehicle-documents/save/' . $decrypted_job_id);
            $submit_button          = 'Update';
        }
        else
        {
            $page_title    = trans('language.add_vehicle_document');
            $save_url      = url('admin-panel/transport/vehicle-documents/save');
            $submit_button = 'Save';
        }

        $data               = array(
            'page_title'    => $page_title,
            'save_url'      => $save_url,
            'submit_button' => $submit_button,
            'login_info'    => $loginInfo,
            'redirect_url'  => url('admin-panel/transport/vehicle-documents/view-vehicle-document'),
        );
        return view('admin-panel.vehicle-documents.add-vehicle-document')->with($data);
    }

}
