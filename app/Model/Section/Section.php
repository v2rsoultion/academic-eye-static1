<?php

namespace App\Model\Section;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table      = 'sections';
    protected $primaryKey = 'section_id';

    public function sectionClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'class_id');
    }

    public function getTimeTable()
    {
        return $this->hasMany('App\Model\TimeTable\TimeTable', 'section_id');
    }
}
