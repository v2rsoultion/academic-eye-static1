@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .table-responsive {
        overflow-x: visible;
    }
      .modal-dialog {
  max-width: 550px !important;
}
</style>
<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Exam Duty</h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 line">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/staff-menu/my-schedule') !!}">My Schedule</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/staff-menu/my-schedule/exam-duty') !!}">Exam Duty</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                           <div class="body form-gap ">
                               <form>
                                    <div class="row clearfix">
                                       <div class="col-lg-2">
                                        <div class="form-group">
                                          <!-- <lable class="from_one1" for="name">Exam</lable> -->
                                          <select class="form-control show-tick select_form1" name="classes" id="">
                                            <option value="">Exam</option>
                                            <option value="A">Exam-1</option>
                                            <option value="B">Exam-2</option>
                                            <option value="C">Exam-3</option>
                                            <option value="D">Exam-4</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-lg-2">
                                        <div class="form-group">
                                          <!-- <lable class="from_one1" for="name">Class</lable> -->
                                          <select class="form-control show-tick select_form1" name="classes" id="">
                                            <option value="">Class</option>
                                            <option value="A">Class-1</option>
                                            <option value="B">Class-2</option>
                                            <option value="C">Class-3</option>
                                            <option value="D">Class-4</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-lg-2">
                                        <div class="form-group">
                                          <!-- <lable class="from_one1" for="name">Section</lable> -->
                                          <select class="form-control show-tick select_form1" name="classes" id="">
                                            <option value="">Section</option>
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                            <option value="D">D</option>
                                          </select>
                                        </div>
                                      </div>
                                        <div class="col-lg-2">
                                        <div class="form-group">
                                          <input type="text" name="date" placeholder="Date" class="form-control">
                                        </div>
                                      </div>
                                       <div class="col-lg-2">
                                        <div class="form-group">
                                          <input type="text" name="room_no" placeholder="Room No" class="form-control">
                                        </div>
                                      </div>
                                    
                                  
                                        <div class="col-lg-1 col-md-1">
                                          <button type="submit" class="btn btn-raised btn-primary" title="Search">Search
                                          </button>
                                        </div>
                                        <div class="col-lg-1 col-md-1">
                                            <button type="submit" class="btn btn-raised btn-primary" title="Clear">Clear
                                          </button>
                                        </div>
                                    </div>
                                </form>
                               
                                <div class="table-responsive">
                                   
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                            {{ csrf_field() }}
                                <thead>
                                    <tr>
                                        <th>{{trans('language.s_no')}}</th>
                                        <th>Exam Name</th>
                                        <th>Class</th>
                                        <th>Subject</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Room No</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                          <td>1</td>
                                          <td>Exam-1</td>
                                          <td>Class-1 - A</td>
                                          <td>Sub-1 - Sub-101</td>
                                          <td>31-10-2018</td>  
                                          <td>08:00 AM - 10:00 AM</td> 
                                          <td>Room-12</td>    
                                        </tr>
                                         <tr>
                                          <td>2</td>
                                          <td>Exam-2</td>
                                          <td>Class-2 - A</td>
                                          <td>Sub-2 - Sub-101</td>
                                          <td>01-11-2018</td>  
                                          <td>11:00 AM - 01:00 PM</td> 
                                          <td>Room-14</td>    
                                        </tr>
                                         
                                </tbody>
                            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>
<div class="modal fade" id="RemarksModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Remark </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
           <div style="width:502px;border: 1px solid #ccc;border-radius: 5px;padding: 5px 10px;margin-left: 0px;">
            <!-- <div class="row">
             <div class="col-lg-5"><b>Class:</b> Class-1 - A</div>
             <div class="col-lg-7"><b>Subject:</b> Sub-1 - Sub-101</div></div>
             <div class="row">
             <div class="col-lg-5"><b>Enroll No:</b> 1001A1</div> 
             <div class="col-lg-7"><b>Student Name:</b> Ankit Dave</div></div> -->
             <div><b>Class:</b> Class-1 - A</div>
             <div><b>Subject:</b> Sub-1 - Sub-101</div>
             <div><b>Student Name:</b> Ankit Dave</div>
             </div>
              <div class="col-lg-12 padding-0">
                  <div class="form-group">
                    <lable class="from_one1" for="name"><b>Remark</b></lable>
                    <textarea class="form-control" name="remark" placeholder="Remark"></textarea>
                    
                  </div>
                </div>
                <hr>
                <div class="row">
                 <div class="col-lg-2">
                      <button type="submit" class="btn btn-raised btn-primary " title="Search">Save
                      </button>
                    </div>
                    <div class="col-lg-1">
                      <button type="reset" class="btn btn-raised btn-primary " title="Clear">Cancel
                      </button>
                    </div>
                    </div>
      </div>
    </div>
  </div>
</div>
@endsection
