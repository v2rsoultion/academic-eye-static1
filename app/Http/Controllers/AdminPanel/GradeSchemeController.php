<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GradeSchemeController extends Controller
{
    public function addGradeScheme() {
    	$loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.grade-scheme.add_grade_scheme')->with($data);
    }

     public function viewGradeScheme() {
    	$loginInfo = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo
        );
        return view('admin-panel.grade-scheme.view_grade_scheme')->with($data);
    }
}
