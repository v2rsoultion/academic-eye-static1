<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarksCriteriaController extends Controller
{
    public function add() {
    	 $loginInfo              = get_loggedin_user_data();
        $data = array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.marks-criteria.add_markcriteria')->with($data);
    }
}
