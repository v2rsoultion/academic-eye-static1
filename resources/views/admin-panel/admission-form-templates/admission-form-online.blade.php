<!DOCTYPE html>
<html>
<head>
	<title>Admission Form</title>
	<link rel="stylesheet" href="../../public/assets/plugins/bootstrap/css/bootstrap.min.css">
	<link href="{{url("/public/assets/plugins/bootstrap-select/css/bootstrap-select.css")}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
	<link href="{{url("/public/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css")}}" rel="stylesheet" />
	{!! Html::script('public/assets/js/jquery.min.js') !!}
<style type="text/css">
	body {
		margin:0px 2px;
		padding: 0px;
		font-size: 13.7px;
		background-color: #b9bfcd;
	}
	.less {
		font-weight: normal;
	}
	.align {
		text-align: center;
	}
	.header {
		width: 1170px;
		margin: auto;
		margin-top: 20px;
		margin-bottom: 20px;
		padding: 20px;
		border: 1px solid #fff;
		border-radius: 5px;
		background-color: #fff;
	}
	tr {
		height: 30px !important;
	}
	.bold {
		font-weight: bold;
	}
	.box-size {
		float:left;
		border:1px solid #000;
		width: 15px;
		height: 15px;
		margin-top: 5px;
		vertical-align: middle;
	}
	.box-content {
		float:left;
		margin: 2px 10px 0px 3px;
	}
	.head-gap {
		float:left;
		margin-right: 10px;
	}
	.heading-style {
		text-decoration: underline;
		padding: 20px 0px 20px 0px;
	}
	.padding_r: {
		padding-right: 0px !important;
	}
	.btn-primary {
	color: #fff;
    background-color: #1f2f60;
    border-color: #1f2f60;
	}
	textarea {
		resize: none;
	}
</style>
</head>
<body>
	<div class="header">
		<table style="width: 100%;">
			<tr>
				<td style="width: 1%;">
					<img src="http://v2rsolution.co.in/academic-eye-static/public/assets/images/logo_new1.png" alt="" width="120px">
				</td>
				<td style="width: 25%;" class="align">
					<h2 style="margin:0px;">Academic Eye Senior Secondary School</h2>	
					<p class="less" style="font-size: 12px;margin-top: 0px;margin-bottom: 2px;">
					<u style="font-size: 13px;">First Floor, Laxmi Tower, Ratanada, Jodhpur</u><br> Phone: 0123-345673, Fax: 0123-345691 &nbsp;&nbsp; <br>E-mail : academiceye.info@gmail.com</p>	
				</td>
				<td style="border:1px solid #000;width:4%;height: 130px;">
				</td>
			</tr>
		</table>
		<hr>
		<div class="row">
        <div class="col-lg-12">
        	<div class="panel panel-default" style=" background-color:#ffffff">
        		<div class="panel-body">
        			<form method="" name="" id="">
					<div class="row col-lg-12 p-0 m-0">
						<div class="row col-lg-6 p-0">
							<div class="col-lg-2" style="margin-top: 7px;">
								<lable class="from_one1">Form No:</lable> 
							</div>
							<div class="col-lg-3 p-0">
				    			<input type="text" name="name1" id="" class="form-control" value="1991101" readonly="true" style="padding-top:10px;">
				    		</div>	
				    	</div>
				    	<div class="clearfix"> </div>
				    	<div class="row col-lg-6 p-0 m-0">
				    		<div class="col-lg-10 text-right" style="padding: 0px 15px 0px 0px;margin-top: 7px;">
								<lable class="from_one1">Date:</lable> 
							</div>
							<div class="col-lg-2 p-0 m-0">
				    			<input type="text" name="name" id="" class="form-control" value="28-12-2018" readonly="true" style="width: 125px;padding-top: 10px;">
				    		</div>	
				    	</div>	
					</div>
					<div class="row col-lg-12">
						<h5 class="heading-style">Student Information: </h5>
					</div>
					<div class="row col-lg-12 p-0 m-0">
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">Student Name:</lable> 
								<input type="text" name="studentName" id="" class="form-control" value="" placeholder="Student Name">
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Student Type:</lable> 
								<select class="form-control show-tick select_form1">
									<option value="">Student type</option>
									<option value="1">Paid</option>
									<option value="2">RTE</option>
									<option value="3">Free</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Gender:</lable> 
								<select class="form-control show-tick select_form1">
									<option value="">Select Gender</option>
									<option value="1">Male</option>
									<option value="2">Female</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Date of Birth: </lable> 
								<input type="Date" name="dob" id="" class="form-control" value="" placeholder="Date of Birth">
							</div>
						</div>
					</div>
					<div class="row col-lg-12 p-0 m-0">
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">Email: </lable> 
								<input type="text" name="email" id="" class="form-control" value="" placeholder="Email">
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Category: </lable> 
								<select class="form-control show-tick select_form1">
									<option value="">Select Category</option>
									<option value="1">GEN</option>
									<option value="2">OBC</option>
									<option value="3">SC/ST</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Blood Group: </lable> 
								<input type="text" name="bloodGroup" id="" class="form-control" value="" placeholder="Blood Group">
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Caste: </lable> 
								<select class="form-control show-tick select_form1" name="caste">
									<option value="">Select Caste</option>
									<option value="1">Jain</option>
									<option value="2">Rajput</option>
									<option value="3">Sikh</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 p-0 m-0">
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">Religion: </lable> 
								<select class="form-control show-tick select_form1" name="religion">
									<option value="">Select Religion</option>
									<option value="1">Hindu</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Nationality: </lable> 
								<select class="form-control show-tick select_form1" name="nationality">
									<option value="">Select Nationality</option>
									<option value="1">Indian</option>
									<option value="2">Nepali</option>
									<option value="3">British</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row col-lg-12">
						<h5 class="heading-style">Parent Information: </h5>
					</div>
					<div class="row col-lg-12 p-0 m-0">
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">Father's Name:</lable> 
								<input type="text" name="fatherName" id="" class="form-control" value="" placeholder="Father Name">
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Father's Occupation: </lable> 
								<input type="text" name="occupation" id="" class="form-control" value="" placeholder="Occupation">
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Father's Annual Salary: </lable> 
								<select class="form-control show-tick select_form1" name="annual_salary">
									<option value="">Select Annual Salary</option>
									<option value="1">None</option>
									<option value="2">Below 1,00,000</option>
									<option value="3">Below 2,00,000</option>
									<option value="4">2,50,000</option>
									<option value="5">Above 3,00,000</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Father's Contact No: </lable> 
								<input type="text" name="contact_no" id="" class="form-control" value="" placeholder="Contact No">
							</div>
						</div>
					</div>
					<div class="row col-lg-12 p-0 m-0">
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">Father's Email: </lable> 
								<input type="text" name="email" id="" class="form-control" value="" placeholder="Email">
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Mother's Name:</lable> 
								<input type="text" name="motherName" id="" class="form-control" value="" placeholder="Mother Name">
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Mother's Occupation: </lable> 
								<input type="text" name="occupation" id="" class="form-control" value="" placeholder="Occupation">
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Mother's Annual Salary: </lable> 
								<select class="form-control show-tick select_form1" name="annual_salary">
									<option value="">Select Annual Salary</option>
									<option value="1">None</option>
									<option value="2">Below 1,00,000</option>
									<option value="3">Below 2,00,000</option>
									<option value="4">2,50,000</option>
									<option value="5">Above 3,00,000</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 p-0 m-0">
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">Mother's Contact No: </lable> 
								<input type="text" name="contact_no" id="" class="form-control" value="" placeholder="Contact No">
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Mother's Email: </lable> 
								<input type="text" name="email" id="" class="form-control" value="" placeholder="Email">
							</div>
						</div>
					</div>
					<div class="row col-lg-12">
						<h5 class="heading-style">Guardian Information: </h5>
					</div>
					<div class="row col-lg-12 p-0 m-0">
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">Guardian's Name:</lable> 
								<input type="text" name="guardianName" id="" class="form-control" value="" placeholder="Guardian Name">
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Email: </lable> 
								<input type="text" name="email" id="" class="form-control" value="" placeholder="Email">
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Contact No: </lable> 
								<input type="text" name="contact_no" id="" class="form-control" value="" placeholder="Contact No">
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Relation: </lable> 
								<input type="text" name="relation" id="" class="form-control" value="" placeholder="Relation">
							</div>
						</div>
					</div>
					<div class="row col-lg-12">
						<h5 class="heading-style">Previous School Information: </h5>
					</div>
					<div class="row col-lg-12 p-0 m-0">
						<div class="col-lg-5 p-0">
							<div class="form-group">
								<lable class="from_one1">Previous School Name:</lable> 
								<input type="text" name="previousSchoolName" id="" class="form-control" value="" placeholder="Previous School Name">
							</div>
						</div>
						<div class="col-lg-2" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Class: </lable> 
								<input type="text" name="class" id="" class="form-control" value="" placeholder="Class">
							</div>
						</div>
						<div class="col-lg-2" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">TC Date: </lable> 
								<input type="Date" name="tc_date" id="" class="form-control" value="" placeholder="TC Date">
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">TC No.: </lable> 
								<input type="text" name="tc_no" id="" class="form-control" value="" placeholder="TC No">
							</div>
						</div>
					</div>
					<div class="row col-lg-12">
						<h5 class="heading-style">Present/Communication Address: </h5>
					</div>
					<div class="row col-lg-12 p-0 m-0">
						<div class="col-lg-12 p-0">
							<div class="form-group">
							<lable class="from_one1">Local Address:</lable> 
							<textarea name="local_address" id="" class="form-control" placeholder="Local Address" rows="2"></textarea>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 p-0 m-0">
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">Country: </lable> 
								<select class="form-control show-tick select_form1" name="annual_salary">
									<option value="">Select Country</option>
									<option value="1">India</option>
									<option value="2">Nepal</option>
									<option value="3">China</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">State: </lable> 
								<select class="form-control show-tick select_form1" name="annual_salary">
									<option value="">Select State</option>
									<option value="1">Rajasthan</option>
									<option value="2">Aasam</option>
									<option value="3">Bihar</option>
									<option value="4">Chandigarh</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">City: </lable> 
								<select class="form-control show-tick select_form1" name="annual_salary">
									<option value="">Select City</option>
									<option value="1">Jodhpur</option>
									<option value="2">Pali</option>
									<option value="3">Jaipur</option>
									<option value="4">Ajmer</option>
									<option value="5">Alwar</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Pincode: </lable> 
								<input type="text" name="pincode" id="" class="form-control" value="" placeholder="Pincode">
							</div>
						</div>
					</div>
					<div class="row col-lg-12">
						<h5 class="heading-style">Permanent Address: </h5>
						<p style="margin-top:21px;"> (Same as Above)
							<div class="checkbox" id="" style="margin-top: 24px;margin-left: 10px;">
							    <input id="checkbox5" type="checkbox" name="sameAddress">
	                    		<label for="checkbox5"> </label>
	                    	</div>
	                    </p> 
					</div>
				<div id="permanent_address_details">
					<div class="row col-lg-12 p-0 m-0">
						<div class="col-lg-12 p-0">
							<div class="form-group">
							<lable class="from_one1">Permanent Address:</lable> 
							<textarea name="permanent_address" id="" class="form-control" placeholder="Permanent Address" rows="2"></textarea>
							</div>
						</div>
					</div>
					<div class="row col-lg-12 p-0 m-0">
						<div class="col-lg-3 p-0">
							<div class="form-group">
								<lable class="from_one1">Country: </lable> 
								<select class="form-control show-tick select_form1" name="annual_salary">
									<option value="">Select Country</option>
									<option value="1">India</option>
									<option value="2">Nepal</option>
									<option value="3">China</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">State: </lable> 
								<select class="form-control show-tick select_form1" name="annual_salary">
									<option value="">Select State</option>
									<option value="1">Rajasthan</option>
									<option value="2">Aasam</option>
									<option value="3">Bihar</option>
									<option value="4">Chandigarh</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">City: </lable> 
								<select class="form-control show-tick select_form1" name="annual_salary">
									<option value="">Select City</option>
									<option value="1">Jodhpur</option>
									<option value="2">Pali</option>
									<option value="3">Jaipur</option>
									<option value="4">Ajmer</option>
									<option value="5">Alwar</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3" style="padding-right: 0px;">
							<div class="form-group">
								<lable class="from_one1">Pincode: </lable> 
								<input type="text" name="pincode" id="" class="form-control" value="" placeholder="Pincode">
							</div>
						</div>
					</div>
				</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="checkbox" id="">
	                    		<input id="checkbox4" type="checkbox" name="declare">
	                    		<label for="checkbox4">I have entered all the Details are Correct</label>
	                		</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-12 text-center">
							<input type="submit" class="btn btn-raised btn-primary" title="Submit" value="Submit">
							<input type="reset" class="btn btn-raised btn-primary" title="reset" value="Reset">
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
		</div>
	</div>
		
{!! Html::script('public/assets/plugins/momentjs/moment.js') !!}
{!! Html::script('public/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') !!}
{!! Html::script('public/assets/js/daterangepicker.min.js') !!}

</body>
</html>

 <script type="text/javascript">
	 $('#dob').bootstrapMaterialDatePicker({
      weekStart: 0, format: 'DD/MM/YYYY',time : false
      });
	 $('#tc_date').bootstrapMaterialDatePicker({
      weekStart: 0, format: 'DD/MM/YYYY',time : false
      });
	 $('input[name="sameAddress"]').click(function () {
	 	if ($(this).is(':checked')) 
	 	{
	 		$('#permanent_address_details').hide();
	 	}
	 	else
	 	{
	 		$('#permanent_address_details').show();
	 	}
	 });
</script>