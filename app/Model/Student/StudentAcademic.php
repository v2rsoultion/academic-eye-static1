<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class StudentAcademic extends Model
{
    protected $table      = 'student_academic_info';
    protected $primaryKey = 'student_academic_info_id';

    public function getStudent()
    {
        return $this->belongsTo('App\Model\Student\Student','student_id');
    }

    public function getCurrentSession()
    {
        return $this->belongsTo('App\Model\Session\Session', 'current_session_id');
    }
    public function getAdmitSession()
    {
        return $this->belongsTo('App\Model\Session\Session', 'admission_session_id');
    }

    public function getAdmitClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'admission_class_id');
    }
    public function getCurrentClass()
    {
        return $this->belongsTo('App\Model\Classes\Classes', 'current_class_id');
    }

    public function getAdmitSection()
    {
        return $this->belongsTo('App\Model\Section\Section', 'admission_section_id');
    }
    public function getCurrentSection()
    {
        return $this->belongsTo('App\Model\Section\Section', 'current_section_id');
    }
    public function getStudentGroup()
    {
        return $this->belongsTo('App\Model\SchoolGroup\SchoolGroup', 'group_id');
    }
    public function getStudentStream()
    {
        return $this->belongsTo('App\Model\Stream\Stream', 'stream_id');
    }
}
