﻿@extends('admin-panel.layout.header')
@section('content')
<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2>Manage Vehicle</h2>
            </div>
             <div class="col-lg-7 col-md-6 col-sm-12 line">
                 <a href="{!! url('admin-panel/transport/vehicle/add-vehicle') !!}" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>
                <ul class="breadcrumb float-md-right ">
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/transport') !!}">Transport<!-- {!! trans('language.staff') !!} --></a></li>
                    <li class="breadcrumb-item"><a href="{!! URL::to('admin-panel/menu/transport') !!}">Vehicle</a></li>
                    <li class="breadcrumb-item active"><a href="{!! URL::to('admin-panel/transport/vehicle/manage-vehicle') !!}">Manage Vehicle</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="tab-content">                   
                    <div class="tab-pane active" id="classlist">
                        <div class="card">
                            <div class="body form-gap">
                                <form>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('vehicle_name', old('vehicle_name', ''), ['class' => 'form-control ','placeholder'=>'Vehicle Name', 'id' => 'vehicle_name']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('vehicle_reg_number', old('vehicle_reg_number', ''), ['class' => 'form-control ','placeholder'=>'Vehicle Registration Number', 'id' => 'vehicle_reg_number']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="input-group ">
                                                {!! Form::text('contact_number', old('contact_number', ''), ['class' => 'form-control ','placeholder'=>'Contact Number', 'id' => 'contact_number']) !!}
                                                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 ">
                                          <button type="submit" class="btn btn-raised btn-primary"  title="Search">Search
                                          </button>
                                        </div>
                                         <div class="col-lg-1 ">
                                          <button type="reset" class="btn btn-raised btn-primary" title="Clear">Clear
                                          </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="table-responsive">
                                    <table class="table m-b-0 c_list" id="#" style="width:100%">
                                    {{ csrf_field() }}
                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Vehicle Name</th>
                                                <th class="v1">Vehicle Registration Number</th>
                                                <th>Capacity</th>
                                                <th>Type</th>
                                                <th>Contact person</th>
                                                <th>Contact number</th>
                                                <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Mahindra NuvoSport</td>
                                                <td class="text-center">AP-02-BK-1084</td>
                                                <td> 1000cc</td>
                                                <td>Contract</td>
                                                <td>Dev Shah</td>
                                                <td>91+ 654986465</td>
                                                <td>    
                                                  <div style="margin-top: 10px !important" class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>  
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Mahindra NuvoSport</td>
                                                <td class="text-center">AP-02-BK-1084</td>
                                                <td> 1000cc</td>
                                                <td>Contract</td>
                                                <td>Dev Shah</td>
                                                <td>91+ 654986465</td>
                                                <td>
                                                 <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td>3</td>
                                                <td>Mahindra NuvoSport</td>
                                                <td class="text-center">AP-02-BK-1084</td>
                                                <td> 1000cc</td>
                                                <td>Contract</td>
                                                <td>Dev Shah</td>
                                                <td>91+ 654986465</td>
                                                <td>
                                                <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td>4</td>
                                                <td>Mahindra NuvoSport</td>
                                                <td class="text-center">AP-02-BK-1084</td>
                                                <td> 1000cc</td>
                                                <td>Contract</td>
                                                <td>Dev Shah</td>
                                                <td>91+ 654986465</td>
                                                <td>
                                                 <div style="margin-top: 10px !important"class="btn btn-success btn-icon  btn-neutral hidden-sm-down demo-google-material-icon" data-toggle="tooltip" title="Approved"> <i class="fas fa-plus-circle"></i> </div>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Edit"><i class="zmdi zmdi-edit"></i></button>
                                                  <button class="btn btn-icon btn-neutral btn-icon-mini" data-toggle="tooltip" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                </td>
                                            </tr>                                             
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

<style type="text/css">

#DataTables_Table_0_filter{
    float: right !important;
}
#DataTables_Table_0_paginate{
    float: right !important;
}
.table tr .v1{
       white-space: normal;
}

</style>

<!-- status fuction -->

<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {

    document.getElementById("myDropdown").classList.toggle("show");
}
function myFunction2() {

    document.getElementById("myDropdown2").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<script type="text/javascript">
      $(document).ready(function() {
       
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            block_name: {
                validators: {
                        stringLength: {
                        min: 4,
                    },
                        notEmpty: {
                        message: 'Please fill required block name'
                    }
                }
            },
            hostel_name: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required hostel name'
                    }
                }
            },
            floor_no: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required floor no'
                    }
                }
            },
            room_no: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required room no'
                    }
                }
            },
            room_alias: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required room alias'
                    }
                }
            },
            capacity: {
                validators: {
                        notEmpty: {
                        message: 'Please fill required capacity'
                    }
                }
            },
            
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});

</script>
