<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MyScheduleController extends Controller
{
    public function dailySchedule() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.my-schedule.daily-schedule')->with($data);
    }

     public function examDuty() {
         $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        
        return view('admin-panel.my-schedule.exam-duty')->with($data);
    }
}
