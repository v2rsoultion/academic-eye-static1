<?php

namespace App\Model\Exam;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $table      = 'exams';
    protected $primaryKey = 'exam_id';

    public function getTerm()
    {
        return $this->belongsTo('App\Model\Term\Term', 'term_exam_id');
    }
}
