<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class InventoryController extends Controller
{
    public function manageInvoiceConfiguration() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.manage-invoice-configuration')->with($data);
    }
    
    public function manageVendor() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.manage-vendor')->with($data);
    }

     public function manageItems() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.manage-item')->with($data);
    }

     public function manageCategory() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo,
            'save_url'        => 'save',  
        );
        return view('admin-panel.inventory.manage-category')->with($data);
    }

    public function manageUnit() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.manage-unit')->with($data);
    }
     public function managePurchase() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.manage-purchase')->with($data);
    }

    public function viewPurchaseEntry() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.view-purchase-entry')->with($data);
    }
     public function itemDetails() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.view-item-details')->with($data);
    }
     public function manageSales() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.manage-sales')->with($data);
    }

     public function viewSales() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.view-sales')->with($data);
    }

    public function salesItemDetails() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.view-sales-item-details')->with($data);
    }

     public function managePrintInvoice() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.manage-print-invoice')->with($data);
    }
    
     public function viewPdfFormat() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.pdfview')->with($data);
    }

     public function pdfview(Request $request)
    {
        if($request->has('download')){
            $pdf = PDF::loadView('admin-panel/inventory/pdfview');
            return $pdf->stream('pdfview.pdf');
        }


        return view('admin-panel.inventory.pdfview');
    }

     public function managePurchaseReturn() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.manage-purchase-return')->with($data);
    }

     public function manageSalesReturn() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.manage-sales-return')->with($data);
    }


    public function manageStockRegister() {
        $loginInfo   = get_loggedin_user_data();
        $data                 = array(
            'login_info'      => $loginInfo
        );
        return view('admin-panel.inventory.stock-register')->with($data);
    }
}
