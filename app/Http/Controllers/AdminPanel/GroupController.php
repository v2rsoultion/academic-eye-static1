<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    public function addGroup()
    {
        $loginInfo 		    = get_loggedin_user_data();
        $data 				= array(
            'login_info'    => $loginInfo,
        );
        return view('admin-panel.group.add-group')->with($data);
    }

}
