<?php $__env->startSection('content'); ?>

<section class="content profile-page">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <h2><?php echo trans('language.view_student_leave_application'); ?></h2>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <a href="<?php echo url('admin-panel/student-leave-application/add-leave-application'); ?>" class="btn btn-white btn-icon1 float-right m-l-10"> <i class="zmdi zmdi-plus"></i> </a>

                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo URL::to('admin-panel/dashboard'); ?>"><?php echo trans('language.dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><?php echo trans('language.menu_student'); ?></li>
                    <li class="breadcrumb-item"><a href="#"><?php echo trans('language.student'); ?></a></li>
                    <li class="breadcrumb-item"><a href="#"><?php echo trans('language.student_leave_application'); ?></a></li>
                    <li class="breadcrumb-item active"><a href=""><?php echo trans('language.view_student_leave_application'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="tab-content">                   
                    <div class="tab-pane active" >
                        <div class="card">
                            <!-- <div class="body">
                                <ul class="nav nav-tabs padding-0">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#"><?php echo trans('language.title'); ?></a></li>
                                    <li class="nav-item"><a class="nav-link"  href="<?php echo e(url('admin-panel/title/add-title')); ?>"><?php echo trans('language.add_title'); ?></a></li>
                                </ul>                        
                            </div> -->
                            <div class="body form-gap">
                                <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable " id="designation-table" style="width:100%">
                                <?php echo e(csrf_field()); ?>

                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><?php echo e(trans('language.stud_leave_app_reason')); ?></th>
                                            <th><?php echo e(trans('language.stud_leave_app_date_to')); ?></th>
                                            <th><?php echo e(trans('language.stud_leave_app_date_from')); ?></th>
                                            <th><?php echo e(trans('language.stud_leave_app_attachment')); ?></th>
                                            <th><?php echo e(trans('language.stud_leave_app_status')); ?></th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
</section>

<script>
    $(document).ready(function () {
        var table = $('#designation-table').DataTable({
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: "<?php echo e(url('admin-panel/student-leave-application/data')); ?>",
            
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'student_leave_reason', name: 'student_leave_reason'},
                {data: 'date_from', name: 'date_from'},
                {data: 'date_to', name: 'date_to'},
                {data: 'student_leave_attachment', name: 'student_leave_attachment'},
                {data: 'student_leave_status', name: 'student_leave_status'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 6, // your case first column
                    "width": "20%"
                },
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });


</script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('admin-panel.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>