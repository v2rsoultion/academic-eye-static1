<?php if(isset($admission['admission_form_id']) && !empty($admission['admission_form_id'])): ?>
<?php  $readonly = true; $disabled = 'disabled'; ?>
<?php else: ?>
<?php $readonly = false; $disabled=''; ?>
<?php endif; ?>
<!-- <style type="">

.theme-blush .btn-primary {
    margin-top: 4px !important;
}

</style> -->
<?php echo Form::hidden('admission_form_id',old('admission_form_id',isset($admission['admission_form_id']) ? $admission['admission_form_id'] : ''),['class' => 'gui-input', 'id' => 'admission_form_id', 'readonly' => 'true']); ?>


<p class="red" id="error-block">
<?php if($errors->any()): ?>
    <?php echo e($errors->first()); ?>

<?php endif; ?>
</p>
<!-- Basic Info section -->


<div class="row clearfix">
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.form_name'); ?> <span class="red-text">*</span> :</lable>
        <div class="form-group">
            <?php echo Form::text('form_name', old('form_name',isset($admission['form_name']) ? $admission['form_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_name'), 'id' => 'form_name']); ?>

        </div>
        <?php if($errors->has('form_name')): ?> <p class="help-block"><?php echo e($errors->first('form_name')); ?></p> <?php endif; ?>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.medium_type'); ?> <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('medium_type', $admission['arr_medium'],old('medium_type',isset($admission['medium_type']) ? $admission['medium_type'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'medium_type', 'onChange'=> 'getClass(this.value)']); ?>

                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.session_name'); ?> <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('session_id', $admission['arr_session'],old('session_id',isset($admission['session_id']) ? $admission['session_id'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'session_id', 'onChange'=> 'getBrochure(this.value)']); ?>

                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.class_name'); ?> <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('class_id', $admission['arr_class'],old('class_id',isset($admission['class_id']) ? $admission['class_id'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'class_id', 'onChange'=>'getIntake(this.value)']); ?>

                <i class="arrow double"></i>
            </label>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.no_of_intake'); ?> <span class="red-text">*</span> :</lable>
        <div class="form-group">
            <?php echo Form::hidden('no_of_intake', old('no_of_intake',isset($admission['no_of_intake']) ? $admission['no_of_intake']: 0), ['class' => 'form-control','placeholder'=>trans('language.no_of_intake'), 'id' => 'no_of_intake', 'min' => 0]); ?>

            <?php echo Form::number('no_of_intake1', old('no_of_intake1',isset($admission['no_of_intake1']) ? $admission['no_of_intake1']: 0), ['class' => 'form-control','placeholder'=>trans('language.no_of_intake'), 'id' => 'no_of_intake', 'min' => 0, 'disabled']); ?>

        </div>
        <?php if($errors->has('no_of_intake')): ?> <p class="help-block"><?php echo e($errors->first('no_of_intake')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.form_prefix'); ?> <span class="red-text">*</span> :</lable>
        <div class="form-group">
            <?php echo Form::text('form_prefix', old('form_prefix',isset($admission['form_prefix']) ? $admission['form_prefix']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_prefix'), 'id' => 'form_prefix']); ?>

        </div>
        <?php if($errors->has('form_prefix')): ?> <p class="help-block"><?php echo e($errors->first('form_prefix')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.form_type'); ?> <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('form_type', $admission['arr_form_type'],old('form_type',isset($admission['form_type']) ? $admission['form_type'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'form_type', 'onChange'=>'showOptions(this.value)']); ?>

                <i class="arrow double"></i>
            </label>
        </div>
        <?php if($errors->has('form_type')): ?> <p class="help-block"><?php echo e($errors->first('form_type')); ?></p> <?php endif; ?>
    </div>

     <div class="col-lg-3 col-md-3">
        <lable class="from_one1"><?php echo trans('language.form_message_via'); ?> <span class="red-text">*</span> :</lable>
        <div class="form-group m-bottom-0">
            <label class=" field select" style="width: 100%">
                <?php echo Form::select('form_message_via', $admission['arr_message_via'],old('form_message_via',isset($admission['form_message_via']) ? $admission['form_message_via'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'form_message_via']); ?>

                <i class="arrow double"></i>
            </label>
        </div>
        <?php if($errors->has('form_message_via')): ?> <p class="help-block"><?php echo e($errors->first('form_message_via')); ?></p> <?php endif; ?>
    </div>
</div>
<div id="admission_fields" style="display: none">
    
    <!-- Admission Information -->
    <div class="header">
        <h2><strong>Admission </strong> Information</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1"><?php echo trans('language.brochure_name'); ?> <span class="red-text">*</span> :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    <?php echo Form::select('brochure_id', $admission['arr_brochure'],old('brochure_id',isset($admission['brochure_id']) ? $admission['brochure_id'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'brochure_id','required']); ?>

                    <i class="arrow double"></i>
                </label>
            </div>
            <?php if($errors->has('brochure_id')): ?> <p class="help-block"><?php echo e($errors->first('brochure_id')); ?></p> <?php endif; ?>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1"><?php echo trans('language.form_last_date'); ?> <span class="red-text">*</span> :</lable>
            <div class="form-group">
                <?php echo Form::text('form_last_date', old('form_last_date',isset($admission['form_last_date']) ? $admission['form_last_date']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_last_date'), 'id' => 'form_last_date','required']); ?>

            </div>
            <?php if($errors->has('form_last_date')): ?> <p class="help-block"><?php echo e($errors->first('form_last_date')); ?></p> <?php endif; ?>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1"><?php echo trans('language.form_fee_amount'); ?> <span class="red-text">*</span> :</lable>
            <div class="form-group">
                <?php echo Form::number('form_fee_amount', old('form_fee_amount',isset($admission['form_fee_amount']) ? $admission['form_fee_amount']: 0), ['class' => 'form-control','placeholder'=>trans('language.form_fee_amount'), 'id' => 'form_fee_amount', 'min'=>0,'required']); ?>

            </div>
            <?php if($errors->has('form_fee_amount')): ?> <p class="help-block"><?php echo e($errors->first('form_fee_amount')); ?></p> <?php endif; ?>
        </div>

        <div class="col-lg-3 col-md-3">
            <lable class="from_one1"><?php echo trans('language.form_pay_mode'); ?> <span class="red-text">*</span> :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    <?php echo Form::select('form_pay_mode[]', $admission['arr_payment_mode'],old('form_pay_mode',isset($admission['form_pay_mode']) ? $admission['form_pay_mode'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'form_pay_mode','multiple','required']); ?>

                    <i class="arrow double"></i>
                </label>
            </div>
            <?php if($errors->has('form_pay_mode')): ?> <p class="help-block"><?php echo e($errors->first('form_pay_mode')); ?></p> <?php endif; ?>
        </div>
        <div class="col-lg-3 col-md-3">
            <lable class="from_one1"><?php echo trans('language.form_email_receipt'); ?> <span class="red-text">*</span> :</lable>
            <div class="form-group m-bottom-0">
                <label class=" field select" style="width: 100%">
                    <?php echo Form::select('form_email_receipt', $admission['arr_email_receipt'],old('form_email_receipt',isset($admission['form_email_receipt']) ? $admission['form_email_receipt'] : ''), ['class' => 'form-control show-tick select_form1 select2','id'=>'form_email_receipt','required']); ?>

                    <i class="arrow double"></i>
                </label>
            </div>
            <?php if($errors->has('form_email_receipt')): ?> <p class="help-block"><?php echo e($errors->first('form_email_receipt')); ?></p> <?php endif; ?>
        </div>
    </div>
</div>

<!-- Instruction Information -->
<div class="header">
    <h2><strong>Instruction  </strong> Information</h2>
</div>
<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <lable class="from_one1"><?php echo trans('language.form_instruction'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::textarea('form_instruction', old('form_instruction',isset($admission['form_instruction']) ? $admission['form_instruction']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_instruction'), 'id' => 'form_instruction', 'rows' => '3']); ?>

        </div>
        <?php if($errors->has('form_instruction')): ?> <p class="help-block"><?php echo e($errors->first('form_instruction')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12">
        <lable class="from_one1"><?php echo trans('language.form_information'); ?> :</lable>
        <div class="form-group">
            <?php echo Form::textarea('form_information', old('form_information',isset($admission['form_information']) ? $admission['form_information']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_information'), 'id' => 'form_information', 'rows' => '3']); ?>

        </div>
        <?php if($errors->has('form_information')): ?> <p class="help-block"><?php echo e($errors->first('form_information')); ?></p> <?php endif; ?>
    </div>
</div>

<!-- Message Information -->
<div class="header">
    <h2><strong>Message  </strong> Information</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <lable class="from_one1"><?php echo trans('language.form_success_message'); ?> <span class="red-text">*</span> :</lable>
        <div class="form-group">
            <?php echo Form::textarea('form_success_message', old('form_success_message',isset($admission['form_success_message']) ? $admission['form_success_message']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_success_message'), 'id' => 'form_success_message', 'rows' => '3']); ?>

        </div>
        <?php if($errors->has('form_success_message')): ?> <p class="help-block"><?php echo e($errors->first('form_success_message')); ?></p> <?php endif; ?>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12">
        <lable class="from_one1"><?php echo trans('language.form_failed_message'); ?> <span class="red-text">*</span> :</lable>
        <div class="form-group">
            <?php echo Form::textarea('form_failed_message', old('form_failed_message',isset($admission['form_failed_message']) ? $admission['form_failed_message']: ''), ['class' => 'form-control','placeholder'=>trans('language.form_failed_message'), 'id' => 'form_failed_message', 'rows' => '3']); ?>

        </div>
        <?php if($errors->has('form_failed_message')): ?> <p class="help-block"><?php echo e($errors->first('form_failed_message')); ?></p> <?php endif; ?>
    </div>
</div>

<div class="row clearfix">                            
    <div class="col-sm-12">
        <hr />
    </div>
    <div class="col-sm-12">
        <?php echo Form::submit('Save', ['class' => 'btn btn-raised btn-round btn-primary','name'=>'save']); ?>

        <a href="<?php echo url('admin-panel/dashboard'); ?>" ><?php echo Form::button('Cancel', ['class' => 'btn btn-raised btn-round']); ?></a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
    });
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) ||  /^[a-z0-9\-\s]+$/i.test(value);
        }, "Only alphanumeric characters");

        $("#admission-form").validate({

            /* @validation  states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation  rules 
             ------------------------------------------ */

            rules: {
                form_name: {
                    required: true,
                    lettersonly:true
                },
                session_id: {
                    required: true,
                },
                class_id: {
                    required: true
                },
                medium_type: {
                    required: true
                },
                no_of_intake: {
                    required: true
                },
                form_prefix: {
                    required: true
                },
                form_type: {
                    required: true
                },
                form_success_message: {
                    required: true
                },
                form_failed_message: {
                    required: true
                },
                form_message_via: {
                    required: true
                }
            },

            /* @validation  highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    element.closest('.form-group').after(error);
                    // error.insertAfter(element.parent());
                }
            }
        });
        
        $('#form_last_date').bootstrapMaterialDatePicker({ weekStart : 0,time: false });
        
    });

    function showOptions(type){
        $(".mycustloading").show();
        if(type == 0){
            $('#admission_fields').show();
            $(".mycustloading").hide();
        } else {
            $('#admission_fields').hide();
            $(".mycustloading").hide();
        }
    }

    function getClass(medium_type)
    {
        if(medium_type != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/class/get-class-data')); ?>",
                type: 'GET',
                data: {
                    'medium_type': medium_type
                },
                success: function (data) {
                    $("select[name='class_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='class_id'").html('<option value="">Select Class</option>');
            $(".mycustloading").hide();
        }
    }

    function getBrochure(session_id)
    {
        if(session_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/brochure/get-brochure-data')); ?>",
                type: 'GET',
                data: {
                    'session_id': session_id
                },
                success: function (data) {
                    $("select[name='brochure_id'").html(data.options);
                    $(".mycustloading").hide();
                }
            });
        } else {
            $("select[name='brochure_id'").html('<option value="">Select Brochure</option>');
            $(".mycustloading").hide();
        }
    }
    function getIntake(class_id)
    {
        if(class_id != "") {
            $(".mycustloading").show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "<?php echo e(url('admin-panel/class/get-total-intake-data')); ?>",
                type: 'GET',
                data: {
                    'class_id': class_id
                },
                success: function (data) {
                    $("#no_of_intake").val(data);
                    $("#no_of_intake1").val(data);
                    $(".mycustloading").hide();
                    if(data == 0){
                        $('#error-block').html("Sorry, We have already exceed class's student capacity.");
                        $(':input[type="submit"]').prop('disabled', true);
                    } else {
                        $(':input[type="submit"]').prop('disabled', false);
                        $('#error-block').html('');
                    }
                }
            });
        } else {
            $("#no_of_intake").vl('0');
            $("#no_of_intake1").vl('0');
            $(".mycustloading").hide();
        }
    }

</script>